function setCookie(cname, cvalue, exdays) {
    var d = new Date();
    d.setTime(d.getTime() + (exdays * 24 * 60 * 60 * 1000));
    var expires = "expires=" + d.toUTCString();
    document.cookie = cname + "=" + cvalue + "; " + expires;
}

function getCookie(cname) {
    var name = cname + "=";
    var ca = document.cookie.split(';');
    for (var i = 0; i < ca.length; i++) {
        var c = ca[i];
        while (c.charAt(0) == ' ') c = c.substring(1);
        if (c.indexOf(name) == 0) return c.substring(name.length, c.length);
    }
    return "";
}

function checkCookie(valor) {
    var user = getCookie(valor);
    if (user != "") {
        return true;
    }
    else {
        return false;
    }
}

function sendDatalayer(url_val, event) {
    if (checkCookie('frmunitec') != true) {
        dataLayer.push({
            'url_value': url_val, //dato dinámico
            'event': event//dato estático
        });
    }
}

function quitaAcentos(str) {
    for (var i = 0; i < str.length; i++) {
        //Sustituye "á é í ó ú"
        if (str.charAt(i) == "á") str = str.replace(/á/, "a");
        if (str.charAt(i) == "é") str = str.replace(/é/, "e");
        if (str.charAt(i) == "í") str = str.replace(/í/, "i");
        if (str.charAt(i) == "ó") str = str.replace(/ó/, "o");
        if (str.charAt(i) == "ú") str = str.replace(/ú/, "u");
    }
    return str;
}

function blinkFieldForm(id) {
    jQuery("#" + id).addClass("blink-animation");
    setTimeout(function () { jQuery("#" + id).removeClass("blink-animation"); }, 2500);
}

function clickSeeDown() {
    $('#project').focus();
}

var safeUrl = (/unitec.mx/.test(window.location.href)) ? "https://unitecmx-universidadtecno.netdna-ssl.com/wp-content/themes/temaunitec" : urlwp;

var objIncluye = {
    prepa: ["Acceso a Biblioteca Virtual las 24 horas", "Uso de laboratorios tecnológicos sin costo", "Actividades culturales y deportivas", "Talleres de orientación vocacional", "Centro de Apoyo y Desarrollo Estudiantil"],
    licenciaturas_ingenierias: ["Acceso a Biblioteca Virtual las 24 horas", "Uso de laboratorios tecnológicos sin costo", "Actividades culturales y deportivas", "Bolsa de Trabajo con más de 43 mil vacantes en línea", "Centro de Apoyo y Desarrollo Estudiantil"],
    enfermeria: ["Primer juego de uniformes sin costo", "Uso de Hospital simulado y laboratorios de alta fidelidad", "Actividades culturales y deportivas", "Bolsa de Trabajo con más de 43 mil vacantes en línea", "Centro de Apoyo y Desarrollo Estudiantil"],
    fisioterapia: ["Primer juego de uniformes sin costo", "Uso del Gimnasio terapéutico y laboratorios de alta fidelidad", "Actividades culturales y deportivas", "Bolsa de Trabajo con más de 43 mil vacantes en línea", "Centro de Apoyo y Desarrollo Estudiantil"],
    nutricion: ["Acceso a Biblioteca Virtual las 24 horas", "Uso de Hospital simulado y laboratorios de alta fidelidad", "Actividades culturales y deportivas", "Bolsa de Trabajo con más de 43 mil vacantes en línea", "Centro de Apoyo y Desarrollo Estudiantil"],
    odontologia: ["Uso de laboratorios tecnológicos sin costo", "Prácticas en la Clínica de Odontología con pacientes reales", "Actividades culturales y deportivas", "Bolsa de Trabajo con más de 43 mil vacantes en línea", "Centro de Apoyo y Desarrollo Estudiantil"],
    gastronomia: ["Utensilios e insumos para prácticas sin costo", "Primer juego de uniformes", "Acceso a cocinas de simulación desde el primer cuatrimestre", "Bolsa de Trabajo con más de 43 mil vacantes en línea", "Centro de Apoyo y Desarrollo Estudiantil"],
    turismo: ["Primer juego de uniformes sin costo", "Convenio para prácticas profesionales en los mejores restaurantes y hoteles de México", "Uso de laboratorios tecnológicos sin costo", "Bolsa de Trabajo con más de 43 mil vacantes en línea", "Centro de Apoyo y Desarrollo Estudiantil"],
    diplomado: ["Acceso a Biblioteca Virtual las 24 horas", "Uso de laboratorios tecnológicos sin costo", "Bolsa de Trabajo con más de 43 mil vacantes en línea", "Plataforma educativa Blackboard"],
    posgrado: ["Acceso a Biblioteca Virtual las 24 horas", "Uso de laboratorios tecnológicos sin costo", "Bolsa de Trabajo con más de 43 mil vacantes en línea", "Plataforma educativa Blackboard"]
}

var videosLineas = {
    ingenieria: "https://www.unitec.mx/wp-content/themes/temaunitec/assets/videos_calculadora/ingenierias.mp4",
    licenciatura: "https://www.unitec.mx/wp-content/themes/temaunitec/assets/videos_calculadora/licenciaturas.mp4",
    online: "https://www.unitec.mx/wp-content/themes/temaunitec/assets/videos_calculadora/online.mp4",
    posgrado: "https://www.unitec.mx/wp-content/themes/temaunitec/assets/videos_calculadora/posgrado.mp4",
    preparatoria: "https://www.unitec.mx/wp-content/themes/temaunitec/assets/videos_calculadora/preparatoria.mp4",
    salud: "https://www.unitec.mx/wp-content/themes/temaunitec/assets/videos_calculadora/salud.mp4"
}

var xCalculadora = 0;
var yCalculadora = 0;
var pasoFinalCalculadora = false;
var regresoAnterior = true;

function getRelation(carrera_string) {
    switch (carrera_string) {
        case "preparatoria":
            return "prepa";
            break;
        case "licenciatura":
            return "licenciaturas_ingenierias";
            break;
        case "ingenieria":
            return "licenciaturas_ingenierias";
            break;
        case "enfermeria":
            return "enfermeria";
            break;
        case "enfermería":
            return "enfermeria";
            break;
        case "fisioterapia":
            return "fisioterapia";
            break;
        case "nutrición":
            return "nutricion";
            break;
        case "nutricion":
            return "nutricion";
            break;
        case "odontología":
            return "odontologia";
            break;
        case "odontologia":
            return "odontologia";
            break;
        case "salud":
            return "odontologia";
            break;
        case "gastronomía":
            return "gastronomia";
            break;
        case "gastronomia":
            return "gastronomia";
            break;
        case "turismo":
            return "turismo";
            break;
        case "posgrado":
            return "posgrado";
            break;
        case "diplomado":
            return "diplomado";
            break;
    }
}

function changeVideo(tipoVideo) {
    var direccionVideo = videosLineas[tipoVideo];
    $("#video_background source").attr("src", direccionVideo);
    $("#video_background")[0].load();
    // $("#video_background")[0].play();
    // <video controls id="video_background" class="PromotionGlobalLarge-videoBackgroundWrapper" preload="auto" autoplay="false" loop="loop" muted="muted" volume="0"><source src="/wp-content/themes/temaunitec/calculadora/videos-calculadora/ingenierias.mp4" type="video/mp4"> Video not supported </video>
}

function changeAddInCareers(careerSearch) {
    /*Llamada en render_modalidades */
    // console.log("Recibo la modalidad 0 ");
    // console.log(careerSearch);
    var careerLineaWeb = careerSearch.lineaweb.toLowerCase();
    var careerName = careerSearch.nombre.toLowerCase();
    var turismoLead = "LICENCIAURA INTERACIONAL EN TURISMO Y REUNIONES";
    turismoLead = turismoLead.toLowerCase();
    var gastronomiaLead = "LICENCIATURA EN GASTRONOMÍA";
    gastronomiaLead = gastronomiaLead.toLowerCase();
    if (careerLineaWeb == "licenciatura") {
        if (careerName == "gastronomia" || careerName == "gastronomía" || careerName == gastronomiaLead) {
            careerLineaWeb = "gastronomia";
        } else if (careerName == turismoLead) {
            careerLineaWeb = "turismo";
        }
    } else if (careerLineaWeb == "salud") {
        if (careerName == "enfermeria" || careerName == "enfermería" || careerName == "fisioterapia" || careerName == "nutrición" || careerName == "nutricion" || careerName == "odontología" || careerName == "odontologia") {
            careerLineaWeb = careerName;
        }
    }
console.warn(careerLineaWeb)
    var selectCareer = objIncluye[getRelation(careerLineaWeb)];
    var html = "";
    var unam = 2;
    for (var i = 0; i < selectCareer.length; i++) {
        html += "<li id='listaBen-" + unam +"'>" + selectCareer[i] + "</li>";
    }

    $('.check').empty();
    $('.check').append(html);

}

// function muestraPaso(seccion){
//     if(jQuery('body').hasClass('inicio')){
//         jQuery('.quest.p-activa').hide();
//         jQuery('#'+seccion).show().addClass('p-activa');
//     }

// }



function muestraPaso(actual, seccion) {



    if (jQuery('body').hasClass('inicio')) {                              //Verifica que este extendido el formulario

        var linea_seleccionada = getUsuario().linea.id_name;
        console.log(linea_seleccionada);

        jQuery('.quest.p-activa').addClass('an-slideUp');               //Agrega SlideUp a la Quest Activa

        setTimeout(function () {
            jQuery('.quest.p-activa').addClass('hide');                 //Esconde la preg Activa 
            jQuery('#' + seccion).removeClass('hide').addClass('p-activa').addClass('an-slideDownToUp');
            jQuery('#' + seccion).removeClass('an-slideUp').removeClass('an-slideDownToUp');

        }, 400);

        setTimeout(function () {
            console.log("Estmoas en:" + actual + "Se va a ir a:" + seccion);
            jQuery('#' + actual).removeClass('an-slideUp');
        }, 400);

        switch (seccion) {
            case "container_carrera_modalidad":
                regresaPaso('container_carrera_modalidad', 'business-line');
                break;
        }




    }

}

function checkOffset() {
    if ($('#selector-float').offset().top + $('#selector-float').height() >= $('#footer').offset().top)
        $('#selector-float').css('position', 'absolute');
    if ($(document).scrollTop() + window.innerHeight < $('#footer').offset().top)
        $('#selector-float').css('position', 'fixed'); // restore when you scroll up
    // $('.px').text($(document).scrollTop() + window.innerHeight);
}
jQuery(document).scroll(function () {
    //checkOffset();
});

//Triggers
jQuery(document).ready(function () {

    /* Funcionalidad en Calculadora UNITEC */
    // if(checkCookie('regcompleto')==1){
    //     jQuery("#aplicativo-calculadora").show();
    //     $('#theme-calculadora-nueva-css').prop('disabled', false);
    //     // $('#theme-calculadora-css').prop('disabled',true);
    //     jQuery("#todo_calculadora_body").hide();
    // }
    /* Funcionalidad en Calculadora UNITEC */

    //INICIALIZACION DE JSONS
    getJsonCarreras();
    getJsonLinksCategorias();
    getJsonBasura();
    getJsonBasuraEmail();
    getJsonCalcCostos();
    getJsonBecasCalc();
    render_lineas_negocio();

    jQuery('#project').focus(function () {
        jQuery(this).val("");
        checkAllInputs();
        on();
    })


    /*******INPUT DE RANGO******/
    jQuery('#range').on("change", function () {
        $('.thumb.active .value').hide();
        var checkRangeSend = $("#range").attr("data-send");
        if (checkRangeSend == "1") {
            $("#range").attr("data-send", "2");
        } else if (checkRangeSend == "2") {
            $("#range").attr("data-send", "2");
        } else {
            $("#range").attr("data-send", "1");
        }
        calcular_uno();
        finaliza();
        console.log('inout de rango 1')
    });

    jQuery(document).on('input change', '#range', function () {
        // sendDatalayer('calculadora/sec-promedio', 'StepCalculator');

        guardaUsuario('promedio', this.value);
        $('.output').html(this.value);
        console.log('inout de rango 3')

    });


    //   var str = "Hello world, welcome to the universe.";
    // var n = str.indexOf("welcome");

    /********BOTONES SELECT MODALIDAD Y CAMPUS********/
    jQuery('select').not('.form-control').on('change', function () {
        console.log('Guardando modalidad');
        console.log(jQuery('#render_modalidad option:selected').val());
        // sendDatalayer('calculadora/sec-modalidad', 'StepCalculator');



        var dataItem = jQuery('#render_modalidad option:selected').data();
        console.log(dataItem);
        guardaUsuario('modalidad', dataItem);
        jQuery('.rs-mod').text('Modalidad ' + jQuery('#render_modalidad option:selected').text());
        guardaUsuario('campus', jQuery('#render_campus option:selected').val());
        // sendDatalayer('calculadora/sec-campus', 'StepCalculator');

        /*Modalidad 5 PREPA UNAM Se muestra Inicio de Clases Anuak By SRP 19-09-2019*/
        if ( jQuery('#render_modalidad').val() == 5 ) {
            jQuery("#alert-prepaunam").removeClass('d-none');
        } else{
            jQuery("#alert-prepaunam").addClass('d-none');
        }
        /*End Modalidad 5 PREPA UNAM Se muestra Inicio de Clases Anuak By SRP 19-09-2019*/

        render_selects();
        /* Cuando campus y modalidad tienen valores */
        if (jQuery('#render_campus').val() != 0 && jQuery('#render_modalidad').val() != 0) {
            /* Cuando es modalidad en linea y campus en linea */
            if (jQuery('#render_campus').val() == "ONL" && jQuery('#render_modalidad').val() != 3) {
                on();
            } else {
                off();
            }
            regresoAnterior = false;
            /* Avance automático hacia promedio */
            // muestraPaso('modalidad', 'promedio');
            $('.steps').text('6');
            if (jQuery('#render_campus').attr('data-preselected')) {
                guardaUsuario('campus', jQuery('#render_campus').attr('data-preselected'));
            } else {
                guardaUsuario('campus', jQuery('#render_campus').val());
            }
            jQuery('.rs-campus').text('Campus ' + jQuery('#render_campus option:selected').text());

            console.log("PRUEBA DE ENVÍO");

            var checkOverlaySend = $("#overlay").attr("data-send");
            if (!jQuery('body').hasClass('inicio')) {
                if (checkOverlaySend == "1") {
                    $("#overlay").attr("data-send", "2");
                    // envioProcWeb();
                } else if (checkOverlaySend == "2") {
                    $("#overlay").attr("data-send", "2");
                    // envioProcWeb();
                } else {
                    $("#overlay").attr("data-send", "1");
                }
            }



            //guardaUsuario('linea', dataItem);  
        }

        /* Auto seleccion cuando se selecciona un campus online */
        if (jQuery('#render_modalidad').val() == 0 && jQuery('#render_campus').val() == "ONL") {
            jQuery('#render_modalidad').next().next().children("div:contains('En línea')").click();
        }

        if (jQuery('#render_modalidad').val() != 0){
            var dataItem = jQuery('#render_modalidad option:selected').data();
            guardaUsuario('modalidad', dataItem);
            /*Implementación para Agregar Diplomados Educación Continúa By SRP 13-02-2019*/
            var nivelDiplomado = getCampo('modalidad').catcosto;

            switch( nivelDiplomado ){
                case "DIPEC":
                    var nivelAnterior = getNivelAnteriorPorCard(nivelDiplomado);
                break;

                default:
                    var nivelAnterior = getNivelAnterior(getCampo('modalidad').interes);
                break;
            }
            /*End Implementación para Agregar Diplomados Educación Continúa By SRP 13-02-2019*/
            jQuery('.rs-canterior').text(nivelAnterior);
            calcular_uno();
        }
        
    });


    jQuery('.reset').on('click', function () {
        render_modalidades(getModalidades(getCampo('carrera').IdCategoria));
        on();
    });

    jQuery('#render_campus').on('change', function () {
        var a = getModalidades(getCampo("carrera").IdCategoria); // Obtengo de nuevo todas las carreras para filtrar por campus 
        var html = "";
        var campus_select = jQuery('#render_campus').val();
        var modalidad_select = jQuery('#render_modalidad').val();

        var modalidadesSize = a.length;

        if (jQuery('#render_campus').val() != 0) {   //Si Cambia el campus
            html += '<option value="0">Modalidad</option>';
            console.log('cambio campus');
            console.log('Campus Elegido:' + campus_select);
            jQuery.each(a, function (index, value) {
                console.log(a);
                console.log('My array has at position ' + index + ', this value: ' + value.campus);
                console.log(value);
                var c = value.campus.toString();

                if (c.search(campus_select) != -1) {
                    console.log('My array has at position ' + index + ', this value: ' + value.modalidad + modalidadtxt(value.modalidad));
                    html += '<option data-Grupo_carreras="' + value.Grupo_carreras + '" data-IdDynamics="' + value.IdDynamics + '" data-campus="' + value.campus + '"  data-catCosto="' + value.catCosto + '" data-interes="' + value.interes + '" data-lineaweb="' + value.lineaweb + '" data-modalidad="' + value.modalidad + '"  data-nombre="' + value.nombre + '" value="' + value.modalidad + '">' + modalidadtxt(value.modalidad) + '</option>';
                };
            });
            jQuery('#render_modalidad').html(html).delay(500).queue(function (next) {
                if (modalidadesSize == 1) {
                    jQuery('#render_modalidad').next().addClass('inactive');
                } else if (modalidadesSize > 1) {
                    jQuery('#render_modalidad').next().removeClass('inactive');
                }
                next();
            });
            jQuery('#render_modalidad option[value="' + modalidad_select + '"]').attr('selected', 'selected');


        }
    });



    jQuery('#render_modalidad').on('change', function () {
        var a = getModalidades(getCampo("carrera").IdCategoria); // Obtengo de nuevo todas las carreras para filtrar por campus 
        var html = "";
        var campus_select = jQuery('#render_campus').val();
        var modalidad_select = jQuery('#render_modalidad').val();
        var sizeCampus = 0;
        var campusValue = "";

        if (jQuery('#render_modalidad').val() != 0) { //si cambia modalidad pinta campus 
            var campus = [];
            var html2 = "";
            jQuery.each(a, function (index, value) {
                console.log(value.modalidad + " Busca modalidad:" + modalidad_select);
                if (value.modalidad == modalidad_select) {
                    console.log('Encontro');
                    console.log(value);
                    console.log("campus encontrados");
                    var ArrCamp = value.campus.toString().split(",");
                    sizeCampus = ArrCamp.length;
                    if (ArrCamp.length == 1) {
                        jQuery('#render_campus').attr('data-preselected', ArrCamp[0]);
                        html2 += '<option selected value="' + ArrCamp[0] + '">' + campustxt(ArrCamp[0]) + '</option>';
                    } else {
                        html2 += '<option value="0">Campus</option>';
                        jQuery('#render_campus').removeAttr('data-preselected');
                        jQuery.each(ArrCamp, function (index1, value1) {
                            console.log(value1);
                            html2 += '<option value="' + value1 + '">' + campustxt(value1) + '</option>';
                        });
                    }
                }
            }); //Guarda los arreglos de campus

            jQuery('#render_campus').html(html2).delay(200).queue(function (next) {
                // if(campus_select != "" || campus_select != undefined){
                //     jQuery('#render_campus option[value="' + campus_select + '"]').attr('selected', 'selected');
                // } else {
                //     jQuery('#render_campus option:first').next().attr('selected', 'selected');
                // }
                console.log("El valor de los campus es de: " + sizeCampus);
                /* Cuando solo hay un campus para autoseleccionarse */
                if (sizeCampus == 1) {
                    jQuery('#render_campus').next().addClass('inactive');
                    /*Avance automático hacia promedio */
                    // muestraPaso('modalidad', 'promedio');
                    regresoAnterior = false;
                    off();
                    calcular();
                    guardaUsuario('campus', jQuery('#render_campus').attr('data-preselected'));
                    var checkOverlaySend = $("#overlay").attr("data-send");
                    if (!jQuery('inicio').hasClass('inicio')) {
                        if (checkOverlaySend == "1") {
                            $("#overlay").attr("data-send", "2");
                            // envioProcWeb();
                        } else if (checkOverlaySend == "2") {
                            $("#overlay").attr("data-send", "2");
                            // envioProcWeb();
                        } else {
                            $("#overlay").attr("data-send", "1");
                        }
                    }

                } else {
                    jQuery('#render_campus').next().removeClass('inactive');
                }
                next();
            });

            jQuery('#render_campus option[value="' + campus_select + '"]').attr('selected', 'selected');


        }


        // if(jQuery('select.ipt-b').eq(0).val() != '0' && jQuery('select.ipt-b').eq(1).val()!='0'){

        //     muestraPaso('modalidad','promedio');
        // } 

    });

    /* Trigger (Linea de Negocio)
    * 
    */
    jQuery(".card.card-calculadora").on("mouseup", function () {
        // sendDatalayer('calculadora/sec-categoriaclik', 'StepCalculator');
        var dataItem = jQuery(this).find("div").data();       //Obtiene todo el data del primer div 
        // console.log("EL DATA ITEM ES: ");
        // console.log(dataItem);
        select(".card", this);                               //this=card-body 
        guardaUsuario('linea', dataItem);                   //guarda en LocalStorage
        var nivelAnterior = getNivelAnteriorPorCard(getCampo('linea').id_name);
        jQuery('.rs-canterior').text(nivelAnterior);
        var a = getCarrerasBien(getCarreras(dataItem.id_name));     //Solicita las carreras y las pinta en el select  
        changeVideo(dataItem.id_name.toLowerCase());
        console.log("HACIENDO RENDER DE CARRERAS");
        console.log(a);
        render_carreras(a);

        if (dataItem.id_name != "PREPARATORIA") {
            // muestraPaso('business-line', 'carrera');             //Se va al siguiente paso
            muestraPaso('business-line', 'container_carrera_modalidad');             //Se va al siguiente paso
            $('.steps').text('4');
            regresaPaso('container_carrera_modalidad', 'business-line');
        }
        if (jQuery('body').hasClass('inicio') == false) {
            on();
            jQuery('.reset').click();
            jQuery('#project').val('');
            // console.log("EL VALOR DE PROJECT ES: " +  jQuery('#project').val());
            jQuery('#project').focus();
            if (dataItem.id_name == "PREPARATORIA") {
                jQuery('#project').val("Preparatoria");
                // console.log("LA SELECCION ES PREPA: ");
                // console.log( $('.ui-menu-item div').first());
                // $('.ui-menu-item div').first().click();
            }
            checkAllInputs();
        } else {
            if (dataItem.id_name == "PREPARATORIA") {
                jQuery('#project').val("Preparatoria");
            }
        };
    });

    /********CLICK EN LA ULTIMA ETAPA***********/
    // jQuery("#resultado").on("click", function(){
    //     finaliza();
    // });

    /*******ESQUEMA DE PAGOS CONTADO O MENSUAL********/
    jQuery("body").on("click", ".btn-dual-y", function () {
        //var catCst = getUsuario().modalidad.catcosto;//mostrar ahorro de contado DIPEC
        select(".btn-dual-y", this);
        jQuery('.rs-tipopago').text(jQuery(this).text());
        if (jQuery(this).text().toLowerCase() == 'mensual' || jQuery(this).text().toLowerCase() == '1 módulo') {
            //mostrar ahorro de contado DIPEC
            /*if(catCst == "DIPEC"){
                jQuery(".r-ahorro").hide()
            }*/
            jQuery('.rs-pg-final').text("$" + getCampo('mensual').format());
            jQuery('.rs-ahorro-mns').text("$" + getCampo('mensualAhorro').format());
        } else {
            //mostrar ahorro de contado DIPEC
            /*if(catCst == "DIPEC"){
                jQuery(".r-ahorro").show()
            }*/
            jQuery('.rs-pg-final').text("$" + getCampo('contado').format());
            jQuery('.rs-ahorro-mns').text("$" + getCampo('contadoAhorro').format());

        }
    });

    /*******PLAN A 3 Y 4 AÑOS********/
    jQuery("body").on("click", ".btn-dual", function () {
        select(".btn-dual", this);
        var duracion = getCampo('costos').duracion;
        console.log("GUARDO MATERIAS CLICK EN AÑOS: " + duracion[jQuery(this).data('termino')].materias);
        guardaUsuario('materias', duracion[jQuery(this).data('termino')].materias);
        jQuery('.btn-circ').removeClass("selected");
        var mat = getCampo('materias');
        console.log(mat);
        jQuery('.btn-circ').eq(mat - 1).addClass('selected');
        calcular();

    });

    /*******NUMERO DE MATERIAS********/
    jQuery("body").on("click", ".btn-circ", function () {
        select(".btn-circ", this);
        jQuery('.btn-dual.selected').removeClass("selected");
        var mat_sel = jQuery(this).text();
        var final;
        jQuery.each(getCampo('costos').duracion, function (key, value) {
            if (value.materias == mat_sel) { final = key; }
        });


        jQuery('[data-termino="' + final + '"]').addClass('selected');
        guardaUsuario('materias', jQuery(this).text());
        calcular();
    });

    /*******INPUT DE BUSQUEDA DE CARRERAS********/
    jQuery('#project').on("focus", function () {
        $('#project').autocomplete('search', $('#project').val())
    });



});

function finalizaBoton() {
    jQuery('.breadcrumbs').addClass('d-none');
    var checkRangeSend = $("#range").attr("data-send");
    if (checkRangeSend == "1") {
        $("#range").attr("data-send", "2");
    } else if (checkRangeSend == "2") {
        $("#range").attr("data-send", "2");
    } else {
        $("#range").attr("data-send", "1");
    }
    calcular_uno();
    finaliza();
};

function regresaPaso(actual, movingTo) {
    if (jQuery('body').hasClass('inicio')) {
        if (actual == 'promedio' && regresoAnterior == false) {
            console.log('Omito todo');
            jQuery('.controls').removeClass('d-none');
            jQuery('.controls a').unbind();
            jQuery('.controls a, #bread2').on('click', function () {
                // Funcionalidad Breadcrumbz AMC
                jQuery('#bread3').removeClass('breadActive');
                jQuery('#bread1').removeClass('breadActive');
                jQuery('#bread2').addClass('breadActive');
                jQuery('.select-items').empty();
                muestraPaso('promedio', 'container_carrera_modalidad');
            });
            $('.steps').text('6');
        } else {
            console.log("Moviendo a " + movingTo);
            switch (movingTo) {
                case "business-line":
                    jQuery('.controls').removeClass('d-none');
                    $('.steps').text('4');
                    jQuery('.controls a').unbind();
                    jQuery('.controls a,#bread1').on('click', function () {
                        muestraPaso(actual, movingTo);
                        jQuery('.controls').addClass('d-none');
                        jQuery('#project').val("");
                        jQuery("#carrera").removeClass("hide");
                        jQuery("#modalidad").addClass("hide");
                        jQuery("#padre_tutor").addClass("hide");
                        // Funcionalidad Breadcrumbz AMC
                        jQuery('#bread3').removeClass('breadActive');
                        jQuery('#bread2').removeClass('breadActive');
                        jQuery('#bread1').addClass('breadActive');
                    });
                    break;
                case "carrera":
                    jQuery('.controls').removeClass('d-none');
                    jQuery('.controls a').unbind();
                    jQuery('.controls a').on('click', function () {
                        muestraPaso(actual, movingTo);

                    });
                    break;
                default:
                    jQuery('.controls').removeClass('d-none');
                    jQuery('.controls a').unbind();
                    jQuery('.controls a').on('click', function () {
                        muestraPaso(actual, movingTo);
                    });
                    break;
            }
        }
    }


}


function select(clase, el) {
    $(clase).each(function () { $(this).removeClass('selected') });
    //Si no se especifica la clase de btn-dual-y causa conflictos
    if (!pasoFinalCalculadora && clase != ".btn-dual-y") {
        setTimeout(function () { $(el).addClass('selected'); }, 500);
    } else {
        $(el).addClass('selected');
    }


}

function on() {
    //document.getElementById("overlay").style.display = "block";
    jQuery('#results').addClass('blur');
    jQuery('#overlay').fadeIn();
}

function off() {
    //document.getElementById("overlay").style.display = "none";
    jQuery('#results').removeClass('blur');
    jQuery('#overlay').fadeOut();
}

function checkAllInputs() {
    var faltantes = 0;
    /* Controla todos los inputs que faltan */
    if (jQuery('#project').val() != "") {
        jQuery('#calc-apply-career').hide();
        jQuery('#project').removeClass('missing-select');
    } else {
        jQuery('#calc-apply-career').show();
        jQuery('#project').addClass('missing-select');
        on();
        faltantes++;
    }

    if (jQuery('#render_campus').val() != 0) {
        jQuery('#calc-apply-campus').hide();
        jQuery('#render_campus').next().removeClass('missing-select');
        jQuery('.rs-campus').text('Campus ' + jQuery('#render_campus option:selected').text());
    } else {
        jQuery('#calc-apply-campus').show();
        jQuery('#render_campus').next().addClass('missing-select');
        on();
        faltantes++;
    }

    if (jQuery('#render_modalidad').val() != 0) {
        jQuery('#calc-apply-type').hide();
        jQuery('#render_modalidad').next().removeClass('missing-select');
        jQuery('.rs-mod').text('Modalidad ' + jQuery('#render_modalidad option:selected').text());
    } else {
        jQuery('#calc-apply-type').show();
        jQuery('#render_modalidad').next().addClass('missing-select');
        on();
        faltantes++;
    }

    var linea_seleccionada = getUsuario().linea.id_name;
    if (linea_seleccionada == "PREPARATORIA") {
        if (jQuery('#render_padre_tutor').val() != "Selecciona") {
            jQuery('#calc-apply-padre').hide();
            jQuery('#render_padre_tutor').next().removeClass('missing-select');
        } else {
            jQuery('#calc-apply-padre').show();
            jQuery('#render_padre_tutor').next().addClass('missing-select');
            on();
            faltantes++;
        }
    }else{
        jQuery('#calc-apply-padre').hide();
    }
    

    if (faltantes == 0 && jQuery('body').hasClass('inicio')) {
        if(getUsuario().modalidad != undefined){
            if(getUsuario().modalidad.catcosto != "DENT"){
                jQuery('.btn-dual').last().click();
            }
        }
    }

    if(faltantes == 0){
        if(getUsuario().modalidad != undefined){
            if(getUsuario().modalidad.catcosto == "DENT"){
                console.log("DANDO CLICK EN LAS 5 MATERIAS");
                jQuery('.btn-circ').eq(4).click();
            }
        }
    }
}

function cerrarModalCalculadora() {
    $('#calculadora-css').attr('disabled', 'disabled');
    $('#calculadora-css').prop("disabled", true);
    $('#bootstrapcss-css').removeAttr('disabled');
    $('#mdb-css').removeAttr('disabled');
    $('#bootstrapcss-css').prop('disabled', false);
    $('#mdb-css').prop('disabled', false);

    if (yCalculadora != 0) {
        console.log("Cambiando scroll en y " + yCalculadora);
        setTimeout(function (xCalculadora, yCalculadora) { window.scrollTo(xCalculadora, yCalculadora); }, 700, xCalculadora, yCalculadora);
    }
}



/* SEGUNDA PARTE JS */




function expandControls(){
    if(jQuery(".more-controls").attr('data-expand') == 0){
        /* Funciones cuando se cierran los controles */
        // jQuery("#selector-float").css('left', function(){ return $(this).offset().left; })
        // .animate({"left":"-40%"}, {duration: "slow",queue: "selector-animation", complete: function(){
        //     jQuery("#video_background").addClass('adjust-large-video');
        //     jQuery(".more-controls").attr('data-expand',"1");
        //     alert("TERMINO");
        // }}).dequeue('selector-animation');
        // jQuery("#results").animate({width:'95%'}, {duration:'slow', queue: "selector-animation"}).dequeue('selector-animation');
        jQuery("#video_background").addClass('adjust-large-video');
        jQuery(".overlay-controls").removeClass("d-none");
        jQuery(".more-controls").attr('data-expand',"1");
        jQuery("#selector-float").css('left', function(){ return $(this).offset().left; })
        .animate({"left":"-40%"}, {duration: "slow",queue: false, complete: function(){ 
        }});
        jQuery("#results").animate({width:'95%'}, {duration:'slow', queue: false});
        jQuery(".more-controls").animate({"right":"93.6%"}, {duration: "slow",queue: false, complete: function(){ 
            jQuery(".plus-controls").removeClass("minus"); 
        }});
        // jQuery("#selector-float").animate({ width: '0%' }, function () {
        //     jQuery("#results").css('width','100%');
        //     jQuery("#video_background").addClass('adjust-large-video');
        //     jQuery(".more-controls").attr('data-expand',"1");
        // });
        jQuery("#video-beca").addClass('cont-video-beca-open');
        jQuery("#video-beca").removeClass('cont-video-beca-close');
        jQuery("#calcu-left").addClass('cont-left-open');
        jQuery("#calcu-left").removeClass('cont-left-close');
        jQuery("#calcu-right").addClass('cont-right-open');
        jQuery("#calcu-right").removeClass('cont-right-close');
        jQuery("#reset").addClass('reset-2');
        jQuery("#reset").removeClass('reset');
    }else{
        /* Funciones cuando se abren los controles */
        jQuery(".overlay-controls").addClass("d-none");
        jQuery("#video_background").removeClass('adjust-large-video');
        jQuery(".more-controls").attr('data-expand',"0");
        jQuery("#selector-float").css('left', function(){ return $(this).offset().left; })
        .animate({"left":"0%"}, {duration: "slow",queue: false, complete: function(){ 
        }});
        jQuery("#results").animate({width:'55%'}, {duration:'slow', queue: false});
        jQuery(".more-controls").animate({"right":"53.5%"}, {duration: "slow",queue: false, complete: function(){
            jQuery(".plus-controls").addClass("minus"); 
        }});
        jQuery("#video-beca").addClass('cont-video-beca-close');
        jQuery("#video-beca").removeClass('cont-video-beca-open');
        jQuery("#calcu-left").addClass('cont-left-close');
        jQuery("#calcu-left").removeClass('cont-left-open');
        jQuery("#calcu-right").addClass('cont-right-close');
        jQuery("#calcu-right").removeClass('cont-right-open');
        // jQuery("#reset").addClass('reset-2');
        // jQuery("#reset").removeClass('reset');
    }
}



function finaliza() {
    console.log("_______FUNCION FINALIZA_______");
    jQuery('.breadcrumbs').addClass('d-none');
    getDuracion();

    jQuery('.an-slideUp').each(function () { jQuery(this).removeClass('an-slideUp') }); // QUITA LA ANIMACION PARA QUE CUANDO HAGA RESIZE NO EJECUTE LA ANIMACION DE NUEVO

    jQuery('body').removeClass('inicio');// EJECUTO EL ULTIMO PASO, SE QUITA LA BANDERA PARA QUE NO HAGA ANIMACIONES
    jQuery('.cont-quest').addClass('containerSecond');
    jQuery('.cont-quest').removeClass('cont-quest');
    jQuery('.imagen-lineas-negocio').addClass('d-none');
    jQuery('.boton-lineas-negocio').addClass('d-none');
    jQuery("#selector-float").animate({ width: '45%' }, function () {
        //jQuery(".output").css(");
        pasoFinalCalculadora = true;
        jQuery('.titlecont').addClass('d-none');
        jQuery('.card-calculadora').css('height','2.5vh');
        jQuery('.titlecont').removeClass('d-flex');
        jQuery('.card-calculadora').addClass('w-20');
        jQuery('document').removeClass('an-slideUp');
        jQuery('.containerSecond').removeClass('justify-content-center');
        jQuery('.controls').addClass('d-none');
        jQuery('.hide').not("#padre_tutor").fadeIn();
        jQuery(".hide-on-res").hide();
        jQuery(".quest.p-activa").removeClass('p-activa');
        jQuery(".quest").show();
        jQuery('.quest').removeClass('w-80');
        jQuery('.quest').addClass('w-100');
        jQuery(".questB").addClass("d-flex");
        jQuery(".pasos").hide();
        jQuery("#coming-back").removeClass('d-none');
        jQuery("#btn-verCol").addClass('d-none');
        jQuery("#btn-verCol-avanzar").addClass('d-none');
///OCULTAR CONTROLES Y ELEMENTOS QUE NA EN EDUCACION CONTINUA
console.warn(getUsuario().modalidad.catcosto)
if(getUsuario().modalidad.catcosto == "DIPEC"){
    jQuery(".r-beca").hide()
    jQuery(".r-ahorro").hide()
    jQuery(".term-beca").hide()
    jQuery("#promedio").hide()
    jQuery(".rs-btn-mensual").text('1 Módulo')
}        

        jQuery(".more").click(function () {
            $(".switch-arrow").toggleClass("down-window top-window");

        });
        jQuery('.btn-parpadeante').click(function (e) {
            $(this).attr('href', $(this).attr('href') == '#resumen' ? '#first' : '#resumen');
        });
        
        var checkRangeSend = $("#range").attr("data-send");
        if (checkRangeSend == "1") {
            expandControls();
            jQuery(".more-controls").show();
        }
    });
    //Envio a Proc web
    var checkRangeSend = $("#range").attr("data-send");
    if (checkRangeSend == "1") {
        // envioProcWeb();
    } else if (checkRangeSend == "2") {
        // envioProcWeb();
    }

    // if (getUsuario().modalidad.catcosto == 'DENT') { // Cdentista debe empezar con 5 materias 
    //     // setTimeout(function () { jQuery('.mat-ctrl').click(); }, 100); //DEhabilita boton de 4 años
    //     jQuery('.btn-circ').eq(4).click();
    // }



    //jQuery('.btn-dual').last().click();

    // var eligeCarrera = getCampo('contado');
    if (jQuery('body').hasClass('inicio') == false) {
        jQuery('.rs-btn-mensual').click();
        if(getUsuario().modalidad.catcosto != "DENT"){
            jQuery('.btn-dual').last().click();
        }else{
            jQuery('.btn-circ').eq(4).click();
        }
    }

}

function readCookie(name) {
    var nameEQ = name + "=";
    var ca = document.cookie.split(';');
    for (var i = 0; i < ca.length; i++) {
        var c = ca[i];
        while (c.charAt(0) == ' ') c = c.substring(1, c.length);
        if (c.indexOf(nameEQ) == 0) return c.substring(nameEQ.length, c.length);
    }
    return null;
}

function obtenerTextoCampus(campus) {
    switch (campus) {
        case 'ATZ': return "Atizapán"; break;
        case 'MAR': return "Marina"; break;
        case 'ECA': return "Ecatepec"; break;
        case 'GDL': return "Guadalajara"; break;
        case 'LEO': return "León"; break;
        case 'SUR': return "Sur"; break;
        case 'CUI': return "Cuitláhuac"; break;
        case 'TOL': return "Toluca"; break;
        case 'QRO': return "Querétaro"; break;
        case 'REY': return "Los Reyes"; break;
        case 'ONL': return "En Línea"; break;

    }
}

function campusDynamics(campus) {
    switch (campus) {
        case "ATZ":
            return "ATIZAPAN";
            break;
        case "REY":
            return "LOS REYES";
            break;
        case "MAR":
            return "MARINA";
            break;
        case "ECA":
            return "ECATEPEC";
            break;
        case "SUR":
            return "SUR";
            break;
        case "CUI":
            return "CUITLAHUAC";
            break;
        case "LEO":
            return "LEON";
            break;
        case "REY":
            return "Los Reyes";
            break;
        case "TOL":
            return "TOLUCA";
            break;
        case "GDL":
            return "GUADALAJARA";
            break;
        case "ONL":
            return "EN LINEA";
            break
        case "QRO":
            return "QUERETARO";
            break
        default:
            return "MARINA";
    }
}

function envioProcWeb() {
    // sendDatalayer('calculadora/sec-Resultados', 'StepCalculator');
    var seleccionJson = JSON.parse(localStorage.getItem("jsonUsr"));
    if(seleccionJson.form != undefined){
        if(seleccionJson.form.frm_amaterno == "telemarketer"){
            seleccionJson.form["estado"]="AGUASCALIENTES";
            seleccionJson.form["banner"]=seleccionJson.form.frm_banner;
            seleccionJson.form["nombre"]=seleccionJson.form.frm_nombre;
            seleccionJson.form["apaterno"]=seleccionJson.form.frm_apaterno;
            seleccionJson.form["amaterno"]=seleccionJson.form.frm_amaterno;
            seleccionJson.form["email"]=seleccionJson.form.frm_correo;
            seleccionJson.form["celular"]=seleccionJson.form.frm_celular;
            seleccionJson.form["tipoRegistro"]=seleccionJson.form.revalidacion;
            var seleccionCookie = seleccionJson.form;
        }
    }else{
        var seleccionCookie = JSON.parse(decodeURIComponent(readCookie('c_form_data')));
    }
    
    var bannerCalculadora = jQuery("#frm_banner").val();

    var urlCalc = (checkCookie('envioCRM') == false) ? "//www.unitec.mx/wp-content/themes/temaunitec/calculadora/v2_calc/almacen1.php" : "//www.unitec.mx/wp-content/themes/temaunitec/calculadora/v2_calc/otra_consulta.php";

    var seleccionNivelOferta = "";
    var seleccionHerramienta = "";
    // var seleccionCampus = obtenerTextoCampus(seleccionJson.campus);
    var seleccionCampus = campusDynamics(seleccionJson.campus);

    if (seleccionJson.linea.id_name == "PREPARATORIA") {
        seleccionNivelOferta = "P";
        seleccionHerramienta = "ASPIRANTES PREPA";
    } else {
        seleccionNivelOferta = "L";
        seleccionHerramienta = "ASPIRANTES LIC";
        if (seleccionJson.modalidad.interes == 4 || seleccionJson.modalidad.interes == 6) {
            seleccionHerramienta = "ONLINEP";
        }
    }

    var objectoSeleccion = {
        nivelOferta: seleccionNivelOferta,
        Herramienta: seleccionHerramienta,
        Campana: 1,
        Origen: seleccionHerramienta,
        Banner: seleccionCookie.banner,
        Calidad: 3,
        L_Negocio: seleccionJson.linea.abrv,
        F_Nacim: "01/01/2000",
        Ciclo: "19-1",
        Alumno: 0,
        Estad: seleccionCookie.estado,
        C_Carrera: seleccionJson.modalidad.iddynamics,
        Carrera: seleccionJson.modalidad.nombre,
        banner_id: seleccionCookie.banner,
        CP: seleccionCookie.tipoRegistro,
        Pobla: seleccionJson.modalidad.interes,
        name_form: "",
        Sexo: "M",
        nom_carrera: "",
        nom_campus: seleccionCampus,
        promedioF: seleccionJson.promedio,
        Pdescuento: seleccionJson.beca,
        nivelF: "L",
        CostoUnico: 0,
        num_carrera: 0,
        costoMensual: 0,
        descuentoF: 0,
        planSelec: "4pagos",
        CostoSelec: seleccionJson.mensual,
        planRecom: "4pagos",
        CostoRecom: seleccionJson.mensual,
        envioB: 1,
        C_Carrera_WP: "",
        Direcc: "WEBCALL",
        Colon: "",
        Nombre2: "",
        email2: "",
        URLreferer: location.href,
        debugs: 1,
        gclid_field: "",
        userId_field: "",
        CID: jQuery('#CID').val(),
        email_unitec: "",
        otraConsulta: (checkCookie('envioCRM') == false) ? 0 : 1,
        SERVER_URL: "",
        frm_LadaLenght: "",
        frm_LadaAllow: "",
        alum_2_2: "",
        Nombre: seleccionCookie.nombre,
        Ap_Pat: seleccionCookie.apaterno,
        Ap_Mat: seleccionCookie.amaterno,
        email: seleccionCookie.email,
        informacion_2: 1,
        Estado: seleccionCookie.estado,
        masculino: "M",
        TipoTel: "cel_calc",
        cel_calc: "",
        Telef: seleccionCookie.celular,
        periodo_interes_sel: "19-1",
        politica: "on",
        promedio: seleccionJson.promedio,
        programa: seleccionJson.modalidad.iddynamics,
        materias: seleccionJson.materias,
        hubspotutk: "",
        padretutor: "No",
        termino: 3,
        pago_mensual_beca: seleccionJson.mensualAhorro,
        formulario_completado: "paso_2",
        form_origen: "Calculadora"
    };

    console.log(objectoSeleccion);
    // console.log("LO QUE ENVIO");
    // console.log(JSON.stringify(objectoSeleccion));
    $
        .ajax({
            type: "POST",
            url: urlCalc,
            // url: "/wp-content/themes/temaunitec/calculadora/v2_calc/almacen1.php",
            data: objectoSeleccion,
            dataType: "JSON",
            cache: false,
            success: function (res) {
                console.log("Datos Enviados");
                console.log(res);
                if (res.tipo == "nuevo") {
                    console.log('nuevo');
                    dataLayer.push({
                        'event': 'CALCULADORA_REGISTRO'
                    });
                    dataLayer.push({
                        'origenLead': 'frmCalcu', //dato dinámico
                        'isAlumn': objectoSeleccion.Alumno, //dato dinámico
                        'ingress': objectoSeleccion.CP, //dato dinámico
                        'state': objectoSeleccion.Estado, //dato dinámico
                        'levelStudies': objectoSeleccion.L_Negocio, //dato dinámico
                        'Period': objectoSeleccion.Ciclo, //dato dinámico
                        'carrera': objectoSeleccion.Carrera, //dato dinámico
                        'campus': objectoSeleccion.nom_campus, //dato dinámico
                        'date': new Date(), //dato dinámico
                        'event': 'leadGenerationCalcu' //dato estático
                    });
                }
                else {
                    dataLayer.push({
                        'event': 'CALCULADORA_DUPLICADO'
                    });

                    dataLayer.push({
                        'url_value': 'formularioCalculadora/Duplicado', //dato dinámico
                        'event': 'StepForm' //dato estático
                    });
                }
                if (checkCookie('envioCRM') == false) {
                    setCookie('envioCRM', 1, 1);
                }
            },
            error: function (xhr, textStatus, error) {
                console.log(xhr.statusText);
                console.log(textStatus);
                console.log(error);
            }
        });
}

function envioProcWebContinuacion() {
    var seleccionJson = JSON.parse(localStorage.getItem("jsonUsr"));
    var seleccionCookie = JSON.parse(decodeURIComponent(readCookie('c_form_data')));

    var seleccionNivelOferta = "";
    var seleccionHerramienta = "";
    var seleccionCampus = obtenerTextoCampus(seleccionJson.campus);

    if (seleccionJson.linea.id_name == "PREPARATORIA") {
        seleccionNivelOferta = "P";
        seleccionHerramienta = "ASPIRANTES PREPA";
    } else {
        seleccionNivelOferta = "L";
        seleccionHerramienta = "ASPIRANTES LIC";
        if (seleccionJson.modalidad.interes == 4 || seleccionJson.modalidad.interes == 6) {
            seleccionHerramienta = "ONLINEP";
        }
    }

    var objectoSeleccion = {
        nivelOferta: seleccionNivelOferta,
        Herramienta: seleccionHerramienta,
        Campana: 1,
        Origen: seleccionHerramienta,
        Banner: seleccionCookie.banner,
        Calidad: 3,
        L_Negocio: seleccionJson.linea.abrv,
        F_Nacim: "01/01/2000",
        Ciclo: "19-1",
        Alumno: 0,
        Estad: seleccionCookie.estado,
        C_Carrera: seleccionJson.modalidad.iddynamics,
        Carrera: seleccionJson.modalidad.nombre,
        banner_id: seleccionCookie.banner,
        CP: seleccionCookie.tipoRegistro,
        Pobla: seleccionJson.modalidad.interes,
        name_form: "",
        Sexo: "M",
        nom_carrera: "",
        nom_campus: seleccionCampus,
        promedioF: seleccionJson.promedio,
        Pdescuento: seleccionJson.beca,
        nivelF: "L",
        CostoUnico: 0,
        num_carrera: 0,
        costoMensual: 0,
        descuentoF: 0,
        planSelec: "4pagos",
        CostoSelec: seleccionJson.mensual,
        planRecom: "4pagos",
        CostoRecom: seleccionJson.mensual,
        envioB: 1,
        C_Carrera_WP: "",
        Direcc: "WEBCALL",
        Colon: "",
        Nombre2: "",
        email2: "",
        URLreferer: location.href,
        debugs: 1,
        gclid_field: "",
        userId_field: "",
        CID: jQuery('#CID').val(),
        email_unitec: "",
        otraConsulta: 1,
        SERVER_URL: "",
        frm_LadaLenght: "",
        frm_LadaAllow: "",
        alum_2_2: "",
        Nombre: seleccionCookie.nombre,
        Ap_Pat: seleccionCookie.apaterno,
        Ap_Mat: seleccionCookie.amaterno,
        email: seleccionCookie.email,
        informacion_2: 1,
        Estado: seleccionCookie.estado,
        masculino: "M",
        TipoTel: "",
        cel_calc: "",
        Telef: seleccionCookie.celular,
        periodo_interes_sel: "19-1",
        politica: "on",
        promedio: seleccionJson.promedio,
        programa: seleccionJson.modalidad.iddynamics,
        materias: seleccionJson.materias,
        hubspotutk: "",
        padretutor: "No",
        termino: 3,
        pago_mensual_beca: seleccionJson.mensualAhorro,
        formulario_completado: "paso_2",
        form_origen: "Calculadora"
    };

    console.log(objectoSeleccion);

    // if(jQuery('body').hasClass('inicio'))
    $
        .ajax({
            type: "POST",
            url: "//www.unitec.mx/wp-content/themes/temaunitec/calculadora/v2_calc/otra_consulta.php",
            // url: "/wp-content/themes/temaunitec/calculadora/v2_calc/otra_consulta.php",
            data: objectoSeleccion,
            // dataType: "JSON",
            success: function (res) {
                console.log("Datos Enviados");
                console.log(res);
            },
            error: function (xhr, textStatus, error) {
                console.log(xhr.statusText);
                console.log(textStatus);
                console.log(error);
            }
        });
}



/* FUNCIONES DE LINEA DE NEGOCIO
 * getLineasNegocio         -> Consulta JSON
 * render_lineas_negocio    -> Pinta todos los divs 
 * set_linea_negocio        -> Cuando el usuario selecciona una linea de negocio
*/
function getLineasNegocio() {
    var lineasNegocio =
        [{ "name": "Preparatoria", "id_name": "PREPARATORIA", "order": "", "abrv": "PREPA", "mostrar_linea": "true" },
        { "name": "Licenciatura", "id_name": "LICENCIATURA", "order": "", "abrv": "UG", "mostrar_linea": "true" },
        { "name": "Ingeniería", "id_name": "INGENIERIA", "order": "", "abrv": "ING", "mostrar_linea": "true" },
        { "name": "Lic. en Salud", "id_name": "SALUD", "order": "", "abrv": "CS", "mostrar_linea": "true" },
        { "name": "Diplomado", "id_name": "DIPLOMADO", "order": "", "abrv": "DIP", "mostrar_linea": "true" },
        { "name": "Posgrado", "id_name": "POSGRADO", "order": "1", "abrv": "POS", "mostrar_linea": "true" }];
    return lineasNegocio;
}
function render_lineas_negocio() {
    var lineas = getLineasNegocio();
    jQuery.each(lineas, function (index, value) {
        switch( value.mostrar_linea ){
            case true:
            case "true":
                console.log('My array has at position ' + index + ', this value: ' + value.name);
                var html = '<div class="card card-calculadora"><div data-name="' + value.name + '" data-id_name="' + value.id_name + '" data-abrv="' + value.abrv + '" class="card-body tc w-100"><h5 class="mt-0" style="letter-spacing: 0px !important;">' + value.name + '</h5><div class="imagen-lineas-negocio imagen-lineas-cuadros cuadro ' + value.abrv + '"></div><button class="btn btn-dtc boton-lineas-negocio waves-effect waves-light"  role="button">Selecciona</button></div></div>';
                jQuery('#render_lineas_negocio').append(html);
            break;

            default:
            break;

        }        
    });
}

function getCarreras(linea, campus) {
    var keySearch = "lineaweb";
    var arrayCarrerasPorLinea = [];
    var objJsonCarreras = JSON.parse(localStorage.getItem("jsonCarreras"));
    arrayCarrerasPorLinea = this.buscar(
        keySearch,
        linea,
        objJsonCarreras
    );

    if (linea == 'LICENCIATURA') {
        arrayCarrerasPorLineaSalud = this.buscar(keySearch, 'SALUD', objJsonCarreras);
        var arrayCarrerasPorLinea = arrayCarrerasPorLinea.concat(arrayCarrerasPorLineaSalud);
        console.log('AGREGANDO LAS DE SALUD');
    }

    /*Para identificar las carreras que se obtuvieron, sirve para la autoselección desde formulario */
    // localStorage.setItem("jsonCarrerasSeleccion", JSON.stringify(arrayCarrerasPorLinea));
    return arrayCarrerasPorLinea
}

function getCarrerasBien(categorias) {
    var objJsonCarreras = JSON.parse(localStorage.getItem("jsonLinksCategorias"));
    var categoriasno = new Array();
    Object.entries(categorias).forEach(function (key, value) {
        categoriasno.push(key[1]['Grupo_carreras']);
    });
    //console.log(categoriasno);
    var nombres = new Array();
    Object.entries(objJsonCarreras).forEach(function (key, value) {
        if (categoriasno.includes(key[1]['IdCategoria'])) {
            nombres.push(key[1]);
        }
    });
    return nombres;
}


/* FUNCIONES DE CARRERA
 * render_carreras         -> Pinta las carreras en el autocomplete 
*/
function render_carreras(carreras) {
    // console.log('las carreras');
    // console.log(carreras);
    var carrerasjson = [];
    jQuery.each(carreras, function (index, value) {
        //console.log(value);
        console.log('My array has at position ' + index + ', this value: ' + value.IdCategoria);
        item = {}
        item["value"] = value.IdCategoria;
        item["label"] = value.Categoria;
        //item["desc"] = value.Categoria;
        item["icon"] = value.IdDynamics;
        item["Categoria"] = value.Categoria;
        item["IdCategoria"] = value.IdCategoria;
        item["Link"] = value.Link;
        carrerasjson.push(item);
    });
    carrerasjson.sort(function (a, b) {
        if (a.Categoria.toLowerCase() < b.Categoria.toLowerCase()) return -1;
        if (a.Categoria.toLowerCase() > b.Categoria.toLowerCase()) return 1;
        return 0;
    });

    // console.log("IMPRIMO LAS CARRERAS ");
    // console.log(carrerasjson);
    //Manejo de las carreras cuando es prepa
    var linea_seleccionada = getUsuario().linea.id_name;

    /*Copies Personalizables By SRP 03-07-2019*/
    //jQuery("#btn-verCol-avanzar").addClass('d-none'); /*Para no mostrar el boton cuando se regrese el Usuario*/
    var getCopy = copyPersonalizable( linea_seleccionada );
    jQuery('#bread1').removeClass('breadActive');
    jQuery('#bread2').addClass('breadActive');
    jQuery('#subtituloInteres').html( getCopy );
    render_selects();
    jQuery('.select-items').empty();
    jQuery('#render_modalidad option, #render_campus option').empty();   
    jQuery('.select-selected').html('Selecciona');
    /*End Copies Personalizables By SRP 03-07-2019*/

    if (linea_seleccionada == "PREPARATORIA") {
        guardaUsuario('carrera', carrerasjson[0]);
        jQuery('.rs-carrera').text(getCampo('carrera').Link.replace(/-/g, ' ').replace(/\//g, ''));
        render_modalidades(getModalidades(carrerasjson[0].IdCategoria));
        muestraPaso('business-line', 'container_carrera_modalidad');
        jQuery("#carrera").addClass("hide");
        jQuery("#carrera").addClass("hide-prepa");
        jQuery("#padre_tutor").removeClass("hide");
        jQuery("#modalidad").removeClass("hide");
        regresaPaso('container_carrera_modalidad', 'business-line');
        $('.steps').text('4');
    } else {
        jQuery("#carrera").removeClass("hide");
        jQuery("#carrera").removeClass("hide-prepa");
        jQuery("#padre_tutor").addClass("hide");
    }

    var accentMap = {
        "á": "a",
        "é": "e",
        "í": "i",
        "ó": "o",
        "ú": "u"
    };
    var normalize = function (term) {
        var ret = "";
        for (var i = 0; i < term.length; i++) {
            ret += accentMap[term.charAt(i)] || term.charAt(i);
        }
        return ret;
    };

    console.log("IMPRIMO LAS CARRERAS ");
    console.log(carrerasjson);
    $("#project").autocomplete({
        minLength: 0,
        // source: carrerasjson,
        source: function (request, response) {
            /* Funcionalidad para hacer la búsqueda sin importar los acentos */
            var matcher = new RegExp($.ui.autocomplete.escapeRegex(request.term), "i");
            response($.grep(carrerasjson, function (value) {
                value = value.label || value.value || value;
                return matcher.test(value) || matcher.test(normalize(value));
            }));
        },
        messages: {
            noResults: '',
            results: function () { $('.ui-helper-hidden-accessible').empty() }
        },
        focus: function (event, ui) {
            $("#project").val(ui.item.label);
            return false;
        },
        select: function (event, ui) {
            console.log(ui);
            $(this).blur();
            $("#project").val(ui.item.label);
            $("#project-id").val(ui.item.value);
            //$("#project-description").html(ui.item.desc);
            $("#project-icon").attr("src", "images/" + ui.item.icon);
            //let oferta =[{ "Categoria": ui.item.Categoria, "IdCategoria": ui.item.IdCategoria, "Link": ui.item.Link }];
            guardaUsuario('carrera', ui.item);
            // jQuery('.rs-carrera').text(getCampo('carrera').Link);
            jQuery('.rs-carrera').text(getCampo('carrera').Link.replace(/-/g, ' ').replace(/\//g, ''));
            render_modalidades(getModalidades(ui.item.IdCategoria));

            //render_campus(getModalidades(ui.item.IdCategoria));
            // console.log(getCarreras(getCampo("carrera").IdCategoria));

            // var campus=alert()("modalidad").campus.toString().split(",").sort();
            // console.log(campus);

            // muestraPaso('carrera', 'modalidad');
            // regresaPaso('modalidad', 'carrera');
            // $('.steps').text('5');
            $('#modalidad').removeClass('hide');
            return false;
        }
    })
        .autocomplete("instance")._renderItem = function (ul, item) {
            return $("<li>")
                .append("<div>" + item.label + "</div>")
                //.append("<div>" + item.label + "<br>" + item.desc + "</div>")
                .appendTo(ul);
        };

        // if (linea_seleccionada == "PREPARATORIA"){
        //     $('.ui-menu-item div').first().click();
        // }

}

function avanzaPaso() {
    var faltantes = 0;
    /* Controla todos los inputs que faltan */
    if (jQuery('#project').val() == "") {
        faltantes++;
    }

    if (jQuery('#render_campus').val() == 0) {
        faltantes++;
    }

    if (jQuery('#render_modalidad').val() == 0) {
        faltantes++;
    }

    var linea_seleccionada = getUsuario().linea.id_name;
    if (linea_seleccionada == "PREPARATORIA") {
        if (jQuery('#render_padre_tutor').val() == "Selecciona") {
            faltantes++;
        }
    }

    if(faltantes > 0){
        console.log("NO PUEDES AVANZAR");
    }else {
        muestraPaso('container_carrera_modalidad','promedio'); 
        $('#container_carrera_modalidad').addClass('hide');
        // muestraPaso('carrera', 'modalidad');
        regresaPaso('promedio', 'container_carrera_modalidad');
        $('.steps').text('5');
        off();
    }
    
}

/* FUNCIONES DE MODALIDADES
* getModalidades         -> OBTIENE MODALIDADES Y RECIBE EL ID DE CATEGORIA
* render_MODALIDADES  -> Obtienes las modalidades y las pinta 
*/

function getModalidades(categoria) {
    var keySearch = "Grupo_carreras";
    var arrayCarrerasPorLinea = [];
    var objJsonCarreras = JSON.parse(localStorage.getItem("jsonCarreras"));
    arrayCarrerasPorLinea = buscar(
        keySearch,
        categoria,
        objJsonCarreras
    );

    return arrayCarrerasPorLinea
}

function render_modalidades(modalidades) {
    console.log('render_modalidades');
    console.log(modalidades);
    console.log('Existen:' + modalidades.length);
    changeAddInCareers(modalidades[0]);

    var modalidadesSize = modalidades.length;
    /*******MODALIDADES******/
    if (modalidades.length == 1) {
        var html = '<option selected data-Grupo_carreras="' + modalidades[0].Grupo_carreras + '" data-IdDynamics="' + modalidades[0].IdDynamics + '" data-campus="' + modalidades[0].campus + '"  data-catCosto="' + modalidades[0].catCosto + '" data-interes="' + modalidades[0].interes + '" data-lineaweb="' + modalidades[0].lineaweb + '" data-modalidad="' + modalidades[0].modalidad + '"  data-nombre="' + modalidades[0].nombre + '" value="' + modalidades[0].modalidad + '">' + modalidadtxt(modalidades[0].modalidad) + '</option>';
    } else {
        console.log('mas de 3');
        var html = '<option value="0">Modalidad</option>';
        jQuery.each(modalidades, function (index, value) {
            console.log('My array has at position ' + index + ', this value: ' + value.modalidad + modalidadtxt(value.modalidad));
            html += '<option data-Grupo_carreras="' + value.Grupo_carreras + '" data-IdDynamics="' + value.IdDynamics + '" data-campus="' + value.campus + '"  data-catCosto="' + value.catCosto + '" data-interes="' + value.interes + '" data-lineaweb="' + value.lineaweb + '" data-modalidad="' + value.modalidad + '"  data-nombre="' + value.nombre + '" value="' + value.modalidad + '">' + modalidadtxt(value.modalidad) + '</option>';
        });
    }
    jQuery('#render_modalidad').html(html).delay(200).queue(function (next) {
        if (modalidadesSize == 1) {
            jQuery('#render_modalidad').next().addClass('inactive');
        } else if (modalidadesSize > 1) {
            jQuery('#render_modalidad').next().removeClass('inactive');
        }
        next();
    });

    /*******CAMPUS******/
    console.log('todos los campus que tienen esta carrera');
    var campus = [];
    jQuery.each(modalidades, function (index, value) { campus.push(value.campus); }); //Guarda los arreglos de campus
    var resultArray = Array.prototype.concat.apply([], campus);
    allcampus = resultArray.join(',');
    var b = unique(allcampus.split(",")); // Array de campus unicos 
    console.log(b);
    b.sort();
    var html = '<option value="0"> Campus</option>';
    if (b.length == 1) {
        html += '<option selected value="' + b[0] + '">' + campustxt(b[0]) + '</option>';
    } else {
        jQuery.each(b, function (index, value) {
            html += '<option value="' + value + '">' + campustxt(value) + '</option>';
        });
    }

    jQuery('#render_campus').html(html).delay(300).queue(function (next) {
        /* If cuando solo hay un campus para que se autoseleccione */
        if (b.length == 1) {
            jQuery('#render_campus').next().addClass('inactive');
            /* If cuando se autoselecciona la modalidad */
            if (jQuery('#render_modalidad').next().hasClass('inactive')) {
                regresoAnterior = false;
                /* Funcion cuando se quiere avanzar automáticamente a promedio */
                // setTimeout(function () { muestraPaso('modalidad', 'promedio'); regresoAnterior = false; }, 500);
                /* Funcion cuando se avanza hasta dar click en el boton */
                setTimeout(function () { regresoAnterior = false; }, 500);
                // muestraPaso('modalidad', 'promedio');
                guardaUsuario('campus', jQuery('#render_campus').val());
                guardaUsuario('modalidad', jQuery('#render_modalidad option:selected').data());
                var nivelAnterior = getNivelAnterior(getCampo('modalidad').interes);
                jQuery('.rs-canterior').text(nivelAnterior);
                off();
                var checkOverlaySend = $("#overlay").attr("data-send");
                if (checkOverlaySend == "1") {
                    // calcular_uno();
                    if (!jQuery('inicio').hasClass('inicio')) {
                        $("#overlay").attr("data-send", "2");
                        // $('#render_campus').next().next().children().first().click();
                        // envioProcWeb();
                    }
                } else if (checkOverlaySend == "2") {
                    // calcular_uno();
                    if (!jQuery('inicio').hasClass('inicio')) {
                        $("#overlay").attr("data-send", "2");
                        // $('#render_campus').next().next().children().first().click();
                        // envioProcWeb();
                    }
                } else {
                    if (!jQuery('inicio').hasClass('inicio')) {
                        $("#overlay").attr("data-send", "1");
                    }
                }
                calcular_uno();
            }
            // $('#render_campus').next().next().children().first().click();
        } else {
            /* En caso de que haya más de un campus se le remueve todo lo preseleccionado */
            jQuery('#render_campus').removeAttr('data-preselected');
            jQuery('#render_campus').next().removeClass('inactive');
        }
        next();
    });
    render_selects();
}

function getMinimoMatBeca(catCosto) {
    var beca;
    switch (catCosto) {

        case "NUTRI":
        case "BLUEM":
        case "GASTRO":
        case "FISIO":
        case "TURI":
        case "DENT":
        case "ENFER":
        case "PREPA":
        case "ING":
        case "ESP":
        case "ESPD":
        case "LIC":
        case "LICPSICOLOG":
        case "LICPEDAG":
            minmaterias = 5;
            break


        case "LICEJEC":
        case "INGEJEC":
        case "LICON":
        case "INGON":
            minmaterias = 4;
            break;

        case "DIP":
        case "DIPON":
        case "POS":
        case "POSON":
        case "POSONCUAT":
        case "POSCUAT":
        case "POSCUATS":
            minmaterias = 3
            break;
        default:
            minmaterias = 5;
    }
    return minmaterias;
}


/* FUNCIONES GENERALES
 * guardaUsuario   -> Guarda en localstorage todo el objeto en formato JSON
*/

function modalidadtxt(num) {
    switch (parseInt(num)) {
        case 1: return "Presencial"; break;
        case 2: return "Ejecutiva"; break;
        case 3: return "En línea"; break;
        case 4: return "Prepa Vespertina"; break;
        case 5: return "Prepa UNAM"; break;
        case 6: return "Prepa UNITEC"; break;
    }
}

function campustxt(campus) {
    switch (campus) {
        case 'ATZ': return "Atizapán, Edo. Mex."; break;
        case 'MAR': return "Marina, CDMX"; break;
        case 'ECA': return "Ecatepec, Edo. Mex."; break;
        case 'GDL': return "Guadalajara"; break;
        case 'LEO': return "León, Gto"; break;
        case 'SUR': return "Sur, CDMX"; break;
        case 'CUI': return "Cuitláhuac, CDMX"; break;
        case 'TOL': return "Toluca, Edo. Mex"; break;
        case 'QRO': return "Querétaro, QRO"; break;
        case 'REY': return "Los Reyes, CDMX"; break;
        case 'ONL': return "En Línea"; break;

    }
}

function unique(array) {
    return array.filter(function (el, index, arr) {
        return index == arr.indexOf(el);
    });
}


function render_selects() {
    jQuery('.select-selected,.select-items').remove();
    var x, i, j, selElmnt, a, b, c;
    /*look for any elements with the class "custom-select":*/
    x = document.getElementsByClassName("custom-select");
    for (i = 0; i < x.length; i++) {
        selElmnt = x[i].getElementsByTagName("select")[0];
        /*for each element, create a new DIV that will act as the selected item:*/
        a = document.createElement("DIV");
        a.setAttribute("class", "select-selected w-100");
        console.log("pasando el primero");
        a.innerHTML = selElmnt.options[selElmnt.selectedIndex].innerHTML;
        x[i].appendChild(a);
        /*for each element, create a new DIV that will contain the option list:*/
        b = document.createElement("DIV");
        b.setAttribute("class", "select-items select-hide");
        for (j = 1; j < selElmnt.length; j++) {
            /*for each option in the original select element,
            create a new DIV that will act as an option item:*/
            c = document.createElement("DIV");
            console.log("pasando el segundo");
            c.innerHTML = selElmnt.options[j].innerHTML;
            c.addEventListener("click", function (e) {
                /*when an item is clicked, update the original select box,
                and the selected item:*/
                var y, i, k, s, h;
                s = this.parentNode.parentNode.getElementsByTagName("select")[0];
                h = this.parentNode.previousSibling;
                for (i = 0; i < s.length; i++) {
                    if (s.options[i].innerHTML == this.innerHTML) {
                        s.selectedIndex = i;
                        h.innerHTML = this.innerHTML;
                        y = this.parentNode.getElementsByClassName("same-as-selected");
                        for (k = 0; k < y.length; k++) {
                            y[k].removeAttribute("class");
                        }
                        this.setAttribute("class", "same-as-selected");
                        break;
                    }
                }
                h.click();
            });
            b.appendChild(c);
        }
        x[i].appendChild(b);



        b.addEventListener("click", function (e) {
            var idSelect = jQuery(this).parent().find('select').prop('id');
            updateSelect(idSelect);

        });

        a.addEventListener("click", function (e) {
            /*when the select box is clicked, close any other select boxes,
            and open/close the current select box:*/
            e.stopPropagation();
            closeAllSelect(this);
            this.nextSibling.classList.toggle("select-hide");
            this.classList.toggle("select-arrow-active");
            console.log('elegiste:')
            console.log(e);



        });
    }
    checkAllInputs();
}
function updateSelect(id) {
    setTimeout(function () { jQuery("#" + id).change(); render_selects(); }, 200);

}

function closeAllSelect(elmnt) {
    /*a function that will close all select boxes in the document,
    except the current select box:*/
    var x, y, i, arrNo = [];
    x = document.getElementsByClassName("select-items");
    y = document.getElementsByClassName("select-selected");
    for (i = 0; i < y.length; i++) {
        if (elmnt == y[i]) {
            arrNo.push(i)
        } else {
            y[i].classList.remove("select-arrow-active");
        }
    }
    for (i = 0; i < x.length; i++) {
        if (arrNo.indexOf(i)) {
            x[i].classList.add("select-hide");
        }
    }
}
function abreCalculadora() {
    var prueba_terminado = $("#modal_frm_app_calculadora").attr('data-terminado');
    var prueba_externo = $("#modal_frm_app_calculadora").attr('data-externo');
    if (prueba_terminado == "1") {
        xCalculadora = window.scrollX;
        yCalculadora = window.scrollY;
        console.log("Guardando y: " + yCalculadora);
        //Se cierra todo para que se muestre la calculadora
        console.log("Cerrando todo");
        window.scrollTo(0, 0);
        $('#bootstrapcss-css').attr('disabled', 'disabled');
        $('#mdb-css').attr('disabled', 'disabled');
        $('#bootstrapcss-css').prop('disabled', true);
        $('#mdb-css').prop('disabled', true);
        $('#calculadora-css').removeAttr("disabled");
        $('#calculadora-css').prop("disabled", false);
        $(".todoElBody").hide();
        $('.main-calculadora-todo').show();
        $('#aplicativo-calculadora').show();

        console.log("AQUI ESTÁ LA COOKIE");
        var data_cookie_form = JSON.parse(decodeURIComponent(getCookie("c_form_data")));
        console.log(data_cookie_form);
        if(data_cookie_form.linea != undefined)
        {
            $("div").find("[data-id_name="+data_cookie_form.linea+"]").mouseup();
            var busqueda_grupo = buscar_grupo("IdDynamics",data_cookie_form.carreraInteres,JSON.parse(localStorage.jsonCarreras));
            console.log("LA BUSQUEDA DEL GRUPO");
            console.log(busqueda_grupo);
            var categoria_grupo = busqueda_grupo[0].Categoria;
            console.log("LA BUSQUEDA DEL DIV");
            $('#project').autocomplete('search', $('#project').val());
            $(".ui-menu-item-wrapper:contains('"+categoria_grupo+"')").click();
            console.log("BUSQUEDA DE MODALIDAD");
            var modalidad_size = $("#render_modalidad").next().next().children().length;
            if(modalidad_size > 0){
                var txt_modalidad = $("#render_modalidad option[value="+data_cookie_form.modalidad+"]").text();
                $("#render_modalidad").next().next().children(":contains("+txt_modalidad+")").click();
            }
            var campus_size = $("#render_campus").next().next().children().length;
            if(campus_size > 0){
                var txt_campus = $("#render_campus option[value="+data_cookie_form.campus+"]").text();
                $("#render_campus").next().next().children(":contains("+txt_campus+")").click();
            }
            $("#btn-verCol-avanzar").click();
        }
        

    } else if(prueba_externo == "1"){
        xCalculadora = window.scrollX;
        yCalculadora = window.scrollY;
        console.log("Guardando y: " + yCalculadora);
        //Se cierra todo para que se muestre la calculadora
        console.log("Cerrando todo");
        window.scrollTo(0, 0);
        $('#bootstrapcss-css').attr('disabled', 'disabled');
        $('#mdb-css').attr('disabled', 'disabled');
        $('#bootstrapcss-css').prop('disabled', true);
        $('#mdb-css').prop('disabled', true);
        $('#calculadora-css').removeAttr("disabled");
        $('#calculadora-css').prop("disabled", false);
        $(".todoElBody").hide();
        $('.main-calculadora-todo').show();
        $('#aplicativo-calculadora').show();
    } else {
        jQuery("#modal_frm_app_calculadora").modal();
    }
}
/*if the user clicks anywhere outside the select box,
then close all select boxes:*/
// document.addEventListener("click", closeAllSelect);

function clickSeeDownModalidad() {
    // console.log("DIO CLICK EN MODALIDAD");
    // $('#render_modalidad').next()[0].nextSibling.classList.toggle("select-hide");
    // $('#render_modalidad').next()[0].classList.toggle("select-arrow-active");
    $('#render_modalidad').next().click();
}

function clickSeeDownCampus() {
    // $('#render_campus').next()[0].nextSibling.classList.toggle("select-hide");
    // $('#render_campus').next()[0].classList.toggle("select-arrow-active");
    $('#render_campus').next().click();
}
/* SEGUNDA PARTE JS */
