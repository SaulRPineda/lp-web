
/* Funciones compartidas */

var urlJsonBasura = "/assets/frontend/json/min/json_basura.min.json";
var urlJsonBasuraEmail = "/assets/frontend/json/min/json_basuraEmail.min.json";
var urlJsonCarreras = "/assets/frontend/json/min/json_calc_carreras.min.json";
//Se agrega nueva url para obtener los links y nombre de las categorias de las carreras
var urlJsonLinksCategorias = "/assets/frontend/json/min/json_calc_linkPaginas.min.json";
//Se agregan variables para calculadora
//Costos
var urlJsonCalcCostos = "/assets/frontend/json/min/json_calc_costos.min.json";
//Becas
var urlJsonCalcBecas = "/assets/frontend/json/min/json_calc_becas.min.json";
//var urlwp = "https://www.unitec.mx/wp-content/themes/temaunitec";
var urlwp = "https://www.unitec.mx/wp-content/themes/temaunitec";
//var urlwp = "/unitec2017/wp-content/themes/temaunitec";

/* Pasos funnels */
// var paso1 = true;
// var paso2 = true;
var paso3 = false;
var paso4 = false;
var paso5 = false;
var paso6 = false;
var paso7 = false;
/* Pasos funnels */

function getJsonCarreras() {
    jQuery
        .ajax({
            type: "GET",
            //url: this.urlJsonCarreras,
            url: urlwp + urlJsonCarreras,
            "Content-Type": "application/json",
            dataType: "json",
            success: function (resultado) { }
        })
        .done(function (resultado) {
            localStorage.setItem("jsonCarreras", JSON.stringify(resultado));
        });
}
//Funcion para obtener el json de links y categorias
function getJsonLinksCategorias() {
    jQuery
        .ajax({
            type: "GET",
            //url: this.urlJsonLinksCategorias,
            url: urlwp + urlJsonLinksCategorias,
            "Content-Type": "application/json",
            dataType: "json",
            success: function (resultado) { }
        })
        .done(function (resultado) {
            localStorage.setItem("jsonLinksCategorias", JSON.stringify(resultado));
        });
}
//Funcion para traer el Json de basura con jquery ajax
function getJsonBasura() {
    jQuery
        .ajax({
            type: "GET",
            //url: this.urlJsonBasura,
            url: urlwp + urlJsonBasura,
            "Content-Type": "application/json",
            dataType: "json",
            success: function (resultado) { }
        })
        .done(function (resultado) {
            console.log(resultado);
            localStorage.setItem("jsonBasura", JSON.stringify(resultado));

        });
}
//Funcion para traer el Json de email basura con jquery ajax
function getJsonBasuraEmail() {
    jQuery
        .ajax({
            type: "GET",
            //url: this.urlJsonBasuraEmail,
            url: urlwp + urlJsonBasuraEmail,
            "Content-Type": "application/json",
            dataType: "json",
            success: function (resultado) { }
        })
        .done(function (resultado) {
            localStorage.setItem("jsonBasuraEmail", JSON.stringify(resultado));
        });
}
function getJsonCalcCostos() {
    jQuery
        .ajax({
            type: "GET",
            //url: this.urlJsonCarreras,
            url: urlwp + urlJsonCalcCostos,
            "Content-Type": "application/json",
            dataType: "json",
            success: function (resultado) { }
        })
        .done(function (resultado) {
            localStorage.setItem("jsonCostos", JSON.stringify(resultado));
        });
}
//Obtener las becas
function getJsonBecasCalc() {
    jQuery
        .ajax({
            type: "GET",
            //url: this.urlJsonCarreras,
            url: urlwp + urlJsonCalcBecas,
            "Content-Type": "application/json",
            dataType: "json",
            success: function (resultado) { }
        })
        .done(function (resultado) {
            localStorage.setItem("jsonBecas", JSON.stringify(resultado));
        });
}

Number.prototype.format = function (n, x) {
    var re = '\\d(?=(\\d{' + (x || 3) + '})+' + (n > 0 ? '\\.' : '$') + ')';
    return this.toFixed(Math.max(0, ~~n)).replace(new RegExp(re, 'g'), '$&,');
};

function getUsuario() {
    return JSON.parse(localStorage.getItem("jsonUsr"));
}

function getCampo(campo) {
    var usuario = getUsuario();
    return usuario[campo];
}
function getCosto() {
    return JSON.parse(localStorage.getItem("jsonCostos"));
}

function getCampoCosto(campo) {
    var costo = getCosto();
    //console.log("el campo"+campo);
    //console.log(costo[0]);
    return costo[0][campo];
}

function getBecaJSON() {
    return JSON.parse(localStorage.getItem("jsonBecas"));
}

function getCampoBeca(campo) {
    var beca = getBecaJSON();
    return beca[0][campo];
}

function buscar(key, value, jsonCarreras) {
    var resultadoBusqueda = [];
    for (var i in jsonCarreras) {
        if (getObjects(jsonCarreras[i], key, value).length != 0) {
            resultadoBusqueda.push(jsonCarreras[i])
        }
    }
    return resultadoBusqueda;
}

function buscar_grupo(key, value, jsonCarreras){
    console.log("BUSCANDO POR " + key + " POR EL VALOR " + value);
    var resultadoBusqueda = [];
    for (var i in jsonCarreras) {
        if (getObjects(jsonCarreras[i], key, value).length != 0) {
            resultadoBusqueda.push(jsonCarreras[i])
        }
    }

    var jsonLinks = JSON.parse(localStorage.jsonLinksCategorias);
    var resultadoBusquedaGrupo = [];

    for(var i in jsonLinks) {
        if (getObjects(jsonLinks[i], "IdCategoria", resultadoBusqueda[0].Grupo_carreras).length != 0) {
            resultadoBusquedaGrupo.push(jsonLinks[i])
        }
    }
    return resultadoBusquedaGrupo;
}

function getObjects(obj, key, val) {
    var objects = [];
    for (var i in obj) {
        if (!obj.hasOwnProperty(i)) continue;

        if (typeof obj[i] == 'object') {
            objects = objects.concat(this.getObjects(obj[i], key, val));
        } else
            //if key matches and value matches or if key matches and value is not passed (eliminating the case where key matches but passed value does not)
            if (i == key && obj[i] == val || i == key && val == '') { //
                objects.push(obj);
            } else if (obj[i] == val && key == '') {
                //only add if the object is not already in the array
                if (objects.indexOf(obj) == -1) {
                    objects.push(obj);
                }
            }
    }
    return objects;
}

function getObjectsRemoveSpecialCharacters(obj, key, val) {
    var objects = [];
    for (var i in obj) {
        if (!obj.hasOwnProperty(i)) continue;

        if (typeof obj[i] == 'object') {
            objects = objects.concat(this.getObjects(obj[i], key, val));
        } else
            //if key matches and value matches or if key matches and value is not passed (eliminating the case where key matches but passed value does not)
            if (i == key && removeAccents(obj[i]).toUpperCase() == removeAccents(val).toUpperCase() || i == key && val == '') { //
                objects.push(obj);
            } else if (obj[i] == val && key == '') {
                //only add if the object is not already in the array
                if (objects.indexOf(obj) == -1) {
                    objects.push(obj);
                }
            }
    }
    return objects;
}

function removeAccents(str) {
    var accents = 'ÀÁÂÃÄÅàáâãäåßÒÓÔÕÕÖØòóôõöøÈÉÊËèéêëðÇçÐÌÍÎÏìíîïÙÚÛÜùúûüÑñŠšŸÿýŽž';
    var accentsOut = "AAAAAAaaaaaaBOOOOOOOooooooEEEEeeeeeCcDIIIIiiiiUUUUuuuuNnSsYyyZz";
    str = str.split('');
    str.forEach(function(letter, index) {
      var i = accents.indexOf(letter);
      if (i != -1) {
        str[index] = accentsOut[i];
      }
    })
    return str.join('');
  }

function guardaUsuario(valor, data) {
    if (localStorage.getItem("jsonUsr") === null) {
        var jsonnew = {};
        jsonnew[valor] = data;
        localStorage.setItem("jsonUsr", JSON.stringify(jsonnew));
        console.log('no existe el Json de usuario');
    } else {
        var viejo = JSON.parse(localStorage.getItem("jsonUsr"));
        viejo[valor] = data;
        localStorage.setItem("jsonUsr", JSON.stringify(viejo));
    }

}

function getNivelAnterior(nivel) {
    var nivelAnt = "";
    switch (nivel) {
        case 1: case 2: case 3: case 4:
            nivelAnt = 'Preparatoria'
            break;
        case 5: case 6:
            nivelAnt = 'Licenciatura'
            break;
        case 7:
            nivelAnt = 'Secundaria'
            break;
        default:
            nivelAnt = 'Licenciatura'
            break;
    }
    return nivelAnt;
}

function getNivelAnteriorPorCard(nivel) {
    var nivelAnt = "";
    switch (nivel) {
        case "LICENCIATURA": case "INGENIERIA": case "SALUD":
            nivelAnt = 'Preparatoria'
            break;
        case "POSGRADO":
            nivelAnt = 'Licenciatura'
            break;
        case "PREPARATORIA":
            nivelAnt = 'Secundaria'
            break;
        case "DIPEC":
            nivelAnt = 'último grado de estudios'
            break;
        default:
            nivelAnt = 'Licenciatura'
            break;
    }
    return nivelAnt;
}

function IsMobile() {
    var Uagent = navigator.userAgent||navigator.vendor||window.opera;
    return(/(android|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|iris|kindle|lge |maemo|midp|mmp|mobile.+firefox|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows ce|xda|xiino|android|ipad|playbook|silk/i.test(Uagent)||/1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-/i.test(Uagent.substr(0,4))); 
};

function calcular_uno() {
    console.log("_______FUNCION CALCULAR UNO_____");
    /***Funciones por defecto en resutlado.component.html primera carga ***/
    var campus = getCampo("campus");
    console.log('El campus escogido');
    console.log(campus);
    var catcosto = getCampo("modalidad").catcosto;
    console.log(catcosto);
    //se tienen que habilitar en este orden las funciones
    var costos = getCampoCosto(catcosto); // 1.-
    guardaUsuario('costos', costos);
    console.log("costos:");
    console.log(costos);
    var allCostos = Object.keys(costos['duracion']).length;
    // if (getUsuario().modalidad.catcosto == 'DENT') { // Cdentista debe empezar con 5 materias 
    //     // setTimeout(function () { jQuery('.mat-ctrl').click(); }, 100); //DEhabilita boton de 4 años
    //     // jQuery('.btn-circ').eq(4).click();
    //     var materias = 5;
    // } else {
    //     var materias = parseInt(costos['duracion'][Object.keys(costos['duracion'])[0]]["materias"]);
    // }
    var materias = parseInt(costos['duracion'][Object.keys(costos['duracion'])[0]]["materias"]);
    guardaUsuario('materias', materias);
    var limitMaterias = parseInt(costos['duracion'][Object.keys(costos['duracion'])[0]]["materias"]);
    if (campus == 'QRO' && catcosto == 'LIC') {
        materias = parseInt(costos['duracion'][Object.keys(costos['duracion'])[0]]["materias"]) * 1 + 1;
        limitMaterias = parseInt(costos['duracion'][Object.keys(costos['duracion'])[0]]["materias"]) * 1 + 1;

    }
    console.log(limitMaterias);
    console.log('Colocando materias:' + materias);
    console.log('El limite de materias es:' + limitMaterias);
    //terminan funciones de primera carga 

    console.log("EL CATCOSTO ES: " + getUsuario().modalidad.catcosto);
    var btnmaterias = "";
    // if (getUsuario().modalidad.catcosto == 'DENT') {
    //     console.log("CAMBIANDO EL LIMITE");
    //     var changeLimit = 5;
    // }else{
    //     var changeLimit = limitMaterias;
    // }
    /*Funcionalidad para personalizar thank you page DIPEC*/
    switch( getUsuario().modalidad.catcosto ){
        case "DIPEC":
        case "PREPAUNAM":
            btnmaterias = '<div class=" alert alert-primary ' + sel + '"> ' + limitMaterias + ' módulos</div>';
            jQuery('.numero_materias').html("Numero de módulos a cursar:");
            
            jQuery('.disponible_materias').addClass('mr');
            //jQuery('.bloque_numero_materias').removeClass('w-50');
            jQuery('bloque_disponible').removeClass('subtitulo').addClass('subtituloIndividual');
            
        break;

        default:
            for (i = 1; i <= limitMaterias; i++) {
            if (i == limitMaterias) { var sel = "selected"; }
                btnmaterias += '<div class="btn-circ ' + sel + '">' + i + '</div>';
            }

            if (limitMaterias > 7) {
                btnmaterias = '<div class=" alert alert-primary ' + sel + '"> ' + i + ' materias</div>';
            }
            jQuery(".numero_materias").html('Numero de materias a cursar:');
            
            jQuery('.disponible_materias').removeClass('mr');
            //jQuery('.bloque_numero_materias').addClass('w-50');
            jQuery('bloque_disponible').removeClass('subtituloIndividual').addClass('subtitulo');
        break;
    }
    /*End Funcionalidad para personalizar thank you page DIPEC*/

    jQuery('.mat-circ').html(btnmaterias);

    // if(getCampo('materias')){

    // }else{
    //     guardaUsuario('materias',materias);
    // }
    getDuracion();
}






function getDuracion() {
    console.log('______FUNCION GETDURACION_____');
    var materias = getCampo('materias');
    var campus = getCampo('campus');
    var catcosto = getCampo('modalidad').catCosto;
    var terminando = void 0;
    var valoranios = void 0;
    var materiasanios = void 0;
    //let aniosRango:any;
    var aniosRango = new Array();
    var materiasMaximo = void 0;
    var lasMaterias = materias;
    var costos = getCampo('costos');
    console.log("costos['duracion']");
    console.log(costos['duracion']);


    Object.entries(costos['duracion']).forEach(function (key, value) {
        console.log("Con " + key[0] + " materias terminas en " + key[1]['materias'] + " años");
        console.log(key[0]);
        console.log(key[1]['materias']);

        valoranios = key[0];
        materiasanios = key[1]['materias'];
        console.log("lasmat: " + lasMaterias);
        if (materiasanios == lasMaterias) {
            console.log('Estaras terminando en ' + valoranios + " Mateiras anios" + materiasanios);
            terminando = valoranios;
        }

        aniosRango.push(valoranios);

    });


    aniosRango = aniosRango;
    console.log("temrinando" + terminando);
    console.log(aniosRango);
    var durahtml = "";
    var i = "";


    // jQuery.each(aniosRango, function (index, value) {
    //     if (durahtml == "") { var sel = "selected" }
    //     durahtml += '<button class="bl btn-dual ' + sel + '" data-termino="' + value + '">' + value + ' años</button>';
    //     i = value;
    // });

    // if (aniosRango.length < 2) {
    //     durahtml = '<div class=" alert alert-primary "> ' + i + ' Años</div>';
    // }


    //var uno="";
    var sel = "";
    console.log('implementaicon de botones');
    //impresion de botones 
    console.log(costos['duracion']);
    jQuery.each(costos['duracion'], function (index, value) {
        console.log("index: " + index + " Value:" + value.materias);
        if (durahtml == "") { sel = "selected"; } else { sel = ""; }
        //if (Object.keys(costos['duracion']).length == index) {sel = "selected";} else { sel = ""; }
        if (index == '4m') {
            durahtml += '<div class=" alert alert-primary "> 4 Meses</div>';
        }

        else if(index == '9m') {
            durahtml += '<div class=" alert alert-primary "> 9 Meses</div>';
        }

        else {
            durahtml += '<button class="bl ml2 btn-dual ' + sel + '"  data-termino="' + index + '" data-materias="' + value.materias + '">' + index + ' años</button>';

        }
        // if(i==1){
        //     durahtml = '<div class=" alert alert-primary "> solo un valor Años</div>';
        //     alert('una');
        // }
        //uno = index; 
        i++;
    });





    //guardaUsuario('materias',jQuery('.btn-dual.selected'));
    jQuery('.aniosduracion').html(durahtml);









    duracionAnios = terminando;
    console.log("duracionAnios:");
    console.log(duracionAnios);
    if (!duracionAnios) {
        console.log('No hay duracion para las materias escogidas');
    }
    calcular();

}

function calcular() {
    console.warn("___fn calcular___")
    if (!jQuery('body').hasClass('inicio')) {
        //LLEGO AL FINAL
        console.warn("valida en calcular")
        console.warn(getUsuario().modalidad.catcosto)
        /*Se ocultan secciones debido a DIPEC y PREPAUNAM By SRP*/
        if(getUsuario().modalidad.catcosto != "DIPEC" && getUsuario().modalidad.catcosto != "PREPAUNAM"){
            //alert("AQUI NO DEBO ENTRAR");

            jQuery("#alertext-prepaunam").addClass("d-none");
            /*Remove beneficios para Prepa UNAM By SRP 01-07-2019*/
            $("#listaBen-0").remove();
            $("#listaBen-1").remove();

            jQuery(".r-beca").show()
            jQuery(".r-ahorro").show()
            jQuery(".term-beca").show()
            jQuery("#promedio").show()
            jQuery(".rs-btn-contado").show()
            jQuery(".rs-btn-mensual").text('Mensual')
            jQuery("#r-ahorro").removeClass("d-none"); 
            jQuery("#prepa-unam").addClass("d-none");
            jQuery(".infoMaterias").removeClass("prepaUnam");
            jQuery("#bloqueMaterias").removeClass("d-none").show();
            jQuery(".prepavesp-term").addClass("d-none");
            jQuery("#pagoNormal").removeClass("d-none");
            jQuery("#tipoPago").removeClass("d-none");
            jQuery("#pagosmensuales-prepa-unam").addClass("d-none");
            

            if(jQuery('.btn-dual-y.selected').text().toLowerCase() == "mensual"){
                jQuery('.rs-tipopago').text('Mensual');
            }else{
                jQuery('.rs-tipopago').text('Contado');
            }            
        }
        else{
            
            if ( getUsuario().modalidad.catcosto == "PREPAUNAM" ) {
                //alert("ENTRO EN CASO ESPECIAL PREPA");
                //alert("Desactivo");
                jQuery("#alertext-prepaunam").removeClass('d-none');

                /*Remove beneficios para Prepa UNAM By SRP 01-07-2019*/
                $("#listaBen-0").remove();
                $("#listaBen-1").remove();

                /*Se inyectan beneficios para Prepa UNAM By SRP 01-07-2019*/
                jQuery("#listaBeneficios").prepend("<li id='listaBen-0'>Plan de estudios avalado por la UNAM</li><li id='listaBen-1'>Acceso a los eventos culturales y académicos organizados por la UNAM</li>");

                //jQuery(".r-beca").hide()
                //jQuery("#promedio").hide()
                jQuery(".bloque_numero_materias").hide()
                jQuery(".rs-btn-contado").hide()
                jQuery(".term-beca").hide()
                //jQuery("#r-ahorro").addClass("d-none"); 
                jQuery("#prepa-unam").removeClass("d-none");
                jQuery("#bloqueMaterias").addClass("d-none");
                jQuery(".infoMaterias").addClass("prepaUnam");
                jQuery(".containerSecond").addClass("prepaUnamContainer");
                jQuery(".prepavesp-term").addClass("d-none");
                jQuery("#pagoNormal").addClass("d-none");
                jQuery("#tipoPago").addClass("d-none");
                jQuery("#pagosmensuales-prepa-unam").removeClass("d-none");
                jQuery(".rs-btn-contado").removeClass("selected");
                jQuery(".rs-btn-mensual").addClass("selected");
            }

            else{
                //alert("ENTRO EN CASO ESPECIAL PREPA");
                jQuery("#pagoNormal").removeClass("d-none");
                jQuery("#tipoPago").removeClass("d-none");
                jQuery(".r-beca").hide()
                jQuery(".r-ahorro").hide()
                jQuery(".term-beca").hide()
                jQuery("#promedio").hide()
                console.warn("1 modulo")
                jQuery(".rs-btn-mensual").text('1 Módulo')
                if(jQuery('.btn-dual-y.selected').text().toLowerCase() == "1 módulo"){
                    jQuery('.rs-tipopago').text('1 Módulo');
                }else{
                    jQuery('.rs-tipopago').text('Contado');
                }  

            }           
        }

        if(getUsuario().modalidad.catcosto == "PREPAVESP"){
            //jQuery(".prepavesp-term").show()
            jQuery(".prepavesp-term").removeClass("d-none").show();
        }
    }

    //no debe calcular si faltan campos
    
    if (jQuery('#project').val() == "" || jQuery('#render_campus').val() == 0 || jQuery('#render_modalidad').val() == 0) {
        
        /*Revisar en el envio la funcion getNivelInteres cambiarlo como esta linea By SRP*/
        if (IsMobile()) {
            var linea_seleccionada = getUsuario().modalidad.lineaweb;
        }else{
            var linea_seleccionada = getUsuario().linea.id_name;
        }
        

        if (linea_seleccionada == "PREPARATORIA") {
            if (jQuery('#render_padre_tutor').val() == "Selecciona") {
                console.warn("fn calcular DUMPED")
                return;
            }
        }else{
            console.warn("fn calcular SKIPED")
            return;
        }
    }
    var campus = getCampo('campus');
    var costos = getCampo('costos');
    var catcosto = getCampo('modalidad').catcosto;
    var materias = getCampo('materias');
    var promedio = Math.round(jQuery('#range').val() * 10) / 10;

    if (campus == undefined || campus == null || campus == "" || campus == "0") {
        campus = jQuery('#render_campus').attr('data-preselected');
    }

    //Obtiene el costo por materia 
    // Costo de la materia X Costo de materia en campus
    console.log("el campus" + campus);
    console.log(costos);
    console.log('Duraciones disponibles');

    // console.log(this.materias);

    // if (this.materias != materiasanios){

    // }else{
    //   this.duracionAnios = valoranios;
    //   this.materias = materiasanios;
    // }

    var costoXmateria = costos["costoMateria"]["1pago"][campus]; //COSTO POR MATERIA DEL CAMPUS SELECCIONADO
    var costoLista = materias * costoXmateria;

    //Detecta a partir de cuanto se crea la beca 
    var minimoMateriasParaBeca = getMinimoMatBeca(catcosto);
    console.log('El minimo para beca es:' + minimoMateriasParaBeca + " y tengo:" + materias);

    if (materias >= minimoMateriasParaBeca) {
        console.log('si aplica beca');
        beca = getBeca(promedio, catcosto);

    } else {
        beca = 0;
        console.log('no aplica beca');

    }
    console.log('tomando el costo por materia');
    var costoXmateriaMens = costos['costoMateria']['4pagos'][campus]; //TODO: revisar si hay nodo de campus, sino usar todos
    /* PAGOS DE CONTADO*/
    var costoXmateriaContado = costos['costoMateria']['1pago'][campus]; //TODO: revisar si hay nodo de campus, sino usar todos

    switch( getUsuario().modalidad.catcosto ){
        case "DIPEC":
            /*PAGOS MENSUALES 4 APGOS */
            var PagoSinBecaMensual = costoXmateriaMens;    
            /* PAGOS DE CONTADO*/ 
            var PagoSinBecaContado = costoXmateriaContado;            
        break;

        default:           
            /*PAGOS MENSUALES 4 APGOS */
            var PagoSinBecaMensual = costoXmateriaMens * materias;            
            /* PAGOS DE CONTADO*/
            var PagoSinBecaContado = costoXmateriaContado * materias;
            
        break;
    }

    // if(jQuery('.btn-dual-y.selected').text().toLowerCase()=='mensual'){
    //         jQuery('.rs-pg-final').text("$"+getCampo('mensual').format());
    //         jQuery('.rs-ahorro-mns').text("$"+getCampo('mensualAhorro').format());
    //     }else{
    //         jQuery('.rs-pg-final').text("$"+getCampo('contado').format());
    //         jQuery('.rs-ahorro-mns').text("$"+getCampo('contadoAhorro').format());

    //     }


    /*PAGOS MENSUALES 4 APGOS */
    console.log('Pago sin descuento' + PagoSinBecaMensual);
    var PagoConBeca = Math.round(PagoSinBecaMensual - ((beca * PagoSinBecaMensual) / 100));
    var pagoConSR = PagoSinBecaMensual - ((beca * PagoSinBecaMensual) / 100);
    jQuery('.rs-beca').text(beca + "%*");
    console.log('con el ' + beca + " de " + PagoSinBecaMensual + " Queda: " + pagoConSR);
    var AhorroMensual = Math.round(PagoSinBecaMensual - pagoConSR);
    console.log('Te ahorras:' + AhorroMensual);

    /* PAGOS DE CONTADO*/
    console.log('Pago sin descuento' + PagoSinBecaContado);
    var PagoConBecaContado = Math.round(PagoSinBecaContado - ((beca * PagoSinBecaContado) / 100));
    var PagoConBecaContadoSR = PagoSinBecaContado - ((beca * PagoSinBecaContado) / 100);
    console.log('con el ' + beca + " de " + PagoSinBecaContado + " Queda: " + PagoConBecaContadoSR);
    var AhorroContado = Math.round(PagoSinBecaContado - PagoConBecaContadoSR);
    console.log('Te ahorras:' + AhorroContado);
    guardaUsuario('costos', costos);
    guardaUsuario('materias', materias);
    guardaUsuario('mensual', pagoConSR);
    guardaUsuario('mensualAhorro', AhorroMensual);
    guardaUsuario('contado', PagoConBecaContadoSR);
    guardaUsuario('contadoAhorro', AhorroContado)
    console.log(jQuery('.btn-dual-y.selected').text());
    if (jQuery('.btn-dual-y.selected').text().toLowerCase() == "mensual" || jQuery('.btn-dual-y.selected').text().toLowerCase() == "1 módulo") {
        console.log("COLOCANDO MENSUAL");
        jQuery('.rs-pg-final').text("$" + (Math.round(pagoConSR * 100) / 100).format());
        jQuery('.rs-ahorro-mns').text("$" + (Math.round(AhorroMensual * 100) / 100).format());
    } else {
        jQuery('.rs-pg-final').text("$" + (Math.round(PagoConBecaContadoSR * 100) / 100).format());
        jQuery('.rs-ahorro-mns').text("$" + (Math.round(AhorroContado * 100) / 100).format());
    }
console.warn(PagoConBecaContadoSR)
    var nivelAnterior = getNivelAnterior(getCampo('modalidad').interes.toString());
    // jQuery('.rs-btn-mensual').click();

    // var eligeCarrera = getCampo('contado');
    // if (jQuery('body').hasClass('inicio')==false) {
    //      jQuery('.rs-btn-mensual').click();
    //      jQuery('.btn-dual').last().click();
    //  }

    /* Actualizacion de textos en mobile */
    jQuery('.rs-materias').html(materias);
    jQuery('.rs-modalidad').text(modalidadtxt(getCampo('modalidad').modalidad));
    jQuery('.rs-carrerac').text(getCampo('carrera').Link);
    /* Actualizacion de textos en mobile */
    $(".blink").animate({opacity:0},200,"linear",function(){
        $(this).animate({opacity:1},200);
      });
    console.warn("fn calcular")
}

function getBeca(promedio, catcosto) {
    console.log("obteniendo la beca");
    console.log('Para' + catcosto);
    var idCarrera = getCampo('modalidad').iddynamics;
    var setbeca = getCampoBeca(catcosto);
    var campus = getCampo('campus');
    var materias = getCampo('materias');
    var becaFinal = 0;
    jQuery('.rs-becamasp').html('');
    console.log('buscando beca de campus' + campus);
    var becacampus = setbeca;
    console.log(becacampus);
    var RangoBecas = "";
    if (becacampus[campus]) {
        console.log('Si eocntro un nodo con' + campus);
        RangoBecas = becacampus[campus];
    }
    else {
        console.log('NO encontro un nodo' + campus + ' y usara el de TODOS');
        RangoBecas = becacampus['TODOS'];
    }
    console.log(RangoBecas);
    console.log('buscando este promedio' + promedio);

    var minimoMateriasParaBeca = getMinimoMatBeca(catcosto);
    console.log('El minimo para beca es:' + minimoMateriasParaBeca + " y tengo:" + materias + " catcosto:" + catcosto);
    if (materias >= minimoMateriasParaBeca) {
        console.log('si aplica beca');








        if (campus == 'GDL') {
            console.log('BECA DE GDL');
            var becaPosibilidades = 20;
            //busca Beca Academica 
            Object.entries(RangoBecas).forEach(function (key, value) {
                console.log(key[1]['promedio'][0] + "<=" + promedio + "&&" + key[1]['promedio'][1] + ">=" + promedio);
                if (key[1]['promedio'][0] <= promedio && key[1]['promedio'][1] >= promedio) {
                    console.log("SI_ENTRA_BECA_entro en este porcentaje de beaca");
                    console.log(key[1]['promedio'][0] + " " + key[1]['promedio'][1] + "con beca de " + key[1]['beca']);
                    becaFinal = key[1]['beca'];
                }
                else {
                    console.log('no encuentra beca');
                }
            });



            //Si es Ingenieria o ingegnieria Ejecutiva Beca fundador 0
            // if (
            //     (catcosto == "ING" || catcosto == "INGEJEC" || catcosto == "LICEJEC") && //Si es ingenieria o ingenieria ejecutiva
            //     (idCarrera != 681 && idCarrera != 682 && idCarrera != 902 && idCarrera != 3976) //Diferente de las carreras que se cobran como ING peor son LIC (681) Diseño G (682) Arqui (902) DAAD (3976) Diseño y animacion Digital
            // ) {
            //     becaPosibilidades = 0;
            // }

            // ING 2 883  INGENIERIA CIVIL
            // ING 2 886 INGENIERIA INDUSTRIAL Y DE SISTEMAS
            // ING 2 889 INGENIERIA SISTEMAS COMPUTACIONALES
            //Todas las demas ing no tienen 


            // INGEJEC 3 3875 INGENIERÍA EN SOFTWARE Y REDES
            // INGEJEC 3 3974 INGENIERÍA INDUSTRIAL Y ADMINISTRACIÓN 
            // "LicEjec" tampoco  Esbeltas solo aplican esas 

            // var incConBecaPosib = [3974, 889, 886, 883, 3875]; //
            // if ((catcosto == "ING" || catcosto == "INGEJEC" || catcosto == "LICEJEC") && incConBecaPosib.indexOf(idCarrera) != -1 && materias > 3) { //Si selecciono ingenieria y es alguna de estas carreras
            //     becaPosibilidades = 20;
            // }
            // 1. LIC 2 603 CONTADURIA PUBLICA
            // 2. LIC 2 704 LIC.EN ADMINISTRACION DE EMPRESA
            // 3. LIC 2 706 MERCADOTECNIA
            // 4. LIC 2 713 NEGOCIOS INTERNACIONALES
            // 5. LIC 2 729 LIC.EN COMERCIO INTERNACIONAL
            // 6. LIC 2 609 DERECHO
            // 7. LIC 2 1326 LICENCIATURA EN MERCADOTECNIA DIGITAL Y PUBLICIDAD
            // 8. LICPSICOLOG 2 611 PSICOLOGIA
            // 9. LICPEDAG 2 612 PEDAGOGÍA
            // 10.
            // 11. PREPA 7 666 PREPARATORIA
            // 12.
            // 13. NUTRI 1 1040 NUTRICIÓN
            // 14. FISIO 1 1060 LICENCIATURA EN FISIOTERAPIA
            // 15. ENFER 1 1150 ENFERMERÍA
            // 16. GASTRO 2 1221 LICENCIATURA EN GASTRONOMÍA
            // 17. BLUEM 2 1305 LICENCIAURA INTERACIONAL EN TURISMO Y REUNIONES
            // 18.
            // 19. ING 2 681 DISEÑO GRAFICO
            // 20. ING 2  588 MECATRONICA______________________________________
            // 21. ING 2 682 ARQUITECTURA
            // 22. ING 2 787 INGENIERIA QUIMICA_________________________________
            // 23. ING 2 883  INGENIERIA CIVIL
            // 24. ING 2 884 INGENIERIA MECANICA_______________________________
            // 25. ING 2 886 INGENIERIA INDUSTRIAL Y DE SISTEMAS
            // 26. ING 2 889 INGENIERIA SISTEMAS COMPUTACIONALES
            // 27. ING 2 902 LIC.EN DISEÑO ANIMACION Y ARTE DIGITAL
            // 28.
            // 29. INGEJEC 3 3974 INGENIERÍA INDUSTRIAL Y ADMINISTRACIÓN
            // 30. INGEJEC 3 3976 DISEÑO DIGITAL Y ANIMACIÓN___________________________________
            // 31. INGEJEC 3 3875 INGENIERÍA EN SOFTWARE Y REDES
            // 32.
            // 33. LICEJEC 3 3914 LICENCIATURA EN RELACIONES INTERNACIONALES__________
            // 34. LICEJEC 3 3953 LIC.EN CONT PUBLICA HIBRIDO_______________________________
            // 35. LICEJEC 3 3954 LIC.EN ADMON DE EMP.HIBRIDO______________________________
            // 36. LICEJEC 3 3956 LIC.EN MERCADOTECNIA HIBRIDO_____________________________
            // 37. LICEJEC 3 3959 LIC.EN DERECHO HIBRIDO_____________________________________
            // 38. LICEJEC 3 3961 LIC.EN PSICOLOGIA HIBRIDO___________________________________
            // 39. LICEJEC 3 3972 LIC.EN PEDAGOGIA HIBRIDO___________________________________
            // 40. LICEJEC 3 3971 LIC.ADMON DE TEC DE INF HIBRIDO___________________________
            // 41. LICEJEC 3 3973 LIC.NEGOCIOS INT.HIBRIDO___________________________________

            var incConBecaPosib = [588, 787, 884, 3976, 3914, 3953, 3954, 3956, 3959, 3961, 3972, 3971, 3973,1584,1587];
            if (incConBecaPosib.indexOf(idCarrera) != -1) { //es alguna de estas no aplica beca academica
                becaPosibilidades = 0;
            }

            if (materias < 3) {
                becaPosibilidades = 0;
            }

             if( idCarrera == 130 || idCarrera == 132 ){
                becaPosibilidades = 0;
            }


            console.log("Sumando beca final " + becaFinal + " y beca mas posibilidades:" + becaPosibilidades);

            if (becaPosibilidades == 20) {
                jQuery('.rs-becamasp').html('<br>*Incluye Beca +posibilidades');
            }
            becaFinal = parseInt(becaFinal) + parseInt(becaPosibilidades);

        } else {
            Object.entries(RangoBecas).forEach(function (key, value) {
                console.log(key[1]['promedio'][0] + "<=" + promedio + "&&" + key[1]['promedio'][1] + ">=" + promedio);
                if (key[1]['promedio'][0] <= promedio && key[1]['promedio'][1] >= promedio) {
                    console.log("SI_ENTRA_BECA_entro en este porcentaje de beaca");
                    console.log(key[1]['promedio'][0] + " " + key[1]['promedio'][1] + "con beca de " + key[1]['beca']);
                    becaFinal = key[1]['beca'];
                }
                else {
                    console.log('no encuentra beca');
                    //console.log('No es de este rango porque ' + key[1]['promedio'][0] +"no es menor o igual que"+this.promedio +"y " );
                }
            });
        }















    }
    else {
        beca = 0;
        console.log('no aplica beca');
    }






    if (becaFinal) {
        becaFinal = becaFinal;
    }
    else {
        becaFinal = 0;
    }
    console.log('La beca fue de' + becaFinal);
    guardaUsuario('beca', becaFinal);
    guardaUsuario('promedio', promedio);


    //BECA FUNDADOR
    // if (campus == 'GDL') {
    //     if (getCampo('modalidad').IdDynamics == "787" 
    //         || getCampo('modalidad').IdDynamics == "884" 
    //         || getCampo('modalidad').IdDynamics == "588") {
    //         console.log('No aplica beca GDL Fundador');
    //     }
    //     else {
    //         console.log('venia ' + becaFinal);
    //         becaFinal = parseInt(becaFinal) + 20; 
    //         console.log('Temrino con beca de' + becaFinal);
    //     }
    // }
    return becaFinal;
}


/*Funcion que personaliza los copies de cada BL By SRP 03-06-2019*/
function copyPersonalizable(linea){
    var text;
    //var style = 'style="color:#0275d8;"';
    var style = 'style="color:#f68b1f;"';

    switch( linea ){
        case 'PREPARATORIA':
            text = 'Escribe ó selecciona el programa académico de tu interés';
        break;

        case 'LICENCIATURA':
            text = 'Escribe ó selecciona la <b ' + style+ '>licenciatura</b> de tu interés';
        break;

        case 'INGENIERIA':
            text = 'Escribe ó selecciona la <b ' + style+ '>ingeniería</b> de tu interés';
        break;

        case 'SALUD':
            text = 'Escribe ó selecciona la <b ' + style+ '>Lic. en salud</b> de tu interés';
        break;

        case 'DIPLOMADO':
            text = 'Escribe ó selecciona el <b ' + style+ '>diplomado</b> de tu interés';
        break;

        case 'POSGRADO':
            text = 'Escribe ó selecciona el <b ' + style+ '>posgrado</b> de tu interés';
        break;

        default:
            text = 'Escribe ó selecciona la <b ' + style+ '>licenciatura</b> de tu interés';
        break;
    }

    return text;
}

function copyPersonalizableMobile(linea){
    var text;
    //var style = 'style="color:#0275d8;"';
    var style = 'style="color:#f68b1f;"';

    switch( linea ){
        case 'PREPARATORIA':
            text = '¿Qué quieres estudiar?';
        break;

        case 'LICENCIATURA':
            text = '¿Qué <b ' + style+ '>licenciatura</b> quieres estudiar?';
        break;

        case 'INGENIERIA':
            text = '¿Qué <b ' + style+ '>ingeniería</b> quieres estudiar?';
        break;

        case 'SALUD':
            text = '¿Qué <b ' + style+ '>Lic. en salud</b> quieres estudiar?';
        break;

        case 'DIPLOMADO':
            text = '¿Qué <b ' + style+ '>diplomado</b> quieres estudiar?';
        break;

        case 'POSGRADO':
            text = '¿Qué <b ' + style+ '>posgrado</b> quieres estudiar?';
        break;

        default:
            text = '¿Qué quieres estudiar?';
        break;
    }

    return text;
}
/*End Funcion que personaliza los copies de cada BL By SRP 03-06-2019*/