
<!doctype html>
<html>
<head>
  <meta charset="utf-8">
  <title>UNITEC | Calcula tu beca y colegiatura</title>
  <base href="/">
<meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no">
  <link rel="stylesheet" type="text/css" href="http://www.calidadacademica.mx/wp-content/themes/temaunitec/assets/frontend/css/home/min/home.min.css" media="all" i-amphtml-timeout="1">
  <link rel="stylesheet" id="bootstrareboot-css" href="http://www.calidadacademica.mx/wp-content/themes/temaunitec/assets/frontend/css/vendor/min/bootstrap-reboot.min.css" type="text/css" media="screen" i-amphtml-timeout="1">
  <link rel="stylesheet" id="bootstrapcss-css" href="http://www.calidadacademica.mx/wp-content/themes/temaunitec/assets/frontend/css/vendor/min/bootstrap.min.css"  type="text/css" media="screen" i-amphtml-timeout="1">
  <link rel="stylesheet" id="mdb-css" href="http://www.calidadacademica.mx/wp-content/themes/temaunitec/assets/frontend/css/vendor/min/mdb.min.css" type="text/css" media="screen" i-amphtml-timeout="1">
  <link rel="stylesheet" id="font-awesome-css" href="http://www.calidadacademica.mx/wp-content/themes/temaunitec/assets/frontend/font/generales/fa/css/font-awesome.min.css" type="text/css" media="screen" i-amphtml-timeout="1">
  <link rel="stylesheet" id="material-icons-css" href="https://fonts.googleapis.com/icon?family=Material+Icons" type="text/css" media="screen" i-amphtml-timeout="1">
  <link rel="stylesheet" id="generales_mobile_temaunitec-css" href="http://www.calidadacademica.mx/wp-content/themes/temaunitec/assets/frontend/css/generales/min/generales_mobile_temaunitec.min.css"    type="text/css" media="screen" i-amphtml-timeout="1">
  <link rel="stylesheet" id="13-css" href="http://www.calidadacademica.mx/wp-content/themes/temaunitec/assets/frontend/css/concatenados_servidor/13.min.css" type="text/css" media="screen" i-amphtml-timeout="1">
  <link rel="stylesheet" id="mob_2_banner_home-css" href="http://www.calidadacademica.mx/wp-content/themes/temaunitec/assets/frontend/css/secciones/min/mob_2_banner_home.min.css"  type="text/css" media="screen" i-amphtml-timeout="1">
  <script type='text/javascript' src='http://www.calidadacademica.mx/wp-content/themes/temaunitec/assets/frontend/js/vendor/min/jquery-3.1.1.min.js'></script>


</head>
<body>


  <div class="todoElBody">


<!-- Sidebar navigation -->
<ul id="UnitecSideNav" class="side-nav">
  <div id="side-nav-content" class="side-nav-content">
    <input type="hidden" id="sidenav_url_imagen_destacada" value="http://www.calidadacademica.mx">
    <input type="hidden" id="sidenav_nombre_imagen_destacada" value="">
    <div class="side-nav-header" style="background: linear-gradient( rgba(0,0,0,.4), rgba(0,0,0,.4) ), url('https://www.calidadacademica.mx/wp-content/uploads/2017/08/h_lic_c_d.png'); background-size:cover;">
      <div class="controls">
        <div class="logo">
          <a href="https://www.unitec.mx">
            <img src="http://www.calidadacademica.mx/wp-content/themes/temaunitec/assets/frontend/img/header/logo_unitec.svg">
          </a>
        </div>
        <span onClick="closeSideNav()" style="cursor:pointer;" class="icon-u icon-u-cerrar pull-right"></span>
      </div>
      <search-side-nav></search-side-nav>
      <!-- <img src="https://www.calidadacademica.mx/wp-content/uploads/2017/05/sidenav.jpg" class="img-fluid img-head" style:"visibility: hidden"> -->
    </div>

    <ul class="collapsible collapsible-accordion">
      <!-- <?php// wp_nav_menu( array( 'menu' => 'sidenav', 'walker' => $sideNav ) ); ?> -->
      <div class="menu-sidenav-container">
        <ul id="menu-sidenav" class="menu">
          <li>
            <a waves-effect href="https://www.calidadacademica.mx/conoce-la-unitec/" target="_blank">
              <i class="icon-u icon-u-conoce-la-universidad "></i>Conocé la Universidad</a>
          </li>
          <li>
            <a waves-effect href="https://www.calidadacademica.mx/prepa/" target="_blank">
              <i class="icon-u icon-u-preparatoria "></i>Preparatoria</a>
          </li>
          <li>
            <a waves-effect href="http://calidadacademica.mx/product-category/licenciaturas/" target="_blank">
              <i class="icon-u icon-u-licenciaturas "></i>Licenciaturas</a>
          </li>
          <li>
            <a waves-effect href="http://calidadacademica.mx/product-category/posgrados" target="_blank">
              <i class="icon-u icon-u-posgrados "></i>Posgrados</a>
          </li>
          <li>
            <a waves-effect href="https://www.calidadacademica.mx/admision-unitec/" target="_blank">
              <i class="icon-u icon-u-admision-nuevo-ingreso "></i>Admisión Nuevo Ingreso</a>
          </li>
          <ul class="collapsible collapsible-accordion">
            <li>
              <a class='collapsible-header waves-effect arrow-r'>
                <i class="icon-u icon-u-respaldo-economico-unitec "></i>Respaldo Económico Unitec
                <i class="fa fa-angle-down rotate-icon"></i>
              </a>
              <div class="collapsible-body">
                <ul>
                  <li>
                    <a waves-effect href="https://www.calidadacademica.mx/admision-unitec/" target="_blank">
                      <i class="icon-u icon-u-admision-nuevo-ingreso "></i>Admisión Nuevo Ingreso</a>
                  </li>
                </ul>
              </div>
            </li>
            <ul class="collapsible collapsible-accordion">
              <li>
                <a class='collapsible-header waves-effect arrow-r'>
                  <i class="icon-u icon-u-campus "></i>Campus
                  <i class="fa fa-angle-down rotate-icon"></i>
                </a>
                <div class="collapsible-body">
                  <ul>
                    <li>
                      <a waves-effect href="https://www.calidadacademica.mx/campus-enlinea/" target="_blank">
                        <i class="icon-u icon-u-universidad-campus-en-linea "></i>Universidad Campus en Línea</a>
                    </li>
                    <li>
                      <a waves-effect href="https://www.calidadacademica.mx/campus-atizapan/" target="_blank">
                        <i class="icon-u icon-u-universidad-campus-atizapan "></i>Universidad Campus Atizapán</a>
                    </li>
                    <li>
                      <a waves-effect href="https://www.calidadacademica.mx/campus-cuitlahuac/" target="_blank">
                        <i class="icon-u icon-u-universidad-campus-cuitlahuac "></i>Universidad Campus Cuitláhuac</a>
                    </li>
                    <li>
                      <a waves-effect href="https://www.calidadacademica.mx/campus-ecatepec/" target="_blank">
                        <i class="icon-u icon-u-universidad-campus-ecatepec "></i>Universidad Campus Ecatepec</a>
                    </li>
                    <li>
                      <a waves-effect href="https://www.calidadacademica.mx/campus-guadalajara/" target="_blank">
                        <i class="icon-u icon-u-universidad-campus-guadalajara "></i>Universidad Campus Guadalajara</a>
                    </li>
                    <li>
                      <a waves-effect href="https://www.calidadacademica.mx/campus-leon/" target="_blank">
                        <i class="icon-u icon-u-universidad-campus-leon "></i>Universidad Campus León</a>
                    </li>
                    <li>
                      <a waves-effect href="https://www.calidadacademica.mx/campus-marina/" target="_blank">
                        <i class="icon-u icon-u-universidad-campus-marina "></i>Universidad Campus Marina</a>
                    </li>
                    <li>
                      <a waves-effect href="https://www.calidadacademica.mx/campus-queretaro/" target="_blank">
                        <i class="icon-u icon-u-universidad-campus-queretaro "></i>Universidad Campus Querétaro</a>
                    </li>
                    <li>
                      <a waves-effect href="https://www.calidadacademica.mx/campus-sur/" target="_blank">
                        <i class="icon-u icon-u-universidad-campus-sur "></i>Universidad Campus Sur</a>
                    </li>
                    <li>
                      <a waves-effect href="https://www.calidadacademica.mx/campus-toluca/" target="_blank">
                        <i class="icon-u icon-u-universidad-campus-toluca "></i>Universidad Campus Toluca</a>
                    </li>
                  </ul>
                </div>
              </li>
              <hr>
              </li>
              <li>
                <a waves-effect href="https://www.calidadacademica.mx/alumnos/" target="_blank">
                  <i class="icon-u icon-u-alumnos "></i>Alumnos</a>
              </li>
              <li>
                <a waves-effect href="https://www.calidadacademica.mx/profesores/" target="_blank">
                  <i class="icon-u icon-u-profesores "></i>Profesores</a>
              </li>
              <li>
                <a waves-effect href="https://www.calidadacademica.mx/exalumnos/" target="_blank">
                  <i class="icon-u icon-u-egresados-de-la-universidad "></i>Egresados de la Universidad</a>
              </li>
            </ul>
      </div>
      </ul>

  </div>
  </ul>

  <!-- elemento para dragable sidenav-->
  <!-- SideNav UnitecSideNav button -->
  <div style="height:1px"></div>
  <div class="nvct">
    <div class="navContent navegador">
      <a href="#" data-activates="UnitecSideNav" class="btnMenuMobile">
        <i class="icon-u-menu"></i>
      </a>
      <a href="https://www.unitec.mx" class="logoNav">
        <img src="http://www.calidadacademica.mx/wp-content/themes/temaunitec/assets/frontend/img/footer/svg/unitec.svg">
      </a>

      <a id="login-biblioteca" onclick="openModal('#modal_frm_biblioteca')" class="searchNav d-none">
        <i class="icon-u icon-u-presencial "></i>
      </a>
      <a id="logout-biblioteca" onclick="cerrarSesion()" class="searchNav d-none">
        <i class="icon-u icon-u-tab-derecha "></i>
      </a>

      <a href="tel:018007864832" class="searchNav">
        <i class="icon-u icon-u-llamar "></i>
      </a>
      <a onclick="openModal('#modal_frm_app')" class="searchNav">
        <i class="icon-u icon-u-contacto "></i>
      </a>
      <a onclick="openModal('#buscador')" class="searchNav">
        <i class="icon-u icon-u-buscar "></i>
      </a>
    </div>
  </div>
  <div style="height:51px"></div>
  <app-root>Loading...</app-root>
  </div>

 
<script type='text/javascript' src='http://www.calidadacademica.mx/wp-content/themes/temaunitec/assets/frontend/js/vendor/min/tether.min.js'></script>
<script type='text/javascript' src='http://www.calidadacademica.mx/wp-content/themes/temaunitec/assets/frontend/js/vendor/min/bootstrap.min.js'></script>
  <script type='text/javascript' src='http://www.calidadacademica.mx/wp-content/themes/temaunitec/assets/frontend/js/vendor/min/mdb.min.js'></script>
<script>

  var topeleft = true;
  var topeRight = false;
  var right = false;

  $(document).ready(function () {

    $(window).scroll(function () {
      if (Math.abs($("#side-nav-content").offset().left) >= 100) {
        enableScrolling();
      }
    });
    var sidenav = document.getElementById('UnitecSideNav');
    var sidenavcontent = document.getElementById('side-nav-content');
    var mc = new Hammer(sidenav);
    var sdnvct = new Hammer(sidenavcontent);

    mc.on("panleft panright panend", function (ev) {

      var offset = $('#UnitecSideNav').position();

      if (ev.type === "panright") {

        $("html").css({ "overflow-y": "hidden" });
        $("body").css({ "overflow-y": "hidden" });
        //console.log("quitar scroll a la derecha");
        if (Math.abs($("#side-nav-content").offset().left) >= 100) {
          enableScrolling();
        } else {
          disableScrolling();
        }

        topeleft = false;
        right = true;
        if (topeRight == true) {
          return;
        }
        if (offset.left <= 0) {
          topeRight = false;
          $("#UnitecSideNav").css('left', parseInt(offset.left) + parseInt(ev.center.x / 32));
        } else {
          $("#UnitecSideNav").css('left', 0);
          topeRight = true;
        }
      }
      if (ev.type === "panleft") {
        //console.log($('#UnitecSideNav').position());
        $("html").css({ "overflow-y": "" });
        $("body").css({ "overflow-y": "" });
        //console.log("poner scroll a la izquierda");
        if (Math.abs($("#side-nav-content").offset().left) >= 100) {
          enableScrolling();
        } else {
          disableScrolling();
        }
        right = false;
        topeRight = false;
        sideNavWidth = (($("#UnitecSideNav").width()) * -1) + 30;
        if (topeleft == true) {
          return;
        }
        if (offset.left >= sideNavWidth) {
          topeleft = false;
          $("#UnitecSideNav").css('left', parseInt(offset.left) - parseInt(ev.center.x / 32));
        } else {
          $("#UnitecSideNav").css('left', '-95%');
          topeleft = true;
        }
      }

      if (ev.type == "panend") {

        if (Math.abs($("#side-nav-content").offset().left) >= 100) {
          enableScrolling();
        } else {
          disableScrolling();
        }


        if (right == true) {
          $("html").css({ "overflow-y": "hidden" });
          $("body").css({ "overflow-y": "hidden" });
          //console.log("quitar scroll a la derecha");
          if (Math.abs($("#side-nav-content").offset().left) >= 100) {
            enableScrolling();
          } else {
            disableScrolling();
          }
          $("#UnitecSideNav").animate({ 'left': "0px" }, 700);
        } else {
          $("html").css({ "overflow-y": "" });
          $("body").css({ "overflow-y": "" });
          //console.log("Poner scroll a la izquierda");
          if (Math.abs($("#side-nav-content").offset().left) >= 100) {
            enableScrolling();
          } else {
            disableScrolling();
          }
          $("#UnitecSideNav").animate({ 'left': "-95%" }, 1200);
        }
      }
    });

    $(".btnMenuMobile").on("click", function (ev) {
      //Evitamos el scrolltop
      ev.preventDefault();
      $("html").css({ "overflow-y": "hidden" });
      $("body").css({ "overflow-y": "hidden" });
      //console.log("Quitar scroll al abrir el sidenav");
      if (Math.abs($("#side-nav-content").offset().left) >= 100) {
        enableScrolling();
      } else {
        disableScrolling();
      }
      topeleft = false;
      topeRight = true;
      $("#UnitecSideNav").animate({ 'left': "0%" }, 1200);
    });
  });

  function closeSideNav() {

    $("html").css({ "overflow-y": "" });
    $("body").css({ "overflow-y": "" });
    //console.log("Poner scroll al cerrar el sidenav");
    enableScrolling();
    topeleft = true;
    topeRight = false;
    $("#UnitecSideNav").animate({ 'left': "-95%" }, 700);
  }


  function disableScrolling() {
    //console.log("Disable SCroll dentro de la funcion");
    var x = window.scrollX;
    var y = window.scrollY;
    window.onscroll = function () { window.scrollTo(x, y); };
  }

  function enableScrolling() {
    //console.log("Enable scroll dentro de la funcion");
    window.onscroll = function () { };
  }
</script>
<script type='text/javascript' src='http://www.calidadacademica.mx/wp-content/themes/temaunitec/assets/frontend/js/angularcalcu/inline.bundle.js'></script>
<script type='text/javascript' src='http://www.calidadacademica.mx/wp-content/themes/temaunitec/assets/frontend/js/angularcalcu/polyfills.bundle.js'></script>
<script type='text/javascript' src='http://www.calidadacademica.mx/wp-content/themes/temaunitec/assets/frontend/js/angularcalcu/main.bundle.js'></script>
</html>
