<?php

/**
* Additional hooks for "Permalink Manager Pro"
*/
class Permalink_Manager_Pro_Functions extends Permalink_Manager_Class {

	public function __construct() {
		define( 'PERMALINK_MANAGER_PRO', true );

		// Stop words
		add_filter( 'permalink_manager_filter_default_post_slug', array($this, 'remove_stop_words'), 9, 3 );
		add_filter( 'permalink_manager_filter_default_term_slug', array($this, 'remove_stop_words'), 9, 3 );

		// Custom fields in permalinks
		add_filter( 'permalink_manager_filter_default_post_uri', array($this, 'replace_custom_field_tags'), 9, 5 );

		// Permalink Manager Pro Alerts
		add_filter( 'permalink-manager-alerts', array($this, 'pro_alerts'), 9, 3 );
	}

	/**
	 * Stop words
	 */
	static function load_stop_words_languages() {
		return array (
			'ar' => __('Arabic', 'permalink-manager'),
			'zh' => __('Chinese', 'permalink-manager'),
			'da' => __('Danish', 'permalink-manager'),
			'nl' => __('Dutch', 'permalink-manager'),
			'en' => __('English', 'permalink-manager'),
			'fi' => __('Finnish', 'permalink-manager'),
			'fr' => __('French', 'permalink-manager'),
			'de' => __('German', 'permalink-manager'),
			'he' => __('Hebrew', 'permalink-manager'),
			'hi' => __('Hindi', 'permalink-manager'),
			'it' => __('Italian', 'permalink-manager'),
			'ja' => __('Japanese', 'permalink-manager'),
			'ko' => __('Korean', 'permalink-manager'),
			'no' => __('Norwegian', 'permalink-manager'),
			'fa' => __('Persian', 'permalink-manager'),
			'pl' => __('Polish', 'permalink-manager'),
			'pt' => __('Portuguese', 'permalink-manager'),
			'ru' => __('Russian', 'permalink-manager'),
			'es' => __('Spanish', 'permalink-manager'),
			'sv' => __('Swedish', 'permalink-manager'),
			'tr' => __('Turkish', 'permalink-manager')
		);
	}

	/**
	 * Load stop words
	 */
	static function load_stop_words($iso = '') {
		$json_dir = PERMALINK_MANAGER_DIR . "/includes/ext/stopwords-json/dist/{$iso}.json";
		$json_a = array();

		if(file_exists($json_dir)) {
			$string = file_get_contents($json_dir);
			$json_a = json_decode($string, true);
		}

		return $json_a;
	}

	/**
	 * Remove stop words from default URIs
	 */
	public function remove_stop_words($slug, $object, $name) {
		global $permalink_manager_options;

		if(!empty($permalink_manager_options['stop-words']['stop-words-enable']) && !empty($permalink_manager_options['stop-words']['stop-words-list'])) {
			$stop_words = explode(",", strtolower(stripslashes($permalink_manager_options['stop-words']['stop-words-list'])));

			foreach($stop_words as $stop_word) {
				$stop_word = trim($stop_word);
				$slug = preg_replace("/([\/-]|^)({$stop_word})([\/-]|$)/", '$1$3', $slug);
			}

			// Clear the slug
			$slug = preg_replace("/(-+)/", "-", trim($slug, "-"));
			$slug = preg_replace("/(-\/-)|(\/-)|(-\/)/", "/", $slug);
		}

		return $slug;
	}

	/**
	 * Hide "Buy Permalink Manager Pro" alert
	 */
	function pro_alerts($alerts = array()) {
		global $permalink_manager_options;

		if(empty($permalink_manager_options['licence']['licence_key'])) {
			$alerts['licence_key'] = array('txt' => sprintf(__("Please paste the licence key <a href=\"%s\">here</a> to access all Permalink Manager Pro updates!", "permalink-manager"), admin_url('tools.php?page=permalink-manager&section=settings')), 'type' => 'notice-error', 'show' => 1);
		}

		return $alerts;
	}

	/**
	 * Replace custom field tags in default post URIs
	 */
	function replace_custom_field_tags($default_uri, $native_slug, $post, $post_name, $native_uri) {
		preg_match_all("/%__(.[^\%]+)%/", $default_uri, $custom_fields);

		if(!empty($custom_fields[1])) {
			foreach($custom_fields[1] as $i => $custom_field) {
				$custom_field_value = apply_filters('permalink_manager_custom_field_value', get_post_meta($post->ID, $custom_field, true), $custom_field, $post);

				// Make sure that custom field is a string
				if(!empty($custom_field_value) && is_string($custom_field_value)) {
					$default_uri = str_replace($custom_fields[0][$i], sanitize_title($custom_field_value), $default_uri);
				}
			}
		}

		return $default_uri;
	}

	public function update_pro_info($raw, $result) {
		if(!empty($result['body'])) {
			$plugin_info = json_decode($result['body']);
			if(!empty($plugin_info['expiration_date'])) {
				// $plugin_info['expiration_date'];
			}
		}
		return $raw;
	}

}

?>
