<?php

/**
* Additional functions used in classes and another subclasses
*/
class Permalink_Manager_URI_Functions_Tax extends Permalink_Manager_Class {

	public function __construct() {
		add_filter( 'term_link', array($this, 'custom_tax_permalinks'), 999, 2 );
		add_filter( 'category_link', array($this, 'custom_tax_permalinks'), 999, 2 );
		add_filter( 'tag_link', array($this, 'custom_tax_permalinks'), 999, 2 );

		// Register _add_form_fields filter for all taxonomies
		add_action( 'admin_init', array($this, 'uri_box_hooks') );
	}

	/**
	* Change permalinks for taxonomies
	*/
	function custom_tax_permalinks($permalink, $term) {
		global $wp_rewrite, $permalink_manager_uris, $permalink_manager_options;

		$term_id = (!empty($term->term_id)) ? $term->term_id : $term;

		// Apend the language code as a non-editable prefix (can be used also for another prefixes)
		$prefix = apply_filters('permalink-manager-term-permalink-prefix', '', $term);

		if(isset($permalink_manager_uris["tax-{$term_id}"])) {
			$permalink = get_option('home');

			// Encode URI?
			if(!empty($permalink_manager_options['general']['decode_uris'])) {
				$permalink .= urldecode("/{$prefix}{$permalink_manager_uris["tax-{$term_id}"]}");
			} else {
				$permalink .= Permalink_Manager_Helper_Functions::encode_uri("/{$prefix}{$permalink_manager_uris["tax-{$term_id}"]}");
			}
		} else if(!empty($permalink_manager_options['general']['decode_uris'])) {
			$permalink = urldecode($permalink);
		}

		return apply_filters('permalink_manager_filter_final_term_permalink', $permalink, $term);
	}

	/**
	* Check if the provided slug is unique and then update it with SQL query.
	*/
	static function update_slug_by_id($slug, $id) {
		global $wpdb;

		// Update slug and make it unique
		$term = get_term(intval($id));
		$slug = (empty($slug)) ? sanitize_title(get_the_title($term->name)) : $slug;
		$new_slug = wp_unique_term_slug($slug, $term);

		$wpdb->query("UPDATE $wpdb->terms SET slug = '$new_slug' WHERE term_id = '$id'");

		return $new_slug;
	}

	/**
	* Get the active URI
	*/
	public static function get_term_uri($term_id, $native_uri = false) {
		global $permalink_manager_uris;

		// Check if input is term object
		$term = (isset($term_id->term_id)) ? $term_id->term_id : get_term($term_id);

		$final_uri = (!empty($permalink_manager_uris["tax-{$term_id}"])) ? $permalink_manager_uris["tax-{$term_id}"] : self::get_default_term_uri($term->term_id, $native_uri);
		return $final_uri;
	}

	/**
	* Get the default (not overwritten by the user) or native URI (unfiltered)
	*/
	public static function get_default_term_uri($term, $native_uri = false) {
		global $permalink_manager_options, $permalink_manager_uris, $permalink_manager_permastructs, $wp_rewrite;

		// Load all bases & term
		$term = is_object($term) ? $term : get_term($term);
		$term_id = $term->term_id;
		$taxonomy_name = $term->taxonomy;
		$taxonomy = get_taxonomy($taxonomy_name);
		$term_slug = ($native_uri == false) ? apply_filters('permalink_manager_filter_default_term_slug', $term->slug, $term, $term->name) : $term->slug;

		// Get the permastruct
		$default_permastruct = Permalink_Manager_Helper_Functions::get_default_permastruct($taxonomy_name, true);
		if($native_uri) {
			$permastruct = $default_permastruct;
		} else {
			$permastruct = (isset($permalink_manager_permastructs['taxonomies'][$taxonomy_name])) ? $permalink_manager_permastructs['taxonomies'][$taxonomy_name] : $default_permastruct;
		}
		$default_base = (!empty($permastruct)) ? trim($permastruct, '/') : "";

		// A. Check if taxonomy has custom rewrite rules
		if(empty($default_base) && !isset($permalink_manager_permastructs['taxonomies'][$taxonomy_name])) {
			if('category' == $taxonomy_name) {
				$default_base = "?cat={$term->term_id}";
			} elseif ($taxonomy->query_var) {
				$default_base = "?{$taxonomy->query_var}={$term_slug}";
			} else {
				$default_base = "?taxonomy={$taxonomy_name}&term={$slug}";
			}
		}
		// B. Use default rewrite rules
		else {
			// Add ancestors to hierarchical taxonomy
			if($taxonomy->rewrite['hierarchical']) {
				$hierarchical_slugs = array();
				$ancestors = get_ancestors( $term->term_id, $taxonomy_name, 'taxonomy' );
				foreach ( (array) $ancestors as $ancestor ) {
					$ancestor_term = get_term($ancestor, $taxonomy_name);
					$hierarchical_slugs[] = Permalink_Manager_Helper_Functions::force_custom_slugs($ancestor_term->slug, $ancestor_term);
				}
				$hierarchical_slugs = array_reverse($hierarchical_slugs);
				$default_base = (!empty($default_base) && strpos($default_base, "%{$taxonomy_name}%") !== false) ? str_replace("%{$taxonomy_name}%", implode('/', $hierarchical_slugs), $default_base) : $default_base . "/" . implode('/', $hierarchical_slugs);
			} else {
				$default_base = str_replace("%{$taxonomy_name}%", $term_slug, $default_base);
			}

			// Append the term slug now
			$term_slug = Permalink_Manager_Helper_Functions::force_custom_slugs($term_slug, $term);
			$default_base = (in_array("tax-{$taxonomy_name}", $permalink_manager_options['general']['disable_slug_appendix'])) ? $default_base : "{$default_base}/{$term_slug}";
		}
		$default_uri = $default_base;

		// Clear the URI
		$default_uri = preg_replace("/(%(?:.+?)%\/)/i", "", $default_uri);
		$default_uri = preg_replace('/\s+/', '', $default_uri);
		$default_uri = str_replace('//', '/', $default_uri);
		$default_uri = trim($default_uri, "/");

		return apply_filters('permalink_manager_filter_default_term_uri', $default_uri, $term->slug, $term, $term_slug, $native_uri);
	}

	/**
	* Find & replace (bulk action)
	*/
	public static function find_and_replace() {
		global $wpdb, $permalink_manager_uris;

		// Check if post types & statuses are not empty
		if(empty($_POST['post_types']) || empty($_POST['post_statuses'])) { return false; }

		// Reset variables
		$updated_slugs_count = 0;
		$updated_array = array();
		$alert_type = $alert_content = $errors = '';

		// Process the variables from $_POST object
		$old_string = esc_sql($_POST['old_string']);
		$new_string = esc_sql($_POST['new_string']);

		$taxonomy_names_array = ($_POST['taxonomies']) ? ($_POST['taxonomies']) : '';
		$taxonomy_names = implode("', '", $taxonomy_names_array);
		$mode = isset($_POST['mode']) ? $_POST['mode'] : 'custom_uris';

		// Filter the terms by IDs
		$where = '';
		if(!empty($_POST['ids'])) {
			// Remove whitespaces and prepare array with IDs and/or ranges
			$ids = esc_sql(preg_replace('/\s*/m', '', $_POST['ids']));
			preg_match_all("/([\d]+(?:-?[\d]+)?)/x", $ids, $groups);

			// Prepare the extra ID filters
			$where .= "AND (";
			foreach($groups[0] as $group) {
				$where .= ($group == reset($groups[0])) ? "" : " OR ";
				// A. Single number
				if(is_numeric($group)) {
					$where .= "(t.term_id = {$group})";
				}
				// B. Range
				else if(substr_count($group, '-')) {
					$range_edges = explode("-", $group);
					$where .= "(t.term_id BETWEEN {$range_edges[0]} AND {$range_edges[1]})";
				}
			}
			$where .= ")";
		}

		// Get the rows before they are altered
		$terms_to_update = $wpdb->get_results("SELECT t.slug, t.name, t.term_id, tt.taxonomy FROM {$wpdb->terms} as t INNER JOIN {$wpdb->term_taxonomy} as tt ON tt.term_id = t.term_id WHERE tt.taxonomy IN ('{$taxonomy_names}') {$where}", ARRAY_A);

		// Now if the array is not empty use IDs from each subarray as a key
		if($terms_to_update && empty($errors)) {
			foreach ($terms_to_update as $row) {
				// Prevent server timeout
				set_time_limit(0);

				// Prepare variables
				$this_term = get_term($row['term_id']);
				$term_permalink_id = "tax-{$row['term_id']}";

				// Get default & native URL
				$native_uri = self::get_default_term_uri($this_term, true);
				$default_uri = self::get_default_term_uri($this_term);
				$old_term_name = $row['slug'];
				$old_uri = (isset($permalink_manager_uris[$term_permalink_id])) ? $permalink_manager_uris[$term_permalink_id] : $native_uri;

				// Do replacement on slugs (non-REGEX)
				if(preg_match("/^\/.+\/[a-z]*$/i", $old_string)) {
					// Use $_POST['old_string'] directly here & fix double slashes problem
					$pattern = "~" . stripslashes(trim(sanitize_text_field($_POST['old_string']), "/")) . "~";

					$new_term_name = ($mode == 'slugs') ? preg_replace($pattern, $new_string, $old_term_name) : $old_term_name;
					$new_uri = ($mode != 'slugs') ? preg_replace($pattern, $new_string, $old_uri) : $old_uri;
				} else {
					$new_term_name = ($mode == 'slugs') ? str_replace($old_string, $new_string, $old_term_name) : $old_term_name; // Term slug is changed only in first mode
					$new_uri = ($mode != 'slugs') ? str_replace($old_string, $new_string, $old_uri) : $old_uri;
				}

				//print_r("{$old_uri} - {$new_uri} - {$native_uri} - {$default_uri} \n");

				// Check if native slug should be changed
				if(($mode == 'slugs') && ($old_term_name != $new_term_name)) {
					self::update_slug_by_id($new_term_name, $row['term_id']);
				}

				if(($old_uri != $new_uri) || ($old_term_name != $new_term_name)) {
					$permalink_manager_uris[$term_permalink_id] = $new_uri;
					$updated_array[] = array('item_title' => $row['name'], 'ID' => $row['term_id'], 'old_uri' => $old_uri, 'new_uri' => $new_uri, 'old_slug' => $old_term_name, 'new_slug' => $new_term_name, 'tax' => $this_term->taxonomy);
					$updated_slugs_count++;
				}
			}

			// Filter array before saving
			$permalink_manager_uris = array_filter($permalink_manager_uris);
			update_option('permalink-manager-uris', $permalink_manager_uris);

			$output = array('updated' => $updated_array, 'updated_count' => $updated_slugs_count);
			wp_reset_postdata();
		}

		return ($output) ? $output : "";
	}

	/**
	* Regenerate slugs & bases (bulk action)
	*/
	static function regenerate_all_permalinks() {
		global $wpdb, $permalink_manager_uris;

		// Check if post types & statuses are not empty
		if(empty($_POST['taxonomies'])) { return false; }

		// Process the variables from $_POST object
		$updated_slugs_count = 0;
		$updated_array = array();
		$alert_type = $alert_content = $errors = '';

		$taxonomy_names_array = ($_POST['taxonomies']) ? ($_POST['taxonomies']) : '';
		$taxonomy_names = implode("', '", $taxonomy_names_array);
		$mode = isset($_POST['mode']) ? $_POST['mode'] : 'custom_uris';

		// Filter the terms by IDs
		$where = '';
		if(!empty($_POST['ids'])) {
			// Remove whitespaces and prepare array with IDs and/or ranges
			$ids = esc_sql(preg_replace('/\s*/m', '', $_POST['ids']));
			preg_match_all("/([\d]+(?:-?[\d]+)?)/x", $ids, $groups);

			// Prepare the extra ID filters
			$where .= "AND (";
			foreach($groups[0] as $group) {
				$where .= ($group == reset($groups[0])) ? "" : " OR ";
				// A. Single number
				if(is_numeric($group)) {
					$where .= "(t.term_id = {$group})";
				}
				// B. Range
				else if(substr_count($group, '-')) {
					$range_edges = explode("-", $group);
					$where .= "(t.term_id BETWEEN {$range_edges[0]} AND {$range_edges[1]})";
				}
			}
			$where .= ")";
		}

		// Get the rows before they are altered
		$terms_to_update = $wpdb->get_results("SELECT t.slug, t.name, t.term_id, tt.taxonomy FROM {$wpdb->terms} as t INNER JOIN {$wpdb->term_taxonomy} as tt ON tt.term_id = t.term_id WHERE tt.taxonomy IN ('{$taxonomy_names}') {$where}", ARRAY_A);

		// Now if the array is not empty use IDs from each subarray as a key
		if($terms_to_update && empty($errors)) {
			foreach ($terms_to_update as $row) {
				// Prevent server timeout
				set_time_limit(0);

				// Prepare variables
				$this_term = get_term($row['term_id']);
				$term_permalink_id = "tax-{$row['term_id']}";

				// Get default & native URL
				$native_uri = self::get_default_term_uri($this_term, true);
				$default_uri = self::get_default_term_uri($this_term);
				$old_term_name = $row['slug'];
				$old_uri = (isset($permalink_manager_uris[$term_permalink_id])) ? $permalink_manager_uris[$term_permalink_id] : $native_uri;
				$correct_slug = sanitize_title($row['name']);

				// Process URI & slug
				$new_slug = wp_unique_term_slug($correct_slug, $this_term);
				$new_term_name = ($mode == 'slugs') ? $new_slug : $old_term_name; // Post name is changed only in first mode

				// Prepare the new URI
				if($mode == 'slugs') {
					$new_uri = $old_uri;
				} else if($mode == 'native') {
					$new_uri = $native_uri;
				} else {
					$new_uri = $default_uri;
				}

				//print_r("{$old_uri} - {$new_uri} - {$native_uri} - {$default_uri} \n");

				// Check if native slug should be changed
				if(($mode == 'slugs') && ($old_term_name != $new_term_name)) {
					self::update_slug_by_id($new_term_name, $row['term_id']);
				}

				if(($old_uri != $new_uri) || ($old_term_name != $new_term_name)) {
					$permalink_manager_uris[$term_permalink_id] = $new_uri;
					$updated_array[] = array('item_title' => $row['name'], 'ID' => $row['term_id'], 'old_uri' => $old_uri, 'new_uri' => $new_uri, 'old_slug' => $old_term_name, 'new_slug' => $new_term_name, 'tax' => $this_term->taxonomy);
					$updated_slugs_count++;
				}
			}

			// Filter array before saving
			$permalink_manager_uris = array_filter($permalink_manager_uris);
			update_option('permalink-manager-uris', $permalink_manager_uris);

			$output = array('updated' => $updated_array, 'updated_count' => $updated_slugs_count);
			wp_reset_postdata();
		}

		return (!empty($output)) ? $output : "";
	}

	/**
	* Update all slugs & bases (bulk action)
	*/
	static public function update_all_permalinks() {
		global $permalink_manager_uris;

		// Setup needed variables
		$updated_slugs_count = 0;
		$updated_array = array();

		$old_uris = $permalink_manager_uris;
		$new_uris = isset($_POST['uri']) ? $_POST['uri'] : array();

		// Double check if the slugs and ids are stored in arrays
		if (!is_array($new_uris)) $new_uris = explode(',', $new_uris);

		if (!empty($new_uris)) {
			foreach($new_uris as $id => $new_uri) {
				// Remove prefix from field name to obtain term id
				$term_id = filter_var(str_replace('tax-', '', $id), FILTER_SANITIZE_NUMBER_INT);

				// Prepare variables
				$this_term = get_term($term_id);
				$updated = '';

				// Get default & native URL
				$native_uri = self::get_default_term_uri($this_term, true);
				$default_uri = self::get_default_term_uri($this_term);

				$old_uri = isset($old_uris[$id]) ? trim($old_uris[$id], "/") : $native_uri;

				// Process new values - empty entries will be treated as default values
				$new_uri = preg_replace('/\s+/', '', $new_uri);
				$new_uri = (!empty($new_uri)) ? trim($new_uri, "/") : $default_uri;
				$new_slug = (strpos($new_uri, '/') !== false) ? substr($new_uri, strrpos($new_uri, '/') + 1) : $new_uri;

				if($new_uri != $old_uri) {
					$old_uris[$id] = $new_uri;
					$updated_array[] = array('item_title' => $this_term->name, 'ID' => $term_id, 'old_uri' => $old_uri, 'new_uri' => $new_uri, 'tax' => $this_term->taxonomy);
					$updated_slugs_count++;
				}
			}

			// Filter array before saving & append the global
			$old_uris = $permalink_manager_uris = array_filter($old_uris);
			update_option('permalink-manager-uris', $old_uris);

			//print_r($permalink_manager_uris);

			$output = array('updated' => $updated_array, 'updated_count' => $updated_slugs_count);
		}

		return ($output) ? $output : "";
	}

	/**
	* Allow to edit URIs from "Edit Term" admin pages (register hooks)
	*/
	public function uri_box_hooks() {
		$all_taxonomies = Permalink_Manager_Helper_Functions::get_taxonomies_array();

		foreach($all_taxonomies as $tax => $label) {
			add_action( "{$tax}_add_form_fields", array($this, 'edit_uri_box'), 10, 1 );
			add_action( "{$tax}_edit_form_fields", array($this, 'edit_uri_box'), 10, 1 );
			add_action( "edited_{$tax}", array($this, 'update_term_uri'), 10, 2 );
			add_action( "create_{$tax}", array($this, 'update_term_uri'), 10, 2 );
		}
	}

	/**
	* Allow to edit URIs from "New Term" & "Edit Term" admin pages
	*/
	public function edit_uri_box($term = '') {
		$label = __("Custom URI", "permalink-manager");
		$description = __("Clear/leave the field empty to use the default permalink.", "permalink-manager");

		// A. New term
		if (empty($term->term_id)) {
			$html = "<div class=\"form-field\">";
			$html .= "<label for=\"term_meta[uri]\">{$label}</label>";
			$html .= "<input type=\"text\" name=\"permalink-manager[term-uri]\" id=\"permalink-manager[term-uri]\" value=\"\">";
			$html .= "<p class=\"description\">{$description}</p>";
			$html .= "</div>";
		}
		// B. Edit term
		else {
			$uri = self::get_term_uri($term->term_id, true);
			$default_uri = self::get_default_term_uri($term->term_id);

			$html = "<tr id=\"permalink-manager\" class=\"form-field permalink-manager-edit-uri-box\">";
			$html .= "<th scope=\"row\" valign=\"top\"><label for=\"permalink-manager[term-uri]\">{$label}</label></th>";
			$html .= "<td>";
			$html .= "<input type=\"text\" name=\"permalink-manager[term-uri]\" data-default=\"{$default_uri}\" id=\"permalink-manager[term-uri]\" value=\"{$uri}\">";
			$html .= sprintf("<p class=\"default-permalink-row columns-container\"><span class=\"column-3_4\"><strong>%s:</strong> {$default_uri}</span><span class=\"column-1_4\"><a href=\"#\" class=\"restore-default\"><span class=\"dashicons dashicons-image-rotate\"></span> %s</a></span></p>", __("Default URI", "permalink-manager"), __("Restore to Default URI", "permalink-manager"));
			$html .= "</td>";
			$html .= "</tr>";
		}

		echo $html;
	}

	/**
	* Update URI from "Edit Term" admin page
	*/
	function update_term_uri($term_id, $taxonomy) {
		global $permalink_manager_uris;

		// Check if user changed URI
		if(!isset($_POST['permalink-manager']['term-uri'])) {
			return;
		}

		// Get the term object
		$this_term = get_term($term_id);
		$term_permalink_id = "tax-{$term_id}";

		// Get default & native & user-submitted URIs
		$native_uri = self::get_default_term_uri($this_term, true);
		$default_uri = self::get_default_term_uri($this_term);
		$old_uri = (isset($permalink_manager_uris[$term_permalink_id])) ? $permalink_manager_uris[$term_permalink_id] : "";
		$new_uri = trim($_POST['permalink-manager']['term-uri'], "/");

		// A little hack (if user removes whole URI from input) ...
		$new_uri = (!empty($new_uri)) ? $new_uri : $default_uri;

		// Save only changed URIs
		if($new_uri != $old_uri) {
			$permalink_manager_uris[$term_permalink_id] = $new_uri;
		}

		update_option('permalink-manager-uris', $permalink_manager_uris);
	}

	/**
	* Remove URI from options array after post is moved to the trash
	*/
	function remove_term_uri($term_id) {
		global $permalink_manager_uris;

		// Check if the custom permalink is assigned to this post
		if(isset($permalink_manager_uris[$term_id])) {
			unset($permalink_manager_uris[$term_id]);
		}

		update_option('permalink-manager-uris', $permalink_manager_uris);
	}

}

?>
