<?php
/*
Plugin Name: Descuentos Comerciales UNITEC
Plugin URI: http://www.unitec.mx
Description: Descuentos Galeria reutilizable. 
Version: 1.0
Author: Web dev
Author URI: http://www.unitec.mx
*/

// Basic plugin definitions 
define ('FF_PLG_NAME', 'descuentos-unitec');
define( 'FF_PLG_VERSION', '1.0' );
define( 'FF_URL', WP_PLUGIN_URL . '/' . str_replace( basename(__FILE__), '', plugin_basename(__FILE__) ));
define( 'FF_DIR', WP_PLUGIN_DIR . '/' . str_replace( basename(__FILE__), '', plugin_basename(__FILE__) ));


// Plugin INIT
require_once(FF_DIR.'inc/install.php');
?>