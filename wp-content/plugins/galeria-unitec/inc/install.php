<?php
/*
File: inc/install.php
Description: Install functions
Plugin: galuni
Author: web Team
*/


//***************************************************************************
// Plugin INIT
//***************************************************************************


// LANGUAGE
add_action('init', 'galuni_language');

function galuni_language() {
	
	load_plugin_textdomain(FF_PLG_NAME, false, FF_DIR . '/languages/'); 
	
}


// ASSET REGISTERING
add_action( 'wp_enqueue_scripts', 'ff_assets_registering' );
function ff_assets_registering() {
	
	// CSS Main File
	//wp_register_style( 'ff-style',  FF_URL . 'assets/css/style.css' );
    //wp_enqueue_style( 'ff-style' );
	
	// CSS Libs
	//wp_register_style( 'ff-animate',  FF_URL . 'assets/css/animate.css' );
	//wp_register_style( 'ff-icons',  FF_URL . 'assets/css/icons.css' );
	//wp_register_style( 'ff-social',  FF_URL . 'assets/css/social.css' );
	//wp_register_style( 'ff-owl',  FF_URL . 'assets/css/owl.carousel.css' );
	//wp_register_style( 'ff-reset',  FF_URL . 'assets/css/reset.css' );
	//wp_register_style( 'ff-fancy',  FF_URL . 'assets/css/jquery.fancybox.css' );
	
	// JS Libs
	//wp_register_script('ff-isotope', FF_URL . 'assets/js/isotope.pkgd.min.js', array('jquery'), '', true);
	//wp_register_script('ff-jqueryeasing', FF_URL . 'assets/js/jquery.easing-min.js', array('jquery'), '', true);
	//wp_register_script('ff-tubeplayer', FF_URL . 'assets/js/jQuery.tubeplayer.min.js', array('jquery'), '', true);
	//wp_register_script('ff-nicescroll', FF_URL . 'assets/js/jquery.nicescroll.min.js', array('jquery'), '', true);
	//wp_register_script('ff-owlcarousel', FF_URL . 'assets/js/owl.carousel.min.js', array('jquery'), '', true);
	//wp_register_script('ff-jqfancy', FF_URL . 'assets/js/jquery.fancybox.js', array('jquery'), '', true);
	
	// JS Main Script
	//wp_register_script('ff-mainscript', FF_URL . 'assets/js/script.js', array('jquery'), '', true); 
	
}


// FUNCTIONS
//require_once(FF_DIR.'inc/functions.php'); 

// POST TYPES
// require_once(FF_DIR.'inc/posttypes/ff-projects.php');
// require_once(FF_DIR.'inc/posttypes/ff-layouts.php');
// require_once(FF_DIR.'inc/posttypes/ff-galleries.php');

require_once(FF_DIR.'inc/posttypes/galeria-unitec.php');
require_once(FF_DIR.'inc/posttypes/testimoniales-unitec.php');
require_once(FF_DIR.'inc/posttypes/descuentos-unitec.php');
require_once(FF_DIR.'inc/posttypes/vacantes-unitec.php');
require_once(FF_DIR.'inc/posttypes/talleres-unitec.php');

// META FRAMEWORK
//require_once(FF_DIR.'framework/bootstrap.php'); 

// META BOXES
// require_once(FF_DIR.'inc/metaboxes/ff-projects.php');
// require_once(FF_DIR.'inc/metaboxes/ff-layouts.php');
// require_once(FF_DIR.'inc/metaboxes/ff-galleries.php');

// SHORTCODES
//require_once(FF_DIR.'inc/shortcodes.php');