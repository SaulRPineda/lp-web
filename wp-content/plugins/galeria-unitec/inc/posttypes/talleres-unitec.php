<?php
/*
File: inc/posttypes/talleres-unitec.php
Description: Talleres Post Type
Plugin: talleres-unitec
Author: Web Team
*/


// Post Type Registration
add_action( 'init', 'talleres_unitec_register_posttype' );

function talleres_unitec_register_posttype() {

	$labels = array(
		'name'               => __('Talleres Unitec', FF_PLG_NAME),
		'singular_name'      => __('Taller', FF_PLG_NAME),
		'add_new'            => __('Nuevo Taller', FF_PLG_NAME),
		'add_new_item'       => __('Agregar nuevo taller', FF_PLG_NAME),
		'edit_item'          => __('Editar taller', FF_PLG_NAME),
		'new_item'           => __('Nuevo taller', FF_PLG_NAME),
		'all_items'          => __('Talleres', FF_PLG_NAME),
		'view_item'          => __('Ver taller', FF_PLG_NAME),
		'search_items'       => __('Buscar taller', FF_PLG_NAME),
		'not_found'          => __('No se encuentran talleres', FF_PLG_NAME),
		'not_found_in_trash' => __('No se encuentran talleres en la basura', FF_PLG_NAME),
		'parent_item_colon'  => '',
	  );
	
	  $args = array(
		'labels'             => $labels,
		'public'             => true,
		'publicly_queryable' => true,
		'show_ui'            => true,
		'show_in_menu'       => true,
		'query_var'          => true,
		'rewrite'            => array( 'slug' => 'talleres-unitec' ),
		'capability_type'    => 'post',
		'has_archive'        => false,
		'hierarchical'       => false,
		'supports'           => array( 'title' )
	  );
	
        register_post_type( 'talleres-unitec', $args );
      
	
}

// Post Type custom messages
add_filter( 'post_updated_messages', 'talleres_unitec_custom_messages' );

function talleres_unitec_custom_messages( $messages ) {
	
  global $post, $post_ID;

  $messages['unitec-talleres'] = array(
    0 => '', // Unused. Messages start at index 1.
    1 => __('Gallery updated.', FF_PLG_NAME),
    2 => __('Custom field updated.', FF_PLG_NAME),
    3 => __('Custom field deleted.', FF_PLG_NAME),
    4 => __('Gallery updated.', FF_PLG_NAME),
    /* translators: %s: date and time of the revision */
    5 => isset($_GET['revision']) ? sprintf( __('Gallery restored to revision from %s', FF_PLG_NAME), wp_post_revision_title( (int) $_GET['revision'], false ) ) : false,
    6 => __('Gallery published.', FF_PLG_NAME),
    7 => __('Gallery saved.', FF_PLG_NAME),
    8 => __('Gallery submitted.', FF_PLG_NAME),
    9 => sprintf( __('Gallery scheduled for: <strong>%1$s</strong>.', FF_PLG_NAME),
      // translators: Publish box date format, see http://php.net/date
      date_i18n( __( 'M j, Y @ G:i', FF_PLG_NAME ), strtotime( $post->post_date ) ) ),
    10 => __('Gallery draft updated.', FF_PLG_NAME),
  );

  return $messages;
  
}


