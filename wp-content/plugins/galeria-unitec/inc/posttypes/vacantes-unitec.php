<?php
/*
File: inc/posttypes/vacantes-unitec.php
Description: Vacantes Post Type
Plugin: vacantes-unitec
Author: Web Team
*/


// Post Type Registration
add_action( 'init', 'vacantes_unitec_register_posttype' );

function vacantes_unitec_register_posttype() {

	$labels = array(
		'name'               => __('Vacantes Impulsa', FF_PLG_NAME),
		'singular_name'      => __('Vacante', FF_PLG_NAME),
		'add_new'            => __('Nueva Vacante', FF_PLG_NAME),
		'add_new_item'       => __('Agregar nueva vacante', FF_PLG_NAME),
		'edit_item'          => __('Editar vacante', FF_PLG_NAME),
		'new_item'           => __('Nueva vacante', FF_PLG_NAME),
		'all_items'          => __('Vacantes', FF_PLG_NAME),
		'view_item'          => __('Ver vacantes', FF_PLG_NAME),
		'search_items'       => __('Buscar vacante impulsa', FF_PLG_NAME),
		'not_found'          => __('No se encuentran vacantes impulsa', FF_PLG_NAME),
		'not_found_in_trash' => __('No se encuentran vacantes impulsa en la basura', FF_PLG_NAME),
		'parent_item_colon'  => '',
	  );
	
	  $args = array(
		'labels'             => $labels,
		'public'             => true,
		'publicly_queryable' => true,
		'show_ui'            => true,
		'show_in_menu'       => true,
		'query_var'          => true,
		'rewrite'            => array( 'slug' => 'vacantes-unitec' ),
		'capability_type'    => 'post',
		'has_archive'        => false,
		'hierarchical'       => false,
		'supports'           => array( 'title' )
	  );
	
        register_post_type( 'vacantes-unitec', $args );
      
	
}

// Post Type custom messages
add_filter( 'post_updated_messages', 'vacantes_unitec_custom_messages' );

function vacantes_unitec_custom_messages( $messages ) {
	
  global $post, $post_ID;

  $messages['unitec-vacantes'] = array(
    0 => '', // Unused. Messages start at index 1.
    1 => __('Gallery updated.', FF_PLG_NAME),
    2 => __('Custom field updated.', FF_PLG_NAME),
    3 => __('Custom field deleted.', FF_PLG_NAME),
    4 => __('Gallery updated.', FF_PLG_NAME),
    /* translators: %s: date and time of the revision */
    5 => isset($_GET['revision']) ? sprintf( __('Gallery restored to revision from %s', FF_PLG_NAME), wp_post_revision_title( (int) $_GET['revision'], false ) ) : false,
    6 => __('Gallery published.', FF_PLG_NAME),
    7 => __('Gallery saved.', FF_PLG_NAME),
    8 => __('Gallery submitted.', FF_PLG_NAME),
    9 => sprintf( __('Gallery scheduled for: <strong>%1$s</strong>.', FF_PLG_NAME),
      // translators: Publish box date format, see http://php.net/date
      date_i18n( __( 'M j, Y @ G:i', FF_PLG_NAME ), strtotime( $post->post_date ) ) ),
    10 => __('Gallery draft updated.', FF_PLG_NAME),
  );

  return $messages;
  
}


