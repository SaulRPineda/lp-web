<?php    
    $data = NULL;
    $response = NULL;
    $urlProcWebLeads = "https://www.unitec.mx/procWebLeads/index.php";
    $datos = $_REQUEST['theData'];
    $jsonResponse = NULL;
    
    function quitar_tildes($cadena) {
		$no_permitidas= array ("á","é","í","ó","ú","Á","É","Í","Ó","Ú","ñ","À","Ã","Ì","Ò","Ù","Ã™","Ã ","Ã¨","Ã¬","Ã²","Ã¹","ç","Ç","Ã¢","ê","Ã®","Ã´","Ã»","Ã‚","ÃŠ","ÃŽ","Ã”","Ã›","ü","Ã¶","Ã–","Ã¯","Ã¤","«","Ò","Ã","Ã„","Ã‹");
		$permitidas= array ("a","e","i","o","u","A","E","I","O","U","n","N","A","E","I","O","U","a","e","i","o","u","c","C","a","e","i","o","u","A","E","I","O","U","u","o","O","i","a","e","U","I","A","E");
		$texto = str_replace($no_permitidas, $permitidas ,$cadena);
		return $texto;
	}	

    if ($datos != NULL || $datos !="") {

    	$estado = quitar_tildes( $datos['estado'] );
    	//Success Envia Datos
    	$data['nombre'] = trim( $datos['nombre'] );
	    $data['apaterno'] = trim( $datos['apaterno'] );
	    $data['amaterno'] = trim( $datos['amaterno'] );
	    $data['campus'] = trim( $datos['campus'] );
	    $data['campusLargo'] = trim( $datos['campusLargo'] );
	    $data['carrera'] = $datos['carrera'];
	    $data['categoria'] = $datos['categoria'];
	    $data['celular'] = $datos['celular'];
	    $data['ciclo'] = $datos['ciclo'];
	    $data['email'] = trim( $datos['email'] );
	    $data['estado'] = trim( strtoupper(  $estado ) );
	    $data['carreraInteres'] = $datos['carreraInteres'];
	    $data['linea'] = $datos['linea'];
	    $data['modalidad'] = $datos['modalidad'];
	    $data['nivelInteres'] = $datos['nivelInteres'];
	    $data['subNivelInteres'] = $datos['subNivelInteres'];
	    $data['tipoRegistro'] = $datos['tipoRegistro'];
	    $data['hubspotutk'] = trim( strtolower( $datos['hubspotutk'] ) );
	    $data['banner'] = $datos['banner'];
	    $data['CID'] = $datos['CID'];
	    $data['esAlumno'] = $datos['esAlumno'];
	    $data['urlreferrer'] = $datos['urlreferrer'] ;
        $data['aPaternoTutor'] = (strtoupper($datos['aPaternoTutor']) == "SI" )? "TUTOR": strtoupper($datos['aPaternoTutor']);

    	if( strpos( strtolower( $datos['urlreferrer']),"impulsa" ) ) {

    		$response = file_get_contents( $urlProcWebLeads."?".http_build_query($data) );

    	} else if( strpos( strtolower( $datos['urlreferrer']),"testvocacional" ) || strpos( strtolower( $datos['urlreferrer']),"orientacion-profesional" )) {

    		$data['formOrigen'] = "CC";
    		$response = file_get_contents( $urlProcWebLeads."?".http_build_query($data) );

    	} else {
    		
    		$response = file_get_contents( $urlProcWebLeads."?".http_build_query($data) );

    	}
    	$leadid = sha1(date('d/m/Y H:i:s')."|".$data['email']);    	
    	$arreglores = json_decode($response,true);
    	$arreglores['leadId'] = $leadid;
    	$jsonResponse = json_encode($arreglores);    	
    } else{
    	//Failure No Data
    	/*echo "Datos No Enviados";*/
    }

    //echo $response;
    echo $jsonResponse;
?>