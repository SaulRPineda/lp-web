import { NgModule, ModuleWithProviders } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HomeComponent } from './home/home.component';

const routes: Routes = [
    // { path: '', redirectTo: '/inicio', pathMatch: 'full' },
    // { path: 'inicio', component: HomeComponent },
    { path: '', component: HomeComponent },
    { path: 'calculadora', component: HomeComponent },
    // { path: 'formulario', loadChildren: './estado/estado.module#EstadoModule' },
    // { path: 'linea', loadChildren: './linea/linea.module#LineaModule' },
    // { path: 'oferta', loadChildren: './oferta/oferta.module#OfertaModule' },
    // { path: 'modalidad', loadChildren: './modalidad/modalidad.module#ModalidadModule' },
    // { path: 'campus', loadChildren: './campus/campus.module#CampusModule' },
    // { path: 'resultado', loadChildren: './resultado/resultado.module#ResultadoModule' }
];

export const routing: ModuleWithProviders = RouterModule.forRoot(routes);

// @NgModule({
//   imports: [RouterModule.forRoot(routes)],
//   exports: [RouterModule]
// })
// export class AppRoutingModule {}