import { EventEmitter, Output, ElementRef, Component, ViewEncapsulation, Input, OnInit, Inject, Injectable } from '@angular/core';
import { FormBuilder, FormControl, FormGroup } from '@angular/forms';
import { Router } from '@angular/router';
import { DOCUMENT } from "@angular/platform-browser";
import { User, UserService, DatosService } from '../shared';
import { Observable } from "rxjs/Rx";
import { ReplaySubject } from 'rxjs/ReplaySubject';
import { NouisliderModule } from 'ng2-nouislider';
import * as noUiSlider from 'nouislider';
import { Http, Headers, RequestOptions, Response } from '@angular/http';
import { formCookiesService } from '../formularios/forms_services/formCookies.service';

export interface NouiFormatter {
  to(value: number): string;
  from(value: string): number;
}
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import { distinctUntilChanged, map } from 'rxjs/operators';
import { DecimalPipe } from '@angular/common';
//import { readJson } from "./../shared/services/readJson.service";
//import { formCookiesService } from "./../shared/services/formCookies.service";
//import { getJson } from "./../shared/services/getJson.service";

declare var dataLayer: any;
declare var jQuery: any;
declare var $: any;


@Component({
  selector: 'resultado',
  templateUrl: './resultado.component.html',
  styleUrls: ['./resultado.component.scss'],
  encapsulation: ViewEncapsulation.None,
})
@Injectable()

export class ResultadoComponent implements OnInit {

  user: User = {} as User;
  formControl: FormControl;
  settingsForm: FormGroup;
  errors: Object = {};
  isSubmitting = false;
  public oferta: {};
  public result: {};
  public limitMaterias: any;
  public campus: string;
  public costos: {};
  public beca: any;
  public PagoConBeca: any;
  public AhorroMensual: any
  public PagoConBecaContado: any;
  public AhorroContado: any
  public promedio: number;
  public catcosto: string;
  public duracionAnios: any;
  public materias: number;
  public costoXmateria: number;
  private currentUserSubject = new BehaviorSubject<User>({} as User);
  public currentUser = this.currentUserSubject.asObservable().pipe(distinctUntilChanged());
  private isAuthenticatedSubject = new ReplaySubject<boolean>(1);
  public costoLista: number;
  public minimoMateriasParaBeca: number;
  public costoconBeca: number;
  public costoFinal: number;
  public nivelAnterior: string;

  public porcentajeBeca: number;
  public data: {};
  public aniosRango: any;
  public someRange: any;
  public obtenPorcentajedeBeca: {};
  public slider: any;
  public handles: any[];
  public algo: any;
  public terms: any;
  @Input() public disabled: boolean; // tslint:disable-line
  @Input() public behaviour: string;
  @Input() public connect: boolean[];
  @Input() public limit: number;
  @Input() public min: number;
  @Input() public max: number;
  @Input() public step: number;
  @Input() public format: NouiFormatter;
  @Input() public pageSteps: number;
  @Input() public config: any = {};
  @Input() public ngModel: number | number[];
  @Input() public keyboard: boolean;
  @Input() public onKeydown: any;
  @Input() public tooltips: Array<any>;
  @Output() public change: EventEmitter<any> = new EventEmitter(true);
  @Output() public update: EventEmitter<any> = new EventEmitter(true);
  @Output() public slide: EventEmitter<any> = new EventEmitter(true);
  @Output() public set: EventEmitter<any> = new EventEmitter(true);
  @Output() public start: EventEmitter<any> = new EventEmitter(true);
  @Output() public end: EventEmitter<any> = new EventEmitter(true);
  constructor(
    private el: ElementRef,
    private router: Router,
    private userService: UserService,
    private fb: FormBuilder,
    private http: Http,
    private cs: formCookiesService,
    //private res:Response,
    //private readJsonService: readJson,
    @Inject(DOCUMENT) private document,
    public datos: DatosService
  ) {

  }

  ngOnInit() {
    var ga: any;


    this.algo = "wrong";
    let inputsConfig = {
      behaviour: this.behaviour,
      connect: this.connect,
      limit: this.limit,
      start: 8,
      step: 0.1,
      pageSteps: this.pageSteps,
      keyboard: this.keyboard,
      onKeydown: this.onKeydown,
      range: this.config.range || { min: 6, max: 10 },
      tooltips: {

        from(value: string): number {
          console.log('from', value);
          return Number(value);
        },

        to(value: number): string {
          // console.log('to', value);
          // console.log('algo');
          this.promedio = value;
          return '' + value;
        }

      }
    };


    this.slider = noUiSlider.create(
      this.el.nativeElement.querySelector('.slideru'),
      Object.assign(this.config, inputsConfig)
    );






    this.promedio = 8;
    // this.beca = "25o9909";
    // this.duracionAnios = 5;
    this.materias = 5;

    // this.campus = this.datos.getCampo("campus");
    // console.log('El campus escogido');
    // console.log(this.campus);
    // this.catcosto = this.datos.getCampo("modalidad").catCosto;

    // //se tienen que habilitar en este orden las funciones
    // this.costos = this.datos.getCampoCosto(this.catcosto); // 1.-
    // this.materias = parseInt(this.costos['duracion'][Object.keys(this.costos['duracion'])[0]]["materias"]);
    // this.limitMaterias = parseInt(this.costos['duracion'][Object.keys(this.costos['duracion'])[0]]["materias"]);
    // if (this.campus == 'QRO' && this.catcosto == 'LIC') {
    //   this.materias = parseInt(this.costos['duracion'][Object.keys(this.costos['duracion'])[0]]["materias"]) * 1 + 1;
    //   this.limitMaterias = parseInt(this.costos['duracion'][Object.keys(this.costos['duracion'])[0]]["materias"]) * 1 + 1;

    // }
    // console.log(this.limitMaterias);
    // console.log('Colocando materias:' + this.materias);
    // console.log('El limite de materias es:' + this.limitMaterias);


    // this.calcular_uno();

    console.log("INICIANDO LA NUEVA FUNCION");
    // calcular_uno.bind(this);
    // var calcuno = calcular_uno.bind(this);
    // calcuno();
    this.calcular_uno();

    // this.getDuracion();                                    // 2.-
    // this.calcular();
    //3.-
    //console.log(this.terminos);
    var terminos = new Array();
    if (this.campus == 'GDL') {
      terminos[0] = '*Incluye Beca +posibilidades';
    }
    terminos[1] = '*Costos vigentes para el periodo junio-diciembre 2018';
    terminos[2] = '*El porcentaje de beca se asignará conforme al promedio general del nivel de estudios anterior al que se inscriba';


    console.log(terminos);
    this.terms = terminos;
    this.slider.on("end", () => { // Yay, cool arrow functions.
      this.calcular();
      this.enviaCRM();
    });
    console.log("Losaños:" + this.duracionAnios);
    this.duracionAnios = this.duracionAnios.toString();
    console.log(this.duracionAnios);
    console.log(this.aniosRango);
    console.log('buscando Para LEFT:' + (this.duracionAnios * 1 - 1).toString());
    console.log(this.aniosRango.includes((this.duracionAnios * 1 - 1).toString()));
    console.log('buscando Para RIGHT:' + (this.duracionAnios * 1 + 1).toString());
    console.log(this.aniosRango.includes((this.duracionAnios * 1 + 1).toString()));
    console.log('Enviando Datos...');
    console.log(this.datos.getCampo('form').frm_nombre);



    console.log("ENVIANDO CRM");
    this.enviaCRM();






  }

  enviaCRM(this) {
    if (this.datos.getCampo('modalidad').IdDynamics == "666") {
      var aspirantes = "ASPIRANTES PREPA";
      var nivel = 'P';

    } else if (this.datos.getCampo('modalidad').modalidad == "3") {
      var aspirantes = "ONLINEP";
    } else {
      var aspirantes = "ASPIRANTES LIC";
      var nivel = 'L';
    }

    // console.log('IDDYNAMICS:');
    // console.log(this.datos.getCampo('modalidad').idDynamics);
    // console.log(this.datos.getCampo('modalidad')['idDynamics']);

    //var banner_crm=document.getElementById("banner").value;

    var banner_crm = (<HTMLInputElement>document.getElementById('banner')).value;
    if (banner_crm == "") { banner_crm = 'CALCULADORA'; }
    // var url_referrer_txt = (<HTMLInputElement>document.getElementById('url_referrer')).value;
    var url_referrer_txt = window.location.href;
    this.cs.appendCookieValue('c_form_data', 'banner', banner_crm);
    var banner_cookie = this.cs.getCookieByKey('c_form_data', 'banner');

    var regcompleto_cookie = this.cs.getCookieValues('regcompleto');

    var person = {
      nivelOferta: nivel,
      Herramienta: aspirantes,
      Campana: 1,
      Origen: aspirantes,
      Banner: banner_cookie,
      Calidad: 3,
      L_Negocio: nivel,
      F_Nacim: "01/01/2000",
      Ciclo: "18-2",
      Alumno: 0,
      Estad: this.datos.getCampo('estado').crm_name,
      C_Carrera: this.datos.getCampo('modalidad').IdDynamics,
      Carrera: this.datos.getCampo('modalidad').nombre,
      banner_id: banner_cookie,
      CP: this.datos.getCampo('form').revalidacion,
      Pobla: this.datos.getCampo('modalidad').interes,
      name_form: "",
      Sexo: "M",
      nom_carrera: "Carrera",
      nom_campus: this.campusDynamics(this.datos.getCampo('campus')),
      promedioF: this.datos.getCampo('promedio'),
      Pdescuento: this.datos.getCampo('beca'),
      nivelF: "L",
      CostoUnico: 0,
      num_carrera: 0,
      costoMensual: 0,
      descuentoF: 0,
      planSelec: "4pagos",
      CostoSelec: this.datos.getCampo('costos').costoMateria['4pagos'][this.datos.getCampo('campus')],
      planRecom: "4pagos",
      CostoRecom: this.datos.getCampo('costos').costoMateria['4pagos'][this.datos.getCampo('campus')],
      envioB: 1,
      C_Carrera_WP: "",
      Direcc: "WEBCALL",
      Colon: "",
      Nombre2: "",
      email2: "",
      URLreferer: url_referrer_txt,
      debugs: "1",
      gclid_field: "",
      userId_field: "",
      CID: this.datos.getCampo('form').CID,
      costoMateria: this.datos.getCampo('costos').costoMateria['4pagos'][this.datos.getCampo('campus')],
      email_unitec: "",
      otraConsulta: (this.datos.getCampo("envio_crm") != "1") ? 0 : 1,
      SERVER_URL: "",
      frm_LadaLenght: "",
      frm_LadaAllow: "",
      alum_2_2: "",
      Nombre: this.datos.getCampo('form').frm_nombre,
      Ap_Pat: this.datos.getCampo('form').frm_apaterno,
      Ap_Mat: this.datos.getCampo('form').frm_amaterno,
      email: this.datos.getCampo('form').frm_correo,
      informacion_2: 1,
      Estado: this.datos.getCampo('estado').crm_name,
      masculino: "M",
      TipoTel: "cel_calc",
      Telef: this.datos.getCampo('form').frm_celular,
      periodo_interes_sel: "18-2",
      politica: "on",
      promedio: this.datos.getCampo('promedio'),
      programa: this.datos.getCampo('modalidad').IdDynamics,
      materias: this.datos.getCampo('materias'),
      hubspotutk: "",
      padretutor: "no",
      termino: "",
      pago_mensual_beca: this.datos.getCampo('costos').costoMateria['4pagos'][this.datos.getCampo('campus')],
    };
    //termina Json
    console.log(person);
    //var arr = $.map(person, function(el) { return el });

    // console.log(arr);
    // var link = '//www.unitec.mx/desk/wp-content/themes/temaunitec/calculadora/v2_calc/almacen1.php';
    console.log("Cambiando el link");
    var link = (this.datos.getCampo("envio_crm") != "1") ? '/wp-content/themes/temaunitec/calculadora/v2_calc/almacen1.php' : '/wp-content/themes/temaunitec/calculadora/v2_calc/otra_consulta.php';
    //let headers = new Headers({ 'Content-Type': 'application/json' });
    //let options = new RequestOptions({ headers: headers });
    //let body = JSON.stringify(person);
    //this.http.post(link, body, options ).map((res: Response) => res.json());


    //console.log(value.message);
    //let headers = new Headers({ 'Content-Type': 'application/json'});
    // let body = JSON.stringify(person);
    // this.http.post(link, arr)
    //             .subscribe(
    //                 () => {alert("Success")}, //For Success Response
    //                 err => {console.error(err)} //For Error Response
    //             );   
    var objthis = this;
    jQuery.ajax({
      type: "POST",
      url: link,
      data: person,
      // dataType: "JSON",
      success: function (msg) {
        objthis.datos.guardaUsuario('envio_crm', "1");

        console.log("Datos Enviados");
        console.log(msg);
        // Proceso tag manager
        if (msg.tipo == "nuevo") {
          dataLayer.push({
            'event': 'CALCULADORA_REGISTRO'
          });

          //ga(function(tracker) { clientId = tracker.get('clientId');});
          dataLayer.push({
            //'leadId': clientId, //dato dinámico
            //  'CDClientID':clientId,
            'origenLead': 'frmCalcu', //dato dinámico
            'isAlumn': '0', //dato dinámico
            'ingress': person.CP, //dato dinámico
            'state': person.Estado, //dato dinámico
            'levelStudies': person.L_Negocio, //dato dinámico
            'Period': person.Ciclo, //dato dinámico
            'carrera': person.Carrera, //dato dinámico
            'campus': person.nom_campus, //dato dinámico
            'date': new Date(), //dato dinámico
            'event': 'leadGenerationCalcu' //dato estático
          });

        } else {
          dataLayer.push({
            'event': 'CALCULADORA_DUPLICADO'
          });
          dataLayer.push({
            'url_value': 'formularioCalculadora/Duplicado', //dato dinÃ¡mico
            'event': 'StepForm' //dato estÃ¡tico
          });
          dataLayer.push({
            'event': 'IX_web_duplicado'
          });

        }
        //End Proceso tag manager

      },
      error: function (xhr, textStatus, error) {
        console.log(xhr.statusText);
        console.log(textStatus);
        console.log(error);
      }
    });
  }

  campusDynamics(campus) {
    switch (campus) {
      case "ATZ":
        campus = "ATIZAPAN";
        break;
      case "REY":
        campus = "LOS REYES";
        break;
      case "MAR":
        campus = "MARINA";
        break;
      case "ECA":
        campus = "ECATEPEC";
        break;
      case "SUR":
        campus = "SUR";
        break;
      case "CUI":
        campus = "CUITLAHUAC";
        break;
      case "LEO":
        campus = "LEON";
        break;
      case "REY":
        campus = "Los Reyes";
        break;
      case "TOL":
        campus = "TOLUCA";
        break;
      case "GDL":
        campus = "GUADALAJARA";
        break;
      case "ONL":
        campus = "EN LINEA";
        break
      case "QRO":
        campus = "QUERETARO";
        break
      default:
        campus = "MARINA";
    }
    return campus;
  }

  // getDuracion() {
  //   console.log('______FUNCION GETDURACION_____');
  //   var materias = this.datos.getCampo('materias');
  //   var campus = this.datos.getCampo('campus');
  //   var catcosto = this.datos.getCampo('modalidad').catCosto;
  //   var terminando = void 0;
  //   var valoranios = void 0;
  //   var materiasanios = void 0;
  //   //let aniosRango:any;
  //   var aniosRango = new Array();
  //   var materiasMaximo = void 0;
  //   var lasMaterias = materias;
  //   var costos = this.datos.getCampo('costos');
  //   console.log("costos['duracion']");
  //   console.log(costos['duracion']);

  //   if (campus == 'QRO' && catcosto == 'LIC') {
  //     Object.entries(costos['duracion']).forEach(function (key, value) {
  //       console.log("Con " + key[1]['materias'] * 1 + 1 + " materias terminas en " + key[0] + " años");
  //       console.log(key[0]);
  //       console.log(key[1]['materias'] * 1 + 1);

  //       valoranios = key[0];
  //       materiasanios = parseInt(key[1]['materias']) + 1;
  //       console.log("lasmat: " + lasMaterias);
  //       if (materiasanios == lasMaterias) {
  //         console.log('Estaras terminando en ' + valoranios);
  //         terminando = valoranios;
  //       }

  //       aniosRango.push(valoranios);
  //     });
  //   } else {
  //     Object.entries(costos['duracion']).forEach(function (key, value) {
  //       console.log("Con " + key[0] + " materias terminas en " + key[1]['materias'] + " años");
  //       console.log(key[0]);
  //       console.log(key[1]['materias']);

  //       valoranios = key[0];
  //       materiasanios = key[1]['materias'];
  //       console.log("lasmat: " + lasMaterias);
  //       if (materiasanios == lasMaterias) {
  //         console.log('Estaras terminando en ' + valoranios + " Mateiras anios" + materiasanios);
  //         terminando = valoranios;
  //       }

  //       aniosRango.push(valoranios);

  //     });
  //   }

  //   this.aniosRango = aniosRango;
  //   console.log("temrinando" + terminando);
  //   console.log(aniosRango);
  //   var durahtml = "";
  //   var i = "";
  //   jQuery.each(aniosRango, function (index, value) {
  //     if (durahtml == "") { var sel = "selected" }
  //     durahtml += '<button class="bl btn-dual ' + sel + '" data-termino="' + value + '">' + value + ' años</button>';
  //     i = value;
  //   });

  //   if (aniosRango.length < 2) {
  //     durahtml = '<div class=" alert alert-primary "> ' + i + ' Años</div>';
  //   }
  //   //guardaUsuario('materias',jQuery('.btn-dual.selected'));
  //   jQuery('.aniosduracion').html(durahtml);



  //   this.duracionAnios = terminando;
  //   console.log("duracionAnios:");
  //   console.log(this.duracionAnios);
  //   if (!this.duracionAnios) {
  //     console.log('No hay duracion para las materias escogidas');
  //   }
  //   this.calcular();

  // }


  // calcular() {
  //   this.promedio = Math.round(this.slider.get() * 10) / 10;
  //   //Obtiene el costo por materia 
  //   // Costo de la materia X Costo de materia en campus
  //   console.log("el campus" + this.campus);
  //   console.log(this.costos);
  //   console.log('Duraciones disponibles');

  //   // console.log(this.materias);

  //   // if (this.materias != materiasanios){

  //   // }else{
  //   //   this.duracionAnios = valoranios;
  //   //   this.materias = materiasanios;
  //   // }


  //   this.costoXmateria = this.costos["costoMateria"]["1pago"][this.campus]; //COSTO POR MATERIA DEL CAMPUS SELECCIONADO
  //   this.costoLista = this.materias * this.costoXmateria;

  //   //Detecta a partir de cuanto se crea la beca 
  //   this.minimoMateriasParaBeca = this.getMinimoMatBeca(this.catcosto);
  //   console.log('El minimo para beca es:' + this.minimoMateriasParaBeca);

  //   if (this.materias >= this.minimoMateriasParaBeca) {
  //     console.log('si aplica beca');
  //     this.beca = this.getBeca(this.promedio, this.catcosto);

  //   } else {
  //     this.beca = 0;
  //     console.log('no aplica beca');

  //   }
  //   console.log('tomando el costo por materia');

  //   /*PAGOS MENSUALES 4 APGOS */
  //   let costoXmateriaMens = this.costos['costoMateria']['4pagos'][this.campus]; //TODO: revisar si hay nodo de campus, sino usar todos
  //   let PagoSinBecaMensual = costoXmateriaMens * this.materias;
  //   console.log('Pago sin descuento' + PagoSinBecaMensual);
  //   this.PagoConBeca = Math.round(PagoSinBecaMensual - ((this.beca * PagoSinBecaMensual) / 100));
  //   let pagoConSR = PagoSinBecaMensual - ((this.beca * PagoSinBecaMensual) / 100);
  //   console.log('con el ' + this.beca + " de " + PagoSinBecaMensual + " Queda: " + pagoConSR);
  //   this.AhorroMensual = Math.round(PagoSinBecaMensual - pagoConSR);
  //   console.log('Te ahorras:' + this.AhorroMensual);

  //   /* PAGOS DE CONTADO*/
  //   let costoXmateriaContado = this.costos['costoMateria']['1pago'][this.campus]; //TODO: revisar si hay nodo de campus, sino usar todos
  //   let PagoSinBecaContado = costoXmateriaContado * this.materias;
  //   console.log('Pago sin descuento' + PagoSinBecaContado);
  //   this.PagoConBecaContado = Math.round(PagoSinBecaContado - ((this.beca * PagoSinBecaContado) / 100));
  //   let PagoConBecaContadoSR = PagoSinBecaContado - ((this.beca * PagoSinBecaContado) / 100);
  //   console.log('con el ' + this.beca + " de " + PagoSinBecaContado + " Queda: " + PagoConBecaContadoSR);
  //   this.AhorroContado = Math.round(PagoSinBecaContado - PagoConBecaContadoSR);
  //   console.log('Te ahorras:' + this.AhorroContado);

  //   this.datos.guardaUsuario('costos', this.costos);
  //   this.datos.guardaUsuario('materias', this.materias);

  //   this.nivelAnterior = this.getNivelAnterior(this.datos.getCampo('modalidad').interes);

  // }

  getNivelAnterior(nivel) {


    var nivelAnt = "";

    switch (nivel) {
      case '1': case '2': case '3': case '4':
        nivelAnt = 'Preparatoria'
        break;
      case '5': case '6':
        nivelAnt = 'Licenciatura'
        break;
      case '7':
        nivelAnt = 'Secundaria'
        break;
      default:
        nivelAnt = 'Licenciatura'
        break;
    }
    return nivelAnt;

  }

  // getBeca(promedio, catCosto) {
  //   console.log("obteniendo la beca");
  //   let setbeca = this.datos.getCampoBeca(catCosto);

  //   console.log('buscando beca de campus' + this.campus);
  //   let becacampus = setbeca;
  //   console.log(becacampus);
  //   let RangoBecas = "";
  //   //console.log(becacampus['LEO']);
  //   if (becacampus[this.campus]) {
  //     console.log('Si eocntro un nodo con' + this.campus);
  //     RangoBecas = becacampus[this.campus];


  //   } else {
  //     console.log('NO encontro un nodo' + this.campus + ' y usara el de TODOS');
  //     RangoBecas = becacampus['TODOS'];
  //   }


  //   console.log(RangoBecas);
  //   let becaFinal: any;
  //   // for (let entry of RangoBecas) {
  //   //   console.log("aa"+entry); // 1, "string", false
  //   // }
  //   console.log('buscando este promedio' + this.promedio);
  //   Object.entries(RangoBecas).forEach(function (key, value) {
  //     if (key[1]['promedio'][0] <= promedio && key[1]['promedio'][1] >= promedio) {
  //       console.log("entro en este porcentaje de beaca");
  //       console.log(key[1]['promedio'][0] + " " + key[1]['promedio'][1] + "con beca de " + key[1]['beca']);
  //       becaFinal = key[1]['beca'];
  //     } else {
  //       console.log('no encuentra beca');
  //       //console.log('No es de este rango porque ' + key[1]['promedio'][0] +"no es menor o igual que"+this.promedio +"y " );
  //     }
  //   });
  //   if (becaFinal) { becaFinal = becaFinal; } else { becaFinal = 0; }

  //   console.log('La beca fue de' + becaFinal);
  //   //return becacampus[''];

  //   this.datos.guardaUsuario('beca', becaFinal);
  //   this.datos.guardaUsuario('promedio', promedio);

  //   if (this.campus == 'GDL') {
  //     if (
  //       this.datos.getCampo('modalidad').IdDynamics == "787" ||
  //       this.datos.getCampo('modalidad').IdDynamics == "883" ||
  //       this.datos.getCampo('modalidad').IdDynamics == "884" ||
  //       this.datos.getCampo('modalidad').IdDynamics == "588"
  //     ) {
  //       console.log('No aplica beca GDL Fundador');
  //     } else {
  //       console.log('venia ' + becaFinal);
  //       becaFinal = parseInt(becaFinal) + 20;
  //       console.log('Temrino con beca de' + becaFinal);
  //     }

  //   }

  //   return becaFinal;

  // }

  getMinimoMatBeca(catCosto) {
    var beca;
    var minmaterias;
    switch (catCosto) {

      case "NUTRI":
      case "BLUEM":
      case "GASTRO":
      case "FISIO":
      case "TURI":
      case "DENT":
      case "ENFER":
      case "PREPA":
      case "ING":
      case "ESP":
      case "ESPD":
      case "LIC":
      case "LICPSICOLOG":
      case "LICPEDAG":
        minmaterias = 5;
        break


      case "LICEJEC":
      case "INGEJEC":
      case "LICON":
      case "INGON":
        minmaterias = 4;
        break;

      case "DIP":
      case "DIPON":
      case "POS":
      case "POSON":
      case "POSONCUAT":
      case "POSCUAT":
      case "POSCUATS":
        minmaterias = 3
        break;
      default:
        minmaterias = 5;
    }
    return minmaterias;
  }

  duracion(tipo) {

    var getUsuario = this.datos.getUsuario.bind(this.datos);
    var guardaUsuario = this.datos.guardaUsuario.bind(this.datos);
    var calcular = this.calcular.bind(this);

    console.log(this.aniosRango);
    console.log(this.duracionAnios);
    console.log(this.aniosRango.includes((this.duracionAnios * 1 + 1).toString()));
    // console.log(this.duracionAnios - 1);
    // console.log('materiasduracion');
    // console.log(this.costos['duracion']);
    // if (tipo == 'menos') {
    //   if (this.aniosRango.includes((this.duracionAnios * 1 - 1).toString())) {
    //     this.duracionAnios = this.duracionAnios * 1 - 1;
    //     if (this.campus == 'QRO' && this.catcosto == 'LIC') {
    //       this.materias = Number(this.costos['duracion'][this.duracionAnios]['materias']) * 1 + 1;
    //     } else {
    //       this.materias = Number(this.costos['duracion'][this.duracionAnios]['materias']);
    //     }

    //     console.log('Si se encuentra')
    //   } else {
    //     console.log('no se encuentra');
    //   }

    // } else {
    //   if (this.aniosRango.includes((this.duracionAnios * 1 + 1).toString())) {
    //     this.duracionAnios = this.duracionAnios * 1 + 1;
    //     this.materias = Number(this.costos['duracion'][this.duracionAnios]['materias']);
    //     if (this.campus == 'QRO' && this.catcosto == 'LIC') {
    //       this.materias = Number(this.costos['duracion'][this.duracionAnios]['materias']) + 1;
    //     }
    //   }
    // }

    // this.calcular();

    var mat = parseInt(getUsuario().materias);
    jQuery('.btn-dual').removeClass("selected");
    this.costos = this.datos.getCampo('costos');

    if (tipo == 'menos') {
      if (this.aniosRango.includes((this.duracionAnios * 1 - 1).toString())) {
        this.duracionAnios = this.duracionAnios * 1 - 1;
        // console.log("ACTUALIZANDO A: ");
        // console.log(Number(this.costos['duracion'][this.duracionAnios]['materias']));
        this.materias = Number(this.costos['duracion'][this.duracionAnios]['materias']);
        guardaUsuario('materias', this.materias);
        calcular();
      }

    } else {

      if (this.aniosRango.includes((this.duracionAnios * 1 + 1).toString())) {
        this.duracionAnios = this.duracionAnios * 1 + 1;
        this.materias = Number(this.costos['duracion'][this.duracionAnios]['materias']);
        guardaUsuario('materias', this.materias);
        calcular();
      }


    }

    jQuery('.btn-dual[data-materias="' + getUsuario().materias + '"]').addClass('selected');
  }

  getMaterias(tipo) {
    var getUsuario = this.datos.getUsuario.bind(this.datos);
    var guardaUsuario = this.datos.guardaUsuario.bind(this.datos);
    var calcular = this.calcular.bind(this);
    var getDuracion = this.getDuracion.bind(this);
    // if (tipo == 'menos') {
    //   if (this.materias > 1) {
    //     this.materias = (this.materias * 1) - 1;
    //   }
    // } else {
    //   if (this.materias < this.limitMaterias) {
    //     this.materias = (this.materias * 1) + 1;
    //   }

    // }
    // this.getDuracion();
    // this.calcular();

    var mat = parseInt(getUsuario().materias);
    jQuery('.btn-dual').removeClass("selected");
    console.log(this.duracionAnios);

    if (tipo == 'menos') {
      if (mat <= 1) {
        //jQuery(this).css({ "visibility": "hidden" });       //esconde el control de menos mat 
      } else {
        //jQuery('.mat-ctrl').css({ "visibility": "visible" })
        guardaUsuario('materias', mat - 1);
        jQuery('.rs-materias').html(getUsuario().materias);
        this.materias = getUsuario().materias;
        getDuracion();
        // calcular();
      }

    } else {
      if (mat >= getUsuario().limitMat) {
        //jQuery(this).css({ "visibility": "hidden" });       //esconde el control de menos mat 
      } else {
        //jQuery('.mat-ctrl').css({ "visibility": "visible" })
        guardaUsuario('materias', mat + 1);
        jQuery('.rs-materias').html(getUsuario().materias);
        this.materias = getUsuario().materias;
        getDuracion();
        // calcular();
      }
    }

    jQuery('.btn-dual[data-materias="' + getUsuario().materias + '"]').addClass('selected');
  }






  setCampus(campus) {
    this.datos.guardaUsuario('campus', campus);
    this.router.navigateByUrl('/resultados');
  }






  getModalidadPorSubnivel(subNivelInteres) {
    let modalidad;
    if (subNivelInteres == 1 || subNivelInteres == 2 || subNivelInteres == 5 || subNivelInteres == 7) {
      //Presencial
      modalidad = 1;
    } else if (subNivelInteres == 3) {
      //ejecutiva
      modalidad = 2;
    } else if (subNivelInteres == 4 || subNivelInteres == 6) {
      modalidad = 3;
    }
    return modalidad;
  }


  logout() {
    this.userService.purgeAuth();
    this.router.navigateByUrl('/');
  }

  setEstado(item) {
    console.log(item);
    this.user.estado = item;
    Object.assign(this.user, item);
    //Object.assign(this.user, item);
    this.currentUserSubject.value;
    console.log(this.user);
    this.router.navigateByUrl('/linea');
  }
  submitForm() {
    this.isSubmitting = true;

    // update the model
    this.updateUser(this.settingsForm.value);

    this.userService
      .update(this.user)
      .subscribe(
        updatedUser => this.router.navigateByUrl('/profile/' + updatedUser.username),
        err => {
          this.errors = err;
          this.isSubmitting = false;
        }
      );
  }

  updateUser(values: Object) {
    Object.assign(this.user, values);
  }

  calcular_uno = () => {
    var datos = this.datos;
    var getCampo = datos.getCampo.bind(this.datos);
    var getCampoCosto = datos.getCampoCosto.bind(datos);
    var guardaUsuario = datos.guardaUsuario;
    var getDuracion = this.getDuracion.bind(this);
    var i;

    console.log("_______FUNCION CALCULAR UNO_____");
    /***Funciones por defecto en resutlado.component.html primera carga ***/
    var campus = getCampo("campus");
    console.log('El campus escogido');
    console.log(campus);
    var catcosto = getCampo("modalidad").catCosto;
    console.log(catcosto);
    //se tienen que habilitar en este orden las funciones
    var costos = getCampoCosto(catcosto); // 1.-
    guardaUsuario('costos', costos);
    console.log("costos:");
    console.log(costos);
    var materias = parseInt(costos['duracion'][Object.keys(costos['duracion'])[0]]["materias"]);
    guardaUsuario('materias', materias);
    var limitMaterias = parseInt(costos['duracion'][Object.keys(costos['duracion'])[0]]["materias"]);
    if (campus == 'QRO' && catcosto == 'LIC') {
      materias = parseInt(costos['duracion'][Object.keys(costos['duracion'])[0]]["materias"]) * 1 + 1;
      limitMaterias = parseInt(costos['duracion'][Object.keys(costos['duracion'])[0]]["materias"]) * 1 + 1;

    }
    this.limitMaterias = limitMaterias;
    console.log(limitMaterias);
    guardaUsuario('limitMat', limitMaterias);
    console.log('Colocando materias:' + materias);
    console.log('El limite de materias es:' + limitMaterias);
    //terminan funciones de primera carga 

    this.materias = materias;

    var btnmaterias = "";
    for (i = 1; i <= limitMaterias; i++) {
      if (i == limitMaterias) { var sel = "selected"; }
      btnmaterias += '<div class="btn-circ ' + sel + '">' + i + '</div>';
    }

    if (limitMaterias > 7) {
      btnmaterias = '<div class=" alert alert-primary ' + sel + '"> ' + i + ' materias</div>';
    }

    jQuery('.mat-circ').html(btnmaterias);

    // if(getCampo('materias')){

    // }else{
    //     guardaUsuario('materias',materias);
    // }
    getDuracion();
  }

  getDuracion() {
    var datos = this.datos;
    var getCampo = datos.getCampo.bind(this.datos);
    var getCampoCosto = datos.getCampoCosto.bind(datos);
    var guardaUsuario = datos.guardaUsuario;
    var duracionAnios = this.duracionAnios;
    // this.duracionAnios = duracionAnios;
    var calcular = this.calcular.bind(this);

    console.log('______FUNCION GETDURACION_____');
    var materias = getCampo('materias');
    var campus = getCampo('campus');
    var catcosto = getCampo('modalidad').catCosto;
    var terminando = void 0;
    var valoranios = void 0;
    var materiasanios = void 0;
    //let aniosRango:any;
    var aniosRango = new Array();
    var materiasMaximo = void 0;
    var lasMaterias = materias;
    var costos = getCampo('costos');
    console.log("costos['duracion']");
    console.log(costos['duracion']);


    Object.entries(costos['duracion']).forEach(function (key, value) {
      console.log("Con " + key[0] + " materias terminas en " + key[1]['materias'] + " años");
      console.log(key[0]);
      console.log(key[1]['materias']);

      valoranios = key[0];
      materiasanios = key[1]['materias'];
      console.log("lasmat: " + lasMaterias);
      if (materiasanios == lasMaterias) {
        console.log('Estaras terminando en ' + valoranios + " Mateiras anios" + materiasanios);
        terminando = valoranios;
      }

      aniosRango.push(valoranios);

    });


    this.aniosRango = aniosRango;
    console.log("temrinando" + terminando);
    console.log(aniosRango);
    var durahtml = "";
    var i = 0;


    // jQuery.each(aniosRango, function (index, value) {
    //     if (durahtml == "") { var sel = "selected" }
    //     durahtml += '<button class="bl btn-dual ' + sel + '" data-termino="' + value + '">' + value + ' años</button>';
    //     i = value;
    // });

    // if (aniosRango.length < 2) {
    //     durahtml = '<div class=" alert alert-primary "> ' + i + ' Años</div>';
    // }


    //var uno="";
    var sel = "";
    console.log('implementacion de botones');
    //impresion de botones 
    console.log(costos['duracion']);
    jQuery.each(costos['duracion'], function (index, value) {
      console.log("index: " + index + " Value:" + value.materias);
      if (durahtml == "") { sel = "selected"; } else { sel = ""; }
      if (index == '4m') {
        durahtml += '<div class=" alert alert-primary "> 4 Meses</div>';
      } else {
        durahtml += '<button class="bl ml2 btn-dual ' + sel + '"  data-termino="' + index + '" data-materias="' + value.materias + '">' + index + ' años</button>';

      }
      // if(i==1){
      //     durahtml = '<div class=" alert alert-primary "> solo un valor Años</div>';
      //     alert('una');
      // }
      //uno = index; 
      i++;
    });





    //guardaUsuario('materias',jQuery('.btn-dual.selected'));
    jQuery('.aniosduracion').html(durahtml);









    duracionAnios = terminando;
    this.duracionAnios = duracionAnios;
    console.log("duracionAnios:");
    console.log(duracionAnios);
    if (!duracionAnios) {
      console.log('No hay duracion para las materias escogidas');
    }
    calcular();

  }


  calcular() {
    var datos = this.datos;
    var getCampo = datos.getCampo.bind(this.datos);
    var getCampoCosto = datos.getCampoCosto.bind(datos);
    var guardaUsuario = datos.guardaUsuario;
    var getMinimoMatBeca = this.getMinimoMatBeca;
    var beca = this.beca;
    var getBeca = this.getBeca.bind(this);
    var getNivelAnterior = this.getNivelAnterior;

    var campus = getCampo('campus');
    var costos = getCampo('costos');
    var catcosto = getCampo('modalidad').catCosto;
    var materias = getCampo('materias');
    // var promedio = Math.round(jQuery('#range').val() * 10) / 10;
    var promedio = Math.round(this.slider.get() * 10) / 10;

    if (campus == undefined || campus == null || campus == "" || campus == "0") {
      campus = jQuery('#render_campus').attr('data-preselected');
    }

    //Obtiene el costo por materia 
    // Costo de la materia X Costo de materia en campus
    console.log("el campus" + campus);
    console.log(costos);
    console.log('Duraciones disponibles');

    // console.log(this.materias);

    // if (this.materias != materiasanios){

    // }else{
    //   this.duracionAnios = valoranios;
    //   this.materias = materiasanios;
    // }

    var costoXmateria = costos["costoMateria"]["1pago"][campus]; //COSTO POR MATERIA DEL CAMPUS SELECCIONADO
    var costoLista = materias * costoXmateria;

    //Detecta a partir de cuanto se crea la beca 
    var minimoMateriasParaBeca = getMinimoMatBeca(catcosto);
    console.log('El minimo para beca es:' + minimoMateriasParaBeca + " y tengo:" + materias);

    if (materias >= minimoMateriasParaBeca) {
      console.log('si aplica beca');
      beca = getBeca(promedio, catcosto);

    } else {
      beca = 0;
      console.log('no aplica beca');

    }
    this.beca = beca;
    console.log('tomando el costo por materia');

    /*PAGOS MENSUALES 4 APGOS */
    var costoXmateriaMens = costos['costoMateria']['4pagos'][campus]; //TODO: revisar si hay nodo de campus, sino usar todos
    var PagoSinBecaMensual = costoXmateriaMens * materias;
    console.log('Pago sin descuento' + PagoSinBecaMensual);
    var PagoConBeca = Math.round(PagoSinBecaMensual - ((beca * PagoSinBecaMensual) / 100));
    var pagoConSR = PagoSinBecaMensual - ((beca * PagoSinBecaMensual) / 100);




    jQuery('.rs-beca').text(beca + "%*");
    console.log('con el ' + beca + " de " + PagoSinBecaMensual + " Queda: " + pagoConSR);
    var AhorroMensual = Math.round(PagoSinBecaMensual - pagoConSR);
    console.log('Te ahorras:' + AhorroMensual);

    // if(jQuery('.btn-dual-y.selected').text().toLowerCase()=='mensual'){
    //         jQuery('.rs-pg-final').text("$"+getCampo('mensual').format());
    //         jQuery('.rs-ahorro-mns').text("$"+getCampo('mensualAhorro').format());
    //     }else{
    //         jQuery('.rs-pg-final').text("$"+getCampo('contado').format());
    //         jQuery('.rs-ahorro-mns').text("$"+getCampo('contadoAhorro').format());

    //     }


    /* PAGOS DE CONTADO*/
    var costoXmateriaContado = costos['costoMateria']['1pago'][campus]; //TODO: revisar si hay nodo de campus, sino usar todos
    var PagoSinBecaContado = costoXmateriaContado * materias;
    console.log('Pago sin descuento' + PagoSinBecaContado);
    var PagoConBecaContado = Math.round(PagoSinBecaContado - ((beca * PagoSinBecaContado) / 100));
    var PagoConBecaContadoSR = PagoSinBecaContado - ((beca * PagoSinBecaContado) / 100);
    console.log('con el ' + beca + " de " + PagoSinBecaContado + " Queda: " + PagoConBecaContadoSR);
    var AhorroContado = Math.round(PagoSinBecaContado - PagoConBecaContadoSR);
    console.log('Te ahorras:' + AhorroContado);

    guardaUsuario('costos', costos);
    guardaUsuario('materias', materias);
    guardaUsuario('mensual', pagoConSR);
    guardaUsuario('mensualAhorro', AhorroMensual);

    guardaUsuario('contado', PagoConBecaContadoSR);
    guardaUsuario('contadoAhorro', AhorroContado)

    // if (jQuery('.btn-dual-y.selected').text().toLowerCase() == "mensual") {
    //     jQuery('.rs-pg-final').text("$" + (Math.round(pagoConSR * 100) / 100).format());
    //     jQuery('.rs-ahorro-mns').text("$" + (Math.round(AhorroMensual * 100) / 100).format());
    // } else {
    //     jQuery('.rs-pg-final').text("$" + (Math.round(PagoConBecaContadoSR * 100) / 100).format());
    //     jQuery('.rs-ahorro-mns').text("$" + (Math.round(AhorroContado * 100) / 100).format());
    // }

    this.AhorroMensual = AhorroMensual;
    this.AhorroContado = AhorroContado;
    this.PagoConBeca = PagoConBeca;
    this.PagoConBecaContado = PagoConBecaContado;

    var nivelAnterior = getNivelAnterior(getCampo('modalidad').interes.toString());
    this.nivelAnterior = nivelAnterior;
    // jQuery('.rs-btn-mensual').click();

    // var eligeCarrera = getCampo('contado');
    // if (jQuery('body').hasClass('inicio')==false) {
    //      jQuery('.rs-btn-mensual').click();
    //      jQuery('.btn-dual').last().click();
    //  }


  }


  getBeca(promedio, catcosto) {
    var datos = this.datos;
    var getCampo = datos.getCampo.bind(this.datos);
    var getCampoCosto = datos.getCampoCosto.bind(datos);
    var guardaUsuario = datos.guardaUsuario;
    var getCampoBeca = datos.getCampoBeca.bind(datos);


    console.log("obteniendo la beca");
    var setbeca = getCampoBeca(catcosto);
    var campus = getCampo('campus');
    console.log('buscando beca de campus' + campus);
    var becacampus = setbeca;
    console.log(becacampus);
    var RangoBecas = "";
    //console.log(becacampus['LEO']);
    if (becacampus[campus]) {
      console.log('Si eocntro un nodo con' + campus);
      RangoBecas = becacampus[campus];


    } else {
      console.log('NO encontro un nodo' + campus + ' y usara el de TODOS');
      RangoBecas = becacampus['TODOS'];
    }


    console.log(RangoBecas);
    var becaFinal;
    // for (let entry of RangoBecas) {
    //   console.log("aa"+entry); // 1, "string", false
    // }
    console.log('buscando este promedio' + promedio);
    Object.entries(RangoBecas).forEach(function (key, value) {
      console.log(key[1]['promedio'][0] + "<=" + promedio + "&&" + key[1]['promedio'][1] + ">=" + promedio);
      if (key[1]['promedio'][0] <= promedio && key[1]['promedio'][1] >= promedio) {
        console.log("SI_ENTRA_BECA_entro en este porcentaje de beaca");
        console.log(key[1]['promedio'][0] + " " + key[1]['promedio'][1] + "con beca de " + key[1]['beca']);
        becaFinal = key[1]['beca'];

      } else {
        console.log('no encuentra beca');
        //console.log('No es de este rango porque ' + key[1]['promedio'][0] +"no es menor o igual que"+this.promedio +"y " );
      }
    });

    if (becaFinal) { becaFinal = becaFinal; } else { becaFinal = 0; }

    console.log('La beca fue de' + becaFinal);
    //return becacampus[''];

    guardaUsuario('beca', becaFinal);
    guardaUsuario('promedio', promedio);

    if (campus == 'GDL') {
      if (
        getCampo('modalidad').IdDynamics == "787" ||
        getCampo('modalidad').IdDynamics == "883" ||
        getCampo('modalidad').IdDynamics == "884" ||
        getCampo('modalidad').IdDynamics == "588"
      ) {
        console.log('No aplica beca GDL Fundador');
      } else {
        console.log('venia ' + becaFinal);
        becaFinal = parseInt(becaFinal) + 20;
        console.log('Temrino con beca de' + becaFinal);
      }

    }

    return becaFinal;

  }


}

// Number.prototype.format = function (n, x) {
//   var re = '\\d(?=(\\d{' + (x || 3) + '})+' + (n > 0 ? '\\.' : '$') + ')';
//   return this.toFixed(Math.max(0, ~~n)).replace(new RegExp(re, 'g'), '$&,');
// };

// function calcular_uno() {
//   var getCampo = this.datos.getCampo;
//   var getCampoCosto = this.datos.getCampoCosto;
//   var guardaUsuario = this.datos.guardaUsuario;
//   var getDuracion = this.getDuracion;
//   var i;
//   console.log("_______FUNCION CALCULAR UNO_____");
//   /***Funciones por defecto en resutlado.component.html primera carga ***/
//   var campus = getCampo("campus");
//   console.log('El campus escogido');
//   console.log(campus);
//   var catcosto = getCampo("modalidad").catCosto;
//   console.log(catcosto);
//   //se tienen que habilitar en este orden las funciones
//   var costos = getCampoCosto(catcosto); // 1.-
//   guardaUsuario('costos', costos);
//   console.log("costos:");
//   console.log(costos);
//   var materias = parseInt(costos['duracion'][Object.keys(costos['duracion'])[0]]["materias"]);
//   guardaUsuario('materias', materias);
//   var limitMaterias = parseInt(costos['duracion'][Object.keys(costos['duracion'])[0]]["materias"]);
//   if (campus == 'QRO' && catcosto == 'LIC') {
//       materias = parseInt(costos['duracion'][Object.keys(costos['duracion'])[0]]["materias"]) * 1 + 1;
//       limitMaterias = parseInt(costos['duracion'][Object.keys(costos['duracion'])[0]]["materias"]) * 1 + 1;

//   }
//   console.log(limitMaterias);
//   console.log('Colocando materias:' + materias);
//   console.log('El limite de materias es:' + limitMaterias);
//   //terminan funciones de primera carga 


//   var btnmaterias = "";
//   for (i = 1; i <= limitMaterias; i++) {
//       if (i == limitMaterias) { var sel = "selected"; }
//       btnmaterias += '<div class="btn-circ ' + sel + '">' + i + '</div>';
//   }

//   if (limitMaterias > 7) {
//       btnmaterias = '<div class=" alert alert-primary ' + sel + '"> ' + i + ' materias</div>';
//   }

//   jQuery('.mat-circ').html(btnmaterias);

//   // if(getCampo('materias')){

//   // }else{
//   //     guardaUsuario('materias',materias);
//   // }
//   getDuracion();
// }