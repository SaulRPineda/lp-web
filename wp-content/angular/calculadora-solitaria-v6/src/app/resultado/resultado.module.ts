import { ModuleWithProviders, NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { NouisliderModule } from 'ng2-nouislider';

import { ResultadoComponent } from './../resultado/resultado.component';
import { AuthGuard, SharedModule } from '../shared';
import { resultadoRouting } from './resultado.router';

// const ResultadoRouting: ModuleWithProviders = RouterModule.forChild([
//   {
//     path: 'resultado',
//     component: ResultadoComponent,
//     canActivate: [AuthGuard]
//   }
// ]);

@NgModule({ 
  imports: [
    SharedModule,
    resultadoRouting,
    NouisliderModule
  ],
  declarations: [
    ResultadoComponent
  ]
})
export class ResultadoModule { }
