import { NgModule, ModuleWithProviders } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { CommonModule } from '@angular/common';
import { ResultadoComponent } from './resultado.component';

const routes: Routes = [
  { path: '', component: ResultadoComponent }
];

export const resultadoRouting: ModuleWithProviders = RouterModule.forChild(routes);