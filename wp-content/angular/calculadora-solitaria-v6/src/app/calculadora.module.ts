import { NgModule, ModuleWithProviders } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { CommonModule } from '@angular/common';
import { EstadoComponent } from './estado/estado.component';
import { LineaComponent } from './linea/linea.component';
import { OfertaComponent } from './oferta/oferta.component';

const routes: Routes = [
  { path: '', component: EstadoComponent }
//   { path: 'linea', component: LineaComponent },
//   { path: 'oferta', component: OfertaComponent }
];

export const routing: ModuleWithProviders = RouterModule.forChild(routes);

// @NgModule({
//   imports: [CommonModule, RouterModule.forChild(routes)],
//   declarations: [EstadoComponent, LineaComponent, OfertaComponent]
// })
// export class CalculadoraModule {}