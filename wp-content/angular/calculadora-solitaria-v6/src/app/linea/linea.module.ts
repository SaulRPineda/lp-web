import { ModuleWithProviders, NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { LineaComponent } from './../linea/linea.component';
import { AuthGuard, SharedModule } from '../shared';
import { lineaRouting } from './linea.router';

// const lineaRouting: ModuleWithProviders = RouterModule.forChild([
//   {
//     path: 'linea',
//     component: LineaComponent,
//     // loadChildren: './linea.module#LineaModule',
//     canActivate: [AuthGuard]
//   }
// ]);

@NgModule({
  imports: [
    SharedModule,
    lineaRouting
    // lineaRouting
  ],
  declarations: [
    LineaComponent
  ]
})
export class LineaModule { }
