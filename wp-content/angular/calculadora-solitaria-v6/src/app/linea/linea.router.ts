import { NgModule, ModuleWithProviders } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { CommonModule } from '@angular/common';
import { LineaComponent } from './linea.component';

const routes: Routes = [
  { path: '', component: LineaComponent }
];

export const lineaRouting: ModuleWithProviders = RouterModule.forChild(routes);