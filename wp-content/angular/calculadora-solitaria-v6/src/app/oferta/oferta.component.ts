import { Component, ViewEncapsulation, Input, OnInit, Inject, Injectable } from '@angular/core';
import { FormBuilder,FormGroup } from '@angular/forms';
import { Router } from '@angular/router';
import { DOCUMENT } from "@angular/platform-browser";
import { User, UserService, DatosService } from '../shared';
import { Observable } from "rxjs/Rx";
import { ReplaySubject } from 'rxjs/ReplaySubject';

import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import { distinctUntilChanged, map } from 'rxjs/operators';
//import { readJson } from "./../shared/services/readJson.service";
//import { formCookiesService } from "./../shared/services/formCookies.service";
//import { getJson } from "./../shared/services/getJson.service";


declare var jQuery : any;
declare var $ :any;

@Component({
  selector: 'oferta', 
  templateUrl: './oferta.component.html', 
  styleUrls: ['./oferta.component.scss'],
  encapsulation: ViewEncapsulation.None,
}) 
@Injectable()

export class OfertaComponent implements OnInit
{ 
  
  user: User = {} as User;
  settingsForm: FormGroup;
  errors: Object = {};
  isSubmitting = false;
  public oferta: {};
  public result:{};
  private currentUserSubject = new BehaviorSubject<User>({} as User);
  public currentUser = this.currentUserSubject.asObservable().pipe(distinctUntilChanged());
  private isAuthenticatedSubject = new ReplaySubject<boolean>(1);

  public estados: {};
  public carreras: {};
  public carrerasBien:any[];
  public data:{};
  
  constructor(
    private router: Router,
    private userService: UserService,
    private fb: FormBuilder,
    //private readJsonService: readJson,
    @Inject(DOCUMENT) private document,
    private datos:DatosService
  ) {

   }

  ngOnInit() {
    console.log(this.userService.getCurrentUser());
    let lineaweb=this.datos.getCampo("linea");
    console.log(lineaweb.id_name);
    this.carreras=this.getCarreras(lineaweb.id_name);
    console.log(this.carreras);
    this.carrerasBien = this.getCarrerasBien(this.carreras);
    console.log(this.carrerasBien);

    this.carrerasBien.sort(this.dynamicSort("Categoria"));

  } 

  dynamicSort(property) {
    var sortOrder = 1;
    if(property[0] === "-") {
        sortOrder = -1;
        property = property.substr(1);
    }
    return function (a,b) {
        var result = (a[property] < b[property]) ? -1 : (a[property] > b[property]) ? 1 : 0;
        return result * sortOrder;
    }
}
  //retorna en la data desde el primer padre del json los coinsidentes iguales a key => value
  buscar(key, value, jsonCarreras) {
    let resultadoBusqueda = [];
    for (let i in jsonCarreras) {
      if (this.getObjects(jsonCarreras[i], key, value).length != 0) {
        resultadoBusqueda.push(jsonCarreras[i])
      }
    }
    return resultadoBusqueda;
  }

  setOferta(oferta){
    this.datos.guardaUsuario('carrera', oferta);
    
    this.router.navigateByUrl('/modalidad');
    //this.router.navigateByUrl('/campus');
  }

  //Retorna los coinsidentes entre key => value desde padre directo
  getObjects(obj, key, val) {
    var objects = [];
    for (var i in obj) {
      if (!obj.hasOwnProperty(i)) continue;

      if (typeof obj[i] == 'object') {
        objects = objects.concat(this.getObjects(obj[i], key, val));
      } else
        //if key matches and value matches or if key matches and value is not passed (eliminating the case where key matches but passed value does not)
        if (i == key && obj[i] == val || i == key && val == '') { //
          objects.push(obj);
        } else if (obj[i] == val && key == '') {
          //only add if the object is not already in the array
          if (objects.indexOf(obj) == -1) {
            objects.push(obj);
          }
        }
    }
    return objects;
  } 


  getCarreras(linea, campus = "") {
    let keySearch = "lineaweb";
    let arrayCarrerasPorLinea = [];
    let objJsonCarreras = JSON.parse(localStorage.getItem("jsonCarreras"));
    arrayCarrerasPorLinea = this.buscar(
      keySearch,
      linea,
      objJsonCarreras
    );
  
    return arrayCarrerasPorLinea
  }

  getCarrerasBien(categorias){
    let objJsonCarreras = JSON.parse(localStorage.getItem("jsonLinksCategorias"));
    let categoriasno= new Array();
    Object.entries(categorias).forEach(function (key, value) {
      categoriasno.push(key[1]['Grupo_carreras']);
    });
    //console.log(categoriasno);
     let nombres=new Array();
     Object.entries(objJsonCarreras).forEach(function (key, value) {
       console.log(key[1]['IdCategoria']);
       if (categoriasno.includes(key[1]['IdCategoria'])) {
         nombres.push(key[1]);
        } 
     });
    return nombres;
  }

  getModalidadPorSubnivel(subNivelInteres) {
    let modalidad;
    if (subNivelInteres == 1 || subNivelInteres == 2 || subNivelInteres == 5 || subNivelInteres == 7) {
      //Presencial
      modalidad = 1;
    } else if (subNivelInteres == 3) {
      //ejecutiva
      modalidad = 2;
    } else if (subNivelInteres == 4 || subNivelInteres == 6) {
      modalidad = 3;
    }
    return modalidad;
  }

  
  logout() {
    this.userService.purgeAuth();
    this.router.navigateByUrl('/');
  }

  setEstado(item){
    console.log(item);
    this.user.estado = item;
    Object.assign(this.user, item);
      //Object.assign(this.user, item);
    this.currentUserSubject.value;
    console.log(this.user);
    this.router.navigateByUrl('/linea');
  }
  submitForm() {
    this.isSubmitting = true;

    // update the model
    this.updateUser(this.settingsForm.value);

    this.userService
      .update(this.user)
      .subscribe(
        updatedUser => this.router.navigateByUrl('/profile/' + updatedUser.username),
        err => {
          this.errors = err;
          this.isSubmitting = false;
        }
      );
  }

  updateUser(values: Object) {
    Object.assign(this.user, values);
  }

  

}