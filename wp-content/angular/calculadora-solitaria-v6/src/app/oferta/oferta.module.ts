import { ModuleWithProviders, NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { OfertaComponent } from './../oferta/oferta.component';
import { AuthGuard, SharedModule } from '../shared';
import { HttpModule } from '@angular/http';
import { ofertaRouting } from './oferta.router';

// const ofertaRouting: ModuleWithProviders = RouterModule.forChild([
//   {
//     path: 'oferta',
//     component: OfertaComponent,
//     canActivate: [AuthGuard]
//   }
// ]);

@NgModule({
  imports: [
    SharedModule,
    ofertaRouting
  ],
  declarations: [
    OfertaComponent
  ]
})
export class OfertaModule { }
