import { NgModule, ModuleWithProviders } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { CommonModule } from '@angular/common';
import { OfertaComponent } from './oferta.component';

const routes: Routes = [
  { path: '', component: OfertaComponent }
];

export const ofertaRouting: ModuleWithProviders = RouterModule.forChild(routes);