import { ModuleWithProviders, NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { ModalidadComponent } from './../modalidad/modalidad.component';
import { AuthGuard, SharedModule } from '../shared';
import { HttpModule } from '@angular/http';
import { modalidadRouting } from './modalidad.router';

// const ModalidadRouting: ModuleWithProviders = RouterModule.forChild([
//   {
//     path: 'modalidad',
//     component: ModalidadComponent,
//     canActivate: [AuthGuard]
//   }
// ]);

@NgModule({
  imports: [
    SharedModule,
    modalidadRouting
  ],
  declarations: [
    ModalidadComponent
  ]
})
export class ModalidadModule { }
