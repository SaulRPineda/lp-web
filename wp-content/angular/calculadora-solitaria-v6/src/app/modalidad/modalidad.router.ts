import { NgModule, ModuleWithProviders } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { CommonModule } from '@angular/common';
import { ModalidadComponent } from './modalidad.component';

const routes: Routes = [
  { path: '', component: ModalidadComponent }
];

export const modalidadRouting: ModuleWithProviders = RouterModule.forChild(routes);