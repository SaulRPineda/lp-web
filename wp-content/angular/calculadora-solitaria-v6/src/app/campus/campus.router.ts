import { NgModule, ModuleWithProviders } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { CommonModule } from '@angular/common';
import { CampusComponent } from './campus.component';

const routes: Routes = [
  { path: '', component: CampusComponent }
];

export const campusRouting: ModuleWithProviders = RouterModule.forChild(routes);