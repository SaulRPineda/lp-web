import { Component, OnInit,DoCheck } from '@angular/core';
import { Router } from '@angular/router';
import { ArticleListConfig, TagsService, User, UserService } from '../shared';
import { formCookiesService } from "../formularios/forms_services/formCookies.service";
import {GenericService}from "../services/generic.service";

declare var jQuery : any;
declare var $ :any;

@Component({
  selector: 'app-home-page',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css'],
  providers: [formCookiesService]
})
export class HomeComponent implements OnInit, DoCheck {
  constructor(
    private getService:GenericService,
    private router: Router,
    private tagsService: TagsService,
    private userService: UserService,
    private formCookiesService: formCookiesService
  ) {

    $ = jQuery;
  }

  isAuthenticated: boolean;
  listConfig: ArticleListConfig = {
    type: 'all',
    filters: {}
  };
  tags: Array<string> = [];
  tagsLoaded = false;


  ngDoCheck(){
    // var $_GET=this.getService.getMethod();
    // console.log($_GET['utm_source']);
  }

  ngOnInit() {

    jQuery("#head_close_modal").on("click", function() {
        jQuery('main').fadeToggle();
        jQuery(".calcuApp").fadeToggle();
        jQuery('footer').fadeToggle();
        jQuery('#carousel-banner').fadeToggle();
    });
   
    const credentials = new Array();
    credentials['email']='a';
    credentials['username'] = 'a';
    credentials['token'] = 'a';

    this.userService.attemptAuth('', credentials)

    $('#termina-registro').click(() => {
      // this.router.navigateByUrl('/formulario');
      console.log("Abro formulario");
    })

    $('.muestracalcu').click(() => {
      // this.router.navigateByUrl('/formulario');
      console.log("Abro formulario");
    })

    var cookie = this.formCookiesService.getCookie('regcompleto');

    this.checkExternalSource();
    // console.log(cookie);

    // if(cookie != undefined && cookie == '1') {
    //   this.router.navigateByUrl('/formulario');
    // }
    

    // this.userService.isAuthenticated.subscribe(
    //   (authenticated) => {
    //     this.isAuthenticated = authenticated;

    //     // set the article list accordingly
    //     if (authenticated) {
    //       //this.setListTo('feed');
    //       console.log(this.formCookiesService.getCookieByKey("preserve_form_calc_values","frm_nombre"));
    //       if (this.formCookiesService.getCookieByKey("preserve_form_calc_values","frm_nombre") == false) {
    //         //this.router.navigateByUrl('/',{ queryParamsHandling: "preserve" });
    //         this.router.navigate(['/'], { preserveQueryParams: true });
    //         localStorage.clear();
    //       }else{
            
    //        // this.router.navigateByUrl('/linea',{ queryParamsHandling: "preserve" });
    //         this.router.navigate(['/linea'], { preserveQueryParams: true });

    //       }
    //     } else {
    //       //this.setListTo('all');
    //       //this.router.navigateByUrl('/',{ queryParamsHandling: "preserve" });
    //       this.router.navigate(['/'], { preserveQueryParams: true });
    //     }
    //   }
    // );

    // this.tagsService.getAll()
    // .subscribe(tags => {
    //   this.tags = tags;
    //   this.tagsLoaded = true;
    // });
    if (this.formCookiesService.checkCookie("c_form_data") == true) {
      //Obtener todos los valores de la cookie
      var arr_obj_cookie_values = this.formCookiesService.getCookieValues(
        "c_form_data"
      );
      console.log("AQUI ESTA LA COOKIE C_FORM_DATA");
      console.log(arr_obj_cookie_values);
      //Validamos los valores de la cookie para ver si ya no le pedimos los datos de contacto
      if (
        arr_obj_cookie_values["nombre"] != "" &&
        typeof arr_obj_cookie_values["nombre"] != "undefined" &&
        (arr_obj_cookie_values["apaterno"] != "" &&
          typeof arr_obj_cookie_values["apaterno"] != "undefined") &&
        (arr_obj_cookie_values["amaterno"] != "" &&
          typeof arr_obj_cookie_values["amaterno"] != "undefined") &&
        (arr_obj_cookie_values["celular"] != "" &&
          typeof arr_obj_cookie_values["celular"] != "undefined") &&
        (arr_obj_cookie_values["email"] != "" &&
          typeof arr_obj_cookie_values["email"] != "undefined") &&
        (arr_obj_cookie_values["tipoRegistro"] != "" &&
          typeof arr_obj_cookie_values["tipoRegistro"] != "undefined")
      ) {
        //Formulario de registro completado
        var paso_cookie_formulario = 1;
        var valores_cookie = "true";
        console.log("SALTANDO EL REGISTRO DE CALCULADORA");
        jQuery("app-root").attr('data-terminado','1');
      }
    }
  }

  /* Verificacion si proviene de una fuente externa */
  checkExternalSource(){
    var seleccionJson = JSON.parse(localStorage.getItem("jsonUsr"));
    if((seleccionJson || {}).form != undefined){
        if(seleccionJson.form.frm_amaterno == "telemarketer"){
          jQuery(".home-page").attr('data-externo','1');
        }
    }
  }

  setListTo(type: string = '', filters: Object = {}) {
    // Si solicita un recurso para logeados y no esta autenticado se redirige a logearse en este caso a registro
    if (type === 'feed' && !this.isAuthenticated) {
      this.router.navigateByUrl('/');
      return;
    }

    // Otherwise, set the list object
    this.listConfig = {type: type, filters: filters};
  }
}
