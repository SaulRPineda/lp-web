import { ModuleWithProviders, NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { EstadoComponent } from './../estado/estado.component';
import { AuthGuard, SharedModule } from '../shared';
import { routing } from '../calculadora.module';

// const estadoRouting: ModuleWithProviders = RouterModule.forChild([
//   {
//     path: 'estado',
//     component: EstadoComponent,
//     // loadChildren: './../linea/linea.module#LineaModule',
//     canActivate: [AuthGuard]
//   }
// ]);

@NgModule({
  imports: [
    SharedModule,
    routing
    // estadoRouting
  ],
  declarations: [
    EstadoComponent
  ]
})
export class EstadoModule { }
