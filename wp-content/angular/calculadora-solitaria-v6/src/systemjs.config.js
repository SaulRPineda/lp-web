/**
 * System configuration for Angular samples
 * Adjust as necessary for your application needs.
 */
(function (global) {
    System.config({
        paths: {
            // paths serve as alias
            'npm:': 'https://unpkg.com/'
        },
        // map tells the System loader where to look for things
        map: {
            // our app is within the app folder
            app: 'app',

            // angular bundles
            'nouislider': 'node_modules/nouislider',
            'ng2-nouislider': 'node_modules/ng2-nouislider',

            // other libraries
            'rxjs': 'npm:rxjs',
            'angular-in-memory-web-api': 'npm:angular-in-memory-web-api/bundles/in-memory-web-api.umd.js',
            'typescript': 'npm:typescript/lib/typescript.js',
            'ts': 'npm:plugin-typescript/lib/plugin.js'
        },

         

        // packages tells the System loader how to load when no filename and/or no extension
        packages: {
            'nouislider': { main: 'distribute/nouislider.js', defaultExtension: 'js' },
            'ng2-nouislider': { main: 'src/nouislider.js', defaultExtension: 'js' }
        }
    });
})(this);