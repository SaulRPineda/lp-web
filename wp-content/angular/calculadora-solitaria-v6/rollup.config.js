import nodeResolve from 'rollup-plugin-node-resolve';
import commonjs from 'rollup-plugin-commonjs';
import uglify from 'rollup-plugin-uglify';
import terser from 'rollup-plugin-terser';

export default {
    // entry: 'src/main.ts',
    // dest: 'src/build.js', // output a single application bundle
    input: 'src/main.ts',
    output: {
        file: 'prueba.js',
        // format: 'cjs', // immediately-invoked function expression — suitable for <script> tags
        format: 'iife'
    },
    
    onwarn: function (warning) {
        // Skip certain warnings

        // should intercept ... but doesn't in some rollup versions
        if (warning.code === 'THIS_IS_UNDEFINED') { return; }

        // console.warn everything else
        console.warn(warning.message);
    },
    plugins: [
        nodeResolve({ jsnext: true, module: true }),
        commonjs({
            include: ['node_modules/rxjs/**', 'node_modules/ng2-nouislider/**'],
            sourcemap: false
        }),
        // terser.terser()
    ]
};
