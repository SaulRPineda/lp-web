import { Injectable } from "@angular/core";
import { formCookiesService } from "../forms_services/formCookies.service";
import { userValidations } from '../forms_services/validaciones.service';
import { CapitalizePipe } from "./service.pipe";
import { readJson } from "../../services/readJson.service";
import { GenericService } from "../../services/generic.service";

declare var $: any;
declare var jQuery: any;
declare var dataLayer: any;
declare var target:any;
declare var ga: any;
declare var $_GET : any;
// var safeUrl = (/unitec.mx/.test(window.location.href)) ? "https://unitecmx-universidadtecno.netdna-ssl.com" : "//" + document.domain;
var safeUrl = (/unitec.mx/.test(window.location.href)) ? "https://www.unitec.mx" : "//" + document.domain;

var path = "https://" + document.domain + "/";

var testingSub: any = "Actualizacion";

@Injectable()
export class sendService {
  ciclo_default = "";
  urlreferrer: any = window.location;
  private url_ajax_php = "/wp-content/phpServeApp/backend0.php";
  private url_ajax_php_traditionalForm = "/wp-content/phpServeApp/backend4.php";
  private urlDomain = "localhost:8888/unitec2017";
  private urlMicroRegistroPrueba = "https://www.unitec.mx/procWeb/microregistro.php";
  private urlMicroRegistroDeploy = "https://www.unitec.mx/desk/procWeb/microregistro.php";
  
  constructor(private formCookieService: formCookiesService, 
    private capitalizePipe: CapitalizePipe,
    private userValidations : userValidations,
    private readJsonService: readJson,
    private formCookiesService: formCookiesService,
    private genericService: GenericService
  ) {
    
  }

  $_GET = this.genericService.getMethod();
  
  send(theData, urls) {
    console.log(theData);
    for (let i in urls) {
      jQuery
        .ajax({
          type: "GET",
          url: urls[i],
          data: { theData: theData }
        })
        .done(function(resultado) {
          console.log( resultado );
          return resultado;
        });
    }
  }

  sendMicroRegistro(evento = null) {
    /*Implementacion enviar datos BY SRP 17-01-2018*/
    //Convertir a JSON la Cookie
    // console.log(JSON.stringify(this.formCookieService.getCookieValues("c_form_data")).replace(/\\n/g,' '));
    // let dataForm = JSON.parse(JSON.stringify(this.formCookieService.getCookieValues("c_form_data")));
    let dataForm = this.formCookieService.getCookieValues("c_form_data");
    // console.log("TODO BIEN AQUI");
    // console.log(dataForm);
    // url: "http://www.unitec.mx/desk/procWeb/microregistro.php",
    // url: "//" + document.domain + this.url_ajax_php,

    jQuery
      .ajax({
        type: "POST",
        url: this.urlMicroRegistroDeploy,
        data: this.formCookieService.getCookieValues("c_form_data"),
        success: function (res) {
          console.log("Datos Enviados");
          console.log(res);
          
          //Proceso tag manager
          if (typeof dataLayer != 'undefined') {
                        
              dataLayer.push({'event': 'PASO1_COMPLETADO'});
              dataLayer.push({
                  'url_value': 'formularioInformacion/paso1', //dato dinámico
                  'event': 'StepForm'//dato estático
              });
          }
          //End Proceso tag manager

        },
        error: function (xhr, textStatus, error) {
          console.log(xhr);
          console.log(xhr.statusText);
          console.log(textStatus);
          console.log(error);
        }
      });
      /*End Implementacion enviar datos BY SRP 17-01-2018*/
  }

  sendFormularioTradicional(evento = null) {
    //var url_multimedia_thankyou_predeterminada = "assets/sidenav.jpg";
    //Si no esta la imagen destacada se asigna la imagen predeterminada
    console.log("ENVIO DE FORMULARIO TRADICIONAL");
    var url_multimedia_thankyou = jQuery("#h_horizontal_url_imagen_destacada").val();
    console.log("CATEGORIA PARA THNK: " + jQuery("#formApp").data("categoria"));
    //Implementacion ajax de la imagen destacada
    // jQuery(".img-header-thankyou").css({
    //   "background-image": "url(" + jQuery("#h_horizontal_url_imagen_destacada").val() + ")"
    // });

    console.log("EL VALOR DEL EVENTO");
    console.log(evento);

    var ev = null;
    //Evento
    if (evento != null) {
      ev = evento.srcElement || evento.target;
    }
    //Validacion para saber si el evento viene de la caja de ciclo
    if (ev == null || typeof ev == "undefined" || ev == "") {
      jQuery("#formApp").data("ciclo", "");
    } else {
      jQuery("#formApp").data("ciclo", ev.attributes.value.nodeValue);
      jQuery("#formApp").data("cicloTexto", ev.attributes['data-text'].nodeValue);
    }

    if (
      jQuery("#formApp").data("ciclo") == "" ||
      typeof jQuery("#formApp").data("ciclo") == "undefined"
    ) {
      console.log(
        "No ha seleccionado el ciclo: " +
          typeof jQuery("#formApp").data("ciclo")
      );
      jQuery(".app-menu-title").addClass("parpadea");
      jQuery("#divisor-menu-app").addClass("hr-error");
      jQuery("#divisor-menu-app").addClass("parpadea");
      setTimeout(function() {
        jQuery(".app-menu-title").removeClass("parpadea");
        jQuery("#divisor-menu-app").removeClass("parpadea");
        jQuery("#divisor-menu-app").removeClass("hr-error");
      }, 1000);
    } else {
      //Se registra el valor de la urlreferer
      this.formCookieService.appendCookieValue(
        "c_form_data",
        "urlreferrer",
        this.urlreferrer
      );

      var cookie_form_values = this.formCookieService.getCookieValues(
        "c_form_data"
      );
      //Preparamos los datos para la thank you page
      jQuery(".modal-header-formulario").hide("slow");
      jQuery(".modal-header-thankyou").hide("slow");
      jQuery(".img-header-thankyou").hide();
      jQuery("#enviarTrd").hide();
      jQuery("#body-form-todo").hide("slow");
      jQuery(".modal-body-thankyou").show("slow");
      jQuery(".tercer-texto-thankyou").html(cookie_form_values["carrera"]);
      jQuery("#nombre-thankyou").html(cookie_form_values["nombre"]);
      jQuery("#head_close_modal").hide("slow");
      jQuery(".titulo-logo").show("slow");

      /*Implementacion enviar datos BY SRP 28-01-2018*/
      jQuery("#txt-thanks").html("Gracias por proporcionarnos tus datos. En breve te enviaremos la información que nos solicitas de:");
      /*End Implementacion enviar datos BY SRP 28-01-2018*/

      //Si no viene con evento
      console.log("EL EVENTO DESPUES DE CLICLEAR CICLO: ");
      console.log(ev);
      if (ev.id != "null" && ev.id != null && typeof ev.id != "undefined") {
        console.log("CICLO: " + ev.attributes.value.nodeValue);
        //Setear en la cookie el ciclo y en formApp
        this.formCookieService.appendCookieValue(
          "c_form_data",
          "ciclo",
          ev.attributes.value.nodeValue
        );
        this.formCookieService.appendCookieValue(
          "c_form_data",
          "cicloTexto",
          ev.attributes['data-text'].nodeValue
        );
      } else {
        console.log("El evento no esta definido para el ciclo");
      }

      var nombreCampus;
      var nombreLicenciatura;
      var nombreModalidad;
      var nombreCiclo;
      var anclaPerfil:string = "#perfil";
      var anclaGaleria:string = "#galeria-campus";

      if(jQuery("#formApp").data("campusLargo") !== "") {
        // nombreCampus = this.formCookieService.getCookieByKey("c_form_data", "campusLargo");
        nombreCampus = jQuery("#formApp").data("campusLargo");

      }
      if(jQuery("#formApp").data("linea") !== "") {
        nombreLicenciatura = jQuery("#formApp").data("linea");
      }
      nombreLicenciatura = nombreLicenciatura.toLowerCase();
      nombreLicenciatura = this.capitalizePipe.transform(nombreLicenciatura);
      if(jQuery("#formApp").data("carreraCompleta") != "") {
        nombreLicenciatura += " en " + jQuery("#formApp").data("carreraCompleta");
      }
      if(jQuery("#formApp").data("modalidad") != "") {
        switch (jQuery("#formApp").data("modalidad")) {
          case "1":
            nombreModalidad = "Modalidad Presencial";
            /*Validación para campus en línea BY SRP 13-09-2018*/
            jQuery("#title-virtualtour").html("Conoce las instalaciones");
            anclaPerfil = "#perfil";
            anclaGaleria = "#galeria-campus";
          break;

          case "2":
            nombreModalidad = "Modalidad Ejecutiva";
            jQuery("#title-virtualtour").html("Conoce las instalaciones");
            anclaPerfil = "#perfil";
            anclaGaleria = "#galeria-campus";
          break;

          case "3":
            nombreModalidad = "Modalidad En Línea";
            jQuery("#title-virtualtour").html("Conoce más");
            anclaPerfil = "#perfil";
            anclaGaleria = "";
            /*End Validación para campus en línea BY SRP 13-09-2018*/
          break;
        }
      }
      if(jQuery("#formApp").data("cicloTexto") != "") {
        nombreCiclo = "Iniciar " + jQuery("#formApp").data("cicloTexto") + " 2018";
      }
      let arrayCategoriasPorID = this.readJsonService.buscar(
        "IdCategoria",
        this.formCookieService.getCookieByKey("c_form_data", "categoria"),
        JSON.parse(localStorage.getItem("jsonLinksCategorias"))
      );
      /*Contenido Thank you Page*/
      jQuery('.second-sub-desk').text("UNITEC Campus " + nombreCampus);
      jQuery('#thank-linea').text("");
      jQuery('#thank-linea').text(nombreLicenciatura);
      jQuery('#thank-modalidad').text("");
      jQuery('#thank-modalidad').text(nombreModalidad);
      jQuery('#thank-campus').text("");
      jQuery('#thank-campus').text("Campus " + nombreCampus);
      jQuery('#thank-ciclo').text("");
      jQuery('#thank-ciclo').text(nombreCiclo);

      /*Implementacion Texto thank you page Formulario Tradicional BY SRP 12-09-2018*/
      jQuery("#descripcionThanks").html("Impulsamos tu esfuerzo recompensándote con una Beca Académica de Primer Ingreso. Conoce el porcentaje que te corresponde según tu promedio.");

      var link_carrera;
      if (arrayCategoriasPorID[0] !== undefined) {
        let linkSeleccionado = arrayCategoriasPorID[0].Link;
        link_carrera = linkSeleccionado.toLowerCase() + anclaPerfil;
        link_carrera = this.userValidations.removeAcentos( link_carrera );
        // $('#link-to-campus-study').attr('href', path + link_carrera.replace(/ /g, "-") );
      }


      /*Implementacion enviar datos BY SRP 17-01-2018*/
      //Convertir a JSON la Cookie
      let dataForm = JSON.parse( JSON.stringify( this.formCookieService.getCookieValues("c_form_data") ) );
      
      if(dataForm.banner == null || dataForm.banner == "" ){
        let bannerSecure :string;
        bannerSecure = this.$_GET["utm_campaign"]; 
        if( bannerSecure == null || bannerSecure == "" ) {
          bannerSecure = "ASPIRANTES LIC";
        }

        //Se registra el valor de la cookie utm_campaing
        this.formCookieService.appendCookieValue(
          "c_form_data",
          "banner",
          bannerSecure
        );
      }
      console.log(this.formCookieService.getCookieValues("c_form_data"));

      // console.log("Campus " + jQuery("#formApp").data("campusLargo"));
      $.ajax({
        type: "POST",
        url: safeUrl + "/wp-admin/admin-ajax.php",
        data: {
          action: "getCampusBySlug",
          campus_name: "Campus " + jQuery("#formApp").data("campusLargo")
        },
        success: function (res) {
          console.log("Datos Enviados");
          console.log(res);
          //End Proceso tag manager
          res = JSON.parse(res);
          jQuery("#image-virtualtour").attr("src",res['imagen-grande']);
          jQuery("#image-virtualtour").attr("alt",res['descripcion-grande']);
          jQuery("#link-to-campus").attr("href", res['url-campus'] + anclaGaleria);

          /*Implementacion Para los Tour Virtuales*/
          // if(jQuery("#formApp").data("campusLargo") == "Atizapán") {
          //   jQuery("#link-to-campus").attr("href", "https://www.google.com.mx/maps/@19.5467281,-99.2396415,3a,75y,258.7h,82.77t/data=!3m7!1e1!3m5!1s9CEBnYu4l0AQlF7iBnMAxQ!2e0!3e5!7i13312!8i6656");
          // } else {
          //   jQuery("#link-to-campus").attr("href", res['url-campus'] + "#galeria-campus");
          // }

        },
        error: function (xhr, textStatus, error) {
          console.log(xhr);
          console.log(xhr.statusText);
          console.log(textStatus);
          console.log(error);
        }
      });


      if ( location.href.indexOf( "impulsa" ) !== -1 ) {
          console.log("impulsa");
          this.sendFormularioImpulsa(evento);
      } 
      
      else if( location.href.indexOf( "testvocacional" ) !== -1 ){
          console.log("Orientacion Profesional");
          this.sendFormularioOrientacion(evento);
      }

      else{
        console.log("Formulario Tradicional");
        /*Implementacion enviar datos BY SRP 17-09-2018*/
        //Convertir a JSON la Cookie
        /*Implementacion enviar datos BY SRP 17-01-2018*/
      //Convertir a JSON la Cookie
      let dataForm = JSON.parse( JSON.stringify( this.formCookieService.getCookieValues("c_form_data") ) );

      jQuery.ajax({
        type: "POST",
        url: safeUrl + this.url_ajax_php,
        data: { theData: this.formCookieService.getCookieValues("c_form_data") },
        dataType: "JSON",
        success: function(res) {
              //console.log("Datos Enviados");          
              console.log(res);
              console.log("RES: " + res.tipo);
              //Proceso tag manager
              if (typeof dataLayer != 'undefined') {dataLayer.push({'event': 'PASO2_COMPLETADO'});}
          
              if(res.tipo == 'dupli'){
                  if (typeof dataLayer != 'undefined') {dataLayer.push({'event': 'FORMULARIO_DUPLICADO'});}
                  dataLayer.push({
                      'url_value': 'formularioInformacion/frmDuplicado', //dato dinámico
                      'event': 'StepForm'//dato estático
                  });
              }

              if(res.tipo == 'nuevo'){

                var clientId="";

                if (typeof dataLayer != 'undefined') {
                    dataLayer.push({'event': dataForm.banner}); 
                    ga(function(tracker) { clientId = tracker.get('clientId');});
                                  
                    dataLayer.push({
                    'leadId': clientId, //dato dinámico
                    'CDClientID':clientId, 
                    'origenLead': 'frmTradicional', //dato dinámico
                    'isAlumn': dataForm.esAlumno, //dato dinámico
                    'ingress': dataForm.tipoRegistro, //dato dinámico
                    'state': dataForm.estado, //dato dinámico
                    'levelStudies': dataForm.nivelInteres, //dato dinámico
                    'Period': dataForm.ciclo, //dato dinámico
                    'carrera': dataForm.carreraInteres, //dato dinámico
                    'campus': dataForm.campus, //dato dinámico
                    'date': new Date(), //dato dinámico
                    'event': 'leadGeneration'//dato estático
                    });

                }

                if (typeof dataLayer != 'undefined') {
                    dataLayer.push({'event': 'FORMULARIO_REGISTRO'});                 
                }
              }
              //End Proceso tag manager

            },
            error: function(xhr, textStatus, error){
                console.log(xhr.statusText);
                console.log(textStatus);
                console.log(error);
            }
        });
      }

      // jQuery
      // .ajax({
      //   type: "POST",
      //   url: "//www.unitec.mx/procWebLeads/index.php",
      //   data: this.formCookieService.getCookieValues("c_form_data"),
      //   // dataType: "JSON",
      //   success: function(res) {
      //     console.log("Datos Enviados");          
      //     console.log(res);
      //     //End Proceso tag manager

      //   },
      //   error: function(xhr, textStatus, error){
      //       console.log(xhr.statusText);
      //       console.log(textStatus);
      //       console.log(error);
      //   }
      // });

      // jQuery
      // .ajax({
      //   type: "POST",
      //   url: "//" + document.domain + this.url_ajax_php,
      //   data: { theData: this.formCookieService.getCookieValues("c_form_data") },
      //   dataType: "JSON",
      //   success: function(res) {
      //     console.log("Datos Enviados");          
      //     console.log(res);
      //     // Proceso tag manager
      //     if (typeof dataLayer != 'undefined') {dataLayer.push({'event': 'PASO2_COMPLETADO'});}
          
      //     if(res.tipo == 'dupli'){
      //         if (typeof dataLayer != 'undefined') {dataLayer.push({'event': 'FORMULARIO_DUPLICADO'});}
      //         dataLayer.push({
      //             'url_value': 'formularioInformacion/frmDuplicado', //dato dinámico
      //             'event': 'StepForm'//dato estático
      //         });
      //     }

      //     if(res.tipo == 'nuevo'){

      //         var clientId="";

      //          if (typeof dataLayer != 'undefined') {
      //             dataLayer.push({'event': dataForm.banner}); 
      //             ga(function(tracker) { clientId = tracker.get('clientId');});
                                  
      //             dataLayer.push({
      //             'leadId': clientId, //dato dinámico
      //             'CDClientID':clientId, 
      //             'origenLead': 'frmTradicional', //dato dinámico
      //             'isAlumn': dataForm.esAlumno, //dato dinámico
      //             'ingress': dataForm.tipoRegistro, //dato dinámico
      //             'state': dataForm.estado, //dato dinámico
      //             'levelStudies': dataForm.nivelInteres, //dato dinámico
      //             'Period': dataForm.ciclo, //dato dinámico
      //             'carrera': dataForm.idCarrera, //dato dinámico
      //             'campus': dataForm.campus, //dato dinámico
      //             'date': new Date(), //dato dinámico
      //             'event': 'leadGeneration'//dato estático
      //             });

      //         }  
      //         if (typeof dataLayer != 'undefined') {
      //             dataLayer.push({'event': 'FORMULARIO_REGISTRO'});                 
      //         }
      //     }
      //     //End Proceso tag manager

      //   },
      //   error: function(xhr, textStatus, error){
      //       console.log(xhr.statusText);
      //       console.log(textStatus);
      //       console.log(error);
      //   }
      // });
      /*End Implementacion enviar datos BY SRP 17-01-2018*/
    } //Fin del else para la validacion del ciclo
  }

  /*Metodo para enviar a Impulsa BY SRP 17-01-2018*/
  sendFormularioImpulsa(evento = null) {

    //Si no esta la imagen destacada se asigna la imagen predeterminada
    ///var url_multimedia_thankyou = jQuery("#h_horizontal_url_imagen_destacada").val();
    //console.log("CATEGORIA PARA THANK Impulsa: " + jQuery("#formApp").data("categoria"));
    //Implementacion ajax de la imagen destacada
    // jQuery(".img-header-thankyou").css({
    //   "background-image": "url(" + jQuery("#h_horizontal_url_imagen_destacada").val() + ")"
    // });

    /*Creamos Cookie para Impulsa By SRP*/
    var oneDay = 24;
    this.formCookieService.appendCookieValue(
      "reg_impulsa",
      "cookie",
      true,
      oneDay
    );
    /*End Creamos Cookie para Impulsa By SRP*/

    var ev = null;
    //Evento
    if (evento != null) {
      ev = evento.srcElement || evento.target;
    }
    //Validacion para saber si el evento viene de la caja de ciclo
    if (ev == null || typeof ev == "undefined" || ev == "") {
      jQuery("#formApp").data("ciclo", "");
    } else {
      jQuery("#formApp").data("ciclo", ev.attributes.value.nodeValue);
    }

    if (
      jQuery("#formApp").data("ciclo") == "" ||
      typeof jQuery("#formApp").data("ciclo") == "undefined"
    ) {
      console.log(
        "No ha seleccionado el ciclo: " +
          typeof jQuery("#formApp").data("ciclo")
      );
      jQuery(".app-menu-title").addClass("parpadea");
      jQuery("#divisor-menu-app").addClass("hr-error");
      jQuery("#divisor-menu-app").addClass("parpadea");
      setTimeout(function() {
        jQuery(".app-menu-title").removeClass("parpadea");
        jQuery("#divisor-menu-app").removeClass("parpadea");
        jQuery("#divisor-menu-app").removeClass("hr-error");
      }, 1000);

    } else {      

      //Se registra el valor de la urlreferer
      this.formCookieService.appendCookieValue(
        "c_form_data",
        "urlreferrer",
        this.urlreferrer
      );

      var cookie_form_values = this.formCookieService.getCookieValues(
        "c_form_data"
      );

      //Preparamos los datos para la thank you page
      jQuery(".modal-header-formulario").hide("slow");
      jQuery(".modal-header-thankyou").hide("slow");
      jQuery("#enviarTrd").hide();
      jQuery(".modal-body-form").hide("slow");
      jQuery(".modal-body-thankyou").show("slow");
      jQuery(".tercer-texto-thankyou").html(cookie_form_values["carrera"]);
      jQuery("#nombre-thankyou").html(cookie_form_values["nombre"]);
      
      /*Implementacion Texto thank you page Formulario Impulsa BY SRP 12-09-2018*/
      jQuery("#descripcionThanks").html("A través de UNITEC Te Impulsa podrás trabajar para pagar tus estudios. En unos momentos un asesor se pondrá en contacto contigo para darte toda la información.");
      jQuery("#div-btn-continuar").hide("slow");
      //console.log("Ocultando EL BOTON");
      /*End Implementacion enviar datos BY SRP 28-01-2018*/

      //Si no viene con evento
      console.log("EL EVENTO DESPUES DE CLICLEAR CICLO: ");
      console.log(ev);
      if (ev.id != "null" && ev.id != null && typeof ev.id != "undefined") {
        console.log("CICLO: " + ev.attributes.value.nodeValue);
        //Setear en la cookie el ciclo y en formApp
        this.formCookieService.appendCookieValue(
          "c_form_data",
          "ciclo",
          ev.attributes.value.nodeValue
        );
      } else {
        console.log("El evento no esta definido para el ciclo");
      }
      
      /*Implementacion enviar datos BY SRP 17-09-2018*/
      //Convertir a JSON la Cookie
      let dataForm = JSON.parse( JSON.stringify( this.formCookieService.getCookieValues("c_form_data") ) );
      // safeUrl = "https://www.unitec.mx";
      jQuery
      .ajax({
        type: "POST",
        url: safeUrl + this.url_ajax_php,
        data: { theData: this.formCookieService.getCookieValues("c_form_data") },
        dataType: "JSON",
        success: function(res) {
          console.log("RES: " + res.tipo);
          // Proceso tag manager
          if (typeof dataLayer != 'undefined') {dataLayer.push({'event': 'PASO2_COMPLETADO'});}
          
          if(res.tipo == 'dupli'){
              if (typeof dataLayer != 'undefined') {dataLayer.push({'event': 'FORMULARIO_DUPLICADO'});}
              dataLayer.push({
                  'url_value': 'formularioInformacion/frmDuplicado', //dato dinámico
                  'event': 'StepForm'//dato estático
              });
          }

          if(res.tipo == 'nuevo') {

            var clientId="";

            if (typeof dataLayer != 'undefined') {
              dataLayer.push({'event': dataForm.banner}); 
              ga(function(tracker) { clientId = tracker.get('clientId');});
                              
              dataLayer.push({
                'leadId': clientId, //dato dinámico
                'CDClientID':clientId, 
                'origenLead': 'frmTradicional', //dato dinámico
                'isAlumn': dataForm.esAlumno, //dato dinámico
                'ingress': dataForm.tipoRegistro, //dato dinámico
                'state': dataForm.estado, //dato dinámico
                'levelStudies': dataForm.nivelInteres, //dato dinámico
                'Period': dataForm.ciclo, //dato dinámico
                'carrera': dataForm.carreraInteres, //dato dinámico
                'campus': dataForm.campus, //dato dinámico
                'date': new Date(), //dato dinámico
                'event': 'leadGeneration'//dato estático
              });

            }

            if (typeof dataLayer != 'undefined') {
                  dataLayer.push({'event': 'FORMULARIO_REGISTRO'});                 
            }
          }
          //End Proceso tag manager

          //Se redirige al PDF / Aplicacion de Vacantes 14-09-2018 BY SRP */
          //http://www.unitec.mx/folleto/impulsa.pdf
          //https://www.unitec.mx/vacantes-impulsa/

          setTimeout(function(){ window.location.href = 'https://www.unitec.mx/folleto/impulsa.pdf'; }, 3000);
          //setTimeout(function(){ var nuevaVentana = window.open('https://www.unitec.mx/vacantes-impulsa/', '_blank'); nuevaVentana.focus(); }, 3000);

          /*Validación para cambiar el atributo del botoón una vez creada la cookie de impulsa*/
          jQuery("#calculadoraModal").attr("href", "https://www.unitec.mx/folleto/impulsa.pdf").removeAttr('onclick');
          /*End Validación para cambiar el atributo del botoón una vez creada la cookie de impulsa*/

          //End Se redirige al PDF / Aplicacion de Vacantes 14-09-2018 BY SRP */
        },
        error: function(xhr, textStatus, error){
            console.log(xhr.statusText);
            console.log(textStatus);
            console.log(error);
        }
      });      
      /*End Implementacion enviar datos BY SRP 17-01-2018*/

    } //Fin del else para la validacion del ciclo
  }
  /*End Metodo para Enviar a Impulsa BY SRP 18-09-2018*/


  /*Metodo para enviar a Impulsa BY SRP 17-01-2018*/
  sendFormularioOrientacion(evento = null) {    
    //Si no esta la imagen destacada se asigna la imagen predeterminada
    var url_multimedia_thankyou = jQuery("#h_horizontal_url_imagen_destacada").val();
    console.log("CATEGORIA PARA THANK Impulsa: " + jQuery("#formApp").data("categoria"));
    //Implementacion ajax de la imagen destacada
    $.ajax({
      type: "POST",
      url: safeUrl + "/wp-admin/admin-ajax.php",
      data: {
        action: "getAttachmentUrlByID",
        page_id: jQuery("#formApp").data("categoria")
      },
      success: function(data) {
        // var obj = JSON.parse(data);
        // url_multimedia_thankyou = obj.img_url;
        // //console.log(url_multimedia_thankyou);
        // console.log(obj);
        // if (
        //   url_multimedia_thankyou == "" ||
        //   typeof url_multimedia_thankyou == "undefined" ||
        //   url_multimedia_thankyou == "undefined" ||
        //   url_multimedia_thankyou == null
        // ) {
        //   console.log("Producto sin imagen destacada o sin sku asignado");
        //   url_multimedia_thankyou = "//" + document.domain + "/assets/defaultImage.jpg";
        // }
        // console.log( "//" + document.domain + "/assets/defaultImage.jpg" );
        // //Asignar imagen predeterminada de Thank you page
        // try {
        //   jQuery(".img-header-thankyou").css({
        //     "background-image": "url(" + "//" + document.domain + "/assets/defaultImage.jpg" + ")"
        //   });
        // }catch(error) {
        //   jQuery(".img-header-thankyou").css({
        //     "background-image": "url(" + "//" + document.domain + "/assets/defaultImage.jpg" + ")"
        //   });
        // }
        try{
          jQuery(".img-header-thankyou").css({
            "background-image": "url(" + jQuery("#h_horizontal_url_imagen_destacada").val() + ")"
          });
        }catch(error){
          jQuery(".img-header-thankyou").css({
            "background-image": "url(" + jQuery("#h_horizontal_url_imagen_destacada").val() + ")"
          });
        }
      }
    });

    var ev = null;
    //Evento
    if (evento != null) {
      ev = evento.srcElement || evento.target;
    }
    //Validacion para saber si el evento viene de la caja de ciclo
    if (ev == null || typeof ev == "undefined" || ev == "") {
      jQuery("#formApp").data("ciclo", "");
    } else {
      jQuery("#formApp").data("ciclo", ev.attributes.value.nodeValue);
    }

    if (
      jQuery("#formApp").data("ciclo") == "" ||
      typeof jQuery("#formApp").data("ciclo") == "undefined"
    ) {
      console.log(
        "No ha seleccionado el ciclo: " +
          typeof jQuery("#formApp").data("ciclo")
      );
      jQuery(".app-menu-title").addClass("parpadea");
      jQuery("#divisor-menu-app").addClass("hr-error");
      jQuery("#divisor-menu-app").addClass("parpadea");
      setTimeout(function() {
        jQuery(".app-menu-title").removeClass("parpadea");
        jQuery("#divisor-menu-app").removeClass("parpadea");
        jQuery("#divisor-menu-app").removeClass("hr-error");
      }, 1000);
    
    } else {      

      //Se registra el valor de la urlreferer
      this.formCookieService.appendCookieValue(
        "c_form_data",
        "urlreferrer",
        this.urlreferrer
      );

      var cookie_form_values = this.formCookieService.getCookieValues(
        "c_form_data"
      );

      //Preparamos los datos para la thank you page
      jQuery(".modal-header-formulario").hide("slow");
      jQuery(".modal-header-thankyou").hide("slow");
      jQuery("#enviarTrd").hide();
      jQuery(".modal-body-form").hide("slow");
      jQuery(".modal-body-thankyou").show("slow");
      jQuery(".tercer-texto-thankyou").html(cookie_form_values["carrera"]);
      jQuery("#nombre-thankyou").html(cookie_form_values["nombre"]);
      

      /*Implementacion Texto thank you page Formulario Orientación Profesional BY SRP 12-09-2018*/
      jQuery("#descripcionThanks").html("Descubre cuáles son tus aptitudes y qué profesión se ajusta a tu perfil. Espera unos instantes y comenzará el test vocacional.");
      jQuery("#div-btn-continuar").hide("slow");
      //console.log("Ocultando EL BOTON");
      /*End Implementacion enviar datos BY SRP 28-01-2018*/

      //Si no viene con evento
      console.log("EL EVENTO DESPUES DE CLICLEAR CICLO: ");
      console.log(ev);
      if (ev.id != "null" && ev.id != null && typeof ev.id != "undefined") {
        console.log("CICLO: " + ev.attributes.value.nodeValue);
        //Setear en la cookie el ciclo y en formApp
        this.formCookieService.appendCookieValue(
          "c_form_data",
          "ciclo",
          ev.attributes.value.nodeValue
        );
      } else {
        console.log("El evento no esta definido para el ciclo");
      }
      
      /*Implementacion enviar datos BY SRP 17-01-2018*/
      //Convertir a JSON la Cookie
      let dataForm = JSON.parse( JSON.stringify( this.formCookieService.getCookieValues("c_form_data") ) );

      jQuery.ajax({
        type: "POST",
        url: safeUrl + this.url_ajax_php,
        data: { theData: this.formCookieService.getCookieValues("c_form_data") },
        dataType: "JSON",
        success: function(res) {
            // Proceso tag manager
          if (typeof dataLayer != 'undefined') {dataLayer.push({'event': 'PASO2_COMPLETADO'});}
          
          if(res.tipo == 'dupli'){
              if (typeof dataLayer != 'undefined') {dataLayer.push({'event': 'FORMULARIO_DUPLICADO'});}
              dataLayer.push({
                  'url_value': 'formularioInformacion/frmDuplicado', //dato dinámico
                  'event': 'StepForm'//dato estático
              });
          }

          if(res.tipo == 'nuevo'){

            var clientId="";

            if (typeof dataLayer != 'undefined') {
              dataLayer.push({'event': dataForm.banner}); 
              ga(function(tracker) { clientId = tracker.get('clientId');});
                              
              dataLayer.push({
                'leadId': clientId, //dato dinámico
                'CDClientID':clientId, 
                'origenLead': 'frmTradicional', //dato dinámico
                'isAlumn': dataForm.esAlumno, //dato dinámico
                'ingress': dataForm.tipoRegistro, //dato dinámico
                'state': dataForm.estado, //dato dinámico
                'levelStudies': dataForm.nivelInteres, //dato dinámico
                'Period': dataForm.ciclo, //dato dinámico
                'carrera': dataForm.carreraInteres, //dato dinámico
                'campus': dataForm.campus, //dato dinámico
                'date': new Date(), //dato dinámico
                'event': 'leadGeneration'//dato estático
              });

            }

            if (typeof dataLayer != 'undefined') {
                dataLayer.push({'event': 'FORMULARIO_REGISTRO'});                 
            }
          }
          //End Proceso tag manager

          //Se redirige al proceso de Orientación pprofesional
          ///setTimeout(function(){ window.location.href = res.urlcc; }, 3000);
          setTimeout(function(){ window.location.href = res.urlcc; }, 3000);
          //setTimeout(function(){ var nuevaVentana = window.open(res.urlcc, '_blank'); nuevaVentana.focus(); }, 3000);
          //End Se redirige al proceso de Orientación pprofesional
        },
        error: function(xhr, textStatus, error){
            console.log(xhr.statusText);
            console.log(textStatus);
            console.log(error);
        }
        
    })
      
      /*End Implementacion enviar datos BY SRP 17-01-2018*/

    } //Fin del else para la validacion del ciclo
  }
  /*End Metodo para Enviar a Impulsa BY SRP 17-01-2018*/






  sendFormularioExpuesto(formData, hiddenData) {
    let urlsFormularioTradicional = [
      "//local.unitec2017/wp-content/phpServeApp/backend0.php"
    ];

    let theData = [];

    theData.push(formData);
    theData.push(hiddenData);

    let response = this.send(theData, urlsFormularioTradicional);
    //return response
  }

  sendFormularioSearchGoogle(formData) {}
}
