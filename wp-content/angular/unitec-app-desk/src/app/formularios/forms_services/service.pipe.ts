import { Pipe, PipeTransform } from "@angular/core";

@Pipe({name: 'capitalize'})
export class CapitalizePipe implements PipeTransform {
    transform(value: any) {
        if(value) {
            return value.charAt(0).toUpperCase() + value.slice(1);
        }
        return value;
    }
}
// @Pipe({name: 'ciclos'})
// export class CiclosPipe implements PipeTransform {
//     transform(value: any) {
//         var tipoCiclo: any = value;
//         if(value) {
//             switch(value) {
//                 case "18-2":
//                 tipoCiclo = 
//                 break;
//                 case "18-3":
//                 tipoCiclo = 
//                 break;
//                 case "19-1":
//                 tipoCiclo = 
//                 break;
//             }
            
//         }
//         return value;
//     }
// }