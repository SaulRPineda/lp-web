import { Injectable } from "@angular/core";
import { formCookiesService } from "../forms_services/formCookies.service";
import { userValidations } from '../forms_services/validaciones.service';
import { CapitalizePipe } from "./service.pipe";
import { readJson } from "../../services/readJson.service";
import { GenericService } from "../../services/generic.service";

declare var $: any;
declare var jQuery: any;
declare var dataLayer: any;
declare var target:any;
declare var ga: any;
declare var $_GET : any;
// var safeUrl = (/unitec.mx/.test(window.location.href)) ? "https://unitecmx-universidadtecno.netdna-ssl.com" : "//" + document.domain;
var safeUrl = (/unitec.mx/.test(window.location.href)) ? "https://www.unitec.mx" : "//" + document.domain;

var path = "https://" + document.domain + "/";

var testingSub: any = "Actualizacion";

@Injectable()
export class sendService {
  ciclo_default = "";
  urlreferrer: any = window.location;
  private url_ajax_php = "/wp-content/phpServeApp/backend0.php";
  private url_ajax_php_traditionalForm = "/wp-content/phpServeApp/backend4.php";
  private urlDomain = "localhost:8888/unitec2017";
  private urlMicroRegistroPrueba = "https://www.unitec.mx/procWeb/microregistro.php";
  private urlMicroRegistroDeploy = "https://www.unitec.mx/desk/procWeb/microregistro.php";
  
  constructor(private formCookieService: formCookiesService, 
    private capitalizePipe: CapitalizePipe,
    private userValidations : userValidations,
    private readJsonService: readJson,
    private formCookiesService: formCookiesService,
    private genericService: GenericService
  ) {
    
  }

  $_GET = this.genericService.getMethod();

  sendMicroRegistro(evento = null) {
    /*Implementacion enviar datos BY SRP 17-01-2018*/
    //Convertir a JSON la Cookie
    // console.log(JSON.stringify(this.formCookieService.getCookieValues("c_form_data")).replace(/\\n/g,' '));
    // let dataForm = JSON.parse(JSON.stringify(this.formCookieService.getCookieValues("c_form_data")));
    let dataForm = this.formCookieService.getCookieValues("c_form_data");
    let talleres = true;
    // console.log("TODO BIEN AQUI");
    // console.log(dataForm);
    // url: "http://www.unitec.mx/desk/procWeb/microregistro.php",
    // url: "//" + document.domain + this.url_ajax_php,
console.warn(typeof this.formCookieService.getCookieValues("c_form_data"))
    if(talleres){
      console.warn("viene de talleres")  
    }
    jQuery
      .ajax({
        type: "POST",
        url: this.urlMicroRegistroDeploy,
        data: this.formCookieService.getCookieValues("c_form_data"),
        success: function (res) {
          console.log("Datos Enviados");
          console.log(res);
          
          //Proceso tag manager
          if (typeof dataLayer != 'undefined') {
                        
              dataLayer.push({'event': 'PASO1_COMPLETADO'});
              dataLayer.push({
                  'url_value': 'formularioInformacion/paso1', //dato dinámico
                  'event': 'StepForm'//dato estático
              });
          }
          //End Proceso tag manager

        },
        error: function (xhr, textStatus, error) {
          console.log(xhr);
          console.log(xhr.statusText);
          console.log(textStatus);
          console.log(error);
        }
      });
      /*End Implementacion enviar datos BY SRP 17-01-2018*/
  }
}
