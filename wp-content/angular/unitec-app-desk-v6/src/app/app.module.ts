//Componentes del core de angular y utils
import { BrowserModule           } from '@angular/platform-browser';
import { NgModule, ApplicationRef } from '@angular/core';
import { ReactiveFormsModule, FormsModule     } from '@angular/forms';
import { HttpModule, JsonpModule } from '@angular/http';
//Modulo para cookies
import { CookieService } from 'ngx-cookie-service';
//Componentes padre de formularios
//comentado por Luis import { formularioTradicionalComponent } from './formularios/forms_parent_components/formulario-tradicional/formulario-tradicional.component';
// import { formularioExpuestoComponent } from './formularios/forms_parent_components/formulario-expuesto/formulario-expuesto.component';
//import { formularioTradicionalOSComponent } from './formularios/forms_parent_components/formulario-tradicional-OS/formulario-tradicional-OS.component';
import { CommonModule } from "@angular/common";

//Componentes hijo del formulario
//comentado por Luis import { frmNombreComponent   } from './formularios/forms_child_components/frm-nombre/frm-nombre.component';
//comentado por Luis import { frmApaternoComponent } from './formularios/forms_child_components/frm-apaterno/frm-apaterno.component';
//comentado por Luis import { frmAmaternoComponent } from './formularios/forms_child_components/frm-amaterno/frm-amaterno.component';
//comentado por Luis import { frmMailComponent     } from './formularios/forms_child_components/frm-mail/frm-mail.component';
import { frmMailLoginComponent } from './formularios/forms_child_components/frm-mail-login/frm-mail-login.component'
// import { frmEstadoComponent   } from './formularios/forms_child_components/frm-estado/frm-estado.component';
//comentado por Luis import { frmEstadoExpuestoComponent   } from './formularios/forms_child_components/frm-estado-expuesto/frm-estado-expuesto.component';
import { frmBusquedaIntegradaComponent } from './formularios/forms_child_components/frm-busqueda-integrada/frm-busqueda-integrada.component';

//import { frmProductosExpuestosComponent } from './formularios/forms_child_components/frm-productos-expuestos/frm-productos-expuestos.component';
// import { frmLineasNegocioAppComponent   } from './formularios/forms_child_components/frm-lineas-negocio-app/frm-lineas-negocio-app.component';
//comentado por Luis import { frmCelularComponent  } from './formularios/forms_child_components/frm-celular/frm-celular.component';
//comentado por Luis import { frmEstudioInteresComponent } from './formularios/forms_child_components/frm-estudio-interes/frm-estudio-interes.component';
//comentado por Luis import { frmPoliticasCondiciones } from './formularios/forms_child_components/frm-politicas-condiciones/frm-politicas-condiciones.component';
// import { frmCicloComponent } from './formularios/forms_child_components/frm-ciclo/frm-ciclo.component';
//comentado por Luis import { frmRevalidacionComponent } from './formularios/forms_child_components/frm-revalidacion/frm-revalidacion.component';
import { frmProblemasCuenta } from './formularios/forms_child_components/frm-problemas-cuenta/frm-problemas-cuenta.component';
//Servicio de validaciones
import { userValidations } from "./formularios/forms_services/validaciones.service";
//Servicio para leer los Json
import { readJson } from "./services/readJson.service";
//Servicio para traer los Json
import { getJson } from "./services/getJson.service";
//Servicio WebSpeach
import { WebspeachService } from "./services/webspeach.service";
//Servicio de envío de los formularios
import { sendService } from './formularios/forms_services/send.service';
//Servicio de las reglas de los input
import { reglasInputsService } from './formularios/forms_services/reglasInputs.service';
//Servicio para autocomplete
import { AutocompleteService } from "./services/autocomplete.service";
//Servicio de metodos genericos
import { GenericService } from "./services/generic.service";
//Servicio para manipulacion de las cookies en el formulario
import {formCookiesService} from "./formularios/forms_services/formCookies.service";

//Componente modal
//Servico que proporcionara animaciones.
// import { Animaciones } from './utilities/animaciones';
import { ModalComponent } from './utilities/modal/modal.component';
// import { ModalFormularioTradicionalComponent } from './formularios/forms_parent_components/modal-formulario-tradicional/modal-formulario-tradicional.component';
//comentado por Luis import { ModalAppFormularioTradicionalComponent } from './formularios/forms_parent_components/modal-app-formulario-tradicional/modal-app-formulario-tradicional.component';
//comentado por Luis import { ModalAppFormularioTallerComponent } from './formularios/forms_parent_components/modal-app-formulario-taller/modal-app-formulario-taller.component';
//import { MentionModule } from 'angular2-mentions/mention';
import { nextOnEnterDirective } from './directives/nextOnenter.directive';
import { FormularioGCSComponent } from './buscador/formularioGCS/formularioGCS.component';
import { FormularioLoginComponent } from './formularios/forms_parent_components/formulario-login/formulario-login.component';
import { FrmNcuentaComponent } from './formularios/forms_child_components/frm-ncuenta/frm-ncuenta.component';
import { FrmRadioPerfilAlumnoMaestroComponent } from './formularios/forms_child_components/frm-radio-perfil-alumno-maestro/frm-radio-perfil-alumno-maestro.component';
import { FrmAvisoDePrivacidadComponent } from './formularios/forms_child_components/frm-aviso-de-privacidad/frm-aviso-de-privacidad.component';
//import {TypewriterModule} from 'ng2-typewriter';
// import { MiniSimuladorComponent } from './mini-simulador/mini-simulador.component';
import { SearchSideNavComponent } from './buscador/search-side-nav/search-side-nav.component';
//comentado por Luis import { ModalCalculadoraFormularioTradicionalComponent } from './formularios/forms_parent_components/modal-calculadora-formulario-tradicional/modal-calculadora-formulario-tradicional.component';
//comentado por Luis import { FormularioSolitarioComponent } from './formularios/forms_parent_components/formulario-solitario/formulario-solitario.component';
import { CapitalizePipe } from './formularios/forms_services/service.pipe';
//comentado por Luis import { formularioTallerComponent } from './formularios/forms_parent_components/formulario-taller/formulario-taller.component';

@NgModule({
  declarations: [
//comentado por Luis    formularioTradicionalComponent,
//comentado por Luis    formularioTallerComponent,
    //formularioTradicionalOSComponent,
//comentado por Luis    frmNombreComponent,
//comentado por Luis    frmApaternoComponent,
//comentado por Luis    frmAmaternoComponent,
//comentado por Luis    frmMailComponent,
    frmMailLoginComponent,
    // frmEstadoComponent,
//comentado por Luis    frmEstadoExpuestoComponent,
    //frmProductosExpuestosComponent,
    // frmLineasNegocioAppComponent,
//comentado por Luis    frmCelularComponent,
//comentado por Luis    frmEstudioInteresComponent,
//comentado por Luis    frmPoliticasCondiciones,
    frmProblemasCuenta,
    // frmCicloComponent,
//    frmRevalidacionComponent,
    frmBusquedaIntegradaComponent,
    ModalComponent,
    // ModalFormularioTradicionalComponent,
//comentado por Luis    ModalAppFormularioTradicionalComponent,
//comentado por Luis    ModalAppFormularioTallerComponent,
    nextOnEnterDirective,
    FormularioGCSComponent,
    FormularioLoginComponent,
    FrmNcuentaComponent,
    FrmRadioPerfilAlumnoMaestroComponent,
    FrmAvisoDePrivacidadComponent,
    // MiniSimuladorComponent,
    SearchSideNavComponent,
//comentado por Luis    ModalCalculadoraFormularioTradicionalComponent,
//comentado por Luis    FormularioSolitarioComponent,
    CapitalizePipe,
    // formularioTallerComponent

  ],
  imports: [
    BrowserModule,
    FormsModule,
    ReactiveFormsModule,
    HttpModule,
    JsonpModule,
    CommonModule
  ],
  exports: [
    FormsModule,
    ReactiveFormsModule
  ],
  providers: [ReactiveFormsModule,
  CookieService, 
  userValidations, 
  readJson, 
  getJson, sendService, /*Animaciones,*/
  reglasInputsService, 
   WebspeachService, AutocompleteService, GenericService, 
   formCookiesService, 
   CapitalizePipe],
  entryComponents: [FormularioLoginComponent, FormularioGCSComponent, 
//comentado por Luis  formularioTradicionalComponent, 
//comentado por Luis  formularioTallerComponent, 
//comentado por Luis  FormularioSolitarioComponent, 
  ModalComponent, 
//comentado por Luis  ModalAppFormularioTradicionalComponent, 
//comentado por Luis  ModalAppFormularioTallerComponent, 
  SearchSideNavComponent, 
  //comentado por LuisModalCalculadoraFormularioTradicionalComponent, 
  ]
})
//Clase para construir la funcionalidad del modulo
export class AppModule { 
    //Carga condicional de componentes padre
    ngDoBootstrap( ref: ApplicationRef ) {
      //Configuracion para hacer funcionar en producion el elemento buscadorGCS
//comentado por Luis      if (location.href.indexOf( "buscador" ) !== -1 ) {
//comentado por Luis        ref.bootstrap( FormularioGCSComponent );
//comentado por Luis      } else 
      if(location.href.indexOf( "biblioteca-virtual" ) !== -1 ){
      //Carga el componente Login para ingresar a la biblioteca Virtual
        ref.bootstrap( FormularioLoginComponent );
        ref.bootstrap( ModalComponent );
        // ref.bootstrap( ModalFormularioTradicionalComponent );
//comentado por Luis        ref.bootstrap( ModalAppFormularioTradicionalComponent );
        ref.bootstrap( SearchSideNavComponent );
//comentado por Luis      } else if (location.href.indexOf( "calcula-tu-beca" ) !== -1 ){
//comentado por Luis        ref.bootstrap( ModalCalculadoraFormularioTradicionalComponent );
//comentado por Luis      } else if (location.href.indexOf( "calcula" ) !== -1 ){
//comentado por Luis        ref.bootstrap( ModalCalculadoraFormularioTradicionalComponent );
//comentado por Luis      } else if (location.href.indexOf( "prueba-performance" ) !== -1 ){
//comentado por Luis        ref.bootstrap(ModalAppFormularioTallerComponent );
//comentado por Luis      } else if ((location.href.indexOf("conexion-unitec") !== -1) || (location.href.indexOf("agendar-cita") !== -1)){

      } else {
      //Formulario
        ref.bootstrap( ModalComponent );
        // ref.bootstrap( ModalFormularioTradicionalComponent );
//comentado por Luis        ref.bootstrap( ModalAppFormularioTradicionalComponent );
        ref.bootstrap( SearchSideNavComponent );
      }
    }
}