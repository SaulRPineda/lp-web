import { Injectable } from '@angular/core';

declare let jQuery: any;

@Injectable()

export class NavegadorService {

    constructor() {}

    whatsBrowser(){
        // if( ( /chrom(e|ium)/.test( navigator.userAgent.toLowerCase() ) ) ){
        //     return('Chrome');
        // }else{
        //     return ( "no definido" )
        // }
        var isWebkit = 'WebkitAppearance' in document.documentElement.style &&  !/Edge/.test(navigator.userAgent);
        if( isWebkit ){
            return('Chrome');
        }else{
            return ( "no definido" )
        }
    }

    speechSupport(){
        if (!('webkitSpeechRecognition' in window)) {
            return false;
        } else {
            return true;
        }
    }
}