import { Injectable } from "@angular/core";
import { formCookiesService } from "../forms_services/formCookies.service";

declare var $: any;
declare var jQuery: any;
declare var dataLayer: any;
declare var ga: any;
// var safeUrl = (/unitec.mx/.test(window.location.href)) ? "https://unitecmx-universidadtecno.netdna-ssl.com" : "//" + document.domain;
var safeUrl = (/unitec.mx/.test(window.location.href)) ? "https://www.unitec.mx" : "//" + document.domain;

@Injectable()
export class sendService {
  ciclo_default = "";
  urlreferrer: any = window.location;
  private url_ajax_php = "/wp-content/phpServeApp/backend0.php";
  private urlDomain = "//localhost:8888/unitec2017";
  private urlMicroRegistroPrueba = "https://www.unitec.mx/procWeb/microregistro.php";
  private urlMicroRegistroDeploy = "https://www.unitec.mx/desk/procWeb/microregistro.php";
  
  constructor(private formCookieService: formCookiesService) {}

  send(theData, urls) {
    console.log(theData);
    for (let i in urls) {
      jQuery
        .ajax({
          type: "GET",
          url: urls[i],
          data: { theData: theData }
        })
        .done(function(resultado) {
          console.log( resultado );
          return resultado;
        });
    }
  }

  sendMicroRegistro(evento = null) {
    /*Implementacion enviar datos BY SRP 17-01-2018*/
    //Convertir a JSON la Cookie
    // console.log(JSON.stringify(this.formCookieService.getCookieValues("c_form_data")).replace(/\\n/g,' '));
    // let dataForm = JSON.parse(JSON.stringify(this.formCookieService.getCookieValues("c_form_data")));
    let dataForm = this.formCookieService.getCookieValues("c_form_data");
    console.log("TODO BIEN AQUI");
    // url: "http://www.unitec.mx/desk/procWeb/microregistro.php",
    // url: "//" + document.domain + this.url_ajax_php,

    jQuery
      .ajax({
        type: "POST",
        url: this.urlMicroRegistroDeploy,
        data: this.formCookieService.getCookieValues("c_form_data"),
        success: function (res) {
          console.log("Datos Enviados");
          console.log(res);
          
          //Proceso tag manager
          if (typeof dataLayer != 'undefined') {
                        
            dataLayer.push({'event': 'PASO1_COMPLETADO'});
            dataLayer.push({
                'url_value': 'formularioInformacion/paso1', //dato dinámico
                'event': 'StepForm'//dato estático
            });
        }
        //End Proceso tag manager

        },
        error: function (xhr, textStatus, error) {
          console.log(xhr);
          console.log(xhr.statusText);
          console.log(textStatus);
          console.log(error);
        }
      });
  }
}
