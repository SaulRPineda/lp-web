import { Component, OnInit, Input, AfterViewInit } from "@angular/core";
import { GenericService } from "../../../services/generic.service";
//Servicio de cookie
import { formCookiesService } from "../../forms_services/formCookies.service";
import { ViewEncapsulation } from '@angular/core';

declare let jQuery;
declare let $;
declare let wistiaApi: any;
// var safeUrl = (/unitec.mx/.test(window.location.href)) ? "https://unitecmx-universidadtecno.netdna-ssl.com" : "//" + document.domain;
var safeUrl = (/unitec.mx/.test(window.location.href)) ? "https://www.unitec.mx" : "//" + "localhost:8888/unitec2017";

@Component({
  selector: "modal-app-formulario-tradicional",
  templateUrl: "./modal-app-formulario-tradicional.component.html",
  styleUrls: ["./modal-app-formulario-tradicional.component.scss"],
  providers: [formCookiesService],
  encapsulation: ViewEncapsulation.Emulated
})
export class ModalAppFormularioTradicionalComponent implements OnInit, AfterViewInit {
  titulo_modal_formulario: any = "Solicita Información";
  recurso_predeterminado: any = "//www.youtube.com/embed/ESLIW1la8LY";
  constructor(
    private genericService: GenericService,
    private formCookiesService: formCookiesService
  ) {
    //Al cargar el dom
    jQuery(document).ready(function () {
      var y;
      var x;
      //Deshabilidar el scroll al abrir el modal
      jQuery("#modal_frm_app").on("shown.bs.modal", function (e) {
        //e.preventDefault();
        //var x=window.scrollX;
        //var y=window.scrollY;
        //window.onscroll=function(){window.scrollTo(x, y);};
        console.log(window.scrollY);
        x = window.scrollX;
        y = window.scrollY;
        $('body').css('overflow', 'hidden');
        //jQuery("body").css({ "overflow-y" : "hidden" });
        jQuery("html").css({ "position": "fixed" });
        jQuery("body").css({ "position": "fixed" });

        //window.onscroll=function(){window.scrollTo(x, y);};
      });

      jQuery("#modal_frm_app").on("hidden.bs.modal", function (e) {
        console.log("Aqui y: " + y);
        //window.onscroll=function(){};
        //jQuery("html").css({ "overflow-y" : "" });
        jQuery("body").css({ "overflow": "" });

        jQuery("html").css({ "position": "" });
        jQuery("body").css({ "position": "" });
        // console.log("jeab - pos closed" + y);
        window.scrollTo(x, y);
      });

      //Saltar paso 1 del formulario
      function saltarRegistro() {
        //Setear el primer titulo
        jQuery(".app-menu-title").html("Selecciona tu estado");
        //Preparar para el segundo paso o aplicacion (Se oculta todo lo del primer paso y se muestra lo del segundo paso)
        jQuery(".modal-header-formulario").hide("slow");
        jQuery(".frm-politicas").hide("slow");
        jQuery("#paso1").hide("slow");
        jQuery("#continuarTrd").hide("slow");
        jQuery("#paso2").show("slow");
        jQuery(".modal-header-app").show("slow");
        jQuery(".modal-body-form").show("slow");
        //Quitamos el margen para el formulario
        jQuery(".modal-body").css("margin-top", "0px");
        //Deshabilitar flecha izquierda después de llenar el formulario no es posible regresar
        jQuery(".app-left-arrow").css("visibility", "hidden");
        //Colocar el evento siguiente a la flecha derecha por el momento es seleccionar linea de negocio
        //Recibe evento selector y nombrepaso (linea, campus, producto, ciclo)
        //bindNavButtons("click", ".app-rigth-arrow", "linea");
        //Remover el titulo que se le puso al video y cerrarlo
        jQuery(".titulo-logo").remove();
        jQuery(".modal-body").removeClass("margen-para-video");

        //Si existe video se cierra
        try {
          $("#iframeytform")[0].contentWindow.postMessage(
            '{"event":"command","func":"' + "stopVideo" + '","args":""}',
            "*"
          );
        } catch (err) {
          console.log(
            "No existe la instancia del video youtube para detenerlo"
          );
        }
      }

      //Variables de inicializacion
      //Titulo del modal
      var titulo_modal_formulario: any = "Solicita Información";
      //Recurso predeterminado en caso de que no exista imagen destacada o video wistia/youtube
      var recurso_predeterminado: any = "//www.youtube.com/embed/ESLIW1la8LY";
      //Validacion para saber si el tipo recurso [wistia/youtube/imagen]
      var tipo_recurso = "";
      //Url del archivo que se necesita descargar para evaluar la velocidad de la conexion para saber si se autoreproduce
      var url_recurso_validar_descarga =
        "//unitecmx-universidadtecno.netdna-ssl.com/wp-content/themes/temaunitec/calculadora/js/min/bootstrap-slider.min.js";
      //El limite de kbps para iniciar la autoreproduccion del video
      var kbps_para_autoplay = 700;
      //Variable para validar si la cookie ya tiene valores
      var valores_cookie = "false";
      //Variable para saber en que paso ponemos al prospecto dependiendo los valores de la cookie [0 = se muestra todo, 1 = paso 1 completado, 2 = paso 2 completado]
      var paso_cookie_formulario = 0;
      //Variable donde se almacenan los valores de la cookie
      var arr_obj_cookie_values = "";
      //Si presionamos el boton chevron left cerramos el modal
      jQuery("#head_close_modal").on("click", function () {
        jQuery("#modal_frm_app").modal("hide");
      });
      if (location.href.indexOf("testvocacional") == -1 && location.href.indexOf("orientacion-profesional") == -1 && location.href.indexOf("impulsa") == -1) {
        //Validacion para saber si existe la cookie y si existe ya no se muestra el formulario se manda a la thank you page JEAB
        //Si ya existe la cookie no se muestra el formulario se abre la página thankyou o la página personalizada
        //La cookie debe tener todos los valores del formulario lleno por completo
        if (formCookiesService.checkCookie("c_form_data") == true) {
          //Obtener todos los valores de la cookie
          arr_obj_cookie_values = formCookiesService.getCookieValues(
            "c_form_data"
          );
          console.log(arr_obj_cookie_values);
          //Validamos los valores de la cookie para ver si ya no le pedimos los datos de contacto
          if (
            arr_obj_cookie_values["nombre"] != "" &&
            typeof arr_obj_cookie_values["nombre"] != "undefined" &&
            (arr_obj_cookie_values["apaterno"] != "" &&
              typeof arr_obj_cookie_values["apaterno"] != "undefined") &&
            (arr_obj_cookie_values["amaterno"] != "" &&
              typeof arr_obj_cookie_values["amaterno"] != "undefined") &&
            (arr_obj_cookie_values["celular"] != "" &&
              typeof arr_obj_cookie_values["celular"] != "undefined") &&
            (arr_obj_cookie_values["email"] != "" &&
              typeof arr_obj_cookie_values["email"] != "undefined") &&
            (arr_obj_cookie_values["tipoRegistro"] != "" &&
              typeof arr_obj_cookie_values["tipoRegistro"] != "undefined")
          ) {
            //Formulario de registro completado
            paso_cookie_formulario = 1;
            valores_cookie = "true";
          }
          //Validamos si tiene los valores necesarios para pasarlo hasta la thankyou page
          if (
            arr_obj_cookie_values["campus"] != "" &&
            typeof arr_obj_cookie_values["campus"] != "undefined" &&
            (arr_obj_cookie_values["carrera"] != "" &&
              typeof arr_obj_cookie_values["carrera"] != "undefined") &&
            (arr_obj_cookie_values["categoria"] != "" &&
              typeof arr_obj_cookie_values["categoria"] != "undefined") &&
            (arr_obj_cookie_values["ciclo"] != "" &&
              typeof arr_obj_cookie_values["ciclo"] != "undefined") &&
            (arr_obj_cookie_values["estado"] != "" &&
              typeof arr_obj_cookie_values["estado"] != "undefined") &&
            (arr_obj_cookie_values["carreraInteres"] != "" &&
              typeof arr_obj_cookie_values["carreraInteres"] != "undefined") &&
            (arr_obj_cookie_values["linea"] != "" &&
              typeof arr_obj_cookie_values["linea"] != "undefined") &&
            (arr_obj_cookie_values["modalidad"] != "" &&
              typeof arr_obj_cookie_values["modalidad"] != "undefined") &&
            (arr_obj_cookie_values["nivelInteres"] != "" &&
              typeof arr_obj_cookie_values["nivelInteres"] != "undefined") &&
            (arr_obj_cookie_values["subNivelInteres"] != "" &&
              typeof arr_obj_cookie_values["subNivelInteres"] != "undefined") &&
            (arr_obj_cookie_values["tipoRegistro"] != "" &&
              typeof arr_obj_cookie_values["tipoRegistro"] != "undefined")
          ) {
            //Seleccion de carrera/campus/modalidad completado
            paso_cookie_formulario = 2;
            valores_cookie = "true";
          }
        }
      }
      //Si existe la cookie validamos a que paso del formulario lo saltamos
      if (valores_cookie == "true") {
        //Como tiene valores en la cookie validamos de que paso tiene los valores de la cookie completos
        //En este caso saltamos el registro por que ya tenemos los datos
        if (paso_cookie_formulario == 1) {
          jQuery(".modal-header-app").hide();
          jQuery(".modal-header-formulario").hide();
          jQuery(".modal-body-form").hide();
          saltarRegistro();
          //En este caso ya tiene registro y la interaccion completa lo saltamos ala thank you page
        } else if (paso_cookie_formulario == 2) {
          //Al mostrar el modal ocultamos los pasos que no corresponden
          //Y llenamos los datos de la thkyp con la cookie
          jQuery("#modal_frm_app").on("show.bs.modal", function (e) {
            //Ocultar en caso de que ya exista
            jQuery(".modal-header-app").hide();
            jQuery(".modal-header-formulario").hide();
            jQuery(".modal-body-form").hide();
            //Llenar los campos de la thankyou page con los valores de la cookie
            jQuery(".tercer-texto-thankyou").html(
              formCookiesService.getCookieByKey("c_form_data", "carrera")
            );
            jQuery("#nombre-thankyou").html(
              formCookiesService.getCookieByKey("c_form_data", "nombre")
            );
            //Mostrar la thankyou page
            jQuery(".modal-header-thankyou").show();
            jQuery(".modal-body-thankyou").show();
            jQuery(".img-header-thankyou").css({
              "background-image": "url(" + jQuery("#h_horizontal_url_imagen_destacada").val() + ")"
            });
          });

          if (location.href.indexOf("testvocacional") !== -1 || location.href.indexOf("orientacion-profesional") !== -1 || location.href.indexOf("impulsa") !== -1) {
            jQuery("#calculadoraModal").addClass("sendCalculadoraModal");
          }

        }

        //Si no existe seguimos el flujo normal
      } else {
        //Obtenemos la url del recurso para formulario del input hidden url_multimedia_formulario
        var url_multimedia_formulario = jQuery(
          "#h_url_multimedia_formulario"
        ).val();
        if (
          url_multimedia_formulario == "" ||
          typeof url_multimedia_formulario == "undefined"
        ) {
          //Si no tiene video de youtube o wistia obtenemos la imagen destacada
          url_multimedia_formulario = jQuery("#h_horizontal_url_imagen_destacada").val();
          //Si no existe la imagen destacada cargamos el recurso predeterminado
          if (
            url_multimedia_formulario == "" ||
            typeof url_multimedia_formulario == "undefined"
          ) {
            url_multimedia_formulario = recurso_predeterminado;
          }
        }

        //Validamos si es imagen video de youtube o video de wistia
        if (url_multimedia_formulario.indexOf("youtube") != -1) {
          tipo_recurso = "youtube";
        } else if (url_multimedia_formulario.indexOf("wistia") != -1) {
          tipo_recurso = "wistia";
        } else if (url_multimedia_formulario.indexOf("jpg") != -1 || url_multimedia_formulario.indexOf("png") != -1 || url_multimedia_formulario.indexOf("jpge") != -1 || url_multimedia_formulario.indexOf("gif") != -1) {
          tipo_recurso = "imagen";
        }

        //Validamos el tipo de recurso para cargar el html dependiendo de eso
        console.log("Tipo recurso" + tipo_recurso);
        // if (tipo_recurso == "youtube") {
        //   console.log("Recurso YT");
        //   jQuery(".contenedor-header-p").addClass("margin-padding-video");
        //   jQuery(".contenedor-header-p").html(youtube_div);
        //   jQuery(".modal-body-form").prepend(header_multimedia_div);
        //   jQuery(".modal-body-form").addClass("margen-para-video");
        //   jQuery(".modal-header-formulario").addClass(
        //     "modal-header-formulario-ytv"
        //   );
        //   //Detectar si se abrio el modal y si es video y con que red para
        //   //Autoreproducir segun los parametros
        //   jQuery("#modal_frm_app").on("show.bs.modal", function(e) {
        //     autoPlayVideos(
        //       tipo_recurso,
        //       url_recurso_validar_descarga,
        //       kbps_para_autoplay
        //     );
        //     console.log("Se abrio el modal para youtube");
        //   });
        //   //Al ocultar el modal
        //   jQuery("#modal_frm_app").on("hide.bs.modal", function(e) {
        //     console.log("Se cerro el modal youtube");
        //     $("#iframeytform")[0].contentWindow.postMessage(
        //       '{"event":"command","func":"' + "stopVideo" + '","args":""}',
        //       "*"
        //     );
        //   });
        //   console.log("Recurso youtube");
        // } else if (tipo_recurso == "wistia") {
        //   console.log("Recurso wistia");
        //   jQuery(".contenedor-header-p").addClass("margin-padding-video");
        //   jQuery(".contenedor-header-p").html(wistia_div);
        //   jQuery(".modal-body-form").prepend(header_multimedia_div);
        //   //Añadir clases para ajustar el header
        //   jQuery(".modal-header-formulario").addClass("header-wistia");
        //   jQuery(".modal-header-formulario").addClass(
        //     "modal-header-formulario-ytv"
        //   );
        //   //Al abrir el modal
        //   jQuery("#modal_frm_app").on("show.bs.modal", function(e) {
        //     jQuery(".contenedor-header-p").html(wistia_div);
        //     autoPlayVideos(
        //       tipo_recurso,
        //       url_recurso_validar_descarga,
        //       kbps_para_autoplay
        //     );
        //     console.log("Se abrio el modal wistia");
        //   });

        //   //Al ocultar el modal
        //   jQuery("#modal_frm_app").on("hide.bs.modal", function(e) {
        //     console.log("Se cerro el modal wistia");
        //     $(".wistia_embed")
        //       .empty()
        //       .remove();
        //   });
        // } else if (tipo_recurso == "imagen") {
        //   console.log("Recurso imagen");
        //   //Asignar la clase para corregir posicion horizontal en caso de ser imagen
        //   jQuery(".modal-header-formulario").addClass("modal-header-form-img-h");
        //   jQuery(".img-header-formulario").css({
        //     "background-image": "url(" + url_multimedia_formulario + ")"
        //   });
        // }

        if (tipo_recurso == "imagen") {
          console.log("Recurso imagen");
          // Implementación de videos personalizados en formularios by AMC
          var videos_campus = jQuery("#post_id").val();
          var page: string;
          var html = "";
          var nameVideo;
          switch (videos_campus) {
              case "340":
                  page = "ATZ";
                  nameVideo = "campus-atizapan-f";
                  break;
              case "342":
                  page = "CUI";
                  nameVideo = "campus-cuitlahuac-f";
                  break;
              case "219":
                  page = "ECA";
                  nameVideo = "campus-ecatepec-f";
                  break;
              case "348":
                  page = "GDL";
                  nameVideo = "campus-guadalajara-f";
                  break;
              case "350":
                  page = "LEO";
                  nameVideo = "campus-leon-f";
                  break;
              case "352":
                  page = "MAR";
                  nameVideo = "campus-marina-f";
                  break;
              case "1057":
                  page = "REY";
                  nameVideo = "campus-los-reyes-f";
                  break;
              case "354":
                  page = "QRO";
                  nameVideo = "campus-queretaro-f";
                  break;
              case "356":
                  page = "SUR";
                  nameVideo = "campus-sur-f";
                  break;
              case "358":
                  page = "TOL";
                  nameVideo = "campus-toluca-f";
                  break;
                  
              case "309":
              case "9821":
              case "9597":
              case "7314":
              case "9919":
              case "9283":
              case "9505":
              case "9589":
                  page = "PREPA";
                  nameVideo = "unitec-preparatoria-f";
                  break;

              case "254":
              case "255":
              case "256":
              case "257":
              case "1031":
              case "261":
              case "263":
              case "264":
              case "266":
              case "1559":
              case "1069":
                  page = "ING";
                  nameVideo = "ingenierias-unitec-f";
                  break;

              case "287":
                  page = "GASTRO";
                  nameVideo = "gastronomia-unitec-f";
                  break;

              case "233":
              case "249":
              case "250":
              case "251":
              case "252":
              case "253":
              case "1071":
              case "8866":
                  page = "ODONTO";
                  nameVideo = "odontologia-unitec-f";
                  break;

              case "1078":
              case "235":
              case "284":
              case "291":
                  page = "SALUD";
                  nameVideo = "csalud-unitec-f";
                  break;

              case "13":
              case "207":
              case "212":
              case "33":
              case "35":
              case "37":
              case "1100":
              case "1112":
              case "4068":
              case "8765":
                  page = "INSTALACIONES";
                  nameVideo = "unitec-instalaciones";
                  break;

              default:
                  page = "DEFAULT";
                  nameVideo = "";
                  break;
          }
          // html += '<video autoplay="autoplay" muted="muted" loop="loop" webkit-playsinline="" playsinline="" noaudio="" height="100%" width="120%">';
          //   html += '<source src="' + nameVideo + '" type="video/mp4">';
          //   html += '<source src="' + videoPersonalizadoWEBM + '" type="video/webm">';
          //   html += '<source src="' + videoPersonalizadoOGG + '" type="video/ogg">';
          //   html += '<img src="' + posterPersonalizado + '" alt="Video no soportado">';
          //   html += '<p class="warning">Su navegador no soporta video HTML5.</p>';
          // html += '</video>';
          // jQuery('.img-header-formulario').empty();
          // jQuery('.img-header-formulario').append(html);

          if(page === 'DEFAULT'){
            //Asignar la clase para corregir posicion horizontal en caso de ser imagen
            jQuery(".modal-header-formulario").addClass("modal-header-form-img-h");
            jQuery(".img-header-formulario").css({ "background-image": "url(" + url_multimedia_formulario + ")" });
          } else{
              html += '<video autoplay="autoplay" muted="muted" loop="loop" webkit-playsinline="" playsinline="" noaudio="" height="100%" width="120%">';
                html += '<source src="https://unitecmx-universidadtecno.netdna-ssl.com/videos/' + nameVideo + '.mp4" type="video/mp4">';
                html += '<source src="https://unitecmx-universidadtecno.netdna-ssl.com/videos/' + nameVideo + '.webm" type="video/webm">';
                html += '<source src="https://unitecmx-universidadtecno.netdna-ssl.com/videos/' + nameVideo + '.ogg" type="video/ogg">';
                html += '<img src="https://unitecmx-universidadtecno.netdna-ssl.com/videos/' + nameVideo + '.jpg" alt="Video no soportado">';
                html += '<p class="warning">Su navegador no soporta video HTML5.</p>';
              html += '</video>';
            jQuery('#videoPersonalizado').empty();
            jQuery('#videoPersonalizado').append(html);
          }
        // Implementación de videos personalizados en formularios by AMC
        } else {
          jQuery(".modal-header-formulario").css({
            "height": "70px"
          });
          jQuery(".img-header-formulario").hide();
        }
      }
    }); //Fin de document ready
  } //Fin del constructor

  //Hook ngOnInite
  ngOnInit() { }

  ngAfterViewInit() {
    this.cambioTextoThankYou();
  }

  //link para abrir el boton de calculadora
  calculadora() {
    window.open("/calcula-tu-beca", "_self");
  }

  cambioTextoThankYou() {
    if (location.href.indexOf("impulsa") !== -1) {
      console.log("impulsa");
      jQuery("#continuarTrdThank").html("CONOCE LAS VACANTES");
      var get_url = localStorage.getItem("vacantes");
      // jQuery("#div-btn-continuar a").attr({"href": get_url, "target":"_blank"});
      jQuery("#div-btn-continuar").hide();
      // jQuery("#calculadoraModal").addClass("sendCalculadoraModal");
      jQuery("#descripcionThanks").html("A través de UNITEC Te Impulsa, descubrirás que trabajar y estudiar se vuelve más fácil cuando un equipo de profesionales de apoyan para llegar a tu meta. En breve nos pondremos en contacto contigo para ofrecerte más información.");

    } else if (location.href.indexOf("testvocacional") !== -1 || location.href.indexOf("orientacion-profesional") !== -1) {
      console.log("Orientacion Profesional");
      jQuery("#continuarTrdThank").html("COMIENZA EL TEST");
      var get_url = localStorage.getItem("urlcc");
      // jQuery("#div-btn-continuar a").attr({"href":get_url,"target":"_blank"});
      jQuery("#div-btn-continuar").hide();
      // jQuery("#calculadoraModal").addClass("sendCalculadoraModal");
      jQuery("#descripcionThanks").html("Descubre cuáles son tus aptitudes y qué profesión se ajusta a tu perfil. Espera unos instantes y comenzará el test vocacional.");

    } else {
      jQuery("#continuarTrdThank").html("CALCULA TU BECA");
      jQuery("#div-btn-continuar a#continuarTrdThank").attr("href", "/calcula-tu-beca");
      jQuery("#descripcionThanks").html("Impulsamos tu esfuerzo recompensándote con una Beca Académica de Primer Ingreso. Conoce el porcentaje que te corresponde según tu promedio.");
    }
  }
} //Fin de clase
