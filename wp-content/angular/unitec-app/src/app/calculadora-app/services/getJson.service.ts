/**
 * 9-05-2017: JEAB - Servicio que consume archivos Json en los diferentes constructores.
 * 
 */
import { Injectable, Inject } from "@angular/core";
import { Http, Headers, RequestOptions, Response } from "@angular/http";
import { DOCUMENT } from "@angular/platform-browser";
import "rxjs/add/operator/toPromise";
import { Observable } from "rxjs/Rx";
import "rxjs/add/operator/map";
import "rxjs/add/operator/catch";
import { AbstractControl } from "@angular/forms";
declare var $: any;
declare var jQuery: any;

@Injectable()
export class getJson {
  private headers = new Headers({ Accept: "application/json" });
  private options = new RequestOptions({ headers: this.headers });
  private protocol = "https:";
  private urlJsonBasura = "/wp-content/themes/temaunitec/assets/frontend/json/min/json_basura.min.json";
  private urlJsonBasuraEmail = "/wp-content/themes/temaunitec/assets/frontend/json/min/json_basuraEmail.min.json";
  private urlJsonCarreras = "/wp-content/themes/temaunitec/assets/frontend/json/min/json_calc_carreras.min.json";
  //Se agrega nueva url para obtener los links y nombre de las categorias de las carreras
  private urlJsonLinksCategorias = "/wp-content/themes/temaunitec/assets/frontend/json/min/json_col_linkPaginas.min.json";
  private $: any;

  //Constructor
  constructor(private http: Http, @Inject(DOCUMENT) private document) {

  }

  //Para las llamdas ajax https en produccion no se debe colocar el dominio, solo la ruta relativa
  //Funcion para obtener el json de carreras
  getJsonCarreras() {
    jQuery
      .ajax({
        type: "GET",
        //url: this.urlJsonCarreras,
        url: "//" + document.domain + this.urlJsonCarreras,
        //url: "//" + this.urlJsonCarreras,
        "Content-Type": "application/json",
        dataType: "json",
        success: function(resultado) {}
      })
      .done(function(resultado) {
        localStorage.setItem("jsonCarreras", JSON.stringify(resultado));
      });
  }

  //Funcion para obtener el json de links y categorias
  getJsonLinksCategorias() {
    jQuery
      .ajax({
        type: "GET",
        //url: this.urlJsonLinksCategorias,
        url: "//" + document.domain + this.urlJsonLinksCategorias,
        //url: "//" + this.urlJsonLinksCategorias,
        "Content-Type": "application/json",
        dataType: "json",
        success: function(resultado) {}
      })
      .done(function(resultado) {
        localStorage.setItem("jsonLinksCategorias", JSON.stringify(resultado));
      });
  }

  //Funcion para traer el Json de basura con jquery ajax
  getJsonBasura() {
    jQuery
      .ajax({
        type: "GET",
        //url: this.urlJsonBasura,
        url: "//" + document.domain + this.urlJsonBasura,
        //url: "//" + this.urlJsonBasura,
        "Content-Type": "application/json",
        dataType: "json",
        success: function(resultado) {}
      })
      .done(function(resultado) {
        jQuery("#data_json_basura").val(1);
        jQuery("#data_json_basura").data(JSON.parse(JSON.stringify(resultado)));
        //console.log($("#data_json_basura").data());
      });
  }
  //Funcion para traer el Json de email basura con jquery ajax
  getJsonBasuraEmail() {
    jQuery
      .ajax({
        type: "GET",
        //url: this.urlJsonBasuraEmail,
        url: "//" + document.domain + this.urlJsonBasuraEmail,
        //url: "//" + this.urlJsonBasuraEmail,
        "Content-Type": "application/json",
        dataType: "json",
        success: function(resultado) {}
      })
      .done(function(resultado) {
        jQuery("#data_json_basura_email").val(1);
        jQuery("#data_json_basura_email").data(
          JSON.parse(JSON.stringify(resultado))
        );
        //console.log($("#data_json_basura").data());
      });
  }
  //Funcion para traer el Json de basura con observable
  getBasuraObs() {
    let response;
    let subscriptionBasuraJson = this.http
      .get( "//" + document.domain + this.urlJsonBasura, this.options)
      .map((res: any) => res.json())
      .catch((error: any) => {
        return Observable.throw(error.statusText);
      });
    subscriptionBasuraJson.subscribe(
      res => {
        response = res;
      },
      error => {
        console.log(error);
      },
      () => {
        console.log("done");
        if (this.document.readyState == "complete") {
          this.document.getElementById("data_json_basura").value = true;
          this.document.getElementById(
            "data_json_basura"
          ).dataset.json = JSON.stringify(response);
          //this.document.body.data = JSON.parse(JSON.stringify(res));
        }
      }
    );
  }
}
