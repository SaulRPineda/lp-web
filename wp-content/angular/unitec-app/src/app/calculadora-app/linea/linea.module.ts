import { ModuleWithProviders, NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { LineaComponent } from './../linea/linea.component';
import { AuthGuard, SharedModule } from '../shared';

//Importar servicio generico JEAB
import { GenericService } from '../../services/generic.service';

const lineaRouting: ModuleWithProviders = RouterModule.forChild([
  {
    path: 'linea',
    component: LineaComponent,
    canActivate: [AuthGuard]
  }
]);

@NgModule({
  imports: [
    SharedModule,
    lineaRouting
  ],
  declarations: [
    LineaComponent
  ],
  providers: [
    GenericService
  ]
})
export class LineaModule { }
