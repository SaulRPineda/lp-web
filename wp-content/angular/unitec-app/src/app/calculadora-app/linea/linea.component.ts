import { Component, ViewEncapsulation, Input, OnInit } from '@angular/core';
import { FormBuilder,FormGroup } from '@angular/forms';
import { Router } from '@angular/router';
import { User, UserService, DatosService } from '../shared';
//Importar servicio generico JEAB
import { GenericService } from '../../services/generic.service';

declare var jQuery : any;
declare var $ :any;

@Component({
  selector: 'linea',
  templateUrl: './linea.component.html',
  styleUrls: ['./linea.component.scss'],
  encapsulation: ViewEncapsulation.None
})

export class LineaComponent implements OnInit
{
  user: User = {} as User;
  

  settingsForm: FormGroup;
  errors: Object = {};
  isSubmitting = false;
  public lineasNegocio: {};

  
  constructor(
    private router: Router,
    private userService: UserService,
    private datos: DatosService,
    private fb: FormBuilder,
    private genericService:GenericService
  ) {
    //Lineas de negocio por servicio
    this.lineasNegocio = this.genericService.getLineasNegocio();
    console.log(this.lineasNegocio);
    // create form group using the form builder
    this.settingsForm = this.fb.group({
      image: '',
      username: '',
      bio: '',
      email: '',
      password: ''
    });
    // Optional: subscribe to changes on the form
    // this.settingsForm.valueChanges.subscribe(values => this.updateUser(values));
  }

  ngOnInit() {
    // Make a fresh copy of the current user's object to place in editable form fields
    Object.assign(this.user, this.userService.getCurrentUser());
  }

  logout() {
    this.userService.purgeAuth();
    this.router.navigateByUrl('/');
  }
  setLinea(item){
    this.datos.guardaUsuario('linea',item);
    this.router.navigateByUrl('/oferta'); 
  }
  submitForm() {
    this.isSubmitting = true;

    // update the model
    this.updateUser(this.settingsForm.value);

    this.userService
      .update(this.user)
      .subscribe(
        updatedUser => this.router.navigateByUrl('/profile/' + updatedUser.username),
        err => {
          this.errors = err;
          this.isSubmitting = false;
        }
      );
  }
  updateUser(values: Object) {
    Object.assign(this.user, values);
  }

}