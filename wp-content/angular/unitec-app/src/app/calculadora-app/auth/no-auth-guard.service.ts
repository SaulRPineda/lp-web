import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, CanActivate, Router, RouterStateSnapshot } from '@angular/router';
import { Observable } from 'rxjs/Observable';
import { UserService } from '../shared';
import { take, map } from 'rxjs/operators';

@Injectable()
export class NoAuthGuard implements CanActivate {
  constructor(
    private router: Router,
    private userService: UserService
  ) {}
  isAuthenticated: boolean;
  canActivate(
   
    route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot
  ): Observable<boolean> {
    console.log(this.userService.isAuthenticated);
    this.userService.isAuthenticated.subscribe(
      (authenticated) => {
        this.isAuthenticated = authenticated;

        // set the article list accordingly
        if (authenticated) {
          console.log('autenticado');
          //this.setListTo('feed');
          //this.router.navigateByUrl('/estado',{ queryParamsHandling: "preserve" });
          this.router.navigate(['/estado'], { preserveQueryParams: true });
          
        } else {
          console.log('NO autenticado');
          //this.setListTo('all');
          //this.router.navigateByUrl('/',{ queryParamsHandling: "preserve" });
          this.router.navigate(['/'], { preserveQueryParams: true });
        }
      }
    );
    console.log('usuario');
    return this.userService.isAuthenticated.take(1).map(bool => !bool);;

  }
}
