import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import { ReplaySubject } from 'rxjs/ReplaySubject';
import { Router } from '@angular/router';
import { ApiService } from './api.service';
import { JwtService } from './jwt.service';
import { User } from '../models';
import { distinctUntilChanged, map } from 'rxjs/operators';


@Injectable()
export class UserService {
  private currentUserSubject = new BehaviorSubject<User>({} as User);
  public currentUser = this.currentUserSubject.asObservable().pipe(distinctUntilChanged());

  private isAuthenticatedSubject = new ReplaySubject<boolean>(1);
  public isAuthenticated = this.isAuthenticatedSubject.asObservable();

  constructor (
    private apiService: ApiService,
    private http: HttpClient,
    private router: Router,
    private jwtService: JwtService
  ) {}

  // Verify JWT in localstorage with server & load user's info.
  // This runs once on application startup.
  populate(user: User) {
    console.log(this.jwtService.getToken());
    // If JWT detected, attempt to get & store user's info
    
    if (this.jwtService.getToken()) {
      // this.apiService.get('/user')
      // .subscribe(
      //   data => this.setAuth(data.user),
      //   err => this.purgeAuth()
      // );
      this.currentUserSubject.next(user);
      // Set isAuthenticated to true
      this.isAuthenticatedSubject.next(true);

      //this.router.navigateByUrl('/',{ queryParamsHandling: "preserve" });
      this.router.navigate(['/'], { preserveQueryParams: true });
      console.log('si loged');
      //this.purgeAuth();
    } else {
      console.log('no loged');
      // Remove any potential remnants of previous auth states
      this.purgeAuth();
    }
  } 

  setAuth(user: User) {
    console.log(user);
    //user.estado="algo"
    console.log(user);
    // Save JWT sent from server in localstorage
    this.jwtService.saveToken(user.token);
    // Set current user data into observable
    this.currentUserSubject.next(user);
    // Set isAuthenticated to true
    this.isAuthenticatedSubject.next(true);
  }

  purgeAuth() {
    // Remove JWT from localstorage
    this.jwtService.destroyToken();
    // Set current user to an empty object
    this.currentUserSubject.next({} as User);
    // Set auth status to false
    this.isAuthenticatedSubject.next(false);
  }

  attemptAuth(type, credentials) {
    console.log("credenciales");
    console.log(type);
    console.log(credentials);
    let data: any;
    credentials.token="sdmnadioam";
    console.log(credentials);
     data= credentials;
    // const route = (type === 'login') ? '/login' : '';
    //  let a=this.apiService.post('/users' + route, {user: credentials})
    //   .pipe(map(
    //   data => {
    //     this.setAuth(data.user);
    //     console.log('1---');
    //     console.log(data);
    //     return data;
    //   }
    // ));
    // console.log(a);
   
    this.setAuth(data);
    return "test";
  }

  getCurrentUser(): User {
    return this.currentUserSubject.value;
  }

  // Update the user on the server (email, pass, etc)
  update(user): Observable<User> {
    return this.apiService
    .put('/user', { user })
    .pipe(map(data => {
      // Update the currentUser observable
      this.currentUserSubject.next(data.user);
      return data.user;
    }));
  }

}
