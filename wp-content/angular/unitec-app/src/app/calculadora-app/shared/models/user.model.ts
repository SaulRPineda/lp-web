export interface User {
  email: string;
  token: string;
  username: string;
  bio: string;
  image: string;
  estado:string;
  linea:string;
}
