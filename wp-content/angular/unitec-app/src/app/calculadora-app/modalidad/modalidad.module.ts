import { ModuleWithProviders, NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { ModalidadComponent } from './../modalidad/modalidad.component';
import { AuthGuard, SharedModule } from '../shared';
import { HttpModule } from '@angular/http';
import { readJson } from "../../services/readJson.service";
const ModalidadRouting: ModuleWithProviders = RouterModule.forChild([
  {
    path: 'modalidad',
    component: ModalidadComponent,
    canActivate: [AuthGuard]
  }
]);

@NgModule({
  imports: [
    SharedModule,
    ModalidadRouting
  ],
  declarations: [
    ModalidadComponent
  ],
  providers: [
    readJson
  ]
})
export class ModalidadModule { }
