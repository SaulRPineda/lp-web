import { Component, ViewEncapsulation, Input, OnInit, Inject, Injectable } from '@angular/core';
import { FormBuilder,FormGroup } from '@angular/forms';
import { Router } from '@angular/router';
import { DOCUMENT } from "@angular/platform-browser";
import { User, UserService, DatosService } from '../shared';
import { Observable } from "rxjs/Rx";
import { ReplaySubject } from 'rxjs/ReplaySubject';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import { distinctUntilChanged, map } from 'rxjs/operators';
import { GenericService } from "../../services/generic.service";

declare var jQuery : any;
declare var $ :any;

@Component({
  selector: 'modalidad', 
  templateUrl: './modalidad.component.html', 
  styleUrls: ['./modalidad.component.scss'],
  encapsulation: ViewEncapsulation.None,
}) 
@Injectable()

export class ModalidadComponent implements OnInit
{ 
  
  user: User = {} as User;
  settingsForm: FormGroup;
  errors: Object = {};
  isSubmitting = false;
  public oferta: {};
  public result:{};
  private currentUserSubject = new BehaviorSubject<User>({} as User);
  public currentUser = this.currentUserSubject.asObservable().pipe(distinctUntilChanged());
  private isAuthenticatedSubject = new ReplaySubject<boolean>(1);

  public estados: {};
  public modalidades: {};
  public carrerasBien:{};
  public data:{};

  constructor(
    private router: Router,
    private userService: UserService,
    private fb: FormBuilder,
    private genericService:GenericService,
    @Inject(DOCUMENT) private document,
    private datos:DatosService
  ) {

   }

  ngOnInit() {

    this.modalidades=this.genericService.getModalidades(this.datos.getCampo('carrera').IdCategoria);
    console.log(this.modalidades);
  } 
  setOferta(modalidad){
    this.datos.guardaUsuario('modalidad', modalidad);
    this.router.navigateByUrl('/campus');
    //this.router.navigateByUrl('/campus');
  }
  
  logout() {
    this.userService.purgeAuth();
    this.router.navigateByUrl('/');
  }

  setEstado(item){
    console.log(item);
    this.user.estado = item;
    Object.assign(this.user, item);
      //Object.assign(this.user, item);
    this.currentUserSubject.value;
    console.log(this.user);
    this.router.navigateByUrl('/linea');
  }
  submitForm() {
    this.isSubmitting = true;

    // update the model
    this.updateUser(this.settingsForm.value);

    this.userService
      .update(this.user)
      .subscribe(
        updatedUser => this.router.navigateByUrl('/profile/' + updatedUser.username),
        err => {
          this.errors = err;
          this.isSubmitting = false;
        }
      );
  }

  updateUser(values: Object) {
    Object.assign(this.user, values);
  }
}