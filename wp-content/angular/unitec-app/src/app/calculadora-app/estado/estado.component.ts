import { Component, ViewEncapsulation, Input, OnInit } from '@angular/core';
import { FormBuilder,FormGroup } from '@angular/forms';
import { Router } from '@angular/router';
import { User, UserService, DatosService } from '../shared';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import { distinctUntilChanged, map } from 'rxjs/operators';
import { Http, Headers, RequestOptions, Response } from "@angular/http";
import { Observable } from "rxjs/Rx";
//Importamos el servicio para consumir los json que esta en unitecapp
import { getJson } from "../../../../../unitec-app/src/app/services/getJson.service";
import { GenericService } from "../../services/generic.service";
/*Homologacion de servicio de JSON con la aplicación de unitec  JEAB 05-03-2018*/

@Component({
  selector: 'estado',
  templateUrl: './estado.component.html',
  styleUrls: ['./estado.component.scss'],
  encapsulation: ViewEncapsulation.None
})

export class EstadoComponent implements OnInit
{
  user: User = {} as User;
  errors: Object = {};
  isSubmitting = false;
  public estados: {};
  
  //Carga de Json de carreras 
  private headers = new Headers({ Accept: "application/json" });
  private options = new RequestOptions({ headers: this.headers });
  private currentUserSubject = new BehaviorSubject<User>({} as User);
  public currentUser = this.currentUserSubject.asObservable().pipe(distinctUntilChanged());

  
  constructor(
    private router: Router,
    private userService: UserService,
    private fb: FormBuilder,
    private http: Http,
    private datos: DatosService,
    private getJsonService: getJson,
    private genericService:GenericService
    
  ) {
    
      //Estados provenientes de servicio JEAB
      this.estados = this.genericService.getEstados();
      console.log(this.estados);
      //Se carga el json de carreras
      // this.getJsonService.getJsonCarreras();
      //Se carga el json de costos
      this.getJsonService.getJsonCalcCostos();
      //SE carga el json de becas
      this.getJsonService.getJsonBecasCalc();
      //Se cargan los links de categorias
      this.getJsonService.getJsonLinksCategorias();
  }

  ngOnInit() {
    // Make a fresh copy of the current user's object to place in editable form fields
    Object.assign(this.user, this.userService.getCurrentUser());
    console.log('........');
    console.log(this.userService.getCurrentUser());
    // Fill the form
    console.log(this.user);
  }

  logout() {
    this.userService.purgeAuth();
    this.router.navigateByUrl('/');
  }

  setEstado(item){
    this.datos.guardaUsuario('estado',item);
    this.router.navigateByUrl('/linea');
  }

}