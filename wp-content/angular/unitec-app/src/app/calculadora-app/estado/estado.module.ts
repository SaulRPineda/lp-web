import { ModuleWithProviders, NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { EstadoComponent } from './../estado/estado.component';
import { AuthGuard, SharedModule } from '../shared';
import { getJson } from "../../../../../unitec-app/src/app/services/getJson.service";
import { GenericService } from "../../services/generic.service";
const estadoRouting: ModuleWithProviders = RouterModule.forChild([
  {
    path: 'estado',
    component: EstadoComponent,
    canActivate: [AuthGuard]
  }
]);

@NgModule({
  imports: [
    SharedModule,
    estadoRouting
  ],
  declarations: [
    EstadoComponent
  ],
  providers: [
    getJson,
    GenericService
  ]
})
export class EstadoModule { }
