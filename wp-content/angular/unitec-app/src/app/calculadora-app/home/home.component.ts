import { Component, OnInit,DoCheck } from '@angular/core';
import { Router } from '@angular/router';
import { ArticleListConfig, User, UserService } from '../shared';
//Integración de servicios JEAB 
import { formCookiesService } from "../../formularios/forms_services/formCookies.service";
import {GenericService}from "../../services/generic.service";
@Component({
  selector: 'app-home-page',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css'],
  providers: [formCookiesService]
})
export class HomeComponent implements OnInit, DoCheck {
  constructor(
    private getService:GenericService,
    private router: Router,
    private userService: UserService,
    private formCookiesService: formCookiesService
  ) {
  }

  isAuthenticated: boolean;
  listConfig: ArticleListConfig = {
    type: 'all',
    filters: {}
  };
  tags: Array<string> = [];
  tagsLoaded = false;


  ngDoCheck(){
    var $_GET=this.getService.getMethod();
    console.log($_GET['utm_source']);
  }

  ngOnInit() {
   

    this.userService.isAuthenticated.subscribe(
      (authenticated) => {
        this.isAuthenticated = authenticated;
        // set the article list accordingly
        if (authenticated) {
          //this.setListTo('feed');
          console.log(this.formCookiesService.getCookieByKey("c_form_data","nombre"));
          if (this.formCookiesService.getCookieByKey("c_form_data","nombre") == false) {
            //this.router.navigateByUrl('/',{ queryParamsHandling: "preserve" });
            this.router.navigate(['/'], { preserveQueryParams: true });
            localStorage.clear();
          }else{
           // this.router.navigateByUrl('/linea',{ queryParamsHandling: "preserve" });
            this.router.navigate(['/linea'], { preserveQueryParams: true });
          }
        } else {
          //this.setListTo('all');
          //this.router.navigateByUrl('/',{ queryParamsHandling: "preserve" });
          this.router.navigate(['/'], { preserveQueryParams: true });
        }
      }
    );
  }

  setListTo(type: string = '', filters: Object = {}) {
    // Si solicita un recurso para logeados y no esta autenticado se redirige a logearse en este caso a registro
    if (type === 'feed' && !this.isAuthenticated) {
      this.router.navigateByUrl('/');
      return;
    }
    // Otherwise, set the list object
    this.listConfig = {type: type, filters: filters};
  }
}
