import { Injectable } from "@angular/core";
import { formCookiesService } from "../forms_services/formCookies.service";

declare var $: any;
declare var jQuery: any;
declare var dataLayer: any;
declare var ga: any;
// var safeUrl = (/unitec.mx/.test(window.location.href)) ? "https://unitecmx-universidadtecno.netdna-ssl.com" : "//" + document.domain;
var safeUrl = (/unitec.mx/.test(window.location.href)) ? "https://www.unitec.mx" : "//" + document.domain;

@Injectable()
export class sendService {
  ciclo_default = "";
  urlreferrer: any = window.location;
  private url_ajax_php = "/wp-content/phpServeApp/backend0.php";
  private urlDomain = "//localhost:8888/unitec2017";
  private urlMicroRegistroPrueba = "https://www.unitec.mx/procWeb/microregistro.php";
  private urlMicroRegistroDeploy = "https://www.unitec.mx/desk/procWeb/microregistro.php";
  
  constructor(private formCookieService: formCookiesService) {}

  send(theData, urls) {
    console.log(theData);
    for (let i in urls) {
      jQuery
        .ajax({
          type: "GET",
          url: urls[i],
          data: { theData: theData }
        })
        .done(function(resultado) {
          console.log( resultado );
          return resultado;
        });
    }
  }

  sendMicroRegistro(evento = null) {
    /*Implementacion enviar datos BY SRP 17-01-2018*/
    //Convertir a JSON la Cookie
    // console.log(JSON.stringify(this.formCookieService.getCookieValues("c_form_data")).replace(/\\n/g,' '));
    // let dataForm = JSON.parse(JSON.stringify(this.formCookieService.getCookieValues("c_form_data")));
    let dataForm = this.formCookieService.getCookieValues("c_form_data");
    console.log("TODO BIEN AQUI");
    // url: "http://www.unitec.mx/desk/procWeb/microregistro.php",
    // url: "//" + document.domain + this.url_ajax_php,

    jQuery
      .ajax({
        type: "POST",
        url: this.urlMicroRegistroDeploy,
        data: this.formCookieService.getCookieValues("c_form_data"),
        success: function (res) {
          console.log("Datos Enviados");
          console.log(res);
          
          //Proceso tag manager
          if (typeof dataLayer != 'undefined') {
                        
            dataLayer.push({'event': 'PASO1_COMPLETADO'});
            dataLayer.push({
                'url_value': 'formularioInformacion/paso1', //dato dinámico
                'event': 'StepForm'//dato estático
            });
        }
        //End Proceso tag manager

        },
        error: function (xhr, textStatus, error) {
          console.log(xhr);
          console.log(xhr.statusText);
          console.log(textStatus);
          console.log(error);
        }
      });
      /*End Implementacion enviar datos BY SRP 17-01-2018*/
  }

  sendFormularioTradicional(evento = null) {
    //var url_multimedia_thankyou_predeterminada = "assets/sidenav.jpg";
    //Si no esta la imagen destacada se asigna la imagen predeterminada
    var url_multimedia_thankyou = jQuery("#h_horizontal_url_imagen_destacada").val();
    console.log("CATEGORIA PARA THNK: " + jQuery("#formApp").data("categoria"));
    //Implementacion ajax de la imagen destacada
    jQuery(".img-header-thankyou").css({
      "background-image": "url(" + jQuery("#h_horizontal_url_imagen_destacada").val() + ")"
    });

    var ev = null;
    //Evento
    if (evento != null) {
      ev = evento.srcElement || evento.target;
    }
    //Validacion para saber si el evento viene de la caja de ciclo
    if (ev == null || typeof ev == "undefined" || ev == "") {
      jQuery("#formApp").data("ciclo", "");
    } else {
      jQuery("#formApp").data("ciclo", ev.attributes.value.nodeValue);
    }

    if (
      jQuery("#formApp").data("ciclo") == "" ||
      typeof jQuery("#formApp").data("ciclo") == "undefined"
    ) {
      console.log(
        "No ha seleccionado el ciclo: " +
          typeof jQuery("#formApp").data("ciclo")
      );
      jQuery(".app-menu-title").addClass("parpadea");
      jQuery("#divisor-menu-app").addClass("hr-error");
      jQuery("#divisor-menu-app").addClass("parpadea");
      setTimeout(function() {
        jQuery(".app-menu-title").removeClass("parpadea");
        jQuery("#divisor-menu-app").removeClass("parpadea");
        jQuery("#divisor-menu-app").removeClass("hr-error");
      }, 1000);
    } else {
      //Se registra el valor de la urlreferer
      this.formCookieService.appendCookieValue(
        "c_form_data",
        "urlreferrer",
        this.urlreferrer
      );

      var cookie_form_values = this.formCookieService.getCookieValues(
        "c_form_data"
      );
      //Preparamos los datos para la thank you page
      jQuery(".modal-header-formulario").hide("slow");
      jQuery(".modal-header-thankyou").show("slow");
      jQuery("#enviarTrd").hide();
      jQuery(".modal-body-form").hide("slow");
      jQuery(".modal-body-thankyou").show("slow");
      jQuery(".tercer-texto-thankyou").html(cookie_form_values["carrera"]);
      jQuery("#nombre-thankyou").html(cookie_form_values["nombre"]);

      /*Implementacion enviar datos BY SRP 28-01-2018*/
      jQuery("#txt-thanks").html("Gracias por proporcionarnos tus datos. En breve te enviaremos la información que nos solicitas de:");
      /*End Implementacion enviar datos BY SRP 28-01-2018*/

      //Si no viene con evento
      console.log("EL EVENTO DESPUES DE CLICLEAR CICLO: ");
      console.log(ev);
      if (ev.id != "null" && ev.id != null && typeof ev.id != "undefined") {
        console.log("CICLO: " + ev.attributes.value.nodeValue);
        //Setear en la cookie el ciclo y en formApp
        this.formCookieService.appendCookieValue(
          "c_form_data",
          "ciclo",
          ev.attributes.value.nodeValue
        );
      } else {
        console.log("El evento no esta definido para el ciclo");
      }
      /*Implementacion enviar datos BY SRP 17-01-2018*/
      //Convertir a JSON la Cookie
      console.log(this.formCookieService.getCookieValues("c_form_data"));
      let dataForm = JSON.parse( JSON.stringify( this.formCookieService.getCookieValues("c_form_data") ) );


      if ( location.href.indexOf( "impulsa" ) !== -1 ) {
        console.log("impulsa");
        this.sendFormularioImpulsa(evento);
    } 
    
    else if( location.href.indexOf( "testvocacional" ) !== -1 ){
        console.log("Orientacion Profesional");
        this.sendFormularioOrientacion(evento);
    }

    else{
      console.log("Formulario Tradicional");
      /*Implementacion enviar datos BY SRP 17-09-2018*/
      //Convertir a JSON la Cookie
      /*Implementacion enviar datos BY SRP 17-01-2018*/
    //Convertir a JSON la Cookie
    let dataForm = JSON.parse( JSON.stringify( this.formCookieService.getCookieValues("c_form_data") ) );

    jQuery.ajax({
      type: "POST",
      url: safeUrl + this.url_ajax_php,
      data: { theData: this.formCookieService.getCookieValues("c_form_data") },
      dataType: "JSON",
      success: function(res) {
            //console.log("Datos Enviados");          
            //console.log(res);
            console.log("RES: " + res.tipo);

          // Proceso tag manager
          if (typeof dataLayer != 'undefined') {dataLayer.push({'event': 'PASO2_COMPLETADO'});}
          
            if(res.tipo == 'dupli'){
              if (typeof dataLayer != 'undefined') {dataLayer.push({'event': 'FORMULARIO_DUPLICADO'});}
                dataLayer.push({
                  'url_value': 'formularioInformacion/frmDuplicado', //dato dinámico
                  'event': 'StepForm'//dato estático
                });
            }

            if(res.tipo == 'nuevo') {

              var clientId="";
        
              if (typeof dataLayer != 'undefined') {
                  dataLayer.push({'event': dataForm.banner}); 
                  ga(function(tracker) { clientId = tracker.get('clientId');});
                                
                  dataLayer.push({
                  'leadId': clientId, //dato dinámico
                  'CDClientID':clientId, 
                  'origenLead': 'frmTradicional', //dato dinámico
                  'isAlumn': dataForm.esAlumno, //dato dinámico
                  'ingress': dataForm.tipoRegistro, //dato dinámico
                  'state': dataForm.estado, //dato dinámico
                  'levelStudies': dataForm.nivelInteres, //dato dinámico
                  'Period': dataForm.ciclo, //dato dinámico
                  'carrera': dataForm.carreraInteres, //dato dinámico
                  'campus': dataForm.campus, //dato dinámico
                  'date': new Date(), //dato dinámico
                  'event': 'leadGeneration'//dato estático
                  });

              }

              if (typeof dataLayer != 'undefined') {
                  dataLayer.push({'event': 'FORMULARIO_REGISTRO'});                 
              }
            }

          //End Proceso tag manager

        },
        error: function(xhr, textStatus, error){
            console.log(xhr.statusText);
            console.log(textStatus);
            console.log(error);
        }
      });

    }

      // jQuery
      // .ajax({
      //   type: "POST",
      //   url: "//" + document.domain + this.url_ajax_php,
      //   data: { theData: this.formCookieService.getCookieValues("c_form_data") },
      //   dataType: "JSON",
      //   success: function(res) {
      //     console.log("Datos Enviados");          
      //     console.log(res);
      //     // Proceso tag manager
      //     if (typeof dataLayer != 'undefined') {dataLayer.push({'event': 'PASO2_COMPLETADO'});}
          
      //     if(res.tipo == 'dupli'){
      //         if (typeof dataLayer != 'undefined') {dataLayer.push({'event': 'FORMULARIO_DUPLICADO'});}
      //         dataLayer.push({
      //             'url_value': 'formularioInformacion/frmDuplicado', //dato dinámico
      //             'event': 'StepForm'//dato estático
      //         });
      //     }

      //     if(res.tipo == 'nuevo'){

      //         var clientId="";

      //          if (typeof dataLayer != 'undefined') {
      //             dataLayer.push({'event': dataForm.banner}); 
      //             ga(function(tracker) { clientId = tracker.get('clientId');});
                                  
      //             dataLayer.push({
      //             'leadId': clientId, //dato dinámico
      //             'CDClientID':clientId, 
      //             'origenLead': 'frmTradicional', //dato dinámico
      //             'isAlumn': dataForm.esAlumno, //dato dinámico
      //             'ingress': dataForm.tipoRegistro, //dato dinámico
      //             'state': dataForm.estado, //dato dinámico
      //             'levelStudies': dataForm.nivelInteres, //dato dinámico
      //             'Period': dataForm.ciclo, //dato dinámico
      //             'carrera': dataForm.idCarrera, //dato dinámico
      //             'campus': dataForm.campus, //dato dinámico
      //             'date': new Date(), //dato dinámico
      //             'event': 'leadGeneration'//dato estático
      //             });

      //         }  
      //         if (typeof dataLayer != 'undefined') {
      //             dataLayer.push({'event': 'FORMULARIO_REGISTRO'});                 
      //         }
      //     }
      //     //End Proceso tag manager

      //   },
      //   error: function(xhr, textStatus, error){
      //       console.log(xhr.statusText);
      //       console.log(textStatus);
      //       console.log(error);
      //   }
      // });
      /*End Implementacion enviar datos BY SRP 17-01-2018*/
    } //Fin del else para la validacion del ciclo
  }

  /*Metodo para enviar a Impulsa BY SRP 17-01-2018*/
  sendFormularioImpulsa(evento = null) {
    //Si no esta la imagen destacada se asigna la imagen predeterminada
    var url_multimedia_thankyou = jQuery("#h_horizontal_url_imagen_destacada").val();
    console.log("CATEGORIA PARA THANK Impulsa: " + jQuery("#formApp").data("categoria"));
    //Implementacion ajax de la imagen destacada
    jQuery(".img-header-thankyou").css({
      "background-image": "url(" + jQuery("#h_horizontal_url_imagen_destacada").val() + ")"
    });

    /*Creamos Cookie para Impulsa By SRP*/
    var oneDay = 24;
    this.formCookieService.appendCookieValue(
      "reg_impulsa",
      "cookie",
      true,
      oneDay
    );
    /*End Creamos Cookie para Impulsa By SRP*/

    var ev = null;
    //Evento
    if (evento != null) {
      ev = evento.srcElement || evento.target;
    }
    //Validacion para saber si el evento viene de la caja de ciclo
    if (ev == null || typeof ev == "undefined" || ev == "") {
      jQuery("#formApp").data("ciclo", "");
    } else {
      jQuery("#formApp").data("ciclo", ev.attributes.value.nodeValue);
    }

    if (
      jQuery("#formApp").data("ciclo") == "" ||
      typeof jQuery("#formApp").data("ciclo") == "undefined"
    ) {
      console.log(
        "No ha seleccionado el ciclo: " +
          typeof jQuery("#formApp").data("ciclo")
      );
      jQuery(".app-menu-title").addClass("parpadea");
      jQuery("#divisor-menu-app").addClass("hr-error");
      jQuery("#divisor-menu-app").addClass("parpadea");
      setTimeout(function() {
        jQuery(".app-menu-title").removeClass("parpadea");
        jQuery("#divisor-menu-app").removeClass("parpadea");
        jQuery("#divisor-menu-app").removeClass("hr-error");
      }, 1000);

    } else {      

      //Se registra el valor de la urlreferer
      this.formCookieService.appendCookieValue(
        "c_form_data",
        "urlreferrer",
        this.urlreferrer
      );

      var cookie_form_values = this.formCookieService.getCookieValues(
        "c_form_data"
      );

      //Preparamos los datos para la thank you page
      jQuery(".modal-header-formulario").hide("slow");
      jQuery(".modal-header-thankyou").show("slow");
      jQuery("#enviarTrd").hide();
      jQuery(".modal-body-form").hide("slow");
      jQuery(".modal-body-thankyou").show("slow");
      jQuery(".tercer-texto-thankyou").html(cookie_form_values["carrera"]);
      jQuery("#nombre-thankyou").html(cookie_form_values["nombre"]);

      /*Implementacion enviar datos BY SRP 28-01-2018*/
      jQuery("#txt-thanks").html("A través de UNITEC te Impulsa podrás trabajar para pagar tus estudios, en unos momentos un asesor te llamara para darte todo la información.");
      jQuery("#calcula-tu-beca").hide();
      /*End Implementacion enviar datos BY SRP 28-01-2018*/

      //Si no viene con evento
      console.log("EL EVENTO DESPUES DE CLICLEAR CICLO: ");
      console.log(ev);
      if (ev.id != "null" && ev.id != null && typeof ev.id != "undefined") {
        console.log("CICLO: " + ev.attributes.value.nodeValue);
        //Setear en la cookie el ciclo y en formApp
        this.formCookieService.appendCookieValue(
          "c_form_data",
          "ciclo",
          ev.attributes.value.nodeValue
        );
      } else {
        console.log("El evento no esta definido para el ciclo");
      }
      
      /*Implementacion enviar datos BY SRP 17-01-2018*/
      //Convertir a JSON la Cookie
      let dataForm = JSON.parse( JSON.stringify( this.formCookieService.getCookieValues("c_form_data") ) );

      jQuery
      .ajax({
        type: "POST",
        url: safeUrl + this.url_ajax_php,
        data: { theData: this.formCookieService.getCookieValues("c_form_data") },
        //dataType: "JSON",
        success: function(res) {
          // Proceso tag manager
          if (typeof dataLayer != 'undefined') {dataLayer.push({'event': 'PASO2_COMPLETADO'});}
          
          if(res.tipo == 'dupli'){
              if (typeof dataLayer != 'undefined') {dataLayer.push({'event': 'FORMULARIO_DUPLICADO'});}
              dataLayer.push({
                  'url_value': 'formularioInformacion/frmDuplicado', //dato dinámico
                  'event': 'StepForm'//dato estático
              });
          }

          if(res.tipo == 'nuevo'){

            var clientId="";

            if (typeof dataLayer != 'undefined') {
                dataLayer.push({'event': dataForm.banner}); 
                ga(function(tracker) { clientId = tracker.get('clientId');});
                              
                dataLayer.push({
                'leadId': clientId, //dato dinámico
                'CDClientID':clientId, 
                'origenLead': 'frmTradicional', //dato dinámico
                'isAlumn': dataForm.esAlumno, //dato dinámico
                'ingress': dataForm.tipoRegistro, //dato dinámico
                'state': dataForm.estado, //dato dinámico
                'levelStudies': dataForm.nivelInteres, //dato dinámico
                'Period': dataForm.ciclo, //dato dinámico
                'carrera': dataForm.carreraInteres, //dato dinámico
                'campus': dataForm.campus, //dato dinámico
                'date': new Date(), //dato dinámico
                'event': 'leadGeneration'//dato estático
                });

            }

            if (typeof dataLayer != 'undefined') {
              dataLayer.push({'event': 'FORMULARIO_REGISTRO'});                 
            }

          }
          //End Proceso tag manager

          //Se redirige al PDF / Aplicacion de Vacantes 14-09-2018 BY SRP */
          //http://www.unitec.mx/folleto/impulsa.pdf

          setTimeout(function(){ window.location.href = 'https://www.unitec.mx/folleto/impulsa.pdf'; }, 3000);
          //setTimeout(function(){ var nuevaVentana = window.open('https://www.unitec.mx/vacantes-impulsa/', '_blank'); nuevaVentana.focus(); }, 3000);

          /*Validación para cambiar el atributo del botoón una vez creada la cookie de impulsa*/
          jQuery("#calculadoraModal").attr("href", "https://www.unitec.mx/folleto/impulsa.pdf").removeAttr('onclick');
          /*End Validación para cambiar el atributo del botoón una vez creada la cookie de impulsa*/

          //End Se redirige al PDF / Aplicacion de Vacantes 14-09-2018 BY SRP */
        },
        error: function(xhr, textStatus, error){
            console.log(xhr.statusText);
            console.log(textStatus);
            console.log(error);
        }
      });      
      /*End Implementacion enviar datos BY SRP 17-01-2018*/

    } //Fin del else para la validacion del ciclo
  }
  /*End Metodo para Enviar a Impulsa BY SRP 17-01-2018*/


  /*Metodo para enviar a Impulsa BY SRP 17-01-2018*/
  sendFormularioOrientacion(evento = null) {    
    //Si no esta la imagen destacada se asigna la imagen predeterminada
    var url_multimedia_thankyou = jQuery("#h_horizontal_url_imagen_destacada").val();
    console.log("CATEGORIA PARA THANK Impulsa: " + jQuery("#formApp").data("categoria"));
    //Implementacion ajax de la imagen destacada
    $.ajax({
      type: "POST",
      url: safeUrl + "/wp-admin/admin-ajax.php",
      data: {
        action: "getAttachmentUrlByID",
        page_id: jQuery("#formApp").data("categoria")
      },
      success: function(data) {
        // var obj = JSON.parse(data);
        // url_multimedia_thankyou = obj.img_url;
        // //console.log(url_multimedia_thankyou);
        // console.log(obj);
        // if (
        //   url_multimedia_thankyou == "" ||
        //   typeof url_multimedia_thankyou == "undefined" ||
        //   url_multimedia_thankyou == "undefined" ||
        //   url_multimedia_thankyou == null
        // ) {
        //   console.log("Producto sin imagen destacada o sin sku asignado");
        //   url_multimedia_thankyou = "//" + document.domain + "/assets/defaultImage.jpg";
        // }
        // console.log( "//" + document.domain + "/assets/defaultImage.jpg" );
        // //Asignar imagen predeterminada de Thank you page
        // try {
        //   jQuery(".img-header-thankyou").css({
        //     "background-image": "url(" + "//" + document.domain + "/assets/defaultImage.jpg" + ")"
        //   });
        // }catch(error) {
        //   jQuery(".img-header-thankyou").css({
        //     "background-image": "url(" + "//" + document.domain + "/assets/defaultImage.jpg" + ")"
        //   });
        // }
        try{
          jQuery(".img-header-thankyou").css({
            "background-image": "url(" + jQuery("#h_horizontal_url_imagen_destacada").val() + ")"
          });
        }catch(error){
          jQuery(".img-header-thankyou").css({
            "background-image": "url(" + jQuery("#h_horizontal_url_imagen_destacada").val() + ")"
          });
        }
      }
    });

    var ev = null;
    //Evento
    if (evento != null) {
      ev = evento.srcElement || evento.target;
    }
    //Validacion para saber si el evento viene de la caja de ciclo
    if (ev == null || typeof ev == "undefined" || ev == "") {
      jQuery("#formApp").data("ciclo", "");
    } else {
      jQuery("#formApp").data("ciclo", ev.attributes.value.nodeValue);
    }

    if (
      jQuery("#formApp").data("ciclo") == "" ||
      typeof jQuery("#formApp").data("ciclo") == "undefined"
    ) {
      console.log(
        "No ha seleccionado el ciclo: " +
          typeof jQuery("#formApp").data("ciclo")
      );
      jQuery(".app-menu-title").addClass("parpadea");
      jQuery("#divisor-menu-app").addClass("hr-error");
      jQuery("#divisor-menu-app").addClass("parpadea");
      setTimeout(function() {
        jQuery(".app-menu-title").removeClass("parpadea");
        jQuery("#divisor-menu-app").removeClass("parpadea");
        jQuery("#divisor-menu-app").removeClass("hr-error");
      }, 1000);
    
    } else {      

      //Se registra el valor de la urlreferer
      this.formCookieService.appendCookieValue(
        "c_form_data",
        "urlreferrer",
        this.urlreferrer
      );

      var cookie_form_values = this.formCookieService.getCookieValues(
        "c_form_data"
      );

      //Preparamos los datos para la thank you page
      jQuery(".modal-header-formulario").hide("slow");
      jQuery(".modal-header-thankyou").show("slow");
      jQuery("#enviarTrd").hide();
      jQuery(".modal-body-form").hide("slow");
      jQuery(".modal-body-thankyou").show("slow");
      jQuery(".tercer-texto-thankyou").html(cookie_form_values["carrera"]);
      jQuery("#nombre-thankyou").html(cookie_form_values["nombre"]);

      /*Implementacion enviar datos BY SRP 28-01-2018*/
      jQuery("#txt-thanks").html("Descubre cuáles son tus aptitudes y qué profesiones se ajustan a tu perfil, espera unos momentos y comenzará el test.");
      jQuery("#calcula-tu-beca").hide();
      /*End Implementacion enviar datos BY SRP 28-01-2018*/

      //Si no viene con evento
      console.log("EL EVENTO DESPUES DE CLICLEAR CICLO: ");
      console.log(ev);
      if (ev.id != "null" && ev.id != null && typeof ev.id != "undefined") {
        console.log("CICLO: " + ev.attributes.value.nodeValue);
        //Setear en la cookie el ciclo y en formApp
        this.formCookieService.appendCookieValue(
          "c_form_data",
          "ciclo",
          ev.attributes.value.nodeValue
        );
      } else {
        console.log("El evento no esta definido para el ciclo");
      }
      
      /*Implementacion enviar datos BY SRP 17-01-2018*/
      //Convertir a JSON la Cookie
      let dataForm = JSON.parse( JSON.stringify( this.formCookieService.getCookieValues("c_form_data") ) );

      jQuery.ajax({
        type: "POST",
        url: safeUrl + this.url_ajax_php,
        data: { theData: this.formCookieService.getCookieValues("c_form_data") },
        dataType: "JSON",
        success: function(res) {
            // Proceso tag manager
          if (typeof dataLayer != 'undefined') {dataLayer.push({'event': 'PASO2_COMPLETADO'});}
          
          if(res.tipo == 'dupli'){
              if (typeof dataLayer != 'undefined') {dataLayer.push({'event': 'FORMULARIO_DUPLICADO'});}
              dataLayer.push({
                  'url_value': 'formularioInformacion/frmDuplicado', //dato dinámico
                  'event': 'StepForm'//dato estático
              });
          }

          if(res.tipo == 'nuevo'){

            var clientId="";

            if (typeof dataLayer != 'undefined') {
              dataLayer.push({'event': dataForm.banner}); 
              ga(function(tracker) { clientId = tracker.get('clientId');});
                              
              dataLayer.push({
                'leadId': clientId, //dato dinámico
                'CDClientID':clientId, 
                'origenLead': 'frmTradicional', //dato dinámico
                'isAlumn': dataForm.esAlumno, //dato dinámico
                'ingress': dataForm.tipoRegistro, //dato dinámico
                'state': dataForm.estado, //dato dinámico
                'levelStudies': dataForm.nivelInteres, //dato dinámico
                'Period': dataForm.ciclo, //dato dinámico
                'carrera': dataForm.carreraInteres, //dato dinámico
                'campus': dataForm.campus, //dato dinámico
                'date': new Date(), //dato dinámico
                'event': 'leadGeneration'//dato estático
              });

            }

            if (typeof dataLayer != 'undefined') {
                dataLayer.push({'event': 'FORMULARIO_REGISTRO'});                 
            }
          }
          //End Proceso tag manager

          //Se redirige al proceso de Orientación pprofesional
          ///setTimeout(function(){ window.location.href = res.urlcc; }, 3000);
          setTimeout(function(){ window.location.href = res.urlcc; }, 3000);
          //setTimeout(function(){ var nuevaVentana = window.open(res.urlcc, '_blank'); nuevaVentana.focus(); }, 3000);
          //End Se redirige al proceso de Orientación pprofesional
        },
        error: function(xhr, textStatus, error){
            console.log(xhr.statusText);
            console.log(textStatus);
            console.log(error);
        }
        
    })
      
      /*End Implementacion enviar datos BY SRP 17-01-2018*/

    } //Fin del else para la validacion del ciclo
  }
  /*End Metodo para Enviar a Impulsa BY SRP 17-01-2018*/


  sendFormularioExpuesto(formData, hiddenData) {
    let urlsFormularioTradicional = [
      "//local.unitec2017/wp-content/phpServeApp/backend0.php"
    ];

    let theData = [];

    theData.push(formData);
    theData.push(hiddenData);

    let response = this.send(theData, urlsFormularioTradicional);
    //return response
  }

  sendFormularioSearchGoogle(formData) {}
}
