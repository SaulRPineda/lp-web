import { Injectable } from "@angular/core";
import { formCookiesService } from "../forms_services/formCookies.service";

declare var $: any;
declare var jQuery: any;

@Injectable()
export class sendService {
  ciclo_default = "";
  urlreferrer: any = window.location;
  constructor(private formCookieService: formCookiesService) {}

  send(theData, urls) {
    console.log(theData);
    for (let i in urls) {
      jQuery
        .ajax({
          type: "GET",
          url: urls[i],
          data: { theData: theData }
        })
        .done(function(resultado) {
          console.log( JSON.parse(resultado) );
          return resultado;
        });
    }
  }

  sendFormularioTradicional(evento = null) {
    var url_multimedia_thankyou_predeterminada = "assets/sidenav.jpg";
    //Si no esta la imagen destacada se asigna la imagen predeterminada
    var url_multimedia_thankyou = jQuery("#h_horizontal_url_imagen_destacada").val();
    console.log("CATEGORIA PARA THNK: " + jQuery("#formApp").data("categoria"));
    //Implementacion ajax de la imagen destacada
    $.ajax({
      type: "POST",
      url: "//" + document.domain +  "/wp-admin/admin-ajax.php",
      data: {
        action: "getAttachmentUrlByID",
        page_id: jQuery("#formApp").data("categoria")
      },
      success: function(data) {
        var obj = JSON.parse(data);
        url_multimedia_thankyou = obj.img_url;
        //console.log(url_multimedia_thankyou);
        console.log(obj);
        if (
          url_multimedia_thankyou == "" ||
          typeof url_multimedia_thankyou == "undefined"
        ) {
          console.log("Producto sin imagen destacada o sin sku asignado");
          url_multimedia_thankyou = url_multimedia_thankyou_predeterminada;
        }
        //Asignar imagen predeterminada de Thank you page
        try {
          jQuery(".img-header-thankyou").css({
            "background-image": "url(" + url_multimedia_thankyou.url + ")"
          });
        }catch(error) {
          jQuery(".img-header-thankyou").css({
            "background-image": "url(" + url_multimedia_thankyou + ")"
          });
        }
      }
    });

    var ev = null;
    //Evento
    if (evento != null) {
      ev = evento.srcElement || evento.target;
    }
    //Validacion para saber si el evento viene de la caja de ciclo
    if (ev == null || typeof ev == "undefined" || ev == "") {
      jQuery("#formApp").data("ciclo", "");
    } else {
      jQuery("#formApp").data("ciclo", ev.attributes.value.nodeValue);
    }

    if (
      jQuery("#formApp").data("ciclo") == "" ||
      typeof jQuery("#formApp").data("ciclo") == "undefined"
    ) {
      console.log(
        "No ha seleccionado el ciclo: " +
          typeof jQuery("#formApp").data("ciclo")
      );
      jQuery(".app-menu-title").addClass("parpadea");
      jQuery("#divisor-menu-app").addClass("hr-error");
      jQuery("#divisor-menu-app").addClass("parpadea");
      setTimeout(function() {
        jQuery(".app-menu-title").removeClass("parpadea");
        jQuery("#divisor-menu-app").removeClass("parpadea");
        jQuery("#divisor-menu-app").removeClass("hr-error");
      }, 1000);
    } else {
      //Urls a donde enviaremos la data del formulario tradicional
      let urlsFormularioTradicional = [
        "//" + document.domain + "/wp-content/phpServeApp/backend0.php",
        "//" + document.domain + "/wp-content/phpServeApp/backend1.php",
        "//" + document.domain + "/wp-content/phpServeApp/backend2.php",
        "//" + document.domain + "/wp-content/phpServeApp/backend3.php"
      ];

      //Se registra el valor de la urlreferer
      this.formCookieService.appendCookieValue(
        "c_form_data",
        "urlreferrer",
        this.urlreferrer
      );

      var cookie_form_values = this.formCookieService.getCookieValues(
        "c_form_data"
      );
      //Preparamos los datos para la thank you page
      jQuery(".modal-header-formulario").hide("slow");
      jQuery(".modal-header-thankyou").show("slow");
      jQuery("#enviarTrd").hide();
      jQuery(".modal-body-form").hide("slow");
      jQuery(".modal-body-thankyou").show("slow");
      jQuery(".tercer-texto-thankyou").html(cookie_form_values["carrera"]);
      jQuery("#nombre-thankyou").html(cookie_form_values["nombre"]);

      /*Implementacion enviar datos BY SRP 28-01-2018*/
      jQuery("#txt-thanks").html("Gracias por proporcionarnos tus datos. En breve te enviaremos la información que nos solicitas de:");
      /*End Implementacion enviar datos BY SRP 28-01-2018*/

      //Si no viene con evento
      console.log("EL EVENTO DESPUES DE CLICLEAR CICLO: ");
      console.log(ev);
      if (ev.id != "null" && ev.id != null && typeof ev.id != "undefined") {
        console.log("CICLO: " + ev.attributes.value.nodeValue);
        //Setear en la cookie el ciclo y en formApp
        this.formCookieService.appendCookieValue(
          "c_form_data",
          "ciclo",
          ev.attributes.value.nodeValue
        );
      } else {
        console.log("El evento no esta definido para el ciclo");
      }
      /*Implementacion enviar datos BY SRP 17-01-2018*/
      let response = this.send( this.formCookieService.getCookieValues(
        "c_form_data"), urlsFormularioTradicional );
      /*End Implementacion enviar datos BY SRP 17-01-2018*/
    } //Fin del else para la validacion del ciclo
  }

  /*Metodo para enviar a Impulsa BY SRP 17-01-2018*/
  sendFormularioImpulsa(evento = null) {
    var url_multimedia_thankyou_predeterminada = "assets/sidenav.jpg";
    //Si no esta la imagen destacada se asigna la imagen predeterminada
    var url_multimedia_thankyou = jQuery("#h_horizontal_url_imagen_destacada").val();
    console.log("CATEGORIA PARA THANK Impulsa: " + jQuery("#formApp").data("categoria"));
    //Implementacion ajax de la imagen destacada
    $.ajax({
      type: "POST",
      url: "//" + document.domain +  "/wp-admin/admin-ajax.php",
      data: {
        action: "getAttachmentUrlByID",
        page_id: jQuery("#formApp").data("categoria")
      },
      success: function(data) {
        var obj = JSON.parse(data);
        url_multimedia_thankyou = obj.img_url;
        //console.log(url_multimedia_thankyou);
        console.log(obj);
        if (
          url_multimedia_thankyou == "" ||
          typeof url_multimedia_thankyou == "undefined"
        ) {
          console.log("Producto sin imagen destacada o sin sku asignado");
          url_multimedia_thankyou = url_multimedia_thankyou_predeterminada;
        }
        //Asignar imagen predeterminada de Thank you page
        try {
          jQuery(".img-header-thankyou").css({
            "background-image": "url(" + url_multimedia_thankyou.url + ")"
          });
        }catch(error) {
          jQuery(".img-header-thankyou").css({
            "background-image": "url(" + url_multimedia_thankyou + ")"
          });
        }
      }
    });

    var ev = null;
    //Evento
    if (evento != null) {
      ev = evento.srcElement || evento.target;
    }
    //Validacion para saber si el evento viene de la caja de ciclo
    if (ev == null || typeof ev == "undefined" || ev == "") {
      jQuery("#formApp").data("ciclo", "");
    } else {
      jQuery("#formApp").data("ciclo", ev.attributes.value.nodeValue);
    }

    if (
      jQuery("#formApp").data("ciclo") == "" ||
      typeof jQuery("#formApp").data("ciclo") == "undefined"
    ) {
      console.log(
        "No ha seleccionado el ciclo: " +
          typeof jQuery("#formApp").data("ciclo")
      );
      jQuery(".app-menu-title").addClass("parpadea");
      jQuery("#divisor-menu-app").addClass("hr-error");
      jQuery("#divisor-menu-app").addClass("parpadea");
      setTimeout(function() {
        jQuery(".app-menu-title").removeClass("parpadea");
        jQuery("#divisor-menu-app").removeClass("parpadea");
        jQuery("#divisor-menu-app").removeClass("hr-error");
      }, 1000);

    } else {      

      //Se registra el valor de la urlreferer
      this.formCookieService.appendCookieValue(
        "c_form_data",
        "urlreferrer",
        this.urlreferrer
      );

      var cookie_form_values = this.formCookieService.getCookieValues(
        "c_form_data"
      );

      //Preparamos los datos para la thank you page
      jQuery(".modal-header-formulario").hide("slow");
      jQuery(".modal-header-thankyou").show("slow");
      jQuery("#enviarTrd").hide();
      jQuery(".modal-body-form").hide("slow");
      jQuery(".modal-body-thankyou").show("slow");
      jQuery(".tercer-texto-thankyou").html(cookie_form_values["carrera"]);
      jQuery("#nombre-thankyou").html(cookie_form_values["nombre"]);

      /*Implementacion enviar datos BY SRP 28-01-2018*/
      jQuery("#txt-thanks").html("Gracias por proporcionarnos tus datos. En breve te redireccionaremos a Impulsa");
      jQuery("#calcula-tu-beca").hide();
      /*End Implementacion enviar datos BY SRP 28-01-2018*/

      //Si no viene con evento
      console.log("EL EVENTO DESPUES DE CLICLEAR CICLO: ");
      console.log(ev);
      if (ev.id != "null" && ev.id != null && typeof ev.id != "undefined") {
        console.log("CICLO: " + ev.attributes.value.nodeValue);
        //Setear en la cookie el ciclo y en formApp
        this.formCookieService.appendCookieValue(
          "c_form_data",
          "ciclo",
          ev.attributes.value.nodeValue
        );
      } else {
        console.log("El evento no esta definido para el ciclo");
      }
      
      /*Implementacion enviar datos BY SRP 17-01-2018*/
      jQuery
      .ajax({
        type: "POST",
        url: "//" + document.domain + "/wp-content/phpServeApp/backend0.php",
        data: { theData: this.formCookieService.getCookieValues(
          "c_form_data") }
      })
      .done(function(data) {
        setTimeout(function(){ window.location.href="http://www.unitec.mx/impulsa"; }, 5000);        
      });
      /*End Implementacion enviar datos BY SRP 17-01-2018*/

    } //Fin del else para la validacion del ciclo
  }
  /*End Metodo para Enviar a Impulsa BY SRP 17-01-2018*/


  /*Metodo para enviar a Impulsa BY SRP 17-01-2018*/
  sendFormularioOrientacion(evento = null) {
    var url_multimedia_thankyou_predeterminada = "assets/sidenav.jpg";
    //Si no esta la imagen destacada se asigna la imagen predeterminada
    var url_multimedia_thankyou = jQuery("#h_horizontal_url_imagen_destacada").val();
    console.log("CATEGORIA PARA THANK Impulsa: " + jQuery("#formApp").data("categoria"));
    //Implementacion ajax de la imagen destacada
    $.ajax({
      type: "POST",
      url: "//" + document.domain +  "/wp-admin/admin-ajax.php",
      data: {
        action: "getAttachmentUrlByID",
        page_id: jQuery("#formApp").data("categoria")
      },
      success: function(data) {
        var obj = JSON.parse(data);
        url_multimedia_thankyou = obj.img_url;
        //console.log(url_multimedia_thankyou);
        console.log(obj);
        if (
          url_multimedia_thankyou == "" ||
          typeof url_multimedia_thankyou == "undefined"
        ) {
          console.log("Producto sin imagen destacada o sin sku asignado");
          url_multimedia_thankyou = url_multimedia_thankyou_predeterminada;
        }
        //Asignar imagen predeterminada de Thank you page
        try {
          jQuery(".img-header-thankyou").css({
            "background-image": "url(" + url_multimedia_thankyou.url + ")"
          });
        }catch(error) {
          jQuery(".img-header-thankyou").css({
            "background-image": "url(" + url_multimedia_thankyou + ")"
          });
        }
      }
    });

    var ev = null;
    //Evento
    if (evento != null) {
      ev = evento.srcElement || evento.target;
    }
    //Validacion para saber si el evento viene de la caja de ciclo
    if (ev == null || typeof ev == "undefined" || ev == "") {
      jQuery("#formApp").data("ciclo", "");
    } else {
      jQuery("#formApp").data("ciclo", ev.attributes.value.nodeValue);
    }

    if (
      jQuery("#formApp").data("ciclo") == "" ||
      typeof jQuery("#formApp").data("ciclo") == "undefined"
    ) {
      console.log(
        "No ha seleccionado el ciclo: " +
          typeof jQuery("#formApp").data("ciclo")
      );
      jQuery(".app-menu-title").addClass("parpadea");
      jQuery("#divisor-menu-app").addClass("hr-error");
      jQuery("#divisor-menu-app").addClass("parpadea");
      setTimeout(function() {
        jQuery(".app-menu-title").removeClass("parpadea");
        jQuery("#divisor-menu-app").removeClass("parpadea");
        jQuery("#divisor-menu-app").removeClass("hr-error");
      }, 1000);
    
    } else {      

      //Se registra el valor de la urlreferer
      this.formCookieService.appendCookieValue(
        "c_form_data",
        "urlreferrer",
        this.urlreferrer
      );

      var cookie_form_values = this.formCookieService.getCookieValues(
        "c_form_data"
      );

      //Preparamos los datos para la thank you page
      jQuery(".modal-header-formulario").hide("slow");
      jQuery(".modal-header-thankyou").show("slow");
      jQuery("#enviarTrd").hide();
      jQuery(".modal-body-form").hide("slow");
      jQuery(".modal-body-thankyou").show("slow");
      jQuery(".tercer-texto-thankyou").html(cookie_form_values["carrera"]);
      jQuery("#nombre-thankyou").html(cookie_form_values["nombre"]);

      /*Implementacion enviar datos BY SRP 28-01-2018*/
      jQuery("#txt-thanks").html("Gracias por proporcionarnos tus datos. En breve te redireccionaremos a Orientacion Vocacional");
      jQuery("#calcula-tu-beca").hide();
      /*End Implementacion enviar datos BY SRP 28-01-2018*/

      //Si no viene con evento
      console.log("EL EVENTO DESPUES DE CLICLEAR CICLO: ");
      console.log(ev);
      if (ev.id != "null" && ev.id != null && typeof ev.id != "undefined") {
        console.log("CICLO: " + ev.attributes.value.nodeValue);
        //Setear en la cookie el ciclo y en formApp
        this.formCookieService.appendCookieValue(
          "c_form_data",
          "ciclo",
          ev.attributes.value.nodeValue
        );
      } else {
        console.log("El evento no esta definido para el ciclo");
      }
      
      /*Implementacion enviar datos BY SRP 17-01-2018*/
      jQuery
      .ajax({
        type: "POST",
        url: "//" + document.domain + "/wp-content/phpServeApp/backend0.php",
        data: { theData: this.formCookieService.getCookieValues(
          "c_form_data") }
      })
      .done(function(data) {
        setTimeout(function(){ window.location.href="http://www.unitec.mx/orientacion-profesional/"; }, 5000);        
      });
      /*End Implementacion enviar datos BY SRP 17-01-2018*/

    } //Fin del else para la validacion del ciclo
  }
  /*End Metodo para Enviar a Impulsa BY SRP 17-01-2018*/






  sendFormularioExpuesto(formData, hiddenData) {
    let urlsFormularioTradicional = [
      "//local.unitec2017/wp-content/phpServeApp/backend0.php"
    ];

    let theData = [];

    theData.push(formData);
    theData.push(hiddenData);

    let response = this.send(theData, urlsFormularioTradicional);
    //return response
  }

  sendFormularioSearchGoogle(formData) {}
}
