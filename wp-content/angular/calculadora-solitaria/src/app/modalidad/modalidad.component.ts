import { Component, ViewEncapsulation, Input, OnInit, Inject, Injectable } from '@angular/core';
import { FormBuilder,FormGroup } from '@angular/forms';
import { Router } from '@angular/router';
import { DOCUMENT } from "@angular/platform-browser";
import { User, UserService, DatosService } from '../shared';
import { Observable } from "rxjs/Rx";
import { ReplaySubject } from 'rxjs/ReplaySubject';


import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import { distinctUntilChanged, map } from 'rxjs/operators';
//import { readJson } from "./../shared/services/readJson.service";
//import { formCookiesService } from "./../shared/services/formCookies.service";
//import { getJson } from "./../shared/services/getJson.service";


declare var jQuery : any;
declare var $ :any;

@Component({
  selector: 'modalidad', 
  templateUrl: './modalidad.component.html', 
  styleUrls: ['./modalidad.component.scss'],
  encapsulation: ViewEncapsulation.None,
}) 
@Injectable()

export class ModalidadComponent implements OnInit
{ 
  
  user: User = {} as User;
  settingsForm: FormGroup;
  errors: Object = {};
  isSubmitting = false;
  public oferta: {};
  public result:{};
  private currentUserSubject = new BehaviorSubject<User>({} as User);
  public currentUser = this.currentUserSubject.asObservable().pipe(distinctUntilChanged());
  private isAuthenticatedSubject = new ReplaySubject<boolean>(1);

  public estados: {};
  public modalidades: {};
  public carrerasBien:{};
  public data:{};

  
  constructor(
    private router: Router,
    private userService: UserService,
    private fb: FormBuilder,
    //private readJsonService: readJson,
    @Inject(DOCUMENT) private document,
    private datos:DatosService
  ) {

   }

  ngOnInit() {

   
    this.modalidades=this.getModalidades(this.datos.getCampo('carrera').IdCategoria);
     console.log(this.modalidades);
  //  let modalidadfix={};
  //   Object.entries(this.modalidades).forEach(function (key, value) {
  //     //if(){}
  //    // console.log(value['modalidad']);
      
  //     if (key[1]['modalidad']==1){
  //       key[1]['modalidad_txt'] ="Prescencial";
  //       Object.assign(modalidadfix, key);
  //      } else if (key[1]['modalidad'] == 2){
  //       key[1]['modalidad_txt'] = "Ejecutiva";
  //       Object.assign(modalidadfix, key);
  //      } else if (key[1]['modalidad'] == 3) {
  //       key[1]['modalidad_txt'] = "En Linea";
  //       Object.assign(modalidadfix, key);
  //      }

  //   });
  //   this.modalidades = modalidadfix;
  //     console.log(modalidadfix);
  } 

 
  //retorna en la data desde el primer padre del json los coinsidentes iguales a key => value
  buscar(key, value, jsonCarreras) {
    let resultadoBusqueda = [];
    for (let i in jsonCarreras) {
      if (this.getObjects(jsonCarreras[i], key, value).length != 0) {
        resultadoBusqueda.push(jsonCarreras[i])
      }
    }
    return resultadoBusqueda;
  }

  setOferta(modalidad){
    this.datos.guardaUsuario('modalidad', modalidad);
    this.router.navigateByUrl('/campus');
    //this.router.navigateByUrl('/campus');
  }

  //Retorna los coinsidentes entre key => value desde padre directo
  getObjects(obj, key, val) {
    var objects = [];
    for (var i in obj) {
      if (!obj.hasOwnProperty(i)) continue;

      if (typeof obj[i] == 'object') {
        objects = objects.concat(this.getObjects(obj[i], key, val));
      } else
        //if key matches and value matches or if key matches and value is not passed (eliminating the case where key matches but passed value does not)
        if (i == key && obj[i] == val || i == key && val == '') { //
          objects.push(obj);
        } else if (obj[i] == val && key == '') {
          //only add if the object is not already in the array
          if (objects.indexOf(obj) == -1) {
            objects.push(obj);
          }
        }
    }
    return objects;
  } 


  getCarreras(linea, campus = "") {
    let keySearch = "lineaweb";
    let arrayCarrerasPorLinea = [];
    let objJsonCarreras = JSON.parse(localStorage.getItem("jsonCarreras"));
    arrayCarrerasPorLinea = this.buscar(
      keySearch,
      linea,
      objJsonCarreras
    );
  
    return arrayCarrerasPorLinea
  }

  getModalidades(categoria){
    let keySearch = "Grupo_carreras";
    let arrayCarrerasPorLinea = [];
    let objJsonCarreras = JSON.parse(localStorage.getItem("jsonCarreras"));
    arrayCarrerasPorLinea = this.buscar(
      keySearch,
      categoria,
      objJsonCarreras
    );

    return arrayCarrerasPorLinea
  }

  getCarrerasBien(categorias){
    let objJsonCarreras = JSON.parse(localStorage.getItem("jsonLinksCategorias"));
    let categoriasno= new Array();
    Object.entries(categorias).forEach(function (key, value) {
      categoriasno.push(key[1]['Grupo_carreras']);
    });
    //console.log(categoriasno);
     let nombres=new Array();
     Object.entries(objJsonCarreras).forEach(function (key, value) {
       console.log(key[1]['IdCategoria']);
       if (categoriasno.includes(key[1]['IdCategoria'])) {
         nombres.push(key[1]);
        } 
     });
    return nombres;
  }

  getModalidadPorSubnivel(subNivelInteres) {
    let modalidad;
    if (subNivelInteres == 1 || subNivelInteres == 2 || subNivelInteres == 5 || subNivelInteres == 7) {
      //Presencial
      modalidad = 1;
    } else if (subNivelInteres == 3) {
      //ejecutiva
      modalidad = 2;
    } else if (subNivelInteres == 4 || subNivelInteres == 6) {
      modalidad = 3;
    }
    return modalidad;
  }

  
  logout() {
    this.userService.purgeAuth();
    this.router.navigateByUrl('/');
  }

  setEstado(item){
    console.log(item);
    this.user.estado = item;
    Object.assign(this.user, item);
      //Object.assign(this.user, item);
    this.currentUserSubject.value;
    console.log(this.user);
    this.router.navigateByUrl('/linea');
  }
  submitForm() {
    this.isSubmitting = true;

    // update the model
    this.updateUser(this.settingsForm.value);

    this.userService
      .update(this.user)
      .subscribe(
        updatedUser => this.router.navigateByUrl('/profile/' + updatedUser.username),
        err => {
          this.errors = err;
          this.isSubmitting = false;
        }
      );
  }

  updateUser(values: Object) {
    Object.assign(this.user, values);
  }

  

}