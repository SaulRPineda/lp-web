import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, FormControl, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';

import {  UserService } from '../shared';

@Component({
  selector: 'app-auth-page',
  templateUrl: './auth.component.html'
})
export class AuthComponent implements OnInit {
  authType: String = '';
  title: String = '';
  //errors: Errors = {errors: {}};
  isSubmitting = false;
  authForm: FormGroup;
  formularioPadre: FormGroup;
  
  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private userService: UserService,
    private fb: FormBuilder
    

  ) {
    console.log('ejecutando autComponent');

    // use FormBuilder to create a form group
    this.authForm = this.fb.group({
      'email': ['', Validators.required],
      //'revalidacion': ['', Validators.required],
      'nombre': ['', Validators.required], 
      'apaterno': ['', Validators.required],
      'amaterno': ['', Validators.required],
      'celular': ['', Validators.required]

    });
  }

 
  myfunc(){}
  ngOnInit() {
    console.log('ejecutando autComponent1');

    this.route.url.subscribe(data => {
      // Get the last piece of the URL (it's either 'login' or 'register')
      this.authType = data[data.length - 1].path;
      // Set a title for the page accordingly
      this.title = (this.authType === 'login') ? 'Sign in' : 'Calcula tu Beca';
      // add form control for username if this is the register page
      console.log("estas en " + this.authType)
      if (this.authType === 'register' || this.authType === 'registro') {
        console.log('entro')
       // this.authForm.addControl('username', new FormControl());
      }
    });
  }

  submitForm(value:any ):void{
    console.log('Form data');
    console.log(value);
    //  const credentials = this.authForm.value;
    //  this.userService
    //  .attemptAuth(this.authType, credentials)
    //   this.router.navigateByUrl('/estado');
   }
}
