import { ModuleWithProviders, NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { EstadoComponent } from './../estado/estado.component';
import { AuthGuard, SharedModule } from '../shared';

const estadoRouting: ModuleWithProviders = RouterModule.forChild([
  {
    path: 'estado',
    component: EstadoComponent,
    canActivate: [AuthGuard]
  }
]);

@NgModule({
  imports: [
    SharedModule,
    estadoRouting
  ],
  declarations: [
    EstadoComponent
  ]
})
export class EstadoModule { }
