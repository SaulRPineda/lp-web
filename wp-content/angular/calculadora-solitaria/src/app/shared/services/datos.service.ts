import { Injectable } from '@angular/core';

@Injectable()
export class DatosService {

    constructor() { }

    guardaUsuario(valor,data){
        if (localStorage.getItem("jsonUsr") === null) {
            let jsonnew={};
            jsonnew[valor]=data; 
            localStorage.setItem("jsonUsr", JSON.stringify(jsonnew));
            console.log('no existe el Json de usuario');
        }else{
            let viejo = JSON.parse(localStorage.getItem("jsonUsr"));
            viejo[valor]=data;
            localStorage.setItem("jsonUsr", JSON.stringify(viejo));
        }
        
    }
 
    getUsuario(){
       return  JSON.parse(localStorage.getItem("jsonUsr"));
    }

    getCosto() {
        return JSON.parse(localStorage.getItem("jsonCostos"));
    }
    getBeca() {
        return JSON.parse(localStorage.getItem("jsonBecas"));
    }

 
    getCampoCosto(campo) {
        let costo = this.getCosto();
        //console.log("el campo"+campo);
        //console.log(costo[0]);
        return costo[0][campo];
    }
    getCampoBeca(campo) {
        let beca = this.getBeca();
        //console.log(beca);
        return beca[0][campo];
    }

    getCampo(campo){
        let usuario=this.getUsuario();
        return usuario[campo];
    }
}