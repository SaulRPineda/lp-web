import { ModuleWithProviders, NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { CampusComponent } from './../campus/campus.component';
import { AuthGuard, SharedModule } from '../shared';

const estadoRouting: ModuleWithProviders = RouterModule.forChild([
  {
    path: 'campus',
    component: CampusComponent,
    canActivate: [AuthGuard]
  }
]);

@NgModule({ 
  imports: [
    SharedModule,
    estadoRouting
  ],
  declarations: [
    CampusComponent
  ]
})
export class CampusModule { }
