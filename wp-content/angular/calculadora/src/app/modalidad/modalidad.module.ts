import { ModuleWithProviders, NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { ModalidadComponent } from './../modalidad/modalidad.component';
import { AuthGuard, SharedModule } from '../shared';
import { HttpModule } from '@angular/http';

const ModalidadRouting: ModuleWithProviders = RouterModule.forChild([
  {
    path: 'modalidad',
    component: ModalidadComponent,
    canActivate: [AuthGuard]
  }
]);

@NgModule({
  imports: [
    SharedModule,
    ModalidadRouting
  ],
  declarations: [
    ModalidadComponent
  ]
})
export class ModalidadModule { }
