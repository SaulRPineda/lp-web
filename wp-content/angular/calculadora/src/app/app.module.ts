import { ModuleWithProviders, NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { RouterModule, Routes } from '@angular/router';
import { HttpModule, JsonpModule } from '@angular/http';
import { AppComponent } from './app.component';
import { AuthModule } from './auth/auth.module';
import { HomeModule } from './home/home.module';
import { ModalidadModule } from './modalidad/modalidad.module';
import { EstadoModule } from './estado/estado.module';
import { ResultadoModule } from './resultado/resultado.module';
import { CampusModule } from './campus/campus.module';
import { LineaModule } from './linea/linea.module';
import { OfertaModule } from './oferta/oferta.module';
import { getJson } from "./shared/services/getJson.service";
import { DatosService } from "./shared/services/datos.service";
import { NouisliderModule } from 'ng2-nouislider'; //add this!!!!!!

import { ReactiveFormsModule, FormsModule     } from '@angular/forms';
//Componentes padre
//import { formularioTradicionalComponent } from './formularios/forms_parent_components/formulario-tradicional/formulario-tradicional.component';
//Componentes hijo del formulario






import {
  ApiService,
  ArticlesService,
  AuthGuard,
  CommentsService,
  FooterComponent,
  HeaderComponent,
  JwtService,
  ProfilesService,
  SharedModule,
  TagsService,
  UserService
} from './shared';
import { CookieService } from 'ngx-cookie-service';

//const rootRouting: ModuleWithProviders = RouterModule.forRoot([]);
const routes: Routes = [

];
@NgModule({
  declarations: [
    AppComponent,
    FooterComponent,
    HeaderComponent ],
  imports: [
    BrowserModule,
    AuthModule,
    HomeModule, 
    //rootRouting,
    SharedModule,
    EstadoModule,
    RouterModule.forRoot(routes, { useHash: true }),  
    CampusModule,
    LineaModule,
    ResultadoModule,
    OfertaModule,
    ModalidadModule,
    NouisliderModule,
    BrowserModule,
    FormsModule,
    ReactiveFormsModule,
    HttpModule,
    JsonpModule,
    JsonpModule
  ],
  providers: [
    ApiService,
    ArticlesService,
    AuthGuard,
    CommentsService,
    JwtService,
    ProfilesService,
    TagsService,
    UserService,
    DatosService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
