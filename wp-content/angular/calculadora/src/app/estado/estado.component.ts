import { Component, ViewEncapsulation, Input, OnInit } from '@angular/core';
import { FormBuilder,FormGroup } from '@angular/forms';
import { Router } from '@angular/router';
import { User, UserService, DatosService } from '../shared';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import { distinctUntilChanged, map } from 'rxjs/operators';
import { Http, Headers, RequestOptions, Response } from "@angular/http";
import { Observable } from "rxjs/Rx";

@Component({
  selector: 'estado',
  templateUrl: './estado.component.html',
  styleUrls: ['./estado.component.scss'],
  encapsulation: ViewEncapsulation.None
})

export class EstadoComponent implements OnInit
{
  user: User = {} as User;
  errors: Object = {};
  isSubmitting = false;
  public estados: {};
  
  //Carga de Json de carreras 
  private headers = new Headers({ Accept: "application/json" });
  private options = new RequestOptions({ headers: this.headers });
  /*private urlJsonBasura = "//www.unitec.mx/mob/wp-content/themes/temaunitec/assets/frontend/json/min/json_calc_carreras.min.json";
  private urlJsonCosto = "//www.unitec.mx/mob/wp-content/themes/temaunitec/assets/frontend/json/min/json_calc_costos.min.json";
  private urlJsonBeca = "//www.unitec.mx/mob/wp-content/themes/temaunitec/assets/frontend/json/min/json_calc_becas.min.json";
  private jsonLinksCategorias = "//www.unitec.mx/mob/wp-content/themes/temaunitec/assets/frontend/json/min/json_calc_linkPaginas.min.json";*/

  private urlJsonBasura = "/wp-content/themes/temaunitec/assets/frontend/json/min/json_calc_carreras.min.json";
  private urlJsonCosto = "/wp-content/themes/temaunitec/assets/frontend/json/min/json_calc_costos.min.json";
  private urlJsonBeca = "/wp-content/themes/temaunitec/assets/frontend/json/min/json_calc_becas.min.json";
  private jsonLinksCategorias = "/wp-content/themes/temaunitec/assets/frontend/json/min/json_calc_linkPaginas.min.json";
  
  private currentUserSubject = new BehaviorSubject<User>({} as User);
  public currentUser = this.currentUserSubject.asObservable().pipe(distinctUntilChanged());

  
  constructor(
    private router: Router,
    private userService: UserService,
    private fb: FormBuilder,
    private http: Http,
    private datos: DatosService
    
  ) {
    this.estados =
      [{"name":"Aguascalientes","crm_name":"AGUASCALIENTES"},
{"name":"Baja California","crm_name":"BAJA CALIFORNIA NOR"},
{"name":"Baja California Sur","crm_name":"BAJA CALIFORNIA SUR"},
{"name":"Campeche","crm_name":"CAMPECHE"},
{"name":"Chiapas","crm_name":"CHIAPAS"},
{"name":"Chihuahua","crm_name":"CHIHUAHUA"},
{"name":"Ciudad de México","crm_name":"CIUDAD DE MEXICO"},
{"name":"Coahuila","crm_name":"COAHUILA"},
{"name":"Colima","crm_name":"COLIMA"},
{"name":"Durango","crm_name":"DURANGO"},
{"name":"Estado de México","crm_name":"ESTADO DE MEXICO"},
{"name":"Guanajuato","crm_name":"GUANAJUATO"},
{"name":"Guerrero","crm_name":"GUERRERO"},
{"name":"Hidalgo","crm_name":"HIDALGO"},
{"name":"Jalisco","crm_name":"JALISCO"},
{"name":"Michoacán","crm_name":"MICHOACAN"},
{"name":"Morelos","crm_name":"MORELOS"},
{"name":"Nayarit","crm_name":"NAYARIT"},
{"name":"Nuevo León","crm_name":"NUEVO LEON"},
{"name":"Oaxaca","crm_name":"OAXACA"},
{"name":"Puebla","crm_name":"PUEBLA"},
{"name":"Querétaro","crm_name":"QUERETARO"},
{"name":"Quintana Roo","crm_name":"QUINTANA ROO"},
{"name":"San Luis Potosí","crm_name":"SAN LUIS POTOSI"},
{"name":"Sinaloa","crm_name":"SINALOA"},
{"name":"Sonora","crm_name":"SONORA"},
{"name":"Tabasco","crm_name":"TABASCO"},
{"name":"Tamaulipas","crm_name":"TAMAULIPAS"},
{"name":"Tlaxcala","crm_name":"TLAXCALA"},
{"name":"Veracruz","crm_name":"VERACRUZ"},
{"name":"Yucatán","crm_name":"YUCATAN"},
{"name":"Zacatecas","crm_name":"ZACATECAS"}];

console.log(this.estados);
   
        //Aprovecho para cargar los Json´s
    let response;
    let subscriptionBasuraJson = this.http
      .get(this.urlJsonBasura, this.options)
      .map((res: any) => res.json())
      .catch((error: any) => {
        return Observable.throw(error.statusText);
      });
    subscriptionBasuraJson.subscribe(
      res => {
        response = res;
        // console.log(response);
        localStorage.setItem("jsonCarreras", JSON.stringify(response));

      },
      error => {
        console.log(error);
      }
    );

    //cosots

    let subscriptionCostoJson = this.http
      .get(this.urlJsonCosto, this.options)
      .map((res: any) => res.json())
      .catch((error: any) => {
        return Observable.throw(error.statusText);
      });
    subscriptionCostoJson.subscribe(
      res => {
        response = res;
        // console.log(response);
        localStorage.setItem("jsonCostos", JSON.stringify(response));

      },
      error => {
        console.log(error);
      }
    );

      //BECAS

    let subscriptionBecaJson = this.http
      .get(this.urlJsonBeca, this.options)
      .map((res: any) => res.json())
      .catch((error: any) => {
        return Observable.throw(error.statusText);
      });
    subscriptionBecaJson.subscribe(
      res => {
        response = res;
        // console.log(response);
        localStorage.setItem("jsonBecas", JSON.stringify(response));

      },
      error => {
        console.log(error);
      }
    );



      //BECAS

      let subscriptionLinkJson = this.http
      .get(this.jsonLinksCategorias, this.options)
      .map((res: any) => res.json())
      .catch((error: any) => {
        return Observable.throw(error.statusText);
      });
    subscriptionLinkJson.subscribe(
      res => {
        response = res;
        // console.log(response);
        localStorage.setItem("jsonLinksCategorias", JSON.stringify(response));

      },
      error => {
        console.log(error);
      }
    );




  }

  ngOnInit() {
    // Make a fresh copy of the current user's object to place in editable form fields
    Object.assign(this.user, this.userService.getCurrentUser());
    console.log('........');
    console.log(this.userService.getCurrentUser());
    // Fill the form
    console.log(this.user);
  }

  logout() {
    this.userService.purgeAuth();
    this.router.navigateByUrl('/');
  }

  setEstado(item){
    this.datos.guardaUsuario('estado',item);
    this.router.navigateByUrl('/linea');
  }

}