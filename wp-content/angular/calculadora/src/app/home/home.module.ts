import { ModuleWithProviders, NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { ReactiveFormsModule, FormsModule     } from '@angular/forms';
import { HomeComponent } from './home.component';
import { HomeAuthResolver } from './home-auth-resolver.service';
import { SharedModule } from '../shared';
import { formularioTradicionalComponent } from '../formularios/forms_parent_components/formulario-tradicional/formulario-tradicional.component';
import { frmNombreComponent   } from '../formularios/forms_child_components/frm-nombre/frm-nombre.component';
import { frmApaternoComponent } from '../formularios/forms_child_components/frm-apaterno/frm-apaterno.component';
import { frmAmaternoComponent } from '../formularios/forms_child_components/frm-amaterno/frm-amaterno.component';
import { frmMailComponent     } from '../formularios/forms_child_components/frm-mail/frm-mail.component';
import { frmMailLoginComponent } from '../formularios/forms_child_components/frm-mail-login/frm-mail-login.component'
import { frmEstadoComponent   } from '../formularios/forms_child_components/frm-estado/frm-estado.component';
import { frmEstadoExpuestoComponent   } from '../formularios/forms_child_components/frm-estado-expuesto/frm-estado-expuesto.component';
import { frmBusquedaIntegradaComponent } from '../formularios/forms_child_components/frm-busqueda-integrada/frm-busqueda-integrada.component';
import { HttpModule, JsonpModule } from '@angular/http';
//import { frmProductosExpuestosComponent } from './formularios/forms_child_components/frm-productos-expuestos/frm-productos-expuestos.component';
import { frmLineasNegocioAppComponent   } from '../formularios/forms_child_components/frm-lineas-negocio-app/frm-lineas-negocio-app.component';
import { frmCelularComponent  } from '../formularios/forms_child_components/frm-celular/frm-celular.component';
import { frmEstudioInteresComponent } from '../formularios/forms_child_components/frm-estudio-interes/frm-estudio-interes.component';
import { frmPoliticasCondiciones } from '../formularios/forms_child_components/frm-politicas-condiciones/frm-politicas-condiciones.component';
import { frmCicloComponent } from '../formularios/forms_child_components/frm-ciclo/frm-ciclo.component';
import { frmRevalidacionComponent } from '../formularios/forms_child_components/frm-revalidacion/frm-revalidacion.component';
//
//import { Animaciones } from '../utilities/animaciones';
//import { ModalComponent } from '../utilities/modal/modal.component';
import { ModalFormularioTradicionalComponent } from '../formularios/forms_parent_components/modal-formulario-tradicional/modal-formulario-tradicional.component';
import { ModalAppFormularioTradicionalComponent } from '../formularios/forms_parent_components/modal-app-formulario-tradicional/modal-app-formulario-tradicional.component';
import { CookieService } from 'ngx-cookie-service';


//Servicio de validaciones
import { userValidations } from "../formularios/forms_services/validaciones.service";
//Servicio para leer los Json
import { readJson } from "../services/readJson.service";
//Servicio WebSpeach
import { WebspeachService } from "../services/webspeach.service";
//Servicio de envío de los formularios
import { sendService } from '../formularios/forms_services/send.service';
//Servicio de las reglas de los input
import { reglasInputsService } from '../formularios/forms_services/reglasInputs.service';
//Servicio para autocomplete
import { AutocompleteService } from "../services/autocomplete.service";
//Servicio de metodos genericos
import { GenericService } from "../services/generic.service";
//Servicio para manipulacion de las cookies en el formulario
import {formCookiesService} from "../formularios/forms_services/formCookies.service";
//import { getJson } from "../shared/services/getJson.service";



const homeRouting: ModuleWithProviders = RouterModule.forChild([
  {
    path: '',
    component: HomeComponent,
    resolve: {
      isAuthenticated: HomeAuthResolver
    }
  }
]);

@NgModule({
  imports: [
    homeRouting,
    SharedModule,
    HttpModule,
    JsonpModule
  ],
  declarations: [
    formularioTradicionalComponent,
    frmNombreComponent,
    HomeComponent,
    //formularioTradicionalOSComponent,
    frmApaternoComponent,
    frmAmaternoComponent,
    frmMailComponent,
    frmMailLoginComponent,
    frmEstadoComponent,
    frmEstadoExpuestoComponent,
    //frmProductosExpuestosComponent,
    frmLineasNegocioAppComponent,
    frmCelularComponent,
    frmEstudioInteresComponent,
    frmPoliticasCondiciones,
    frmCicloComponent,
    frmRevalidacionComponent,
    frmBusquedaIntegradaComponent,
    //ModalComponent,
    ModalFormularioTradicionalComponent,
    ModalAppFormularioTradicionalComponent 
    
  ],
  providers: [
    HomeAuthResolver,
    //Animaciones,
    CookieService, userValidations, readJson, sendService, reglasInputsService, WebspeachService, AutocompleteService, GenericService, formCookiesService
  ]
})
export class HomeModule {}
