<?php
/**
 * The template for displaying all pages.
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site will use a
 * different template.
 *
 * @package storefront
 */
get_header(); ?>			
<?php

	/******FUNCION PARA TRAER LAS PLANTILLAS ASIGNADAS A ESTA PAGINA********/
	if( wp_is_mobile() ){
		// echo "es tipo mobil";
		$secciones=get_post_meta($post->ID,"_page_section_order_fieldmobile");	
		$carpeta="secciones_mobile/";
	}else{
		// echo "es tipo Escritorio";
		$secciones=get_post_meta($post->ID,"_page_section_order_field");
		$carpeta="secciones_desk/";
	}
	$secciones=explode(',', str_replace('_php','', str_replace('.', '_', $secciones[0]) ) );
	foreach ($secciones as $key => $value) {
		/*echo "......".$carpeta.$value."....<br>";*/
		get_template_part($carpeta.$value);
	}
	/***********************************************************************/



?>


<?php

  $taxonomy     = 'product_cat';
  $orderby      = 'name';  
  $show_count   = 0;      // 1 for yes, 0 for no
  $pad_counts   = 0;      // 1 for yes, 0 for no
  $hierarchical = 1;      // 1 for yes, 0 for no  
  $title        = '';  
  $empty        = 0;

  $args = array(
         'taxonomy'     => $taxonomy,
         'orderby'      => $orderby,
         'show_count'   => $show_count,
         'pad_counts'   => $pad_counts,
         'hierarchical' => $hierarchical,
         'title_li'     => $title,
         'hide_empty'   => $empty
  );
 $all_categories = get_categories( $args );
 //var_dump($all_categories);

//  foreach ($all_categories as $cat) {
// 	// var_dump($cat); //id de categoria
// 	echo "----------";
// 	echo "Nombre de categoria: ".$cat->name."<br>";
// 	echo "Id de la categoria: ".$cat->term_id."<br>";
// 	$a=get_term_meta($cat->term_id); //va por los terms asociados al post
// 	//var_dump($a);
// 	echo "El id del template asignado a esa categoria: ".$a['dhvc_woo_category_page_id'][0]."</br>";
// 	$id_post=$a[dhvc_woo_category_page_id][0];

// 	//var_dump($id_post);
// 	if($id_post!=NULL){
// 		$pagina=get_post($id_post);
// 		echo "El nombre de la pagina: ".$pagina->post_title."<br>"; 
// 		echo "La imagen destacada: ".get_the_post_thumbnail($id_post,1);
// 	}
	
// 	//var_dump($pagina);      
// 	echo "<br><br>";
// }

//die();





get_footer();
