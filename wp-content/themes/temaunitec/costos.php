<?php
/*
Template Name: Simulador de costos
*/
//error_reporting(E_ALL);
//ini_set("display_errors", 1);

//$urlcalc = "https://www.unitec.mx";
$urlcalc = "https://www.unitec.mx";
$urlcalc2 = "https://www.unitec.mx/wp-content/themes/temaunitec";
//$urlcalc = "http://localhost:8888/unitec2017";

?>
<head>
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no">
<script type="text/javascript">  
  const params = new URLSearchParams(window.location.search);  
  var email = params.get("email"); 
  const Http = new XMLHttpRequest();

  const url='https://www.unitec.mx/procWebLeads/costos/costos.php?email='+email;
  //const url='http://localhost:8888/unitec2017/procWebLeads/costos/costos.php?email='+email;

  Http.open("GET", url);
  Http.send();

  Http.onreadystatechange = function(){
    console.log(Http.responseText)
    if (this.readyState == 4 && this.status==200) {      
      if (Http.responseText!=="ok") {
                window.location.replace("/calcula-tu-beca/");
      }else{
        var red = document.getElementById("ocultRedir")
        red.parentNode.removeChild(red);
        var over = document.getElementById("overlay-oculta")
        over.classList.remove("overlay")
      }
    }
  }  
</script>
<style type="text/css">
  .overlay {
  position: fixed; /* Sit on top of the page content */
  display: block; /* Hidden by default */
  width: 100%; /* Full width (cover the whole page) */
  height: 100%; /* Full height (cover the whole page) */
  top: 0; 
  left: 0;
  right: 0;
  bottom: 0;
  background-color: white; /* Black background with opacity */
  z-index: 200; /* Specify a stack order in case you're using a different order for other elements */
  cursor: pointer; /* Add a pointer on hover */
}
</style>
</head>
<div id="overlay-oculta" class="overlay"><h3 id="ocultRedir">Redirigiendo...</h3></div>



<script>localStorage.setItem("jsonUsr", '{"form":{"CID":"462840016.1532551272","frm_banner":"CALCULADORA","alum":"0","S_alumno":"0","frm_nombre":"Contacto","frm_apaterno":"unitec","frm_amaterno":"telemarketer","frm_correo":"masil@munitecl.com","frm_celular":"5567483924","revalidacion":"10040"}}');
 
 function setCookie(cname, cvalue, exdays) {
    var d = new Date();
    d.setTime(d.getTime() + (exdays*24*60*60*1000));
    var expires = "expires="+ d.toUTCString();
    document.cookie = cname + "=" + cvalue + ";" + expires + ";path=/";
}
setCookie('regcompleto', '1', 1);
setCookie('calculadoraHubspot', '1', 1);
 </script>

<script src="https://code.jquery.com/jquery-3.3.1.min.js" integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8=" crossorigin="anonymous"></script>
<?php if(!wp_is_mobile()){ ?>
    <link rel="stylesheet" id="calculadora-css" type="text/css" href="<?php echo $urlcalc2; ?>/assets/frontend_desktop/css/secciones/min/desk_0_calculadora.min.css">
    <script type="text/javascript" src="<?php echo $urlcalc; ?>/kalc/desk_0_1_calculadora.js"></script>
    <script type="text/javascript" src="<?php echo $urlcalc; ?>/kalc/desk_0_calculadora.js"></script>

    <script>
    jQuery(document).ready(function () {
//        $("#ocultRedir").remove();
        $('#cierra-tu-calculadora').remove();
        $("#aplicativo-calculadora").show();
        $("body").addClass("inicio").css({"margin":"0px"});
        $(".calc-becas").css({"height":"54%"});
    });
    var dataLayer = [];
</script>
<script type='text/javascript' src='<?php echo $urlcalc2 ?>/assets/frontend_desktop/js/vendor/min/jquery-ui.min.js'></script>

<!-- Scripts Mobile By SRP 18-07-2019 -->
    <!-- <script async src="<?php echo $urlcalc2; ?>/assets/frontend/js/calculadora_integracion_hb/min/funciones-async.min.js"></script>
    <script  src="<?php echo $urlcalc2; ?>/assets/frontend/js/calculadora_integracion_hb/min/funciones-calculadora.min.js"></script> -->

<?php } else {?>
    <script>
    jQuery(document).ready(function(){
        //$("#ocultRedir").remove();
        jQuery('main').fadeToggle();
            jQuery('footer').fadeToggle();
            jQuery('#carousel-banner').fadeToggle();
            jQuery('#calculadora-main-mobile').fadeToggle();
            jQuery('#UnitecSideNav').fadeToggle();
            jQuery('#bootstrapcss-css').prop("disabled",true);
            jQuery('#mdb-css').prop("disabled",true);
            jQuery('#footer').fadeIn();
            scrollTo(0,0);
    });
</script>
<?php } ?>
<?php 

    ///ob_start(); /* tell php to buffer the output */
    
    // echo utf8_decode(file_get_contents('http://unitecbeta.mx/calcula-tu-beca/'));
    // echo utf8_decode(file_get_contents('http://unitecbeta.mx/wp-content/themes/temaunitec/secciones_desk/desk_0_calculadora.php'));
    // echo utf8_decode(file_get_contents('https://www.unitec.mx/wp-content/themes/temaunitec/secciones_desk/desk_0_calculadora.php'));
    if(wp_is_mobile()){
        $tryContents = file_get_contents($urlcalc.'/wp-content/themes/temaunitec/secciones_mobile/mob_36_calculadora_hb.php');
        // var_dump($tryContents);
    } else {
        $tryContents = file_get_contents($urlcalc.'/wp-content/themes/temaunitec/secciones_desk/desk_0_calculadora.php');
    }
    $fileEndEnd = mb_convert_encoding($tryContents, 'HTML-ENTITIES', "UTF-8");
    echo utf8_decode($fileEndEnd);
?>


<!-- manipulate the DOM  using PHP only -->
<?php
    ///libxml_use_internal_errors( true );

    /* read the currently stored buffer and load into DOMDocument */
    ///$buffer=@ob_get_contents();
    ///$dom=new DOMDocument;
    ///$dom->loadHTML( $buffer );
    ///libxml_clear_errors();

    /* Find the DOM element you wish to remove */
    //  $div=$dom->getElementById('UnitecSideNav');
    //  $div->parentNode->removeChild( $div );

    //   $div=$dom->getElementById('GTMTAG');
    //  $div->parentNode->removeChild( $div );



    // $div=$dom->getElementById('menun');
    // $div->parentNode->removeChild( $div );

    /* Save the current DOM and flush the buffer */
    ///$buffer=$dom->saveHTML();
    ///@ob_end_clean();

    ///echo $buffer;

    ///if( @ob_get_level() > 0 ) {
       /// for( $i=0; $i < ob_get_level(); $i++ ) @ob_flush();
        ///@ob_end_flush();
    ///}
    ///$dom=null;
?>

<?php if(wp_is_mobile()){ ?>
<link rel="stylesheet" type="text/css" href="<?php echo $urlcalc2; ?>/assets/frontend/css/vendor/min/normalize.min.css" media="none" onload="if(media!='all')media='all'" />
<link rel="stylesheet" type="text/css" href="<?php echo $urlcalc2; ?>/assets/frontend/css/vendor/min/basscss.min.css" media="none" onload="if(media!='all')media='all'" />

<!--DESCOMENTAR CUANDO SE SUBA A PRODUCCION BY SRP-->
<link rel="stylesheet" type="text/css" href="<?php echo $urlcalc2; ?>/assets/frontend/css/vendor/min/no-critical.min.css" media="none" onload="if(media!='all')media='all'" />

<!-- <link rel="stylesheet" type="text/css" href="<?php echo $urlcalc2; ?>/assets/frontend/css/vendor/no-critical.css" media="none" onload="if(media!='all')media='all'" /> -->

<!-- <script  src="<?php echo $urlcalc ?>/wp-content/themes/temaunitec/assets/frontend/js/calculadora_integracion_hb/min/funciones-calculadora.min.js"></script> -->

<script async type="text/javascript" src="<?php echo $urlcalc2; ?>/assets/frontend/js/vendor/min/jquery-ui.min.js"></script>

<!-- <script async src="<?php echo $urlcalc ?>/wp-content/themes/temaunitec/assets/frontend/js/calculadora_integracion_hb/min/funciones-async.min.js"></script> -->

<!-- <script async src="<?php echo $urlcalc2; ?>/assets/frontend/js/calculadora_integracion_hb/min/funciones-on.min.js"></script> -->

<?php }?>