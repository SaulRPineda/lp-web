var cacheName = 'UNITEC-PWA-v1';
/*incluir todos los archivos que necesita nuestra app, como imágenes, JavaScript, hojas de estilo, etc*/
var filesToCache = [
  'secciones_mobile/sec_mobile_header.php',
  'secciones_mobile/sec_mobile_footer.php',
  'assets/frontend/img/footer/facebook-logo-button.png',
  'assets/frontend/img/footer/twitter-logo-button.png',
  'assets/frontend/img/footer/youtube-logotype.png',
  'assets/frontend/img/header/logo.png',
  'assets/frontend/img/footer/vis.png',
  'assets/frontend/img/footer/mastercard.png',
  'assets/frontend/img/footer/esr.png',
  'assets/frontend/img/footer/LOGO-LAUREATE1.png',
  'assets/frontend/img/footer/logo-amipci.png',
  'assets/frontend/img/footer/FIMPES.png',
  'assets/frontend/img/footer/1-detail.png'
];

self.addEventListener('install', function(e) {
  console.log('[ServiceWorker] Install');
  e.waitUntil(
    caches.open(cacheName).then(function(cache) {
      console.log('[ServiceWorker] Caching app shell');
      return cache.addAll(filesToCache);
    })
  );
});

self.addEventListener('activate', function(e) {
  console.log('[ServiceWorker] Activate');
  e.waitUntil(
    caches.keys().then(function(keyList) {
      return Promise.all(keyList.map(function(key) {
        if (key !== cacheName) {
          console.log('[ServiceWorker] Removing old cache', key);
          return caches.delete(key);
        }
      }));
    })
  );
  return self.clients.claim();
});

self.addEventListener('fetch', function(e) {
  console.log('[ServiceWorker] Fetch', e.request.url);
  e.respondWith(
    caches.match(e.request).then(function(response) {
      return response || fetch(e.request);
    })
  );
});