<?php
/**
 * The template for displaying 404 pages (not found).
 *
 * @package storefront
 */
?>
<!DOCTYPE HTML>
<html itemscope itemtype="http://schema.org/Product" class="no-js" lang="es"><!--<![endif]-->
	<head>
        <!-- Google Analytics -->
        <script>
            window.ga=window.ga||function(){(ga.q=ga.q||[]).push(arguments)};ga.l=+new Date;
            ga('create', 'UA-9351414-14', 'auto');
            ga('send', {
                hitType: 'event',
                eventCategory: '404',
                eventAction: 'visita',
                eventLabel: window.location.pathname
            });
        </script>
        <script async src='https://www.google-analytics.com/analytics.js'></script>
        <!-- End Google Analytics -->
		<title>Error 404</title>
		<meta charset="UTF-8">
		<meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no">
		<meta name="theme-color" content="#f68b1f">
		<!-- Agrega las tres etiquetas siguientes en la sección "head". -->

		<link rel="pingback" href="<?php echo get_template_directory_uri()?>/xmlrpc.php">
		<link rel="shortcut icon" type="image/png" href="<?php echo get_template_directory_uri()?>/assets/images/favicon/favicon.png">

		<link rel='dns-prefetch' href='//s.w.org' />
        <link rel="alternate" type="application/rss+xml" title="UNITEC &raquo; Colegiaturas RSS de los comentarios" href="<?php echo get_template_directory_uri()?>/colegiaturas/feed/" />
        <meta name="generator" content="WordPress 4.9.8" />
        <meta property="fb:admins" content="YOUR USER ID">
        <meta property="og:title" content="Error 404">
        <meta property="og:type" content="article">
        <meta property="og:url" content="<?php echo get_template_directory_uri()?>/colegiaturas/"><meta property="og:site_name" content="UNITEC">
        
        <link rel="EditURI" type="application/rsd+xml" title="RSD" href="<?php echo get_template_directory_uri()?>/xmlrpc.php?rsd" />
        <link rel="wlwmanifest" type="application/wlwmanifest+xml" href="<?php echo get_template_directory_uri()?>/wp-includes/wlwmanifest.xml" /> 
        <link rel="alternate" type="application/json+oembed" href="<?php echo get_template_directory_uri()?>/wp-json/oembed/1.0/embed?url=http%3A%2F%2Funiteclocal.mx%2Fcolegiaturas%2F" />
        <link rel="alternate" type="text/xml+oembed" href="<?php echo get_template_directory_uri()?>/wp-json/oembed/1.0/embed?url=http%3A%2F%2Funiteclocal.mx%2Fcolegiaturas%2F&#038;format=xml" />
        
        <script type="text/javascript">
			var ajaxurl =	"<?php echo get_template_directory_uri()?>/wp-admin/admin-ajax.php";
			var wpurl =		"<?php echo get_template_directory_uri()?>";
        </script>
        
        <?php 
        if( wp_is_mobile() ) {
            $menu = 'secciones_mobile/mob_1_menu';
            $footer = 'secciones_mobile/mob_21_footer';
            $carpeta = 'frontend';
            $angular = 'angularapp';
            $estilosMenu = get_template_directory_uri(). '/assets/frontend/css/secciones/min/mob_1_menu.min.css';
            $estilosFooter =  get_template_directory_uri().'/assets/frontend/css/secciones/min/mob_21_footer.min.css';
            $estilosGenerales =  get_template_directory_uri().'/assets/frontend/css/generales/min/generales_mobile_temaunitec.min.css';
            $jsMenu =  get_template_directory_uri().'/assets/frontend/js/secciones/min/mob_1_menu.min.js';
        }
            else{
                $menu = 'secciones_desk/desk_1_menu';
                $footer = 'secciones_desk/desk_21_footer';
                $carpeta = 'frontend_desktop';
                $angular = 'angularapp_desk';
                $estilosMenu = get_template_directory_uri(). '/assets/frontend_desktop/css/secciones/min/desk_1_menu.min.css';
                $estilosFooter =  get_template_directory_uri().'/assets/frontend_desktop/css/secciones/min/desk_21_footer.min.css';
                $estilosGenerales =  get_template_directory_uri().'/assets/frontend_desktop/css/generales/min/generales_mobile_temaunitec.min.css';
                $jsMenu =  get_template_directory_uri().'/assets/frontend_desktop/js/secciones/min/desk_1_menu.min.js';
            } 
        ?>

        <!-- Universal Analytics -->
        <script type='text/javascript' src='<?php echo get_template_directory_uri()?>/assets/<?php echo $carpeta; ?>/js/vendor/min/jquery-2.2.3.min.js'></script>
        <script type='text/javascript' src='<?php echo get_template_directory_uri()?>/assets/<?php echo $carpeta; ?>/js/vendor/min/jquery-3.1.1.min.js'></script>
        <script type='text/javascript' src='<?php echo get_template_directory_uri()?>/assets/<?php echo $carpeta; ?>/js/vendor/min/jquery-ui.min.js'></script>
        <script type='text/javascript' src='<?php echo get_template_directory_uri()?>/assets/<?php echo $carpeta; ?>/js/secciones/min/generales.min.js'></script>
        <script type='text/javascript' src='<?php echo get_template_directory_uri()?>/assets/<?php echo $carpeta; ?>/js/vendor/min/bootstrap3-typeahead.min.js'></script>
        
	
        <link rel="stylesheet" href="<?php echo get_template_directory_uri()?>/assets/frontend_desktop/css/secciones/min/404.min.css"> 
        <link rel="stylesheet" href="<?php echo get_template_directory_uri()?>/assets/frontend/css/vendor/min/bootstrap.min.css">
        <link rel="stylesheet" href="<?php echo get_template_directory_uri()?>/assets/frontend/css/vendor/min/mdb.min.css">
        <link rel="stylesheet" href="<?php echo $estilosMenu; ?>">
        <link rel="stylesheet" href="<?php echo $estilosGenerales; ?>">
        <link rel="stylesheet" href="<?php echo $estilosFooter; ?> ">
    </head>

    <style>
        .link-simple-list {color: #ffffff!important} .side-nav{left:-100%; width:30%} 
    </style>


    <body>

    <?php get_template_part( $menu ); ?>

        <section class="background-definido contenidoCompleto mt-md-5"><!-- Background -->
            <div class="container-fluid col-11">
                <!-- Live preview-->
                <div class="row justify-content-center">
                    <div class="col-11">
                        <!-- Descripción del producto -->
                        <article class="col-12 text-center mb-1 mt-5 d-flex justify-content-center" data-wow-offset="50">
                            <h1 class="w-100 titleSection">LO SENTIMOS</h1>
                        </article>
                        <article class="col-12 text-center mb-3 mt-1 d-flex justify-content-center" data-wow-offset="50">
                            <h4 class="w-100 titleSection">Error 404: Página no encontrada.</h4>
                        </article>
                        <!-- Descripción del producto -->
                    </div>           
            
                    <!-- Sección calendario -->
                    <div class="col-11 mb-4 d-flex flex-column justify-content-between card p-3">
                        <!-- Implementacion de Titulo y descripcion en la sección calendario -->
                        <h3><span class="filo-inicio-t pb-2">|</span> Esperamos que una de las siguientes opciones sea de utilidad para ti.</h3>                   
                        <!-- Implementacion de Titulo y descripcion en la sección calendario -->
                        <div class="row mt-2">
                            <div class="col-md-4 col-sm-12">
                                <a href="/prepa/" class="btn btn-primary p-2 tooltip-info w-100" title=""><i class="icon-u icon-u-preparatoria link-simple-list"></i>&nbsp&nbspPreparatoria<span class="tooltiptext">Conoce nuestra oferta en Preparatoria</span></a>
                            </div>
                            <div class="col-md-4 col-sm-12">
                                <a href="/licenciaturas/" class="btn btn-warning p-2 tooltip-info w-100" title=""><i class="icon-u icon-u-licenciaturas link-simple-list"></i>&nbsp&nbspLicenciaturas<span class="tooltiptext">Conoce nuestra oferta en Licenciaturas</span></a>
                            </div>
                            <div class="col-md-4 col-sm-12">
                                <a href="/maestrias/" class="btn btn-maestria p-2 tooltip-info w-100" title=""><i class="icon-u icon-u-maestrias link-simple-list"></i>&nbsp&nbspMaestrías<span class="tooltiptext">Conoce nuestra oferta en Maestrías</span></a>
                            </div>
                        </div>
                    </div>
                    <!-- Fin Sección calendario -->
                </div>
            </div>
        </section>




    </body>
    <?php get_template_part( $footer ); ?>



    <app-modal></app-modal>
    <modal-formulario-tradicional></modal-formulario-tradicional>
    <modal-app-formulario-tradicional></modal-app-formulario-tradicional>
    <modal-calculadora-formulario-tradicional></modal-calculadora-formulario-tradicional>
    <input id="data_json_basura" type="hidden" value="0">
    <input id="data_json_basura_email" type="hidden" value="0">
    <input id="formApp" type="hidden">
    <input id="data_json_basura" type="hidden" value="0">
    <input id="data_json_basura_email" type="hidden" value="0">
    <input id="data_json_telefonos_basura" type="hidden" value="0">
    <!-- Input que almacena el titulo Dinamico del formulario -->
    <input id="h_titulo_modal_formulario" type="hidden" value="">
    <input id="formApp" type="hidden">
    <input id="url_img_destacada" type="hidden" value="">
    <input id="url_video_destacado" type="hidden" value="">

    
    <script type='text/javascript' src='<?php echo get_template_directory_uri()?>/assets/frontend/js/vendor/min/jquery.lazy.min.js'></script>
    <script type='text/javascript'defer src='<?php echo get_template_directory_uri()?>/assets/<?php echo $carpeta; ?>/js/vendor/min/slideout.min.js'></script>
    <script type='text/javascript'defer src='<?php echo get_template_directory_uri()?>/assets/<?php echo $carpeta; ?>/js/vendor/min/tether.min.js'></script>
    <script type='text/javascript'defer src='<?php echo get_template_directory_uri()?>/assets/<?php echo $carpeta; ?>/js/vendor/min/bootstrap.min.js'></script>
    <script type='text/javascript'defer src='<?php echo get_template_directory_uri()?>/assets/<?php echo $carpeta; ?>/js/vendor/min/mdb.min.js'></script>
    <script type='text/javascript' src='<?php echo get_template_directory_uri()?>/assets/<?php echo $carpeta; ?>/js/vendor/min/typeahead.min.js'></script>
    
    <script type='text/javascript' src='<?php echo get_template_directory_uri()?>/assets/<?php echo $carpeta; ?>/js/<?php echo $angular; ?>/app/formApp.min.js'></script>
    
    <script type='text/javascript' src='<?php echo $jsMenu; ?>'></script>
</html>


