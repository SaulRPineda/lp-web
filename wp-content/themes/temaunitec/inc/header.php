<?php
/**
 * The header for our theme.
 *
 * Displays all of the <head> section and everything up till <div id="content">
 *
 * @package storefront
 */
?>
<!doctype html>
<html lang="es">
<head>
    <?php
        if(is_page('calcula-tu-beca')){
        $meta= '<base href="/calcula-tu-beca/">';
        }
        if(is_page('calcula-tu-beca')&& $_SERVER['REQUEST_URI']=='/r_unitec/calcula-tu-beca/'){
            $meta= '<base href="/r_unitec/calcula-tu-beca/">';
        }
        echo $meta;
    ?>

    <meta charset="<?php bloginfo( 'charset' ); ?>">

    <meta name="google-site-verification" content="eX5Oh5kHT3WqJ4L8Y2xN__4V80op-sUyWQ3wiyri1lU" />
    <meta name=viewport content="width=device-width, initial-scale=1, user-scalable=no">
    <link rel="profile" href="http://gmpg.org/xfn/11">
    <link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">
    <link rel="shortcut icon" type="image/png" href="<?php echo get_template_directory_uri()?>/assets/images/favicon/favicon.png">
    <!-- recursps AMP -->
    <!-- <script async src="https://cdn.ampproject.org/v0.js"></script>
    <script async custom-element="amp-bind" src="https://cdn.ampproject.org/v0/amp-bind-0.1.js"></script> -->

    <?php
        /*Implementacion Page Map BY SRP*/
        $pageId = get_the_ID();
        $type = get_post_type( $pageId );
        $taxonomy='product_cat';
        $categoria_temp = NULL;
        $i=1;
        /*Obtenemos las categorias de cada producto asignado en el backoffice de wordpress para implementar la indexación de búsqueda con GCS*/
        switch ($type) {
            case 'product':
                $product_cats = wp_get_post_terms( $pageId, 'product_cat' );
                echo "<!--";
    ?>
    <PageMap>
        <DataObject type="document">
        <?php
            foreach ($product_cats as $key => $categoria) {
                $cat_parent = get_term_by('id', $categoria->parent, $taxonomy);
                if ($categoria_temp === $categoria->parent) {
                    $cat_name = $cat_parent->slug."_".$i;
                    $i++;
                } else{
                    $cat_name = $cat_parent->slug;
                }
        ?>
            <Attribute name="<?php echo $cat_name; ?>"><?php echo $categoria->name; ?></Attribute>
        <?php
            $categoria_temp = $categoria->parent;
            }
        ?>
        </DataObject>
    </PageMap>
    <?php
        echo "-->\n";
        $categoria_temp = NULL;
        break;
        default: break;
    }
    /*End Implementacion Page Map BY SRP*/
    ?>

    <?php
        $dirWorpress = get_template_directory_uri();
        $tempDir = get_template_directory();
        /*Validar si la página es mobile*/
        if( wp_is_mobile() ) {
            $carpeta = "frontend";
            $page_section_conf = "_page_section_order_fieldmobile";
            $rutaMenu = $dirWorpress ."/assets/".$carpeta."/css/secciones/min/mob_1_menu.min.css";
            $rutaBanner = $dirWorpress ."/assets/".$carpeta."/css/secciones/min/mob_2_banner_home.min.css";
        }   else {
            $carpeta = "frontend_desktop";
            $page_section_conf = "_page_section_order_field";
            $rutaMenu = $dirWorpress ."/assets/".$carpeta."/css/secciones/min/desk_1_menu.min.css";
            $rutaBanner = NULL;
        } 
        /*Funcion para cargar css inline de Banner Wordpress o DFP*/
        $field = get_field_object('tipo_de_banner');
        $bannerDFP = $field['value'];
        $label = $field['choices'][ $bannerDFP ];

    switch ( $bannerDFP ) {
        case "dfp":
    ?>
<!-- Carga de estilos para Banner DFP -->
    <style type="text/css" id="estilos-inline-DFP" rel="preload" as="style">
        <?php echo file_get_contents( $rutaMenu ); ?>
        <?php echo file_get_contents( $dirWorpress . '/assets/frontend_desktop/css/secciones/min/desk_2_9_banner_carrusel_DFP.min.css' ); ?>
        <?php echo file_get_contents( $dirWorpress . '/assets/frontend_desktop/css/secciones/min/desk_2_10_banner_video_DFP.min.css' ); ?> 
        <?php echo file_get_contents( $dirWorpress . '/assets/frontend_desktop/css/secciones/min/desk_2_11_banner_estatico_DFP.min.css' ); ?>
    </style>

    <!-- End Carga de estilos para Banner DFP -->
    <?php
    break;
    default:
        /*Obtener las secciones Por Página*/
        $page_id = get_the_ID(); 
        $page_sec_id = get_post_meta($page_id, $page_section_conf, true);
        $page_sec_id = explode(",", $page_sec_id);
        $numero_secciones = count($page_sec_id);
        /*Remove de la Palabra PHP BY SRP 09-08-2018*/
        $seccionUnoCss = explode( ".php", $page_sec_id[0] );
        $seccionDosCss = explode( ".php", $page_sec_id[1] );
        /*Remove de la Palabra PHP*/

        /*Obtenemos las secciones a descartar para colocarlas inline BY SRP 09-08-2018*/
        $assetsCssInline = array(  str_replace( ".", "_", $seccionUnoCss[0] ), str_replace( ".", "_", $seccionDosCss[0] ) );
        /*End Obtenemos las secciones a descartar para colocarlas inline BY SRP 09-08-2018*/
        //$styleGenerales = getAssetsByDevice("path_generales_css");  
        //$hoja = file_get_contents( $styleGenerales );
        $seccionesInline = NULL;
        $styleSheetUrl = $tempDir ."/assets/".$carpeta."/css/secciones/min/";
        $dirWorpressFile = '/assets/'.$carpeta.'/css/secciones/min/';
        
        /*Inicia la concatenación de assets por página*/
        if($numero_secciones > 0) {
    ?>
<!-- Carga de estilos para Banner Wordpress -->
    <?php
        echo '<style type="text/css" id="estilos-inline-Wordpress" rel="preload" as="style">';
            // Esta función se comenta por que imprimía dos veces el estilo en IMPULSA
            // echo file_get_contents( $rutaMenu );
            echo file_get_contents( $rutaBanner );
            /*Concatenacón de hojas de estilo*/
            foreach ($page_sec_id as $id_page_sec) {
                //echo "ID_SECCIONES: ".$id_page_sec;
                $section_name_array = explode(".php", $id_page_sec);            
                try {
                    /*Se verifica las secciones a concatenar el el archivo CSS BY SRP 08-09-2018*/
                    if(  in_array( str_replace(".", "_", $section_name_array[0]) , $assetsCssInline) ) {
                        if( file_exists ( $styleSheetUrl.str_replace(".", "_", $section_name_array[0]).".min.css" ) ) {
                            echo file_get_contents( $dirWorpress.$dirWorpressFile.str_replace(".", "_", $section_name_array[0]).".min.css" );
                        }
                    } 
                }
                catch (Exception  $ex) {
                    $seccionesInline = $seccionesInline;
                }
                $inline_css =  $seccionesInline;
            }
        echo '</style>';
    ?>

    <!-- End Carga de estilos para Banner Wordpress -->
    <?php
        }
        break;
    }
    /*End Funcion para cargar css inline de Banner Wordpress o DFP*/

    ?>

    <!-- Se inyecta inline jquery en Home y Async en todo lo  demás BY SRP 03-09-2018-->
    <?php
    //echo '<script type="text/javascript" id="inlineJquery">'; 
        //echo file_get_contents( $dirWorpress . '/assets/frontend/js/vendor/min/jquery-3.1.1.min.js' );
    //echo '</script>';
    ?>
    <link rel="preload" href="<?php echo $dirWorpress . '/assets/frontend/js/vendor/min/jquery-3.1.1.min.js' ?>" as="script" />
    <script async type="text/javascript" src="<?php echo $dirWorpress . '/assets/frontend/js/vendor/min/jquery-3.1.1.min.js' ?>"></script>
    <!-- End Se inyecta inline jquery en Home y Async en todo lo  demás BY SRP 03-09-2018-->

    <?php wp_head(); ?>

    <?php global $post; if(strpos($post->post_name,'calcula-tu-beca')!==false  && strpos($post->post_name,'conexion-unitec')===false && strpos($post->post_name,'agendar-cita')===false && strpos($post->post_name,'prueba-performance')===false && !wp_is_mobile()) {?>
        <script> //url_wp = <?php //echo get_template_directory_uri(); ?> </script>
        <script src="https://code.jquery.com/jquery-3.3.1.min.js" integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8=" crossorigin="anonymous"></script>
        <link rel="stylesheet" id="calculadora-css" type="text/css" href="<?php echo get_template_directory_uri()?>\assets\frontend_desktop\css\secciones\min\desk_0_calculadora.min.css" disabled="disabled">
        <script type="text/javascript" src="<?php echo get_template_directory_uri()?>/assets/frontend_desktop/js/secciones/min/desk_0_1_calculadora.min.js"></script>
        <script type="text/javascript" src="<?php echo get_template_directory_uri()?>/assets/frontend_desktop/js/secciones/min/desk_0_calculadora.min.js"></script>
    <?php } ?>

    <!-- <?php if(strpos($post->post_name,'calcula-tu-beca')===false && strpos($post->post_name,'conexion-unitec')===false && strpos($post->post_name,'agendar-cita')===false && strpos($post->post_name,'prueba-performance')===false && !wp_is_mobile()){?>
        <link rel="stylesheet" id="calculadora-css" type="text/css" href="<?php echo get_template_directory_uri()?>\assets\frontend_desktop\css\secciones\min\desk_31_formulario.min.css" disabled="disabled">
        <script type="text/javascript" async src="<?php echo get_template_directory_uri()?>/assets/frontend_desktop/js/secciones/min/desk_31_1_formulario.min.js"></script>
        <script type="text/javascript" async src="<?php echo get_template_directory_uri()?>/assets/frontend_desktop/js/secciones/min/desk_31_formulario.min.js"></script>
    <?php }
        else if(strpos($post->post_name,'calcula-tu-beca')===false  && strpos($post->post_name,'conexion-unitec')===false && strpos($post->post_name,'agendar-cita')===false && strpos($post->post_name,'prueba-performance')===false && wp_is_mobile()){
        $critical_min= get_template_directory_uri()."/assets/frontend/css/vendor/min/critical.min.css";
        echo '<style id="form-css" disabled="disabled">'.file_get_contents($critical_min).'</style>';
        echo '<script async src="'.get_template_directory_uri().'/assets/frontend_desktop/js/secciones/min/desk_31_1_formulario.min.js"></script>';
        echo '<script async src="'.get_template_directory_uri().'/assets/frontend/js/formulario_integracion/min/funciones-criticas.min.js"></script>';
    } ?> -->

    <?php  
        $ocultarWhatsapp = get_field( 'ocultar_whatsapp' ,$theID );
        $ocultarBotonera = get_field( 'ocultar_botonera' ,$theID );
        $ocultarCTC = get_field( 'ocultar_frm_ctc' ,$theID );
        // $ocultarChat = get_field( 'ocultar_frm_chat' ,$theID );
        $ocultarChat = false;
    ?>

    <?php if ($ocultarBotonera != true){ ?>
    <!-- CSS BOTONERA -->
    <link rel="stylesheet" id="widget-botonera" type="text/css" href="<?php echo get_template_directory_uri()?>/assets/frontend/css/secciones/min/widget-botones.min.css" media='none' onload="if(media!='screen')media='screen'">
    <?php } ?>
    <?php if ($ocultarCTC != true ){ ?>
    <!-- CSS CTC -->
    <link rel="stylesheet" id="ctc" type="text/css" href="<?php echo get_template_directory_uri()?>/assets/frontend/css/secciones/min/click-to-call.min.css" media='none' onload="if(media!='screen')media='screen'">
    <?php } ?>
    <?php if ($ocultarWhatsapp != true ){ ?>
    <!-- CSS WHATSAPP -->
    <link rel="stylesheet" id="whatsapp" type="text/css" href="<?php echo get_template_directory_uri()?>/assets/frontend_desktop/css/secciones/min/form-whatsapp.min.css" media='none' onload="if(media!='screen')media='screen'">
    <?php } ?>
    <?php if ($ocultarChat != true && strpos($post->post_name,'test-kranon') !== false){ ?>
    <!-- CSS CHAT -->
    <link rel="stylesheet" id="chat" type="text/css" href="<?php echo get_template_directory_uri()?>/assets/frontend_desktop/css/secciones/min/form-chat.min.css" media='none' onload="if(media!='screen')media='screen'">
    <?php } ?>

</head>

<body style="margin:0" <?php body_class('inicio'); ?>>
    <input type="hidden" name="post_id" id="post_id" value="<?php echo $post->ID ?>">
    <input type="hidden" name="post_title" id="post_title" value="<?php echo $post->post_title ?>">
    <input type="hidden" name="h_posicion" id="h_posicion" value="">
    <input type="hidden" id="CID" name="CID">

    <!-- Google Tag Manager -->
    <noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-WVDBVVJ"
    height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
    <script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
    new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
    j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
    'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
    })(window,document,'script','dataLayer','GTM-WVDBVVJ');</script>
    <!-- End Google Tag Manager -->
    <?php global $post; if(strpos($post->post_name,'calcula-tu-beca')!==false && !wp_is_mobile()) {
        $pathToCalculator  = get_template_directory_uri()."/secciones_desk/desk_0_calculadora.php";
        $pagina_inicio = file_get_contents($pathToCalculator);
        // $arrContextOptions=array(
        //     "ssl"=>array(
        //           "verify_peer"=>false,
        //           "verify_peer_name"=>false,
        //       ),
        //   );  
    
        // $pagina_inicio = file_get_contents($pathToCalculator, false, stream_context_create($arrContextOptions));
        echo $pagina_inicio;
    ?>
    
    <?php } else if(strpos($post->post_name,'calcula-tu-beca')===false && !wp_is_mobile()) {
        $pathToCalculator  = get_template_directory_uri()."/secciones_desk/desk_31_formulario.php";
        $pagina_inicio = file_get_contents($pathToCalculator);
        echo $pagina_inicio;
    } else if(strpos($post->post_name,'calcula-tu-beca')===false && wp_is_mobile()) { 
        $pathToCalculator  = get_template_directory_uri()."/secciones_mobile/mob_31_formulario.php";
        $pagina_inicio = file_get_contents($pathToCalculator);
        echo $pagina_inicio;
    } ?>
        <div class="todoElBody">