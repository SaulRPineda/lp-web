<div class="align-items-center justify-content-center modal modal-whatsapp p-0" id="modal-whatsapp" tabindex="-1" role="dialog" aria-labelledby="label-whatsapp" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div id="whatsapp-success" class="modal-content card-whatsapp p-md-3 p-1"></div>
    <div id="frm-whatsapp" class="modal-content p-md-3 p-1">
      <div id="blur-whatsapp">
        <div class="d-flex justify-content-end">
          <button type="button" class="p-0" data-dismiss="modal" aria-label="Close" style="background: 0 0; border: 0">
            <div class="icons-desktop d-flex align-items-center justify-content-center">
              <a href="">
                <i class="icon-u icon-u-cerrar"></i>
              </a>
            </div>
          </button>
        </div>
        <h4 class="titleSection text-center px-md-5 px-1" id="label-ctc">¡Hablemos por Whatsapp!</h4>
        <h6 class="descriptionSection text-center px-md-5 px-1" id="label-ctc">Compártenos tu número celular para hablar con uno de nuestros asesores y despejar todas tus dudas.</h6>
        <form class="container" id="formulario-whatsapps" action="">
          <!-- <div class="row mt-1">
            <div class="col-12 d-flex justify-content-center">
              <div class="form-check form-check-inline mb-0">
                <input type="radio" id="customRadio1" value="9" name="customRadio" class="custom-control-input">
                <label class="custom-control-label mb-0" for="customRadio1">Fijo</label>
              </div>
              <div class="form-check form-check-inline mb-0">
                <input type="radio" id="customRadio2" value="8" name="customRadio" class="custom-control-input">
                <label class="custom-control-label mb-0" for="customRadio2">Celular</label>
              </div>
            </div>
            <div class="myFeedBackErrorCTC mx-auto" id="err-valor">Por favor, seleccione una opción</div>
          </div> -->
          <div class="row mt-1 pl-3 pr-3 d-flex justify-content-center">
            <div class="col-1 p-1 icon-cel-whatsapp d-flex align-items-center justify-content-center">
              <i class="icon-u icon-u-celular cel-whats"></i>
            </div>
            <div class="col-11 p-1 md-form form-sm w-100">
              <input class="form-control" name="phone-whatsapp" id="phone-whatsapp" maxlength="10" onpaste="return false;" type="tel">
              <label for="phone-whatsapp" class="col-12"><span class="color-asterisco">*</span> Número celular (10 dígitos):</label>
            </div>
          </div>
          <div class="myFeedBackErrorCTC mx-auto text-center" id="telefono-erroneo">Por favor, ingrese un número válido</div>
          <input id="Cel" type="hidden" value="">

          <div _ngcontent-c17="" class="frm-politicas" id="frm_politicas_y_condiciones">
            <label _ngcontent-c17="" class="politicas light frm-tradicional-cl text-center mt-2" id="form_politicas">
                Al hacer clic en <span _ngcontent-c17="" id="continuar_politicas">"INICIA UNA CONVERSACIÓN"</span>, reconoces haber leído las 
                <a _ngcontent-c17="" href="//www.unitec.mx/politicas-de-privacidad/" target="_blank">
                Políticas de Privacidad</a> y confirmas estar de acuerdo con el uso de ellas, así como los 
                <a _ngcontent-c17="" href="//www.unitec.mx/terminos-y-condiciones/" target="_blank">
                Términos y Condiciones</a> del sitio
            </label>
          </div>

          <div class="call-me mt-1 mt-md-2 mb-2">
            <button id="init-whatsapp" type="button" class="btn btn-primary btn-whats w-100 p-2 m-0"><i class="icon-u icon-u-whatsapp"></i>&nbsp;&nbsp;INICIA UNA CONVERSACIÓN</button>
          </div>
          
          <div id="whatsapp-alert" class="alert msg-alert text-alert p-2 fade show" role="alert">
            <h3 class="descriptionSection m-0">Por favor, solicita nuevamente el mensaje.</h3>
            <button type="button" class="close text-alert" data-dismiss="alert" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>

        </form>
      </div>
      <div id="proceso-whatsapp"></div>
    </div>
  </div>
</div>