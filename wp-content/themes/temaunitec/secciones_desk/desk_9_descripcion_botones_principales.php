<div id="descripcion_botones_principales"> </div>
<?php
/**
 * WP Post Template: desk_9_descripcion_botones_principales
 */
?>

<?php    
    global $woocommerce, $product, $post;

    /*Se insertan los plugins que necesita la seccion tal y como se llama el archivo js*/

    load_script( $pluginsNecesarios = array('mapaOperations', 'sec_specialList') );

    /*End Se insertan los plugins que necesita la seccion tal y como se llama el archivo js*/
    $mes_inicio = NULL;
    $target = NULL;

    /*Primera Version se concatena dependiendo lo que este en el custom field*/

    /*$fecha_ini = get_field('fecha_inicio', $theID);
    foreach($fecha_ini as $key => $meses) {
        $mes_inicio .= $meses;

        switch ($key) {
            case count($fecha_ini)-2:
                $mes_inicio .= " y ";
            break;
            
            default:
                $mes_inicio .= ", ";
            break;
        }
    }*/

    /*$mes_inicio = substr($mes_inicio, 0, strlen($mes_inicio) - 2);   */

    /*Segunda Version se concatena dependiendo lo que este en el custom field*/


    /*Primera Version se concatena dependiendo lo que este en el custom field*/
    /*$fecha_Divisor = explode(",", $fecha_ini);
    $dia_inicio = $fecha_Divisor[0];
    $mes_inicio = $fecha_Divisor[1];*/

    //obtenemos el id del producto
    /*$parentid = get_queried_object_id();*/
    /*Primera Version se concatena dependiendo lo que este en el custom field*/
?>

<?php //echo "Status: ".$product->is_in_stock();
//print_r($product);

$contenidoProducto = get_field('descripcion_del_producto', $theID);

    if( !empty( $contenidoProducto ) || $contenidoProducto != NULL || $contenidoProducto != "" ) {

        //Getting product attributes
        //$product_attributes = $product->get_attributes();
        //var_dump($product_attributes);

        //Getting product attributes
        $atributos = get_post_meta($product->id);
        $product_attributes = unserialize($atributos['_product_attributes'][0]);
        $variacion_producto = 1;


        foreach ($product_attributes as $key => $product_atributo) {
            $product_attribute_labels[] = wc_attribute_label($product_atributo["name"]);
            $product_attribute_slugs[] = $product_atributo["name"];
            $variacion_producto = 0;

            // switch ($product_atributo["is_variation"]) {
            //     case 0:
            //         $product_attribute_labels[] = wc_attribute_label($product_atributo["name"]);
            //         $product_attribute_slugs[] = $product_atributo["name"];

            //         $variacion_producto = 0;
            //     break;
                
            //     default:
            //     break;
            // }

        }

    foreach ($product_attribute_slugs as $key => $valordelatributo) {
        $valores = wp_get_post_terms( $product->id, $valordelatributo );
        foreach ($valores as $key => $value) {

            switch ($value->taxonomy) {
                case 'pa_inicio-de-ciclo':
                    $mes_inicio = $value->name;
                break;
                
                default:
                break;
            }
        }        
    }
?>
<section class="descripcion-botones d-flex align-items-center"><!-- Background -->
    <div class="container-fluid ml-5 mr-5">
        <div class="row justify-content-center">            
            <!-- Descripción del producto -->
            <article class="col-12 text-center mb-3 mt-3 d-flex justify-content-center" data-wow-offset="50">
                <h6 class="w-100 descriptionSection"><?php echo $contenidoProducto; ?></h6><!-- Hacer las clases de esta etiqueta globales -->
            </article>
            <!-- Descripción del producto -->
            <!--  Caracteristicas -->
            <div class="row justify-content-center">
                <div class="ancho-card col-lg-5 mb-4 card p-3">
                    <div>
                        <?php
                            if( !empty($product_attributes) && $variacion_producto != 1){ 
                        ?>
                        <h3><span class="filo-inicio-t">|</span><?php echo get_field('titulo_ficha_tecnica', $theID); ?></h3>
                        <div class="specialList tblSimulador d-flex flex-column mt-2">
                            <?php    
                            //Getting product attributes slugs
                            //$product_attribute_slugs = array_keys($product_attributes);
                            $count_slug = 0;
                            
                            //var_dump($product_attribute_slugs);
                            foreach ($product_attribute_slugs as $key => $product_attribute_slug){
                                $count_slug++;
                            ?>

                            <div class="row mt-0 mb-1">
                                <div class="col-5 backGray"><?php echo $product_attribute_labels[$key]; ?></div>

                                <?php ##  ===>  ===>  // Getting the product attribute values
                                    //$attribute_values = get_terms($product_attribute_slug);
                                    $attribute_values = wp_get_post_terms( $product->id, $product_attribute_slug );

                                    $count_value = 0;
                                ?>
                                <div class="col-7 caracteristicas">
                                    <div class="caracteristicas-lic">
                                        <?php foreach($attribute_values as $attribute_value){
                                            $count_value++;
                                            $attribute_name_value = $attribute_value->name; // name value
                                            $attribute_slug_value = $attribute_value->slug; // slug value
                                            $attribute_id_value = $attribute_value->term_id; // ID value                                    

                                            if ( filter_var($attribute_name_value, FILTER_VALIDATE_URL) ){ ?>
                                                <a data-gtm-tr="downloadFiles" data-gtm-pos="folleto-de-carrera" href="<?php echo $attribute_name_value; ?>" class="text-primary" target="_blank" title="Cons&uacute;ltalo aqu&iacute;">Cons&uacute;ltalo aqu&iacute;</a>
                                                <?php //echo get_field('texto_para_folleto', $theID); ?>
                                                <?php } else{
                                                    echo $attribute_name_value;
                                                    if($count_value != count($attribute_values)) echo ', ';
                                                }
                                            } 
                                        ?>
                                    </div>  
                                </div>
                            </div>
                            <?php 
                                }   
                            }              
                        ?>
                        </div>
                    </div>
                </div>
                <!--  Caracteristicas -->
                <div class="col-lg-1 d-none d-lg-block phantom"></div>
                <!-- Sección calendario -->
                <div class="ancho-card col-lg-5 mb-4 d-flex flex-column justify-content-between card p-3 altura">
                    <!-- Implementacion de Titulo y descripcion en la sección calendario -->
                    <?php 
                        $desk_contenidoGroup = get_field('desk_group_descripcion_botones', $theID);
                        $desk_titulo = ( $desk_contenidoGroup['desk_titulo_seccion_ficha'] == NULL ) ? "Información inicio de curso": $desk_contenidoGroup['desk_titulo_seccion_ficha'];

                    if( $desk_contenidoGroup['desk_titulo_seccion_ficha'] != NULL || $desk_contenidoGroup['desk_titulo_seccion_ficha'] != "" ) { ?>
                        <h3><span class="filo-inicio-t pb-2">|</span>&nbsp;<?php echo $desk_titulo; ?></h3>                   
                        <div class="mb-0 descriptionSection mt-1"><?php echo do_shortcode( $desk_contenidoGroup['desk_descripcion_seccion_ficha'] ); ?></div>
                    <?php } ?>
                    <!-- Implementacion de Titulo y descripcion en la sección calendario -->
                    <div class="no-active mb-1 mt-1">
                        <div class="list-item-container">                                
                            <div class="item-description inicio-clases">
                                <div class="row">
                                    <div class="col-6 d-flex align-center justify-content-center flex-column">
                                        <h4><?php echo get_field('texto_fecha_de_inicio', $theID); ?></h4>
                                        <h4><b><?php echo $mes_inicio; ?></b></h4> <!-- Se imprimen los meses -->
                                            <div class="botones">
                                                <div class="botones-contacto">
                                                    <!-- Icono de Telefono -->  
                                                    <!-- <a href="tel:018007864832" data-gtm-tr="call" data-gtm-pos="descripcion | botones-contacto" class="iconos-contacto" data-toggle="tooltip" data-html="true" title="<b>Llama</b>"><?php echo do_shortcode( '[icono nombre="'.sanitize_title("llamar-card").'"][/icono]' ); ?></a> -->
                                                    <!-- Icono de sobre para contacto Formulario -->               
                                                    <a onclick="openModal('#modal_frm_app')" class="iconos-contacto tooltip-info" data-solicitud-location="Middle"><?php echo do_shortcode( '[icono nombre="'.sanitize_title("correo-card").'"][/icono]' ); ?><span class="tooltiptext">Escríbenos</span></a>
                                                    <!-- Icono de chat -->  
                                                    <a href="javascript:$zopim.livechat.window.show()" data-gtm-tr="chat" data-gtm-pos="intencion" data-gtm-location="Descripci&oacute;n Botones Principales" data-gtm-etiqueta="con formulario" class="iconos-contacto tooltip-info"><?php echo do_shortcode( '[icono nombre="'.sanitize_title("sms").'"][/icono]' ); ?><span class="tooltiptext">Chat en línea</span></a>
                                                    <!-- Icono de lapiz para contacto Formulario -->               
                                                    <a onclick="openModal('#modal_frm_app')" class="iconos-contacto tooltip-info" data-solicitud-location="Middle"><?php echo do_shortcode( '[icono nombre="'.sanitize_title("formularios").'"][/icono]' ); ?><span class="tooltiptext">Contáctanos</span></a>
                                                </div>                    
                                            </div>
                                    </div>
                                    <?php if(get_field('texto_link', $theID) != ""){?>
                                    <div class="col-6 calendario d-flex justify-content-center">
                                        <?php echo do_shortcode( '[icono nombre="'.sanitize_title("calendario").'"][/icono]' ); ?>
                                        <a data-gtm-tr="downloadFiles" data-gtm-pos="calendario-escolar" href="<?php echo get_field('link', $theID) ?>" class="text-center pt-2 align-self-center caracteristicas ver-calendario" target="_blank" title="<?php echo get_field('texto_link', $theID) ?>"><?php echo get_field('texto_link', $theID) ?></a>
                                    </div>
                                    <?php }?>
                                </div>
                            </div>
                        </div>
                    </div>
                    <section class="botonesUnitec d-flex"><!-- Botones CTA -->
                        <?php 
                            $links_botones = get_field( 'call_to_actions_principales', $theID );                 
                            if( $links_botones > 0 ) {
                                $s=1;
                                for ($i=0; $i < 3; $i++) {
                                    $texto_call = $links_botones['texto_call_to_action_'.$s];
                                    $link_call = $links_botones['link_call_to_action_'.$s];
                                    //$target = ( strpos( strtolower( $link_call ), "unitec.mx" ) ) ? "" : "target='_blank'";
                                    if ( strpos( strtolower( $link_call ), "://" ) ) {

                                        if( strpos( strtolower( $link_call ), "pdf" ) ){
                                            $target = "target='_blank'";
                                        }

                                        else if ( strpos( strtolower( $link_call ), "unitec.mx" ) ) {
                                            $target = "";
                                        }

                                        else{
                                            $target = "target='_blank'";
                                        }
                                    } else{
                                        $target = "";
                                    }
                                    
                                    switch ($s) {
                                        case 1: $clase = "btn-primary mx-auto mt-1 waves-effect waves-light"; break;
                                        case 2: $clase = "back-naranja-unitec btn-warning mx-auto mt-1 waves-effect waves-light"; break;
                                        case 3: $clase = "boton-azul btn-default mx-auto mt-1 waves-effect waves-light"; break;                                    
                                        default: break;
                                    }
                        ?>
                        <?php if (strpos($link_call, 'javascript') !== false) { ?>
                        <a onclick="<?php echo $link_call; ?>" class="btn <?php echo $clase; ?>" <?php echo $target; ?> title="<?php echo $texto_call; ?>" data-solicitud-location="Middle"><?php echo $texto_call; ?></a>
                        <?php } else { ?>
                        <a href="<?php echo $link_call; ?>" class="btn <?php echo $clase; ?>" <?php echo $target; ?> title="<?php echo $texto_call; ?>"><?php echo $texto_call; ?></a>
                        <?php } ?>
                        <?php
                                    $s++;
                                } 
                            }
                        ?>
                    </section><!-- Botones CTA -->
                </div>
            </div><!-- Sección calendario -->
        </div>
    </div>
</section><!-- Background -->
<?php } ?>


