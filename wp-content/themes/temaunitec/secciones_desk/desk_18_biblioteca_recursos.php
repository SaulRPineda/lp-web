<?php
/**
 * WP Post Template: desk_18_biblioteca_recursos
 */
?>

<?php
    $desk_descripcionRecurso = get_field( 'contenido_recursos_electronicos', $theID);
    $desk_Recurso = get_field('contenido_collapsable_recursos', $theID);
    $iconoLinks = get_field('desk_IconoRecurso', $theID);
?>
<div id="listado"></div>
<?php //if( !empty( $_COOKIE['c_matricula'] ) || $_COOKIE['c_matricula'] != NULL ) { ?>
<div id="seccion-biblioteca-recursos" class="contenido-recursos d-flex" style="display:none !important;"><!-- Background -->
    <main class="container-fluid wow fadeIn d-flex justify-content-center align-items-center">
        <!-- Live preview-->
        <div class="col-lg-11 justify-content-center mt-2 mb-2">

            <article class="col-12 text-center mb-1 d-flex justify-content-center" data-wow-offset="50">
                <h4 class="w-75 titleSection"><?php echo get_field('titulo_card_biblioteca_recursos', $theID); ?></h4>
            </article>

            <!-- Descripción de sección del producto -->
            <article class="col-12 text-center mb-2 mt-0 d-flex justify-content-center" data-wow-offset="50">
                <h6 class="w-100 descriptionSection"><?php echo $desk_descripcionRecurso; ?></h6>
            </article>
            <!-- Descripción de sección del producto -->

            <!-- Botones Biblioteca-->
            <div class="contenedor-cards ml-3 mr-3 justify-content-center">            
                <!-- Card deck -->
                <div class="row">

                    <?php 
                    if( have_rows('contenido_collapsable_recursos') ){                        
                        while ( have_rows('contenido_collapsable_recursos') ) { the_row(); 
                    ?>                    
                    <!-- Card -->
                    <div class="col-xl-3 col-lg-3 col-md-6 col-sm-12 mt-md-3 mt-sm-4 materiales-cards pl-2 pr-2">
                        <div class="card p-3 h-100">
                            <div class="contenido-titulo-material card-body">
                                <!--Title-->
                                <h4 class="titulo-material"><?php echo get_sub_field('titulo_collapsable_recursos'); ?></h4>
                                <!--Text-->
                            </div>
                            <div class="row">

                                <?php while ( have_rows('link_del_recurso') ) { the_row(); ?>

                                <div class="material-links col-12 d-flex align-items-center pb-1">
                                    <span><?php echo do_shortcode( '[icono nombre="'.sanitize_title( $iconoLinks ).'"][/icono]' ); ?></span>
                                    <a href="<?php echo get_sub_field('link'); ?>" class="pl-2" target="_blank" title="<?php echo get_sub_field('titulo_del_link'); ?>"><?php echo get_sub_field('titulo_del_link'); ?></a>
                                </div>                            
                                <?php } /*End While Links*/ ?>
                            </div>
                        </div>
                    </div>
                    <!-- Card -->
                    <?php 
                        } /*End While collapsable*/
                    } /*End if collapsable*/

                    ?>

                </div>
                <!-- Card deck -->
            </div>
            <!-- Botones Biblioteca-->
        </div>
        <!-- /.Live preview-->
    </main><!-- /.Main layout-->
</div><!-- Background -->
<?php //} ?>
