<div id="mapa-campus"></div>
<?php
/**
 * WP Post Template: desk_15_mapa_campus
 */
?>

<?php
    /*Se insertan los plugins que necesita la seccion tal y como se llama el archivo*/
    load_script( $pluginsNecesarios = array('places', 'mapaOperations') );
    /*End Se insertan los plugins que necesita la seccion tal y como se llama el archivo*/

    /*$page = get_page_by_title( 'Home' );
    echo "Home: ".get_field('como_llegar', $page->ID);
    echo '<span id="'.get_field('como_llegar', $page->ID).'"></span>';*/
    /*$comoLlegar = get_field('como_llegar', 13);*/

    $mapa = get_field('mapa', $theID);
    $coordenadasCampus = $mapa['lat'].",".$mapa['lng'];    

    $idPost = get_the_ID();
    $postType = get_post( $idPost );
    $fachadaCampus = get_field( 'imagen_destacada_horizontal', $theID);
    $contador=0;
?>

<section class="container-fluid wow fadeIn d-flex justify-content-center align-items-center p-0 mapa-campus"><!-- /.Main <layout-->
    <input type="hidden" name="h_destino" id="h_destino" value="<?php echo $coordenadasCampus; ?>">
    <input type="hidden" id="mi-destino" value="<?php echo get_field('direccion_del_campus'); ?>" disabled>
    <input type="hidden" name="imgFachada" id="imgFachada" value="<?php echo $fachadaCampus['url']; ?>">
    

    

    <section class="geolocalizacion">  
        <!-- Iniciar Mapa Google Maps -->
        <!-- <div id="map" class="mapa"></div> -->
        <iframe class="map-top" width="100%" frameborder="0" y framespacing="0" src="https://www.google.com/maps/embed/v1/place?key=AIzaSyB8rc5Fp2F0TyXna7Y5ylBhusDesXq_UrI&q=<?php echo $mapa['address']; ?>" allowfullscreen=""></iframe>

        <section id="elegant-card" class="card-mapa">
            <div class="col-lg-12 justify-content-center d-flex align-items-center">
                <div class="card mt-4 mb-1 p-4">
                    <h4 class="card-title-mapa"><?php echo get_field('titulo', $theID); ?></h4>
                    <!-- Panel donde se muestra com llegar -->
                    <div id="right-panel"></div>

                    <div class="card-block pt-0">            
                        <div class="row">
                            <div class="col-12 p-0">

                                <div class="parametros-geolocalizacion">
                                    <!-- <div class="parametros mb-1">
                                        <div class="iconos">
                                            <?php echo do_shortcode( '[icono nombre="'.sanitize_title("localizacion").'"][/icono]' ); ?>
                                            <?php echo do_shortcode( '[icono nombre="'.sanitize_title("distancia").'"][/icono]' ); ?>
                                            <?php echo do_shortcode( '[icono nombre="'.sanitize_title("ubicacion").'"][/icono]' ); ?>
                                        </div>
                                        <form>
                                            <input type="text" id="mi-posicion" class="mb-1" placeholder="Ingresa punto de partida">
                                            <input type="text" id="fake_mi-destino" value="<?php echo $postType->post_title; ?>" disabled>
                                        </form>
                                    </div> -->
                                    <!-- <a href="//maps.google.es/?q=<?php echo $mapa['address']; ?>" class="botonSecundario white-text" id="getDirections" target="_blank">CÓMO LLEGAR</a> -->


                                    <section class="botonesUnitec my-2">
                                        <!-- <a class="btn back-naranja-unitec-2 waves-effect waves-light" id="getDirections" target="_blank" title="C&oacute;mo llegar" data-gtm-tr="Directions" data-gtm-campus="<?php echo $postType->post_title; ?>" >C&Oacute;MO LLEGAR</a> -->
                                        <a class="btn back-naranja-unitec-2 waves-effect waves-light" href="//maps.google.es/?q=<?php echo $mapa['address']; ?>" target="_blank" title="C&oacute;mo llegar" data-gtm-tr="Directions" data-gtm-campus="<?php echo $postType->post_title; ?>" >C&Oacute;MO LLEGAR</a>
                                    </section>
                                </div>
                                <?php
                                // check if the repeater field has rows of data
                                if( have_rows('contenido_collapsable') ){
                                    // loop through the rows of data
                                    while ( have_rows('contenido_collapsable') ) { the_row();
                                ?>
                                    <a class="simple-list" data-gtm-tr="NavOpt" data-gtm-campus="<?php echo $postType->post_title; ?>" data-gtm-ref="" data-gtm-type="<?php echo get_sub_field('titulo_collapsable'); ?>">
                                        <?php 
                                            switch ($contador) {
                                                case '0':
                                                    $claseCard = "color-card-1";
                                                    $claseIcono = "icon-color-1";
                                                    break;
                                                case '1':
                                                    $claseCard = "color-card-2";
                                                    $claseIcono = "icon-color-2";
                                                    break;
                                                case '2':
                                                    $claseCard = "color-card-3";
                                                    $claseIcono = "icon-color-3";
                                                    break;
                                                
                                                default:
                                                    break;
                                            }

                                         ?>
                                        <div type="button" class="list-item-mapa border-top-fix <?php echo $claseCard; ?>">
                                            <div class="item-icon list-icon">
                                            <?php echo do_shortcode( '[icono nombre="'.sanitize_title( get_sub_field('icono_collapsable') ).'" clase="'.$claseIcono.'"][/icono]' ); ?>
                                            </div>
                                            <div class="item-description"><?php echo get_sub_field('titulo_collapsable'); ?></div>
                                            <div class="arrow">
                                                <?php echo do_shortcode( '[icono nombre="'.sanitize_title("ver-abajo").'"][/icono]' ); ?>
                                            </div>
                                        </div>
                                        
                                    </a>
                                    <div class="text-colapse collapse animated flipInX desplegable">
                                        <div class="fix-margen-cards item-interno">
                                <?php                                        
                                        if( have_rows('item_referencia') ){
                                            // loop through the rows of data
                                            while ( have_rows('item_referencia') ) { the_row();
                                ?>                                                
                                            <span class="link-simple-list" title="<?php echo get_sub_field('referencia'); ?>">
                                                <label>
                                                    <label class="referencia"> <?php echo get_sub_field('referencia'); ?></label>
                                                </label>
                                            </span> 
                                <?php                                                
                                            }
                                        }
                                ?>
                                        </div>
                                    </div>
                                <?php
                                        $contador++;
                                    }
                                }
                                ?>                                
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </section>
</section><!-- /.Main <layout-->