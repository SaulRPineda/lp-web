<div id="contacto"></div>
<?php
/**
 * WP Post Template: desk_16_contacto
 */
?>

<?php
    $icono = NULL;
    $class_div = NULL;
    $contenido = NULL;
    $enlaceInicio = NULL;
    $enlaceFin = NULL;
    $telefonos = NULL;

    $contacto_telefonico = get_field('contacto_telefonico');
    $horarios = get_field('horarios');

    $dias_semana = array("Domingo", "Lunes", "Martes", "Miércoles", "Jueves", "Viernes", "Sábado", "Domingo");
    /*$dias_semana = array_keys( $horarios );*/
    /*array_push( $dias_semana, $dias_semana[0]);*/    

    $horarios_semana = array_values( $horarios );
    array_push( $horarios_semana, $horarios_semana[0] ); 

    date_default_timezone_set("America/Mexico_City");
    $dia = date('w');

    foreach ($contacto_telefonico['telefonos'] as $key => $value) {
        foreach ($value as $key => $numeros_telefonicos) {
            $telefonos[] = $numeros_telefonicos;
        }
    }
?>

<?php if( count($dias_semana > 0) ) { ?>

<script type="text/javascript">
    var diasSemana = new Array("Domingo","Lunes","Martes","Miércoles","Jueves","Viernes","Sábado");
    var f = new Date();
</script>

<section class="contactanos">
    <div class="container-fluid mt-4 mb-4 ml-5 mr-5">

        <div class="row justify-content-center mt-4 mb-4">
            <!--Card-->
            <div class="content-gral">
                <!-- <div class="col-lg-1 d-none d-lg-block phantom"></div> -->
                <div class="col-lg-12 col-xl-6 p-0 d-flex align-items-center">
                    <!--Card image-->
                    <?php 
                        $imagen = get_field('imagen', $theID);
                        $desk_imgDescripcion = get_field('desk_descripcion_contacto', $theID);
                    ?>
                    <div class="overlay fix-img lazy" data-src="<?php echo $imagen['url']; ?>" alt="<?php echo $imagen['alt'] ?>">
                        <div class="legend-contacto d-flex flex-column justify-content-center">
                            <span><?php echo wp_trim_words( $desk_imgDescripcion, 40 ); ?></span>
                        </div>
                    </div>
                    <!--/Card image-->
                </div>
                <div class="col-lg-12 col-xl-5 align-self-center">
                    <!--Card content-->
                    <div class="card-body card text-md-left card-left p-4">
                        <div class="texto-int-card">
                            <h4 class="w-100 titulo-contactanos mb-1"><?php echo get_field('titulo_contacto', $theID); ?></h4>
                            <?php
                                $direccion_campus = get_field('direccion_del_campus' ,$theID);
                                
                                if( $direccion_campus != NULL && $direccion_campus !="" && strlen($direccion_campus) > 1 ) { 
                            ?>
                            <div class="simple-list">
                                <div type="button" class="row list-item-container border-top-fix">
                                    <div class="col-2 item-icon d-flex align-center justify-content-center"><a href="//maps.google.es/?q=UNITEC, <?php echo get_field('direccion_del_campus' ,$theID); ?>" title="<?php echo get_field('direccion_del_campus' ,$theID); ?>" target="_blank" style="color:#757575;"><?php echo do_shortcode( '[icono nombre="'.sanitize_title( get_field('icono_direccion_campus') ).'"][/icono]' ); ?></a></div>
                                    <div class="col-10 item-description"><a href="//maps.google.es/?q=UNITEC, <?php echo get_field('direccion_del_campus' ,$theID); ?>" title="<?php echo get_field('direccion_del_campus' ,$theID); ?>" target="_blank"><?php echo get_field('direccion_del_campus' ,$theID); ?></a></div>
                                </div>
                            </div>
                            <?php } ?>

                            <?php if ( count($telefonos) > 0  ) { ?>
                            <div class="simple-list">
                                <div type="button" class="row list-item-container border-top-fix" data-toggle="collapse" href="#multiCollapseExample2" role="button" aria-expanded="false" aria-controls="multiCollapseExample1">
        
                                    <?php if ( count($telefonos) === 1 ) {
                                        $enlaceInicio = '<a href="tel:'.$telefonos[0].'" class="item-icon list-icon d-flex" data-gtm-tr="call" data-gtm-pos="seccion-contactanos" title="teléfono:'.$telefonos[0].'">';
                                        $enlaceFin = '</a>';
                                    } ?>

                                    <div class="col-2 flex-item item-icon list-icon d-flex align-items-center justify-content-center"><?php echo $enlaceInicio; ?><?php echo do_shortcode( '[icono nombre="'.sanitize_title( get_field('icono_contacto_telefonico') ).'"][/icono]' ).$enlaceFin; ?></div>
                                    <div class="col-10 flex-item item-description-arrow"><?php echo $enlaceInicio; ?><?php echo $telefonos[0].$enlaceFin; ?>
                                        <?php if ( count($telefonos) > 1 ) { ?>
                                            <div class="arrow ml-3"><?php echo do_shortcode( '[icono nombre="'.sanitize_title("ver-abajo").'"][/icono]' ); ?></div>
                                        <?php } ?>
                                    </div>                                        
                                </div>

                                <?php if ( count( $telefonos) > 1 ) { ?>
                                <div class="text-colapse card-flotante-tel collapse multi-collapse" id="multiCollapseExample2">
                                    <div class="fix-horario-campus-tel horario">
                                        <?php 
                                            foreach ($telefonos as $key => $value) {
                                        ?>
                                        <a class="link-simple-list" title="<?php echo $value; ?>"><label class="horario-item"><label> <?php echo $value; ?></label><label class="horario-hora"></label></label></a>
                                            <?php
                                            }
                                        ?>
                                    </div>
                                </div>
                                <?php } ?>
                            </div>
                            

                            <?php } //Valida si existen no telefónicos ?>

                            <?php if( !empty( $horarios['domingo'] ) ) { 
                            //if ( count($horarios) > 0 ) { ?>
                            <div class="simple-list">
                                <div type="button" class="row list-item-container border-top-fix" data-toggle="collapse" href="#multiCollapseExample1" role="button" aria-expanded="false" aria-controls="multiCollapseExample1">
                                    <div class="col-2 flex-item item-icon list-icon d-flex align-items-center justify-content-center"><?php echo do_shortcode( '[icono nombre="'.sanitize_title( get_field('icono_horarios') ).'"][/icono]' ); ?></div>
                                    <div class="col-10 flex-item item-description">
                                        <div class="row fix-horarios-button">
                                            <span class="col-3 horarios-pre"><script type="text/javascript">document.write( diasSemana[f.getDay()] );</script><?php //echo ucwords($dias_semana[$dia]); ?></span>
                                            <span class="col-5 horarios-pre-arrow horarios-pre"><?php echo $horarios_semana[$dia]; ?></span>
                                            <span class="col-1 arrow ml-3"><?php echo do_shortcode( '[icono nombre="'.sanitize_title("ver-abajo").'"][/icono]' ); ?></span>
                                        </div>
                                    </div>
                                </div>
                                <div class="text-colapse card-flotante collapse multi-collapse" id="multiCollapseExample1">
                                    <div class="fix-horario-campus horario">
                                        <?php 
                                            for ($i = 1; $i < count($dias_semana); $i++) {
                                        ?>
                                        <label class="horario-item"><label class="horario-dia"> <?php echo ucwords($dias_semana[$i]); ?></label><label class="horario-hora"><?php echo $horarios_semana[$i]; ?></label></label>
                                            <?php
                                            }
                                        ?>
                                    </div>
                                </div>
                            </div>
                            <?php } ?>

                            <?php //if ( !empty($contacto_telefonico['whatsapp']) ) { ?>
                            <!-- <div class="simple-list">
                                 <div type="button" class="row list-item-container border-top-fix">
                                    <div class="col-1 flex-item item-icon list-icon"><?php echo do_shortcode( '[icono nombre="'.sanitize_title( get_field('icono_whatsapp') ).'" clase="icon-size"][/icono]' ); ?></div>
                                     <div class="col-11 item-description"><a href="https://api.whatsapp.com/send?phone=<?php echo $contacto_telefonico['whatsapp']; ?>" target="_blank">Whatsapp<?php //echo $contacto_telefonico['whatsapp']; ?></a></div>
                                     <div class="col-11 item-description"><a href="https://clxt.ch/unitecg" data-gtm-tr="whats" data-gtm-pos="seccion-contacto | whats" target="_blank">Escríbenos por Whatsapp</a></div>
                                 </div>
                             </div> --> 
                            <?php //} ?>

                            <!-- Comienza implemantacio para agregar contacto redes sociales -->

                            <?php 

                                $contenidoRedes = get_field( 'desk_contactoSociales', $theID );

                                foreach ($contenidoRedes as $key => $dato) {
                                    $link = ( strpos( strtolower( trim( $dato['desk_urlContacto'] ) ), "unitec" )  ) ? 'href=' : 'onclick=';
                                    $target = ( strpos( strtolower( trim( $dato['desk_urlContacto'] ) ), "unitec" ) ) ? "target='_blank'" : "";
                                    if ( strpos( strtolower( trim( $dato['desk_urlContacto'] ) ), "#modal_frm_app") ) {
                                        $trackeoLocation =  'data-solicitud-location="Middle"';
                                        $ocultarWhats = '';
                                    } else if( strpos( strtolower( trim( $dato['desk_urlContacto'] ) ), "#modal-whatsapp")) {
                                        $trackeoLocation =  'data-gtm-tr="evWhats" data-gtm-accion="intención" data-gtm-seccion="contacto" data-gtm-depto=""';
                                        $ocultarWhats = 'd-none';
                                    } else if( strpos( strtolower( trim( $dato['desk_urlContacto'] ) ), "zopim.livechat.window.show()")) {
                                        $trackeoLocation =  'data-gtm-tr="chat" data-gtm-pos="intencion" data-gtm-location="Secci&oacute;n Contacto" data-gtm-etiqueta="con formulario"';
                                    } else {
                                        $trackeoLocation = '';
                                        $ocultarWhats = '';
                                    } 
                            ?>
                                <div class="<?php echo $ocultarWhats; ?> simple-list">
                                    <div type="button" class="row list-item-container border-top-fix">
                                        <div class="col-2 flex-item item-icon list-icon d-flex align-items-center justify-content-center"><a class="d-flex" <?php echo $link.' " '.trim($dato['desk_urlContacto']).' " '.$target ?> title="<?php echo $dato['desk_descripcionContacto']; ?>" <?php echo $trackeoLocation ?> ><?php echo do_shortcode( '[icono nombre="'.sanitize_title( $dato['desk_iconoContacto'] ).'" clase="icon-size"][/icono]' ); ?></a></div>
                                        <div class="col-10 item-description"><a <?php echo $link.' " '.trim($dato['desk_urlContacto']).' " '.$target ?> title="<?php echo $dato['desk_descripcionContacto']; ?>" <?php echo $trackeoLocation ?> ><?php echo $dato['desk_descripcionContacto']; ?></a></div>
                                    </div>
                                </div>
                            
                            <?php } ?>
                            
                            <!-- <div class="simple-list">
                                <div type="button" class="row list-item-container border-top-fix">
                                    <div class="col-1 flex-item item-icon list-icon"><a href="https://www.facebook.com/unitec" target="_blank"><?php echo do_shortcode( '[icono nombre="'.sanitize_title( 'facebook-card' ).'" clase="icon-size"][/icono]' ); ?></a></div>
                                    <div class="col-11 item-description"><a href="https://www.facebook.com/unitec" target="_blank">Facebook<?php //echo get_field('titulo_chat', $theID); ?></a></div>
                                </div>
                            </div>
                            
                            
                            <div class="simple-list">
                                <div type="button" class="row list-item-container border-top-fix">
                                    <div class="col-1 flex-item item-icon list-icon"><a href="https://twitter.com/unitecmx" target="_blank"><?php echo do_shortcode( '[icono nombre="'.sanitize_title( 'twitter-card' ).'" clase="icon-size"][/icono]' ); ?></a></div>
                                    <div class="col-11 item-description"><a href="https://twitter.com/unitecmx" target="_blank">Twitter<?php //echo get_field('titulo_mail', $theID); ?></a></div>
                                </div>
                            </div>
                            
                            <div class="simple-list">
                                <div type="button" class="row list-item-container border-top-fix">
                                    <div class="col-1 flex-item item-icon list-icon"><a htef="https://www.youtube.com/user/unitecmex/featured" target="_blank"><?php echo do_shortcode( '[icono nombre="'.sanitize_title( 'youtube-footer' ).'"][/icono]' ); ?></a></div>
                                    <div class="col-11 item-description"><a href="https://www.youtube.com/user/unitecmex/featured" target="_blank">You Tube<?php //echo get_field('titulo_mail', $theID); ?></a></div>
                                </div>
                            </div> -->
                        
                        </div>
                    </div>
                    <!--/.Card content-->
                </div>
                <!-- <div class="col-lg-1 d-none d-lg-block phantom"></div> -->
            </div>
            <!--/.Card--> 
        </div><!-- div para sdistinguir mapa curricular -->
    </div>
</section>


<?php } ?>

