<div id="nuevas_carreras"></div>
<?php
/**
 * WP Post Template: desk_5_1_nueva_oferta
 */
?>
<?php   
    /*Se insertan los plugins que necesita la seccion tal y como se llama el archivo*/
    // load_script( $pluginsNecesarios = array('flipcard') );
    /*End Se insertan los plugins que necesita la seccion tal y como se llama el archivo*/
    $nuevaCarrera = get_field('nuevas_carreras', $theID);
    $titulo = get_field('titulo_nuevas_carreras', $theID);
    $boton = get_field('boton', $theID); 
    $descripcion = get_field('desk_descripcion_nuevas_carreras', $theID);
    $active = NULL;
    $totalCaracteres = 78;
 ?>

<?php 
    if(have_rows("nuevas_carreras",$theID) ) {
?>
<section class="contenedor-nuevas-carreras align-items-center">
    <div class="svg-top w-100">
        <svg version="1.1" viewBox="0 0 1890 150">
            <g transform="translate(0,-902.36218)"></g>
            <path d="m 898.41609,-33.21176 0.01,0 -0.005,-0.009 -0.005,0.009 z"></path>
            <path d="m 898.41609,-33.21176 0.01,0 -0.005,-0.009 -0.005,0.009 z"></path>
            <path d="m 1925,0 0,150 -1925,0"></path>
        </svg>
    </div>
    <!-- Contenedor de la galería -->
    <div class="container-fluid col-12 d-flex align-items-center justify-content-center back-carreras">
        <div class="contenido-centrado w-100">
            <div class="row d-flex justify-content-center"> <!-- Contenedor de encabezado de la sección -->
                <article class="col-10 text-center d-flex justify-content-center" data-wow-offset="50">
                    <h4 class="w-75 titleSection"><?php echo get_field('titulo_nuevas_carreras', $theID); ?></h4>
                </article>
                <article class="col-10 text-center d-flex justify-content-center pl-3 pr-3" data-wow-offset="50">
                        <h6 class="w-100 descriptionSection mt-1"><?php echo get_field('desk_descripcion_nuevas_carreras', $theID); ?></h6>
                </article>
            </div> <!-- Fin del contenedor de encabezado de la sección -->

            
            <div class="row cards-nuevas-carreras w-100 m-0">
                <div class="col-1 d-flex align-items-center justify-content-center">
                    <!--Controls-->
                    <div class="controls-nuevas-carreras d-flex justify-content-center">
                        <a class="btn-floating waves-effect waves-light btn-arrow carrousel_left_nuevas_carreras d-none" data-slide="next" title="Anterior">
                            <?php echo do_shortcode( '[icono nombre="'.sanitize_title("ver-izquierda").'" clase="arrow-right"][/icono]' ); ?>
                        </a>                        
                    </div>
                    <!--Controls-->
                </div>
                <div class="col-10">
                    <div class="text-center">
                        <div class="row">
                            <div class="carrousel_nuevas_carreras w-100" data-pos="0">
                                <div class="carrousel_inner_nuevas_carreras">
                                    <ul class="d-flex">
                                    <?php
                                        $l = 0;
                                        foreach ($nuevaCarrera as $key => $contenido) {
                                            $thumbID = get_post_thumbnail_id( $contenido->ID );
                                            $urlImg = wp_get_attachment_url( $thumbID );
                                            $altImg = get_post_meta($thumbID, '_wp_attachment_image_alt', true);

                                            $imgHorizontal = get_field('imagen_destacada_horizontal', $contenido->ID);
                                            $urlImgHorizontal = $imgHorizontal['url'];
                                            
                                            $terms = get_the_terms( $contenido->ID, 'product_cat' );
                                            foreach ($terms as $key => $value) {
                                                if ($value ->parent==62){
                                                    switch ($value -> slug) {
                                                        case 'licenciatura':
                                                            $colorNuevoProd = "licenciatura";
                                                            break;
                                                        case 'preparatoria-2':
                                                            $colorNuevoProd = "preparatoria-2";
                                                            break;
                                                        case 'ingenieria':
                                                            $colorNuevoProd = "ingenieria";
                                                            break;
                                                        case 'diplomado':
                                                            $colorNuevoProd = "diplomado";
                                                            break;
                                                        case 'especialidad':
                                                            $colorNuevoProd = "especialidad";
                                                            break;
                                                        case 'maestria':
                                                            $colorNuevoProd = "maestria";
                                                            break;
                                                        
                                                        default:
                                                            $colorNuevoProd = "default";
                                                            break;
                                                    }
                                                }
                                            }

                                            $tituloCarrera = ( strlen( $contenido->post_title ) > $totalCaracteres )? substr($contenido->post_title, 0, $totalCaracteres)." ...": $contenido->post_title;
                                        ?>
                                        <li id="card-nuevas-carreras-<?php echo $l;?>" class="col-lg-6 col-sm-12 pb-2">
                                             <div class="w-100">
                                                <div class="card">
                                                    <a class="loader" href="<?php echo get_permalink($contenido->ID); ?>" data-gtm-tr="NewCareers" data-gtm-value="<?php echo $contenido->post_title; ?>" data-gtm-interaccion="Imagen">
                                                        <span class="<?php echo $colorNuevoProd; ?> new-ofert"><?php echo do_shortcode( '[icono nombre="'.sanitize_title("egresados").'"][/icono]' ); ?>&nbsp;&nbsp;NUEVA</span>
                                                        <img alt="<?php echo $altImg; ?>" class="img-fluid lazy" data-src="<?php echo $urlImgHorizontal; ?>">
                                                    </a>
                                                    <div class="card-wrapper m-0">
                                                        <div class="col-12 face front card-body">
                                                            <h4 class="card-title-nuevas-carreras text-left px-3"><?php echo $tituloCarrera; ?></h4>
                                                            <p class="card-text px-3 mb-1">
                                                                <?php echo wp_trim_words( get_field('descripcion_del_producto', $contenido->ID), 27, '...' ); ?>
                                                            </p>
                                                            <div class="share d-flex justify-content-between align-items-center px-3 mb-1">
                                                                <a class="rotate-btn d-flex" data-card="card-nuevas-carreras"><?php echo do_shortcode( '[icono nombre="'.sanitize_title("compartir").'" clase="activator p-0"][/icono]' ); ?></a>
                                                                <a href="<?php echo get_permalink($contenido->ID); ?>" class="linkSecundario verMasNewCareers" style="padding:.25rem .5rem!important;" data-gtm-tr="NewCareers" data-gtm-value="<?php echo $contenido->post_title; ?>" data-gtm-interaccion="Ver más"><?php echo $boton; ?></a>
                                                            </div>
                                                        </div>
                                                        <div class="face back card-reveal" id="share-nuevas-carreras">
                                                            <?php echo do_shortcode('[share cerrar="card-nuevas-carreras" pagina="'.get_permalink($contenido->ID).'"][/share]');?>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </li>
                                        <?php
                                        $l++;
                                        }
                                        $none = ( $l > 2 )? "": "d-none";
                                        ?>
                                    </ul>
                                </div>
                           </div>
                        </div>
                        <!--Grid row-->
                    </div>
                    <!--Section: Products v.4-->
                </div>
                <div class="col-1 d-flex align-items-center justify-content-center">
                    <!--Controls-->
                    <div class="controls-nuevas-carreras d-flex justify-content-center">
                        <a class="btn-floating waves-effect waves-light btn-arrow carrousel_right_nuevas_carreras <?php echo $none; ?>" data-slide="prev" title="Siguiente">
                            <?php echo do_shortcode( '[icono nombre="'.sanitize_title("ver-derecha").'" clase="arrow-left"][/icono]' ); ?>
                        </a>                       
                    </div>
                    <!--Controls-->
                </div>
            </div>
        </div>
    </div>
    <div class="svg-bottom w-100">
        <svg version="1.1" viewBox="0 0 1890 150">
            <g transform="translate(0,-902.36218)"></g>
            <path d="m 898.41609,-33.21176 0.01,0 -0.005,-0.009 -0.005,0.009 z"></path>
            <path d="m 898.41609,-33.21176 0.01,0 -0.005,-0.009 -0.005,0.009 z"></path>
            <path d="m 1925,0 0,150 -1925,0"></path>
        </svg>
    </div>
    <!-- Fin del contenedor de galería -->
    <!-- <input type="hidden" name="totalTestimoniales" id="totalTestimoniales" value="<?php echo $l; ?>"> -->
</section>
<?php } ?>