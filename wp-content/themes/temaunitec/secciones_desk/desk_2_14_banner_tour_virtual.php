<?php
/**
 * WP Post Template: desk_2_14_tour_virtual
 */
?>
<?php

$desk_idPost = get_the_ID(); 
// $desk_postType = get_post( $idPost ); 
// $permalink = get_permalink($idPost);
$grupoBanner = get_field('group_banner', $theID);
$grupoBannerContenido = get_field('desk_group_banner_contenido', $theID);

$callToAction = $grupoBannerContenido['call_to_action'];
$bulletsGroup = $grupoBannerContenido['bullets_cita_en_linea'];

//con este se obtiene el campo completo
$selectorTipoImg = $grupoBanner['selector_tipo_imagen_video'];

$imgBannerDesk = $grupoBanner['banner_fondo_estatico_desk'];
    $urlBannerDesk = $imgBannerDesk["url"];
    $altBannerDesk = $imgBannerDesk["alt"];
$imgVideoDesk = $grupoBanner['banner_fondo_video_desk'];

$desk_TituloVirtual = $grupoBannerContenido['desk_titulo_banner'];
$desk_descripcionBannerVirtual = $grupoBannerContenido['descripcion_corta_banner'];


$desk_callToActionBannerVirtual = $callToAction['name_call_to_action'];
$desk_urlCallToActionBannerVirtual = $callToAction['url_call_to_action'];

$horarios = array(
    'CUITLAHUAC'=> ["name"=>"Cuitláhuac, CDMX","horarios"=>[
            1=>["inicio"=>9,"fin"=>20],//lunes
            2=>["inicio"=>9,"fin"=>20],
            3=>["inicio"=>9,"fin"=>20],
            4=>["inicio"=>9,"fin"=>20],
            5=>["inicio"=>9,"fin"=>20],
            6=>["inicio"=>9,"fin"=>16],
            0=>["inicio"=>10,"fin"=>14]//domingo
        ]],
    'LOS REYES'=> ["name"=>"Los Reyes, CDMX","horarios"=>[
            1=>["inicio"=>9,"fin"=>20],//lunes
            2=>["inicio"=>9,"fin"=>20],
            3=>["inicio"=>9,"fin"=>20],
            4=>["inicio"=>9,"fin"=>20],
            5=>["inicio"=>9,"fin"=>20],
            6=>["inicio"=>9,"fin"=>16],
            0=>["inicio"=>10,"fin"=>14]//domingo
        ]],
    'MARINA'=> ["name"=>"Marina, CDMX","horarios"=>[
            1=>["inicio"=>9,"fin"=>20],//lunes
            2=>["inicio"=>9,"fin"=>20],
            3=>["inicio"=>9,"fin"=>20],
            4=>["inicio"=>9,"fin"=>20],
            5=>["inicio"=>9,"fin"=>20],
            6=>["inicio"=>9,"fin"=>16],
            0=>["inicio"=>10,"fin"=>14]//domingo
            ]],
    'SUR'=> ["name"=>"Sur, CDMX","horarios"=>[
            1=>["inicio"=>9,"fin"=>20],//lunes
            2=>["inicio"=>9,"fin"=>20],
            3=>["inicio"=>9,"fin"=>20],
            4=>["inicio"=>9,"fin"=>20],
            5=>["inicio"=>9,"fin"=>20],
            6=>["inicio"=>9,"fin"=>16],
            0=>["inicio"=>10,"fin"=>14]//domingo
        ]],
    'ATIZAPAN'=> ["name"=>"Atizapán, Edo. Méx.","horarios"=>[
            1=>["inicio"=>9,"fin"=>20],//lunes
            2=>["inicio"=>9,"fin"=>20],
            3=>["inicio"=>9,"fin"=>20],
            4=>["inicio"=>9,"fin"=>20],
            5=>["inicio"=>9,"fin"=>20],
            6=>["inicio"=>9,"fin"=>16],
            0=>["inicio"=>10,"fin"=>14]//domingo
        ]],
    'ECATEPEC'=> ["name"=>"Ecatepec, Edo. Méx","horarios"=>[
            1=>["inicio"=>9,"fin"=>20],//lunes
            2=>["inicio"=>9,"fin"=>20],
            3=>["inicio"=>9,"fin"=>20],
            4=>["inicio"=>9,"fin"=>20],
            5=>["inicio"=>9,"fin"=>20],
            6=>["inicio"=>9,"fin"=>16],
            0=>["inicio"=>10,"fin"=>14]//domingo
        ]],
    'TOLUCA'=> ["name"=>"Toluca, Edo. Méx","horarios"=>[
            1=>["inicio"=>9,"fin"=>20],//lunes
            2=>["inicio"=>9,"fin"=>20],
            3=>["inicio"=>9,"fin"=>20],
            4=>["inicio"=>9,"fin"=>20],
            5=>["inicio"=>9,"fin"=>20],
            6=>["inicio"=>9,"fin"=>16],
            0=>["inicio"=>10,"fin"=>14]//domingo
        ]],
    'GUADALAJARA'=> ["name"=>"Guadalajara","horarios"=>[
            1=>["inicio"=>9,"fin"=>20],//lunes
            2=>["inicio"=>9,"fin"=>20],
            3=>["inicio"=>9,"fin"=>20],
            4=>["inicio"=>9,"fin"=>20],
            5=>["inicio"=>9,"fin"=>20],
            6=>["inicio"=>9,"fin"=>16],
            0=>["inicio"=>10,"fin"=>14]//domingo
        ]],
    'LEON'=> ["name"=>"León, GTO","horarios"=>[
            1=>["inicio"=>9,"fin"=>20],//lunes
            2=>["inicio"=>9,"fin"=>20],
            3=>["inicio"=>9,"fin"=>20],
            4=>["inicio"=>9,"fin"=>20],
            5=>["inicio"=>9,"fin"=>20],
            6=>["inicio"=>9,"fin"=>16],
            0=>["inicio"=>10,"fin"=>14]//domingo
        ]],
    'QUERETARO'=> ["name"=>"Querétaro, QRO","horarios"=>[
            1=>["inicio"=>9,"fin"=>20],//lunes
            2=>["inicio"=>9,"fin"=>20],
            3=>["inicio"=>9,"fin"=>20],
            4=>["inicio"=>9,"fin"=>20],
            5=>["inicio"=>9,"fin"=>20],
            6=>["inicio"=>9,"fin"=>16],
            0=>["inicio"=>10,"fin"=>14]//domingo
        ]]
);

/*Definición de domingos Permitidos By SRP 29-05-2019*/
$permitidos = array(
    'ATIZAPAN'=> [  "2019-06-30",
                    "2019-07-14", 
                    "2019-07-21", 
                    "2019-07-28", 
                    "2019-08-04", 
                    "2019-08-11", 
                    "2019-08-18", 
                    "2019-08-25", 
                    "2019-09-01", 
                    "2019-09-08", 
                    "2019-09-15", 
                    "2019-09-22", 
                    "2019-09-29"],//domingo

    'CUITLAHUAC'=> ["2019-06-30",
                    "2019-07-14", 
                    "2019-07-21", 
                    "2019-07-28", 
                    "2019-08-04", 
                    "2019-08-11", 
                    "2019-08-18", 
                    "2019-08-25", 
                    "2019-09-01", 
                    "2019-09-08", 
                    "2019-09-15", 
                    "2019-09-22", 
                    "2019-09-29"],//domingo

    'ECATEPEC'=> [  "2019-06-30",
                    "2019-07-14", 
                    "2019-07-21", 
                    "2019-07-28", 
                    "2019-08-04", 
                    "2019-08-11", 
                    "2019-08-18", 
                    "2019-08-25", 
                    "2019-09-01", 
                    "2019-09-08", 
                    "2019-09-15", 
                    "2019-09-22", 
                    "2019-09-29"],//domingo

    'GUADALAJARA'=> ["2019-06-30", 
                    "2019-07-14", 
                    "2019-07-21", 
                    "2019-07-28", 
                    "2019-08-04", 
                    "2019-08-11", 
                    "2019-08-18", 
                    "2019-08-25", 
                    "2019-09-01", 
                    "2019-09-08",  
                    "2019-09-15", 
                    "2019-09-22", 
                    "2019-09-29"],//domingo

    'LEON'=> [      "2019-06-30", 
                    "2019-07-28", 
                    "2019-08-18", 
                    "2019-08-25", 
                    "2019-09-01", 
                    "2019-09-08",  
                    "2019-09-15"],//domingo

    'LOS REYES'=> [ "2019-06-30",
                    "2019-07-14", 
                    "2019-07-21", 
                    "2019-07-28", 
                    "2019-08-04", 
                    "2019-08-11", 
                    "2019-08-18", 
                    "2019-08-25", 
                    "2019-09-01", 
                    "2019-09-08", 
                    "2019-09-15", 
                    "2019-09-22", 
                    "2019-09-29"],//domingo

    'MARINA'=> [    "2019-06-30",
                    "2019-07-14", 
                    "2019-07-21", 
                    "2019-07-28", 
                    "2019-08-04", 
                    "2019-08-11", 
                    "2019-08-18", 
                    "2019-08-25", 
                    "2019-09-01", 
                    "2019-09-08", 
                    "2019-09-15", 
                    "2019-09-22", 
                    "2019-09-29"],//domingo

    'QUERETARO'=> [ "2019-07-14", 
                    "2019-07-21", 
                    "2019-07-28", 
                    "2019-08-04", 
                    "2019-08-11", 
                    "2019-08-18", 
                    "2019-08-25", 
                    "2019-09-01", 
                    "2019-09-08",  
                    "2019-09-15", 
                    "2019-09-22", 
                    "2019-09-29"],//domingo

    'SUR'=> [       "2019-06-30",
                    "2019-07-14", 
                    "2019-07-21", 
                    "2019-07-28", 
                    "2019-08-04", 
                    "2019-08-11", 
                    "2019-08-18", 
                    "2019-08-25", 
                    "2019-09-01", 
                    "2019-09-08", 
                    "2019-09-15", 
                    "2019-09-22", 
                    "2019-09-29"],//domingo

    'TOLUCA'=> ["2019-06-02", 
                "2019-06-09", 
                "2019-07-14", 
                "2019-07-21", 
                "2019-07-28", 
                "2019-08-04", 
                "2019-08-11", 
                "2019-08-18", 
                "2019-08-25", 
                "2019-09-01", 
                "2019-09-08",  
                "2019-09-15",  
                "2019-09-22", 
                "2019-09-29"]//domingo
);
/*End Definición de domingos Permitidos By SRP 29-05-2019*/
    
?>
<script type="text/javascript">
    function checkCookie(valor) {
        var user = getCookie(valor);
        if (user != "") {
            return true;
        }
        else {
            return false;
        }
    }
    function getCookie(cname) {
        var name = cname + "=";
        var ca = document.cookie.split(';');
        for (var i = 0; i < ca.length; i++) {
            var c = ca[i];
            while (c.charAt(0) == ' ') c = c.substring(1);
            if (c.indexOf(name) == 0) return c.substring(name.length, c.length);
        }
        return "";
    }
    function campusDynamics(campus) {
        switch (campus) {
            case "ATZ":
                return "ATIZAPAN";
                break;
            case "REY":
                return "LOS REYES";
                break;
            case "MAR":
                return "MARINA";
                break;
            case "ECA":
                return "ECATEPEC";
                break;
            case "SUR":
                return "SUR";
                break;
            case "CUI":
                return "CUITLAHUAC";
                break;
            case "LEO":
                return "LEON";
                break;
            case "TOL":
                return "TOLUCA";
                break;
            case "GDL":
                return "GUADALAJARA";
                break;
            case "ONL":
                return "EN LINEA";
                break
            case "QRO":
                return "QUERETARO";
                break
            default:
                return "MARINA";
        }
    }
    const params = new URLSearchParams(window.location.search);  
    var validaEmail = params.get("email");
    if(validaEmail !== null){
        email = params.get("email");

        var validHS = false;
        var nombre;
        var email;
        var lastname;
        var campus;
        var campusHbpst;        
        var noPersona;
        var subnivel;    
        var publicContactURL;

        var cookieValidation = false;

        $.ajax({
            type: "GET",
            url: "wp-content/themes/temaunitec/assets/html/AgendarCita/verificaCorreoHB.php",
            data: {
                email: email        
            },
            dataType: 'json',
            success: function (res) {           
                // res = JSON.parse(res);
                if (res['existeHS']) {

                    validHS = res['existeHS'];
                    // contactProps = json_decode(HSContact,true);
                    email = res.properties['email']['value'];
                    nombre = res.properties['firstname']['value']
                    lastname = res.properties['lastname']['value'];
                    campus = res.properties['campus']['value'];
                    campusHbpst = res.properties['campus']['value'];                
                    noPersona = res.properties['nopersona']['value'];
                    subnivel = res.properties['subnivelinteres']['value'];          
                    publicContactURL = res['profile-url'];
    
                    jQuery('#agendar-cita').attr({'data-fn': nombre, 'data-ln': lastname, 'data-np': noPersona, 'data-url': publicContactURL, 'data-cmp': campusHbpst,});
                    jQuery('#co-email').val(email);
                    jQuery('#co-email').attr('disabled', true);
    
                }else{
                    // alert('no existe correo');      
                }          
            },
            error: function (xhr, textStatus, error) {
                console.log(xhr);
                console.log(xhr.statusText);
                console.log(textStatus);
                console.log(error);
            },
            beforeSend: function () {          
            },
            complete: function () {
                
            }
        })
    }else if(checkCookie('c_form_data')) {
        var objCookie = JSON.parse(decodeURIComponent(getCookie('c_form_data')));
        var objLocalstorage = JSON.parse(localStorage.getItem("jsonUsr"));
        console.log(objCookie);
        if(typeof objCookie.email !== 'undefined'){
            email = objCookie.email;
            nombre = objCookie.nombre;
            lastname = objCookie.apaterno;
            campus = campusDynamics(objLocalstorage.campus);
            subnivel = objCookie.subNivelInteres;
            
            cookieValidation = true;
        }
    }
    else{
    }
    var jsonhorarios = <?php echo json_encode($horarios);?>;
    var jsonpermitidos = <?php echo json_encode($permitidos);?>;
</script>
<div id="cita-campus"></div>
<section class="cita">
    <?php
        switch ($selectorTipoImg) {
            case 'fondo_fijo':
                $background = "<div style='background-image:url(".$urlBannerDesk.");' alt='".$altBannerDesk."' class='img-background'></div>";
            break;
    
            case 'fondo_video':
                $background = "<video autoplay muted loop webkit-playsinline playsinline class='video-tour' style='background: transparent url(".$imgVideoDesk.") no-repeat;'><source src='".$imgVideoDesk."' type='video/mp4'></video>";
            break;
            
            default:
                $background = "<div style='background-image:url(".$urlBannerDesk.");' alt='".$altBannerDesk."' class='img-background'></div>";
            break;
        }
    ?>
    <?php echo $background ?>

    <div class="view p-heredada d-flex align-items-center">
        <div class="mask container container-formulario d-flex align-items-center">
            <div class="row col-12 pl-md-5 pl-sm-0">
                <div class="col-md-6 pl-md-2 col-sm-12 pl-sm-1 d-flex flex-column justify-content-center">
                    <div class="d-flex align-items-center">
                        <h1 class="texto_claro tituloBanner h1 m-0"><?php echo $desk_TituloVirtual; ?></h1>
                    </div>
                    <h2 class="texto_claro subtituloBanner mt-2"><?php echo $desk_descripcionBannerVirtual; ?></h2>
                    <ul class="ml-3">
                        <?php
                            foreach ($bulletsGroup as $key => $bullet) {                               
                            $iconBullet = $bullet['icono_bullet'];
                            $descriptionBullet = $bullet['descripcion_bullet'];
                        ?>                                        
                            <li class="row text-white bulletCita my-1">
                                <div class="col-1 d-flex align-items-center justify-content-center"><?php echo do_shortcode( '[icono nombre="'.sanitize_title($iconBullet).'" clase=""][/icono]' ); ?></div>
                                <div class="col-10 p-0"><?php echo $descriptionBullet;?></div>
                            </li>
                        <?php
                            }
                        ?>
                    </ul>
                    <?php
                        if ($desk_callToActionBannerVirtual != ""){
                            echo "<a href='".$desk_urlCallToActionBannerVirtual."' class='btn back-naranja-unitec waves-effect waves-light m-0 mt-1 w-50' title='".$desk_callToActionBannerVirtual."' id='btn-carousel-0'>".$desk_callToActionBannerVirtual."</a>";
                        } 
                    ?>
                </div>
                <div class="col-md-5 col-sm-12 offset-1">
                    <div class="card w-100 card-formulario">
                        <div class="card-body mx-4" id="paso1-frm-absorcion">
                            <div class="row my-3">
                                <h4 class="text-center titulo-formulario-expuesto w-100">¡Descubre más de la UNITEC!</h4>
                                <p class="m-0 text-center descFormExpuesto w-100">Visita nuestras instalaciones y vive el ambiente universitario.</p>
                            </div>
                            <div class="md-form">
                                <div class="row justify-content-center my-1 correo">
                                    <div class="col-1 d-flex align-items-center justify-content-center icon-style">
                                        <span class="icon-u-correo"></span>
                                    </div>
                                    <div class="col-10 md-select-form">
                                        <label for="email" class="pl-3 m-0">Correo: <span>*</span></label>
                                        <input id="co-email"  name="email" class="form-control m-0 p-1" type="email" autocomplete="off" maxlength="100" minlength="1" ref="input">
                                        <div class="invalid-feedback m-0">Por favor, ingresa un correo valido</div>
                                    </div>
                                </div>
                                <div class="row justify-content-center mt-2 campus">
                                    <div class="col-1 d-flex align-items-center justify-content-center icon-style">
                                        <span class="icon-u-campus"></span>
                                    </div>                          
                                    <div class="col-10 md-select-form">
                                        <label for="campus" class="pl-3 m-0 active">Campus: <span>*</span></label>
                                        <div class="dropdown inp-campus">
                                            <button type="button" id="input-campus" class="btn-select dropdown-toggle d-flex align-items-center justify-content-between px-1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" ref="dropdown">
                                                Selecciona un campus
                                            </button>
                                            <div id="co-campus" class="dropdown-menu p-1 m-0 w-100 altura-campuscita scrollbar-ripe">
                                            <?php
                                                foreach ($horarios as $k => $c) {                               
                                                $campus = $c['name'];
                                            ?>                                        
                                                <a data-c="<?php echo $k?>" class="dropdown-item align-items-center campus p-2">
                                                    <?php echo $c['name'];?>
                                                </a>
                                            <?php
                                                }
                                            ?>
                                            </div>
                                        </div>
                                        <div id="validacion-campus" class="invalid-feedback m-0">Por favor, ingresa un correo valido</div>
                                    </div>
                                </div>
                                <div class="row justify-content-center my-4 dia">
                                    <div id="iconCita" class="col-1 d-flex align-items-center justify-content-center icon-style icon-disabled">
                                        <span class="icon-u-calendario"></span>
                                    </div>                          
                                    <div class="col-10 md-select-form">
                                        <label for="dia" class="pl-3 m-0 active">Día: <span>*</span></label>
                                        <div class="dropdown inp-fecha">
                                            <button id="input-fecha" type="button" class="btn-select dropdown-toggle d-flex align-items-center justify-content-between px-1 disabled" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" ref="dropdown" disabled>
                                                Selecciona un día
                                            </button>
                                        <div id="co-fecha" class="dropdown-menu p-1 m-0 w-100 altura-campuscita scrollbar-ripe">

                                                
                                            
                                            </div>
                                            <input type="hidden" name="valorDomingo" id="valorDomingo" value="">
                                        </div>
                                        <div id="validacion-fecha" class="invalid-feedback m-0">Por favor, ingresa un correo valido</div>
                                    </div>
                                </div>
                                <div id="row-hora" class="row justify-content-center mt-1 hora d-none">
                                    <div class="col-1 d-flex align-items-center justify-content-center icon-style">
                                        <span class="icon-u-horarios"></span>
                                    </div>                          
                                    <div class="col-10 md-select-form d-flex align-items-center">
                                        <label for="hora" id="label-hora" class="pl-3 m-0 ">Hora: <span>*</span></label>
                                        <div class="col-12 p-0 mt-1" >
                                            <section class="inp-hora" id="tabNav">
                                                <i class="icon-u icon-u-tab-izquierda leftTabNavigation direccionales dirLeft px-1" style="display: none;"></i>
                                                <div class="tabs-wrapper" id="elementoConScroll">
                                                    <ul id="co-hora" class="nav classic-tabs  alturaFila m-0" role="tablist">              
                                                    </ul>
                                                </div>
                                                <i class="icon-u icon-u-tab-derecha rightTabNavigation direccionales dirRight px-1" style="display: none;"></i>
                                            </section>
                                            <div class="invalid-feedback m-0">Por favor, ingresa un correo valido</div>
                                        </div>
                                        <!-- <div class="dropdown inp-hora w-100">
                                            <button id="input-hora" type="button" class="btn-select dropdown-toggle d-flex align-items-center justify-content-between" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" ref="dropdown" style="display:none !important;">
                                                Selecciona una hora
                                            </button>
                                        </div> -->
                                    </div>
                                    <!-- <div class="col-12 " >
                                        <section class="inp-hora" id="tabNav">
                                            <i class="icon-u icon-u-tab-izquierda leftTabNavigation direccionales dirLeft px-1" style="display: none;"></i>
                                            <div class="tabs-wrapper" id="elementoConScroll">
                                                <ul id="co-hora" class="nav classic-tabs  alturaFila m-0" role="tablist">              
                                                </ul>
                                            </div>
                                            <i class="icon-u icon-u-tab-derecha rightTabNavigation direccionales dirRight px-1" style="display: none;"></i>
                                        </section>
                                        <div class="invalid-feedback m-0">Por favor, ingresa un correo valido</div>
                                    </div> -->
                                </div>

                            </div>
                            <div class="frm-politicas my-1 text-center">
                                <label  class="politicas light">
                                    Al hacer clic en <span class="span-continuar">"AGENDAR CITA"</span>, reconoces haber leído las <a href="//www.unitec.mx/politicas-de-privacidad/" target="_blank">Políticas de Privacidad</a> y confirmas estar de acuerdo con el uso de ellas, así como los <a href="//www.unitec.mx/terminos-y-condiciones/" target="_blank">Términos y Condiciones</a> del sitio.
                                </label>
                            </div>
                            <div class="row mb-1">
                                <div class="w-100 px-3">
                                    <button <?php if(isset($_GET['fuente_cita']) && trim($_GET['fuente_cita'])=='sitio_web'){echo 'data-fuente="'.trim($_GET['fuente_cita']).'"';}?>  class="btn btn-frm-expuesto w-100 m-0" id="agendar-cita">AGENDAR CITA</button>
                                </div>
                            </div>
                        </div>

                        <div id="proceso-absorcion"></div>
                        <div id="msj-guardado" class="card-blue-absorcion h-100 w-100 align-items-center"></div>

                    </div>
                </div>
            </div>
        </div>
    </div>

</section>