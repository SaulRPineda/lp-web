<div id="listado"></div>
<?php
/**
 * WP Post Template: desk_6_descripcion_links
 */
?>

<?php
    $respaldoEconomicoContent = get_field('respaldo_economico', $theID);
    $respaldoEconomicoBullets = get_field('contenido_respaldo_economico', $theID); 
?>

<section class="descripcion-links d-flex">
    <div class="container-fluid wow fadeIn d-flex justify-content-center align-items-center">
        <!-- Live preview-->
        <div class="col-lg-10 p-0 justify-content-center mt-3 mb-5">            
            <article class="col-12 text-center d-flex justify-content-center" data-wow-offset="50">
                <h4 class="w-75 titleSection"><?php echo $respaldoEconomicoContent->post_title; ?></h4>
                <?php $thumbID = get_post_thumbnail_id( $respaldoEconomicoContent->ID );
                    $urlImg = wp_get_attachment_url( $thumbID );
                    $altImg = get_post_meta($thumbID, '_wp_attachment_image_alt', true);
                ?>
            </article>
            <article class="col-12 text-center mt-1 d-flex justify-content-center" data-wow-offset="50">
                <h6 class="w-100 descriptionSection mt-1"><?php echo get_field('contenido_personalizado', $theID); ?></h6>
            </article>
            <!-- Respaldo Económico-->
            <div class="respaldo-eco ml-3 mr-3 justify-content-center">            
                <!-- Card deck -->
                <div class="row d-flex justify-content-center">
                    <?php foreach ($respaldoEconomicoBullets as $key => $bullet) {

                        switch ($key) {
                            case '0':
                                $claseCard = "color-card-1";
                                $claseIcono = "icon-color-1";
                                break;
                            case '1':
                                $claseCard = "color-card-2";
                                $claseIcono = "icon-color-2";
                                break;
                            case '2':
                                $claseCard = "color-card-3";
                                $claseIcono = "icon-color-3";
                                break;
                            
                            default:
                                break;
                        }

                     ?>
                    <!-- Card -->
                    <div class="col-xl-4 col-lg-4 col-md-8 col-sm-12 mt-1 materiales-cards pl-2 pr-2">
                        <div class="card p-3 h-100 <?php echo $claseCard; ?>">    
                            <div class="card-body respaldo-card">
                                <!--Title-->
                                <h4 class="respaldo-title"><?php echo $bullet['titulo']; ?></h4>
                                <!--Text-->
                                <p class="respaldo-text"><?php echo wp_trim_words( $bullet['desk_descripcionContenido'], 40 ); ?></p>    
                            </div>
                            <div class="row extra-info">
                                <div class="col-9 d-flex align-items-center">
                                    <a href="<?php echo $bullet['link']; ?>" class="respaldo-mas" title="<?php echo $bullet['desk_TextoCallToAction']; ?>" data-gtm-tr="EcSup" data-gtm-economic="<?php echo $bullet['titulo']; ?>"><?php echo $bullet['desk_TextoCallToAction']; ?></a>
                                </div>
                                <div class="col-3 d-flex justify-align-center">
                                    <span><?php echo do_shortcode( '[icono nombre="'.sanitize_title($bullet['icono']).'" clase="'.$claseIcono.'"][/icono]' ); ?></span>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- Card -->
                    <?php } ?>
                </div>
                <!-- Card deck -->
            </div>
            <!-- /.Respaldo Económico-->
        </div>
        <!-- /.Live preview-->
    </div><!-- /.Main layout-->
</section><!-- Background -->
