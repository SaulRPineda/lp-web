<div id="testimoniales"></div>
<?php
/**
 * WP Post Template: desk_12_testimoniales
 */
?>
<?php   
    /*Se insertan los plugins que necesita la seccion tal y como se llama el archivo*/
    load_script( $pluginsNecesarios = array('flipcard') );
    /*End Se insertan los plugins que necesita la seccion tal y como se llama el archivo*/
 ?>

<?php 
$l = 0;
if(have_rows("contenido_testimoniales",$theID) ) {
?>
<section class="container-fluid  contenedor-testimoniales wow fadeIn d-flex justify-content-center align-items-center p-0">
    <!-- Contenedor de la galería -->
    <div class="col-lg-12 justify-content-center d-flex align-items-center">
        <div class="contenido-centrado w-100">
            <div class="row d-flex justify-content-center"> <!-- Contenedor de encabezado de la sección -->
                <article class="col-10 text-center d-flex justify-content-center" data-wow-offset="50">
                    <h4 class="w-75 titleSection"><?php echo get_field('titulo_card_testimoniales', $theID); ?></h4>
                </article>
                <article class="col-10 text-center d-flex justify-content-center pl-3 pr-3" data-wow-offset="50">
                        <h6 class="w-100 descriptionSection mt-1"><?php echo get_field('contenido_card_testimoniales', $theID); ?></h6>
                </article>
            </div> <!-- Fin del contenedor de encabezado de la sección -->

            
            <div class="row cards-testimoniales w-100 m-0">
                <div class="col-1 d-flex align-items-center justify-content-center">
                    <!--Controls-->
                    <div class="controls-testimoniales d-flex justify-content-center">
                        <a class="btn-floating waves-effect waves-light btn-arrow carrousel_left_testimoniales d-none" data-slide="next" title="Anterior">
                            <?php echo do_shortcode( '[icono nombre="'.sanitize_title("ver-izquierda").'" clase="arrow-right"][/icono]' ); ?>
                        </a>                        
                    </div>
                    <!--Controls-->
                </div>
                <div class="col-10">
                    <!--Section: Products v.4-->
                    <section class="text-center">
                        <!--Grid row-->
                        <div class="row">
                            <div class="carrousel_testimoniales w-100" data-pos="0">
                                <div class="carrousel_inner_testimoniales">
                                    <ul class="d-flex">
                                        <?php 
                                                while( have_rows('contenido_testimoniales') ) { the_row();

                                                    $contenido_testimoniales = get_sub_field('contenido_testimonios', $theID);

                                                    foreach ($contenido_testimoniales as $key => $value) {
                                                        $testimonial_seleccionado = get_field('fotografias_testimonios', $value);
                                        ?>
                        
                                        <?php if( strlen($testimonial_seleccionado[1]['url']) > 0 && strlen( $testimonial_seleccionado[1]['description'] ) == 0 ) { ?>                                        
                                        <!--Grid column-->
                                        <li class="col-lg-4 col-sm-12 pt-2 pb-2">
                                            <?php echo $testimonial_seleccionado[1]['description']; ?>
                                             <div class="w-100 card-wrapper altura-card">
                                                <div id="card-testimonial-<?php echo $l;?>" class="card-testimonial card-rotating effect__click text-center h-100 w-100">

                                                    <div class="face front">
                                                        <div class="cover lazy" data-src="<?php echo $testimonial_seleccionado[1]['url']; ?>" alt="<?php $testimonial_seleccionado[1]['alt']; ?>"> <!-- Se coloca el uno ya que en la galeria la posición Cero es la portada Mobile -->
                                                            <!-- <img src="<?php //echo $testimonial_seleccionado[1]['url']; ?>" alt="<?php //$testimonial_seleccionado[1]['alt']; ?>" /> -->
                                                        </div>
                                                        <div class="content pt-0">
                                                            <div class="main">
                                                                <div class="name"><?php echo get_field('nombre_testimonio', $value); ?></div>
                                                                <div class="ocupacion"><?php echo get_field('perfil_testimonio', $value); ?></div>
                                                                <div class="profesion"><?php echo get_field('nombre_licenciatura', $value); ?></div>
                                                                <?php echo "<span class='comilla'>&quot;</span>".get_field('contenido_testimonio', $value). "<span class='comilla'>&quot;</span>" ; ?>      
                                                            </div>
                                                        </div>
                                                    </div> <!-- end front panel -->

                                                </div> <!-- end card -->
                                            </div> <!-- end card-container -->                          
                                        </li>
                                        <!--Grid column-->
                                        <?php $l++;
                                        } ?> <!-- End File exists -->

                                        <?php }
                                            }
                                        $none = ( $l > 3 )? "": "d-none"; ?>
                                    </ul>
                                </div>
                           </div>
                        </div>
                        <!--Grid row-->
                    </section>
                    <!--Section: Products v.4-->
                </div>
                <div class="col-1 d-flex align-items-center justify-content-center">
                    <!--Controls-->
                    <div class="controls-testimoniales d-flex justify-content-center">
                        <a class="btn-floating waves-effect waves-light btn-arrow carrousel_right_testimoniales <?php echo $none; ?>" data-slide="prev" title="Siguiente">
                            <?php echo do_shortcode( '[icono nombre="'.sanitize_title("ver-derecha").'" clase="arrow-left"][/icono]' ); ?>
                        </a>                       
                    </div>
                    <!--Controls-->
                </div>
            </div>
        </div>
    </div>
    <!-- Fin del contenedor de galería -->
    <!-- <input type="hidden" name="totalTestimoniales" id="totalTestimoniales" value="<?php //echo $l; ?>"> -->
</section>
<?php } ?>

