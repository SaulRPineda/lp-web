<?php
/*
 * WP Post Template: desk_2_5_banner_buscador
 */
 ?>

<!DOCTYPE HTML> 
<html amp>
    <head>
        <meta charset="utf-8"> 
        <title>Banner Buscador Unitec</title> 
        <link rel="canonical" href="$REGULAR_HTML_URL" /> 
        <meta name="viewport" content="width=device-width,minimum-scale=1,initial-scale=1"> 
        <script async src="https://cdn.ampproject.org/amp4ads-v0.js"></script>
        <script async='async' src='https://www.googletagservices.com/tag/js/gpt.js'></script>
        <script>
            var googletag = googletag || {};
            googletag.cmd = googletag.cmd || [];
        </script>
        <script>
            googletag.cmd.push(function() {
                googletag.defineSlot('/7847748/BHUNITEC', [1024, 768], 'div-gpt-ad-1548348453647-0').addService(googletag.pubads());
                googletag.pubads().enableSingleRequest();
                googletag.enableServices();
            });
        </script>
        <style amp-custom="">.banner{width:100vw;height:100vh}</style>
    </head>
    <body>
         <!-- /7847748/AMP-prueba -->
        <div class="banner" id='div-gpt-ad-1548348453647-0'>
            <script>
                googletag.cmd.push(function() { googletag.display('div-gpt-ad-1548348453647-0'); });
            </script>
        </div>
    </body>
    <span id="start-page"></span>
</html>