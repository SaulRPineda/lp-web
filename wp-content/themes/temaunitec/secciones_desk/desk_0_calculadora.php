<!-- Implementacion de la nueva calculadora -->
<div id="aplicativo-calculadora" class="contenedor-principal-calculadora" style="display:none;">
    <div id="overlay" >
        <div id="text">
            <p>Para mostrarte el porcentaje de beca y costos de colegiatura, por favor selecciona los campos:</p>
            <div class="bullets mt-2">
                <p class="bullet" id="calc-apply-career"><span>•</span> Carrera</p>
                <p class="bullet" id="calc-apply-padre"><span>•</span> ¿Eres padre o tutor?</p>
                <p class="bullet" id="calc-apply-campus"><span>•</span> Campus</p>
                <p class="bullet" id="calc-apply-type"><span>•</span> Modalidad</p>
            </div>
        </div>
    </div>

    <div class="main main-calculadora-todo">
        <div id="selector-float">
            <div class="tc overlay-controls d-flex d-none">
            </div>
            <div class="tc titlecont d-flex">
                <div class="titulo-calculadora d-flex align-items-center mx-auto">
                    <h3 class="title-inside">Calcula tu Beca</h3>
                </div>
                <div class="titulo-calculadora">
                    <div class="steps-calc d-flex">
                        <!-- <p class="pasos">Paso <span class="steps">3</span> de 6</p> -->
                        <button id="cierra-tu-calculadora" class="close-x d-flex justify-content-center" onclick="cerrarModalCalculadora()">X</button>
                    </div>
                </div>
            </div>

            <!-- <div id="coming-back" class="btn-group d-none" style="display: block; padding-top: 2vh;">
                <div class="card card-adapted w-20">
                    <div class="card-body tc w-100">
                        <h5 class="mt-0">⮨ Regresar</h5>
                    </div>
                </div>
            </div> -->
            <div class="breadcrumbs w-100">
                <ul class="d-flex justify-content-center align-items-center">
                    <li id="bread1" class="bread-name d-flex justify-content-center align-items-center breadActive">Nivel Académico <i class="icon-u icon-u-ver-derecha p-0">></i> </li>
                    <li id="bread2" class="bread-name d-flex justify-content-center align-items-center">Área de Interés <i class="icon-u icon-u-ver-derecha p-0">></i> </li>
                    <li id="bread3" class="bread-name d-flex justify-content-center align-items-center">Promedio <i class="icon-u icon-u-ver-derecha p-0 d-none">></i> </li>
                </ul>
            </div>


            <div class="containerFirst cont-quest d-flex align-items-center justify-content-center">
                <div class="controls d-none">
                    <a class="btn-floating waves-effect waves-lights">
                        <div class="back"><i class="left"></i></div>
                    </a>
                </div>
                <div class="quest w-80 p-activa" id="business-line">
                    <div class="subtitulo">¿Qué grado de estudio te interesa estudiar?</div>   
                    <!-- <div class="desc-subtitle hide-on-res">Selecciona el nivel de estudios que te interesa estudiar en la UNITEC</div> -->
                    <div id="render_lineas_negocio" class="btn-group"></div>
                </div>
            
                <div class="adjust-container quest w-80 hide" id="container_carrera_modalidad">
                    <div class="w-100" id="carrera">
                        <div class="subtitulo" id="subtituloInteres"></div>
                        <div class="btn-group">
                            <div class="tc w-100 d-flex">
                                <input class="ipt-b" placeholder="Buscar..." id="project">
                                <div class="abajo d-flex align-items-center justify-content-center"><i class="ver-abajo"></i></div>
                                <input type="hidden" id="project-id">
                                <!-- <button class="arrow">&#9660;</button> -->
                            </div>
                        </div>
                        <div>
                            <p style="display: none" id="project-description"></p>
                        </div>
                    </div>

                    <div class="w-100 hide" id="padre_tutor">
                        <div class="subtitulo">¿Eres padre o tutor?</div>
                        <div class="btn-group">
                            <div class="sel w-100">
                                <div class="custom-select w-100 d-flex">
                                    <select class="ipt-b" id="render_padre_tutor">
                                        <option>Selecciona</option>
                                        <option>Si</option>
                                        <option>No</option>
                                    </select>
                                </div>
                                <div class="abajo d-flex align-items-center justify-content-center"><i class="ver-abajo"></i></div>
                            </div>
                        </div>
                        <div>
                            <p style="display: none" id="project-description"></p>
                        </div>
                    </div>
                
                    <div class="w-100" id="modalidad" style="padding-top: 5vh;">
                        <div class="subtitulo">¿Qué modalidad y campus te interesan?</div>
                        <div class="desc-subtitle hide-on-res">Las opciones disponibles se muestran de acuerdo al programa académico elegido</div> <!-- Selecciona la modalidad y el campus para ver la colegiatura correspondiente -->
                        <div class="btn-group">
                            <div class="sel mr">
                                <div class="custom-select w-100 d-flex">
                                    <select class="ipt-b" id="render_modalidad"><option>Modalidad</option></select>
                                </div>
                                <div class="abajo d-flex align-items-center justify-content-center"><i class="ver-abajo"></i></div>
                            </div>        
                            <div class="sel">
                                <div class="custom-select w-100 d-flex">
                                    <select  class="ipt-b" id="render_campus"><option>Campus</option></select>
                                </div>
                                <div class="abajo d-flex align-items-center justify-content-center"><i class="ver-abajo"></i></div>            
                                <div id="reset" class="reset"><div class="flecha-reset">✏</div></div> <!-- <i class="icono-reset"⇨></i> -->
                            </div>
                        </div>

                        <div id="alert-prepaunam" style='margin: 1vh 0 2vh; font-family: "Roboto Light",sans-serif; text-align: center; font-size: 2vh; font-weight: 400;' class="d-none">
                            <div class="alert alert-light" role="alert">
                              *Iniciamos Clases en Agosto de 2020
                            </div>
                        </div>

                    </div>
                    <div class="center">
                        <button id="btn-verCol-avanzar" class="btn btn-or" onclick="avanzaPaso()">Siguiente</button>
                    </div> 
                </div>
                

                <div class="quest w-80 hide" id="promedio">
                    <div class="subtitulo">Compártenos el promedio que obtuviste en tu <span class="rs-canterior"></span>:</div>
                    <div class="btn-group sliderCalc">
                        <div class="output">8</div>
                        <div class="w-100" style="margin: 2vw 0 0vw 0;">
                            <input type="range"  id="range" name="distance" class=" " min="6" max="10" step=".1" value="8" list="range_list">
                            <datalist id="range_list">                                                                  
                                <option>6</option>                                            
                                <option>7</option>                                            
                                <option>8</option>                                            
                                <option>9</option>                                            
                                <option>10</option>                                            
                            </datalist>        
                        </div>
                        <div class="center">
                            <button id="btn-verCol" class="btn btn-or" onclick="finalizaBoton()">Ver Colegiatura</button>
                        </div>            
                    </div>
                </div>
                
                <div class="infoMaterias quest w-80 hide questB">
                    <div class="w-50 tc disponible_materias">
                        <div class="subtitulo bloque_disponible">Disponible para terminar en:</div>    
                        <div class="aniosduracion mt-1"></div>

                        <!--PREPA VESPERTINA-->
                        <div class="prepavesp-term d-none politicas">
                            <p class="">* Dependiendo de las materias a revalidar</p>                 
                        </div>

                    </div>

                    <div class="w-50 tc hide bloque_numero_materias" id="bloqueMaterias">
                        <div class="subtitulo numero_materias">Numero de materias a cursar:</div>
                        <div class="mat-circ mt-1"></div>
                    </div>
                </div>
                
                <div class="infoMaterias quest w-80 hide questB">
                    <div class="d-flex align-items-center materias ml-0">
                        <div class="subtitulo">Mi forma de pago es:</div>
                    </div>
                    <div class="btns tc materias">
                        <button  class="bl btn-dual-y selected  rs-btn-mensual" >Mensual</button>
                        <button title="Button" class="bl btn-dual-y rs-btn-contado" >Contado</button>
                    </div>
                </div>
            </div>
        </div>
        <a class="btn-show-controls" onclick="expandControls()"><div class="more-controls" data-expand="0" style="display: none;"><i class="plus-controls"></i></div></a>
        <div id="results" class="scrollbar-calculadora">
            <button id="cierra-tu-calculadora-2" class="close-x-last d-flex justify-content-center" onclick="cerrarModalCalculadora()">X</button>
                
            <div id="first">
                <div class="first-text" style="width:100%">
                    <h3 class="titulo rs-carrera"></h3>
                    <div class="sub-carrera">
                        <span class="rs-mod"></span>
                        <span class="rs-line">&nbsp;&nbsp;|&nbsp;&nbsp;</span>
                        <span class="rs-campus"></span>
                    </div>
                    <div id="alertext-prepaunam" class="d-none" style='display: flex; justify-content: flex-start; font-size: 2.5vh; font-family: "Roboto Light",sans-serif; font-weight: 400;'>
                        <div role="alert">
                          Iniciamos Clases en Agosto de 2020
                        </div>
                    </div>
                </div>
               
                <div id="video-beca" class=" ">

                    <div id="calcu-left" class=" ">
                        <div class="imagen">
                            <video id="video_background" class="PromotionGlobalLarge-videoBackgroundWrapper" preload="auto" loop="loop" muted="muted" volume="0" autoplay="true"><source src="" type="video/mp4"> Video not supported </video>
                        </div>
                    </div>
                    <div id="calcu-right" class=" ">
                        <div class="calc-becas blink">
                            <div class="resumen r-beca">
                                <h2 class="resumen-sub">Beca Académica</h2>
                                <span class="rs-beca"></span>
                            </div>
                            <div class="resumen">
                                <h2 class="d-none resumen-sub" id="pagosmensuales-prepa-unam">11 Pagos Mensuales de</h2>
                                <h2 class="resumen-sub" id="pagoNormal">Pago <span class="rs-tipopago" id="tipoPago">Mensual</span></h2>
                                <span class="rs-pg-final"></span>
                            </div>
                            <div class="resumen r-ahorro" id="r-ahorro">
                                <h2 class="resumen-sub" id="ahorro-prepa-unam">Ahorro Total</h2>
                                <span class="rs-ahorro-mns"></span>
                            </div>

                            <!--PREPA UNAM-->
                            <!-- <div class="resumen r-ahorro d-none" id="prepa-unam">
                                <h2 class="resumen-sub">Inscripci&oacute;n Anual</h2>
                                <span class="rs-pg-final">$3,950</span>
                            </div> -->
                        </div>
                    </div>
                </div>

                <div class="politicas" style="width:100%">
                    <p>*Costos vigentes para el periodo 31 de Diciembre 2019</p>
                    <p><span class="rs-becamasp"> </span></p>
                    <p class="term-beca">*El porcentaje de beca se asignará conforme al promedio general del nivel de estudios anterior al que se inscriba</p>
                </div>

                <!-- <div class="copy tc">
                    <div class="copy-line d-flex align-items-center">
                        <span class="btn-copy"><a class="btn btn-or">Solicita tu beca</a></span> 
                        <span class="btn-copy">Sin costo de Inscripción / Reinscripción</span>
                    </div>
                </div> -->
            </div>

            <a class="btn-parpadeante" href="#first"><div class="more"><i class="switch-arrow down-window"></i></div></a>
            <!-- <a class="btn-show-controls" onclick="expandControls()"><div class="more-controls" data-expand="0"><i class="switch-arrow down-window"></i></div></a> -->

            <div id="resumen">
                <!-- <div class="politicas">
                    <p>*Aplican cambios y condiciones sin previo aviso<br>*El porcentaje de beca se asignará conforme al promedio general del nivel de estudios anterior al que se uinscriba</p>
                </div> -->

                <div class="d-flex justify-content-between">
                    <div class="w-50 copy">
                        <p class="copy-sub mb-1">Además incluye:</p>
                        <ul class="check" id="listaBeneficios">
                            <li>Laboratorios</li>
                            <li>Materiales</li>
                            <li>Prácticas</li>
                            <li>Uniformes</li>
                            <li>Libros</li>
                        </ul>
                    </div>
                    <div class="w-40 copy tc">
                        <p class="copy-plan mb-2 tc">Descubre más posibilidades</p>
                        <br>
                        <div class="btns-calc">
                            <button type="button" onclick="javascript:window.location.href='/admision-unitec/'" class="btn btn-or btn-proceso">CONOCE EL PROCESO DE ADMISIÓN</button>
                        </div>
                        <br>
                        <div class="btns-clc">
                            <a class="btn button-whatsapp ml2 mr2" href="https://api.whatsapp.com/send?phone=525529688928&text=Hola%2C%20estoy%20en%20la%20página%20de%20internet%20y%20quiero%20más%20información" data-gtm-tr="evWhats" data-gtm-accion="intención" data-gtm-seccion="calculadora" data-gtm-depto="" target="_blank">
                                <!-- <i class="icon-u icon-u-whatsapp"></i>&nbsp;&nbsp;Resuelve tus dudas -->
                                <svg style="width:20px;height:20px;" version="1.1" x="0px" y="0px" viewBox="0 0 512 512" style="enable-background:new 0 0 512 512;" xml:space="preserve" >
                                    <g><path d="M256.064,0h-0.128C114.784,0,0,114.816,0,256c0,56,18.048,107.904,48.736,150.048l-31.904,95.104l98.4-31.456 C155.712,496.512,204,512,256.064,512C397.216,512,512,397.152,512,256S397.216,0,256.064,0z M405.024,361.504 c-6.176,17.44-30.688,31.904-50.24,36.128c-13.376,2.848-30.848,5.12-89.664-19.264C189.888,347.2,141.44,270.752,137.664,265.792 c-3.616-4.96-30.4-40.48-30.4-77.216s18.656-54.624,26.176-62.304c6.176-6.304,16.384-9.184,26.176-9.184 c3.168,0,6.016,0.16,8.576,0.288c7.52,0.32,11.296,0.768,16.256,12.64c6.176,14.88,21.216,51.616,23.008,55.392 c1.824,3.776,3.648,8.896,1.088,13.856c-2.4,5.12-4.512,7.392-8.288,11.744c-3.776,4.352-7.36,7.68-11.136,12.352 c-3.456,4.064-7.36,8.416-3.008,15.936c4.352,7.36,19.392,31.904,41.536,51.616c28.576,25.44,51.744,33.568,60.032,37.024 c6.176,2.56,13.536,1.952,18.048-2.848c5.728-6.176,12.8-16.416,20-26.496c5.12-7.232,11.584-8.128,18.368-5.568 c6.912,2.4,43.488,20.48,51.008,24.224c7.52,3.776,12.48,5.568,14.304,8.736C411.2,329.152,411.2,344.032,405.024,361.504z" fill="#FFFFFF"/></g>
                                </svg>&nbsp;&nbsp;Resuelve tus dudas
                            </a>
                        </div>
                    </div>
                    <!-- <div class="w-40 copy tc">
                        <p class="copy-plan mb-1">Conoce el plan de estudios, así como todos los requisitos de inscripción.</p>
                        <a class="btn btn-descarga">DESCARGA AQUÍ</a>
                    </div>                     -->
                </div>

                <!-- <div class="tc mt-2">
                    <button onclick="javascript:window.location.href='/admision-unitec/'" class="btn btn-or">INICIAR PROCESO DE ADMISIÓN</button>
                </div> -->
            </div>

        </div>
    </div>
</div>