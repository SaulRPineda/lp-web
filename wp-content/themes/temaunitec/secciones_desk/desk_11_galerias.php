<?php
/**
 * WP Post Template: desk_11_galerias
 */ 
?>
<?php
/*Se insertan los plugins que necesita la seccion tal y como se llama el archivo*/
/*load_script( $pluginsNecesarios = array('masonry') );*/
/*End Se insertan los plugins que necesita la seccion tal y como se llama el archivo*/

$galerias = get_field('categoria_de_galeria');
$json_galery = NULL;
$portadas_galeria = NULL; 

$json_galery = '{';
foreach ($galerias as $key => $galeria) {
    $galeria_seleccionada = get_field('fotografias', $galeria->ID);
    $json_galery .= '"'.$galeria->post_name.'":[';

    foreach ($galeria_seleccionada as $key => $imagenes) {
        // echo $imagenes['caption'];
        $json_galery .= '{"title":"'.$galeria->post_name.'", "url":"'.$imagenes['url'].'", "alt" : "'.$imagenes['alt'].'", "caption": "'.str_replace(array("\r\n", "\r", "\n"), "", $imagenes['caption'] ).'", "description" : "'.str_replace(array("\r\n", "\r", "\n"), "", $imagenes['description'] ).'", "urlthumbnail": "'.$imagenes['sizes']['thumbnail'].'"},';

        /*Asignamos la segunda Imagen como portada para version Desktop*/
        if($key == 1) {
            $portadas_galeria []= $imagenes['url'];
        }
        /*End Asignamos la segunda Imagen como portada para version Desktop*/
        
    }
    $json_galery = substr($json_galery, 0, strlen($json_galery) - 1);
    $json_galery .= '],';
}
$json_galery = substr($json_galery, 0, strlen($json_galery) - 1);
$json_galery .= '}'; 


// $json_galery = preg_replace('/[\n]/', '<br />', $json_galery);
// $json_galery = preg_replace('/[\r]/', '', $json_galery)

?>

<?php if( !empty( $galeria_seleccionada ) || $galeria_seleccionada != NULL || count( $galeria_seleccionada ) > 0 ) { ?>

<script>
    var galery_json = '<?php echo $json_galery; ?>';
</script>


<input type="hidden" id="galeria-activa" name="galeria-activa" value="<?php echo $galerias[0]->post_name; ?>">
<input type="hidden" id="galeria-name" name="galeria-name" value="<?php echo $galerias[0]->post_title; ?>">

<div id="galeria-campus"></div>
<section class="container-fluid contenedor-galeria wow fadeIn d-flex justify-content-center align-items-center">
    <div class="col-lg-11 justify-content-center d-flex align-items-center"> <!-- Contenedor de la galería -->
        <div class="centrar-contenido col-12">
            <div class="row"> <!-- Contenedor de encabezado de la sección -->
                <article class="col-12 text-center d-flex justify-content-center" data-wow-offset="50">
                    <h4 class="w-75 titleSection"><?php echo get_field('titulo_card_galeria'); ?></h4>
                </article>
                <article class="prodDescription col-12 text-center mb-2 d-flex justify-content-center" data-wow-offset="50">
                        <h6 class="w-100 descriptionSection mt-1"><?php echo do_shortcode( get_field('desk_descripcionGalerias') ); ?></h6>
                </article>
            </div> <!-- Fin del contenedor de encabezado de la sección -->

            <div class="row">
                <div class="col-md-12">
                    <!--Section: Products v.4-->
                    <section class="text-center fila-galeria">
                        <!--Grid row-->
                        <div class="row">
                            <div class="carrousel w-100" data-pos="0">
                                <div class="carrousel_inner">
                                    <ul class="d-flex">
                                      <?php 
                                        foreach ($galerias as $key => $galeria) { ?>
                                        <!--Grid column-->
                                        <li class="col-lg-4 col-md-4 item-<?php echo $key;  ?>" id="<?php echo $galeria->post_name; ?>" name="<?php echo $galeria->post_name; ?>" title="<?php echo $galeria->post_title; ?>">
                                          <!--Collection card-->
                                          <div class="card collection-card z-depth-1-half">
                                              <!--Card image-->
                                              <a onclick="abreGaleria()" data-gtm-tr="OurFac" data-gtm-facilidad="<?php echo $galeria->post_title; ?>" data-gtm-interaccion="Fotogaler&iacute;a">
                                                  <div class="view zoom portada-galeria" id="<?php echo $galeria->post_name; ?>" name="<?php echo $galeria->post_name; ?>" title="<?php echo $galeria->post_title; ?>">
                                                      <img data-src="<?php echo $portadas_galeria[$key]; ?>" alt="portada galeria <?php echo $galeria->post_title; ?>" class="img-fluid centrar flex-custom w-100 lazy">
                                                      <!-- <?php echo do_shortcode( '[icono nombre="'.sanitize_title("hospital-simulado").'"][/icono]' ); ?> -->
                                                      <div class="stripe dark">
                                                          <p class="title-galerias galeria-title-<?php echo $galeria->post_name; ?>"><?php echo $galeria->post_title; ?></p>
                                                      </div>
                                                  </div>
                                              </a>
                                              <!--Card image-->
                                          </div>
                                          <!--Collection card-->
                                        </li>
                                        <?php }
                                        ?>
                                        <!--Grid column-->
                                    </ul>
                                </div>
                           </div>
                        </div>
                        <!--Grid row-->
                    </section>
                    <!--Section: Products v.4-->

                </div>
            </div>    
        </div>                        
    </div> <!-- Fin del contenedor de galería -->

    <!--Controls-->
    <?php 
        $none = ( count($galerias) > 3 )? "": "d-none";
    ?>
    <div class="controls-gallery">
        <a class="btn-floating waves-effect waves-light btn-arrow carrousel_right <?php echo $none; ?>" data-slide="prev" title="Siguiente">
            <!-- <i class="fa fa-chevron-left"></i> -->
            <?php echo do_shortcode( '[icono nombre="'.sanitize_title("ver-derecha").'" clase="arrow-left"][/icono]' ); ?>
        </a>

        <a class="btn-floating waves-effect waves-light btn-arrow carrousel_left d-none" data-slide="next" title="Anterior">
            <!-- <i class="fa fa-chevron-right"></i> -->
            <?php echo do_shortcode( '[icono nombre="'.sanitize_title("ver-izquierda").'" clase="arrow-right"][/icono]' ); ?>
        </a>
    </div>
    <!--Controls-->
</section>

<!-- Modal -->    
<div class="modal fade modal-tarjeta carousel-fade" id="modal-tarjeta-galeria-carrusel" role="dialog" aria-hidden="true" data-backdrop="static" data-keyboard="false">
    <div class="modal-dialog modal-fluid" role="document">
        <div class="modal-content">
            <div class="icons" onclick="cierraGaleria()">
                <a href="#galeria-desk" data-dismiss="modal" title="Cerrar">
                <?php echo do_shortcode( '[icono nombre="'.sanitize_title("cerrar").'"][/icono]' ); ?>
                </a>
            </div>
            
            <div class="modal-content-tarjeta">
                <!-- carousel-thumbnails -->

                <!--Carousel Wrapper-->
                <div id="carousel-tarjeta" class="carousel slide carousel-multi-item" data-ride="carousel-tarjeta">

                    <!--Slides-->
                    <div class="carousel-inner bsn" role="listbox" id="slides-carusel"></div>
                    <!--/.Slides-->                                    
                </div>
                <!--Carousel Wrapper-->

                <!--Controls-->
                <div class="controls-top">
                    <a class="btn-floating waves-effect waves-light btn-arrow" href="#carousel-tarjeta" data-slide="prev" title="Anterior">
                        <?php echo do_shortcode( '[icono nombre="'.sanitize_title("ver-izquierda").'" clase="arrow-right"][/icono]' ); ?>
                    </a>

                    <a class="btn-floating waves-effect waves-light btn-arrow" href="#carousel-tarjeta" data-slide="next" title="Siguiente">
                        <?php echo do_shortcode( '[icono nombre="'.sanitize_title("ver-derecha").'" clase="arrow-left"][/icono]' ); ?>
                    </a>
                </div>
                <!--Controls-->

            </div>
        </div>
    </div>
</div>
<!-- Final del Modal -->

<?php } ?>