<div id="procesos_paso_paso"></div>
<?php
/**
 * WP Post Template: desk_3_procesos_paso_a_paso
 */
?>
<!--Seccion prestigio-->
<!-- Nav tabs -->

<?php
    $tabs = get_field('mis_tabs', $theID);
    $imagen = get_field('desk_imagenIzquierda', $theID);
    $active = NULL;
    $columna = "col-6";
?>
<?php if( is_array($tabs) ) { ?>
<section class="col-12 wow fadeIn content-gral-procesos"><!-- /.Main <layout-->
    <div class="row">
        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-6 no-padding cont-img lazy" data-src="<?php echo $imagen['url']; ?>" alt="<?php echo $imagen['alt']; ?>"></div>
        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-6 pl-4 pr-4">
            <section id="elegant-card"><!-- SECTION-->
                <div class="row"><!-- Live preview-->
                    <div class="col-md-12"><!-- col-md-12 -->
                        <section id="tabNav" class="pt-1">
                            <!-- Tab Navegacion -->
                            <div  class="tabs-wrapper"> 
                                <ul class="nav classic-tabs" role="tablist">
                                    <?php foreach ($tabs as $key => $contentTabs) { ?>
                                        <?php if ($key == 0) { $active = "active text-black"; } ?>
                                            <?php foreach ($contentTabs as $itemTabs) { ?>
                                                <?php if ( is_array($itemTabs) ) { ?>
                                                    <?php  foreach ($itemTabs as $data) { ?>
                                                        <li class="nav-item-2 nav-item">
                                                            <a class="waves-light <?php echo $active ?>" data-toggle="tab" href="#tab-<?php echo $data['acf_fc_layout'].$key; ?>" role="tab" title="<?php echo $contentTabs['nombre_tab']; ?>" data-gtm-tr="Tab" data-gtm-tab="<?php echo $contentTabs['nombre_tab']; ?>"><?php echo $contentTabs['nombre_tab']; ?></a>
                                                        </li>
                                                    <?php } ?>
                                                <?php } ?>
                                            <?php } ?>
                                        <?php $active = NULL; ?>
                                    <?php } ?>
                                </ul>
                            </div>                    
                            <?php //echo do_shortcode( '[icono nombre="'.sanitize_title("ver-derecha").'" clase="rightTabNav direccionales" estilo="style=\'display:none\'"][/icono]' ); ?>
                        </section>  

                        <!-- Tab panels -->
                        <div class="tab-content lighten-4 pl-3 pr-3">
                            <!--Panel 1-->
                            <?php
                            foreach ($tabs as $foo => $contentTabs) {
                                if($foo == 0) { $active = "active"; }
                                foreach ($contentTabs as $itemTabs) {
                                    if ( is_array($itemTabs) ) {
                                        foreach ($itemTabs as $key => $data) {
                                            if ($key == 0) {
                                        ?>
                                                <div class="tab-pane fade in show <?php echo $active; ?>" id="tab-<?php echo $data['acf_fc_layout'].$foo; ?>" role="tabpanel">
                                            
                                        <?php
                                            }
                                           //validar Tipo de Tab
                                           switch ($data['acf_fc_layout']) {
                                               case 'prestigio': ?>
                                                <!--Panel 1-->
                                                    <div class="card">
                                                        <ul class="list-group">
                                                            <?php foreach ($data['prestigio_item'] as $prestigio) { ?>
                                                                    <div class="simple-list">
                                                                        <div class="list-item-container">
                                  
                                                                            <?php
                                                                                switch ($prestigio['link']) {
                                                                                    case NULL:
                                                                                        $linkInicio = "<a>";
                                                                                        $linkFin = "</a>";
                                                                                    break;
                                                                                    
                                                                                    default:
                                                                                        $target = ( strpos( strtolower($prestigio['link']), "unitec.mx" ) ) ? "" : "target='_blank'";

                                                                                        $linkInicio = "<a href='".$prestigio['link']."' ".$target." >";
                                                                                        $linkFin = "</a>";
                                                                                    break;
                                                                                }                        
                                                                            ?>


                                                                            <div class="item-icon"><?php echo $linkInicio; ?>
                                                                            <?php echo do_shortcode( '[icono nombre="'.sanitize_title( $prestigio['icono'] ).'" clase="link-simple-list"][/icono]' ).$linkFin; ?>  
                                                                            </div>
                                                                            <div class="item-description">         

                                                                                <p><?php echo $linkInicio; ?><b> <?php echo $prestigio['texto_destacado']; ?></b>&nbsp;<label for="texto-secundario" class="fix-texto-secundario"><?php echo $prestigio['texto_secundario'].$linkFin;?></label>
                                                                              <?php $linkInicio = NULL;
                                                                                    $linkFin = NULL;
                                                                                ?>
                                                                                </p>

                                                                            </div>

                                                                        </div>
                                                                    </div>
                                                            <?php } ?>
                                                        </ul>
                                                    </div>
                                                </div> 
                                                <!-- termina tab de Prestigio-->
                                                <?php
                                                break;
                                               
                                                default:
                                                ?>
                                                <!--Inicia tab de Admisión -->
                                                <!-- Stepers Wrapper -->

                                                <ul  class="stepper stepper-vertical">
                                                <?php foreach ($data['admision_item'] as $key => $admision) { ?>
                                                <?php if($key == 0) { $active = "active"; $miStep = "miStep"; } ?>
                                                    <li class="<?php echo $active; ?>">
                                                        <a href="#!" title="<?php echo $admision['titulo']; ?>" data-gtm-tr="AdmissionProc" data-gtm-proceso="<?php echo $admision['titulo']; ?>" data-gtm-paso="<?php echo $key + 1; ?>">
                                                            <span class="circle"><?php echo $key + 1; ?></span>
                                                            <span class="label titulo-stepper"><?php echo $admision['titulo']; ?></span>
                                                        </a>

                                                        <div id="<?php echo $miStep; ?>" class="content-procesos step-content animated fadeIn">

                                                        <?php 
                                                            switch ($admision['acf_fc_layout']) {
                                                                case 'contenido_step': echo do_shortcode($admision['contenido']); break;
                                                                case 'video_step': ?> 
                                                                <p class="text-left"><?php echo do_shortcode( $admision['contenido_you_tube'] ); ?></p>
                                                                <div id="admision-<?php echo $key; ?>" class="wrapper youtube" data-embed="<?php print($admision['video']); ?>"><div class="play-button"></div></div>
                                                                <!-- <iframe src="<?php print($admision['video']); ?>" frameborder="0" allowfullscreen style="position:relative; width:100%"></iframe> -->
                                                        <?php   break;

                                                                case 'video_wistia':
                                                        ?>
                                                                    <p class="text-left"><?php echo do_shortcode( $admision['contenido_wistia'] ); ?></p>
                                                        <?php
                                                                    echo $admision['video_wistia']; 
                                                                break;

                                                                default: break;
                                                            }
                                                        ?>


                                                            <div class="row botones-coninuar">
                                                                <?php if( ($key+1) !== count($data['admision_item']) ) { $columna = "col-6";?>
                                                                <!-- <a class="w-50" data-gtm-tr="AdmissionProc" data-gtm-proceso="<?php echo $admision['titulo']; ?>" data-gtm-paso="<?php echo $key + 1; ?>"> -->
                                                                    <input type="button" class="nextSeccion d-flex justify-content-center col-6" value="<?php echo $admision['cita_boton']; ?>" data-gtm-tr="AdmissionProc" data-gtm-proceso="<?php echo $admision['titulo']; ?>" data-gtm-paso="<?php echo $key + 1; ?>">
                                                                <!-- </a> -->
                                                                <?php } ?>

                                                                <div class="vertical-centered-text d-flex align-items-center <?php echo $columna; ?>">
                                                                    <h6 class="asesor">
                                                                        <?php if (strpos($admision['call_to_action'], 'javascript') !== false) { ?>
                                                                        <a onclick="<?php echo $admision['call_to_action']; ?>" class="verMas" title="<?php echo $admision['cita_link'] ; ?>" data-solicitud-location="Middle">
                                                                           <?php echo $admision['cita_link'] ; ?>
                                                                        </a>
                                                                        <?php } else { ?>
                                                                        <a href="<?php echo $admision['call_to_action']; ?>" class="verMas" title="<?php echo $admision['cita_link'] ; ?>">
                                                                           <?php echo $admision['cita_link'] ; ?>
                                                                        </a>
                                                                        <?php }?>
                                                                    </h6>
                                                                </div>
                                                            </div>

                                                        </div>
                                                    </li>
                                                    <?php $active = NULL; $miStep = NULL; $columna = "col-12"; ?>                      
                                                <?php } ?>
                                                    </ul>
                                                    <!-- /.Stepers Wrapper -->
                                                </div>                                        
                                                <!--/.Panel 2-->
                                                <?php                     
                                                break;
                                            }
                                
                                        }
                                    }
                                }
                                $active = NULL;
                            }
                            ?>                                          
                            <!--/.Panel 2-->
                        </div>
                    </div><!-- col-md-12 -->
                </div><!-- /.Live preview-->
            </section><!-- SECTION-->
            
        </div>
    </div>
</section><!-- /.Main <layout-->
<?php } ?>

