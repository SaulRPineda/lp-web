<?php
/**
 * WP Post Template: desk_26_galeria_descuentos
 */
?>
<?php
/*Se insertan los plugins que necesita la seccion tal y como se llama el archivo*/
/*load_script( $pluginsNecesarios = array('masonry') );*/
/*End Se insertan los plugins que necesita la seccion tal y como se llama el archivo*/

$args = array(
    'post_type'             => 'descuentos-unitec',
    'post_status'           => 'publish',
    'ignore_sticky_posts'   => 1,
    'posts_per_page'        => -1
);
$descuentos = query_posts($args);
$actualDiscount = 0;
$categoryDiscount = [];
$categoryDiscountCampus = [];
$categoryDiscountContactData = [];
$categoryDiscountLinkData = [];
$field_key = NULL;
$categoryDiscounts = [];
$json_descuentos = NULL;
$array_campus = [];

// print_r (get_fields());

$json_descuentos = '{';
//   print_r($descuentos);
foreach ($descuentos as $key => $descuento) {
    $json_descuentos .= '"'.$descuento->post_name.'":';

    $telefono = get_field('datos_de_contacto',$descuento->ID);


    $discount_categoria = get_field('categoria',$descuento->ID);
    $discount_campus = get_field('campus',$descuento->ID);
    $discount_logo = get_field('logotipo_de_la_empresa',$descuento->ID);
    $discount_contacto = get_field('datos_de_contacto',$descuento->ID);
    $discount_links = get_field('links_de_contacto_descuentos',$descuento->ID);
    // $discount_caracteristica = get_field('caracteristica_del_descuento',$descuento->ID);
    $discount_terminos = get_field('terminos_condiciones',$descuento->ID);
    $discount_qr_link = get_field('qr_code_descuento',$descuento->ID);

    $discount_terminos = str_replace('"', '\\\\"', $discount_terminos); 
    $discount_terminos = str_replace("'", "\\'", $discount_terminos); 
    //$discount_terminos = preg_replace('"', '\"', $discount_terminos);

    $json_descuentos .= '{"title":"'.addslashes(trim($descuento->post_title)).'", "url":"'.trim($discount_logo).'", "categoria": "'.trim($discount_categoria).'", "terminos_condiciones": "'.trim($discount_terminos).'", "contacto": {';

    $array_campus[$descuento->post_name] =[];
    foreach ($discount_campus as $key => $campus) {
        $array_campus[$descuento->post_name].array_push($array_campus[$descuento->post_name], $campus);
        // $json_descuentos .= '"'.$key.'": ' . '';
    }

    foreach ($discount_contacto as $key => $datos_array) {
        $json_descuentos .= '"'.$datos_array['medio_de_contacto'].'": ' . '"'.$datos_array['datos'].'",';
    }

    if(count($discount_contacto)> 0){
        if(mb_substr($json_descuentos, -1) != "{") {
            $json_descuentos = substr($json_descuentos, 0, strlen($json_descuentos) - 1);
        }
    }
    $json_descuentos .= '}, "links": {';

    foreach ($discount_links as $key => $datos_array) {
        $json_descuentos .= '"'.$datos_array['link_de_contacto'].'": ' . '["'.$datos_array['texto_link'].'","'.$datos_array['link_contact'].'"],';
    }

    if(count($discount_links)> 0){
        if(mb_substr($json_descuentos, -1) != "{") {
            $json_descuentos = substr($json_descuentos, 0, strlen($json_descuentos) - 1);
        }
    }
    $json_descuentos .= '},';

    $json_descuentos .= '"qr-code-descuento":"'.$discount_qr_link.'"';
    $json_descuentos .= '},';


    if($actualDiscount == 0) {
        $field_key = get_post_meta( $descuento->ID, '_categoria', true);
        $field = get_field_object($field_key);
        $choices = $field['choices'];
        $categoryDiscount = $choices;
        $categoryDiscount = array("all-tipes" => "Ver todas las Categorías") + $categoryDiscount;

        $field_key = get_post_meta( $descuento->ID, '_campus', true);
        $field = get_field_object($field_key);

        $choices = $field['choices'];
        // print_r($choices);
        $categoryDiscountCampus = $choices;
        $categoryDiscountCampus = array("all-campus" => "Ver todos los Campus") + $categoryDiscountCampus;

        $field_key = get_post_meta( $descuento->ID, '_datos_de_contacto', true);
        $field = get_field_object($field_key);
        $choices = $field['sub_fields'][0]['choices'];
        // echo "<br><br>";
        // print_r($choices);
        $categoryDiscountContactData = $choices;

        $field_key = get_post_meta( $descuento->ID, '_links_de_contacto_descuentos', true);
        $field = get_field_object($field_key);
        $choices = $field['sub_fields'][0]['choices'];
        // echo "<br><br>";
        // print_r($choices);
        $categoryDiscountLinkData = $choices;
    }

    $categoryDiscounts.array_push($categoryDiscounts, $descuento->post_name);

    $actualDiscount++;
}

$json_descuentos = substr($json_descuentos, 0, strlen($json_descuentos) - 1);
$json_descuentos .= '}';

/* Limpia el JSON de caracteres que no son ASCII */
// echo $json_descuentos . '<br>';
// $json_descuentos = preg_replace('/[\x00-\x1F\x80]/', '', $json_descuentos);
$json_descuentos = preg_replace('/[\n]/', '<br />', $json_descuentos);
$json_descuentos = preg_replace('/[\r]/', '', $json_descuentos);
// $json_descuentos = nl2br($json_descuentos);

?>

<script>
    var discounts_json = '<?php echo $json_descuentos; ?>';
    var category_array_json = '<?php echo json_encode($categoryDiscount) ?>';
    var category_contact_array_json = '<?php echo json_encode($categoryDiscountContactData) ?>';
    var category_link_array_json = '<?php echo json_encode($categoryDiscountLinkData) ?>';
</script>

<?php
    // $json_descuentos = json_decode(preg_replace('/[\x00-\x1F\x80-\xFF]/', '', $json_descuentos), true); // Quita las letras con acentos, no usar si es en español
    /* Se usa striplashes para volver los double quotes en un string a la normalidad */
    $json_descuentos = json_decode(stripslashes($json_descuentos), true);
?>

<input type="hidden" id="galeria-activa" name="galeria-activa" value="<?php echo $galerias[0]->post_name; ?>">
<input type="hidden" id="galeria-name" name="galeria-name" value="<?php echo $galerias[0]->post_title; ?>">

<div id="instalaciones"></div>

<section class="container-fluid wow fadeIn d-flex justify-content-center align-items-center descuentos"><!-- /.Main <layout-->
    <div class="col-lg-11 justify-content-center d-flex align-items-center contenedor-descuentos" id="galeria_catalogo"> <!-- Contenedor de la galería -->
        <div class="w-100 centrar-contenido mt-3 mb-3">

            <div class="row"> <!-- Contenedor de encabezado de la sección -->
                <article class="col-12 text-center d-flex justify-content-center" data-wow-offset="50">
                    <h4 class="w-75 titleSection"><?php echo get_field('titulo_descuentos_empresriales'); ?></h4>
                </article>
                <article class="col-12 text-center mb-1 d-flex flex-column justify-content-center" data-wow-offset="50">
                        <h6 class="w-100 descriptionSection"><?php echo do_shortcode( get_field('desk_descripcionDescuentos') ); ?></h6>
                        <h4 class="w-100"><a href="#contacto" class="verMas" title="Ver datos de contacto">Ver datos de contacto</a></h4>
                </article>
            </div> <!-- Fin del contenedor de encabezado de la sección -->

            <div class="row filtros mb-2">
                <div class="col-xl-6 col-md-6 col-sm-12">
                    <div class="dropdown">
                        <div class="drop-view filtro-descuentos d-flex align-items-center justify-content-center p-2" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            <div class="col-11"><h4 id="nameCampusDescuentos" data-descuentos-campus="">Ver todos los Campus</h4></div>
                            <div class="col-1 d-flex align-items-center justify-content-end"><?php echo do_shortcode( '[icono nombre="'.sanitize_title("ver-abajo").'" clase="icon-abajo"][/icono]' ); ?></div>
                        </div>
                        <div class="dropdown-menu dropdown-primary menu-filtro-descuentos">
                            <?php foreach ($categoryDiscountCampus as $key => $campus_discount) { ?>
                                <a class="dropdown-item destacar" onclick="filtroTipoCampus( '<?php echo $key ?>', '<?php echo $campus_discount; ?>' );" title="<?php echo $campus_discount; ?>"><?php echo $campus_discount; ?></a>
                            <?php
                            } ?>
                            <!-- <a class="dropdown-item destacar" onclick="filtroNoAnos( '<?php //echo $key ?>', '<?php //echo $opc_no_anos; ?>' );"><?php //echo $opc_no_anos; ?>PRIMERA OPCIÓN</a>
                            <a class="dropdown-item destacar" onclick="filtroNoAnos( '<?php //echo $key ?>', '<?php //echo $opc_no_anos; ?>' );"><?php //echo $opc_no_anos; ?>SEGUNDA OPCIÓN</a> -->
                        </div>
                    </div>
                </div>

                <div class="col-xl-6 col-md-6 col-sm-12">
                    <div class="dropdown">
                        <div id="" class="drop-view filtro-descuentos d-flex align-items-center justify-content-center p-2 pl-4 pr-4" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            <div class="w-75"><h4 id="nameCategoriasDescuentos" data-descuentos-categorias="">Ver todas las Categorías</h4></div>
                            <div class="w-25 w-25 d-flex align-items-center justify-content-end"><?php echo do_shortcode( '[icono nombre="'.sanitize_title("ver-abajo").'" clase="icon-abajo"][/icono]' ); ?></div>
                        </div>
                        <div class="dropdown-menu dropdown-primary menu-filtro-descuentos">
                            <?php foreach ($categoryDiscount as $key => $category_discount) { ?>
                                <a class="dropdown-item destacar" onclick="filtroTipoCategorias( '<?php echo $key ?>', '<?php echo $category_discount; ?>' );" title="<?php echo $category_discount; ?>"><?php echo $category_discount; ?></a>
                            <?php
                            } ?>
                            <!-- <a class="dropdown-item destacar" onclick="filtroNoAnos( '<?php //echo $key ?>', '<?php //echo $opc_no_anos; ?>' );"><?php //echo $opc_no_anos; ?>PRIMERA OPCIÓN</a>
                            <a class="dropdown-item destacar" onclick="filtroNoAnos( '<?php //echo $key ?>', '<?php //echo $opc_no_anos; ?>' );"><?php //echo $opc_no_anos; ?>SEGUNDA OPCIÓN</a> -->
                        </div>
                    </div>
                </div>
                <!-- <div class="col-xl-3 col-md-3 col-sm-6 offset-xl-9 offset-md-9 offset-sm-3 mt-1 mb-1">
                    <a href="<?php //echo $descarga; ?>" class="descargar d-flex align-items-center float-right" target="_blank">
                        <?php echo do_shortcode( '[icono nombre="'.sanitize_title("descargar").'" clase="descarga-folleto pr-2"][/icono]' ); ?>
                        FOLLETO DE BOLSILLO
                    </a>
                </div> -->
            </div>

            <section class="row fila-descuentos">
                <article class="col-12 text-center d-flex justify-content-center" data-wow-offset="50">
                    <h4 class="w-75 titulo-descuentos" id="available-discounts" hidden>No hay descuentos disponibles</h4>
                </article>
            <?php
                $i = 0;
                foreach ($json_descuentos as $key_descuento => $descuento_array) { ?>

                <!--Grid column-->
                <div class="col-lg-4 pb-1 item-<?php echo $key_descuento; ?> categoria-<?php echo $descuento_array['categoria']; ?> <?php foreach ($array_campus[$key_descuento] as $key_campus => $campus_value) { echo " campus-".$campus_value; }?> campus-all-campus categoria-all-tipes" id="<?php echo $key_descuento; ?>" name="<?php echo $key_descuento; ?>" title="<?php echo $descuento_array['title']; ?>" style="<?php if ($i++ >= 9) //echo "display:none"; ?>">
                    <!--Collection card-->
                    <div class="card collection-card z-depth-1-half">
                        <!--Card image-->
                        <a onclick="abreGaleriaDesc()" title="<?php echo $descuento_array['title']; ?>" data-gtm-tr="OurFac" data-gtm-facilidad="<?php echo $descuento_array['title']; ?>" data-gtm-interaccion="Descuentos">
                            <div class="view zoom portada-galeria-descuentos" id="<?php echo $key_descuento; ?>" name="<?php echo $key_descuento; ?>" title="<?php echo $descuento_array['title']; ?>">
                                <img data-src="<?php echo $descuento_array['url']; ?>" alt="portada galeria <?php echo $descuento_array['title']; ?>" class="img-fluid centrar flex-custom w-100 lazy">
                                <!-- <?php echo do_shortcode( '[icono nombre="'.sanitize_title("hospital-simulado").'"][/icono]' ); ?> -->
                                <div class="stripe dark">
                                    <p class="title-galerias galeria-title-<?php echo $key_descuento; ?>"><?php echo $descuento_array['title']; ?></p>
                                </div>
                            </div>
                        </a>
                        <!--Card image-->
                    </div>
                    <!--Collection card-->
                </div>
            <?php }
            ?>
            </section class="row">

            <!-- Modal -->
            <div class="modal fade modal-tarjeta carousel-fade p-0" id="modal-tarjeta-galeria-carrusel-descuentos" role="dialog" aria-hidden="true" data-backdrop="static" data-keyboard="false">
                <div class="modal-dialog modal-fluid" role="document">
                    <div class="modal-content d-flex align-items-center justify-content-center modal-descuentos">
                        <div class="modal-content-tarjeta p-5">
                            <div class="container contactanos">
                                <!--Card-->
                                <div class="content-gral d-flex justify-content-center">
                                    <div class="col-lg-12 col-xl-6 card-blue p-5">
                                        <!--Card image-->
                                        <div class="overlay">
                                            <div class="card-desc">
                                                <h3 class="tit-desc m-0 modal-titulo-descuentos"></h3>
                                                <h4 class="sub-desc modal-categoria-descuentos"></h4>
                                                <div class="lista-desc mb-2">
                                                    <!-- <ul class="mt-2" id="modal-descripcion-descuentos"> -->
                                                        <!-- <li class="desc-ind d-flex align-items-center"><?php // echo do_shortcode( '[icono nombre="'.sanitize_title("bullet").'" clase="bullet pr-2" ][/icono]' ); ?> 10% de descuento al recibir pagos en efectivo</li>
                                                        <li class="desc-ind d-flex align-items-center"><?php // echo do_shortcode( '[icono nombre="'.sanitize_title("bullet").'" clase="bullet pr-2" ][/icono]' ); ?> 10% de descuento al recibir pagos en efectivo</li>
                                                        <li class="desc-ind d-flex align-items-center"><?php // echo do_shortcode( '[icono nombre="'.sanitize_title("bullet").'" clase="bullet pr-2" ][/icono]' ); ?> 10% de descuento al recibir pagos en efectivo</li> -->
                                                    <!-- </ul> -->
                                                </div>
                                                <span class="legales modal-terminos-descuentos"></span>
                                            </div>
                                        </div>
                                        <!--/Card image-->
                                    </div>
                                    <div class="col-lg-12 col-xl-5 align-self-center card-white p-0">
                                        <!--Card content-->
                                        <div class="card-body card text-md-left card-left p-4">
                                            <div class="icons" onclick="cierraGaleriaDesc()">
                                                <a href="#galeria-desk" data-dismiss="modal" title="Cerrar">
                                                <?php echo do_shortcode( '[icono nombre="'.sanitize_title("cerrar").'"][/icono]' ); ?>
                                                </a>
                                            </div>
                                            <div class="texto-int-card">
                                                <div class="col-12 d-flex justify-content-center">
                                                    <img class="convenio modal-imagen-descuento lazy" data-src="https://www.medicapanamericana.com/imagen/logoShellPoint.ashx?id=250" alt="">
                                                </div>
                                                <div class="contacto-desc">
                                                    <h3 class="card-title-contacto mt-2 mb-1">Contacto</h3>
                                                    <ul class="card-sub-contacto" id="modal-contacto-descuentos">
                                                        <!-- <li>Teléfono: <a href="#"><b>2159-0066</b></a></li>
                                                        <li>Correo: <a href="#"><b>info@casadelibro.com.mx</b></a></li>
                                                        <li>Página <a href="#"><b>web: https://casadelibro.com.mx/</b></a></li>
                                                        <li>QR de descuento: <a href="#"><b><span type="button" class="descargar-descuento">Descargalo aquí</span></b></a></li> -->
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                        <!--/.Card content-->
                                    </div>
                                    <!-- <div class="col-lg-1 d-none d-lg-block phantom"></div> -->
                                </div>
                                <!--/.Card-->
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- Final del Modal -->
        </div>
    </div>
</section>
