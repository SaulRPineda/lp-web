<?php
/*
 * WP Post Template: desk_2_1_banner_estatico
 */
?>
<?php

    /*Obtenemos tipo de Banner BY SRP 28-08-2018*/
    $field = get_field_object('tipo_de_banner');
    $value = $field['value'];
    $label = $field['choices'][ $value ];

    switch ($value) {
        case 'wordpress':
            $banner = "wordpress";
        break;

        case 'dfp':
            $banner = "dfp";
        break;
         
        default:
            $banner = "wordpress";
        break;
    }
    /*End Obtenemos tipo de Banner BY SRP 28-08-2018*/

    $grupoBanner = get_field('desk_group_banner', $theID);
    $grupoBannerContenido = get_field('desk_group_banner_contenido', $theID);

    $deskBannerEstatico = $grupoBanner['desk_banner_estatico'];
    $urlBannerEstatico = $deskBannerEstatico["url"];
    $altBannerEstatico = $deskBannerEstatico["alt"];

    $desk_descripcionBannerEstatico = $grupoBanner['desk_descripcion_banner_estatico'];
    $desk_SubtituloEstatico = $grupoBanner['desk_subtitulo_del_slider'];

    $desk_idPost = get_the_ID(); 
    $desk_postType = get_post( $idPost ); 
    $permalink = get_permalink($idPost);

    $desk_callToActionBannerEstatico = $grupoBannerContenido['desk_call_to_action_banner_estatico'];
    $desk_urlCallToActionBannerEstatico = $grupoBannerContenido['desk_url_call_to_action_banner_estatico'];
    $desk_colorTextoBanner = $grupoBannerContenido['desk_color_del_banner'];

    if ( $banner == "wordpress" ) {
        
        
    ?>

    <script type="text/javascript">
        (function ($) {
    $(document).ready(function () {

        $(document).on('click.card', '.card', function (e) {
            if ($(this).find('.card-reveal').length) {
                if ($(e.target).is($('.card-reveal .card-title')) || $(e.target).is($('.card-reveal .card-title i'))) {
                    // Make Reveal animate down and display none
                    $(this).find('.card-reveal').velocity({
                        translateY: 0
                    }, {
                        duration: 225,
                        queue: false,
                        easing: 'easeInOutQuad',
                        complete: function () {
                            $(this).css({
                                display: 'none'
                            });
                        }
                    });
                } else if ($(e.target).is($('.card .activator')) ||
                    $(e.target).is($('.card .activator i'))) {
                    $(this).find('.card-reveal').css({
                        display: 'block'
                    }).velocity("stop", false).velocity({
                        translateY: '-100%'
                    }, {
                        duration: 300,
                        queue: false,
                        easing: 'easeInOutQuad'
                    });
                }
            }


        });

    });
}(jQuery));

//Social reveal

$('.banner-share > a').on('click', function (e) {
    e.preventDefault() // prevent default action - hash doesn't appear in url
    $(this).parent().find('div').toggleClass('social-reveal-active');
    $(this).toggleClass('share-expanded');
});

    </script>


        <section class="section-banner">
            <div style="background-image: url(<?php echo $urlBannerEstatico; ?>);" class="bann_wind animated fadeIn" alt="<?php echo $altBannerEstatico; ?>">
                <div class="view p-heredada d-flex align-items-center">
                    <div class="mask container d-flex align-items-center">
                        <div class="row col-12 pl-md-5 pl-sm-0">
                            <div class="col-md-6 pl-md-2 col-sm-12 pl-sm-1">
                                <div class="hs1 d-flex align-items-center">
                                    <h1 class="<?php echo $desk_colorTextoBanner; ?> title-banner m-0"><?php echo $desk_postType->post_title; ?></h1>
                                    <br class="d-md-none">
                                    <?php if( strpos($post->post_name,'landing') === false && strpos($post->post_name,'prueba-performance') === false ){ ?>
                                    <!--Social buttons-->
                                    <div class="banner-share d-inline-block">
                                        <div class="social-reveal">                                        
                                            <?php echo do_shortcode('[desktop_share cerrar="banner_carrusel" pagina="'.$permalink.'" clase="icon-share-banner" tipo="inline-ul" seccion="Banner: '.$desk_postType->post_title.'"]');?>
                                        </div>
                                        <!-- Validación para Imprimr el icono de Compartir -->
                                        <?php if( strlen( trim( $permalink ) ) > 3 ) { ?>
                                        <a class="share-button" title="Compartir"><?php echo do_shortcode( '[icono nombre="'.sanitize_title("compartir").'"][/icono]' ); ?></a>
                                        <?php } ?>
                                    </div>
                                    <!--/Social buttons-->
                                    <?php } ?>
                                </div>
                                <!-- Caption -->
                                <h2 class="<?php echo $desk_colorTextoBanner; ?> descripcion-banner mt-3"><?php echo $desk_SubtituloEstatico; ?></h2>
                                <h3 class="texto-banner-carousel <?php echo $desk_colorTextoBanner; ?>"><?php echo $desk_descripcionBannerEstatico; ?></h3>

                                <!-- Validación para Imprimr el call to action -->
                                <?php if( strlen( trim( $desk_urlCallToActionBannerEstatico ) ) > 3  &&
                                        strlen( trim( $desk_callToActionBannerEstatico ) ) > 3
                                        ) {
                                        if (strpos($desk_urlCallToActionBannerEstatico, 'javascript') !== false) {
                                    ?>
                                <a onclick="<?php echo $desk_urlCallToActionBannerEstatico; ?>" <?php if($post->post_name === 'testvocacional' ){echo 'data-solicitud-location="Banner"';} ?> class="btn back-naranja-unitec btn-warning mx-auto waves-effect waves-light" title="<?php echo $desk_callToActionBannerEstatico; ?>" id="btn-carousel-0"><?php echo $desk_callToActionBannerEstatico; ?></a>
                                <?php } else {?>
                                            <a href="<?php echo $desk_urlCallToActionBannerEstatico; ?>" class="btn back-naranja-unitec btn-warning mx-auto waves-effect waves-light" title="<?php echo $desk_callToActionBannerEstatico; ?>" id="btn-carousel-0"><?php echo $desk_callToActionBannerEstatico; ?></a>
                                <?php } }?>
                                <!-- Validación para Imprimr el call to action -->
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
<?php
    } 
/*End if Banner WordPress By SRP 28-08-2018*/
    else {
?>        
            <div class="result"></div>
            <!-- /7847748/HOMEUNITEC -->

            <script type='text/javascript'>
                
                var Linea = undefined;
                var mySpreadsheet = undefined;
                var urldfp = undefined;
                var TituloPost = "<?php echo $desk_postType->post_title; ?>";

                function impresion_dfp(linea, spreadsheet, url, urldfp){
                    //console.log(url);
                    var self = this;
                    
                    $.get("<?php echo get_template_directory_uri(); ?>/secciones_desk/desk_2_11_banner_estatico_DFP.php?titlePost= "+ TituloPost + "&urlDFP=" + urldfp, (data) => {
                         $(".result").html( data );
                        
                        Linea = linea;
                        mySpreadsheet = spreadsheet;
                        $ = jQuery;
                        //console.log("Ejecutando extraer con " + Linea);
                        extraer(Linea);
                    });
                };

                var googletag = googletag || {};
                googletag.cmd = googletag.cmd || [];
                (function() {
                    var gads = document.createElement('script');
                    gads.async = true;
                    gads.type = 'text/javascript';
                    var useSSL = 'https:' == document.location.protocol;
                    gads.src = (useSSL ? 'https:' : 'http:') +
                      '//www.googletagservices.com/tag/js/gpt.js';
                    var node = document.getElementsByTagName('script')[0];
                    node.parentNode.insertBefore(gads, node);
                })();
               
                googletag.cmd.push(function() {
                    googletag.pubads().addEventListener('impressionViewable', function(event) {
                        //console.log("LISTO");
                    });

                    googletag.defineSlot('/7847748/BHUNITEC', [1024, 768], 'div-gpt-ad-1534976505516-0').addService(googletag.pubads());
                    googletag.pubads().enableSingleRequest();
                    googletag.enableServices();
                    //console.log("Añadiendo el event");
                });
            </script>

            <script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/assets/frontend/js/vendor/min/jqueryLibrariesDFP.min.js"></script>
            <script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/assets/frontend_desktop/js/secciones/min/desk_2_11_banner_estatico_DFP.min.js"></script>

            <div id='div-gpt-ad-1534976505516-0' class='iframedfp' style="height: 0px;">
                <script type='text/javascript'>
                    googletag.cmd.push(function() { googletag.display('div-gpt-ad-1534976505516-0'); });
                </script>
            </div>

<?php
    }
?>



    