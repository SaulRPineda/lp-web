<?php 
    /**
     * WP Post Template: desk_10_1_descripcion_cards
     */
?>
<div id="perfiles"></div>
<section class="panelize">
<?php 
    $i=0;
    if(have_rows("tabs",$theID) ) { 
        while( have_rows('tabs') ) { the_row();
            $desk_tituloTab = get_sub_field('titulo_del_tab');
?>
        <?php 
            while( have_rows('tipo_de_contenido') ) { the_row();
                $opcion = get_row_layout('tipo_de_contenido');

                switch ($opcion) {
                    case 'contenido':
                        $cardPosicion = ( $i%2 == 0) ? "card-left" :"card-right";
                        //$cardAnimation = ( $i%2 ==0) ? "fadeInLeft" :"fadeInRight";
                        $imgPosicion = ( $i%2 == 0) ? "" :"card-inversa";
                        $desk_imagenCard = get_sub_field('imagen');
                        $desk_lista = get_sub_field('tipo_lista');
        ?>
    <div class="panel w-100" id="panel<?php echo $i; ?>">
        <article class="row w-100 pl-5 pr-5 m-0">
            <!--Card-->
            <div class="content-gral d-flex justify-content-center <?php echo $imgPosicion; ?>">
                <div class="col-lg-6 col-xl-6 col-md-12 p-0 d-flex align-items-center justify-content-center">
                    <!--Card image-->
                    <div class="overlay">
                        <img data-src="<?php echo $desk_imagenCard['url']; ?>" class="img-fluid w-100 rounded-0 lazy" alt="<?php echo $desk_imagenCard['alt']; ?>">
                        <a href="#!" title="">
                            <div class="mask rgba-white-slight"></div>
                        </a>
                    </div>
                    <!--/Card image-->
                </div>
                <div class="col-lg-5 col-xl-5 col-md-12 align-self-center justify-content-center">
                    <!--Card content-->
                    <div class="card-body card text-md-left <?php echo $cardPosicion; ?> p-4">
                        <div class="texto-int-card h-100 d-flex justify-content-center flex-column">
                            <h4 class="card-title-desk"><span class="filo-inicio-t pb-2">|</span>&nbsp;&nbsp;<?php echo $desk_tituloTab; ?></h4>
                            <!--Title-->
                            <h3 class="pl-4 card-sub-desk"><?php echo get_sub_field('titulo_card'); ?></h3>
                            <!--Text-->
                            <ul>
                                <?php foreach ($desk_lista as $key => $desk_bullet) { ?>
                                        <li class="lista-perfil w-100 text-left">
                                        <?php echo do_shortcode( '[icono nombre="'.sanitize_title("bullet").'" clase="bullet" ][/icono]' ); ?>
                                            <span><?php echo $desk_bullet['listado']; ?></span></li>
                                    </li>                                
                                <?php } ?>
                            </ul>
                        </div>
                    </div>
                    <!--/.Card content-->
                </div>
            </div>
            <!--/.Card--> 
        </article>
            <?php $i++; ?><!-- Bandera para cambiar de clase left o right -->
    </div>
        <?php
                    break;

                    default:
                    break;
                }

            }
        ?>
<?php
        }
    }
?>
</section>