<div id="carreras_relacionadas"></div>
<?php
/**
 * WP Post Template: desk_13_carreras_relacionadas
 */
?>
<?php   
    /*Se insertan los plugins que necesita la seccion tal y como se llama el archivo*/
    load_script( $pluginsNecesarios = array('flipcard') );
    /*End Se insertan los plugins que necesita la seccion tal y como se llama el archivo*/
 ?>
<section class="container-fluid contenedor-carreras wow fadeIn d-flex justify-content-center align-items-center p-0">
    <!-- Contenedor de la galería -->
    <div class="col-lg-12 justify-content-center d-flex align-items-center">
        <div class="contenido-centrado w-100">
            <div class="row d-flex justify-content-center"> <!-- Contenedor de encabezado de la sección -->
                <article class="col-10 text-center d-flex justify-content-center" data-wow-offset="50">
                    <h4 class="w-75 titleSection"><?php echo get_field('titulo', $theID); ?></h4>
                </article>
                <article class="col-10 text-center d-flex justify-content-center" data-wow-offset="50">
                        <h6 class="w-100 descriptionSection mt-1"><?php echo do_shortcode( get_field('desk_descripcion_carreras', $theID) ); ?></h6>
                </article>
            </div> <!-- Fin del contenedor de encabezado de la sección -->

            <div class="row cards-relacionadas w-100 m-0">
                <div class="col-1 d-flex align-items-center justify-content-center">
                    <!--Controls-->
                    <div class="controls-carreras d-flex justify-content-center">
                        <a class="btn-floating waves-effect waves-light btn-arrow carrousel_left_carreras d-none" data-slide="prev" title="Anterior">
                            <?php echo do_shortcode( '[icono nombre="'.sanitize_title("ver-izquierda").'" clase="arrow-right"][/icono]' ); ?>
                        </a>                        
                    </div>
                    <!--Controls-->
                </div>
                <div class="col-10">
                    <!--Section: Products v.4-->
                    <section class="text-center">
                        <!--Grid row-->
                        <div class="row">
                            <div class="carrousel_carreras w-100" data-pos="0">
                                <div class="carrousel_inner_carreras">
                                    <ul class="d-flex">
                                        <?php
                                            $i = 0;

                                            if(have_rows("carreras", $theID) ) {
                                                while( have_rows('carreras') ) { the_row();
                                                    $carreras = get_field( 'carreras' );    
                                                    $imagenDestacadaHorizontal = get_field( 'imagen_destacada_horizontal', $carreras[$i]->ID );
                                        ?>
                                        <!--Grid column-->
                                        <li class="col-lg-4 col-sm-12">
                                             <div class="w-100 card-wrapper m-0">
                                                <div id="card-carreras-de-interes-<?php echo $i;?>" class="card-rotating effect__click text-center h-100 w-100 d-flex align-items-center">
                                                        <div class="face front">
                                                            <a href="<?php echo get_permalink($carreras[$i]->ID); ?>" title="<?php echo $carreras[$i]->post_title; ?>" data-gtm-tr="SeeMore" data-gtm-carrera="<?php echo $carreras[$i]->post_title; ?>" data-gtm-interaccion="Imagen">
                                                                <div class="cover">
                                                                    <img data-src="<?php echo $imagenDestacadaHorizontal['url']; ?>" class="w-100 lazy" alt="<?php echo $imagenDestacadaHorizontal['alt']; ?>">
                                                                </div>
                                                                <div class="content text-left">
                                                                    <div class="title mb-xl-2"><?php echo $carreras[$i]->post_title; ?></div>
                                                                    <div class="desc-prod">
                                                                        <?php $contenido_producto = get_field('descripcion_del_producto', $carreras[$i]->ID); ?>
                                                                        <?php echo wp_trim_words( $contenido_producto, 40 ); ?>                                 

                                                                    </div>
                                                                </div>
                                                            </a>
                                                            <div class="footer w-100">
                                                                <div class="d-flex justify-content-between">
                                                                    <!-- Triggering button -->
                                                                    <a class="rotate-btn" data-card="card-carreras-de-interes-<?php echo $i;?>" title="Compartir"><?php echo do_shortcode( '[icono nombre="'.sanitize_title("compartir").'" clase="activator compartir-fix"][/icono]' ); ?></a>
                                                                    <a href="<?php echo get_permalink($carreras[$i]->ID); ?>" class="verMas" title="<?php echo get_field('boton', $theID); ?>" data-gtm-tr="SeeMore" data-gtm-carrera="<?php echo $carreras[$i]->post_title; ?>" data-gtm-interaccion="Ver más"><?php echo get_field('boton', $theID); ?></a>
                                                                </div>
                                                            </div>
                                                        </div> <!-- end front panel -->
                                                    <div class="face back p-0">
                                                        <div class="content p-0">
                                                            <div class="main p-3">
                                                                <div class="title text-center mb-1"><?php echo $carreras[$i]->post_title; ?></div>
                                                                <?php //$contenido_producto = get_field('descripcion_del_producto', $carreras[$i]->ID); ?>
                                                                <?php //echo wp_trim_words( $contenido_producto, 14 ); ?>
                                                                <div class="text-center">
                                                                    Conoce más sobre la oferta académica, y comparte esta información en tus redes sociales.
                                                                </div>
                                                                <div class="stats-container d-flex">
                                                                    <div class="stats d-block w-100">
                                                                        <h4 class="card-title desktop-card-title-comparte text-center border-0">Comparte</h4>
                                                                        <div class="card-back text-center d-flex align-items-center justify-content-center">
                                                                            <?php echo do_shortcode('[desktop_share cerrar="card-carreras-de-interes-'.$i.'" pagina="'.get_permalink($carreras[$i]->ID).'" clase="btn-share" tipo="inline-ul-desktop" seccion="Carreras Relacionadas: '.$carreras[$i]->post_title.'"]');?>
                                                                        </div>    
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="footer text-center w-100">
                                                            <a class="regresar rotate-btn" title="Volver" data-card="card-carreras-de-interes-<?php echo $i;?>">
                                                                <i class="fa fa-reply"></i><span> Regresar</span>
                                                            </a>
                                                        </div>
                                                    </div> <!-- end back panel -->
                                                </div> <!-- end card -->
                                            </div> <!-- end card-container -->                          
                                        </li>
                                        <!--Grid column-->
                                        <?php   
                                                    $i++;
                                                }
                                            }
                                        $none = ( $i > 3 )? "": "d-none"; ?>

                                    </ul>
                                </div>
                           </div>
                        </div>
                        <!--Grid row-->
                    </section>
                    <!--Section: Products v.4-->                
                </div>
                <div class="col-1 d-flex align-items-center justify-content-center">
                    <!--Controls-->
                    <div class="controls-carreras d-flex justify-content-center">
                        <a class="btn-floating waves-effect waves-light btn-arrow carrousel_right_carreras <?php echo $none; ?>" data-slide="next" title="Siguiente">
                            <!-- <i class="fa fa-chevron-left"></i> -->
                            <?php echo do_shortcode( '[icono nombre="'.sanitize_title("ver-derecha").'" clase="arrow-left"][/icono]' ); ?>
                        </a>                        
                    </div>
                    <!--Controls-->
                </div>
            </div>

        </div>
    </div>
    <!-- Fin del contenedor de galería -->
    <input type="hidden" name="totalCarreras" id="totalCarreras" value="<?php echo $i; ?>">
</section>
<?php  ?>


