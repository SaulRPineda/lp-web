<?php 
    /**
     * WP Post Template: desk_21_footer
     */
?>

<?php
    $urlSVG = get_template_directory_uri() . "/assets/frontend/img/footer/";

    /*Implementacion Custom field para Formulario*/
    $id_page = get_the_ID();
    $thumbID = get_post_thumbnail_id( $id_page );
    $urlImg = wp_get_attachment_url( $thumbID );
    $altImg = get_post_meta($thumbID, '_wp_attachment_image_alt', true);

    $contenido_multimedia = get_field('url' ,$theID);
    /*Definir una imagen en caso de no contar ni con Imagen Y Video Destacado*/
    /*End Implementacion Custom field para Formulario*/

    /*Se obtiene el SKU del producto asignado en el backoffice de wordpress (Grupo Carreras)*/
    global $woocommerce, $product, $post;
    $sku = get_post_meta($product->id); 
    $sku_id = $sku['_sku'][0];
    /*End Se obtiene el SKU del producto asignado en el backoffice de wordpress (Grupo Carreras)*/ 

    /*Se obtiene el tipo de post Type Page or Product*/
    $idPage = get_the_ID();
    $pageType = get_post_type( $idPage );
    /*End Se obtiene el tipo de post Type*/

    /*Se obtiene img Horizontal*/
    $img_horizontal = get_field('imagen_destacada_horizontal' ,$theID);
    $url_img_horizontal = $img_horizontal['url'];
    /*Se obtiene img Horizontal*/


    class footer extends Walker_Nav_Menu {

        /**
         * At the start of each element, output a <li> and <a> tag structure.
         * 
         * Note: Menu objects include url and title properties, so we will use those.
         * 
         */
        
        function start_lvl( &$output, $depth = 0, $args = array() ) {
        $output .= "\n<ul class=\"sub-menu row pl-3\">\n";
    }
        
        function start_el( &$output, $item, $depth = 0, $args = array(), $id = 0 ) {
            $elementoPadre = NULL;
    
            $output .= ( $depth > 0 ? str_repeat( "\t", $depth ) : '' ); // code indent
    
            // Passed classes.
            $classes = empty( $item->classes ) ? array() : (array) $item->classes;
            //print_r($classes);
            $class_names = esc_attr( implode( ' ', apply_filters( 'nav_menu_css_class', array_filter( $classes ), $item ) ) );
    
            if ( $item->menu_item_parent == 0 ) {
    
                if(empty($bandera)) {
                    $output .= '</ul>
                        </div>';
                }

                //print_r($item);
    
                if( strrpos( $class_names, "menu-item-has-children") > -1 ){
                    $elementoPadre = $item->title;
                    // echo  $item->menu_order;
                    //print_r ($item);
                } else{
                    $elementoPadre = '<a title="'.$item->title.'" href="'.$item->url.'">'.$item->title.'</a>'; 
                }
                switch ($item->ID) {
                    case 5014:
                        $colFooter = "2";
                        break;
                    case 5018:
                        $colFooter = "3";
                        break;
                    case 5022:
                        $colFooter = "5";
                        $rowUl = "row px-3";
                        $line = "sociales-border";
                        break;

                    default:
                        $colFooter = "3";
                        $rowUl = "";
                        $line = "";
                        break;
                }
    
                $output .= '<div class="col-sm-'.$colFooter.' '.$line.'">                           
                                   <span>';
                $output .= $elementoPadre;
    
                $output .= '</span>';
    
                $output .= '<ul class="'.$rowUl.'">';
                $bandera = 0;
            }
    
            //print_r($class_names);

            if( strrpos( $class_names, "menu-item-has-children") != -1 && $item->menu_item_parent != 0 ) {
                if($item->menu_item_parent == 5022){
                  //print_r($item->menu_item_parent);
                    $output .= '<li class="list-item col-6 p-0"><a title="'.$item->title.'" href="'.$item->url.'">'.$item->title.'</a></li>';
                }else{
                    $output .= '<li class="list-item col-12 p-0"><a title="'.$item->title.'" href="'.$item->url.'">'.$item->title.'</a></li>';
                }
                // echo $item->menu_item_parent;
                $bandera = 1;
            }
        }
    
    }
     
    ?>
    <!-- <pre><?php echo $item  ?></pre> -->
<!-- <script type="text/javascript">
    window.$zopim||(function(d,s){var z=$zopim=function(c){z._.push(c)},$=z.s=
    d.createElement(s),e=d.getElementsByTagName(s)[0];z.set=function(o){z.set.
    _.push(o)};z._=[];z.set._=[];$.async=!0;$.setAttribute("charset","utf-8");
    $.src="https://v2.zopim.com/?gOpEGGCuGfdOWrwyPRDrLEu8bVq8NRno";z.t=+new Date;$.
    type="text/javascript";e.parentNode.insertBefore($,e)})(document,"script");
</script> -->
<!-- Script del Chat <?php echo $post->post_name?>   -->
<!--Start of Zendesk Chat Script-->
<!-- <script type="text/javascript">
$zopim(function() {
    $zopim.livechat.button.show();
});
</script> -->
<!--End of Zendesk Chat Script-->

<!--Footer-->
<footer class="pt-2 pl-5 pr-5 footer">

    <input type="hidden" name="h_url_multimedia_formulario" id="h_url_multimedia_formulario" value='<?php echo $contenido_multimedia; ?>'>
    <input type="hidden" name ="h_url_imagen_destacada" id="h_url_imagen_destacada" value='<?php echo $urlImg; ?>'>
    <input type="hidden" name ="h_horizontal_url_imagen_destacada" id="h_horizontal_url_imagen_destacada" value='<?php echo $url_img_horizontal; ?>'>
    <input type="hidden" name="h_id_producto" id="h_id_producto" value='<?php echo $sku_id; ?>'>
    <input id="h_prellenado_formulario_pagina" type="hidden" value="true">
    <input id="h_prellenado_formulario_tipo" type="hidden" value="<?php echo $pageType; ?>">
    <!-- <input id="data_json_basura" type="hidden" value="0">
    <input id="data_json_basura_email" type="hidden" value="0"> -->
   
    <!-- Input que almacena el titulo Dinamico del formulario -->
    <!-- <input id="h_titulo_modal_formulario" type="hidden" value="">
 -->    <input type="hidden" id="h_tab-contiene-video" name="h_tab-contiene-video" value="N">
    <input type="hidden" id="h_video-activo" name="h_video-activo">

    <!--Footer contenido-->
    <div class="container-fluid text-center text-sm-left">
        <div class="row">
            <!--Título Footer-->
            <div class="col-sm-12">
                <h2 class="text-left mb-1"></i>Informes de Nuevo Ingreso</h2>
            </div>

        </div>
             <!-- segunda linea Footer -->
        <div class="sub-t-footer row">
              <h3 class="col-md-6 col-lg-3"><a class="d-flex" title="Solicita una llamada" data-gtm-tr="pushCall" data-gtm-accion="intención" style="cursor:auto;"><?php echo do_shortcode( '[icono nombre="'.sanitize_title("llamar-footer").'" clase="d-flex align-items-center"][/icono]' ); ?><span class="tel-footer"></span></a></h3>
              <?php if(strpos($post->post_name,'calcula-tu-beca')===false) { ?>
              <h3 class="col-md-6 col-lg-3"><a class="d-flex" title="Escríbenos" onclick="openModal('#modal_frm_app')" data-solicitud-location="Footer"><?php echo do_shortcode( '[icono nombre="'.sanitize_title("contacto-footer").'" clase="d-flex align-items-center"][/icono]' ); ?>&nbsp;&nbsp;Solicita información</a></h3>
              <h3 class="col-md-6 col-lg-3"><a class="d-flex" title="Chatea con nosotros" data-gtm-tr="chat" data-gtm-pos="intencion" data-gtm-location="footer" data-gtm-etiqueta="con formulario" onclick="javascript:$zopim.livechat.window.show()"><?php echo do_shortcode( '[icono nombre="'.sanitize_title("chat").'" clase="d-flex align-items-center"][/icono]' ); ?>&nbsp;&nbsp;Chatea con nosotros</a></h3>
              <!-- <h3 class="col-md-6 col-lg-3"><a class="d-flex" title="WhatsApp" onclick="openModal('#modal-whatsapp')" data-gtm-tr="evWhats" data-gtm-accion="intención" data-gtm-seccion="footer" data-gtm-depto=""><?php echo do_shortcode( '[icono nombre="'.sanitize_title("whatsapp").'" clase="d-flex align-items-center"][/icono]' ); ?>&nbsp;&nbsp;Whatsapp</a></h3> -->
              <?php } ?>
              <!-- <h3 class="col-md-6 col-lg-3"><a class="d-flex" title="WhatsApp"data-gtm-tr="Whatsapp" data-gtm-pos="footer" href="https://clxt.ch/unitecg" target="_blank"><?php echo do_shortcode( '[icono nombre="'.sanitize_title("whatsapp").'" clase="d-flex align-items-center"][/icono]' ); ?> Whatsapp</a></h3> -->
        </div>
        <div class="row pt-1">
            <!-- Aquí se llama al menú footer_desk y se declaran sus parámetros nativos WP -->
              <?php
              wp_nav_menu(
                  array('menu'=> 'footer_desk',
                  'items_wrap' => '%3$s',
                  'container_class' => false, 
                  'walker'  => new footer() //use our custom walker
                      )
              );
              ?>
              <div class="col-sm-2 sociales-border">
                  <!-- Título de categoría --> <span>Redes Sociales</span>
                  <ul>
                      <li><a title="Nuestro Blog" href="https://blogs.unitec.mx/" target="blank"><?php echo do_shortcode( '[icono nombre="'.sanitize_title("blog").'"][/icono]' ); ?> Nuestro Blog</a></li>
                      <li><a title="Comunidad UNITEC" href="https://comunidad.unitec.mx/" target="blank"><?php echo do_shortcode( '[icono nombre="'.sanitize_title("comunidad").'"][/icono]' ); ?> Comunidad UNITEC</a></li>
                      <li><a title="Facebook" href="https://www.facebook.com/unitec" target="blank" data-gtm-tr="SocNetJmp" data-gtm-value="Facebook"><?php echo do_shortcode( '[icono nombre="'.sanitize_title("facebook-footer").'"][/icono]' ); ?> Facebook</a></li>
                      <li><a title="Twitter" href="https://twitter.com/unitecmx" target="blank" data-gtm-tr="SocNetJmp" data-gtm-value="Twitter"><?php echo do_shortcode( '[icono nombre="'.sanitize_title("twitter-footer").'"][/icono]' ); ?> Twitter</a></li>
                      <li><a title="Youtube" href="https://www.youtube.com/user/unitecmex/featured" target="blank" data-gtm-tr="SocNetJmp" data-gtm-value="Youtube"><?php echo do_shortcode( '[icono nombre="'.sanitize_title("youtube-footer").'"][/icono]' ); ?> Youtube</a></li>
                  </ul>
              </div>
        </div>
    </div>
    <!--/.Footer contenido-->
</footer>
<!--/.Footer-->

<!--Copyright-->
<div class="container-fluid">
  <div class="row pt-1 pl-5 pr-5">
    <div class="col-sm-8 term-pol p-0">
        <ul class="d-flex flex-wrap mt-0">
            <li><a title="Términos y condiciones" class="terminos" href="//www.unitec.mx/terminos-y-condiciones/">Términos y condiciones</a> |&nbsp; </li>
            <li><a title="Políticas de privacidad" class="terminos" href="//www.unitec.mx/politicas-de-privacidad/">Políticas de privacidad</a> |&nbsp; </li>
            <li><a title="Comunidad UNITEC" class="terminos" href="https://comunidad.unitec.mx/" target="blank">Comunidad UNITEC</a> |&nbsp; </li>
            <li><a title="Portal Administrativo" class="terminos" href="https://my.laureate.net/pages/fbalogin.aspx?ReturnUrl=%2fstaff%2fPages%2fhome.aspx" target="blank">Portal Administrativo</a></li>
        </ul>
    <!-- | 
        <a href="//www.unitec.mx/pago-seguro/">Pago Seguro</a><br> -->
        <!-- <div class="text-legal">
            <p>Acuerdo Secretarial 142 de la SEP de la fecha 24 de Octubre de 1988 <br> Universidad Tecnológica de México. Derechos Reservados 2017 UNITEC. Avenida Parque Chapultepec No. 56, Piso 1, Col. El Parque. Mun. Naucalpan de Juarez, EdoMéx., 53398. Laureate Education Inc.</p>
        </div> -->
    </div>
    <div class="col-sm-4">
        <div class="text-center d-flex justify-content-between align-items-center">
            <a href="" class="sponsors" title="esr">
                <img class="img-spons lazy" data-src="<?php echo $urlSVG . "svg/esr.svg" ?>" alt="esr" />
            </a>
            <a href="" class="sponsors" title="laureate">
                <img class="img-spons lazy" data-src="<?php echo $urlSVG . "svg/laureate.svg" ?>" alt="laureate" />
            </a>
            <a href="" class="sponsors" title="amipci">
                <img class="img-spons lazy" data-src="<?php echo $urlSVG . "svg/amipci.svg" ?>" alt="amipci" />
            </a>
            <a href="" class="sponsors" title="fimpes">
                <img class="img-spons lazy" data-src="<?php echo $urlSVG . "svg/fimpes.svg" ?>" alt="fimpes" />
            </a>
            <a href="" class="sponsors" title="geotrust">
                <img class="img-spons lazy" data-src="<?php echo $urlSVG . "svg/geotrust.svg" ?>" alt="geotrust" />
            </a>
        </div> 
    </div>  
  </div>
  <div class="row pt-1">
    <div class="text-legal pl-5 pr-4">
        <p>Acuerdo Secretarial 142 de la SEP de la fecha 24 de Octubre de 1988 <br> Universidad Tecnológica de México. Derechos Reservados 2018 UNITEC. Avenida Parque Chapultepec No. 56, Piso 1, Colonia El Parque. Municipio Naucalpan de Juárez, Estado de México., 53398. Laureate Education Inc.</p>
    </div>
  </div>
</div>
<!--/.Copyright-->   
<!-- /* termina coopia*/ -->

