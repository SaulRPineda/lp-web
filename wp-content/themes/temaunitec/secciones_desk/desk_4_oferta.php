<div id="oferta-educativa" ></div>
<?php
/**
 * WP Post Template: desk_4_oferta
 */
?>

<?php

    $field = get_field_object('tipo_de_banner');
    $tipoBanner = $field['value'];
    $label = $field['choices'][ $tipoBanner ];

    /*Se insertan los plugins que necesita la seccion tal y como se llama el archivo*/
    load_script( $pluginsNecesarios = array('flipcard') );
    /*End Se insertan los plugins que necesita la seccion tal y como se llama el archivo*/

    $actualTab = 0;
    $json_completo = NULL;
    /* Arreglo para manejar las areas y descripciones de iconos */
    $totalPadres = NULL;
    $json_pestanas = [];
    $json_areas = [];
    $nombre_json_tab = "tab-";
    $json_completo ="{";
    $array_completo = [];
    $numTabs = count(get_field('tabs'));
    while( have_rows('tabs') ) {
        $concat_nombre_tab = $nombre_json_tab . $actualTab;

        the_row();
        $json_completo .= '"' . $concat_nombre_tab . '": "'.trim(get_sub_field('titulo_del_tab_oferta')).'",';
        $array_completo[$concat_nombre_tab] = trim(get_sub_field('titulo_del_tab_oferta'));


        $json_completo .= '"' . $concat_nombre_tab . '-pestanas" :[{';
        $case_valido = false;
        $json_pestanas[$actualTab] = [];
        $countPrepas = 1;
        while( have_rows('pestanas') ) {
            the_row();
            // $array_completo[$concat_nombre_tab . "-pestanas"] = [];

            $campo_obtenido = get_sub_field('titulo');
            $tipo_contenido = get_sub_field('tipo_de_contenido');

            // if($campo_obtenido->name == "Preparatoria"){
            if($campo_obtenido->name == ""){
                $json_pestanas[$campo_obtenido->name.$countPrepas] = [];
                $json_pestanas[$actualTab].array_push($json_pestanas[$actualTab],$campo_obtenido->name);
                $json_pestanas[$campo_obtenido->name.$countPrepas].array_push($json_pestanas[$campo_obtenido->name],$campo_obtenido->description);
                $json_pestanas[$campo_obtenido->name.$countPrepas].array_push($json_pestanas[$campo_obtenido->name],$tipo_contenido);
            }else{
                $json_pestanas[$campo_obtenido->name] = [];
                $json_pestanas[$actualTab].array_push($json_pestanas[$actualTab],$campo_obtenido->name);
                $json_pestanas[$campo_obtenido->name].array_push($json_pestanas[$campo_obtenido->name],$campo_obtenido->description);
                $json_pestanas[$campo_obtenido->name].array_push($json_pestanas[$campo_obtenido->name],$tipo_contenido);
            }

            // $array_completo_pestanas = $array_completo[$concat_nombre_tab . "-pestanas"][0] = [];
            // $array_objeto = {};
            // $array_completo_pestanas.array_push($array_completo, $campo_obtenido, $array_objeto);
            switch ($tipo_contenido) {
                case 'filtrado':
                    $categoria_padre=get_sub_field('filtro');
                    $term=$categoria_padre->term_id;

                    $totalPadres[] = $term;

                    $taxonomy='product_cat';
                    // Id´s adm y ciencias
                    $areas_de_estudio= get_term_children( $term, $taxonomy );
                    $areas = NULL;

                    $json_limpio = NULL;

                    /* Declararla solo si es la primera tab */
                    // if($actualTab == 0)
                    $json_careers = NULL;

                    foreach ($areas_de_estudio as $key => $value) {
                        $orden_areas_de_estudio = get_term_meta($value);
                        $areas[$orden_areas_de_estudio['order'][0]] = $value;
                    }

                    /*Ordenar Alfabeticamente*/
                    ksort($areas);


                    /* Iniciar el objeto json solo si es la primera tab */
                    // if($actualTab == 0)
                    $json_careers = '{';

                    /* Arreglo para manejar las areas por tab */
                    $json_areas[$actualTab] = [];
                    $json_completo .= '"' . $campo_obtenido->name . '" : {';
                    // $json_completo .= '"nombre": "' . $campo_obtenido->name . '",';
                    $json_completo .= '"tipo-contenido" : ' . '"' . $tipo_contenido . '",';
                    $json_completo .= '"areas" : ' . '[';
                    foreach ($areas as $key => $value) {
                        if($value!=NULL){
                            //Nombres de adm y ciencias
                            $nombre_areas_interes = get_term_by('id', $value, $taxonomy);

                            $args = array(
                                'post_type'             => 'product',
                                'order'                 => 'ASC',
                                'post_status'           => 'publish',
                                'ignore_sticky_posts'   => 1,
                                'posts_per_page'        => '-1',
                                // 'orderby'       =>     'title',
                                // 'order' => 'ASC',
                                'tax_query' => array(
                                    'relation' => 'AND',
                                    array(
                                        'taxonomy' => 'product_cat',
                                        'field'    => 'term_id',
                                        'terms'    => $campo_obtenido->term_id,
                                    ),
                                    array(
                                        'taxonomy' => 'product_cat',
                                        'field'    => 'term_id',
                                        'terms'    => $value,
                                    ),
                                ),
                            );
                            $products = query_posts($args);

                            if ( count($products) > 0 ) {
                                $json_areas[$nombre_areas_interes->name] = [];
                                $json_areas[$actualTab].array_push($json_areas[$actualTab],$nombre_areas_interes->name);
                                $json_areas[$nombre_areas_interes->name].array_push($json_areas[$nombre_areas_interes->name],$nombre_areas_interes->description);
                                $json_careers .= '"'.$nombre_areas_interes->name.'":[';
                                foreach ($products as $key => $product) {

                                    $imgHorizontal = get_field('imagen_destacada_horizontal', $product->ID);
                                    $urlImgHorizontal = $imgHorizontal['url'];
                                    $altImg = $imgHorizontal['alt'];
                                    $descriptionProduct = get_field('descripcion_del_producto', $product->ID);
                                    $productLink = get_permalink($product->ID);

                                    $json_careers .= '{"title":"'.$product->post_title.'", "url":"'.$urlImgHorizontal.'", "alt" : "'.$altImg.'", "permalink": "'.$productLink.'", "description" : "'.trim($descriptionProduct).'"},';
                                }
                                $json_careers = substr($json_careers, 0, strlen($json_careers) - 1);
                                $json_careers .= '],';
                            }
                          }
                        } // foreach
                        $json_limpio = $json_careers;
                        $json_completo .= $json_limpio;
                        $json_completo = substr($json_completo, 0, strlen($json_completo) - 1);
                        $json_completo .= '}]},';
                        $case_valido = true;
                        break;

                     default:
                         $case_valido = false;
                     break;
                  }
              } //Terminan pestañas

              $actualTab++;
              if ($actualTab == $numTabs) {
                  $json_careers = substr($json_careers, 0, strlen($json_careers) - 1);
                  $json_careers .= '}';
              }

              $json_completo = substr($json_completo, 0, strlen($json_completo) - 1);
              $json_completo .= '}],'; /* json que termina pestañas */
          }
          $json_completo = substr($json_completo, 0, strlen($json_completo) - 1);
          $json_completo .= '}';
?>
<script>
    var careers_json = '<?php echo $json_completo; ?>';
</script>

<?php
    $json_completo = json_decode($json_completo, true);
    // print_r($json_completo);
    //print_r($json_pestanas);
    $mostrarPadre = ( count( $totalPadres ) > 1 ) ? "": "d-none";
    // print($json_completo['tab-0-pestanas'][0]['Licenciaturas']['areas']);
    // $json_completo_areas = $json_completo['tab-1-pestanas'][0]['En línea']['areas'][0];
    // echo count($json_completo['tab-0-pestanas'][0]);
?>

<section class="container-fluid wow fadeIn d-flex justify-content-center align-items-center cont-principal-oferta px-1"><!-- /.Main <layout-->
    <div class="col-lg-11 justify-content-center d-flex align-items-center p-0 oferta-educativa">

        <div class="contenido-oferta-educativa w-100">
            <div class="row"> <!-- Contenedor de encabezado de la sección -->
                <article class="col-12 text-center d-flex justify-content-center" data-wow-offset="50">
                    <h4 class="w-75 titleSection"><?php echo get_field('desk_tituloOferta', $theID); ?></h4>
                </article>
                <article class="col-12 text-center d-flex flex-column justify-content-center" data-wow-offset="50">
                    <h6 class="w-100 descriptionSection m-0"><?php echo get_field('desk_descripcionOferta', $theID); ?></h6>
                    <?php if ($tipoBanner == 'dfp'){ ?>
                        <h1 class="w-100 descriptionSection m-0"><?php echo get_field('contenidoh1', $theID); ?></h1>
                    <?php } ?>
                </article>
            </div> <!-- Fin del contenedor de encabezado de la sección -->

            <?php if( $numTabs > 1 ) { ?>

            <div class="row filtros-oferta">
                <div class="col-12">
                    <!-- Tab Navegacion -->
                    <section id="tabNav">
                        <div class="tabs-wrapper">
                            <ul class="nav classic-tabs d-flex justify-content-center" role="tablist">
                            <?php $noTabs = 0;
                                while( have_rows('tabs') ) { the_row();

                            ?>
                            <?php if($noTabs == 0) { $active = "active text-black"; } ?>
                                <li id="nav-item" class="nav-item">
                                    <a class="waves-light <?php echo $active; ?>" data-toggle="tab" href="#tab-panel-oferta-<?php echo $noTabs; ?>" role="tab" onclick="resaltar('tab-panel-oferta-<?php echo $noTabs; ?>');" title="<?php echo trim(get_sub_field('titulo_del_tab_oferta')); ?>"><?php echo trim(get_sub_field('titulo_del_tab_oferta')); ?></a>
                                </li>
                              <?php $active = NULL; ?>
                              <?php $noTabs++; } ?>
                            </ul>
                        </div>
                    </section>
                    <!-- Tab Secciones -->
                </div>
            </div>
            <?php } ?>

            <div class="oferta-card">

                <div class="tab-content">
                <?php
                    $active = NULL;
                    $show = NULL;
                    $areas_de_estudio = NULL;
                    $panelContent=0;
                    $itemSlide = 0;
                    $actualTab = 0;
                    $actualPrepa = 0;
                    /*$itemCollapse = 0;*/
                ?>
                <?php while( have_rows('tabs') ) { the_row();
                      // $countPestanas = count(get_sub_field('pestanas',$theID));
                ?>
                  <?php if(have_rows("pestanas",$theID) ){ ?>
                      <?php if($panelContent == 0){ $active = "active"; } ?>

                    <div class="tab-pane fade in show tabs-oferta <?php echo $active; ?>" id="tab-panel-oferta-<?php echo $panelContent; ?>" role="tabpanel">
                        <div class="row">
                            <!--    Aquí va el menú izquierdo    -->
                            <div class="col-xl-2 col-lg-2 col-md-12 col-s-12 p-md-0">
                                <article class="col-12 text-left d-flex p-0" data-wow-offset="50">
                                    <h4 class="w-100 titulo-menu-oferta">Quiero cursar:</h4>
                                </article>

                                <!--Accordion wrapper-->
                                <div class="accordion md-accordion mb-1" id="accordionEx-<?php echo $actualTab ?>" role="tablist" aria-multiselectable="true">
                                    <!-- Accordion card -->
                                    <div class="card card-menu-oferta">
                                    <!-- while para contar slides -->
                                    <?php $actualPestana = 0; foreach ($json_pestanas[$actualTab] as $key => $pestanas_value) {
                                    //echo $actualTab;
                                    // if ($numTabs == 1)
                                    //     break 1;?>
                                    <?php //$icono = ( trim($pestanas_value) == "preparatoria" || trim($pestanas_value) == "Preparatoria" || trim($pestanas_value) == "PREPARATORIA")? "preparatoria": $json_pestanas[$pestanas_value][0];  ?>
                                    <?php $icono = ( trim($pestanas_value) == "" || trim($pestanas_value) == "" || trim($pestanas_value) == "")? "": $json_pestanas[$pestanas_value][0];  ?>

                                    <?php 
                                        $iconArrow = "ver-abajo";
                                        if($itemSlide == 0) { $active = "active"; }
                                        //if($actualPestana == 0) { $show = ""; }
                                         if($actualPestana == 0) { $show = "show"; }
                                    ?>
                                        <!-- Card header Titulo de la categoría-->
                                        <div class="card-first <?php echo $mostrarPadre; ?>" role="tab" id="heading-<?php echo $actualTab ?>">
                                            <a class="oferta-tit d-flex align-items-center px-2 py-2 collapsed" data-toggle="collapse" data-parent="#accordionEx-<?php echo $actualTab ?>" href="#collapse-<?php echo $actualTab ?>-<?php echo $actualPestana ?>" aria-expanded="true" aria-controls="collapse-<?php echo $actualTab ?>" title="<?php echo trim($pestanas_value); ?>">
                                                <?php echo do_shortcode( '[icono clase="icon-menu-oferta" nombre="'.sanitize_title($icono).'"][/icono]' ); ?>
                                                <h5 class="txt-menu-oferta m-0 pl-1" id="nameOffer"><?php echo trim($pestanas_value); ?></h5>
                                                <?php echo do_shortcode( '[icono id="iconArrow" clase="rotate-icon icon-arrow-oferta d-flex justify-content-center align-items-center" nombre="'.sanitize_title($iconArrow).'"][/icono]' ); ?>
                                            </a>
                                        </div>
                                        <!-- Card body Hijos de la Categoría-->
                                        <div id="collapse-<?php echo $actualTab ?>-<?php echo $actualPestana ?>" class="collapse card-second <?php echo $show; ?>" role="tabpanel" aria-labelledby="heading-<?php echo $actualTab ?>" data-parent="#accordionEx-<?php echo $actualTab ?>" >
                                            <div class="menu-oferta">
                                            <?php 
                                                // if($pestanas_value == "Preparatoria") {
                                                if($pestanas_value == "") {
                                                    $actualPrepa++;
                                                    $pestanas_value = $pestanas_value.$actualPrepa;
                                                }
                                                switch ($json_pestanas[$pestanas_value][1]) {
                                                case 'filtrado':?>
                                                <div class="col-12 p-1">
                                                <?php
                                                    foreach ($json_completo['tab-'.$actualTab.'-pestanas'][0][$pestanas_value]['areas'][0] as $key => $areas_value) {
                                                ?>
                                                    <div class="padding-bottom-menu">
                                                        <div type="button" class="list-item-container menu-lista d-flex align-items-center lista-nivel-academico pl-2" id="<?php echo trim($key); ?>" name="<?php echo trim($key); ?>" data-parentNode="<?php echo $pestanas_value ?>">
                                                            <div class="item-icon d-flex justify-content-center">
                                                                <?php echo do_shortcode( '[icono nombre="'.sanitize_title($json_areas[$key][0]).'"][/icono]' ); ?>
                                                            </div>
                                                            <div class="item-description pl-2" id="nameCareer"><?php echo trim($key); ?></div>
                                                        </div>
                                                    </div>
                                                    <?php
                                                        } // foreach
                                                    ?>
                                                </div>
                                                <?php
                                                    break;
                                                 //Bullets para Prepa
                                                    default:
                                                        // echo "<div class='list-item-container menu-lista d-flex align-items-center lista-nivel-academico pl-2' id='Preparatoria-conoce-mas'><div class='item-description pl-4' id='nameCareer'>Conoce Más</div></div>";
                                                     break;
                                                } ?><!-- end Switch -->
                                            </div>
                                        </div>
                                    <?php
                                        $active = NULL;
                                        $show = NULL;
                                        $actualPestana++;
                                    } //end while
                                    ?>
                                    </div>
                                    <!-- Accordion card -->
                                </div>
                                <!--/.Accordion wrapper-->
                            </div>
                            <div class="col-xl-10 col-lg-10 col-md-12 col-s-12 p-md-0">
                                <div class="row w-100 m-0">
                                    <div class="col-1 d-flex align-items-center justify-content-center pr-0">
                                        <!--Control izq-->
                                        <div class="controls">
                                            <a class="btn-floating waves-effect waves-light btn-arrow carrousel_left_oferta d-none" data-slide="next" title="Anterior">
                                                <?php echo do_shortcode( '[icono nombre="'.sanitize_title("ver-izquierda").'" clase="arrow-right"][/icono]' ); ?>
                                            </a>
                                        </div>
                                        <!--Control izq-->
                                    </div>
                                    <!--div: Products v.4-->
                                    <div class="col-10 p-1 text-center carrusel-oferta" data-pos="0">
                                        <!--Grid row-->
                                        <div class="row cards-oferta">
                                            <div class="carrousel_inner_oferta">
                                                <ul id="fillCareers">

                                                </ul>
                                            </div>
                                        </div>
                                        <!--Grid row-->
                                    </div>
                                    <!--div: Products v.4-->

                                    <div class="col-1 d-flex align-items-center justify-content-center">
                                        <!--Control der-->
                                        <div class="controls">
                                            <a class="btn-floating waves-effect waves-light btn-arrow carrousel_right_oferta <?php echo $none; ?>" data-slide="prev" title="Siguiente">
                                                <?php echo do_shortcode( '[icono nombre="'.sanitize_title("ver-derecha").'" clase="arrow-left"][/icono]' ); ?>
                                            </a>
                                        </div>
                                        <!--Control der-->
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>
                    <!-- #panel -->
                    <?php $itemSlide=0; ?>
                    <?php $panelContent++; ?>
                    <?php } ?>
                <?php $actualTab++;  } ?>
                </div>
                <input type="hidden" name="panelContent" id="panelContent" value="<?php echo $panelContent; ?>">

            </div>
        </div>
    </div>
</section><!-- /.Main <layout-->
