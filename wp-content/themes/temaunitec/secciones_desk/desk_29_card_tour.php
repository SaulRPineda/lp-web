<div id="tour_virtual"></div>
<?php
/**
 * WP Post Template: desk_29_card_tour
 */
?>
<?php
    $postType = get_post( $idPost );
    $tituloTourVirtual = get_field('titulo_seccion_tour_virtual', $theID);
    $descriptionTourVirtual = get_field('descripcion_tour_virtual', $theID);
    $imgTourDesk = get_field('img_fondo_tour_desk', $theID);
        $urlTourDeskImg = $imgTourDesk["url"];
        $altTourDesk = $imgTourDesk["alt"];
    $icon360 = get_field('icon_360', $theID);
        $urlIcon360 = $icon360["url"];
        $altIcon360 = $icon360["alt"];
    $textCallToAction = get_field('call_to_action_tour_virtual', $theID);
    $urlTourVirtual = get_field('tour_virtual', $theID);
?>
<script type="text/javascript">
    var titlePage = '<?php echo $postType->post_title; ?>';
    jQuery(window).on('load', function(){
        if(localStorage.getItem("abrirModalTour") == 'true'){
            openModal('#tourVirtual');
            dataLayer.push({
                'event' : 'autoOpenTour',
                'campus' : titlePage,
                'location' : 'Auto Open Tour Virtual'                          
            });
        }
        localStorage.removeItem("abrirModalTour");
    });
    function onVrViewLoad() {
        var vrView = new VRView.Player('#vr-view', {
            image: urlTourMobImg360,
            width: '100%',
            height: '100%',
            is_vr_off: true,
            default_yaw: 0,
            default_pitch: 90,
            hide_fullscreen_button: true,
            is_stereo: true
        });
    }
</script>
<section class="align-items-center mt-4">
    <div class="container-fluid wow fadeIn d-flex justify-content-center align-items-center mb-1">
        <div class="col-lg-12">            
            <article class="col-12 text-center d-flex justify-content-center mt-1" data-wow-offset="50">
                <h4 class="w-75 titleSection"><?php echo $tituloTourVirtual; ?></h4>
            </article>
            <article class="col-12 text-center d-flex justify-content-center" data-wow-offset="50">
                <h6 class="w-100 descriptionSection mt-1"><?php echo $descriptionTourVirtual; ?></h6>
            </article>
        </div>
    </div>
    <div class="contenedor-tour-virtual wrap container-fluid d-flex justify-content-center align-items-center">
        <div class="bg-tour-virtual" style="background-image: url(<?php echo $urlTourDeskImg; ?>)"></div>
        <div class="info-tour row w-100 d-flex justify-content-center">
            <div class="col-4 d-flex flex-column align-items-center justify-content-center">
                <svg viewBox="0 0 100 100" style="width:100px;">
                    <g style="transform-origin:50px 50px 0px;transform:scale(0.9)">
                        <g style="transform-origin:50px 50px 0px">
                            <g style="transform-origin:50px 50px 0px">
                                <g class="ld ld-breath" style="animation-delay:-2.2s;animation-direction:normal;animation-duration:2.2s;transform-origin:50px 50px 0px">
                                    <circle cx="50" cy="50" r="27.5" fill="none" stroke="#fff" stroke-linecap="round" stroke-linejoin="round" stroke-miterlimit="10" stroke-width="3"/>
                                </g>
                                <g class="ld ld-breath" style="animation-delay:-2.112s;animation-direction:normal;animation-duration:2.2s;transform-origin:50px 50px 0px">
                                    <ellipse cx="50" cy="50" rx="15" ry="27.5" fill="none" stroke="#fff" stroke-linecap="round" stroke-linejoin="round" stroke-miterlimit="10" stroke-width="3"/>
                                </g><g class="ld ld-breath" style="animation-delay:-2.024s;animation-direction:normal;animation-duration:2.2s;transform-origin:50px 50px 0px">
                                    <line x1="50" x2="50" y1="22.5" y2="77.5" fill="none" stroke="#fff" stroke-linecap="round" stroke-linejoin="round" stroke-miterlimit="10" stroke-width="3"/>
                                </g><g class="ld ld-breath" style="animation-delay:-1.936s;animation-direction:normal;animation-duration:2.2s;transform-origin:50px 50px 0px">
                                    <line x1="24.365" x2="75.686" y1="40.046" y2="40.046" fill="none" stroke="#fff" stroke-linecap="round" stroke-linejoin="round" stroke-miterlimit="10" stroke-width="3"/>
                                </g>
                                <g class="ld ld-breath" style="animation-delay:-1.848s;animation-direction:normal;animation-duration:2.2s;transform-origin:50px 50px 0px">
                                    <line x1="24.365" x2="75.686" y1="59.454" y2="59.454" fill="none" stroke="#fff" stroke-linecap="round" stroke-linejoin="round" stroke-miterlimit="10" stroke-width="3"/>
                                </g>
                            </g>
                            <g style="transform-origin:50px 50px 0px">
                                <g style="transform-origin:50px 50px 0px">
                                    <g class="ld ld-breath" style="animation-delay:-1.76s;animation-direction:normal;animation-duration:2.2s;transform-origin:50px 50px 0px">
                                        <path d="M30.002,84.637 C18.049,77.72,10,64.804,10,50c0-20.492,15.409-37.385,35.273-39.724" fill="none" stroke="#fff" stroke-linecap="round" stroke-linejoin="round" stroke-width="3"/>
                                    </g>
                                    <g style="transform-origin:50px 50px 0px">
                                        <g class="ld ld-breath" style="animation-delay:-1.672s;animation-direction:normal;animation-duration:2.2s;transform-origin:50px 50px 0px">
                                            <polygon points="44.157 4.939 44.94 15.844 50 10" fill="#fff"/>
                                        </g>
                                    </g>
                                </g>
                            </g>
                            <g style="transform-origin:50px 50px 0px">
                                <g style="transform-origin:50px 50px 0px">
                                    <g class="ld ld-breath" style="animation-delay:-1.584s;animation-direction:normal;animation-duration:2.2s;transform-origin:50px 50px 0px">
                                        <path d="M69.998,15.363 C81.951,22.28,90,35.196,90,50c0,20.492-15.409,37.385-35.273,39.724" fill="none" stroke="#fff" stroke-linecap="round" stroke-linejoin="round" stroke-width="3"/>
                                    </g>
                                    <g style="transform-origin:50px 50px 0px">
                                        <g class="ld ld-breath" style="animation-delay:-1.496s;animation-direction:normal;animation-duration:2.2s;transform-origin:50px 50px 0px">
                                            <polygon points="55.843 95.061 55.06 84.156 50 90" fill="#fff"/>
                                        </g>
                                    </g>
                                </g>
                            </g>
                        </g>
                    </g>
                </svg>
                <!-- <img class="img-fluid logo-unitec-tour" src="<?php echo $urlIcon360; ?>" alt="<?php echo $altIcon360; ?>"> -->
                <span class="title-campus-tour"><?php echo $postType->post_title; ?></span>
                <a href="#" type="button" class="btn btn-back-tour waves-effect waves-light" data-toggle="modal" data-target="#tourVirtual" data-gtm-tr="TourVirtual" data-gtm-campus="<?php echo $postType->post_title; ?>" data-gtm-pos="Secci�n Tour Virtual" >
                    <span>► <?php echo $textCallToAction; ?></span>
                </a> 
            </div>
        </div>
    </div>
</section>
<!-- Modal -->
<div class="modal p-0 fade" id="tourVirtual" tabindex="-1" role="dialog" aria-labelledby="modalTourVirtual| " aria-hidden="true">
  <div class="modal-dialog w-100 h-100 contentIframeTour" role="document">
    <div class="modal-content h-100">
      <div class="modal-header p-0 header-close-tour">
        <button type="button" class="close closeIframe" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>

       <iframe class="w-100 h-100 frameToruVirtual" src="<?php echo $urlTourVirtual; ?>"></iframe>

    </div>
  </div>
</div>

<!-- <div class="modal p-0 fade" id="tourVirtual" tabindex="-1" role="dialog" aria-labelledby="modalTourVirtual" aria-hidden="true">
    <div class="w-100 h-100 contentIframeTour" role="document">
        <iframe class="w-100 h-100 frameToruVirtual" src="https://tourmkr.com/G15jzVuTKQ/6277097p,250366m,-14.29h,89.10t"></iframe>
    </div>
</div> -->
