<div id="detalle"></div>
<?php
/**
 * WP Post Template: desk_22_card_basica
 */
?>

<?php
$activeCardB = NULL;
$claseCardB = "card-inversa";
$claseCardBasica = "card-right";

$conteoTituloCB = count(get_field('desk_titulo_card_basica'));
if ($conteoTituloCB == 0)
    $claseTituloCB = "d-none";
else
    $claseTituloCB = NULL;

$conteoDescripcionCB = count(get_field('descripcion_card_basica'));
if ($conteoDescripcionCB == 0)
    $claseDescripcionCB = "d-none";
else
    $claseDescripcionCB = NULL;

$conteoTitDescCB = $conteoTituloCB + $conteoDescripcionCB;
if ($conteoTitDescCB == 0)
    $claseTitDescCB = "d-none";
else
    $claseTitDescCB = NULL;

$conteoCardsB = count(get_field('card_basica_tabs'));
if($conteoCardsB == 1)
    $claseTabsCB = "d-none";
else
    $claseTabsCB = NULL;

?>
<?php if(have_rows("card_basica_tabs",$theID) ) { ?>
<section class="card-basica d-flex align-items-center"><!-- d-flex align-items-center -->
    <div class="container-fluid wow fadeIn d-flex justify-content-center w-100">
        <div class="col-lg-12">
            <div class="row justify-content-center <?php echo $claseTitDescCB; ?>"> <!-- Contenedor de encabezado de la sección -->
                <article class="col-10 text-center <?php echo $claseTituloCB; ?>" data-wow-offset="50">
                    <h4 class="w-75 titleSection mx-auto"><?php echo get_field('desk_titulo_card_basica', $theID); ?></h4>
                </article>
                <article class="col-10 text-center <?php echo $claseDescripcionCB; ?>" data-wow-offset="50">
                        <h6 class="w-100 descriptionSection mt-1 mx-auto"><?php echo do_shortcode( get_field('descripcion_card_basica', $theID) ); ?></h6>
                </article>
            </div> <!-- Fin del contenedor de encabezado de la sección -->


            <!-- Tab Navegacion -->
            <section class="mb-1 <?php echo $claseTabsCB; ?>" id="tabNav">
                <div class="tabs-wrapper">
                    <ul class="nav classic-tabs tabs-dinamicas justify-content-center" role="tablist">
                    <?php //if(have_rows("card_basica_tabs",$theID) ) { 
                        $noTabsDinamicosCB=0;  
                        while( have_rows('card_basica_tabs') ) { the_row(); 
                            if($noTabsDinamicosCB == 0){ $activeCardB = "active"; } 
                    ?>         
                        <li class="nav-item m-0"><!-- waves-effect waves-light -->
                            <a class="tab-card-basica-text waves-light <?php echo $activeCardB; ?>" data-toggle="tab" href="#tab-card-basica-<?php echo $noTabsDinamicosCB; ?>" role="tab" title="<?php echo get_sub_field('titulo_del_tab_oferta_basica', $theID); ?>" data-gtm-tr="Tab" data-gtm-tab= "<?php echo get_sub_field('titulo_del_tab_oferta_basica'); ?>"><?php echo get_sub_field('titulo_del_tab_oferta_basica', $theID); ?></a>
                        </li>
                        <?php   $noTabsDinamicosCB++; 
                                $activeCardB = NULL; 
                            } 
                        //} 
                    ?>
                    </ul>
                </div>
            </section>

            <div class="row w-100 m-0">
                <div class="col-1 d-flex align-items-center justify-content-center">
                    <!--Controls-->
                    <div class="controls-card-basica d-flex justify-content-center">
                        <a class="btn-floating waves-effect waves-light btn-arrow carrousel_left_card_basica d-none" data-slide="next" title="Anterior">
                            <?php echo do_shortcode( '[icono nombre="'.sanitize_title("ver-izquierda").'" clase="arrow-right"][/icono]' ); ?>
                        </a>                        
                    </div>
                    <!--Controls-->
                </div>
                <div class="col-10 iconos-unitec justify-content-center">
                    <!--Grid row-->
                    <div class="row tab-content">
                        <?php 
                            $lCardB = 0; 
                            $panelContentCB = 0; 
                            $tabCountCB = 0;
                            $cardsCountCB = 0;
                            while( have_rows('card_basica_tabs') ) { the_row();
                                $activeCardB = NULL; 
                                if($panelContentCB == 0) { $activeCardB = "active"; }
                                ?>
                        <div class="carrousel_cards_basicas w-100 tab-pane fade in show <?php echo $activeCardB; ?>" data-pos="0" id="tab-card-basica-<?php echo $tabCountCB; ?>"  role="tabpanel">
                        <div class="carrousel_inner_cards_basicas">
                            <!-- Tab panels -->
                            <ul class="d-flex m-0" >
                                <?php //if(have_rows("card_basica_tabs",$theID) ) {
                                    while( have_rows('pestanas_basic') ) { the_row();                               
                                        $desk_ImagenBasica = get_sub_field('imagen_nueva_basica');                                
                                        $desk_btnbasica = get_sub_field('boton_basico');
                                        $desk_urlbasica = get_sub_field('url_del_boton_card_basica');
                                        //$subtitulo = get_sub_field('subtitulo_card_basica');
                                        if($panelContentCB%2 == 0){ $claseCardB = ""; $claseCardBasica = "card-left"; } 
                                            if($tabCountCB == 0)
                                                $cardsCountCB++;            
                                    ?>
                                    <!--Inicia Tab -->
                                    <li class="col-12 p-2 cardstext">
                                        <!--Card-->
                                        <div class="row m-0 justify-content-center d-flex <?php echo $claseCardB; ?>">
                                            <div class="col-lg-12 col-xl-7 p-0 d-flex align-items-center justify-content-center">
                                                <!--Card image-->
                                                <div class="overlay b-rad">
                                                    <img class="lazy img-fluid rounded-0" alt="<?php echo $desk_ImagenBasica['alt']; ?>" data-src="<?php echo $desk_ImagenBasica['url']; ?>">
                                                </div>
                                                <!--/Card image-->
                                            </div>
                                            <div class="col-lg-12 col-xl-5 align-self-center">
                                                <!--Card content-->
                                                <div class="card-body card text-md-left <?php echo $claseCardBasica; ?> p-4">
                                                    <div class="texto-int-card h-100 d-flex justify-content-center flex-column">
                                                        <?php
                                                            $validarSubtitulo = strlen( get_sub_field('titulo_card_basica'));
                                                            if ($validarSubtitulo == 0)
                                                                $ocultarSubtitulo = "d-none";
                                                            else
                                                                $ocultarSubtitulo = NULL;
                                                        ?>
                                                        <h4 class="card-title-desk <?php echo $ocultarSubtitulo; ?>">
                                                            <span class="filo-inicio-t pr-3">|</span>
                                                            <?php echo get_sub_field('titulo_card_basica'); ?>
                                                            <?php //echo $desk_TituloBasica; ?>
                                                        </h4>
                                                        <!--Title-->
                                                        <!-- <h3 class="pl-4 card-sub-desk"><?php //echo get_sub_field('titulo_card_basica'); ?></h3> -->
                                    
                                                        <!-- <?php 
                                                            if ( strlen( trim($subtitulo) ) > 2 ) { ?>
                                                            <h4 class="pl-4 card-sub-int"><?php echo $subtitulo; ?></h4>
                                                        <?php } ?>                             -->

                                                        <!-- Ajustar Estilo -->
                                                        <div class="card-sub-int"><?php echo get_sub_field('tipo_de_contenido_basica'); ?></div>
                                                        <!--Text-->
                                                        
                                                        <?php if ( strlen( trim($desk_urlbasica) ) > 5 ) { ?>
                                                        <div class="navegacion d-flex justify-content-end">
                                                            <a href="<?php echo $desk_urlbasica; ?>" class="verMas pull-right" title="<?php echo $desk_btnbasica; ?>">
                                                                <?php echo $desk_btnbasica; ?>
                                                            </a>
                                                        </div>
                                                        <?php } ?>
                                                    </div>
                                                </div>
                                                <!--/.Card content-->
                                            </div>
                                        </div>
                                        <!--/.Card--> 
                                    </li>
                                    <!-- Termina Tab -->
                                    <?php   
                                        if($tabCountCB == 0)
                                            $lCardB++;
                                        $claseCardB = "card-inversa";
                                        $claseCardBasica = "card-right";
                                        $panelContentCB++;
                                            }         
                                    ?>
                                <?php //} ?>
                                    <!-- Termina Tab -->
                                </ul>
                                <!-- //Tab panels -->
                            </div>
                        </div>
                        <?php
                            $tabCountCB++;
                        } 
                        ?>
                    </div>
                    <!-- //Grid row-->
                </div>
                <div class="col-1 d-flex align-items-center justify-content-center">
                    <!--Controls-->
                    <div class="controls-card-basica d-flex justify-content-center">
                        <a class="btn-floating waves-effect waves-light btn-arrow carrousel_right_card_basica <?php if($cardsCountCB == 1) {echo "d-none";} ?>" data-slide="prev" title="Siguiente">
                            <?php echo do_shortcode( '[icono nombre="'.sanitize_title("ver-derecha").'" clase="arrow-left"][/icono]' ); ?>
                        </a>                       
                    </div>
                    <!--Controls-->
                </div>
            </div>

        </div>
    </div>
    <input type="hidden" name="totalCardsBasicas" id="totalCardsBasicas" value="<?php echo $lCardB; ?>">
</section><!-- Background -->
<?php } ?>