<div id="botones_principales"> </div>
<?php
/**
 * WP Post Template: desk_23_botones_principales
 */
?>
<?php 
    //$nuevaCarrera = get_field('card_nueva_carrera', $theID);
    $nuevaCarrera = get_field('nuevas_carreras', $theID);
    $titulo = get_field('titulo', $theID);
    $boton = get_field('boton', $theID); 
    $active = NULL;   

    $desc = do_shortcode( get_field('descripcion_contenido_botones_principales', $theID) );
    $contenido = explode("|", $desc );
    $descripcion = $contenido[0];
    $fechas = $contenido[1];
    
    $columna = "col-lg-3";
    $ajuste_botones = "";
?>

<section class="background-definido contenidoCompleto mt-2"><!-- Background -->
    <div class="container-fluid col-11">
        <!-- Live preview-->
        <div class="row justify-content-center">
            <div class="col-11">
                <!-- Descripción del producto -->
                <article class="col-12 text-center mb-3 mt-3 d-flex justify-content-center" data-wow-offset="50">
                    <h6 class="w-100 descripcion"><?php echo $descripcion; ?></h6>
                </article>
                <!-- Descripción del producto -->
            </div>           
    
            <!-- Sección calendario -->
            <div class="col-11 mb-4 d-flex flex-column justify-content-between card p-3">
                <!-- Implementacion de Titulo y descripcion en la sección calendario -->
                <?php 
                    $desk_contenidoGroup = get_field('desk_group_descripcion_botones', $theID);
                    $desk_titulo = ( empty( get_field('desk_titulobotones') ) || get_field('desk_titulobotones') == NULL ) ? "Información inicio de curso": get_field('desk_titulobotones');
            
                //if( $desk_contenidoGroup['desk_titulo_seccion_ficha'] != NULL || $desk_contenidoGroup['desk_titulo_seccion_ficha'] != "" ) { ?>
                    <h3><span class="filo-inicio-t pb-2">|</span>&nbsp;<?php echo $desk_titulo; ?></h3>                   
                    <div class="mb-0 descripcion mt-1"><?php echo get_field('desk_contenido_principal_botones', $theID); ?></div>
                <?php //} ?>
                <!-- Implementacion de Titulo y descripcion en la sección calendario -->
                <div class="mt-1">
                    <div class="list-item-container">                                
                        <div class="item-description">
                            
                            <div class="row justify-content-center">
                                <?php
                                    if ( empty( $fechas ) < 0) {
                                        $columna = "col-lg-4";
                                }
                                 //if ( count( $fechas ) == NULL ) { 
                                    //$columna = "col-lg-4";
                                ?>

                                <?php if($fechas != NULL) { ?>
                                <div class="<?php echo $columna; ?> col-md-7 col-sm-12 d-flex justify-content-center flex-column">
                                    <h4 class="fecha-cluster"><?php echo $fechas; ?></h4>
                                </div>
                                <?php
                                    //+}
                                ?>
                                <div class="<?php echo $columna; ?> col-md-7 col-sm-12 d-flex flex-column align-items-center justify-content-center">
                                    <div class="datos">
                                        <h6>Datos de contacto</h6>
                                    </div>
                                    <div class="botones w-100">
                                        <div class="botones-contacto d-flex justify-content-around">             
                                            <a onclick="openModal('#modal_frm_app')" data-gtm-tr="frmTradicional" data-gtm-pos="descripcion | boton-contacto-sobre" class="iconos-contacto tooltip-info"><?php echo do_shortcode( '[icono nombre="'.sanitize_title("correo-card").'"][/icono]' ); ?><span class="tooltiptext">Escríbenos</span></a>
                                            <a href="javascript:$zopim.livechat.window.show()" data-gtm-tr="chat" data-gtm-pos="descripcion | botones-contacto" class="iconos-contacto tooltip-info"><?php echo do_shortcode( '[icono nombre="'.sanitize_title("sms").'"][/icono]' ); ?><span class="tooltiptext">Chat en línea</span></a>
                                            <a onclick="openModal('#modal_frm_app')" data-gtm-tr="frmTradicional" data-gtm-pos="descripcion | boton-contacto-lapiz" class="iconos-contacto tooltip-info"><?php echo do_shortcode( '[icono nombre="'.sanitize_title("formularios").'"][/icono]' ); ?><span class="tooltiptext">Contacto</span></a>
                                        </div>                    
                                    </div>
                                </div>
                                <div class="<?php echo $columna; ?> col-md-7 col-sm-12 calendario d-flex justify-content-center align-items-center">
                                    <?php echo do_shortcode( '[icono nombre="'.sanitize_title("calendario").'"][/icono]' ); ?>
                                        <a href="<?php echo get_field('desk_linkBotones', $theID) ?>" class="text-center pt-2 align-self-center caracteristicas ver-calendario" target="_blank" title="<?php echo get_field('desk_texto_Botones') ?>"><?php echo get_field('desk_texto_Botones') ?></a>
                                </div>
                                <?php } else { $columna = "col-lg-12"; }?>

                                <div class="<?php echo $columna; ?> <?php if($fechas == NULL) { echo "col-md-7 col-sm-12 botonesUnitecB d-flex flex-row"; $ajuste_botones = "adjust-botonesUnitecB"; } else { echo "col-md-7 col-sm-12 botonesUnitecB d-flex flex-column"; } ?>">
                                    <?php 
                                        $links_botones = get_field( 'call_to_actions_pagina', $theID );                 

                                        if( $links_botones > 0 ) {
                                            $s=1;

                                            for ($i=0; $i < 3; $i++) {
                                                $texto_call = $links_botones['pagina_texto_call_to_action_'.$s];
                                                $link_call = $links_botones['pagina_link_call_to_action_'.$s];
                                                if ( strpos( strtolower( $link_call ), "://" ) ) {

                                                    if( strpos( strtolower( $link_call ), "pdf" ) ){
                                                        $target = "target='_blank'";
                                                    }

                                                    else if ( strpos( strtolower( $link_call ), "unitec.mx" ) ) {
                                                        $target = "";
                                                    }

                                                    else{
                                                        $target = "target='_blank'";
                                                    }
                                                } else{
                                                    $target = "";
                                                }

                                                switch ($s) {
                                                    case 1: $clase = "btn-primary mx-auto mt-1 waves-effect waves-light $ajuste_botones"; break;
                                                    case 2: $clase = "back-naranja-unitec btn-warning mx-auto mt-1 waves-effect waves-light $ajuste_botones"; break;
                                                    case 3: $clase = "boton-azul btn-default mx-auto mt-1 waves-effect waves-light $ajuste_botones"; break;                                    
                                                    default: break;
                                                }
                                    ?>
                                    <?php if (strpos($link_call, 'javascript') !== false) { ?>
                                    <a onclick="<?php echo $link_call; ?>" class="btn <?php echo $clase; ?> d-flex justify-content-center align-items-center p-2" <?php echo $target; ?> title="<?php echo $texto_call; ?>" data-solicitud-location="Middle"><?php echo $texto_call; ?></a>
                                    <?php } else { ?>
                                    <a href="<?php echo $link_call; ?>" class="btn <?php echo $clase; ?> d-flex justify-content-center align-items-center p-2" <?php echo $target; ?> title="<?php echo $texto_call; ?>"><?php echo $texto_call; ?></a>
                                    <?php } ?>
                                    <?php
                                                $s++;
                                            } 
                                        }
                                    ?>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- Fin Sección calendario -->
        </div>
    </div>
</section>
<?php //} ?>
