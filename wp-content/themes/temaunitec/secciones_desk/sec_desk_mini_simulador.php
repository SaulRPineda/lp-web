<?php
/**
 * WP Post Template: sec_desk_mini_simulador
 */
?>

    <!-- Start your project here-->
    <div class="container">
        <div class="row text-center">
            <div class="min col-md-4">
                <span class="min-chart" id="chart-alumnos" data-percent="56"><span class="percent"></span></span>
                <h5><span class="label red">Alumnos <i class="fa fa-arrow-circle-up"></i></span></h5>
            </div>
            <div class="col-md-4">
                <span class="min-chart" id="chart-profesores" data-percent="23"><span class="percent"></span></span>
                <h5><span class="label red">Prefesores <i class="fa fa-arrow-circle-up"></i></span></h5>
            </div>
            <div class="col-md-4">
                <span class="min-chart" id="chart-exalumnos" data-percent="62"><span class="percent"></span></span>
                <h5><span class="label red">Ex-alumnos <i class="fa fa-arrow-circle-up"></i></span></h5>
            </div>
        </div>

        <br>

    </div>

    <script>
        $(function () {
            $('.min-chart#chart-alumnos, .min-chart#chart-profesores,  .min-chart#chart-exalumnos').easyPieChart({
                barColor: "#FF007F",
                onStep: function (from, to, percent) {
                    $(this.el).find('.percent').text(Math.round(percent));
                }
            })
        });
     </script>

