<?php
/**
 * WP Post Template: desk_2_13_banner_conexion
 */
?>
<?php
$grupoBanner = get_field('desk_group_banner', $theID);
$grupoBannerContenido = get_field('desk_group_banner_contenido', $theID);

$desk_TituloConexion = $grupoBanner['desk_subtitulo_del_slider'];
$desk_descripcionBannerConexion = $grupoBanner['desk_descripcion_banner_estatico'];

$deskBannerDesktop = $grupoBanner['desk_banner_estatico'];
$urlBannerDesktop = $deskBannerDesktop["url"];
$altBannerDesktop = $deskBannerDesktop["alt"];


$desk_callToActionBannerConexion = $grupoBannerContenido['desk_call_to_action_banner_estatico'];
$desk_urlCallToActionBannerConexion = $grupoBannerContenido['desk_url_call_to_action_banner_estatico'];
$desk_colorTextoBanner = $grupoBannerContenido['desk_color_del_banner'];

$desk_idPost = get_the_ID(); 
$desk_postType = get_post( $idPost ); 
$permalink = get_permalink($idPost);
?>

<div id="conexion"></div>
<section class="section-banner">
    <!-- <video style="position:absolute;bottom:0" src="https://www.unitec.mx/videos/video-buscador-desktop.mp4" class="video-fluid video-banner" preload="auto" autoplay loop muted playsinline ></video> -->
    <div style="background-image:url(<?php echo $urlBannerDesktop; ?>);" alt="<?php echo $altBannerDesktop; ?>" class="img-background">
        <div class="bann_wind animated fadeIn bg-video">
            <div class="view p-heredada d-flex align-items-center">
                <div class="mask container d-flex align-items-center">
                    <div class="row col-12 pl-md-5 pl-sm-0">
                        <div class="col-md-6 pl-md-2 col-sm-12 pl-sm-1 d-flex flex-column justify-content-center">
                            <div class="d-flex align-items-center">
                                <h1 class="texto_claro tituloBanner h1 m-0"><?php echo $desk_TituloConexion; ?></h1>
                                <br class="d-md-none">
                            </div>
                            <h2 class="texto_claro subtituloBanner mt-2"><?php echo $desk_descripcionBannerConexion; ?></h2>
                            <!-- <h3 class="texto-banner-carousel texto_claro">Recibe fechas importantes del examen EGEL, toma de fotografías, entrega de diplomas y más.</h3> -->
                            <a href=<?php echo $desk_urlCallToActionBannerConexion; ?>" class="btn back-naranja-unitec waves-effect waves-light m-0 mt-1 w-50" title="<?php echo $desk_callToActionBannerConexion; ?>" id="btn-carousel-0"><?php echo $desk_callToActionBannerConexion; ?></a>
                        </div>
                        <div class="col-md-6 pl-md-2 col-sm-12 pl-sm-1">
                            <div class="card w-75 card-formulario">
                                <!-- <div class="img-header-formulario-expuesto" id="img-frm-absorcion" style="background-image: url(https://unitecmx-universidadtecno.netdna-ssl.com/wp-content/uploads/2017/08/admision-unitec-principal-h.jpg);"></div> -->
                                <div class="card-body mx-4" id="paso1-frm-absorcion">
                                    <div class="p-0 d-flex justify-content-center flex-column form-head">
                                        <div class="row">
                                            <!-- <div class="col-2 p-0 logo-formulario-expuesto"></div> -->
                                            <div class="col-12 p-0 d-flex flex-column justify-content-center align-items-center titulo-formulario-expuesto">
                                                <h2 class="m-0">Sé verde y actualiza tus datos</h2>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="md-form pb-1 mt-1">
                                        <div class="col-md-12 mb-1 md-form p-0">
                                            <label for="form-user">Ingresa tu número de cuenta: <span class="sr-only">*</span></label>
                                            <input type="tel" id="form-user" name="quantity" min="1" maxlength="12" class="form-control" id="validationCustom032" required>
                                            <div class="invalid-feedback">Por favor, ingresa un número valido</div>
                                        </div>
                                    </div>
                                    <div class="frm-politicas mt-2">
                                        <label  class="politicas light">
                                            Al hacer clic en <span class="span-continuar">"Continuar"</span>, reconoces haber leído las <a href="//www.unitec.mx/politicas-de-privacidad/" target="_blank">Políticas de Privacidad</a> y confirmas estar de acuerdo con el uso de ellas, así como los <a href="//www.unitec.mx/terminos-y-condiciones/" target="_blank">Términos y Condiciones</a> del sitio.
                                        </label>
                                    </div>
                                    <div class="row mb-1">
                                        <div class="w-100 px-2">
                                            <button class="btn btn-frm-expuesto w-100 m-0" id="ingresar-cuenta">CONTINUAR</button>
                                        </div>
                                    </div>
                                </div>

                                <div id="proceso-absorcion"></div>
                                
                                <div class="card-body mx-4 d-none" id="paso2-frm-absorcion">
                                    <div class="row">
                                        <div class="col-12 d-flex flex-column justify-content-center align-items-center p-3">
                                            <h3 class="m-0 descFormExpuesto">Al actualizar tus datos, recibirás fechas importantes del examen EGEL, toma de fotografías, entrega de diplomas y más.</h3>
                                        </div>
                                    </div>
                                    <form class="needs-validation mt-2" novalidate>
                                        <div class="form-row">
                                            <div class="col-md-10 mb-1 md-form p-0">
                                                <label for="validationCustom012">Nombre Completo:</label>
                                                <input disabled type="text" class="form-control" id="validationCustom012" placeholder="Nombre completo"  required>
                                                <div class="invalid-feedback">No puede estar vacío el campo</div>
                                            </div>

                                            <div class="col-md-10 mb-1 md-form p-0">
                                                <label for="validationCustomUsername2">Correo personal:</label>
                                                <input type="email" name="Correo" class="form-control" id="validationCustomUsername2" aria-describedby="inputGroupPrepend2" required> 
                                                <div class="invalid-feedback">Por favor, ingresa un correo valido</div>
                                            </div>
                                            <div class="col-md-10 mb-1 md-form p-0">
                                                <label for="validationCustom032">Número celular:</label>
                                                <input type="tel" name="Teléfono" name="quantity" min="1" maxlength="10" class="form-control" id="validationCustom032" required>
                                                <div class="invalid-feedback">Por favor, ingresa un número valido</div>
                                            </div>
                                        </div>

                                        <div class="form-group m-0 mt-1">
                                            <div class="form-check pl-0">
                                                <input class="form-check-input" type="checkbox" value="" id="invalidCheck2" required>
                                                <label class="form-check-label" for="invalidCheck2">Acepto términos y condiciones</label>
                                                <div class="invalid-feedback">Debes estar deacuerdo antes de enviar</div>
                                            </div>
                                        </div>

                                        <div class="frm-politicas mb-1">
                                            <label  class="politicas light">
                                                Al hacer clic en <span class="span-continuar">"GUARDAR"</span>, reconoces haber leído las <a href="//www.unitec.mx/politicas-de-privacidad/" target="_blank">Políticas de Privacidad</a> y confirmas estar de acuerdo con el uso de ellas, así como los <a href="//www.unitec.mx/terminos-y-condiciones/" target="_blank">Términos y Condiciones</a> del sitio.
                                            </label>
                                        </div>

                                        <div class="row mb-1">
                                            <div class="w-100 px-2">
                                                <button class="btn btn-frm-expuesto w-100 m-0" id="guardar" type="submit">GUARDAR</button>
                                            </div>
                                        </div>
                                    </form>
                                </div>

                                <div id="msj-guardado" class="card-blue-absorcion h-100 w-100 align-items-center d-none"></div>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>