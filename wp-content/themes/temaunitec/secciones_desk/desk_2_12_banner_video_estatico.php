<?php
/*
 * WP Post Template: desk_2_12_banner_video_estatico
 */
 ?>
<?php
    $grupoBanner = get_field('group_banner_video_estatico', $theID);
    $grupoBannerDescripciones = get_field('descripciones_banner_video', $theID);
    $grupoBannerContenido = get_field('desk_group_banner_contenido', $theID);

    $deskBannerEstatico = $grupoBanner['desk_video_banner'];

    $descripcionBannerPrincipal = $grupoBannerDescripciones['descripcion_principal'];
    $descripcionBannerSecundaria = $grupoBannerDescripciones['descripcion_secundaria'];
    $descripcionBAnnerRefuerzo = $grupoBannerDescripciones['descripcion_refuerzo'];

    $desk_idPost = get_the_ID(); 
    $desk_postType = get_post( $idPost ); 
    $permalink = get_permalink($idPost);

    $desk_callToActionBannerEstatico = $grupoBannerContenido['desk_call_to_action_banner_estatico'];
    $desk_urlCallToActionBannerEstatico = $grupoBannerContenido['desk_url_call_to_action_banner_estatico'];
    $desk_colorTextoBanner = $grupoBannerContenido['desk_color_del_banner'];

?>

<script type="text/javascript">
    (function ($) {
    $(document).ready(function () {

        $(document).on('click.card', '.card', function (e) {
            if ($(this).find('.card-reveal').length) {
                if ($(e.target).is($('.card-reveal .card-title')) || $(e.target).is($('.card-reveal .card-title i'))) {
                    // Make Reveal animate down and display none
                    $(this).find('.card-reveal').velocity({
                        translateY: 0
                    }, {
                        duration: 225,
                        queue: false,
                        easing: 'easeInOutQuad',
                        complete: function () {
                            $(this).css({
                                display: 'none'
                            });
                        }
                    });
                } else if ($(e.target).is($('.card .activator')) ||
                    $(e.target).is($('.card .activator i'))) {
                    $(this).find('.card-reveal').css({
                        display: 'block'
                    }).velocity("stop", false).velocity({
                        translateY: '-100%'
                    }, {
                        duration: 300,
                        queue: false,
                        easing: 'easeInOutQuad'
                    });
                }
            }


        });

    });
}(jQuery));

</script>


<section class="section-banner">
    <video style="position:absolute;bottom:0" src="<?php echo $deskBannerEstatico; ?>" class="video-fluid video-banner" preload="auto" autoplay loop muted playsinline ></video>
    <div class="bann_wind animated fadeIn bg-video">
        <div class="view p-heredada d-flex align-items-center">
            <div class="mask container d-flex align-items-center">
                <div class="row col-12 pl-md-5 pl-sm-0">
                    <div class="col-md-6 pl-md-2 col-sm-12 pl-sm-1">
                        <div class="d-flex align-items-center">
                            <!-- <h1 class="<?php echo $desk_colorTextoBanner; ?> title-banner m-0"><?php echo $desk_postType->post_title; ?></h1> -->
                            <h1 class="<?php echo $desk_colorTextoBanner; ?> texto-banner-carousel h1 m-0"><?php echo $descripcionBannerPrincipal; ?></h1>
                            <br class="d-md-none">
                        </div>
                        <!-- Caption -->
                        <h2 class="<?php echo $desk_colorTextoBanner; ?> descripcion-banner mt-3"><?php echo $descripcionBannerSecundaria; ?></h2>
                        <!-- <h3 class="texto-banner-carousel <?php echo $desk_colorTextoBanner; ?>"><?php echo $descripcionBannerSecundaria; ?></h3> -->
                        <h3 class="texto-banner-carousel <?php echo $desk_colorTextoBanner; ?>"><?php echo $descripcionBAnnerRefuerzo; ?></h3>

                        <!-- Validación para Imprimr el call to action -->
                        <?php if( strlen( trim( $desk_urlCallToActionBannerEstatico ) ) > 3  &&
                                strlen( trim( $desk_callToActionBannerEstatico ) ) > 3
                                ) {
                                if (strpos($desk_urlCallToActionBannerEstatico, 'javascript') !== false) {
                            ?>
                        <a onclick="<?php echo $desk_urlCallToActionBannerEstatico; ?>" class="btn back-naranja-unitec btn-warning mx-auto waves-effect waves-light" title="<?php echo $desk_callToActionBannerEstatico; ?>" id="btn-carousel-0"><?php echo $desk_callToActionBannerEstatico; ?></a>
                        <?php } else {?>
                                    <a href="<?php echo $desk_urlCallToActionBannerEstatico; ?>" class="btn back-naranja-unitec btn-warning mx-auto waves-effect waves-light" title="<?php echo $desk_callToActionBannerEstatico; ?>" id="btn-carousel-0"><?php echo $desk_callToActionBannerEstatico; ?></a>
                        <?php } }?>
                        <!-- Validación para Imprimr el call to action -->
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>  