<?php
/*
 * WP Post Template: desk_1_menu
 */

/**
* Custom walker class.
*/
class sidenav extends Walker_Nav_Menu {
    public $mini_menu; 

    function start_lvl( &$output, $depth = 0, $args = array()) {
        $output .= "\n" . $indent . '<div class="collapsible-body"><ul>';
    }

    function end_lvl( &$output, $depth = 0, $args = array() ) {
        $output .=  "\n" . $indent . "</ul></div>";
    }

    function start_el( &$output, $item, $depth = 0, $args = array(), $id = 0 ) {
        //print_r($args);
        //print_r($item);

        $indent = ( $depth > 0 ? str_repeat( "\t", $depth ) : '' ); // code indent

        // Passed classes.
        $classes = empty( $item->classes ) ? array() : (array) $item->classes;

        $class_names = esc_attr( implode( ' ', apply_filters( 'nav_menu_css_class', array_filter( $classes ), $item ) ) );
        

        $arrow = ( strrpos( $class_names, "menu-item-has-children") > -1 )? '<i class="icon-u icon-u-ver-abajo"></i>': "";

        if( strrpos( $class_names, "menu-item-has-children") > -1 ){
            $parentHead = '<ul class="collapsible collapsible-accordion"><li>';
            $class_names = "class='collapsible-header waves-effect d-flex align-items-center text-white arrow-r'";
        }else{
            $parentHead = '<li>';
            $class_names = "class='waves-effect d-flex align-items-center text-white'";
            $arrowcollapse = "";
            $attributes = ! empty( $item->url )? ' href="' . esc_attr( $item->url) .'" ' : '';
        }
        
        if( $item->title == "hr" ){
            $output .= "<hr>";
        }else{
            $mm = array( "Preparatoria", "Licenciaturas", "Posgrados" );
            if( in_array( $item->title, $mm ) ){
                $this->mini_menu[$item->title] = $item->url;
            }
            // Build HTML.
            // <a href=”” data-gtm-tr=”NavOpt” data-gtm-menu=”<<Menu Section>>” > </a>
            $output .= $indent . $parentHead . 
                '<a title="' . $item->title . '" ' . $class_names . $attributes . ' data-gtm-tr="NavOpt" data-gtm-menu="'.$item->title.'">' .
                    /*'<i class="icon-u icon-u-' . sanitize_title( $item->title ) . '"></i>' .*/
                    /*do_shortcode( '[icono nombre="'.sanitize_title($item->title).'"][/icono]' ).*/
                    do_shortcode( '[icono nombre="'.sanitize_title($item->classes[0]).'"][/icono]' ).
                    $item->title .
                    $arrow .
                '</a>';
        }
    }

    function end_el( &$output, $item, $depth = 0, $args = array() ) {
        $output .= "</li>";
    }
}

$sideNav = new sidenav();

//FUNCIONALIDAD PARA RECUPERAR LA OFERTA EDUCATIVA

//$orderby = 'name';
//$order = 'asc';
//$hide_empty = false ;
//$taxonomy='product_cat';
//$cat_args = array(
//    'orderby'    => $orderby,
//    'order'      => $order,
//    'hide_empty' => $hide_empty,
//    'slug'       => 'oferta-educativa',
//);
//
//$product_category_parent = get_terms( 'product_cat', $cat_args );
//// print_r($product_category_parent[0]->term_id);
//
//$cat_args = array(
//    'orderby'    => $orderby,
//    'order'      => $order,
//    'hide_empty' => $hide_empty,
//    'parent'     => $product_category_parent[0]->term_id,
//);
//
//$product_categories = get_terms( 'product_cat', $cat_args );
//
//// print_r($product_categories);
//// print_r($product_categories[0]->term_id);
//
//// $areas_de_estudio= get_term_children( $category_object->term_id, $taxonomy );
//
//// print_r($areas_de_estudio);
//
//$args_menu = array(
//        'post_type'             => 'product',
//        'post_status'           => 'publish',
//        'ignore_sticky_posts'   => 1,
//        'posts_per_page'        => '-1',
//        'fields'        =>      array('ID', 'post_title', 'post_name', 'guid'),
//        'orderby'       =>     'post_title',
//        'order' => 'ASC',
//        'tax_query' => array(
//            array(
//                'taxonomy' => 'product_cat',
//                'field'    => 'slug',
//                'terms'    => 'en-linea',
//            )
//        ),
//    );
//    $products_la_prueba = query_posts($args_menu);
//
//    // echo('MENU-LINEA-');
//    // print_r ($products_la_prueba);
//
//    $careers_product_lic_linea = [];
//    $careers_product_ing_linea = [];
//    $careers_product_dip_linea = [];
//    $careers_product_mtria_linea = [];
//    foreach($products_la_prueba as $key => $productLinea) {
//        if(strpos($productLinea->post_name,"licen") !== false){
//            array_push($careers_product_lic_linea,$productLinea);
//        }
//        if(strpos($productLinea->post_name,"ingen") !== false){
//            array_push($careers_product_ing_linea,$productLinea);
//        }
//        if(strpos($productLinea->post_name,"diplo") !== false){
//            array_push($careers_product_dip_linea,$productLinea);
//        }
//        if(strpos($productLinea->post_name,"maest") !== false){
//            array_push($careers_product_mtria_linea,$productLinea);
//        }
//    }
//
//    $all_linea_careers = [];
//    $all_linea_careers['licenciatura'] = $careers_product_lic_linea;
//    $all_linea_careers['ingenieria'] = $careers_product_ing_linea;
//    $all_linea_careers['diplomado'] = $careers_product_dip_linea;
//    $all_linea_careers['maestria'] = $careers_product_mtria_linea;
//
//
//$all_careers_product = [];
//foreach($product_categories as $key => $category_object) {
//    // print_r($category_object);
//    $args_menu = array(
//        'post_type'             => 'product',
//        'post_status'           => 'publish',
//        'ignore_sticky_posts'   => 1,
//        'posts_per_page'        => '-1',
//        'fields'        =>      array('ID', 'post_title', 'post_name', 'guid'),
//        'orderby'       =>     'post_title',
//        'order' => 'ASC',
//        'tax_query' => array(
//            array(
//                'taxonomy' => 'product_cat',
//                'field'    => 'term_id',
//                'terms'    => $category_object->term_id,
//            )
//        ),
//    );
//    $products_la_prueba = query_posts($args_menu);
//    // array_push($all_careers_product, $category_object->name );
//    $all_careers_product[$category_object->name] = $products_la_prueba;
//    // print_r ($products_la_prueba);
//}

wp_reset_query();
// print_r ($all_careers_product);
//$all_careers_product=json_encode($all_careers_product);
//$all_linea_careers=json_encode($all_linea_careers);
// $marcaHtml = "";
// foreach ($products as $key => $value) {
//     $marcaHtml .= "<li>".$value->post_title."</li>";
// }

// print_r ($marcaHtml);
if($post->post_name === 'testvocacional' ){
    $textoBtnHeader = 'REALIZA EL TEST';
}else{
    $textoBtnHeader = 'Solicitud de Admisi&oacute;n';
}

    switch ($post->post_name) {
        case 'testvocacional':
            $textoBtnHeader = 'REALIZA EL TEST';
            break;
        case 'trabajar-y-estudiar':
            $textoBtnHeader = 'Conoce los trabajos';
            break;
        
        default:
            $textoBtnHeader = 'Solicitud de Admisi&oacute;n';
            break;
    }
?>
<script>
    //var add_ons_menu = <?php //echo $all_careers_product; ?>//;
    //var add_ons_menu_linea= <?php //echo $all_linea_careers; ?>//;
</script>

<!-- elemento para dragable sidenav-->
<!-- SideNav UnitecSideNav button -->
<nav class="navbar fixed-top p-0 z-depth-0">
    <div class="menuSuperior w-100">
        <div class="optionMenuSuperior d-flex justify-content-end px-3">
            <a href="<?php echo get_home_url(); ?>/alumnos" title="Alumnos">Alumnos</a><span class="px-2">|</span>
            <a href="<?php echo get_home_url(); ?>/exalumnos" title="Exalumnos">Exalumnos</a><span class="px-2">|</span>
            <a href="<?php echo get_home_url(); ?>/profesores" title="Profesores" class="pr-1">Profesores</a>
        </div>
    </div>
    <div class="navCont d-flex align-items-center">
        
        <div class="col-xl-3 col-md-3 d-flex align-items-center">
            <a href="javascript:void(0);" class="btnMenuMobile d-flex align-items-center" title="Menú UNITEC">
                <i class="icon-u-menu"></i>
                <div class="txt-menu">
                    MEN&Uacute;
                </div>
            </a>
            <a href="<?php echo get_home_url(); ?>" class="logoMenu p-0 m-0 pl-3" title="Bienvenido a la UNITEC">
                <!-- <?php echo do_shortcode( '[icono nombre="'.sanitize_title("unitec").'"][/icono]' ); ?> -->
                <img src="<?php echo get_template_directory_uri() . "/assets/frontend_desktop/img/header/unitec.svg"; ?>" alt="LOGO UNITEC">
            </a>
        </div>
        
        <div class="col-xl-4 col-md-3">
            <div class="row">
                <div id="searchHead" class="col-xl-10 offset-xl-2 col-lg-10 offset-lg-2 col-md-12 offset-md-0">
                    <?php if( strpos($post->post_name,'calcula-tu-beca') === false ){ ?>
                        <?php 
                            //echo "<fancy-app-drawer></fancy-app-drawer>";                            
                        ?>
                    <!-- <div class="buscador-desk d-flex align-items-center">
                        <input type="hidden" class="inputSearch" name="_token" id="" value="">                    
                        <div class="buscador-desk-input d-flex w-100">
                           <input type="search" placeholder="Buscar..." id="">
                           <button type="submit" id="" class="microfono">
                             <?php //echo do_shortcode( '[icono nombre="'.sanitize_title("microfono").'"][/icono]' ); ?>
                           </button>
                           <a href="#" class="buscador-desk-clear"></a>
                        </div>
                    </div> -->

                    <?php } ?>  
                    <search-app
        :bus="bus"
        :formgroup="form"
        btn-id="modal_search">
        </search-app>
                </div>
            </div>
        </div>
        <div class="col-xl-5 col-md-6">
            <div class="btn-menu-head d-flex align-items-center justify-content-end">
                <?php if( strpos($post->post_name,'calcula-tu-beca') === false && strpos($post->post_name,'biblioteca-virtual') === false ){ ?>
                <div class="whats pr-4">
                    <a href="#!" class="searchNav d-flex align-items-center" title="Chat en línea" data-gtm-tr="chat" data-gtm-pos="intencion" data-gtm-location="header" data-gtm-etiqueta="con formulario" onclick="javascript:$zopim.livechat.window.show()" >
                        <?php echo do_shortcode( '[icono nombre="'.sanitize_title("chat").'" clase="d-flex align-items-center"][/icono]' ); ?>
                        Chat en línea
                    </a>
                </div>
                <?php } ?>
                <!-- <div class="whats pr-2">
                    Antes https://api.whatsapp.com/send?phone=5555555555
                    <a href="https://clxt.ch/unitecg" class="searchNav d-flex align-items-center" target="_blank" title="Whatsapp" data-gtm-tr="evWhats" data-gtm-accion="Intención" data-gtm-seccion="Header" data-gtm-depto="">
                        <?php echo do_shortcode( '[icono nombre="'.sanitize_title("whatsapp").'"][/icono]' ); ?>
                        <span>Whatsapp</span>
                    </a>
                </div> -->
                <div class="tel pr-1">
                    <div class="searchNav d-flex align-items-center">
                        <?php echo do_shortcode( '[icono nombre="'.sanitize_title("llamar-footer").'"][/icono]' ); ?>
                        <span class="telefono">800 7864832</span>
                    </div>
                </div>
                <div class="solicitud">

                    <?php
                        $urlBtn = 'onclick="openModal(\'#modal_frm_app\')"';
                    ?>

                    <!-- Icono de sobre para contacto Formulario -->
                    <?php if( strpos($post->post_name,'calcula-tu-beca') === false && strpos($post->post_name,'biblioteca-virtual') === false ){ ?>
                    <!-- Icono de Formulario -->
                    <a id="calculadoraModal" <?php echo $urlBtn; ?> class="btn btn-naranja-unitec z-depth-0 searchNav waves-effect waves-light d-flex align-items-center justify-content-center p-0" target="_blank" title="Solicitud de admisión" data-solicitud-location="Header"><?php echo $textoBtnHeader; ?></a>
                    <?php } ?>

                    <?php if( strpos($post->post_name,'biblioteca-virtual') !== false ) { ?>
                    <!-- btn Login Biblioteca Virtual -->
                    <a id="login-biblioteca" onclick="openModal('#modal_frm_biblioteca')" class="btn btn-naranja-unitec z-depth-0 searchNav waves-effect waves-light d-flex align-items-center justify-content-center p-0" title="Ingresar Biblioteca" style="display:none !important;">INGRESAR</a>
                    <!-- btn Login Biblioteca Virtual -->

                    <!-- btn Logout Biblioteca Virtual -->
                    <a id="logout-biblioteca" onclick="" class="btn btn-naranja-unitec z-depth-0 searchNav waves-effect waves-light d-flex align-items-center justify-content-center p-0 d-none" title="Cerrar sesión" style="display:none !important;">CERRAR SESI&Oacute;N</a>
                    <!-- btn Logout Biblioteca Virtual -->
                    <?php } ?>
                </div>
            </div>
        </div>            
    
    </div>
</nav>
<div id="UnitecBackSideNav" class="back-sidenav" style="position: fixed; z-index: 1500; width: 100%; height: 100%; background: rgba(0,0,0,0.3); left:-100%; top:0"></div> <!-- Propuesta de cierre de menú  AMC-->
<!-- Sidebar navigation -->
<ul id="UnitecSideNav" class="side-nav" style="display:none">
    <div id="side-nav-content" class="side-nav-content scrollbar-ripe-malinka">
        <?php $custom_imagen_horizontal = get_field( 'imagen_destacada_horizontal'); ?>        
        <!-- <div class="side-nav-header" style="background: linear-gradient( rgba(0,0,0,.4), rgba(0,0,0,.4) ), url('<?php echo $custom_imagen_horizontal['sizes']['thumbnail']; ?>'); background-size:cover;"> -->
        <div class="side-nav-header nav-fixed-logo"> 
            <div class="navContent navegador d-flex align-items-center justify-content-between">
                <a href="<?php echo get_home_url(); ?>" class="logoNav" title="Bienvenido a la  UNITEC">
                    <!-- <?php echo do_shortcode( '[icono nombre="'.sanitize_title("unitec").'"][/icono]' ); ?> -->
                    <img src='<?php echo get_template_directory_uri() . "/assets/frontend_desktop/img/header/unitec.svg"; ?>' alt="LOGO UNITEC">
                </a>
                <!-- <span onClick="closeSideNav()" class="icon-u icon-u-cerrar pull-right"></span> -->
                <span onClick="closeSideNav()" class="btnCerrar mr-4" title="Cerrar"><?php echo do_shortcode( '[icono nombre="'.sanitize_title("cerrar").'" clase="logoMenu"][/icono]' ); ?></span>
            </div>
        </div>

        <ul class="collapsible collapsible-accordion nav-margin-logo">
            <?php
            wp_nav_menu(
                array('menu'=> 'sidenav',
                'walker'  => $sideNav //use our custom walker
                    )
            );
            ?>
        </ul>

    </div>
</ul>

<!-- El tamaño de la tipografía es de 12px ya lo revise con Thania -->
<!-- El tamaño de los iconos es de 18px ya lo revise con Thania -->


<!--  <a id="login-biblioteca" onclick="openModal('#modal_frm_biblioteca')" class="searchNav d-none"><?php echo do_shortcode( '[icono nombre="'.sanitize_title("login").'"][/icono]' ); ?></a>
      <a id="logout-biblioteca" onclick="cerrarSesion()" class="searchNav d-none"><?php echo do_shortcode( '[icono nombre="'.sanitize_title("logout").'"][/icono]' ); ?></a> -->