<?php 
    /**
     * WP Post Template: desk_32_footer_landing
     */
?>

<?php
    $urlSVG = get_template_directory_uri() . "/assets/frontend/img/footer/";

    /*Implementacion Custom field para Formulario*/
    $id_page = get_the_ID();
    $thumbID = get_post_thumbnail_id( $id_page );
    $urlImg = wp_get_attachment_url( $thumbID );
    $altImg = get_post_meta($thumbID, '_wp_attachment_image_alt', true);

    $contenido_multimedia = get_field('url' ,$theID);
    /*Definir una imagen en caso de no contar ni con Imagen Y Video Destacado*/
    /*End Implementacion Custom field para Formulario*/

    /*Se obtiene el SKU del producto asignado en el backoffice de wordpress (Grupo Carreras)*/
    global $woocommerce, $product, $post;
    $sku = get_post_meta($product->id); 
    $sku_id = $sku['_sku'][0];
    /*End Se obtiene el SKU del producto asignado en el backoffice de wordpress (Grupo Carreras)*/ 

    /*Se obtiene el tipo de post Type Page or Product*/
    $idPage = get_the_ID();
    $pageType = get_post_type( $idPage );
    /*End Se obtiene el tipo de post Type*/

    /*Se obtiene img Horizontal*/
    $img_horizontal = get_field('imagen_destacada_horizontal' ,$theID);
    $url_img_horizontal = $img_horizontal['url'];
    /*Se obtiene img Horizontal*/


    class footer extends Walker {

        /**
         * At the start of each element, output a <li> and <a> tag structure.
         * 
         * Note: Menu objects include url and title properties, so we will use those.
         */
        function start_el( &$output, $item, $depth = 0, $args = array(), $id = 0 ) {
            $elementoPadre = NULL;
    
            $output .= ( $depth > 0 ? str_repeat( "\t", $depth ) : '' ); // code indent
    
            // Passed classes.
            $classes = empty( $item->classes ) ? array() : (array) $item->classes;
            $class_names = esc_attr( implode( ' ', apply_filters( 'nav_menu_css_class', array_filter( $classes ), $item ) ) );
    
            if ( $item->menu_item_parent == 0 ) {
    
                if(empty($bandera)) {
                    $output .= '</ul>
                        </div>';
                }
    
                if( strrpos( $class_names, "menu-item-has-children") > -1 ){
                    $elementoPadre = $item->title;
                } else{
                    $elementoPadre = '<a title="'.$item->title.'" href="'.$item->url.'">'.$item->title.'</a>'; 
                }
    
                $output .= '<div class="col-sm-3">                           
                                   <span>';
                $output .= $elementoPadre;
    
                $output .= '</span>';
    
                $output .= '<ul>';
                $bandera = 0;
            }
    
            if( strrpos( $class_names, "menu-item-has-children") != -1 && $item->menu_item_parent != 0 ) {
                $output .= '<li><a title="'.$item->title.'" href="'.$item->url.'">'.$item->title.'</a></li>';
                $bandera = 1;
            }
        }
    
    }
     
    ?>


<!--Footer-->
<footer class="pl-5 pr-5">

    <input type="hidden" name="h_url_multimedia_formulario" id="h_url_multimedia_formulario" value='<?php echo $contenido_multimedia; ?>'>
    <input type="hidden" name ="h_url_imagen_destacada" id="h_url_imagen_destacada" value='<?php echo $urlImg; ?>'>
    <input type="hidden" name ="h_horizontal_url_imagen_destacada" id="h_horizontal_url_imagen_destacada" value='<?php echo $url_img_horizontal; ?>'>
    <input type="hidden" name="h_id_producto" id="h_id_producto" value='<?php echo $sku_id; ?>'>
    <input id="h_prellenado_formulario_pagina" type="hidden" value="true">
    <input id="h_prellenado_formulario_tipo" type="hidden" value="<?php echo $pageType; ?>">
    <input type="hidden" id="h_tab-contiene-video" name="h_tab-contiene-video" value="N">
    <input type="hidden" id="h_video-activo" name="h_video-activo">

</footer>
<!--/.Footer-->

<!--Copyright-->
<section class="w-100" style="height:4px; background-color:#006fba;"></section>
<div class="container-fluid">
  <div class="row pt-1 pl-5 pr-5">
    <div class="col-sm-8 term-pol p-0">
        <ul class="d-flex flex-wrap mt-0">
            <li><a title="Términos y condiciones" class="terminos" href="//www.unitec.mx/terminos-y-condiciones/">Términos y condiciones</a> |&nbsp; </li>
            <li><a title="Políticas de privacidad" class="terminos" href="//www.unitec.mx/politicas-de-privacidad/">Políticas de privacidad</a> |&nbsp; </li>
            <li><a title="Comunidad UNITEC" class="terminos" href="https://comunidad.unitec.mx/">Comunidad UNITEC</a> |&nbsp; </li>
            <li><a title="Portal Administrativo" class="terminos" href="https://my.laureate.net/pages/fbalogin.aspx?ReturnUrl=%2fstaff%2fPages%2fhome.aspx">Portal Administrativo</a></li>
        </ul>
    <!-- | 
        <a href="//www.unitec.mx/pago-seguro/">Pago Seguro</a><br> -->
        <!-- <div class="text-legal">
            <p>Acuerdo Secretarial 142 de la SEP de la fecha 24 de Octubre de 1988 <br> Universidad Tecnológica de México. Derechos Reservados 2017 UNITEC. Avenida Parque Chapultepec No. 56, Piso 1, Col. El Parque. Mun. Naucalpan de Juarez, EdoMéx., 53398. Laureate Education Inc.</p>
        </div> -->
    </div>
    <div class="col-sm-4">
        <div class="text-center d-flex justify-content-between align-items-center">
            <a href="" class="sponsors" title="esr">
                <img class="img-spons lazy" src="<?php echo $urlSVG . "svg/esr.svg" ?>" alt="esr" />
            </a>
            <a href="" class="sponsors" title="laureate">
                <img class="img-spons lazy" src="<?php echo $urlSVG . "svg/laureate.svg" ?>" alt="laureate" />
            </a>
            <a href="" class="sponsors" title="amipci">
                <img class="img-spons lazy" src="<?php echo $urlSVG . "svg/amipci.svg" ?>" alt="amipci" />
            </a>
            <a href="" class="sponsors" title="fimpes">
                <img class="img-spons lazy" src="<?php echo $urlSVG . "svg/fimpes.svg" ?>" alt="fimpes" />
            </a>
            <a href="" class="sponsors" title="geotrust">
                <img class="img-spons lazy" src="<?php echo $urlSVG . "svg/geotrust.svg" ?>" alt="geotrust" />
            </a>
        </div> 
    </div>  
  </div>
  <div class="row pt-1">
    <div class="text-legal pl-5 pr-4">
        <p>Acuerdo Secretarial 142 de la SEP de la fecha 24 de Octubre de 1988 <br> Universidad Tecnológica de México. Derechos Reservados 2018 UNITEC. Avenida Parque Chapultepec No. 56, Piso 1, Colonia El Parque. Municipio Naucalpan de Juárez, Estado de México., 53398. Laureate Education Inc.</p>
    </div>
  </div>
</div>
<!--/.Copyright-->   
<!-- /* termina coopia*/ -->

