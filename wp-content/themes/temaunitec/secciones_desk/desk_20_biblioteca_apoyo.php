<?php
/**
 * WP Post Template: desk_20_biblioteca_apoyo
 */
?>

<?php
    $iconoApoyo = get_field('desk_IconoApoyo', $theID);
?>

<div id="biblioteca_apoyo"></div>
<?php //if( !empty( $_COOKIE['c_matricula'] ) || $_COOKIE['c_matricula'] != NULL ) { ?> 
<div id="seccion-biblioteca-apoyo" class="contenido-apoyo d-flex" style="display:none !important;"><!-- Background -->
    <main class="container-fluid wow fadeIn d-flex justify-content-center align-items-center">
        <!-- Live preview-->
        <div class="col-lg-11 justify-content-center mt-2 mb-2">

            <article class="col-12 text-center mb-1 d-flex justify-content-center" data-wow-offset="50">
                <h4 class="w-75 titleSection"><?php echo get_field('titulo_card_materiales', $theID); ?></h4>
            </article>

            <!-- Descripción de sección del producto -->
            <article class="col-12 text-center mb-2 mt-0 d-flex justify-content-center" data-wow-offset="50">
                <h6 class="w-100 descriptionSection"><?php echo get_field('contenido_materiales_apoyo', $theID); ?></h6>
            </article>
            <!-- Descripción de sección del producto -->

            <!-- Botones Biblioteca-->
            <div class="contenedor-cards ml-3 mr-3 justify-content-center">            
                <!-- Card deck -->
                <div class="row">
                    <?php 
                        if( have_rows('contenido_collapsable_materiales') ){ // check if the repeater field has rows of data
                            while ( have_rows('contenido_collapsable_materiales') ) { the_row(); // loop through the rows of data
                    ?>                 
                    <!-- Card -->
                    <div class="col-xl-3 col-lg-3 col-md-6 col-sm-12 mt-md-3 mt-sm-4 materiales-cards pl-2 pr-2">
                        <div class="card p-3 h-100">
                            <div class="contenido-titulo-material card-body">
                                <!--Title-->
                                <h4 class="titulo-material"><?php echo get_sub_field('titulo_collapsable_materiales'); ?></h4>
                                <!--Text-->
                            </div>
                            <div class="row">
                                <?php while ( have_rows('link_del_material') ) { the_row(); ?>
                                <div class="material-links col-12 d-flex align-items-center pb-1">
                                    <span><?php echo do_shortcode( '[icono nombre="'.sanitize_title( $iconoApoyo ).'"][/icono]' ); ?></span>
                                    <a href="<?php echo get_sub_field('link_material'); ?>" target="_blank" class="pl-2" target="_blank" title="<?php echo get_sub_field('titulo_del_link_material'); ?>"><?php echo get_sub_field('titulo_del_link_material'); ?></a>
                                </div>                            
                                <?php } /*End While Links*/ ?>
                            </div>
                        </div>
                    </div>
                    <!-- Card -->
                    <?php 
                        } /*End While collapsable*/
                    } /*End if collapsable*/
                    ?>
                </div>
                <!-- Card deck -->
            </div>
            <!-- Botones Biblioteca-->
        </div>
        <!-- /.Live preview-->
    </main><!-- /.Main layout-->
</div><!-- Background -->
<?php //} ?>
