<?php
/**
 * WP Post Template: desk_2_3_banner_carrusel
 */
?>

<?php
 $field = get_field_object('tipo_de_banner');
 $value = $field['value'];
 $label = $field['choices'][ $value ];

 /*Se insertan los plugins que necesita la seccion tal y como se llama el archivo*/
//load_script( $pluginsNecesarios = array('mob_2_banner_home') );
/*End Se insertan los plugins que necesita la seccion tal y como se llama el archivo*/

 switch ($value) {
     case 'wordpress':
         $banner = "wordpress";
     break;

     case 'dfp':
         $banner = "dfp";
     break;
     
     default:
         $banner = "wordpress";
     break;
 }
/* Consultamos el Id del post para Obtener su Titulo */
$desk_idPost = get_the_ID();
$desk_postType = get_post($idPost);
/* Consultamos el Id del post para Obtener su Titulo */

/* Declaración de variables */
$activo = null;
/* Bandera para contabilizar el total de Slides */
$noSlider = 0;
$i = 0;
/* End Bandera para contabilizar el total de Slides */
$bannerimgCarrusel = null;
$desk_bannerimgCarrusel = null;
$desk_bannerbtnCarrusel = null;
$permalink = get_permalink($desk_idPost);
/* End Declaración de variables */

$desk_callToActionBannerEstatico = $grupoBannerContenido['desk_call_to_action_banner_estatico'];
$desk_urlCallToActionBannerEstatico = $grupoBannerContenido['desk_url_call_to_action_banner_estatico'];

/* Obtenemos las imagenes del Slider y las guardamos en un Arreglo */
if (have_rows("contenido_del_slider_carrusel", $theID)) {
    while (have_rows('contenido_del_slider_carrusel')) {the_row();
        $bannerimgCarrusel = get_sub_field('desk_group_banner_carrusel');

        $desk_bannerimgCarrusel[$i] = array($bannerimgCarrusel['desk_banner_carrusel']['url'], $bannerimgCarrusel['desk_banner_carrusel']['alt']);
        $desk_bannerbtnCarrusel[$i] = array($bannerimgCarrusel['desk_call_to_action_banner_carrusel'], $bannerimgCarrusel['desk_url_call_to_action_banner_carrusel'], $bannerimgCarrusel['desk_color_del_banner_carrusel']);
        $i++;
    }
}
$wordpress = false;
/* End Obtenemos las imagenes del Slider y las guardamos en un Arreglo */
?>

<!--Carousel Wrapper-->
<?php if ($banner == "wordpress") {
        /*Se insertan los plugins que necesita la seccion tal y como se llama el archivo*/
        load_script( $pluginsNecesarios = array('mob_2_banner_home') );
        /*End Se insertan los plugins que necesita la seccion tal y como se llama el archivo*/
?>
<section id="carousel-banner" class="section-banner carousel slide carousel-fade carousel-banner" data-ride="carousel">
    <!--Slides-->
    <div class="carousel-inner z-depth-0" role="listbox">
        <?php
        $i = 0;
        if (have_rows("contenido_del_slider_carrusel", $theID)) {
            while (have_rows('contenido_del_slider_carrusel')) {the_row();
            if ($i == 0) {$activo = "active";
                $varh1Open = "<h1 class='" .$desk_bannerbtnCarrusel[$i][2]. " title-banner m-0'>";
                $varh1Close = "</h1>";
            }
        ?>
        <!-- First slide -->
        <div class="bann_wind item-banner carousel-item <?php echo $activo; ?> carousel-fade lazy" data-src="<?php echo $desk_bannerimgCarrusel[$i][0]; ?>" id="desk_banner-item-slider-<?php echo $i; ?>" alt="<?php echo $desk_bannerimgCarrusel[$i][1]; ?>">
            <div class="view w-100"><!-- hm-black-light -->
                <div class="mask container d-flex align-items-center">
                    <div class="row pl-5 col-12">
                        <div class="col-md-6">
                            <div class="hs1 d-flex align-items-center">
                                <?php echo $varh1Open; ?><?php echo $desk_postType->post_title; ?><?php echo $varh1Close; ?>

                                <?php if( strpos($post->post_name,'landing') === false && strpos($post->post_name,'prueba-performance') === false ){ ?>
                                <!--Social buttons-->
                                <div class="banner-share d-inline-block ml-2">
                                    <div class="social-reveal">
                                        <?php echo do_shortcode('[desktop_share cerrar="banner_carrusel" pagina="' . $permalink . '" clase="icon-share-banner" tipo="inline-ul" seccion="Banner: '.get_sub_field('descripcion_del_slider').'"]'); ?>
                                    </div>
                                    <!-- Validación para Imprimr el icono de Compartir -->
                                    <?php if (strlen(trim($permalink)) > 3) {?>
                                    <a title="Compartir" class="share-button"><?php echo do_shortcode('[icono nombre="' . sanitize_title("compartir") . '"][/icono]'); ?></a>
                                    <?php }?>
                                </div>
                                <!--/Social buttons-->
                                <?php } ?>
                            </div>

                            <!-- Caption -->
                            <h2 class="<?php echo $desk_bannerbtnCarrusel[$i][2]; ?> descripcion-banner mt-3"><?php echo get_sub_field('descripcion_del_slider'); ?></h2>
                            <h3 class="texto-banner-carousel <?php echo $desk_bannerbtnCarrusel[$i][2]; ?> subtitulo-banner"><?php echo get_sub_field('subtitulo_del_slider'); ?></h3>
                            <?php if (strlen(trim($desk_bannerbtnCarrusel[$i][1])) > 3 &&
                                strlen(trim($desk_bannerbtnCarrusel[$i][0])) > 3
                                     )
                            {   if (strpos($desk_bannerbtnCarrusel[$i][1], 'javascript') !== false) {
                            ?>
                                <a onclick="<?php echo $desk_bannerbtnCarrusel[$i][1]; ?>" <?php if($post->post_name === 'testvocacional' ){echo 'data-solicitud-location="Banner"';} ?> class="btn back-naranja-unitec btn-warning mx-auto waves-effect waves-light" title="<?php echo $desk_bannerbtnCarrusel[$i][0]; ?>" id="btn-carousel-<?php echo $i; ?>"><?php echo $desk_bannerbtnCarrusel[$i][0]; ?></a>
                            <?php } else {?>
                                <a href="<?php echo $desk_bannerbtnCarrusel[$i][1]; ?>" class="btn back-naranja-unitec btn-warning mx-auto waves-effect waves-light" title="<?php echo $desk_bannerbtnCarrusel[$i][0]; ?>" id="btn-carousel-<?php echo $i; ?>"><?php echo $desk_bannerbtnCarrusel[$i][0]; ?></a>
                            <?php } }?>
                            <!-- Caption -->

                        </div>
                    </div>
                </div>
            </div>
            <!-- Caption -->

        </div>
        <!-- First slide -->
        <?php
            $i++;
            $noSlider++;
            $activo = null;
            $varh1Open = "<h2 class='" .$desk_bannerbtnCarrusel[$i][2]. " title-banner m-0'>";
            $varh1Close = "</h2>";
        }
    }
    ?>
    </div>
    <!--/.Slides-->
    <!--Indicators-->
    <ol class="custom-indicadores carousel-indicators indicadores">
        <?php for ($j = 0; $j < $noSlider; $j++) {
        if ($j == 0) {$activo = "active";}
        ?>
            <li data-target="#carousel-banner" data-slide-to="<?php echo $j ?>" class="<?php echo $activo; ?>"></li>
        <?php $activo = null;
    }?>
    </ol>
    <!--/.Indicators-->
</section>
<?php } else { ?>

        <script async type='text/javascript'>
            var Linea = undefined;
            var mySpreadsheet = undefined;
            var urldfp = undefined;
            var TituloPost = "<?php echo $desk_postType->post_title; ?>";
            function impresion_dfp(linea, spreadsheet, url, urldfp){
                //console.log(url);
                var self = this;
                $.get("<?php echo get_template_directory_uri(); ?>/secciones_desk/desk_2_9_banner_carrusel_DFP.php?titlePost= "+ TituloPost + "&urlDFP=" + urldfp, (data) => {
                        $(".result").html( data );
                    
                    Linea = linea;
                    mySpreadsheet = spreadsheet;
                    $ = jQuery;
                    //console.log("Ejecutando extraer con " + Linea);
                    imprimeBanner(Linea);
                });
            };
            var googletag = googletag || {};
            googletag.cmd = googletag.cmd || [];
            (function() {
                var gads = document.createElement('script');
                gads.async = true;
                gads.type = 'text/javascript';
                var useSSL = 'https:' == document.location.protocol;
                gads.src = (useSSL ? 'https:' : 'http:') +
                    '//www.googletagservices.com/tag/js/gpt.js';
                var node = document.getElementsByTagName('script')[0];
                node.parentNode.insertBefore(gads, node);
            })();
            
            googletag.cmd.push(function() {
                googletag.defineSlot('/7847748/BHUNITEC', [1024, 768], 'div-gpt-ad-1541092674189-0').addService(googletag.pubads());
                googletag.pubads().enableSingleRequest();
                googletag.enableServices();
                //console.log("Añadiendo el event");
            });

        </script>

        <script type="text/javascript" async src="<?php echo get_template_directory_uri(); ?>/assets/frontend/js/vendor/min/jqueryLibrariesDFP.min.js"></script>
        <script type="text/javascript" async src="<?php echo get_template_directory_uri(); ?>/assets/frontend_desktop/js/secciones/min/desk_2_9_banner_carrusel_DFP.min.js"></script>

        <!-- <div id='div-gpt-ad-1541092674189-0' class='iframedfp'>
            <script type='text/javascript'>
                googletag.cmd.push(function() { googletag.display('div-gpt-ad-1541092674189-0'); });
            </script>
        </div> -->
        <div class="result iframedfp" id='div-gpt-ad-1541092674189-0'>
            <script type='text/javascript'>
                googletag.cmd.push(function() { googletag.display('div-gpt-ad-1541092674189-0'); });
            </script>
        </div>
    
<?php }?>
<span id="start-page"></span>