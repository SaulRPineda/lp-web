<?php
/**
 *
 */
?>

<?php
    /*Obtenemos el Titulo del Post Por GET BY SRP 29-08-2018*/
    $postTitleDFP = $_GET['titlePost'];
    $urlDFP = $_GET['urlDFP'];
    /*End Obtenemos el Titulo del Post Por GET BY SRP 29-08-2018*/
?>

<section id="carousel-banner" class="margen-carrusel carousel slide carousel-fade carousel-banner" data-ride="carousel">
    <!--Slides-->
    <div id="bannerDinamico" class="carousel-dfp carousel-inner z-depth-0" role="listbox">

        <!-- First slide -->
        <script id=hr-template type=text/x-handlebars-template>
        <input type="hidden" name="nid" id="nid" value="{{cells.id}}">
        <div id="backImage" class="bann_wind item-banner carousel-item animated fadeIn lazy carousel-item-dfp" alt="<?php //echo $desk_bannerimgCarrusel[$i][1]; ?>" style="background-image: url({{cells.imagenFondoDesktop}});" link-mobile-vertical="{{cells.imagenFondoMobileVertical}}" link-mobile-horizontal="{{cells.imagenFondoMobileHorizontal}}">
            <div class="view w-100 position-dfp"><!-- hm-black-light -->
                <div class="mask container full-bg-img d-flex align-items-center justify-content-center p-0 px-md-2">
                    <div class="row col-12 p-0 pl-lg-5">
                        <div class="col-lg-6 col-md-12 col-sm-12 p-0 px-lg-2 cont-banner-mob">
                            <h1 id="titlePostDFP" class="title-banner cintillo titulo-mobile"><span class="vineta-orange {{cells.ColorTextDesk}}"><?php echo $postTitleDFP; ?></span>
                            <h1 id="titlePostDFP" class="title-banner-dfp cintillo titulo-desktop"><span class="vineta-orange {{cells.ColorTextDesk}}">{{cells.titular}}</span>
                            <br class="d-md-none">                            
                            </h1>
                            <div class="position-mob w-100 mt-lg-2">
                                <!-- Caption -->
                                <h2 class="{{cells.ColorTextDesk}} descripcion-banner descripcion-mobile">{{cells.titular}}</h2>
                                <h3 class="texto-banner-carousel {{cells.ColorTextDesk}} subtitulo-banner">{{cells.cuerpo}}</h3>
    
                                <a class="btn back-naranja-unitec btn-warning mx-auto waves-effect waves-light" name="btn_DFP" id="btn_DFP" href='<?php echo $urlDFP; ?>{{cells.urlBoton}}' target="_blank">{{cells.textoBoton}}</a>
                                <!-- Caption -->
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- Caption -->
            <!-- <div class="full-bg-img"></div> -->
        </div>
        </script>
        <!-- First slide -->

    </div>
    <!--/.Slides-->
    <!--Indicators-->
    <ol id="ol-dfp" class="custom-indicadores carousel-indicators indicadores m-0 p-0"></ol>
    <!--/.Indicators-->
    <a href="#start-page" class="scroll-spy btnB btn-banner-estatico"><i class="icon-u icon-u-ver-abajo "></i></a>
</section>
<span id="start-page"></span>