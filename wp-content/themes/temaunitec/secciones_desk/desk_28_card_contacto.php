<div id="becas"></div>
<?php
/**
 * WP Post Template: desk_28_card_contacto
 */
?>

<?php
// $desk_idPost = get_the_ID();
// $desk_postType = get_post($idPost);


$active = NULL;
$claseInversa = "card-inversa";
$classPositionCard = "card-right";

$conteoTituloContacto = count(get_field('titulo_contacto_carrusel'));
if ($conteoTituloContacto == 0)
    $clseTituloContacto = "d-none";
else
    $clseTituloContacto = NULL;

$conteoDescripcionContacto = count(get_field('descripcion_contacto_carrusel'));
if ($conteoDescripcionContacto == 0)
    $clseDescripcionContacto = "d-none";
else
    $clseDescripcionContacto = NULL;

$conteoTitDescContacto = $conteoTituloContacto + $conteoDescripcionContacto;
if ($conteoTitDescContacto == 0)
    $titDescContactoClase = "d-none";
else
    $titDescContactoClase = NULL;

$conteoTabsContacto = count(get_field('tabs_contacto_carrusel'));
if($conteoTabsContacto == 1)
    $tabsContactoClase = "d-none";
else
    $tabsContactoClase = NULL;
?>


<?php if(have_rows('tabs_contacto_carrusel',$theID) ) { ?>
<section class="tabs-cardsbullets d-flex align-items-center w-100"><!-- d-flex align-items-center -->
    <div class="container-fluid wow fadeIn d-flex justify-content-center w-100">
        <div class="col-lg-12">
            <div class="row justify-content-center <?php echo $titDescContactoClase; ?>"> <!-- Contenedor de encabezado de la sección -->
            <article class="col-10 text-center <?php echo $clseTituloContacto; ?>" data-wow-offset="50">
                    <h4 class="w-75 titleSection mx-auto"><?php echo get_field('titulo_contacto_carrusel', $theID); ?></h4>
                </article>
                <article class="col-10 text-center <?php echo $clseDescripcionContacto; ?>" data-wow-offset="50">
                        <h6 class="w-100 descriptionSection mt-1 mx-auto"><?php echo do_shortcode( get_field('descripcion_contacto_carrusel', $theID) ); ?></h6>
                </article>
            </div> <!-- Fin del contenedor de encabezado de la sección -->


            <!-- Tab Navegacion -->
            <section class="mb-1 <?php echo $tabsContactoClase; ?>" id="tabNav">
                <div class="tabs-wrapper">
                    <ul class="nav classic-tabs tabs-dinamicas justify-content-center" role="tablist">
                    <?php //if(have_rows('tabs_contacto_carrusel',$theID) ) { 
                            $noTabsDinamicos=0;  

                            while( have_rows('tabs_contacto_carrusel') ) { the_row(); 
                                if($noTabsDinamicos == 0){ $active = "active"; } 
                    ?>        
                        <li class="nav-item m-0"><!-- waves-effect waves-light -->
                            <a class="tab-dinamico-cardbullets waves-light <?php echo $active; ?> " data-toggle="tab" href="#tab-panel-unitec-<?php echo $noTabsDinamicos; ?>" role="tab" title="<?php echo get_sub_field('titulo_tab_card_contacto', $theID); ?>"><?php echo get_sub_field('titulo_tab_card_contacto', $theID); ?></a>
                        </li>
                        <?php   $noTabsDinamicos++; 
                                $active = NULL; 
                            } 
                        //} 
                    ?>
                    </ul>
                </div>
            </section>

            <div class="row w-100 m-0">
                <div class="col-1 d-flex align-items-center justify-content-center">
                    <!--Controls-->
                    <div class="controls-tabs-dinamicos d-flex justify-content-center">
                        <a class="btn-floating waves-effect waves-light btn-arrow carrousel_left_contacto_carrusel d-none" data-slide="next" title="Anterior">
                            <?php echo do_shortcode( '[icono nombre="'.sanitize_title("ver-izquierda").'" clase="arrow-right"][/icono]' ); ?>
                        </a>                        
                    </div>
                    <!--Controls-->
                </div>
                <div class="col-10 iconos-unitec justify-content-center">
                    <!--Grid row-->
                    <div class="row tab-content">
                        <?php 
                            $ll = 0; 
                            $panelContent = 0; 
                            $tabCount = 0;
                            $cardsCount = 0;
                            while( have_rows('tabs_contacto_carrusel') ) { the_row();
                                $active = NULL; 
                                if($panelContent == 0) { $active = "active"; }
                                ?>
                        <div class="carrousel_tabs_dinamicos w-100 tab-pane fade in show <?php echo $active; ?>" data-pos="0" id="tab-panel-unitec-<?php echo $tabCount; ?>"  role="tabpanel">
                            <div class="carrousel_inner_tabs_dinamicos">
                                <!-- Tab panels -->
                                <ul class="d-flex m-0" >
                                <?php
                                    while( have_rows('contenido_card_contacto') ) { the_row();
                                        $desk_TituloDinamico = get_sub_field('subtitulo_card_contacto_carrusel');
                                        $getCampusNo = get_sub_field('opciones_campus');
                                        switch ($getCampusNo) {
                                            case 'vacio':
                                                $campusNo = '';
                                                break;
                                            case 'atizapan':
                                                $campusNo = '340';
                                                break;
                                            case 'cuitlahuac':
                                                $campusNo = '342';
                                                break;
                                            case 'ecatepec':
                                                $campusNo = '219';
                                                break;
                                            case 'guadalajara':
                                                $campusNo = '348';
                                                break;
                                            case 'leon':
                                                $campusNo = '350';
                                                break;
                                            case 'marina':
                                                $campusNo = '352';
                                                break;
                                            case 'los-reyes':
                                                $campusNo = '1057';
                                                break;
                                            case 'queretaro':
                                                $campusNo = '354';
                                                break;
                                            case 'sur':
                                                $campusNo = '356';
                                                break;
                                            case 'toluca':
                                                $campusNo = '358';
                                                break;
                                            
                                            default:
                                                $campusNo = '';
                                                break;
                                        }
                                        $desk_ImagenoDinamico = get_sub_field('imagen_contacto_carrusel');

                                        if($panelContent%2 == 0){ $claseInversa = ""; $classPositionCard = "card-left"; } 
                                            if($tabCount == 0)
                                        ?>
                                    <!--Inicia Tab -->
                                    <li class="col-12 p-2 cardsbullets">
                                        <!--Card-->
                                        <div class="row m-0 justify-content-center d-flex <?php echo $claseInversa; ?>">
                                            <div class="col-lg-12 col-xl-7 p-0 d-flex align-items-center justify-content-center">
                                                <!--Card image-->
                                                <div class="overlay">
                                                    <img src="<?php echo $desk_ImagenoDinamico['url']; ?>" class="img-fluid rounded-0" alt="<?php echo $desk_ImagenoDinamico['alt']; ?>">
                                                </div>
                                                <!--/Card image-->
                                            </div>
                                            <div class="col-lg-12 col-xl-5 align-self-center">
                                                <!--Card content-->
                                                <div class="card-body card text-md-left <?php echo $classPositionCard; ?> p-4">
                                                    <div class="texto-int-card h-100 d-flex justify-content-center flex-column">
                                                        <h4 class="card-title-desk d-flex"><span class="filo-inicio-t pr-3">|</span> <?php echo $desk_TituloDinamico; ?></h4>
                                                        <!-- <h4 class="card-title-desk d-flex"><span class="filo-inicio-t pr-3">|</span> <?php echo $desk_postType; ?></h4> -->
                                                    </div>
                                                    <!-- Comienza implemantacio para agregar descripción de contacto by AMC -->
                                                    <?php   
                                                        $contenidoContactoInfo = get_sub_field( 'cardContactoCampus_carrusel' );
                                                        // print_r($contenidoContactoInfo);
                                                        foreach ($contenidoContactoInfo as $k => $datoContacto) {
                                                    ?>
                                                    <div class="simple-list">
                                                        <div type="button" class="row list-item-container border-top-fix">
                                                            <div class="col-2 flex-item item-icon list-icon d-flex align-items-center justify-content-center">
                                                                <span class="d-flex"><?php echo do_shortcode( '[icono nombre="'.sanitize_title( $datoContacto['iconoContactoCampus_carrusel'] ).'" clase="icon-size"][/icono]' ); ?></span>
                                                            </div>
                                                            <div class="col-10 item-description">
                                                                <span><?php echo $datoContacto['nombreContacto_carrusel']; ?></span>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <?php } ?>

                                                    <?php
                                                        $direccion_campus = get_field('direccion_del_campus' , $campusNo);
                                                        if( $direccion_campus != NULL && $direccion_campus !="" && strlen($direccion_campus) > 1 ) { 
                                                    ?>
                                                    <div class="simple-list">
                                                        <div type="button" class="row list-item-container border-top-fix">
                                                            <div class="col-2 item-icon d-flex align-center justify-content-center"><a href="//maps.google.es/?q=UNITEC, <?php echo get_field('direccion_del_campus' , $campusNo); ?>" title="<?php echo get_field('direccion_del_campus' , $campusNo); ?>" target="_blank" style="color:#757575;"><?php echo do_shortcode( '[icono nombre="'.sanitize_title("ubicacion").'" clase="d-flex"][/icono]' ); ?></a></div>
                                                            <div class="col-10 item-description"><a href="//maps.google.es/?q=UNITEC, <?php echo get_field('direccion_del_campus' , $campusNo); ?>" title="<?php echo get_field('direccion_del_campus' , $campusNo); ?>" target="_blank"><?php echo get_field('direccion_del_campus' , $campusNo); ?></a></div>
                                                        </div>
                                                    </div>
                                                    <?php } ?>

                                                    <?php
                                                        $telefonos = NULL;

                                                        $colTel = "10";

                                                        $contacto_telefonico = get_sub_field('contacto_telefonico_carrusel');
                                                        $telFijo = $contacto_telefonico['telefonos_carrusel']['numero_telefonico_carrusel'];
                                                        $telExtension = $contacto_telefonico['telefonos_carrusel']['extension_carrusel'];
                                                        $tipoTel = $contacto_telefonico['telefonos_carrusel'];
                                                        $cantidadTel = count($contacto_telefonico['telefonos_carrusel']);

                                                        // var_dump(count($contacto_telefonico['telefonos_carrusel']));

                                                        foreach ($contacto_telefonico['telefonos_carrusel'] as $key => $value) {
                                                            foreach ($value as $key => $numeros_telefonicos) {
                                                                $telefonos[] = $numeros_telefonicos;
                                                            }
                                                        }
                                                        $telExtension = $telefonos[1];
                                                        if ( $telExtension != "" ) {
                                                            $colTel = "5";
                                                        }
                                                    ?>

                                                    <?php if ( $cantidadTel > 0  ) { ?>
                                                    <div class="simple-list">
                                                        <div type="button" class="row list-item-container border-top-fix py-2" data-toggle="collapse" href="#tel-colapse-<?php echo $tabCount.$cardsCount;?>" role="button" aria-expanded="false" aria-controls="tel-colapse-<?php echo $tabCount.$cardsCount;?>">

                                                            <?php if ( $cantidadTel === 1 ) {
                                                                $enlaceInicio = '<a class="item-icon item-description list-icon d-flex align-items-center justify-content-center" title="teléfono:'.$telefonos[0].'">';
                                                                $enlaceInicioNum = '<a class="item-icon item-description list-icon d-flex" title="teléfono:'.$telefonos[0].'">';
                                                                $enlaceFin = '</a>';
                                                            } ?>

                                                            <div class="col-2 flex-item item-icon list-icon d-flex align-items-center justify-content-center"><?php echo $enlaceInicio; ?><?php echo do_shortcode( '[icono nombre="'.sanitize_title("llamar-footer").'" clase="arrow-left"][/icono]' ).$enlaceFin; ?></div>
                                                            <div class="col-<?php echo $colTel; ?> flex-item item-description item-description-arrow"><?php echo $enlaceInicioNum; ?><?php echo $telefonos[0].$enlaceFin; ?></div>
                                                            <?php if ($telExtension != "") { echo '<div class="col-4 flex-item item-description item-description-arrow">'.$enlaceInicioNum.$telefonos[1].$enlaceFin.'</div>'; } ?>
                                                            <?php if ( $cantidadTel > 1 ) { ?>
                                                            <div class="col-1 flex-item item-description d-flex align-items-center justify-content-center">
                                                                <div><?php echo do_shortcode( '[icono nombre="'.sanitize_title("ver-abajo").'" clase="d-flex"][/icono]' ); ?></div>
                                                            </div>                                        
                                                            <?php } ?>
                                                        </div>

                                                        <?php if ( $cantidadTel > 1 ) { ?>
                                                        <div class="text-colapse collapse multi-collapse card-int-float p-0" id="tel-colapse-<?php echo $tabCount.$cardsCount;?>">
                                                            <?php foreach ($tipoTel as $key => $value) { ?>
                                                            <a class="link-simple-list row" title="<?php echo $value['numero_telefonico_carrusel']; ?>">
                                                                <div class="col-2 my-1"></div>
                                                                <div class="col-5 my-1"><?php echo $value['numero_telefonico_carrusel']; ?></div>
                                                                <div class="col-5 my-1 pl-1"><?php echo $value['extension_carrusel']; ?></div>
                                                            </a>
                                                            <?php } ?>
                                                        </div>
                                                        <?php } ?>
                                                    </div>
                                                    <?php } //Valida si existen no telefónicos ?>

                                                    <?php
                                                    $horarios = get_sub_field('horarios_carrusel');
                                                    $dias_semana = array("Domingo", "Lunes", "Martes", "Miércoles", "Jueves", "Viernes", "Sábado", "Domingo");  
                                                    $horarios_semana = array_values( $horarios );
                                                    array_push( $horarios_semana, $horarios_semana[0] ); 
                                                    date_default_timezone_set("America/Mexico_City");
                                                    $dia = date('w');
                                                        if( !empty( $horarios['domingo'] ) ) { 
                                                    ?>
                                                    <?php if( count($dias_semana > 0) ) { ?>
                                                    <div class="simple-list">
                                                        <div type="button" class="row list-item-container border-top-fix" data-toggle="collapse" href="#horario-colapse-<?php echo $tabCount.$cardsCount;?>" role="button" aria-expanded="false" aria-controls="horario-colapse-<?php echo $tabCount.$cardsCount;?>">
                                                            <div class="col-2 flex-item item-icon list-icon d-flex align-items-center justify-content-center"><?php echo do_shortcode( '[icono nombre="'.sanitize_title("horarios").'"][/icono]' ); ?></div>
                                                            <div class="col-9 flex-item item-description">
                                                                <div class="row">
                                                                    <span class="col-3 horariosLine"><?php echo ucwords($dias_semana[$dia]); ?></span>
                                                                    <span class="col-9 horariosLine"><?php echo $horarios_semana[$dia]; ?></span>
                                                                </div>
                                                            </div>
                                                            <div class="col-1 d-flex align-items-center justify-content-center"><?php echo do_shortcode( '[icono nombre="'.sanitize_title("ver-abajo").'"][/icono]' ); ?></div>
                                                        </div>
                                                        <div class="text-colapse collapse multi-collapse card-int-float p-0" id="horario-colapse-<?php echo $tabCount.$cardsCount;?>">
                                                            <div class="horario pt-1">
                                                            <?php  for ($i = 1; $i < count($dias_semana); $i++) { ?>
                                                                <div class="row">
                                                                    <div class="col-2"></div> 
                                                                    <div class="col-9 flex-item item-description">
                                                                        <div class="row">
                                                                            <label class="col-3 horarios-int">
                                                                                <?php echo ucwords($dias_semana[$i]); ?>
                                                                            </label>
                                                                            <label class="col-9 horarios-int">
                                                                                <?php echo $horarios_semana[$i]; ?>
                                                                            </label>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            <?php } ?>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <?php
                                                        }
                                                    }
                                                    ?>
                                                    <!-- Comienza implemantacio para agregar contacto redes sociales -->
                                                    <?php   
                                                        $contenidoRedes = get_sub_field( 'cardContactoSociales_carrusel' );
                                                        //print_r($contenidoRedes);
                                                        foreach ($contenidoRedes as $key => $dato) {
                                                            $urlItem = $dato['urlContacto_carrusel'];
                                                            $emailItem = str_split($urlItem);
                                                            $tipoURL = "unitec.mx";
                                                            $mailto = '';
                                                            if (in_array("@", $emailItem)) {
                                                                $tipoURL = "@";
                                                                $mailto = 'mailto:';
                                                            }
                                                            $urlEnlace = ( strpos( strtolower( trim( $dato['urlContacto_carrusel'] ) ), $tipoURL )  );
                                                            $link = $urlEnlace ? 'href=' : 'onclick=';
                                                            $target = ( strpos( strtolower( trim( $dato['urlContacto_carrusel'] ) ), "unitec.mx" ) ) ? "target='_blank'" : "";
                                                            if ( strpos( strtolower( trim( $dato['urlContacto_carrusel'] ) ), "#modal_frm_app") ) {
                                                                $trackeoLocation =  'data-solicitud-location="Middle"';
                                                                $ocultarWhats = '';
                                                            } else if( strpos( strtolower( trim( $dato['urlContacto_carrusel'] ) ), "#modal-whatsapp")) {
                                                                $trackeoLocation =  'data-gtm-tr="evWhats" data-gtm-accion="intención" data-gtm-seccion="contacto" data-gtm-depto=""';
                                                                $ocultarWhats = 'd-none';
                                                            } else if( strpos( strtolower( trim( $dato['urlContacto_carrusel'] ) ), "zopim.livechat.window.show()")) {
                                                                $trackeoLocation =  'data-gtm-tr="chat" data-gtm-pos="intencion" data-gtm-location="Secci&oacute;n Contacto Carrusel" data-gtm-etiqueta="con formulario"';
                                                            } else {
                                                                $trackeoLocation = '';
                                                                $ocultarWhats = '';
                                                            }
                                                    ?>
                                                    <div class="<?php echo $ocultarWhats; ?> simple-list">
                                                        <div type="button" class="row list-item-container border-top-fix">
                                                            <div class="col-2 flex-item item-icon list-icon d-flex align-items-center justify-content-center">
                                                                <a class="d-flex" <?php echo $link.'"'.$mailto.trim($dato['urlContacto_carrusel']).' " '.$target ?> title="<?php echo $dato['descripcionContacto_carrusel']; ?>"><?php echo do_shortcode( '[icono nombre="'.sanitize_title( $dato['iconoContacto_carrusel'] ).'" clase="icon-size"][/icono]' ); ?></a>
                                                            </div>
                                                            <div class="col-10 item-description">
                                                                <a <?php echo $link.'"'.$mailto.trim($dato['urlContacto_carrusel']).' " '.$target ?> title="<?php echo $dato['descripcionContacto_carrusel']; ?>"><?php echo $dato['descripcionContacto_carrusel']; ?></a>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <?php } ?>
                                                    <!-- </div> -->


                                                </div>
                                                <!--/.Card content-->
                                            </div>
                                        </div>
                                        <!--/.Card--> 
                                    </li>
                                    <!-- Termina Tab -->
                                    <?php   
                                            if($tabCount == 0)
                                                $ll++;
                                            $claseInversa = "card-inversa";
                                            $classPositionCard = "card-right";
                                            $cardsCount++;
                                            $panelContent++;
                                                }     
                                    ?>
                                    <!-- Termina Tab -->
                                </ul>
                                <!-- //Tab panels -->
                            </div>
                        </div>
                        <?php
                            $tabCount++;
                        } 
                        ?>
                    </div>
                    <!-- //Grid row-->
                </div>
                <div class="col-1 d-flex align-items-center justify-content-center">
                    <!--Controls-->
                    <div class="controls-tabs-dinamicos d-flex justify-content-center">
                        <a class="btn-floating waves-effect waves-light btn-arrow carrousel_right_contacto_carrusel <?php if($cardsCount == 1) {echo "d-none";} ?>" data-slide="prev" title="Siguiente">
                            <?php echo do_shortcode( '[icono nombre="'.sanitize_title("ver-derecha").'" clase="arrow-left"][/icono]' ); ?>
                        </a>                       
                    </div>
                    <!--Controls-->
                </div>
            </div>

        </div>
    </div>
    <input type="hidden" name="totalTabsDinamicosContacto" id="totalTabsDinamicosContacto" value="<?php echo $ll - 1; ?>">
</section><!-- Background -->
<?php } ?>