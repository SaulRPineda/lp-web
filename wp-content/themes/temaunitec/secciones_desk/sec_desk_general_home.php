<?php
/**
 * WP Post Template: sec_desk_general_home
 */
?>
<!--Seccion General HOME-->

<?php
$original = wp_get_attachment_url( get_post_thumbnail_id($theID, "thumbnail"));
//Funcionalidad para JQuery lazy
$fileName = basename($original);
$fullPath = str_replace($fileName, "mob_".$fileName, $original);
?>

<section class="home_principal section">
    <article  class="home-banner" data-resize="65" style="position:relative;">
        <div class="img-principal"> <img class="lazy-sec-general-home img-responsive invertedImage" data-mobile-src="<?php echo $fullPath; ?>" data-src="<?php echo $original; ?>" alt="unitec_general_home" /></div>
            <div class="container-fluid textBanner valign-container">
            <div class="row valign-row">
<?php if (strpos(strtolower($_SERVER['REQUEST_URI']), 'respaldo-economico-unitec') !== false){ $colr = "8";}else{$colr="7";}?>
                <div class="col-md-<?php echo $colr; ?> col-sm-12 box floatRight">
                    <div class="col-sm-offset-1">
                        <div class="col-md-12 valign-col">
                            <header>
                                <h1 class="dark" style="display: inline-block;"><?php echo get_post($theID)->post_title ?></h1>

                                <a class="" href="javascript:showButtons(this);"><img src="/wp-content/uploads/2015/share-icon.png" width="48" height="42" border="0" alt="Share" /></a>
                                    <div id="addthisContainer" data-visible="no" class="addthis_toolbox addthis_default_style addthis_32x32_style" style="vertical-align: middle;display:inline-block">                        
                                        <a class="addthis_button_facebook"><img src="/wp-content/uploads/2015/facebook-icon.png" width="32" height="32" border="0" alt="Facebook" /></a>
                                        <a class="addthis_button_twitter"><img src="/wp-content/uploads/2015/twitter-icon.png" width="32" height="32" border="0" alt="Twitter" /></a>
                                        <a class="addthis_button_whatsapp"><img src="/wp-content/uploads/2015/watts-icon.png" width="32" height="32" border="0" alt="WhatsApp" /></a>
                                        <a class="addthis_button_email"><img src="/wp-content/uploads/2015/mesage-icon.png" width="32" height="32" border="0" alt="Email" /></a>
                                        <a class="addthis_button_more"><img src="/wp-content/uploads/2015/plus-icon.png" width="32" height="32" border="0" alt="More" /></a>
                                    </div>
                                <p class="dark justifyText">
                                <?php echo get_post($theID)->post_content; ?>
<?php if (strpos(strtolower($_SERVER['REQUEST_URI']), 'prepa') !== false ||
          strpos(strtolower($_SERVER['REQUEST_URI']), 'licenciaturas') !== false ||
          strpos(strtolower($_SERVER['REQUEST_URI']), 'ciencias-salud') !== false ||
          strpos(strtolower($_SERVER['REQUEST_URI']), 'ingenierias') !== false ||
          strpos(strtolower($_SERVER['REQUEST_URI']), 'ejecutivas') !== false ||
          strpos(strtolower($_SERVER['REQUEST_URI']), 'diplomados') !== false ||
          strpos(strtolower($_SERVER['REQUEST_URI']), 'maestrias') !== false ||
          strpos(strtolower($_SERVER['REQUEST_URI']), 'especialidades-odontologicas') !== false ||
          strpos(strtolower($_SERVER['REQUEST_URI']), 'revalidacion') !== false ||
          strpos(strtolower($_SERVER['REQUEST_URI']), 'nuestras-modalidades') !== false

          ){ ?>
                                <div class="row">
                                        <div class="col-md-12" style="margin-top: -50px;">
                                        
                                            <!-- IMPLEMENTACION DEL BUSCADOR -->
                                            <style type="text/css">
                                            <?php
                                                echo file_get_contents(get_template_directory_uri().'/componente_buscador/buscador.min.css');
                                            ?>
                                            </style>
                                            <?php
                                             echo file_get_contents(get_template_directory_uri().'/componente_buscador/buscador.html');
                                             ?>
                                            <!-- TERMINA IMPLEMENTACION DEL BUSCADOR --> 
                                       
                                    </div>
                                        <div class="col-md-12">
                                               <div class="row botones-informacion" style="margin-top: 20px; margin-left: 0px;"><a class="btn btn-primary frmtradicional " data-gtm-tr="frmTradicional" data-gtm-pos="Banner producto | banner">
                                                <i class="fa fa-envelope fa-2x hidden-lg hidden-md hidden-sm"></i>
                                                <i class="fa fa-envelope hidden-xs"></i> <span class="hidden-xs">Solicita información</span></a>
                                                 <a href="javascript:$zopim.livechat.window.show()" data-gtm-tr="chat" data-gtm-pos="seccion-home | banner" class="btn btn-secundary " >
                                                <i class="fa fa-comment fa-2x hidden-lg hidden-md hidden-sm"></i>
                                                <i class="fa fa-comment hidden-xs"></i> <span class="hidden-xs">Chatea con nosotros</span></a>
                                                <a class="btn btn-phone btn-phone-dsk-hidden" href="tel:018007864832" data-gtm-tr="call" data-gtm-pos="Home | Banner">
                                                <i class="fa fa-phone fa-2x hidden-lg hidden-md hidden-sm"></i>
                                                <i class="fa fa-phone hidden-xs"></i> <span class="hidden-xs">01 800 7 864 832</span>
                                                </a></div>
                                    </div>
                                </div>
   <?php } ?>
                                </p>
                            </header>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </article>
  <style type="text/css">
      #caja2 h3 a{
        color: white!important;
        text-decoration: none !important;
      }
      #caja2 p a{
        color: white!important;
      }
      #caja2 p {
        color: white!important;
      }
     #caja2 p a:hover{
        color: white!important;
        border-bottom: 2px solid white!important;
      }
      #caja4 p{
        color: #443939!important;
      }
      #caja4 h3 a{
        color: #443939!important;
        text-decoration: none !important;
      }
      #caja4 p a{
        color: #443939!important;
        text-decoration: none !important;
      }
      #caja4 p a:hover{
        color: #443939!important;
        text-decoration: none !important;
        border-bottom: 2px solid #443939!important;
      }
     #caja1 h3 a{
        color: #443939!important;
        text-decoration: none !important;
      }
      #caja1 p a{
        color: #443939!important;
        text-decoration: none !important;
      }
      #caja1 p a:hover{
        color: #443939!important;
        text-decoration: none !important;
        border-bottom: 2px solid #443939!important;
      }
  </style>
    <article data-resize="35">
        <div  id="caja1" class="col-sm-4 hidden-lg hidden-md  box naranja light  "><?php the_field('caja_3', $theID);?></div>
        <div  id="caja2" class="col-sm-4  box gris dark  "><?php the_field('caja_1', $theID);?></div>
        <div  id="caja3" class="col-sm-4  box azul light  "><?php the_field('caja_2', $theID);?></div>
        <div  id="caja4" class="col-sm-4  box naranja light   hidden-xs hidden-sm"><?php the_field('caja_3', $theID);?></div>

        <!--FLECHA SECCION ABAJO-->

        <div class="hidden-xs hidden-sm flecha-seccion">
            <!-- JEAB Se cambia la clase dark por light para estandarizar los estilos 21-04-2016-->
            <a href="#" class="light" onclick="jQuery.fn.fullpage.moveSectionDown();"><i class="fa fa-angle-down fa-3x"></i></a>
        </div>

        <!--FIN FLECHA SECCION ABAJO-->
<div class="limpiar"></div>
    </article>
</section>
<!--
<h1 class="clase_para_h1">Titulo de la seccion sec_desk_header</h1> -->