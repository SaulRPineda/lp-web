<?php
/**
 * 
 */
?>

<?php
    /*Obtenemos el Titulo del Post Por GET BY SRP 29-08-2018*/
    $postTitleDFP = $_GET['titlePost'];
    $urlDFP = $_GET['urlDFP'];
    /*End Obtenemos el Titulo del Post Por GET BY SRP 29-08-2018*/
?>

<div id=nids>
    <script id=hr-template-nids type=text/x-handlebars-template>
        <input type="text" name="nid_imp_{{cells.id}}" id="nid_imp_{{cells.id}}" value="{{cells.id}}" style="display:none;">
    </script>
</div>

<!--Carousel Wrapper-->
    <section id="carousel-banner" class="section-banner carousel slide carousel-fade carousel-banner" data-ride="carousel">
        <div class="carousel-inner z-depth-0" >
            <script id=hr-template type=text/x-handlebars-template>
            <input type="hidden" name="nid" id="nid" value="{{cells.id}}">
            <!-- First slide -->
                <div class="bann_wind item-banner carousel-item active carousel-fade lazy" id="desk_banner-item-slider-0{{cells.id}}" alt="<?php //echo $desk_bannerimgCarrusel[$i][1]; ?>" style="background-image: url({{cells.imagenFondoDesktop}});" link-mobile-vertical="{{cells.imagenFondoMobileVertical}}" link-mobile-horizontal="{{cells.imagenFondoMobileHorizontal}}">
                    <div class="view w-100"><!-- hm-black-light -->
                        <div class="mask container d-flex align-items-center justify-content-center p-0 px-md-2">
                            <div class="row col-12 p-0 pl-lg-5">
                                <div class="col-lg-6 col-md-12 col-sm-12 p-0 px-lg-2 cont-banner-mob">
                                    <h1 class="title-banner cintillo"><span class="{{cells.ColorTextDesk}} vineta-orange"><?php echo $postTitleDFP; ?></span>
                                    <br class="d-md-none">                            
                                        <!--Social buttons-->
                                        <div class="banner-share d-inline-block">
                                            <div class="social-reveal">                                        
                                                <?php //echo do_shortcode('[desktop_share cerrar="banner_carrusel" clase="icon-share-banner" tipo="inline-ul"]');?>
                                            </div>
                                            <!-- Validación para Imprimr el icono de Compartir -->
                                            <a class="share-button"><?php //echo do_shortcode( '[icono nombre="'.sanitize_title("compartir").'"][/icono]' ); ?></a>
                                        </div>
                                        <!--/Social buttons-->
                                    </h1>
                                    <div class="position-mob">
                                        <!-- Caption -->
                                        <h2 class="{{cells.ColorTextDesk}} descripcion-banner mt-lg-5">{{cells.titular}}</h2>
                                        <h3 class="texto-banner-carousel {{cells.ColorTextDesk}} subtitulo-banner">{{cells.cuerpo}}</h3>
            
                                        <a href='<?php echo $urlDFP; ?>{{cells.urlBoton}}' class="btn back-naranja-unitec btn-warning mx-auto waves-effect waves-light" >{{cells.textoBoton}}</a>
                                        <!-- Caption -->
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- Caption -->
                </div>
            </script>
            <!-- First slide -->
        </div>
        <a href="#start-page" class="scroll-spy btnB btn-banner-estatico"><i class="icon-u icon-u-ver-abajo "></i></a>
        <!--/.Slides-->
    </section>
<!--/.Carousel Wrapper-->
<div id="start-page"></div>
