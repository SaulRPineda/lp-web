<?php
/**
 * WP Post Template: desk_35_impulsa
 */
?>
<?php
/*Se insertan los plugins que necesita la seccion tal y como se llama el archivo*/
/*load_script( $pluginsNecesarios = array('masonry') );*/
/*End Se insertan los plugins que necesita la seccion tal y como se llama el archivo*/

$args = array(
    'post_type'             => 'vacantes-unitec',
    'post_status'           => 'publish',
    'ignore_sticky_posts'   => 1,
    'posts_per_page'        => -1
);
$descuentos = query_posts($args);
$actualDiscount = 0;
$categoryDiscount = [];
$categoryDiscountCampus = [];
$categoryDiscountContactData = [];
$categoryDiscountLinkData = [];
$field_key = NULL;
$categoryDiscounts = [];    
$json_descuentos = NULL;
$array_campus = [];

// print_r (get_fields());

$json_descuentos = '{';
//   print_r($descuentos);
foreach ($descuentos as $key => $descuento) {
    $json_descuentos .= '"'.$descuento->post_name.'":';

    $telefono = get_field('datos_de_contacto',$descuento->ID);


    $discount_categoria = get_field('categoria',$descuento->ID);
    $discount_campus = get_field('campus',$descuento->ID);
    $discount_logo = get_field('logotipo_de_la_empresa',$descuento->ID);
    // $discount_ubicacion = get_field('ubicacion_de_la_vacante',$descuento->ID);



    // vars
    $ubicacion_vacante = get_field_object('ubicacion_de_la_vacante',$descuento->ID);
    $value = $ubicacion_vacante['value'];
    $ubicacion = $ubicacion_vacante['choices'][ $value ];


    $discount_campus_object = get_field_object('campus',$descuento->ID);


    $discount_name = get_field('nombre_de_la_empresa',$descuento->ID);
    $discount_contacto = get_field('datos_de_contacto',$descuento->ID);
    $discount_links = get_field('links_de_contacto_descuentos',$descuento->ID);
    // $discount_caracteristica = get_field('caracteristica_del_descuento',$descuento->ID);
    $discount_terminos = get_field('terminos_condiciones',$descuento->ID);
    $discount_qr_link = get_field('qr_code_descuento',$descuento->ID);

    $discount_terminos = str_replace('"', '\\\\"', $discount_terminos); 
    $discount_terminos = str_replace("'", "\\'", $discount_terminos); 
    //$discount_terminos = preg_replace('"', '\"', $discount_terminos);

    $json_descuentos .= '{"title":"'.trim($descuento->post_title).'", "url":"'.trim($discount_logo).'", "categoria": "'.trim($discount_categoria).'", "terminos_condiciones": "'.trim($discount_terminos).'", "contacto": {';

    $array_campus[$descuento->post_name] =[];

    foreach ($discount_campus as $key => $campus) {
        $array_campus[$descuento->post_name].array_push($array_campus[$descuento->post_name], $campus);
        // $json_descuentos .= '"'.$key.'": ' . '';
    }

    foreach ($discount_contacto as $key => $datos_array) {
        $json_descuentos .= '"'.$datos_array['medio_de_contacto'].'": ' . '"'.$datos_array['datos'].'",';
    }

    if(count($discount_contacto)> 0){
        if(mb_substr($json_descuentos, -1) != "{") {
            $json_descuentos = substr($json_descuentos, 0, strlen($json_descuentos) - 1);
        }
    }
    $json_descuentos .= '}, "links": {';

    foreach ($discount_links as $key => $datos_array) {
        $json_descuentos .= '"'.$datos_array['link_de_contacto'].'": ' . '["'.$datos_array['texto_link'].'","'.$datos_array['link_contact'].'"],';
    }

    if(count($discount_links)> 0){
        if(mb_substr($json_descuentos, -1) != "{") {
            $json_descuentos = substr($json_descuentos, 0, strlen($json_descuentos) - 1);
        }
    }
    $json_descuentos .= '},';

    $json_descuentos .= '"qr-code-descuento":"'.$discount_qr_link.'"';
    $json_descuentos .= '},';


    if($actualDiscount == 0) {
        $field_key = get_post_meta( $descuento->ID, '_categoria', true);
        $field = get_field_object($field_key);
        $choices = $field['choices'];
        $categoryDiscount = $choices;
        $categoryDiscount = array("all-tipes" => "Ver todas las Vacantes") + $categoryDiscount;

        $field_key = get_post_meta( $descuento->ID, '_campus', true);
        $field = get_field_object($field_key);

        $choices = $field['choices'];
        // print_r($choices);
        $categoryDiscountCampus = $choices;
        $categoryDiscountCampus = array("all-campus" => "Ver todas las &Aacute;reas") + $categoryDiscountCampus;

        $field_key = get_post_meta( $descuento->ID, '_datos_de_contacto', true);
        $field = get_field_object($field_key);
        $choices = $field['sub_fields'][0]['choices'];
        // echo "<br><br>";
        // print_r($choices);
        $categoryDiscountContactData = $choices;

        $field_key = get_post_meta( $descuento->ID, '_links_de_contacto_descuentos', true);
        $field = get_field_object($field_key);
        $choices = $field['sub_fields'][0]['choices'];
        // echo "<br><br>";
        // print_r($choices);
        $categoryDiscountLinkData = $choices;
    }

    $categoryDiscounts.array_push($categoryDiscounts, $descuento->post_name);

    $actualDiscount++;
}

$json_descuentos = substr($json_descuentos, 0, strlen($json_descuentos) - 1);
$json_descuentos .= '}';

/* Limpia el JSON de caracteres que no son ASCII */
// echo $json_descuentos . '<br>';
// $json_descuentos = preg_replace('/[\x00-\x1F\x80]/', '', $json_descuentos);
$json_descuentos = preg_replace('/[\n]/', '<br />', $json_descuentos);
$json_descuentos = preg_replace('/[\r]/', '', $json_descuentos);
// $json_descuentos = nl2br($json_descuentos);

?>

<script>
    var discounts_json = '<?php echo $json_descuentos; ?>';
    var category_array_json = '<?php echo json_encode($categoryDiscount) ?>';
    var category_contact_array_json = '<?php echo json_encode($categoryDiscountContactData) ?>';
    var category_link_array_json = '<?php echo json_encode($categoryDiscountLinkData) ?>';
</script>

<?php
    // $json_descuentos = json_decode(preg_replace('/[\x00-\x1F\x80-\xFF]/', '', $json_descuentos), true); // Quita las letras con acentos, no usar si es en espa�ol
    /* Se usa striplashes para volver los double quotes en un string a la normalidad */
    $json_descuentos = json_decode(stripslashes($json_descuentos), true);
?>

<input type="hidden" id="galeria-activa" name="galeria-activa" value="<?php echo $galerias[0]->post_name; ?>">
<input type="hidden" id="galeria-name" name="galeria-name" value="<?php echo $galerias[0]->post_title; ?>">

<div id="vacantes"></div>

<section class="d-flex justify-content-center align-items-center impulsa"><!-- /.Main <layout-->
    <div class="col-lg-11 p-0 justify-content-center d-flex align-items-center contenedor-impulsa" id="galeria_catalogo"> <!-- Contenedor de la galer�a -->
        <div class="w-100 centrar-contenido pb-2">

            <div class="row"> <!-- Contenedor de encabezado de la secci�n -->
                <article class="col-12 text-center d-flex justify-content-center p-0 mt-1" data-wow-offset="50">
                    <h4 class="w-100 titleSection m-0">Vacantes Disponibles <?php //echo get_field('desk_tituloimpulsa'); ?></h4>
                </article>
                <article class="col-12 text-center py-3 d-flex flex-column justify-content-center" data-wow-offset="50">
                        <h6 class="w-100 descriptionSection m-0">Culpa adipisicing laborum proident consectetur exercitation mollit ex voluptate minim mollit tempor.
                </article>
            </div> <!-- Fin del contenedor de encabezado de la secci�n -->

            <div class="row filtros mb-1 d-flex justify-content-center">
                <div class="col-xl-8 col-md-8 col-sm-12">
                    <div class="dropdown">
                        <div class="drop-view filtro-descuentos d-flex align-items-center justify-content-center" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            <div class="col-11"><h4 id="nameCampusDescuentos" class="m-0" data-descuentos-campus="">Ver todas las vacantes</h4></div>
                            <div class="col-1 d-flex align-items-center justify-content-end"><?php echo do_shortcode( '[icono nombre="'.sanitize_title("ver-abajo").'" clase="icon-abajo"][/icono]' ); ?></div>
                        </div>
                        <div class="dropdown-menu dropdown-primary menu-filtro-descuentos">
                            <?php foreach ($categoryDiscountCampus as $key => $campus_discount) { ?>
                                <a class="dropdown-item destacar" onclick="filtroTipoCampus( '<?php echo $key ?>', '<?php echo $campus_discount; ?>' );" title="<?php echo $campus_discount; ?>"><?php echo $campus_discount; ?></a>
                            <?php
                            } ?>
                            <!-- <a class="dropdown-item destacar" onclick="filtroNoAnos( '<?php //echo $key ?>', '<?php //echo $opc_no_anos; ?>' );"><?php //echo $opc_no_anos; ?>PRIMERA OPCI�N</a>
                            <a class="dropdown-item destacar" onclick="filtroNoAnos( '<?php //echo $key ?>', '<?php //echo $opc_no_anos; ?>' );"><?php //echo $opc_no_anos; ?>SEGUNDA OPCI�N</a> -->
                        </div>
                    </div>
                </div>

                <!-- <div class="col-xl-6 col-md-6 col-sm-12">
                    <div class="dropdown">
                        <div id="" class="drop-view filtro-descuentos d-flex align-items-center justify-content-center p-2 pl-4 pr-4" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            <div class="w-75"><h4 id="nameCategoriasDescuentos" data-descuentos-categorias="">Ver todas las Categor�as</h4></div>
                            <div class="w-25 w-25 d-flex align-items-center justify-content-end"><?php echo do_shortcode( '[icono nombre="'.sanitize_title("ver-abajo").'" clase="icon-abajo"][/icono]' ); ?></div>
                        </div>
                        <div class="dropdown-menu dropdown-primary menu-filtro-descuentos">
                            <?php //foreach ($categoryDiscount as $key => $category_discount) { ?>
                                <a class="dropdown-item destacar" onclick="filtroTipoCategorias( '<?php echo $key ?>', '<?php echo $category_discount; ?>' );" title="<?php echo $category_discount; ?>"><?php echo $category_discount; ?></a>
                            <?php
                            //} ?>
                        </div>
                    </div>
                </div> -->
                <!-- <div class="col-xl-3 col-md-3 col-sm-6 offset-xl-9 offset-md-9 offset-sm-3 mt-1 mb-1">
                    <a href="<?php //echo $descarga; ?>" class="descargar d-flex align-items-center float-right" target="_blank">
                        <?php echo do_shortcode( '[icono nombre="'.sanitize_title("descargar").'" clase="descarga-folleto pr-2"][/icono]' ); ?>
                        FOLLETO DE BOLSILLO
                    </a>
                </div> -->
            </div>

            <section class="row d-flex justify-content-center fila-descuentos">
                <article class="col-12 text-center d-flex justify-content-center" data-wow-offset="50">
                    <h4 class="w-75 titulo-descuentos m-0" id="available-discounts" hidden>No hay descuentos disponibles</h4>
                </article>
            <?php
                $i = 0;
                foreach ($json_descuentos as $key_descuento => $descuento_array) { ?>

                <!--Grid column-->
                <div class="col-md-3 pb-1 item-<?php echo $key_descuento; ?> categoria-<?php echo $descuento_array['categoria']; ?> <?php foreach ($array_campus[$key_descuento] as $key_campus => $campus_value) { echo " campus-".$campus_value; $campus = $campus_value; }?> campus-all-campus categoria-all-tipes" id="<?php echo $key_descuento; ?>" name="<?php echo $key_descuento; ?>" title="<?php echo $descuento_array['title']; ?>" style="<?php if ($i++ >= 9) //echo "display:none"; ?>">
                    <!--Collection card-->
                    <div class="card collection-card z-depth-1-half">
                        <!--Card image-->
                        <a onclick="abreGaleriaDesc()" title="<?php echo $descuento_array['title']; ?>" data-gtm-tr="OurFac" data-gtm-facilidad="<?php echo $descuento_array['title']; ?>" data-gtm-interaccion="Descuentos">
                            <div class="view zoom portada-galeria-descuentos" id="<?php echo $key_descuento; ?>" name="<?php echo $key_descuento; ?>" title="<?php echo $descuento_array['title']; ?>">
                                <img data-src="<?php echo $descuento_array['url']; ?>" alt="portada galeria <?php echo $descuento_array['title']; ?>" class="w-75 mx-auto lazy">
                                <!-- <?php echo do_shortcode( '[icono nombre="'.sanitize_title("hospital-simulado").'"][/icono]' ); ?> -->
                                <div class="stripe d-flex flex-column align-items-left justify-content-center">
                                    <p class="px-3 py-1 galeria-title-<?php echo $key_descuento; ?>"><b><?php echo $descuento_array['title']; ?></b></p>
                                    <p class="px-3 py-1">&Aacute;rea: <b><?php echo $discount_campus_object['choices'][$campus]; ?></b></p>
                                    <p class="px-3 py-1">Empresa: <b><?php echo $discount_name; ?></b></p>
                                    <p class="px-3 py-1 mb-2"><?php echo wp_trim_words( $discount_terminos, 25 ); ?></p>
                                </div>
                            </div>
                        </a>
                        <div class="row collection-card m-0 dir">
                            <div class="col-1 p-1 d-flex align-items-center justify-content-center">
                                <span class="d-flex"><?php echo do_shortcode( '[icono nombre="'.sanitize_title("ubicacion").'"][/icono]' ); ?></span>
                            </div>
                            <div class="col-7 p-1 d-flex align-items-center justify-content-left"><?php echo $ubicacion; ?></div>
                            <div class="col-4 p-1"><a class="verMas" onclick="abreGaleriaDesc()" title="<?php echo $descuento_array['title']; ?>">VER M&Aacute;S</a></div>
                        </div>
                    </div>
                    <!--Collection card-->
                </div>
            <?php }
            ?>
            </section class="row">

            <!-- Modal -->
            <div class="modal fade modal-tarjeta carousel-fade p-0" id="modal-tarjeta-galeria-carrusel-descuentos" role="dialog" aria-hidden="true" data-backdrop="static" data-keyboard="false">
                <div class="modal-dialog modal-fluid" role="document">
                    <div class="modal-content d-flex align-items-center justify-content-center modal-descuentos">
                        <div class="modal-content-tarjeta p-5">
                            <div class="container contactanos">
                                <!--Card-->
                                <div class="content-gral d-flex justify-content-center">
                                    <div class="col-lg-12 col-xl-6 card-blue p-5">
                                        <!--Card image-->
                                        <div class="overlay">
                                            <div class="card-desc">
                                                <h3 class="tit-desc m-0 modal-titulo-descuentos"></h3>
                                                <h4 class="sub-desc modal-categoria-descuentos"></h4>
                                                <div class="lista-desc mb-2">
                                                    <!-- <ul class="mt-2" id="modal-descripcion-descuentos"> -->
                                                        <!-- <li class="desc-ind d-flex align-items-center"><?php // echo do_shortcode( '[icono nombre="'.sanitize_title("bullet").'" clase="bullet pr-2" ][/icono]' ); ?> 10% de descuento al recibir pagos en efectivo</li>
                                                        <li class="desc-ind d-flex align-items-center"><?php // echo do_shortcode( '[icono nombre="'.sanitize_title("bullet").'" clase="bullet pr-2" ][/icono]' ); ?> 10% de descuento al recibir pagos en efectivo</li>
                                                        <li class="desc-ind d-flex align-items-center"><?php // echo do_shortcode( '[icono nombre="'.sanitize_title("bullet").'" clase="bullet pr-2" ][/icono]' ); ?> 10% de descuento al recibir pagos en efectivo</li> -->
                                                    <!-- </ul> -->
                                                </div>
                                                <span class="legales modal-terminos-descuentos"></span>
                                            </div>
                                        </div>
                                        <!--/Card image-->
                                    </div>
                                    <div class="col-lg-12 col-xl-5 align-self-center card-white p-0">
                                        <!--Card content-->
                                        <div class="card-body card text-md-left card-left p-4">
                                            <div class="icons" onclick="cierraGaleriaDesc()">
                                                <a href="#galeria-desk" data-dismiss="modal" title="Cerrar">
                                                <?php echo do_shortcode( '[icono nombre="'.sanitize_title("cerrar").'"][/icono]' ); ?>
                                                </a>
                                            </div>
                                            <div class="texto-int-card">
                                                <div class="col-12 d-flex justify-content-center">
                                                    <img class="convenio modal-imagen-descuento lazy" data-src="https://www.medicapanamericana.com/imagen/logoShellPoint.ashx?id=250" alt="">
                                                </div>
                                                <div class="contacto-desc">
                                                    <h3 class="card-title-contacto mt-2 mb-1">Contacto</h3>
                                                    <ul class="card-sub-contacto" id="modal-contacto-descuentos">
                                                        <!-- <li>Tel�fono: <a href="#"><b>2159-0066</b></a></li>
                                                        <li>Correo: <a href="#"><b>info@casadelibro.com.mx</b></a></li>
                                                        <li>P&aacute;gina <a href="#"><b>web: https://casadelibro.com.mx/</b></a></li>
                                                        <li>QR de descuento: <a href="#"><b><span type="button" class="descargar-descuento">Descargalo aqu�</span></b></a></li> -->
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                        <!--/.Card content-->
                                    </div>
                                    <!-- <div class="col-lg-1 d-none d-lg-block phantom"></div> -->
                                </div>
                                <!--/.Card-->
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- Final del Modal -->
        </div>
    </div>
</section>
