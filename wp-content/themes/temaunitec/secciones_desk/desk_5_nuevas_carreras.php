<div id="nuevas_carreras"></div>
<?php
/**
 * WP Post Template: desk_5_nuevas_carreras
 */
?>
<?php
    //$nuevaCarrera = get_field('card_nueva_carrera', $theID);
    $nuevaCarrera = get_field('nuevas_carreras', $theID);
    $titulo = get_field('titulo_nuevas_carreras', $theID);
    $boton = get_field('boton', $theID);
    $descripcion = get_field('desk_descripcion_nuevas_carreras', $theID);
    $seleccionCarrera = $nuevaCarrera[0];
    $active = NULL;
?>
<section class="contenedor-nuevas-carreras align-items-center">
    <div class="svg-top w-100">
        <svg xmlns="http://www.w3.org/2000/svg" version="1.1" viewBox="0 0 1890 150">
            <g transform="translate(0,-902.36218)"></g>
            <path d="m 898.41609,-33.21176 0.01,0 -0.005,-0.009 -0.005,0.009 z"></path>
            <path d="m 898.41609,-33.21176 0.01,0 -0.005,-0.009 -0.005,0.009 z"></path>
            <path d="m 1925,0 0,150 -1925,0"></path>
        </svg>
    </div>
    <div class="container-fluid col-12 back-carreras d-flex align-items-center justify-content-center p-4">
        <div class="col-11">
            <div class="row">
                <div class="col-md-6 nuevas-carreras d-flex">
                    <div class="contenido-carreras">
                        <!-- Title -->
                        <h4 class="titleSection"><?php echo $titulo; ?></h4>
                        <!-- max 250 caracteres -->
                        <article>
                            <p><?php echo wp_trim_words( $descripcion, 30 ); ?></p>
                        </article>
                        <ul class="grupo-carreras-nuevas mt-2">
                            <?php foreach ($nuevaCarrera as $key => $contenido) {
                                switch ($key) {
                                    case '0':
                                        $active = "active";
                                        $seleccionCarrera = $contenido;
                                        break;
                                    default:
                                        $active = NULL;
                                        break;
                                }
                            ?>
                            <li class="nuevas-carreras">
                                <a class="<?php echo $active; ?> link-nuevas-carreras" href="" title="<?php echo $contenido->post_title; ?>" onclick="updateImage(<?php echo $contenido->ID ?>)"><?php echo $contenido->post_title; ?></a>
                            </li>
                            <?php } ?>
                        </ul>
                    </div>  
                </div>
                <div class="col-md-5 offset-1 nuevas-carreras d-flex">
                    <!--Carousel Wrapper-->
                    <div id="carruselNuevasCarreras" class="carousel slide carousel-fade" data-ride="carruselNuevasCarreras">
                        <!-- Slides -->
                        <div class="carousel-inner carrusel-nuevas-carreras" role="listbox">
                        <?php foreach ($nuevaCarrera as $key => $contenido) { ?>
                            <?php if ($key == 0) { $active = "active"; } ?>
                            <!-- First slide -->
                            <div class="backface carousel-item <?php echo $active; ?>" id="_desk_5_<?php echo $contenido->ID ?>">
                                <!-- Card -->
                                <div class="card">
                                <?php
                                    $thumbID = get_post_thumbnail_id( $contenido->ID );
                                    $urlImg = wp_get_attachment_url( $thumbID );
                                    $altImg = get_post_meta($thumbID, '_wp_attachment_image_alt', true);
    
                                    $imgHorizontal = get_field('imagen_destacada_horizontal', $contenido->ID);
                                    $urlImgHorizontal = $imgHorizontal['url'];
                                ?>
                                    <!-- card image -->
                                    <div class="view overlay hm-white-slight">
                                        <a href="<?php echo get_permalink($contenido->ID); ?>" title="<?php echo $boton; ?>" data-gtm-tr="NewCareers" data-gtm-value="<?php echo $contenido->post_title; ?>" data-gtm-interaccion="Imagen" >
                                            <img alt="<?php echo $altImg; ?>" class="img-fluid lazy" data-src="<?php echo $urlImgHorizontal; ?>">
                                        </a>
                                    </div>
                                    <!-- card image -->
                                    <!-- Aqui van los componentes list_icon, list, texto, cascade, footer, image, collapse -->
                                    <div class="card-block pb16">
                                        <!-- <div class="row"> -->
                                        <div class="col-12">
                                            <h4 class="sub-nuevas-carreras mt-1"><?php echo $contenido->post_title; ?></h4>
                                            <p class="card-text descripcion-carrera">
                                                <?php echo get_field('descripcion_del_producto', $contenido->ID); ?>
                                            </p>
                                            <a href="<?php echo get_permalink($contenido->ID); ?>" title="<?php echo $boton; ?>" class="verMas float-right" data-gtm-tr="NewCareers" data-gtm-value="<?php echo $contenido->post_title; ?>" data-gtm-interaccion="Ver más" ><?php echo $boton; ?></a>
                                        </div>
                                    </div>
                                </div>
                                <!-- /.Card -->
                            </div>
                            <!-- /.First slide -->
                                <?php $active = NULL; 
                            } ?>
                        </div>
                        <!--  /.Slides -->
                    </div>
                    <!--/.Carousel Wrapper-->
                </div>
            </div>
        </div>
    </div>
    <div class="svg-bottom w-100">
        <svg xmlns="http://www.w3.org/2000/svg" version="1.1" viewBox="0 0 1890 150">
            <g transform="translate(0,-902.36218)"></g>
            <path d="m 898.41609,-33.21176 0.01,0 -0.005,-0.009 -0.005,0.009 z"></path>
            <path d="m 898.41609,-33.21176 0.01,0 -0.005,-0.009 -0.005,0.009 z"></path>
            <path d="m 1925,0 0,150 -1925,0"></path>
        </svg>
    </div>
</section>
