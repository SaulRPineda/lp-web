<?php 
    /**
     * WP Post Template: desk_10_mapa_curricular
     */
?>

<?php

global $woocommerce, $product, $post;
$active = NULL;
$n = 0;
$descarga = NULL;
$muestraDescarga = '';

/*Se obtienen los años Asignados por Carrera*/ 
if(have_rows("tabs",$theID) ) { 
    while( have_rows('tabs') ) { the_row();
        while( have_rows('tipo_de_contenido') ) { the_row();            
            $opcion = get_row_layout('tipo_de_contenido');

            switch ($opcion) {
                case 'mapa_curricular':
                    $field = get_sub_field_object('no_anos');
                    $fieldTipo = get_sub_field_object('desk_plan_de_estudios');
                    $valueTipo = $fieldTipo['value'];
                    $labelTipo = $fieldTipo['choices'][ $valueTipo ];
                    // print_r ($fieldTipo['choices']);
                    // echo "<br>";
                    // print_r ($valueTipo);
                    $value = $field['value'];
                    $label = $field['choices'][ $value ];
                    $opc_noAnos[$valueTipo][$value] = $label;
                    $opc_tipoPlan[$valueTipo] = $labelTipo;
                    $tipo_de_plan = get_sub_field('desk_plan_de_estudios');
                    $duracion_por_oferta=get_sub_field('duracion_por_oferta_academica');
                    $duracion = "duracion-".get_sub_field('no_anos');
                    $plan = "plan-".get_sub_field('desk_plan_de_estudios');
                    $anios = get_sub_field('no_anos');
                    // print_r( get_sub_field('cuatrimestre'));
                    // echo "<br><br>";
                    while( have_rows('cuatrimestre') ) { the_row();
                        $description = get_sub_field( 'descripcion_del_cuatrimestre');
                        /*Se obtinen el total de cuatrimestres*/
                        $totalCuatrimestres[] = array(get_sub_field( 'nombre_cuatrimestre'), $duracion, $plan, $description);
                        $cuatrimestreActual = get_sub_field( 'nombre_cuatrimestre');
                        
                        /*Se obtinen el total de cuatrimestres*/
                        $s = 0;                      
                        while( have_rows('materias') ) { the_row();                            
                            $class = explode("_", get_sub_field('tipo_de_materia') );
                            $materiasLabel = get_sub_field_object( 'tipo_de_materia' );
                            $value = $materiasLabel['value'];
                            $label = $materiasLabel['choices'][ $value ];
                            // if($materiasLabel['choices'][ $value ] !== 'Sin Filtro' ){
                            //    $class[0] = $materiasLabel['choices'][ $value ];
                            // }
                            /*Se obtinen todas las materias x cuatrimestre*/
                            $materiasXCuatrimestres[$n][$s] = array( get_sub_field( 'nombre_materia' ), $class, $label );
                            //echo "Numero: ".$n." -> ".$duracion." -> ".get_sub_field( 'nombre_materia' )."<br />";
                            $s++;
                            /*Se obtinen todas las materias x cuatrimestre*/
                        }
                        $n++;
                        
                    }               
                break;

                default:
                break;
            }
            
        }
    }
}

$totalDuracion = ( count($opc_noAnos) > 1 ) ? array_keys($opc_noAnos)[1]: array_keys($opc_noAnos)[0];

/*Se obtiene la url asignada en los atributos del folleto para compartir en redes Sociales*/
foreach ($product->get_attributes() as $key => $atributo) {    
    $atributosDelProducto = wp_get_post_terms( $product->id, $atributo["name"] );
    foreach ($atributosDelProducto as $key => $value) {
        switch ($value->taxonomy) {
            case 'pa_liga-a-folleto':
                $descarga['plan_presencial'] = $value->name;
            break;

             case 'pa_liga-a-folleto-ejecutiva':
                $descarga['plan_ejecutiva'] = $value->name;
            break;

            case 'pa_liga-a-folleto-enlinea':
                $descarga['plan_enlinea'] = $value->name;
            break;

            case 'pa_liga-a-folleto-pres-linea':
                $descarga['plan_presencial_enlinea'] = $value->name;
            break;

            case 'pa_liga-a-folleto-pres-vesp':
                $descarga['plan_presencial_vespertina'] = $value->name;
            break;
            
            default:
                /*Colocar por defecto presencial para que imprima el total de Cuatrimestres*/
                //$descarga['plan_presencial'] = "Sin_Folleto";
            break;
        }
    }    
}

if ( $descarga == NULL ) {
    $descarga['plan_presencial'] = 'Sin_Folleto';
    $muestraDescarga = 'd-none';
}

$descarga = json_encode($descarga);

//echo $muestraDescarga;
//print_r($descarga);

/*Se obtiene la url asignada en los atributos del folleto para compartir en redes Sociales*/
// print_r($materiasXCuatrimestres);
// print_r($opc_tipoPlan);
// print_r($opc_noAnos);
// echo "<br><br>";
//print_r($totalCuatrimestres);
?>
<div id="perfil"></div>

<script>
    var descarga = <?php echo $descarga; ?>;
</script> 

<section class="mapa-curricular d-flex flex-column align-items-center">
    <input type="hidden" name="actual" id="actual" value="<?php echo $totalDuracion; ?>">
    <input type="hidden" name="h_duracionOferta" id="h_duracionOferta" value="<?php echo $duracion_por_oferta; ?>">
    <input type="hidden" name="h_duracionTitle" id="h_duracionTitle" value="Duración: ">
    
    <div class="container-fluid col-12 p-0">
        <?php 
        //Se obtienen los años Asignados por Carrera 
        if(have_rows("tabs",$theID) ) { 
            while( have_rows('tabs') ) { the_row();
                $desk_tituloTabMapa = get_sub_field('titulo_del_tab');
                /* Validación para imprimimr solo un plan de estudios*/
                $cnt = 0;
                /* End Validación para imprimimr solo un plan de estudios*/
                 while( have_rows('tipo_de_contenido') ) { the_row();            
                    $opcion = get_row_layout('tipo_de_contenido');
                    $duracion = "duracion-".get_sub_field('no_anos');
                    /* Validación para imprimimr solo un plan de estudios*/
                    if($cnt == 0) {

                    switch ($opcion) {
                        case 'mapa_curricular':
        ?>

        <div class="row w-100 mx-0">
            <article class="col-12 text-center d-flex justify-content-center titulo-mapa" data-wow-offset="50">
                <h4 class="w-75 titleSection"><?php echo $desk_tituloTabMapa; ?></h4>
            </article>
            <div class="svg-top w-100">
                <svg xmlns="http://www.w3.org/2000/svg" version="1.1" viewBox="0 0 1890 150">
                    <g transform="translate(0,-902.36218)"></g>
                    <path d="m 898.41609,-33.21176 0.01,0 -0.005,-0.009 -0.005,0.009 z"></path>
                    <path d="m 898.41609,-33.21176 0.01,0 -0.005,-0.009 -0.005,0.009 z"></path>
                    <path d="m 1925,0 0,150 -1925,0"></path>
                </svg>
            </div>
            <article class="col-12 text-center back-mapa d-flex justify-content-center" data-wow-offset="50">
                    <h6 class="w-100 col-11 descriptionSection mb-1"><?php echo do_shortcode( get_sub_field('desk_descripcionPlan', $theID) ); ?></h6>
            </article>
        </div>
            <?php                                
                /*Se obtienen valores del Select ACF*/
                $select = get_sub_field_object( 'no_anos' );
                $filtrosAsignados[0] = "Todo";
                
                while( have_rows('cuatrimestre') ) { the_row();

                    while( have_rows('materias') ) { the_row(); 
                        $materias = get_sub_field_object( 'tipo_de_materia' );
                        $value = $materias['value'];
                        $filtrosAsignados[$value] = $materias['choices'][$value];
                    }
                }                                                            
            ?>
            <!-- Comprobación si trae Filtro -->
            <?php $claseDivFiltro = ( !in_array("Sin Filtro", $filtrosAsignados) )? "": "d-none"; ?>
            <!-- Comprobación si trae Filtro -->
        <div class="container-fluid col-12 back-mapa">
            <div class="row d-flex justify-content-center">
                <div class="col-5 pl-0">
                    <span class="dropdown d-flex mx-auto">
                        <div id="prodTipoPlan" class="drop-view d-flex flex-row align-items-center justify-content-between w-100" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            <span class="drop-text duracion m-0">
                                <div id="nombrePlan"><?php echo array_values($opc_tipoPlan)[0]; ?></div>
                                <div id="descModalidad" class="semestres m-0"></div>
                                <input type="hidden" id="h_plan" value="<?php echo array_keys($opc_tipoPlan)[0]; ?>">
                            </span>
                            <?php echo do_shortcode( '[icono nombre="'.sanitize_title("ver-abajo").'" clase="abajo mr-2" ][/icono]' ); ?>
                        </div>
                        <div class="dropdown-menu dropdown-primary w-100">
                            <?php
                                foreach ($opc_tipoPlan as $key => $opc_tipo_plan) { ?>
                                <a class="dropdown-item destacar" onclick="filtroTipoPlan( '<?php echo $key ?>', '<?php echo $opc_tipo_plan; ?>' );" title="<?php echo $opc_tipo_plan; ?>"><?php echo $opc_tipo_plan; ?><br></a>
                            <?php   } ?>                                          
                        </div>
                    </span>
                </div>
                <div class="col-5 pr-0">
                    <span class="dropdown d-flex mx-auto">
                        <div id="prodCampus" class="drop-view d-flex flex-row align-items-center justify-content-between w-100" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            <span class="drop-text duracion m-0">
                                <div id="ciclos"><?php echo array_values(array_values($opc_noAnos)[0])[0]; ?></div>
                                <div id="duracionPor" class="semestres m-0"></div>
                                <input type="hidden" id="h_duracion" value="<?php echo array_keys(array_values($opc_noAnos)[0])[0]; ?>">
                            </span>
                            <?php echo do_shortcode( '[icono nombre="'.sanitize_title("ver-abajo").'" clase="abajo mr-2" ][/icono]' ); ?>
                        </div>
                        <div id="prodCampusDropdown" class="dropdown-menu dropdown-primary w-100">
                            <?php
                                foreach ($opc_tipoPlan as $keyPlan => $opc_tipo_planes) {
                                    foreach ($opc_noAnos[$keyPlan] as $key => $opc_no_anos) { ?>
                                <a class="dropdown-item destacar plan-<?php echo $keyPlan; ?>" onclick="filtroNoAnos( '<?php echo $key ?>', '<?php echo $opc_no_anos; ?>', '<?php echo $keyPlan; ?>' );" title="<?php echo $opc_no_anos; ?>"><?php echo $opc_no_anos; ?><br></a>
                            <?php   } } ?>                                          
                        </div>
                    </span>
                </div>
            </div>

            <!-- Agregar Validacion para que solo se imprima una vez el plan de estudios -->
            <?php //echo $cnt;?>
            
            <div class="row mt-2">
                <div class="col-12">
                    <!-- Nav tabs -->
                    <div class="row justify-content-center">
                        <div class="col-md-3 pl-0 col-sm-10 col-izq">
                            <!-- Se imprimen el total de Cuatrimestres -->
                            <ul class="nav md-pills pills-primary flex-column mt-0" role="tablist">
                                <?php foreach ($totalCuatrimestres as $key => $cuatrimestres) {
                                    $active = ($key == 0) ? "active": "";                                 
                                ?>
                                <li class="nav-item item-cuatrimestre w-100 align-items-center p-0 mb-1 text-center <?php echo $cuatrimestres[1]; echo " ".$cuatrimestres[2]; ?>">
                                    <a class="nav-link text-center <?php echo $active; ?>" data-toggle="tab" href="#panel_<?php echo $key; ?>" role="tab" title="<?php echo $cuatrimestres[0]; ?>"><h3 class="tab-materias mb-0"><?php echo $cuatrimestres[0]; ?></h3></a>
                                </li>
                                <?php                                   
                                        $active = NULL;
                                    } 
                                ?>
                            </ul>
                            <!-- End Se imprimen el total de Cuatrimestres -->
                        </div>

                        <div class="col-md-7 pr-0">
                            <div class="card-body card p-3 h-100">
                                <!-- Tab panels -->
                                <div class="tab-content p-0">
                                    <!--Panel 1-->
                                    <?php 
                                        //$s = 0;
                                        foreach ($materiasXCuatrimestres as $key => $materias) {
                                            $active = ($key == 0) ? "active": "";
                                    ?>

                                    <div class="<?php echo $totalCuatrimestres[$key][1]; echo " ".$totalCuatrimestres[$key][2];?> panel-materias tab-pane fade in show <?php echo $active; ?>" id="panel_<?php echo $key; ?>" role="tabpanel">
                                    <input type="hidden" id="ciclo" name="ciclo" value="<?php echo $totalCuatrimestres[0][0]; ?>">
                                    <div class='labelMaterias px-2'><?php echo $totalCuatrimestres[$key][3] ?></div>
                                    <?php
                                        echo "<div class='col-12 d-flex align-items-center justify-content-between px-2 my-2 indicadoresMaterias'>";
                                        foreach ($materias as $foo => $filtrosCiclo) {
                                            $arrLabel[$filtrosCiclo[1][1]] = $filtrosCiclo[2];
                                        }

                                        
                                        /*Fix Categorias By SRP 11-07-2019*/
                                        foreach ($arrLabel as $foo => $arrLabelF) {
                                            if ($arrLabelF!== "Sin Filtro"){
                                            // print_r($filtrosCiclo[1][1]);
                                                echo "<label class='filtro-indicadores d-flex align-items-center my-1'><div class='indicador mr-1 ".$foo."'></div>&nbsp;".$arrLabelF."</label>";
                                                // echo $filtrosCiclo[2];
                                            }
                                        }
                                        /*End Fix Categorias By SRP 11-07-2019*/

                                        unset($arrLabel);
                                        echo "</div>";

                                        $j=0;
                                        foreach ($materias as $foo => $materia) {
                                        $res = $j%3;
                                            if ($res == 0) { echo '<div class="d-flex flex-row fila">'; }
                                    ?>
                                        <div class="cont-materia <?php echo $materias[$foo][1][1]; ?> d-flex align-items-center justify-content-center text-center"><?php echo $materia[0]; ?></div>

                                        <?php   
                                            if ($res == 2) { echo '</div><!--Cierre1-->'; }
                                            $j++;        
                                        } /*End While materias*/

                                            if ($res%3 == 0) { echo '</div><!--Cierre2-->'; }
                                            if ($res%3 == 1) { echo '</div><!--Cierre2-->'; }
                                        ?>
                                        </div>
                                    <?php
                                            $active = NULL;
                                            //$s++;
                                        }
                                    ?>                                
                                </div>
                                <!-- Cierra Tab panels -->
                            </div>
                        </div>
                    </div>
                    <!-- Nav tabs -->
                </div>
            </div>

            <div class="row mt-1">
                <div class="col-1"></div>
                <div class="col-10 mb-0 no-padding d-flex justify-content-end">
                    <!--Social buttons-->
                    <div class="mapa-share">
                        <div class="social-reveal-mapa pr-3" id="demo">                                        
                            <?php echo do_shortcode('[desktop_share pagina="'.get_permalink().'" clase="icon-share-banner" tipo="inline-ul" seccion="Plan de Estudios: '.$post->post_title.'"]');?>
                        </div>
                        <!-- Validación para Imprimr el icono de Compartir -->
                        <a data-toggle="toggle" data-target="#demo" title="Compartit">
                            <?php echo do_shortcode( '[icono nombre="'.sanitize_title("compartir").'" clase="pull-right descarga-plan mr-2"][/icono]' ); ?>
                        </a>
                    </div>
                    <!--/Social buttons-->
                    <a data-gtm-tr="downloadFiles" data-gtm-pos="folleto-de-carrera" target="_blank" id="descarga_folleto_mapa" title="Descargar folleto"><?php echo do_shortcode( '[icono nombre="'.sanitize_title("descargar").'" clase="pull-right descarga-plan '.$muestraDescarga.' "][/icono]' ); ?></a>
                </div>
            </div>         
            <?php               $cnt++;
                                break;
                                default:
                                break;
                            }
                        } /*End Comprobacion un plan de estudios*/
                    }
                } 
            }
            ?> <!-- Se cierran llaves del div contenedor y del main -->
        </div>
        <div class="svg-bottom w-100">
            <svg xmlns="http://www.w3.org/2000/svg" version="1.1" viewBox="0 0 1890 150">
                <g transform="translate(0,-902.36218)"></g>
                <path d="m 898.41609,-33.21176 0.01,0 -0.005,-0.009 -0.005,0.009 z"></path>
                <path d="m 898.41609,-33.21176 0.01,0 -0.005,-0.009 -0.005,0.009 z"></path>
                <path d="m 1925,0 0,150 -1925,0"></path>
            </svg>
        </div>
    </div>
</section>
