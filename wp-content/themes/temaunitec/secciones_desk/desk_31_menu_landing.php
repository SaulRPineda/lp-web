<?php
/*
 * WP Post Template: desk_31_menu_landing
 */

/**
* Custom walker class.
*/
class sidenav extends Walker_Nav_Menu {
    public $mini_menu; 

    function start_lvl( &$output, $depth = 0, $args = array()) {
        $output .= "\n" . $indent . '<div class="collapsible-body"><ul>';
    }

    function end_lvl( &$output, $depth = 0, $args = array() ) {
        $output .=  "\n" . $indent . "</ul></div>";
    }

    function start_el( &$output, $item, $depth = 0, $args = array(), $id = 0 ) {
        //print_r($args);
        // print_r($item);

        $indent = ( $depth > 0 ? str_repeat( "\t", $depth ) : '' ); // code indent

        // Passed classes.
        $classes = empty( $item->classes ) ? array() : (array) $item->classes;

        $class_names = esc_attr( implode( ' ', apply_filters( 'nav_menu_css_class', array_filter( $classes ), $item ) ) );
        

        $arrow = ( strrpos( $class_names, "menu-item-has-children") > -1 )? '<i class="icon-u icon-u-ver-abajo"></i>': "";

        if( strrpos( $class_names, "menu-item-has-children") > -1 ){
            $parentHead = '<ul class="collapsible collapsible-accordion"><li>';
            $class_names = "class='collapsible-header waves-effect d-flex align-items-center text-white arrow-r'";
        }else{
            $parentHead = '<li>';
            $class_names = "class='waves-effect d-flex align-items-center text-white'";
            $arrowcollapse = "";
            $attributes = ! empty( $item->url )? ' href="' . esc_attr( $item->url) .'" ' : '';
        }
        
        if( $item->title == "hr" ){
            $output .= "<hr>";
        }else{
            $mm = array( "Preparatoria", "Licenciaturas", "Posgrados" );
            if( in_array( $item->title, $mm ) ){
                $this->mini_menu[$item->title] = $item->url;
            }
            // Build HTML.
            $output .= $indent . $parentHead . 
                '<a title="' . $item->title . '" ' . $class_names . $attributes . '>' .
                    /*'<i class="icon-u icon-u-' . sanitize_title( $item->title ) . '"></i>' .*/
                    /*do_shortcode( '[icono nombre="'.sanitize_title($item->title).'"][/icono]' ).*/
                    do_shortcode( '[icono nombre="'.sanitize_title($item->classes[0]).'"][/icono]' ).
                    $item->title .
                    $arrow .
                '</a>';
        }
    }

    function end_el( &$output, $item, $depth = 0, $args = array() ) {
        $output .= "</li>";
    }
}

$sideNav = new sidenav();

$orderby = 'name';
$order = 'asc';
$hide_empty = false ;
$taxonomy='product_cat';
$cat_args = array(
    'orderby'    => $orderby,
    'order'      => $order,
    'hide_empty' => $hide_empty,
    'slug'       => 'oferta-educativa',
);
 
$product_category_parent = get_terms( 'product_cat', $cat_args );
// print_r($product_category_parent[0]->term_id);

$cat_args = array(
    'orderby'    => $orderby,
    'order'      => $order,
    'hide_empty' => $hide_empty,
    'parent'     => $product_category_parent[0]->term_id,
);

$product_categories = get_terms( 'product_cat', $cat_args );
$all_careers_product = [];
foreach($product_categories as $key => $category_object) {
    // print_r($category_object);
    $args_menu = array(
        'post_type'             => 'product',
        'post_status'           => 'publish',
        'ignore_sticky_posts'   => 1,
        'posts_per_page'        => '-1',
        'fields'        =>      array('ID', 'post_title', 'post_name', 'guid'),
        'orderby'       =>     'post_title',
        'order' => 'ASC',
        'tax_query' => array(
            array(
                'taxonomy' => 'product_cat',
                'field'    => 'term_id',
                'terms'    => $category_object->term_id,
            )
        ),
    );
    $products_la_prueba = query_posts($args_menu);
    // array_push($all_careers_product, $category_object->name );
    $all_careers_product[$category_object->name] = $products_la_prueba;
    // print_r ($products_la_prueba);
}
wp_reset_query();
$all_careers_product=json_encode($all_careers_product);


?>
<script>
    var add_ons_menu = <?php echo $all_careers_product; ?>;
</script>

<nav class="navbar fixed-top p-0 z-depth-0">  
    <div class="navContLanding navCont d-flex align-items-center">
        
        <div class="col-xl-3 col-md-3 d-flex align-items-center">
            <a href="" class="logoMenu p-0 m-0 pl-3" title="Bienvenido a la UNITEC">
                <!-- <?php echo do_shortcode( '[icono nombre="'.sanitize_title("unitec").'"][/icono]' ); ?> -->
                <img src="<?php echo get_template_directory_uri() . "/assets/frontend_desktop/img/header/unitec.svg"; ?>" alt="LOGO UNITEC">
            </a>
        </div>
        
        <div class="col-xl-4 col-md-3">
            <div class="row">
                <div class="col-xl-10 offset-xl-2 col-lg-10 offset-lg-2 col-md-12 offset-md-0">
                    
                </div>
            </div>
        </div>
        <div class="col-xl-5 col-md-6">
            <div class="btn-menu-head d-flex align-items-center justify-content-end">
                <div class="tel pr-2">
                    
                </div>
                <div class="solicitud">

                    <?php
                        if( strpos($post->post_name,'impulsa') !== false  ){
                            $urlBtn = ( $_COOKIE["reg_impulsa"] ) ? 'href="https://www.unitec.mx/folleto/impulsa.pdf"' : 'onclick="openModal(\'#modal_frm_app\')"';
                        } else{
                            $urlBtn = 'onclick="openModal(\'#modal_frm_app\')"';
                        }
                    ?>

                    <!-- Icono de sobre para contacto Formulario -->
                    <?php if( strpos($post->post_name,'conexion-unitec') === false && strpos($post->post_name,'prueba-performance') === false && strpos($post->post_name,'agendar-cita') === false ){ ?>
                    <!-- Icono de Formulario -->
                    <a id="calculadoraModal" <?php echo $urlBtn; ?> class="btn btn-naranja-unitec  btn-naranja-landing z-depth-0 searchNav" target="_blank" title="Reg&iacute;strate">Reg&iacute;strate</a>
                    <?php } ?>

                    <?php if( strpos($post->post_name,'biblioteca-virtual') !== false ) { ?>
                    <!-- btn Login Biblioteca Virtual -->
                    <a id="login-biblioteca" onclick="openModal('#modal_frm_biblioteca')" class="btn btn-naranja-unitec z-depth-0 searchNav" title="Ingresar">INGRESAR</a>
                    <!-- btn Login Biblioteca Virtual -->

                    <!-- btn Logout Biblioteca Virtual -->
                    <a id="logout-biblioteca" onclick="" class="btn btn-naranja-unitec z-depth-0 searchNav d-none" title="CERRAR SESI&Oacute;N">CERRAR SESI&Oacute;N</a>
                    <!-- btn Logout Biblioteca Virtual -->
                    <?php } ?>
                </div>
            </div>
        </div>            
    
    </div>
</nav>
