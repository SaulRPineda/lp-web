<!-- Implementacion de la nueva calculadora -->
<div id="aplicativo-formulario" class="contenedor-principal-calculadora" style="display:none;">

    <div class="main main-calculadora-todo">
        <div id="selector-float">
            <div class="tc overlay-controls d-flex d-none">
            </div>
            <div class="tc titlecont d-flex">
                <div class="titulo-calculadora d-flex align-items-center mx-auto">
                    <h3 class="title-inside">Solicitud de información</h3>
                </div>
                <div class="titulo-calculadora">
                    <div class="steps-calc d-flex">
                        <!-- <p class="pasos">Paso <span class="steps">3</span> de 6</p> -->
                        <button id="cierra-tu-calculadora" class="close-x d-flex justify-content-center" onclick="cerrarModalCalculadora()">X</button>
                    </div>
                </div>
            </div>

            <!-- <div id="coming-back" class="btn-group d-none" style="display: block; padding-top: 2vh;">
                <div class="card card-adapted w-20">
                    <div class="card-body tc w-100">
                        <h5 class="mt-0">⮨ Regresar</h5>
                    </div>
                </div>
            </div> -->
            <div class="breadcrumbs w-100">
                <ul class="d-flex justify-content-center align-items-center">
                    <li id="bread1" class="bread-name d-flex justify-content-center align-items-center breadActive">Nivel Académico <i class="icon-u icon-u-ver-derecha p-0"></i> </li>
                    <li id="bread2" class="bread-name d-flex justify-content-center align-items-center">Área de Interés</li>
                </ul>
            </div>


            <div class="containerFirst cont-quest d-flex align-items-center justify-content-center">
                <div class="controls d-none">
                    <a class="btn-return-form waves-effect waves-lights m-0 d-flex justify-content-center align-items-center">
                        <i class="icon-u icon-u-ver-izquierda"></i>
                    </a>
                </div>
                <div class="quest w-80 p-activa" id="business-line">
                    <div class="subtitulo">¿Qué grado de estudio te interesa estudiar?</div>   
                    <!-- <div class="desc-subtitle hide-on-res">Selecciona el nivel de estudios que te interesa estudiar en la UNITEC</div> -->
                    <div id="render_lineas_negocio" class="btn-group"></div>
                </div>
            
                <div class="adjust-container quest w-80 hide" id="container_carrera_modalidad">
                    <div class="w-100" id="carrera">
                        <div class="subtitulo" id="subtituloInteres"></div>
                        <div class="btn-group">
                            <div class="tc w-100 d-flex">
                                <input class="ipt-b" placeholder="Buscar..." id="project">
                                <div class="abajo d-flex align-items-center justify-content-center"><i class="ver-abajo"></i></div>
                                <input type="hidden" id="project-id">
                                <!-- <button class="arrow">&#9660;</button> -->
                            </div>
                        </div>
                        <div>
                            <p style="display: none" id="project-description"></p>
                        </div>
                    </div>

                    <div class="w-100 hide" id="padre_tutor">
                        <div class="subtitulo">¿Eres padre o tutor?</div>
                        <div class="btn-group">
                            <div class="sel w-100">
                                <div class="custom-select w-100 d-flex">
                                    <select class="ipt-b" id="render_padre_tutor">
                                        <option>Selecciona</option>
                                        <option>Si</option>
                                        <option>No</option>
                                    </select>
                                </div>
                                <div class="abajo d-flex align-items-center justify-content-center"><i class="ver-abajo"></i></div>
                            </div>
                        </div>
                        <div>
                            <p style="display: none" id="project-description"></p>
                        </div>
                    </div>
                
                    <div class="w-100" id="modalidad" style="padding-top: 5vh;">
                        <div class="subtitulo">¿Qué modalidad y campus te interesan?</div>
                        <div class="desc-subtitle hide-on-res">Las opciones disponibles se muestran de acuerdo al programa académico elegido</div> <!-- Selecciona la modalidad y el campus para ver la colegiatura correspondiente -->
                        <div class="btn-group">
                            <div class="sel mr">
                                <div class="custom-select w-100 d-flex">
                                    <select class="ipt-b" id="render_modalidad"><option>Modalidad</option></select>
                                </div>
                                <div class="abajo d-flex align-items-center justify-content-center"><i class="ver-abajo"></i></div>
                            </div>        
                            <div class="sel">
                                <div class="custom-select w-100 d-flex">
                                    <select  class="ipt-b" id="render_campus"><option>Campus</option></select>
                                </div>
                                <div class="abajo d-flex align-items-center justify-content-center"><i class="ver-abajo"></i></div>            
                                <div id="reset" class="reset"><div class="flecha-reset">✏</div></div> <!-- <i class="icono-reset"⇨></i> -->
                            </div>
                        </div>

                        <div id="alert-prepaunam" style='margin: 1vh 0 2vh; font-family: "Roboto Light",sans-serif; text-align: center; font-size: 2vh; font-weight: 400;' class="d-none">
                            <div class="alert alert-light" role="alert">
                              *Iniciamos Clases en Agosto de 2020
                            </div>
                        </div>

                    </div>
                    <!--Se comenta por nueva Implementación BY SRP 03-06-2019-->
                    <div class="center">
                        <button id="btn-verCol-avanzar" class="btn btn-or" onclick="avanzaPaso()">Siguiente</button>
                    </div> 
                </div>
                <div id="secCiclos" class="quest w-80 hide">
                    <div class="subtitulo">¿Cuál es el periodo de tu interés?</div>
                    <div class="row adapt-desk-lineas col-12 mt-2" style="justify-content: center;">
                    </div>
                </div>
            </div>

        </div>
    </div>
</div>