<?php
/**
 * WP Post Template: desk_2_15_virtual_tour
 */
?>
<?php
    $postType = get_post( $idPost );
    $tituloTourVirtual = get_field('titulo_banner_tour_virtual', $theID);
    $descriptionOneTour = get_field('descripcion_one_tour_virtual', $theID);
    $descriptionTwoTour = get_field('descripcion_two_tour_virtual', $theID);
    $selectorTipoImg = get_field('selector_tipo_de_imagen_banner', $theID);

    $imgTourDesk = get_field('img_banner_tour_desk', $theID);
        $urlTourDesk = $imgTourDesk["url"];
        $altTourdesk = $imgTourDesk["alt"];

    $callToAction = get_field('action_to_tour_virtual', $theID);
    $textCallToActionTour = $callToAction['text_call_to_action'];
    $urlCallToActionTour = $callToAction['url_call_to_action'];
?>
<script type="text/javascript">
function openTourVirtual(campus){
    localStorage.setItem("abrirModalTour", true);
    window.location.href = campus;
}
</script>
<section class="align-items-center">
    <div id='banner-imagen' class="contenedor-tour-virtual wrap container-fluid d-flex justify-content-center align-items-center">
        <div class="bg-tour-virtual" style="background-image: url(<?php echo $urlTourDesk; ?>)"></div>
        <div class="info-tour row w-100 d-flex justify-content-center">
            <div class="col-4 d-flex flex-column align-items-center justify-content-center">
                <span class="title-campus-tour"><b><?php echo $tituloTourVirtual; ?></b></span>
                <svg viewBox="0 0 100 100" style="width:150px;">
                    <g style="transform-origin:50px 50px 0px;transform:scale(0.9)">
                        <g style="transform-origin:50px 50px 0px">
                            <g style="transform-origin:50px 50px 0px">
                                <g class="ld ld-breath" style="animation-delay:-2.2s;animation-direction:normal;animation-duration:2.2s;transform-origin:50px 50px 0px">
                                    <circle cx="50" cy="50" r="27.5" fill="none" stroke="#fff" stroke-linecap="round" stroke-linejoin="round" stroke-miterlimit="10" stroke-width="3"/>
                                </g>
                                <g class="ld ld-breath" style="animation-delay:-2.112s;animation-direction:normal;animation-duration:2.2s;transform-origin:50px 50px 0px">
                                    <ellipse cx="50" cy="50" rx="15" ry="27.5" fill="none" stroke="#fff" stroke-linecap="round" stroke-linejoin="round" stroke-miterlimit="10" stroke-width="3"/>
                                </g><g class="ld ld-breath" style="animation-delay:-2.024s;animation-direction:normal;animation-duration:2.2s;transform-origin:50px 50px 0px">
                                    <line x1="50" x2="50" y1="22.5" y2="77.5" fill="none" stroke="#fff" stroke-linecap="round" stroke-linejoin="round" stroke-miterlimit="10" stroke-width="3"/>
                                </g><g class="ld ld-breath" style="animation-delay:-1.936s;animation-direction:normal;animation-duration:2.2s;transform-origin:50px 50px 0px">
                                    <line x1="24.365" x2="75.686" y1="40.046" y2="40.046" fill="none" stroke="#fff" stroke-linecap="round" stroke-linejoin="round" stroke-miterlimit="10" stroke-width="3"/>
                                </g>
                                <g class="ld ld-breath" style="animation-delay:-1.848s;animation-direction:normal;animation-duration:2.2s;transform-origin:50px 50px 0px">
                                    <line x1="24.365" x2="75.686" y1="59.454" y2="59.454" fill="none" stroke="#fff" stroke-linecap="round" stroke-linejoin="round" stroke-miterlimit="10" stroke-width="3"/>
                                </g>
                            </g>
                            <g style="transform-origin:50px 50px 0px">
                                <g style="transform-origin:50px 50px 0px">
                                    <g class="ld ld-breath" style="animation-delay:-1.76s;animation-direction:normal;animation-duration:2.2s;transform-origin:50px 50px 0px">
                                        <path d="M30.002,84.637 C18.049,77.72,10,64.804,10,50c0-20.492,15.409-37.385,35.273-39.724" fill="none" stroke="#fff" stroke-linecap="round" stroke-linejoin="round" stroke-width="3"/>
                                    </g>
                                    <g style="transform-origin:50px 50px 0px">
                                        <g class="ld ld-breath" style="animation-delay:-1.672s;animation-direction:normal;animation-duration:2.2s;transform-origin:50px 50px 0px">
                                            <polygon points="44.157 4.939 44.94 15.844 50 10" fill="#fff"/>
                                        </g>
                                    </g>
                                </g>
                            </g>
                            <g style="transform-origin:50px 50px 0px">
                                <g style="transform-origin:50px 50px 0px">
                                    <g class="ld ld-breath" style="animation-delay:-1.584s;animation-direction:normal;animation-duration:2.2s;transform-origin:50px 50px 0px">
                                        <path d="M69.998,15.363 C81.951,22.28,90,35.196,90,50c0,20.492-15.409,37.385-35.273,39.724" fill="none" stroke="#fff" stroke-linecap="round" stroke-linejoin="round" stroke-width="3"/>
                                    </g>
                                    <g style="transform-origin:50px 50px 0px">
                                        <g class="ld ld-breath" style="animation-delay:-1.496s;animation-direction:normal;animation-duration:2.2s;transform-origin:50px 50px 0px">
                                            <polygon points="55.843 95.061 55.06 84.156 50 90" fill="#fff"/>
                                        </g>
                                    </g>
                                </g>
                            </g>
                        </g>
                    </g>
                </svg>
                <span class="title-campus-tour text-center"><?php echo $descriptionOneTour; ?></span>
                <span class="subtitle-campus-tour text-center my-2"><?php echo $descriptionTwoTour; ?></span>
                <a onclick="openTourVirtual('<?php echo $urlCallToActionTour; ?>/#tour_virtual')" type="button" class="btn btn-back-tour waves-effect waves-light">
                    <span>&#9658; <?php echo $textCallToActionTour; ?></span>
                </a> 
            </div>
        </div>
    </div>
</section>
