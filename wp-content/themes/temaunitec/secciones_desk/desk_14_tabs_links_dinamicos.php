<div id="tabs_links_dinamicos"></div>
<?php
/**
 * WP Post Template: desk_14_tabs_links_dinamicos
 */
?>

<?php
    /*Se insertan los plugins que necesita la seccion tal y como se llama el archivo js*/
    load_script( $pluginsNecesarios = array('sec_specialList') );
    /*End Se insertan los plugins que necesita la seccion tal y como se llama el archivo js*/

    $contenidoCardUnitec = get_field('contenido_card', $theID);
?>
<?php if( strlen($contenidoCardUnitec) > 5 ) { ?>
<section class="background-definido-prestigio d-flex"><!-- Background -->
    <div class="container-fluid wow fadeIn d-flex justify-content-center align-items-center">
        <div class="col-lg-10 p-0 con-centrado-vert">
            <!-- Live preview-->
            <div class="row pl-4 pr-4 justify-content-center">            
                <article class="col-12 text-center d-flex justify-content-center" data-wow-offset="50">
                    <h4 class="w-75 titleSection"><?php echo get_field('titulo_card', $theID); ?></h4>
                </article>
                <article class="col-12 text-center mb-1 mt-1 d-flex justify-content-center" data-wow-offset="50">
                    <h6 class="w-100 descriptionSection mt-1"><?php echo $contenidoCardUnitec; ?></h6><!-- Hacer las clases de esta etiqueta globales -->
                </article>
                <!-- Secciones Prestigio / Calidad Educativa -->
                <section class="prestigio mb-1" id="tabNav">
                    <?php ?>
                    <div class="tabs-wrapper"> 
                        <ul class="nav classic-tabs" role="tablist" style="justify-content: center;">
                        <?php 
                            $noTabs=0;
                            if(have_rows("contenido",$theID) ) {
                                while( have_rows('contenido') ) { the_row();
                                    if($noTabs == 0) { $active = "active text-black"; }
                        ?>
                            <li class="nav-item m-0"><!-- waves-effect waves-light -->
                                <a class=" waves-light <?php echo $active; ?> " data-toggle="tab" href="#tab_panel_unitec_<?php echo $noTabs; ?>" role="tab"  title="<?php echo get_sub_field('titulo_del_tab'); ?>" data-gtm-tr="Tab" data-gtm-tab= "<?php echo get_sub_field('titulo_del_tab'); ?>"><?php echo get_sub_field('titulo_del_tab'); ?></a>
                            </li>            
                        <?php 
                                    $active = NULL;
                                    $noTabs++;
                                }
                            }
                        ?>
                        </ul>
                    </div>
                    <?php //echo do_shortcode( '[icono nombre="'.sanitize_title("tab-derecha").'" clase="rightTabNav direccionales" estilo="style=\'display:none\'"][/icono]' ); ?>
                </section>
                <!-- Secciones Prestigio / Calidad Educativa -->

                    
                <!--  Caracteristicas -->
                <div class="col-12 iconos-unitec justify-content-center mb2">
                    <?php 
                        $active = NULL;    
                        $panelContent=0;
                        $itemSlide = 0;
                    ?>
                    <!-- Tab panels -->
                    <div class="tab-content">
                        
                    <?php 
                        if(have_rows("contenido",$theID) ) {        
                            while( have_rows('contenido') ) { the_row();
                                if($panelContent == 0){ $active = "active"; }
                    ?>
                        <!--Inicia Tab -->
                        <div class="tab-pane fade in show <?php echo $active; ?>" id="tab_panel_unitec_<?php echo $panelContent; ?>" role="tabpanel">
                            <div class="row justify-content-center">
                            <?php
                                $nameTab = get_sub_field('titulo_del_tab');

                                    while( have_rows('links') ) { the_row();
                                        $descriptionIcon = get_sub_field('titulo');
                                        $descriptionIconGTM = trim(substr($descriptionIcon, 0, 30)).'...';
                            ?>
                            
                                <div class="col-lg-3 col-sm-6 justify-content-center text-center mb-1">
                                    <div class="list-item-container">
                                        <?php
                                            $target = ( strpos( strtolower(get_sub_field('link')), "unitec.mx" ) ) ? "" : "target='_blank'";
                                            
                                            if ( get_sub_field('link') != NULL || get_sub_field('link') != "" ) {
                                                $linkInicio = "<a class='linkDinamico' href='".get_sub_field('link')."' ".$target." data-gtm-tr='Prestige'  data-gtm-tab='". $nameTab ."'  data-gtm-option='". $descriptionIconGTM ."' >";
                                                $linkFin = "</a>";                                   
                                            }
                                        ?>

                                        <div class="item-icon mb-1">
                                            <?php echo $linkInicio; ?><?php echo do_shortcode( '[icono nombre="'.sanitize_title( get_sub_field("icono") ).'" clase="link-simple-list"][/icono]' ).$linkFin; ?>
                                        </div>
                                        
                                        <div class="item-description">
                                            <?php 
                                                    echo $linkInicio.$descriptionIcon.$linkFin;
                                                    $linkInicio = NULL;
                                                    $linkFin = NULL;
                                            ?>
                                        </div> <!-- icon-u icon-u-alumnos -->
                                    </div>
                                </div>

                            <?php   $itemSlide++;
                                    $active = NULL;  
                                 } 
                                ?>
                            </div>
                        </div>
                        <!-- Termina Tab -->
                    <?php       $itemSlide=0;
                                $panelContent++;  
                            }  
                        }
                    ?>
                    </div>
                </div>
                <!--  Caracteristicas -->
            </div><!-- /.Live preview-->
        </div>
    </div>
</section>
<?php } ?>