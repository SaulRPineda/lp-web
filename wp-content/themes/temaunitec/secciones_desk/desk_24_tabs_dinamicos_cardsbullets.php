<div id="becas"></div>
<?php
/**
 * WP Post Template: desk_24_tabs_dinamicos_cardsbullets
 */
?>

<?php
$active = NULL;
$clase = "card-inversa";
$claseCard = "card-right";

$conteoTitulo = count(get_field('titulo_tabs_dinamicos_cardsbullets'));
if ($conteoTitulo == 0)
    $claseTitulo = "d-none";
else
    $claseTitulo = NULL;

$conteoDescripcion = count(get_field('descripcion_tabs_dinamicos_cardsbullets'));
if ($conteoDescripcion == 0)
    $claseDescripcion = "d-none";
else
    $claseDescripcion = NULL;

$conteoTitDesc = $conteoTitulo + $conteoDescripcion;
if ($conteoTitDesc == 0)
    $claseTitDesc = "d-none";
else
    $claseTitDesc = NULL;

$conteoTabsD = count(get_field('tabs_dinamicos_cardsbullets'));
if($conteoTabsD == 1)
    $claseTabs = "d-none";
else
    $claseTabs = NULL;
?>
<?php if(have_rows("tabs_dinamicos_cardsbullets",$theID) ) { ?>
<section class="tabs-cardsbullets d-flex align-items-center"><!-- d-flex align-items-center -->
    <div class="container-fluid wow fadeIn d-flex justify-content-center">
        <div class="col-lg-12">
            <div class="row justify-content-center <?php echo $claseTitDesc; ?>"> <!-- Contenedor de encabezado de la sección -->
            <article class="col-10 text-center <?php echo $claseTitulo; ?>" data-wow-offset="50">
                    <h4 class="w-75 titleSection mx-auto"><?php echo get_field('titulo_tabs_dinamicos_cardsbullets', $theID); ?></h4>
                </article>
                <article class="col-10 text-center <?php echo $claseDescripcion; ?>" data-wow-offset="50">
                        <h6 class="w-100 descriptionSection mt-1 mx-auto"><?php echo do_shortcode( get_field('descripcion_tabs_dinamicos_cardsbullets', $theID) ); ?></h6>
                </article>
            </div> <!-- Fin del contenedor de encabezado de la sección -->


            <!-- Tab Navegacion -->
            <section class="mb-1 <?php echo $claseTabs; ?>" id="tabNav">
                <div class="tabs-wrapper">
                    <ul class="nav classic-tabs tabs-dinamicas justify-content-center" role="tablist">
                    <?php //if(have_rows("tabs_dinamicos_cardsbullets",$theID) ) { 
                            $noTabsDinamicos=0;  

                            while( have_rows('tabs_dinamicos_cardsbullets') ) { the_row(); 
                                if($noTabsDinamicos == 0){ $active = "active"; } 
                    ?>        
                        <li class="nav-item m-0"><!-- waves-effect waves-light -->
                            <a class="tab-dinamico-cardbullets waves-light <?php echo $active; ?> " data-toggle="tab" href="#tab-panel-unitec-<?php echo $noTabsDinamicos; ?>" role="tab" title="<?php echo get_sub_field('titulo_del_tabs_dinamicos_cardsbullets', $theID); ?>" data-gtm-tr="Tab" data-gtm-tab= "<?php echo get_sub_field('titulo_del_tabs_dinamicos_cardsbullets', $theID); ?>"><?php echo get_sub_field('titulo_del_tabs_dinamicos_cardsbullets', $theID); ?></a>
                        </li>
                        <?php   $noTabsDinamicos++; 
                                $active = NULL; 
                            } 
                        //} 
                    ?>
                    </ul>
                </div>
            </section>

            <div class="row w-100 m-0">
                <div class="col-1 d-flex align-items-center justify-content-center">
                    <!--Controls-->
                    <div class="controls-tabs-dinamicos d-flex justify-content-center">
                        <a class="btn-floating waves-effect waves-light btn-arrow carrousel_left_tabs_dinamicos d-none" data-slide="next" title="Anterior">
                            <?php echo do_shortcode( '[icono nombre="'.sanitize_title("ver-izquierda").'" clase="arrow-right"][/icono]' ); ?>
                        </a>                        
                    </div>
                    <!--Controls-->
                </div>
                <div class="col-10 iconos-unitec justify-content-center">
                    <!--Grid row-->
                    <div class="row tab-content">
                        <?php 
                            $l = 0; 
                            $panelContent = 0; 
                            $tabCount = 0;
                            $cardsCount = 0;
                            while( have_rows('tabs_dinamicos_cardsbullets') ) { the_row();
                                $active = NULL; 
                                if($panelContent == 0) { $active = "active"; }
                                ?>
                        <div class="carrousel_tabs_dinamicos w-100 tab-pane fade in show <?php echo $active; ?>" data-pos="0" id="tab-panel-unitec-<?php echo $tabCount; ?>"  role="tabpanel">
                        <div class="carrousel_inner_tabs_dinamicos">
                            <!-- Tab panels -->
                            <ul class="d-flex m-0" >
                                <?php //if(have_rows("tabs_dinamicos_cardsbullets",$theID) ) { 
                                    while( have_rows('tipo_de_contenido_cardsbullets') ) { the_row();
                                        $desk_TituloDinamico = get_sub_field('titulo_cardsbullets');
                                        $desk_ImagenoDinamico = get_sub_field('imagen_cardsbullets');
                                        $desk_btnbullets = get_sub_field('boton_bullets');
                                        $desk_urlbullets = get_sub_field('url_del_boton_card_bullets');

                                        if($panelContent%2 == 0){ $clase = ""; $claseCard = "card-left"; } 
                                            if($tabCount == 0)
                                            $cardsCount++;                        
                                        ?>
                                    <!--Inicia Tab -->
                                    <li class="col-12 p-2 cardsbullets">
                                        <!--Card-->
                                        <div class="row m-0 justify-content-center d-flex <?php echo $clase; ?>">
                                            <div class="col-lg-12 col-xl-7 p-0 d-flex align-items-center justify-content-center">
                                                <!--Card image-->
                                                <div class="overlay">
                                                    <img data-src="<?php echo $desk_ImagenoDinamico['url']; ?>" class="img-fluid rounded-0 lazy" alt="<?php echo $desk_ImagenoDinamico['alt']; ?>">
                                                </div>
                                                <!--/Card image-->
                                            </div>
                                            <div class="col-lg-12 col-xl-5 align-self-center">
                                                <!--Card content-->
                                                <div class="card-body card text-md-left <?php echo $claseCard; ?> p-4">
                                                    <div class="texto-int-card h-100 d-flex justify-content-center flex-column">
                                                        <h4 class="card-title-desk d-flex"><span class="filo-inicio-t pr-3">|</span> <?php echo $desk_TituloDinamico; ?></h4>
                                                        <!--Title-->
                                                        <!-- <h3 class="pl-4 card-sub-desk">Adquirirás los siguientes conocimientos:</h3> -->
                                                        <!--Text-->
                                                        <ul>
                                                            <?php while( have_rows('tipo_lista_cardsbullets') ) { the_row(); ?>
                                                                <li class="text-left w-100">
                                                                    <i class="icon-u icon-u-bullet bullet"></i>
                                                                    <span><?php echo get_sub_field('listado_cardsbullets'); ?></span>
                                                                </li>
                                                        <?php } ?>                                                
                                                        </ul>
                                                    </div>
                                                    <?php if ( strlen( trim($desk_urlbullets) ) > 5 ) { ?>
                                                        <div class="navegacion d-flex justify-content-end mt-2">
                                                            <?php if (strpos($desk_urlbullets, 'javascript') !== false) {?>
                                                                <a onclick="<?php echo $desk_urlbullets; ?>" class="verMas pull-right" title="<?php echo $desk_btnbullets; ?>">
                                                                    <?php echo $desk_btnbullets; ?>
                                                                </a>
                                                            <?php } else {?>
                                                                <a href="<?php echo $desk_urlbullets; ?>" class="verMas pull-right" title="<?php echo $desk_btnbullets; ?>">
                                                                    <?php echo $desk_btnbullets; ?>
                                                                </a>
                                                            <?php }?>
                                                        </div>
                                                    <?php } ?>
                                                </div>
                                                <!--/.Card content-->
                                            </div>
                                        </div>
                                        <!--/.Card--> 
                                    </li>
                                    <!-- Termina Tab -->
                                    <?php   
                                            if($tabCount == 0)
                                                $l++;
                                            $clase = "card-inversa";
                                            $claseCard = "card-right";
                                            $panelContent++;
                                                }     
                                    ?>
                                <?php //} ?>
                                    <!-- Termina Tab -->
                                </ul>
                                <!-- //Tab panels -->
                            </div>
                        </div>
                        <?php
                            $tabCount++;
                        } 
                        ?>
                    </div>
                    <!-- //Grid row-->
                </div>
                <div class="col-1 d-flex align-items-center justify-content-center">
                    <!--Controls-->
                    <div class="controls-tabs-dinamicos d-flex justify-content-center">
                        <a class="btn-floating waves-effect waves-light btn-arrow carrousel_right_tabs_dinamicos <?php if($cardsCount == 1) {echo "d-none";} ?>" data-slide="prev" title="Siguiente">
                            <?php echo do_shortcode( '[icono nombre="'.sanitize_title("ver-derecha").'" clase="arrow-left"][/icono]' ); ?>
                        </a>                       
                    </div>
                    <!--Controls-->
                </div>
            </div>

        </div>
    </div>
    <input type="hidden" name="totalTabsDinamicos" id="totalTabsDinamicos" value="<?php echo $l; ?>">
</section><!-- Background -->
<?php } ?>