<?php
/**
 * WP Post Template: desk_2_10_banner_video_DFP
 */
?>

<?php
     /*Tipo de Banner By SRP 22-08-2018*/
     $field = get_field_object('tipo_de_banner');
     $value = $field['value'];
     $label = $field['choices'][ $value ];
 
     switch ($value) {
         case 'wordpress':
             $banner = "wordpress";
         break;
 
         case 'dfp':
             /*Se insertan los plugins que necesita la seccion tal y como se llama el archivo*/
             load_script( $pluginsNecesarios = array('sheetrock', 'handlebars') );
             /*End Se insertan los plugins que necesita la seccion tal y como se llama el archivo*/
             $banner = "dfp";
         break;
         
         default:
             $banner = "wordpress";
         break;
     }
     //echo "TIPO DE BANNER DESKTOP: ". $value;
     /*Tipo de Banner By SRP 22-08-2018*/

    /* Consultamos el Id del post para Obtener su Titulo */
    $desk_idPost = get_the_ID(); 
    $desk_postType = get_post( $idPost ); 
    /* Consultamos el Id del post para Obtener su Titulo */

    /* Declaración de variables */
    $activo = NULL;
    /* Bandera para contabilizar el total de Slides */
    $noSlider = 0;
    $i=0;
    /* End Bandera para contabilizar el total de Slides */ 
    $bannerimgCarrusel = NULL;
    $desk_bannerimgCarrusel = NULL;
    $desk_bannerbtnCarrusel = NULL;
    $permalink = get_permalink($desk_idPost);
    /* End Declaración de variables */

    $desk_callToActionBannerEstatico = $grupoBannerContenido['desk_call_to_action_banner_estatico'];
    $desk_urlCallToActionBannerEstatico = $grupoBannerContenido['desk_url_call_to_action_banner_estatico'];

    /* Obtenemos las imagenes del Slider y las guardamos en un Arreglo */
    if(have_rows("contenido_del_slider_carrusel",$theID) ) { 
        while( have_rows('contenido_del_slider_carrusel') ) { the_row();
            $bannerimgCarrusel = get_sub_field('desk_group_banner_carrusel');

            $desk_bannerimgCarrusel[$i] = array( $bannerimgCarrusel['desk_banner_carrusel']['url'], $bannerimgCarrusel['desk_banner_carrusel']['alt'] );
            $desk_bannerbtnCarrusel[$i] = array( $bannerimgCarrusel['desk_call_to_action_banner_carrusel'], $bannerimgCarrusel['desk_url_call_to_action_banner_carrusel'], $bannerimgCarrusel['desk_color_del_banner_carrusel'] );
            $i++;            
        }
    }
    /* End Obtenemos las imagenes del Slider y las guardamos en un Arreglo */ 
?>
<!--Carousel Wrapper-->
<section id="carousel-banner" class="section-banner carousel slide carousel-fade carousel-banner" data-ride="carousel">
    <!--Slides-->
    <video autoplay muted loop id="myVideo" class="myVideo">
        <source src="https://unitecmx-universidadtecno.netdna-ssl.com/DFP/2017/videos/Mp4/Enfermeria.mp4" type="video/mp4">
        Your browser does not support HTML5 video.
    </video>
    <div id="filter-blue" class="filter-blue"></div>
    <div class="carousel-inner z-depth-0" role="listbox">
        <?php
            $i=0; 
            if(have_rows("contenido_del_slider_carrusel",$theID) ) { 
                while( have_rows('contenido_del_slider_carrusel') ) { the_row();
                    if($i == 0){ $activo = "active"; }
        ?>
        <!-- First slide -->
        <div class="bann_wind item-banner carousel-item <?php echo $activo; ?> carousel-fade lazy">
            <div class="view w-100"><!-- hm-black-light -->
                <div class="mask container d-flex align-items-center justify-content-center p-0 px-md-2">
                    <div class="row col-12 p-0 pl-lg-5">
                        <div class="col-lg-6 col-md-12 col-sm-12 p-0 px-lg-2 cont-banner-mob">
                            <h1 class="texto_claro title-banner cintillo"><?php //echo $desk_postType->post_title; ?><span class="vineta-orange">Bienvenido a la UNITEC</span> <!-- <?php //echo $desk_bannerbtnCarrusel[$i][2]; ?> -->
                            <br class="d-md-none">                            
                                <!--Social buttons-->
                                <div class="banner-share d-inline-block">
                                    <div class="social-reveal">                                        
                                        <?php echo do_shortcode('[desktop_share cerrar="banner_carrusel" pagina="'.$permalink.'" clase="icon-share-banner" tipo="inline-ul"]');?>
                                    </div>
                                    <!-- Validación para Imprimr el icono de Compartir -->
                                    <?php if( strlen( trim( $permalink ) ) > 3 ) { ?>
                                    <a class="share-button"><?php echo do_shortcode( '[icono nombre="'.sanitize_title("compartir").'"][/icono]' ); ?></a>
                                    <?php } ?>
                                </div>
                                <!--/Social buttons-->

                            </h1>
                            <div class="position-mob">
                                <!-- Caption -->
                                <h2 class="texto_claro descripcion-banner mt-lg-5"><?php //echo get_sub_field('descripcion_del_slider'); ?>Licenciatura en Administración de Empresas de Entretenimiento y Comunicación 80c</h2>
                                <h3 class="texto-banner-carousel texto_claro subtitulo-banner"><?php //echo get_sub_field('subtitulo_del_slider'); ?>Estudia la Preparatoria, una Licenciatura o una carrera en Ciencias de la Salud en instalaciones universitarias diseñadas para el aprendizaje. másOcho</h3>
    
                                <a href="https://www.unitec.mx/calcula-tu-beca/" class="btn back-naranja-unitec btn-warning mx-auto waves-effect waves-light" >CONSULTA EL PLAN DE ESTUDIOS 1</a>
                                <!-- Caption -->
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- Caption -->
        </div>
        <!-- First slide -->
        <?php
                    $i++;
                    $noSlider++;
                    $activo = NULL;
                }
            }
        ?>
    </div>
    <!--/.Slides-->
    <!--Indicators--> 
    <ol class="custom-indicadores carousel-indicators indicadores">
        <?php for ($j=0; $j < $noSlider ; $j++) { 
                if($j == 0){ $activo = "active"; }
        ?>
            <li data-target="#carousel-banner" data-slide-to="<?php echo $j ?>" class="<?php echo $activo; ?>"></li>
        <?php       $activo = NULL;
                } ?>
    </ol>
    <!--/.Indicators-->
    <a href="#start-page" class="scroll-spy btnB btn-banner-estatico"><i class="icon-u icon-u-ver-abajo "></i></a>
</section>
<!--/.Carousel Wrapper-->
<div id="start-page"></div>
