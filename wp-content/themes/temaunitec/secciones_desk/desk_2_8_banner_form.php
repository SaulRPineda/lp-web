<?php
/**
 * WP Post Template: desk_2_8_banner_form
 */
?>

<?php
    /* Consultamos el Id del post para Obtener su Titulo */
    $desk_idPost = get_the_ID(); 
    $desk_postType = get_post( $idPost ); 
    /* Consultamos el Id del post para Obtener su Titulo */

    /* Declaración de variables */
    $activo = NULL;
    /* Bandera para contabilizar el total de Slides */
    $noSlider = 0;
    $i=0;
    /* End Bandera para contabilizar el total de Slides */ 
    $bannerimgCarrusel = NULL;
    $desk_bannerimgCarrusel = NULL;
    $desk_bannerbtnCarrusel = NULL;
    $permalink = get_permalink($desk_idPost);
    /* End Declaración de variables */

    $desk_callToActionBannerEstatico = $grupoBannerContenido['desk_call_to_action_banner_estatico'];
    $desk_urlCallToActionBannerEstatico = $grupoBannerContenido['desk_url_call_to_action_banner_estatico'];

    /* Obtenemos las imagenes del Slider y las guardamos en un Arreglo */
    if(have_rows("contenido_del_slider_carrusel",$theID) ) { 
        while( have_rows('contenido_del_slider_carrusel') ) { the_row();
            $bannerimgCarrusel = get_sub_field('desk_group_banner_carrusel');

            $desk_bannerimgCarrusel[$i] = array( $bannerimgCarrusel['desk_banner_carrusel']['url'], $bannerimgCarrusel['desk_banner_carrusel']['alt'] );
            $desk_bannerbtnCarrusel[$i] = array( $bannerimgCarrusel['desk_call_to_action_banner_carrusel'], $bannerimgCarrusel['desk_url_call_to_action_banner_carrusel'], $bannerimgCarrusel['desk_color_del_banner_carrusel'] );
            $i++;            
        }
    }
    /* End Obtenemos las imagenes del Slider y las guardamos en un Arreglo */
?>

<!--Carousel Wrapper-->
<section id="carousel-banner" class="section-banner carousel slide carousel-fade carousel-banner" data-ride="carousel">
    <!--Slides-->
    <div class="carousel-inner z-depth-0" role="listbox">
        <?php
            $i=0; 
            if(have_rows("contenido_del_slider_carrusel",$theID) ) { 
                while( have_rows('contenido_del_slider_carrusel') ) { the_row();
                    if($i == 0){ $activo = "active"; }
        ?>
        <!-- First slide -->
        <div style="background-image: url(<?php echo $desk_bannerimgCarrusel[$i][0]; ?>);" class="bann_wind item-banner carousel-item <?php echo $activo; ?> animated fadeIn" id="desk_banner-item-slider-<?php echo $i; ?>" alt="<?php echo $desk_bannerimgCarrusel[$i][1]; ?>">              
            <div class="view w-100"><!-- hm-black-light -->
                <div class="mask container d-flex align-items-center">
                    <div class="row pl-5 col-12">
                        <div class="col-md-6">
                            <h1 class="<?php echo $desk_bannerbtnCarrusel[$i][2]; ?> title-banner"><?php echo $desk_postType->post_title; ?>    
                            <br class="d-md-none">
                            
                                <!--Social buttons-->
                                <div class="banner-share d-inline-block">
                                    <div class="social-reveal">                                        
                                        <?php echo do_shortcode('[desktop_share cerrar="banner_carrusel" pagina="'.$permalink.'" clase="icon-share-banner" tipo="inline-ul"]');?>
                                    </div>
                                    <!-- Validación para Imprimr el icono de Compartir -->
                                    <?php if( strlen( trim( $permalink ) ) > 3 ) { ?>
                                    <a class="share-button"><?php echo do_shortcode( '[icono nombre="'.sanitize_title("compartir").'"][/icono]' ); ?></a>
                                    <?php } ?>
                                </div>
                                <!--/Social buttons-->

                            </h1>

                            <!-- Caption -->
                            <h1 class="<?php echo $desk_bannerbtnCarrusel[$i][2]; ?> descripcion-banner mt-3"><?php echo get_sub_field('descripcion_del_slider'); ?></h1>
                            <h3 class="texto-banner-carousel <?php echo $desk_bannerbtnCarrusel[$i][2]; ?> subtitulo-banner"><?php echo get_sub_field('subtitulo_del_slider'); ?></h3>
                            <?php if( strlen( trim( $desk_bannerbtnCarrusel[$i][1] ) ) > 3  &&
                                        strlen( trim( $desk_bannerbtnCarrusel[$i][0] ) ) > 3
                                    )
                            { if (strpos($desk_bannerbtnCarrusel[$i][1], 'javascript') !== false) {
                            ?>
                            <a href="<?php echo $desk_bannerbtnCarrusel[$i][1]; ?>" class="btn back-naranja-unitec btn-warning mx-auto waves-effect waves-light" ><?php echo $desk_bannerbtnCarrusel[$i][0]; ?></a>
                            <?php } else {?>
                            <a href="<?php echo $desk_bannerbtnCarrusel[$i][1]; ?>" class="btn back-naranja-unitec btn-warning mx-auto waves-effect waves-light" ><?php echo $desk_bannerbtnCarrusel[$i][0]; ?></a>
                            <?php } }?>
                            <!-- Caption -->
                            
                        </div>
                    </div>
                </div>
            </div>
            <!-- Caption -->
            
        </div>
        <!-- First slide -->
        <?php
                    $i++;
                    $noSlider++;
                    $activo = NULL;
                }
            }
        ?>
    </div>
    <!--/.Slides-->
    <!--Indicators--> 
    <ol class="custom-indicadores carousel-indicators indicadores">
        <?php for ($j=0; $j < $noSlider ; $j++) { 
                if($j == 0){ $activo = "active"; }
        ?>
            <li data-target="#carousel-banner" data-slide-to="<?php echo $j ?>" class="<?php echo $activo; ?>"></li>
        <?php       $activo = NULL;
                } ?>
    </ol>
    <!--/.Indicators-->
    <!-- <app-formulario-expuesto-mobile></app-formulario-expuesto-mobile>   -->
    <app-formulario-expuesto-desktop></app-formulario-expuesto-desktop>  
</section>
<!--/.Carousel Wrapper-->
<span id="start-page"></span>