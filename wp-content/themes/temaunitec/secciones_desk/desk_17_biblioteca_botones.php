<div id="listado"></div>
<?php
/**
 * WP Post Template: desk_17_biblioteca_botones
 */
?>

<?php
    $desk_descripcionBotones = get_field( 'desk_botonesBiblioteca', $theID);
    $desk_Botones = get_field('desk_contenidoBiblioteca', $theID); 
?>

<div class="contenido-biblioteca d-flex"><!-- Background -->
    <main class="container-fluid wow fadeIn d-flex justify-content-center align-items-center">
        <!-- Live preview-->
        <div class="col-lg-11 justify-content-center mt-5 mb-5">
            <!-- Descripción de sección del producto -->
            <article class="col-12 text-center mb-2 mt-0 d-flex justify-content-center" data-wow-offset="50">
                <h6 class="w-100 descripcion"><?php echo $desk_descripcionBotones; ?></h6>
            </article>
            <!-- Descripción de sección del producto -->
            <!-- Botones Biblioteca-->
            <div class="biblioteca-botones ml-3 mr-3 justify-content-center">            
                <!-- Card deck -->
                <div class="row d-flex justify-content-center">
                    <?php foreach ($desk_Botones as $key => $bullet) {

                        switch ($key) {
                            case '0':
                                $claseCard = "color-card-1";
                                $claseIcono = "icon-color-1";
                                break;
                            case '1':
                                $claseCard = "color-card-2";
                                $claseIcono = "icon-color-2";
                                break;
                            case '2':
                                $claseCard = "color-card-3";
                                $claseIcono = "icon-color-3";
                                break;
                            
                            default:
                                break;
                        }

                        $link = ( strpos( strtolower( trim( $bullet['desk_linkBiblioteca'] ) ), "unitec" )  ) ? 'href=' : 'onclick=';
                        $target = ( strpos( strtolower( trim( $bullet['desk_linkBiblioteca'] ) ), "unitec" ) ) ? "target='_blank'" : "";
                     ?>
                    <!-- Card -->
                    <div class="col-xl-4 col-lg-4 col-md-8 col-sm-12 mt-1 materiales-cards pl-2 pr-2">
                        <div class="card p-3 h-100 <?php echo $claseCard; ?>">
                            <div class="card-body biblioteca-card">
                                <!--Title-->
                                <h4 class="biblioteca-title"><?php echo $bullet['desk_tituloBiblioteca']; ?></h4>
                                <!--Text-->
                                <p class="respaldo-text"><?php echo wp_trim_words( $bullet['desk_descripcionBiblioteca'], 40 ); ?></p>    
                            </div>
                            <div class="row extra-info">
                                <div class="col-9 d-flex align-items-center">
                                    <a href="<?php echo $bullet['desk_linkBiblioteca']; ?>" class="biblioteca-links" title="<?php echo $bullet['desk_TextoLinkBiblioteca']; ?>" id="biblioteca_libri_<?php echo $key; ?>" <?php echo $target; ?>><?php echo $bullet['desk_TextoLinkBiblioteca']; ?></a>
                                </div>
                                <div class="col-3 d-flex justify-content-center">
                                    <span><?php echo do_shortcode( '[icono nombre="'.sanitize_title($bullet['desk_iconoBiblioteca']).'" clase="'.$claseIcono.'"][/icono]' ); ?></span>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- Card -->
                    <?php } ?>
                </div>
                <!-- Card deck -->
            </div>
            <!-- Botones Biblioteca-->
        </div>
        <!-- /.Live preview-->
    </main><!-- /.Main layout-->
</div><!-- Background -->
