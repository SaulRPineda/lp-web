<div id="informacion"></div>
<?php
/**
 * WP Post Template: desk_27_card_simple
 */
?>

<?php
    $columna = "col-xl-12";
    $cardPosition = NULL;
?>

<?php if(have_rows("card_basica_tabs",$theID) ) { ?>

<section class="card-simple d-flex"><!-- Background -->
    <div class="container-fluid wow fadeIn d-flex justify-content-center align-items-center p-5">
        <div class="col-lg-11 iconos-unitec justify-content-center">
            <!--Card-->
            <div class="row cardsbullets justify-content-center d-flex">

                <?php

                    //if(have_rows("card_basica_tabs",$theID) ) {
                        while( have_rows('card_basica_tabs') ) { the_row();

                            $desk_TituloBasica = get_sub_field('titulo_del_tab_oferta_basica');

                            while( have_rows('pestanas_basic') ) { the_row();                               
                                $desk_ImagenBasica = get_sub_field('imagen_nueva_basica');                                
                                $desk_btnbasica = get_sub_field('boton_basico');
                                $desk_urlbasica = get_sub_field('url_del_boton_card_basica');
                                $subtitulo = get_sub_field('subtitulo_card_basica');
                ?>

                <?php if ( count( $desk_ImagenBasica ) > 2 ) { 
                        $columna = "col-xl-5";
                        $cardPosition = "card-right";
                ?>
                    <div class="col-lg-12 col-xl-7 d-flex align-items-center justify-content-center">
                        <!-- Card image -->
                        <div class="overlay b-rad">
                            <img data-src="<?php echo $desk_ImagenBasica['url']; ?>" class="img-fluid rounded-0 lazy" alt="<?php echo $desk_ImagenBasica['alt']; ?>">
                        </div>
                        <!-- /Card image -->
                    </div>
               <?php } ?>
                

                <div class="col-lg-12 <?php echo $columna; ?> align-self-center"><!-- Colocarlo solo cuando tenga imagen col-xl-5 -->
                    <!--Card content-->
                    <div class="card-body card text-md-left <?php echo $cardPosition; ?> p-4"><!-- Colocarlo solo cuando tenga imagen card-right -->
                        <div class="texto-int-card h-100 d-flex justify-content-center flex-column">
                            <h4 class="card-title-desk d-flex"><span class="filo-inicio-t pr-3">|</span><?php echo $desk_TituloBasica; ?></h4>
                            <!--Title-->
                            <h3 class="pl-4 card-sub-desk"><?php echo get_sub_field('titulo_card_basica'); ?></h3>
        
                            <?php 
                                if ( strlen( trim($subtitulo) ) > 2 ) { ?>
                                   <h4 class="pl-4 card-sub-int"><?php echo $subtitulo; ?></h4>
                          <?php } ?>                            

                            <!-- Ajustar Estilo -->
                            <div class="card-sub-int"><?php echo get_sub_field('tipo_de_contenido_basica'); ?></div>
                            <!--Text-->
                            
                            <?php if ( strlen( trim($desk_urlbasica) ) > 5 ) { ?>
                            <div class="navegacion d-flex justify-content-end">
                                <a href="<?php echo $desk_urlbasica; ?>" class="ver pull-right" title="<?php echo $desk_btnbasica; ?>">
                                    <?php echo $desk_btnbasica; ?>
                                </a>
                            </div>
                            <?php } ?>
                        </div>
                    </div>
                    <!--/.Card content-->
                </div>
                <!-- <div class="col-lg-1 d-none d-lg-block phantom"></div> -->
                
                <?php 
                        }   /*End While*/
                    }   /*End While*/
                //}   /*End if*/
                ?>

            </div>
            <!--/.Card--> 
        </div>
    </div><!-- /.Main layout-->
</section><!-- Background -->
<?php } ?>

