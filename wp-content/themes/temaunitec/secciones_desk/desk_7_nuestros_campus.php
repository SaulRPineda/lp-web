<div id="nuestro_campus"></div>
<?php
/**
 * WP Post Template: desk_7_nuestros_campus
 */
?>

<?php 
$j=0;
// check if the repeater field has rows of data
if( have_rows('nuestros_campus') ){
    // loop through the rows of data
    while ( have_rows('nuestros_campus') ) { the_row(); 
        // check if the repeater field has rows of data
        if( have_rows('links') ){
            // loop through the rows of data
            while ( have_rows('links') ) { the_row();
                // display a sub field value
                $collapsables_campus[$j] = array( get_sub_field('icono_como_llegar'), get_sub_field('titulo_nuestros_campus'), get_sub_field('link_nuestros_campus') );
                $j++;   
            }
        }
    }
}
?>

<section class="container-fluid wow fadeIn d-flex justify-content-center align-items-center contenedor-principal-campus"><!-- /.Main <layout-->
    <div class="col-lg-11 justify-content-center d-flex align-items-center"> <!-- Contenedor -->
        <section id="elegant-card"><!-- SECTION-->
            <div class="row justify-content-center">
                <article class="col-12 text-center d-flex justify-content-center" data-wow-offset="50">
                    <h4 class="w-75 titleSection">Nuestros Campus</h4>
                </article>
                <article class="col-12 text-center mb-1 mt-1 d-flex justify-content-center" data-wow-offset="50">
                        <h6 class="w-100 descriptionSection"><?php echo get_field( 'desk_descripcionSeccionCampus', $theID ); ?></h6>
                </article>
            </div>
            <div class="row">
                <div class="col-1 d-flex align-items-center justify-content-center">
                    <!--Control izq-->
                    <div class="controls">
                        <a class="btn-floating waves-effect waves-light btn-arrow carrousel_left_oferta" href="#carouselNuestrosCampus" role="button" data-slide="prev" title="Anterior">
                            <?php echo do_shortcode( '[icono nombre="'.sanitize_title("ver-izquierda").'" clase="arrow-right"][/icono]' ); ?>
                        </a>
                    </div>
                    <!--Control izq-->
                </div>
                <div class="col-10">
                    <!--Carousel Wrapper-->
                    <div id="carouselNuestrosCampus" class="carousel slide carousel-fade w-100" data-ride="carouselNuestrosCampus">
                    <?php
                        $active = NULL;
                            if(have_rows("nuestros_campus", $theID) ) {   
                                while( have_rows('nuestros_campus') ) { the_row();
                                    //while( have_rows('campus') ) { the_row();
                                    $campus = get_sub_field('campus');
                                    $links = get_sub_field('links');             
                                    //$campusField = get_sub_field('campus');                                
                                    $taxonomy = 'product_cat';
                                    //$categoty_id = $campusField[0]->term_id;
                                    $categoty_id = $campus[0]->term_id;
                                //term_Ids de todas las subcategorias de Campus
                                $campus= get_term_children($categoty_id, $taxonomy);
                    ?>

                        <!--Slides-->
                        <div class="carousel-inner z-depth-0" role="listbox">
                        <?php
                            //Con los term_Ids obtenemos los Page Templates Asociados
                            foreach ($campus as $key => $value) {
                                if ($key == 0) { $active = "active"; }
                                if($value != NULL) {
                                    $a = get_term_meta($value); 
                                    //va por los terms asociados al post
                                    $id_post=$a['dhvc_woo_category_page_id'][0];
                                    $paginas = get_post($id_post);                     
                                ?>
                                <?php if($id_post != NULL){ ?>
                                <!-- slide -->
                                <div class="carousel-item backface <?php echo $active; ?>">
                                    <!-- Card -->
                                    <div class="w-100">
                                        <!-- card image -->
                                        <div class="col-12 d-flex align-items-center p-0">
                                            <!--Card image-->
                                            <div class="w-100">
                                                <?php
                                                    /*Obtenemos la imagen destacada horizontal del Filtro seleccionado
                                                      Recordando que esta imagen se obtiene del page template asignado
                                                      en la categoria de products
                                                    */
                                                      
                                                    $thumbID = get_field('imagen_destacada_horizontal', $paginas->ID);
                                                    $urlImg = $thumbID["url"];
                                                    $altImg = $thumbID["alt"];
                                                    $urlPage = get_permalink($paginas->ID);

                                                    /*$thumbID = get_post_thumbnail_id( $paginas->ID );
                                                    $urlImg = wp_get_attachment_url( $thumbID );
                                                    $altImg = get_post_meta($thumbID, '_wp_attachment_image_alt', true);
                                                    $urlPage = get_permalink($paginas->ID);*/
                                                    /*End Obtenemos la imagen destacada horizontal del Filtro seleccionado*/

                                                ?>
                                                <div class="imagen-campus pt-2 lazy" data-src="<?php echo $urlImg; ?>" alt="<?php echo $altImg; ?>">
                                                    <span class="campusName"><?php echo $paginas->post_title; ?></span>
                                                </div>
                                            </div>
                                            <!--/Card image-->
                                        </div>

                                        <div class="col-12 align-self-center">
                                            <!--Card content-->
                                            <div class="card-body card cont-card mb-1 p-2">
                                                <div class="texto-int-card">
                                                    <h3 class="card-sub-desk mt-1 pl-3 pr-3"><?php echo $paginas->post_content ?></h3>
                                                    <!--Text-->
                                                    <!-- Inicia botones colapsables -->
                                                    <div class="row d-flex justify-content-center">
                                                        <div class="col-12 d-flex justify-content-around mb-1">
                                                        <?php foreach ($collapsables_campus as $key => $value) { 
                                                            if($paginas->post_title == "Campus en Línea" && $collapsables_campus[$key][2] == "mapa-campus"){
                                                                continue;
                                                            } else if($paginas->post_title == "Campus en Línea" && $collapsables_campus[$key][2] == "galeria-campus"){
                                                                continue;
                                                            }
                                                        ?>                              
                                                            <div type="button" class="botones-campus">
                                                                <a href="<?php echo $urlPage.'#'.$collapsables_campus[$key][2]; ?>" class="link-list-boton d-flex" title="<?php echo $collapsables_campus[$key][1] ?>" data-gtm-tr="OurCampus" data-gtm-campus="<?php echo $paginas->post_title; ?>" data-gtm-ubicacion="<?php echo $collapsables_campus[$key][1] ?>" >
                                                                    <div class="item-icon d-flex align-items-center mr-2">
                                                                        <?php echo do_shortcode( '[icono nombre="'.sanitize_title($collapsables_campus[$key][0]).'"][/icono]' ); ?>
                                                                    </div>
                                                                    <div class="item-description">
                                                                        <?php echo $collapsables_campus[$key][1] ?>        
                                                                    </div>
                                                                </a>
                                                            </div>                           
                                                        <?php } ?>
                                                        
                                                        <?php
                                                            if ($paginas->post_name == "campus-atizapan" || $paginas->post_name == "campus-ecatepec" || $paginas->post_name == "campus-sur" || $paginas->post_name == "campus-toluca" || $paginas->post_name == "campus-los-reyes" || $paginas->post_name == "campus-queretaro"){
                                                        ?>
                                                            <div type="button" class="botones-campus">
                                                                <a href="<?php echo $urlPage; ?>#tour_virtual" class="link-list-boton d-flex" title="Tour Virtual" data-gtm-tr="OurCampus" data-gtm-campus="<?php echo $paginas->post_title; ?>" data-gtm-ubicacion="Tour Virtual">
                                                                    <div class="item-icon d-flex align-items-center mr-2">
                                                                        <?php echo do_shortcode( '[icono nombre="'.sanitize_title('tour-virtual').'"][/icono]' ); ?>
                                                                    </div>
                                                                    <div class="item-description">
                                                                        Tour Virtual 
                                                                    </div>
                                                                </a>
                                                            </div>
                                                        <?php 
                                                            } else{
                                                        ?>
                                                            <div type="button" class="botones-campus">
                                                                <a href="<?php echo $urlPage; ?>" class="link-list-boton d-flex" title="Conoce M&aacute;s" data-gtm-tr="OurCampus" data-gtm-campus="<?php echo $paginas->post_title; ?>" data-gtm-ubicacion="Conoce M&aacute;s">
                                                                    <div class="item-icon d-flex align-items-center mr-2">
                                                                        <?php echo do_shortcode( '[icono nombre="'.sanitize_title('ver-mas').'"][/icono]' ); ?>
                                                                    </div>
                                                                    <div class="item-description">
                                                                        Conoce M&aacute;s
                                                                    </div>
                                                                </a>
                                                            </div>
                                                        <?php } ?>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <!--/.Card content-->
                                        </div>
                                        
                                    </div>
                                    <!--/.Card-->
                                </div>
                                <?php } ?>
                                <!--/.First slide-->
                            <?php } /*end if null*/
                                $active = NULL;
                                } /*end foreach*/                                        
                            ?> <!-- end while -->
                        <?php } ?> <!-- end while -->
                    <?php } ?> <!-- end if -->                                        
                        </div>
                        <!--/.Slides-->
                    </div>
                    <!--/.Carousel Wrapper-->
                </div>
                <div class="col-1 d-flex align-items-center justify-content-center">
                    <!--Control der-->
                    <div class="controls">
                        <a class="btn-floating waves-effect waves-light btn-arrow carrousel_right_oferta <?php echo $none; ?>" href="#carouselNuestrosCampus" role="button" data-slide="next" title="Siguiente">
                            <?php echo do_shortcode( '[icono nombre="'.sanitize_title("ver-derecha").'" clase="arrow-left"][/icono]' ); ?>
                        </a>
                    </div>
                    <!--Control der-->
                </div>
            </div>
        </section>
    </div> <!-- End Contenedor -->
</section>