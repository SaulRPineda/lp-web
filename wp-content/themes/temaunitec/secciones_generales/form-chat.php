<div style="display:none" class="justify-content-center align-items-center modal modal-chat m-0 p-0" id="modal-chat" tabindex="-1" role="dialog" aria-labelledby="label-chat" aria-hidden="true" data-backdrop="static" data-keyboard="false">
  <div class="modal-dialog m-0" role="document">
    <div id="chat-success" class="modal-content card-chat p-md-2 p-1 d-none"></div>
    <div id="frm-chat" class="modal-content p-md-2 p-1">
      <div id="blur-chat">
        <div class="d-flex justify-content-end">
          <button type="button" class="p-0" data-dismiss="modal" aria-label="Close" style="background: 0 0; border: 0">
            <div class="icons-desktop d-flex align-items-center justify-content-center">
              <a href="">
                <i class="icon-u icon-u-cerrar"></i>
              </a>
            </div>
          </button>
        </div>
        <div id="msg-svg" class="d-none">
          <svg class="ldi-z1mv4x" width="100%" height="100%" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" viewBox="0 0 100 20" preserveAspectRatio="xMidYMid"><!--?xml version="1.0" encoding="utf-8"?--><!--Generator: Adobe Illustrator 21.0.0, SVG Export Plug-In . SVG Version: 6.00 Build 0)--><svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="-6px" viewBox="0 0 100 60" style="transform-origin: 50px 50px 0px;" xml:space="preserve"><g style="transform-origin: 50px 50px 0px;"><g style="transform-origin: 50px 50px 0px; transform: scale(0.6);"><g style="transform-origin: 50px 50px 0px; animation-duration: 4s; animation-delay: -4s; animation-direction: normal;" class="ld ld-rush-px-rtl"><g><style type="text/css" style="transform-origin: 50px 50px 0px; animation-duration: 1s; animation-delay: -1s; animation-direction: normal;" class="">.st0{fill:#F4E6C8;} .st1{fill:#333333;} .st2{fill:#E0E0E0;} .st3{fill:#E15C64;} .st4{fill:#C33837;} .st5{fill:#D65A62;} .st6{fill:none;} .st7{fill:#F7B26A;} .st8{fill:#F47E5F;} .st9{opacity:0.2;fill:#353035;} .st10{fill:#666766;} .st11{fill:#A0C8D7;} .st12{fill:#77A4BD;} .st13{fill:#ACBD81;} .st14{fill:#FFFFFF;} .st15{fill:#849B87;} .st16{fill:none;stroke:#E0E0E0;stroke-width:4;stroke-miterlimit:10;} .st17{fill:none;stroke:#333333;stroke-width:4;stroke-miterlimit:10;} .st18{fill:#FFFFFF;stroke:#333333;stroke-width:4;stroke-miterlimit:10;} .st19{fill:none;stroke:#000000;stroke-width:4;stroke-miterlimit:10;} .st20{fill:#FFFFFF;stroke:#000000;stroke-width:4;stroke-miterlimit:10;} .st21{fill:#010101;} .st22{fill:#A5A6A6;} .st23{fill:#666666;} .st24{fill:#F3E4C7;} .st25{fill:none;stroke:#FFFFFF;stroke-linecap:round;stroke-miterlimit:10;} .st26{fill:#353035;} .st27{fill:#B93A38;} .st28{fill:#EA7C60;} .st29{fill:#E0E0E0;stroke:#F7B26A;stroke-width:8;stroke-miterlimit:10;} .st30{fill:none;stroke:#F7B26A;stroke-width:10;stroke-linecap:round;stroke-miterlimit:10;} .st31{fill:none;stroke:#010101;stroke-width:8;stroke-miterlimit:10;} .st32{fill:#F0AF6B;stroke:#010101;stroke-width:8;stroke-miterlimit:10;} .st33{fill:#829985;} .st34{fill:#A8B980;} .st35{fill:#F0AF6B;} .st36{fill:#4A3827;} .st37{fill:#332518;} .st38{fill:#E6E6E6;}</style><g style="transform-origin: 50px 50px 0px;"><g><g style="transform-origin: 50px 50px 0px; animation-duration: 1s; animation-delay: -0.9375s; animation-direction: normal;" class=""><rect x="67.5" y="59.1" class="st34" width="19.6" height="4" fill="#006fba" style="fill: rgb(0, 111, 186);"></rect></g><g style="transform-origin: 50px 50px 0px; animation-duration: 1s; animation-delay: -0.875s; animation-direction: normal;" class=""><rect x="67.5" y="39.3" class="st34" width="25" height="4" fill="#006fba" style="fill: rgb(0, 111, 186);"></rect></g><g style="transform-origin: 50px 50px 0px; animation-duration: 1s; animation-delay: -0.8125s; animation-direction: normal;" class=""><rect x="67.5" y="49.2" class="st34" width="22.6" height="4" fill="#006fba" style="fill: rgb(0, 111, 186);"></rect></g></g></g><g style="transform-origin: 50px 50px 0px; animation-duration: 1s; animation-delay: -0.75s; animation-direction: normal;" class=""><path class="st0" d="M32,57.7L7.5,42.5v22.9c0,3,2.4,5.4,5.4,5.4h43.7c3,0,5.4-2.4,5.4-5.4V42.5L37.5,57.7 C35.8,58.8,33.7,58.8,32,57.7z" fill="rgb(244, 230, 200)" style="fill: rgb(237, 237, 237);"></path></g><g style="transform-origin: 50px 50px 0px; animation-duration: 1s; animation-delay: -0.6875s; animation-direction: normal;" class=""><path class="st7" d="M11.1,39.8l21.1,13.1c0.8,0.5,1.7,0.7,2.6,0.7c0.9,0,1.8-0.2,2.6-0.7l21.1-13.1c2.3-1.4,3.6-3.8,3.6-6.5 c0-2.2-1.8-4-4-4H11.5c-2.2,0-4,1.8-4,4C7.5,35.9,8.9,38.4,11.1,39.8z" fill="rgb(247, 178, 106)" style="fill: rgb(237, 237, 237);"></path></g><metadata xmlns:d="https://loading.io/stock/" style="transform-origin: 50px 50px 0px; animation-duration: 1s; animation-delay: -0.625s; animation-direction: normal;" class="">
<d:name style="transform-origin: 50px 50px 0px; animation-duration: 1s; animation-delay: -0.5625s; animation-direction: normal;" class="">send mail</d:name>
<d:tags style="transform-origin: 50px 50px 0px; animation-duration: 1s; animation-delay: -0.5s; animation-direction: normal;" class="">email,envelope,packet,message,notification,information,letter,send mail,web application</d:tags>
<d:license style="transform-origin: 50px 50px 0px; animation-duration: 1s; animation-delay: -0.4375s; animation-direction: normal;" class="">cc-by</d:license>
<d:slug style="transform-origin: 50px 50px 0px; animation-duration: 1s; animation-delay: -0.375s; animation-direction: normal;" class="">z1mv4x</d:slug>
</metadata></g></g></g></g><style type="text/css" style="transform-origin: 50px 50px 0px; animation-duration: 1s; animation-delay: -0.3125s; animation-direction: normal;" class="">@keyframes ld-rush-px-rtl {
  0% {
    -webkit-transform: translate(100px, 0) skewX(-45deg);
    transform: translate(100px, 0) skewX(-45deg);
    animation-timing-function: cubic-bezier(0, 0.5, 0.5, 1);
  }
  30% {
    -webkit-transform: translate(-20px, 0) skewX(35deg);
    transform: translate(-20px, 0) skewX(35deg);
  }
  45% {
    -webkit-transform: translate(10px, 0) skewX(-15deg);
    transform: translate(10px, 0) skewX(-15deg);
  }
  60% {
    -webkit-transform: translate(-5px, 0) skewX(7deg);
    transform: translate(-5px, 0) skewX(7deg);
  }
  80% {
    -webkit-transform: translate(0, 0) skewX(0deg);
    transform: translate(0, 0) skewX(0deg);
  }
  100% {
    -webkit-transform: translate(-150px, 0) skewX(-45deg);
    transform: translate(-150px, 0) skewX(-45deg);
  }
}
@-webkit-keyframes ld-rush-px-rtl {
  0% {
    -webkit-transform: translate(100px, 0) skewX(-45deg);
    transform: translate(100px, 0) skewX(-45deg);
    animation-timing-function: cubic-bezier(0, 0.5, 0.5, 1);
  }
  30% {
    -webkit-transform: translate(-20px, 0) skewX(35deg);
    transform: translate(-20px, 0) skewX(35deg);
  }
  45% {
    -webkit-transform: translate(10px, 0) skewX(-15deg);
    transform: translate(10px, 0) skewX(-15deg);
  }
  60% {
    -webkit-transform: translate(-5px, 0) skewX(7deg);
    transform: translate(-5px, 0) skewX(7deg);
  }
  80% {
    -webkit-transform: translate(0, 0) skewX(0deg);
    transform: translate(0, 0) skewX(0deg);
  }
  100% {
    -webkit-transform: translate(-150px, 0) skewX(-45deg);
    transform: translate(-150px, 0) skewX(-45deg);
  }
}
.ld.ld-rush-px-rtl {
  -webkit-animation: ld-rush-px-rtl 1.5s infinite linear;
  animation: ld-rush-px-rtl 1.5s infinite linear;
}
</style></svg></svg>
        </div>
        <h4 class="font-weight-bolder text-center px-md-3 px-1 chat-title-text">¡Déjanos tu mensaje!</h4>
        <h6 class="text-center px-md-3 px-1 chat-message-text mb-3" style="font-size:1rem;">Por favor deja un mensaje y te responderemos vía correo electrónico. ¡Gracias!</h6>
        <form class="container" id="formulario-chats" action="">
          <div class="row pl-3 pr-3 d-flex justify-content-center">
            <div class="col-1 p-1 icon-form-chat d-flex align-items-center justify-content-center">
              <i class="icon-u icon-u-nombre cel-chat"></i>
            </div>
            <div class="col-11 px-1 md-form form-sm w-100">
              <input class="form-control py-2" name="name-chat" id="name-chat" onpaste="return false;" type="text">
              <label for="name-chat" class="col-12">Nombre: <span class="color-asterisco">*</span></label>
            </div>
          </div>          
          <div class="row mt-1 pl-3 pr-3 d-flex justify-content-center">
            <div class="col-1 p-1 icon-form-chat d-flex align-items-center justify-content-center">
              <i class="icon-u icon-u-celular cel-chat"></i>
            </div>
            <div class="col-11 px-1 md-form form-sm w-100">
              <input class="form-control py-2" name="phone-chat" id="phone-chat" maxlength="10" onkeypress="return event.charCode >= 48 && event.charCode <= 57" onpaste="return false;" type="tel">
              <label for="phone-chat" class="col-12">Número celular (10 dígitos):<span class="color-asterisco">*</span></label>
            </div>
          </div>        

          <div class="row mt-1 pl-3 pr-3 d-flex justify-content-center">
            <div class="col-1 p-1 icon-form-chat d-flex align-items-center justify-content-center">
              <i class="icon-u icon-u-correo cel-chat"></i>
            </div>
            <div class="col-11 px-1 md-form form-sm w-100">
              <input class="form-control py-2 m_autocomplete typeahead" name="email" id="email-chat" onpaste="return false;" type="email">
              <label for="email" class="col-12">Correo: <span class="color-asterisco">*</span></label>
            </div>
          </div>

          <div class="row mt-1 pl-3 pr-3 d-flex justify-content-center">
            <div class="col-1 p-1 icon-form-chat d-flex align-items-center justify-content-center">
              <i class="icon-u icon-u-egresados cel-chat"></i>
            </div>               

            <div class="col-11 px-1 md-form form-sm w-100 md-select-form">
              <label for="campus" class="pl-3 m-0 active">Elige: <span class="color-asterisco">*</span></label>
              <div class="dropdown inp-depto">
                <a id="depto-chat" class="btn-select dropdown-toggle d-flex align-items-center justify-content-between" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Selecciona un departamento</a>
                <div class="dropdown-menu p-1 m-0 w-100 lineButtonCampus">        
                    <a class="dropdown-item align-items-center campus p-2 w-100" data-queue="4" href="#">Servicios Escolares</a> 
                    <a class="dropdown-item align-items-center campus p-2 w-100" data-queue="2" href="#">Estudios en línea</a> 
                    <a class="dropdown-item align-items-center campus p-2 w-100" data-queue="1" href="#">Licenciatura</a>
                    <a class="dropdown-item align-items-center campus p-2 w-100" data-queue="1" href="#">Preparatoria</a>
                    <a class="dropdown-item align-items-center campus p-2 w-100" data-queue="1" href="#">Ciencias de la Salud</a> 
                    <a class="dropdown-item align-items-center campus p-2 w-100" data-queue="3" href="#">Posgrados</a> 
                    <a class="dropdown-item align-items-center campus p-2 w-100" data-queue="3" href="#">Quiero titularme por seminario</a>
                    <a class="dropdown-item align-items-center campus p-2 w-100" data-queue="1" href="#">Otro</a>                                     
                </div>
              </div>
            </div>
          </div>
          <div class="myFeedBackErrorCTC mx-auto text-center" id="telefono-erroneo">Por favor, ingrese una área válido</div>
          <div class="row mt-1 pl-3 pr-3 d-flex justify-content-center offlineMsg">
            <div class="col-1 p-1 icon-form-chat d-flex align-items-center justify-content-center">
              <i class="icon-u icon-u-correo cel-chat"></i>
            </div>
            <div class="col-11 px-1 md-form form-sm w-100">
              <label for="msj-chat" class="m-0 ml-3" style="top:4px">Escribe tu mensaje: <span class="color-asterisco">*</span></label>
              <textarea id="msj-chat" name="msj-chat" class="rounded-0 py-1" rows="3"></textarea>
            </div>
          </div>
          <div class="frm-politicas" id="chat_frm_politicas_y_condiciones">
            <label _ngcontent-c17="" class="politicas light frm-tradicional-cl text-center my-1" id="form_politicas">
                Al hacer clic en <span id="continuar_politicas">"INICIA UNA CONVERSACIÓN"</span>, reconoces haber leído las 
                <a _ngcontent-c17="" href="//www.unitec.mx/politicas-de-privacidad/" target="_blank">
                Políticas de Privacidad</a> y confirmas estar de acuerdo con el uso de ellas, así como los 
                <a _ngcontent-c17="" href="//www.unitec.mx/terminos-y-condiciones/" target="_blank">
                Términos y Condiciones</a> del sitio
            </label>
          </div>

          <div class="call-me my-2 mt-md-2 mb-2 px-2">
            <a id="init-chat" data-gtm-tr="chat" data-gtm-pos="llenado de formulario"  data-gtm-etiqueta="solicitar conversacion" data-gtm-location="formulario offline" type="button" class="btn btn-primary btn-chat w-100 p-2 m-0"><i class="icon-u icon-u-chat"></i> Envía tu mensaje</a>
          </div>
          
          <div id="chat-alert" class="alert msg-alert text-alert p-2 fade show d-none" role="alert">
            <h3 class="descriptionSection m-0">Por favor, solicita nuevamente el mensaje.</h3>
            <button type="button" class="close text-alert" data-dismiss="alert" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>

        </form>
      </div>
      <div id="proceso-chat"></div>
    </div>
  </div>
</div>
<script src="https://apps.mypurecloud.com/webchat/jsapi-v1.js" type="text/javascript"></script>