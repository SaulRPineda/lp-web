<?php
/**
 * Variable product add to cart
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/single-product/add-to-cart/variable.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see 	https://docs.woocommerce.com/document/template-structure/
 * @author  WooThemes
 * @package WooCommerce/Templates
 * @version 3.0.0
 */
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

global $product;


do_action( 'woocommerce_before_add_to_cart_form' ); ?>
<script>
	 jQuery(document).ready(function() {
	// //	jQuery('.mdb-select').material_select('destroy');
		jQuery('.mdb-select').material_select();
	// //Pregunta si ya trae la cookie de ubicacion
	// 	//Si la trae 
	// 	//Ejecuta funcion de calculo con base a las lat y lng de la cookie
		
	// 	//Si no la trae
	// 	//Ejecuta solicitud de ubicacion 
	// 		//Si no da permiso no hace nada
			
	// 		//Si da permiso 
	// 	 	//crea la cookie y ejecuta funcion para calculo de longitud
	// 		var Ops = new mapaOperations();
	// 		var cookieUbicacion = "c_ubicacion";	
			
	// 		if (Ops.checkCookie(cookieUbicacion)===true) {
	// 			calcula_cercania(Ops.getCookie(cookieUbicacion));
	// 		}else{
	// 			jQuery('.mdb-select').material_select('destroy');
	// 			jQuery('.mdb-select').material_select();
	// 			console.log('no encontro cookie de ubcacion');
	// 			navigator.geolocation.watchPosition(function(position) {
	// 				console.log('si hay navigator geolocation');
    //             	navigator.geolocation.getCurrentPosition(function (position) {
	// 					var valorCookie = position.coords.latitude+','+position.coords.longitude;
	// 					console.log('ya tome la ubicacion');
	// 					Ops.registerCookie(cookieUbicacion, valorCookie); 
	// 					calcula_cercania(Ops.getCookie(cookieUbicacion));
	// 				});
					
	// 			});	

	// 			// if (navigator.geolocation) {
	// 			// 	console.log('si hay navigator geolocation');
    //             // 	navigator.geolocation.getCurrentPosition(function (position) {
	// 			// 	var valorCookie = position.coords.latitude+','+position.coords.longitude;
	// 			// 	Ops.registerCookie(cookieUbicacion, valorCookie);        

	// 			// 	}, function (error) {
	// 			// 		// logs the error if we can't get the users current position for some reason
	// 			// 		console.log(error);
	// 			// 		console.log('no se obtuvi la ubicacion ');
	// 			// 	});
	// 			// 	calcula_cercania(Ops.getCookie(cookieUbicacion));
	// 			// }else{
	// 			// 	console.log('NOOO hay navigator geolocation');
	// 			// 	console.log('no se puede determinar ubicacion del usuario');
	// 			// }
				

	// 		}
	 });
</script>
<script> 
	// Armado de Campus, con slugs y lat y lng debe darse de alta en attributos como description
    var  cities = [
				<?php
				$values = wc_get_product_terms($product->id, 'pa_campus', array('fields' => 'all'));
				foreach ($values as $value) {
					echo '["' . $value->slug . '",' . $value->description . '],';
					//echo '["samara",19.3680369,-99.2585967],';
				}
				?>];
				  
	function calcula_cercania(ubicacion){
		jQuery('.mdb-select').material_select('destroy');
			jQuery('.mdb-select').material_select();
		try {
			var ubicacion=ubicacion.split(",");
			var ubicacionCercana = NearestCity(ubicacion[0], ubicacion[1], cities);
			window.localStorage.usersLocation = ubicacionCercana[0];
			console.log('La ubicacion mas cercana es:'+ubicacionCercana[0]);
			
			preseleccion_select(ubicacionCercana[0],'pa_campus');
		}catch(err) {
			console.log("Sin ubicación: " + err);
		}
	}

	function Deg2Rad(deg) {
                return deg * Math.PI / 180;
            }
 
	function PythagorasEquirectangular(lat1, lon1, lat2, lon2) {
		lat1 = Deg2Rad(lat1);
		lat2 = Deg2Rad(lat2);
		lon1 = Deg2Rad(lon1);
		lon2 = Deg2Rad(lon2);
		var R = 6371; // km
		var x = (lon2 - lon1) * Math.cos((lat1 + lat2) / 2);
		var y = (lat2 - lat1);
		var d = Math.sqrt(x * x + y * y) * R;
		return d;
	}

	function NearestCity(latitude, longitude, locations) {
                var mindif = 99999;
                var closest;

                for (index = 0; index < locations.length; ++index) {
                    var dif = PythagorasEquirectangular(latitude, longitude, locations[index][1], locations[index][2]);
                    if (dif < mindif) {
                        closest = index;
                        mindif = dif;
                    }
                }

                var closestLocation = (locations[closest]);
                return closestLocation;
			}
			
	function preseleccion_select(campus,id_select){

				    jQuery("#"+id_select+" [value="+campus+"]").attr("selected", "selected"); 
					jQuery('.mdb-select').material_select('destroy');
					jQuery('.mdb-select').material_select();

			}
  </script>

  <script>
       

     

            

            

            
			
			

			</script>


<form class="variations_form cart" method="post" enctype='multipart/form-data' data-product_id="<?php echo absint( $product->get_id() ); ?>" data-product_variations="<?php echo htmlspecialchars( wp_json_encode( $available_variations ) ) ?>">
	<?php do_action( 'woocommerce_before_variations_form' ); ?>

	<?php if ( empty( $available_variations ) && false !== $available_variations ) : ?>
		<p class="stock out-of-stock"><?php _e( 'This product is currently out of stock and unavailable.', 'woocommerce' ); ?></p>
	<?php else : ?>
		<div class="variations " cellspacing="0" data-wow-offset="50">
			<div class="row">
					<?php foreach ( $attributes as $attribute_name => $options ) : ?>
						<div class="value col-6 select-box">
							<?php
								$class = 'mdb-select force-select-style';
								$show_option_none = "Selecciona ";
								$label_sel = $attribute_name;
								$selected = isset( $_REQUEST[ 'attribute_' . sanitize_title( $attribute_name ) ] ) ? wc_clean( stripslashes( urldecode( $_REQUEST[ 'attribute_' . sanitize_title( $attribute_name ) ] ) ) ) : $product->get_variation_default_attribute( $attribute_name );
								wc_dropdown_variation_attribute_options( array( 'options' => $options, 'attribute' => $attribute_name, 'product' => $product, 'selected' => $selected, 'name' => $name,'id'=>$id,'class'=>$class, 'show_option_none'=> $show_option_none, 'label_sel'=> $label_sel, 'clear-link => ' ) );
							?>
						</div>
					<?php endforeach;?>
			</div>
			<?php // echo end( $attribute_keys ) === $attribute_name ? apply_filters( 'woocommerce_reset_variations_link', '<a class="reset_variations" href="#">' . esc_html__( 'Clear', 'woocommerce' ) . '</a>' ) : ''; ?>
		</div>

		<?php do_action( 'woocommerce_before_add_to_cart_button' ); ?>

		<div class="single_variation_wrap">
			<?php
				/**
				 * woocommerce_before_single_variation Hook.
				 */
				do_action( 'woocommerce_before_single_variation' );

				/**
				 * woocommerce_single_variation hook. Used to output the cart button and placeholder for variation data.
				 * @since 2.4.0
				 * @hooked woocommerce_single_variation - 10 Empty div for variation data.
				 * @hooked woocommerce_single_variation_add_to_cart_button - 20 Qty and cart button.
				 */




	


				do_action( 'woocommerce_single_variation' );

				

			
			?>
		</div>

		<?php do_action( 'woocommerce_after_add_to_cart_button' ); ?>
	<?php endif; ?>

	<?php //do_action( 'woocommerce_after_variations_form' ); ?>
</form>

<?php
//do_action( 'woocommerce_after_add_to_cart_form' );