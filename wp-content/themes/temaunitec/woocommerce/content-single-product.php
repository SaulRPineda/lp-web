<?php
/**
 * The template for displaying product content in the single-product.php template
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/content-single-product.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see 	    https://docs.woocommerce.com/document/template-structure/
 * @author 		WooThemes
 * @package 	WooCommerce/Templates
 * @version     3.0.0
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}
global $woocommerce, $product, $post; //Importante carga toda la info del producto
// var_dump($product->attributes);

// $e=get_post_meta($post->ID, '_product_attributes', true);



// echo "<br>------";
// var_dump($e);

// var_dump(get_taxonomy('pa_fecha-de-inicio'));
// echo "aaaaa:".$p=get_taxonomy('pa_fecha-de-inicio')->labels->name;


// echo "<br><br><br><br><br><br>-------------<br><br><br><br><br><br>";
// echo "<pre>".print_r($product->get_attributes())."</pre>";
// var_dump($product->get_data());
// var_dump($product->get_terms());

//echo "lllslalasla: ".$size = $product->get_attribute( 'pa_fecha-de-inicio' );




//echo '<br>_____________________<br>';


//echo "FICHA TECNICA";
        //Getting product attributes
        $product_attributes = $product->get_attributes();
			//var_dump($product_attributes);
        

		//do_action( 'woocommerce_single_product_summary' );
		
?>

<?php
	/**
	 * woocommerce_before_single_product hook.
	 *
	 * @hooked wc_print_notices - 10
	 */
	 do_action( 'woocommerce_before_single_product' );

	 if ( post_password_required() ) {
	 	echo get_the_password_form();
	 	return;
	 }
?>





<?php


					/******FUNCION PARA TRAER LAS PLANTILLAS ASIGNADAS A ESTA PAGINA********/
					if( wp_is_mobile() ){
						/*echo "es tipo mobil";*/
						$secciones=get_post_meta($post->ID,"_page_section_order_fieldmobile");	
						$carpeta="secciones_mobile/";
					}else{
						/*echo "es tipo desk";*/
						$secciones=get_post_meta($post->ID,"_page_section_order_field");
						$carpeta="secciones_desk/";
					}
					$secciones=explode(',', str_replace('_php','', str_replace('.', '_', $secciones[0]) ) );
					foreach ($secciones as $key => $value) {
						/*echo "......".$carpeta.$value."....<br>";*/
						get_template_part($carpeta.$value);
					}
					/***********************************************************************/
?>

