<div class="align-items-center justify-content-center modal modal-ctc p-0" id="modal-ctc" tabindex="-1" role="dialog" aria-labelledby="label-ctc" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div id="msg-success" class="modal-content card-blue p-md-3 p-1"></div>
    <div id="frm-ctc" class="modal-content p-md-3 p-1">
      <div id="blur-ctc">
        <div class="d-flex justify-content-end">
          <button type="button" class="p-0" data-dismiss="modal" aria-label="Close" style="background: 0 0; border: 0">
            <div class="icons-desktop d-flex align-items-center justify-content-center">
              <a href="">
                <i class="icon-u icon-u-cerrar"></i>
              </a>
            </div>
          </button>
        </div>
        <h4 class="titleSection text-center px-md-5 px-1" id="label-ctc">Hablar con un asesor ahora</h4>
        <h6 class="descriptionSection text-center px-md-5 px-1" id="label-ctc">Introduce tu teléfono y enseguida uno de nuestros asesores educativos te llamará de forma gratuita.</h6>
        <form class="container" id="formulario-ctc" action="">
          <div class="row mt-1">
            <div class="col-12 d-flex justify-content-center">
              <div class="form-check form-check-inline mb-0">
                <input type="radio" id="customRadio1" value="9" name="customRadio" class="custom-control-input">
                <label class="custom-control-label mb-0" for="customRadio1">Fijo</label>
              </div>
              <div class="form-check form-check-inline mb-0">
                <input type="radio" id="customRadio2" value="8" name="customRadio" class="custom-control-input">
                <label class="custom-control-label mb-0" for="customRadio2">Celular</label>
              </div>
            </div>
            <div class="myFeedBackErrorCTC mx-auto" id="err-valor">Por favor, seleccione una opción</div>
          </div>
          <div class="row mt-1 pl-3 pr-3 d-flex justify-content-center">
            <div class="col-1 p-1 identificacion d-flex align-items-center justify-content-center">
              <span class="icon-u-celular"></span>
            </div>
            <div class="col-11 p-1 md-form form-sm w-100">
              <input class="form-control" name="ctc-phone" id="ctc-phone" maxlength="10" onpaste="return false;" type="tel">
              <label for="ctc-phone" class="col-12"><span class="color-asterisco">*</span> Número telefónico (10 dígitos):</label>
            </div>
          </div>
          <div class="myFeedBackErrorCTC mx-auto text-center" id="err-tel">Por favor, ingrese un número válido</div>
          <input id="Cel" type="hidden" value="">

          <div class="call-me mt-1 mt-md-2 mb-2">
            <button id="llamada-ctc" type="button" class="btn btn-primary btn-frm btn-frm-ctc p-2" data-gtm-tr="pushCall" data-gtm-accion="confirmación ctc">Solicitar llamada</button>
          </div>
          
          <div id="msg-alert" class="alert msg-alert text-alert p-2 fade show" role="alert">
            <h3 class="descriptionSection m-0">Por favor, solicita nuevamente la llamada.</h3>
            <button type="button" class="close text-alert" data-dismiss="alert" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
        </form>
      </div>
      <div id="proceso-ctc"></div>
    </div>
  </div>
</div>