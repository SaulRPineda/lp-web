var nom_carreras = new Array(),currentLT = 0, asec = new Array(), costos = new Array(), campusOrigen = new Array(), carreras = new Array();
asec[0] =	new Array("06/0351","7 de Abril del 2006"); // prepa eca, ecav, mar; 
asec[1] =	new Array("142","24 de Octubre de 1988"); 	// prepa eca, sur; facs: ati, cui, eca, ecan; ing: todas; mae: todas; esp todas;
var m = "3.5 % QUINCENAL", // por incumplimiento
p = "$400.00" ,xmlCarreras; // prorroga
var Catcampus= new Array();
Catcampus[1] =  new Array('atizapan');
Catcampus[2] =  new Array('cuitlahuac');
Catcampus[3] =  new Array('ecatepec');
Catcampus[4] =  new Array('marina');
Catcampus[5] =  new Array('sur');
Catcampus[6] =  new Array('toluca')
Catcampus[7] =  new Array('leon');
Catcampus[9] = new Array('guadalajara');
//Pago Unico, Primer Pago, Pago 2 y 3, 4 pagos iguales asi es como se agregan las cantidades
costos[0] = new Array(); // no se usa
/*
0 - Pago Mayor diferido [4 o 5 Pagos]
1 - Mininmo Materias
2 - Maximo Materias
3 - Ciclo
4 - Vigencia
5 - Pago Unico
6 - Tres Pagos
7 - Diez Pagos [solo maestrias semestrales]// opciones a meses de pago
*/

//revisar costos ejecutivas toluca hibridas ingenierias
log = function(s){
	//console.log(s);
}

/*Prepa*/
campusOrigen[0] = new Array(2,3,1,3,4,38,"",48,54);
costos[2] = new Array(790,1,7,'18-1','02 de Enero de 2017',3000,Array(1235,930),Array(1,3,4)); 	// prepa ati ok  
costos[1] = new Array(745,1,7,'18-1','02 de Enero de 2017',2800,Array(1155,865),Array(1,3,4)); 	// prepa eca ok
costos[4] = new Array(815,1,7,'18-1','02 de Enero de 2017',3095,Array(1270,955),Array(1,3,4)); 	// prepa sur ok 
costos[3] = new Array(745,1,7,'18-1','02 de Enero de 2017',2865,Array(1170,875),Array(1,3,4)); 	// prepa mar-cui ok 
costos[38] = new Array(775,1,7,'18-1','02 de Enero de 2017',2900,Array(1195,900),Array(1,3,4)); 	// prepa tol ok 
costos[48] = new Array(450,1,7,'18-1','02 de Enero de 2017',1690,Array(695,520),Array(1,3,4)); 	// prepa leon ok
costos[54] = new Array(660,1,7,'18-1','02 de Enero de 2017',2525,Array(1035,775),Array(1,3,4)); 	// prepa guadalajara ok 
/*Prepa*/

/*FAcs*/
campusOrigen[1] = new Array(5,6,7,6,9,39,"",49,55);
costos[5] = new Array(970,1,7,'18-1','02 de Enero de 2017',3715,Array(1535,1150),Array(1,3,4)); 	// FACS. atizapan ok
costos[7] = new Array(900,1,7,'18-1','02 de Enero de 2017',3395 ,Array(1395,1045),Array(1,3,4)); 	// FACS. eca ok
costos[9] = new Array(970,1,7,'18-1','02 de Enero de 2017',3725,Array(1530,1150),Array(1,3,4)); 	// FACS. sur ok 
costos[6] = new Array(910,1,7,'18-1','02 de Enero de 2017',3465,Array(1425,1070),Array(1,3,4)); 	// FACS. cui ok
costos[8] = new Array(555,1,7,'18-1','02 de Enero de 2017',1890,Array(830,625),Array(1,3,4)); 	// ING. Corporativas todos los campus
costos[39] = new Array(910,1,7,'18-1','02 de Enero de 2017',3430,Array(1410,1060),Array(1,3,4)); 	// FACS. toluca ok
costos[49] = new Array(760,1,7,'18-1','02 de Enero de 2017',2875,Array(1180,885),Array(1,3,4)); 	// FACS. leon ok
costos[55] = new Array(790,1,7,'18-1','02 de Enero de 2017',3020,Array(1240,930),Array(1,3,4)); 	// FACS guadalajara ok

/*FAcs*/


/*ING*/
campusOrigen[2] = new Array(10,11,12,11,13,40,"",52,57);
costos[10] = new Array(1000,1,7,'18-1','02 de Enero de 2017',3835,Array(1580,1185),Array(1,3,4)); 	// Ing. atizapan ok
costos[12] = new Array(930,1,7,'18-1','02 de Enero de 2017',3555,Array(1465,1100),Array(1,3,4)); 	// Ing. eca ok 
costos[13] = new Array(1020,1,7,'18-1','02 de Enero de 2017',3890,Array(1600,1200),Array(1,3,4)); 	// Ing. sur ok
costos[11] = new Array(940,1,7,'18-1','02 de Enero de 2017',3565,Array(1480,1110),Array(1,3,4)); 	// Ing. cui-mar ok 
costos[40] = new Array(935,1,7,'18-1','02 de Enero de 2017',3565,Array(1470,1105),Array(1,3,4)); 	// Ing. toluca ok
/*Habia dos de toluca*/
///costos[52] = new Array(765,1,7,'17-2','02 de Enero de 2017',2915,Array(1200,900),Array(1,3,4)); 	// Ing. toluca ok
/**/
costos[52] = new Array(795,1,7,'18-1','02 de Enero de 2017',3020,Array(1245,935),Array(1,3,4)); 	// Ing. leon ok
costos[57] = new Array(820,1,7,'18-1','02 de Enero de 2017',3135,Array(1285,965),Array(1,3,4));     //Ing. guadalajara ok
/*ING*/

costos[14] = new Array(3050,1,7); 	// Ing. ati

/*Maestrias Cuatrimestrales*/
//costos[15] = new Array(7380); // Maestrías cuat 0 - 4, sem

/*pago unico, primero, segundo, 4 pagos,*/
//WWcostos[16] = new Array(8640); // Esp. cuat , Sem
costos[17] = new Array(72830,0,0); // Esp. odo.

/*Esp. Cuatri*/
//costos[18] = new Array(7030); // Esp.  cuatrimestrales Altas
costos[19] = new Array(2205,1,7); 	// prepa atizapan

/*Maestrias Semestrales  precenciales*/
costos[20] = new Array(1850,1,4,'18-2','01 de Febrero de 2017',9915,Array(4040,3030),6000,Array(1,3,6,10)); 
//maestr leon
costos[300] = new Array(895,1,4,'18-2','01 de Febrero de 2017',4785,Array(1950,1465),Array(1,3,6)); 
//maest toluca
costos[400] = new Array(1290,1,4,'18-2','01 de Febrero de 2017',6935,Array(2830,2125),Array(1,3,6)); 
/*Especialidades Semestrales*/
costos[21] = new Array(7690); 

 

/*Hibridas Todos los campus, corporativas e hibridas leon */
costos[22] = new Array(530,1,7,'18-1','02 de Enero de 2017',1805,Array(790,590),Array(1,3,4)); // FACS. Corporativas todos los campus  ok

/*Corporativa campus guadalajara*/
costos[202] = new Array(510,1,7,'18-1','02 de Enero de 2017',1740,Array(760,570),Array(1,3,4)); // FACS. hibridas guadalajara ok

/*Corporativas campus Toluca*/
costos[47] = new Array(530,1,7,'18-1','02 de Enero de 2017',1810,Array(790,595),Array(1,3,4)); // FACS. Corporativas ok

/*  NO EXISTEN!! hibridas campus guadalajara*/
costos[201] = new Array(510,1,7,'18-1','02 de Enero de 2017',1740,Array(760,570),Array(1,3,4)); // FACS. Corporativas guadalajara ok


/*Licenciaturas Online*/
costos[23] = new Array(530,1,7,'18-1','02 de Enero de 2017',1805,Array(790,590),Array(1,3,4)); //ok

/*Ingenierias nuevas Online*/
costos[200] = new Array(555,1,7,'18-1','15 de Mayo de 2017',1890,Array(830,625),Array(1,3,4)); //ok  535 800,600  1825

/*Nutricion*/
campusOrigen[3] = new Array(24,25,26,25,27,41,"",53,56);
costos[24] = new Array(1000,1,7,'18-1','02 de Enero de 2017',3835,Array(1580,1185),Array(1,3,4)); //Atizapan ok
costos[26] = new Array(930,1,7,'18-1','02 de Enero de 2017',3555,Array(1465,1100),Array(1,3,4)); //Eca ok
costos[27] = new Array(1020,1,7,'18-1','02 de Enero de 2017',3890,Array(1600,1200),Array(1,3,4)); //Sur ok
costos[25] = new Array(940,1,7,'18-1','02 de Enero de 2017',3565,Array(1480,1110),Array(1,3,4)); //Cui-mar
costos[41] = new Array(935,1,7,'18-1','02 de Enero de 2017',3565,Array(1470,1105),Array(1,3,4)); //toluca ok 
costos[53] = new Array(795,1,7,'18-1','02 de Enero de 2017',3020,Array(1245,935),Array(1,3,4)); //LEON ok 
costos[56] = new Array(820,1,7,'18-1','02 de Enero de 2017',3135,Array(1285,965),Array(1,3,4)); //guadalajar ok 


/*Fisioterapia*/
campusOrigen[4] = new Array(42,45,43,45,44,46,51,58);
costos[42] = new Array(1000,1,7,'18-1','02 de Enero de 2017',3835,Array(1580,1185),Array(1,3,4)); //Atizapan ok
costos[43] = new Array(930,1,7,'18-1','02 de Enero de 2017',3555,Array(1465,1100),Array(1,3,4)); //Eca ok
costos[44] = new Array(1020,1,7,'18-1','02 de Enero de 2017',3890,Array(1600,1200),Array(1,3,4)); //Sur ok
costos[45] = new Array(940,1,7,'18-1','02 de Enero de 2017',3565,Array(1480,1110),Array(1,3,4)); //Cui-mar ok
costos[46] = new Array(935,1,7,'18-1','02 de Enero de 2017',3565,Array(1470,1105),Array(1,3,4)); //toluca ok
costos[51] = new Array(795,1,7,'18-1','02 de Enero de 2017',3020,Array(1245,935),Array(1,3,4)); //Leon ok
costos[58] = new Array(820,1,7,'18-1','02 de Enero de 2017',3135,Array(1285,965),Array(1,3,4)); //guadalajara ok


/*Gastronomia*/
costos[59] = new Array(1145,1,7,'18-1','15 de Mayo de 2017',4435,Array(1810,1360),Array(1,3,4)); //Atizapan ok  1105  1745,1310  4285
costos[61] = new Array(1145,1,7,'18-1','15 de Mayo de 2017',4435,Array(1810,1360),Array(1,3,4)); //Ecatepec ok


/*Maestrias Semestrales  Online*/
costos[28] = new Array(1380,1,4,'18-1','01 de Febrero de 2017',7385,Array(3020,2265),4400,Array(1,3,6,10)); 

/*Enfermeria*/
costos[29] = new Array(685,1,7,'18-1','02 de Enero de 2017',2550,Array(1060,795),Array(1,3,4)); //zona metro 
costos[50] = new Array(610,1,3,'18-1','02 de Enero de 2017',2300,Array(950,710),Array(1,3,4)); //leon
costos[30] = new Array(585,1,3,'18-1','02 de Enero de 2017',2220,Array(915,685),Array(1,3,4)); //hospitalarias
costos[60] = new Array(610,1,7,'18-1','02 de Enero de 2017',2335,Array(955,720),Array(1,3,4)); //guadalajara
/*Turisticas*/
costos[31] = new Array(1105,1,7,'18-1','02 de Enero de 2017',4285,Array(1745,1310),Array(1,3,4));  //
/*Cirujano Dentista*/
costos[32] = new Array(Array(1440,1445),1,7,'18-1','01 de Febrero de 2017',7570,Array(3960,1980),Array(1,3,6));
/*Especialidades*/
costos[33] = new Array(18515,0,0,'18-1','01 de Febrero de 2017',94835,Array(49400,24700),Array(1,3,6)); 
//Especialidad Materia Adicional
costos[34] = new Array(3090,0,0,'18-1','01 de Febrero de 2017',15805,Array(8235,4120),Array(1,3,5));
/*Maestrias Cuatri*/
costos[35] = new Array(1895,1,3,'','',7380,Array(2990,2245),Array(1,3,4));
/*Diplomados*/
costos[36] = new Array(1850,1,4,'17-2','01 de Febrero de 2017',9915,Array(4040,3030),6000,Array(1,3,6,10)); 
/*Diplomados Online*/
costos[37] = new Array(1380,1,4,'17-2','01 de Febrero de 2017',7385,Array(3020,2265),4400,Array(1,3,6,10));

jQuery(function(){
	$("#colegUNITEC").data("campus","0");
	$("#colegUNITEC").data("oferta","0");
	$("#colegUNITEC").data("tipoOferta","0");
	$("#colegUNITEC").data("procesado","0");
	$("#carouselCampus, #carouselCarreras").touchCarousel({					
		pagingNav: false,
		snapToItems: true,
		itemsPerMove: 1,				
		scrollToLast: false,
		loopItems: false,
		scrollbar: false
	});
	//Evento seleccion Campus
	var arrCampus = [1,2,3,4,5,6,7,8,9];	
	jQuery(".campus > img").each(function(index, element) {
        jQuery(this).click(function(e) {
        		//console.log(arrCampus[index] + " " +index);
			setCampus(arrCampus[index],index);
		}); 
    });
	jQuery(".tituloCampus").each(function(index, element) {
       jQuery(this).click(function(e) {

			setCampus(arrCampus[index],index);			
		}); 
    });
	//Cargamos el xml de Carreras en un array, solo una carga con toda la info
	jQuery.ajax({type: "GET",url: wpurl+"/colegiaturas/carreras.php",dataType: "xml",success: function(xml){	xmlCarreras = xml;}});
	//console.log(wpurl+"/colegiaturas/carreras.php");
	addEventOfertaUnitec();
});
//-->
function setCampus(campus,index){
	//Definimos Campus
	jQuery("#colegUNITEC").data("campus",campus);	
	//SET SELECCOIONADO
	jQuery(".campus > img").removeClass("campSel").addClass("imgCampus");
	jQuery("#imgCampus_"+campus+"").addClass("campSel");
	//jQuery(".carrera:eq(3)").hide();
	//jQuery(".carrera:not(:eq(3))").fadeIn();
	
	//RESET VARIABLES IF CAMPUS HAS DATA
	if(jQuery("#colegUNITEC").data("campus")!="0"){
		jQuery("#colegUNITEC").data("oferta","0");
		jQuery("#colegUNITEC").data("tipoOferta","0");
		jQuery("#colegUNITEC").data("procesado","0");
		jQuery("#unitecFinal").slideUp();
	}
		
	resetCarrera(0);
	jQuery(".carrera").show();
	$(".campus").unblock();		
	if(campus==7){ //Campus Online	
	//	jQuery(".campus:not(:eq(6))").block({message:null, overlayCSS:  { backgroundColor: '#000'} });
		jQuery(".carrera:eq(0), .carrera:eq(2), .carrera:eq(5), .carrera:eq(6)").hide();
	}
	else if(campus==6){ //Camppus Toluca
		jQuery(".carrera:eq(3), .carrera:eq(5)").hide();
	//	jQuery(".campus:eq(6)").block({message:null, overlayCSS:  { backgroundColor: '#000'} });
	}
	else if(campus==4){//Marina
		jQuery(".carrera:eq(2)").hide();
	//	jQuery(".campus:eq(6)").block({message:null, overlayCSS:  { backgroundColor: '#000'} });
	}
	else if(campus==8){//LEON
		jQuery(".carrera:eq(3), .carrera:eq(5)").hide();
	//	jQuery(".campus:eq(6)").block({message:null, overlayCSS:  { backgroundColor: '#000'} });
	}
	else if(campus==9){//GUADALAJARA
		jQuery(".carrera:eq(4), .carrera:eq(5), .carrera:eq(3)").hide();
	//	jQuery(".campus:eq(6)").block({message:null, overlayCSS:  { backgroundColor: '#000'} });
	}
	else{		
		jQuery(".carrera").show();
	//	jQuery(".campus:eq(6)").block({message:null, overlayCSS:  { backgroundColor: '#000'} });
	}
	//jQuery(".carrera:eq(3)").block({message:null, overlayCSS:  { backgroundColor: '#000'} });
	//addEventOfertaUnitec();
	
	//Calcular	
	loadTODO(0);
}
//-->
function addEventOfertaUnitec(){
	var ofertas = ['P','LIC','LC','DIP','SP','ESP','HLX'];
	jQuery(".carrera").each(function(index, element) {
        jQuery(this).off("click").on("click",function(e) {
			//Resetaamos posiciones y valor de oferta
			jQuery("#colegUNITEC").data("oferta","0");		
			resetCarrera(element);							
			if(index!=0){
				jQuery("#"+element.id).css("background","url(http://www.unitec.mx/wp-content/themes/temaunitec/colegiaturas/linea_"+element.id+".png) 50% 20% no-repeat");			
				//Cargamos las carreras correspondiens de acuerdo al nivel
			}		
			//cargamos carreras
			parseCarreraXml(xmlCarreras,element.id);
			jQuery("#colegUNITEC").data("tipoOferta",element.id);
		});
	});
	//Evitamos la propagacion del evento en los hijos
	jQuery('.ofertaUnitec').click(function(event) {
		var agent = jQuery.browser;
		if(agent.msie) {			
			event.cancelBubble = true;
			event.stopPropagation();
		} else {
			event.stopPropagation();
		}
	});
}
//-->
//-->
function resetCarrera(elementSel){
	//validamos si ya fue procesado todo apra evitar reset de carreras
	var procesado = jQuery("#colegUNITEC").data("procesado");
	var campus = jQuery("#colegUNITEC").data("campus");
	if(procesado==0){
		jQuery("#programa").remove();
		//jQuery(".carrera").css({"height":"90px","width":"200px"});	
		jQuery(".carrera").each(function(index, element) {
			if(element.id != elementSel.id){
				jQuery("#"+element.id).css("background","url(http://www.unitec.mx/wp-content/themes/temaunitec/colegiaturas/linea_"+element.id+".png) 50% 40% no-repeat");
			}
		});
	}
}
//-->
function parseCarreraXml(xml,carrera){
	var cssP, campus;
	if(carrera=="PREPA"){
		cssP = "style='display:none;'";	
	}else{
		cssP = "";	
	}
	//console.log("parseCarreraXml ",carrera);
	campus = jQuery("#colegUNITEC").data("campus");
	//creamos en el momento el elemento drop
	var dropProgramas = '<select onchange="javascript:loadTODO(this.value);" class="programas" id="programa" name="programa" '+cssP+' ></select>';
	jQuery("#"+carrera+" > .ofertaUnitec").append(dropProgramas);	
	//Obtenemos el elemento select
	var select = jQuery("#programa");
	if(select.prop) {
		var options = select.prop('options');
	}else{
		var options = select.attr('options');	
	}	
	jQuery('option', select).remove();
	if(carrera!="PREPA"){
		log("no es prepa");
		//Definimos la opcion seleccione
		options[options.length] = new Option('', '0', true, true);
		//Iteramos todos los elementos de carreras
		jQuery(xml).find("carrera").each(function(){	
			if(jQuery(this).attr("nivelOpcion")==carrera){
				//console.log(jQuery(this).attr("nivelOpcion"));
				//console.log(campus);
				//Separar carreras ONLINE de Tradicionales
				switch(campus){									
					case 7:
					//console.log(7);
						if(jQuery(this).attr("LN")=="DIPON" || jQuery(this).attr("LN") == "LON" || jQuery(this).attr("LN") == "ON"){
							if(parseInt(jQuery(this).attr("codigo"))>0){
								options[options.length] = new Option(replaceAccents(jQuery(this).attr("nombre")), jQuery(this).attr("codigo"));
							}							
						}
					break;
					case 6:

						if(parseInt(jQuery(this).attr("codigoD"))>0 && jQuery(this).attr("LN") != "GASTRO"){
							options[options.length] = new Option(replaceAccents(jQuery(this).attr("nombre")), jQuery(this).attr("codigoD"));
						}
					break;	
					case 8:
					//console.log(8);
						if(parseInt(jQuery(this).attr("codigoLEO"))>0){
							//console.log(jQuery(this).attr("nombre"));
							options[options.length] = new Option(replaceAccents(jQuery(this).attr("nombre")), jQuery(this).attr("codigoLEO"));
						}
					break;
					case 9:
					//console.log("campus guadalajara ",9);
						if(jQuery(this).attr("codigoGDL")== "true"){
							//console.log(jQuery(this).attr("nombre"));
							options[options.length] = new Option(replaceAccents(jQuery(this).attr("nombre")), jQuery(this).attr("codigoD"));
						}
					break;
					case 1:
					case 3:
					//console.log("atizapan ecatepec");
						if(jQuery(this).attr("LN") != "LON" && jQuery(this).attr("LN") != "ON"){
							if(parseInt(jQuery(this).attr("codigo"))>0 || parseInt(jQuery(this).attr("codigoD"))=="1221"){
								options[options.length] = new Option(replaceAccents(jQuery(this).attr("nombre")), jQuery(this).attr("codigo"));
							}	
						}
					break;							
					default:
					//console.log("default");
						//console.log(jQuery(this).attr("nombre"));
						if(jQuery(this).attr("LN") != "LON" && jQuery(this).attr("LN") != "ON"){
							if(parseInt(jQuery(this).attr("codigo"))>0 && jQuery(this).attr("LN") != "GASTRO"){
								options[options.length] = new Option(replaceAccents(jQuery(this).attr("nombre")), jQuery(this).attr("codigo"));
							}
						}
					break;
				}
			}
		});
		//ordenamos los elementos del select
		orderSelect();		
	}else{			
			log("si es prepa");	
		var codigo;
		jQuery(xml).find("carrera").each(function(){		
			if(jQuery(this).attr("nivelOpcion")=="P"){		
				if(campus==6){
					log("entra a campus 6");
					options[options.length] = new Option(jQuery(this).attr("nombre"), jQuery(this).attr("codigoD"));
					codigo = jQuery(this).attr("codigoD");
				}else{
					log("no entra a campus 6");
					options[options.length] = new Option(jQuery(this).attr("nombre"), jQuery(this).attr("codigo"));
					codigo = jQuery(this).attr("codigo");
				}
			}
		});				
		 jQuery("#colegUNITEC").data("tipoOferta","P")
		 log("oferta antes de entrar "+codigo);
		loadTODO(codigo);
	}
}
//-->
function orderSelect(){
	jQuery("select").each(function() {			
		// Keep track of the selected option.
		var selectedValue = jQuery(this).val();			
		// Sort all the options by text. I could easily sort these by val.
		jQuery(this).html(
			jQuery("option",jQuery(this)).sort(function(a, b) {
				return a.text == b.text ? 0 : a.text < b.text ? -1 : 1
			})
		);	
		// Select one option.
		jQuery(this).val(selectedValue);
	});
}
//--
function replaceAccents(carrera){
	var arrAccents = Array('Contaduría','Pública','Administración','Tecnologías','Información','Comunicación','Pedagogía','Psicología','Ingeniería','Creación','Economía','Química','Electrónica','Mecánica','Enfermería','Nutrición','Mecatrónica','Electrónicos','Investigación','Énfasis','Robótica','Turísticos','Maestría','Educación','Tecnología','Dirección','Migración','Odontología','Pediátrica');
	var arrNoAccents = Array('Contaduria','Publica','Administracion','Tecnologias','Informacion','Comunicacion','Pedagogia','Psicologia','Ingenieria','Creacion','Economia','Quimica','Electronica','Mecanica','Enfermeria','Nutricion','Mecatronica','Electronicos','Investigacion','enfasis','Robotica','Turisticos','Maestria','Educacion','Tecnologia','Direccion','Migracion','Odontologia','Pediatrica');
	var arrCarrera = carrera.split(" ");
	var nombre_carrera = "";
	var positionArr = 0;
	for(i=0;i<=(arrCarrera.length-1);i++){ //iteramos todas las
		positionArr = jQuery.inArray(arrCarrera[i], arrNoAccents);
		if(positionArr>0){
			nombre_carrera += arrAccents[positionArr]+" ";			
		}else{
			nombre_carrera += arrCarrera[i]+" ";
		}
	}
	return nombre_carrera;
}
//-->
function loadTODO(codigo){
	if(codigo!=0){
		jQuery("#colegUNITEC").data("oferta",codigo);
	}
	//console.log("el codigo al inicio ",codigo);
	var campusSelect = parseInt(jQuery("#colegUNITEC").data("campus")),
	ofertaSelect = parseInt(jQuery("#colegUNITEC").data("oferta")),
	tipoOfertaSelect = jQuery("#colegUNITEC").data("tipoOferta"), nivel  = "",programa = jQuery("#programa").val();
	if(campusSelect != 0 && ofertaSelect == 0){
		jQuery("#areaCarreras").fadeIn("fast").animate({
			opacity:1,
			marginTop: '15px'
		  },500,'easeInOutQuad',function(){
		});
	}else if(campusSelect != 0 && ofertaSelect != 0){
		jQuery(xmlCarreras).find("carrera").each(function(){	
			if(campusSelect==6 || campusSelect==9 || campusSelect==8){
				//console.log("entra a campusSelect 6 ");
				if(jQuery(this).attr("codigoD")==ofertaSelect){
					nivel = jQuery(this).attr("LN");
				}
			}else{
				//console.log("no entra a campusSelect 6 ",ofertaSelect);
				if(jQuery(this).attr("codigo")==ofertaSelect){
									//console.log("no entra a campusSelect 6 aabhjbgj");
					nivel = jQuery(this).attr("LN");
				}
			}
		});
		//console.log("el nivel en la condicion es "+nivel);
		currentLT++;
		jQuery("#unitecFinal").slideUp();
		//console.log(campusSelect+"-----"+ofertaSelect+"-----"+tipoOfertaSelect+"-----"+campusOrigen[0][(campusSelect-1)]+"-------"+campusOrigen[0][(campusSelect)]);						
		if(currentLT == 1){
			setTimeout(function(){		
				var origen = 0,
				costoFinal = 0,
				costoFinal2 = "",
				calcPagos = "",
				minMateria = 0,
				maxMateria = 0,
				totalArray = 0,
				positionCostos = 0,
				positionNumCostos = 7,
				info = "",
				urlPDF = "";			
				//Proceso para mostrar el precio
				//console.log("el nivel "+nivel);
				/*alert(nivel);*/
				switch(nivel){
					
					case 'P':/*Preparatoria*/
						origen = campusOrigen[0][(campusSelect-1)];

						positionCostos = origen;
						costoFinal = Array(costos[origen][5],costos[origen][6],costos[origen][0]);	
						minMateria	= costos[origen][1];	
						maxMateria = costos[origen][2];
						totalArray = costos[origen].length;
						info += "<span style='font-size:13px; color:#006FBA; font-weight:bo[ld;'>Colegiaturas para el ciclo "+costos[origen][3];
						info += "<br>Vigente a partir del "+ costos[origen][4]+"</span>";					
						
						urlPDF = "preparatoria_"+Catcampus[campusSelect]+".pdf";
						if(campusSelect==8){urlPDF = "preparatoria_leon.pdf";}


					break;
					case 'GASTRO':/*Gastronomia*/
					//console.log("entra a gastro");
							costoFinal = Array(costos[61][5],costos[61][6],costos[61][0]);	
							minMateria	= costos[61][1];	
							maxMateria = costos[61][2];	
							totalArray = costos[61].length;
							positionCostos = 23;
							info += "<span style='font-size:13px; color:#006FBA; font-weight:bold;'>Colegiaturas para el ciclo "+costos[61][3];
							info += "<br>Vigente a partir del "+ costos[61][4]+"</span>";
							urlPDF = "gastronomia_atizapan.pdf";
							if(campusSelect == 3){
								urlPDF = "gastronomia_ecatepec.pdf";
							}

					break;
					case 'T': case 'TI': case 'C': case 'L':
						origen = campusOrigen[1][(campusSelect-1)];
						positionCostos = origen;	
						costoFinal = Array(costos[origen][5],costos[origen][6],costos[origen][0]);	
						minMateria	= costos[origen][1];	
						maxMateria = costos[origen][2];	
						totalArray = costos[origen].length;
						info += "<span style='font-size:13px; color:#006FBA; font-weight:bold;'>Colegiaturas para el ciclo "+costos[origen][3];
						info += "<br>Vigente a partir del "+ costos[origen][4]+"</span>";
						if(nivel=="TI"){
							info += "<br><br>*Del 1ro al 4to Cuatrimestre."+
									"<br>A partir del 5to Cuatrimestre la materia aplica costo de Ingeniería según el campus.";								
						}else if(nivel=="C"){
							info += "<br><br>*Del 1ro al 5to Cuatrimestre."+
									"<br>A partir del 6to Cuatrimestre la materia aplica costo de Ingeniería según el campus.";
						}		
						if(campusSelect==8){
							urlPDF = "licenciatura_leon.pdf";
						}else{
							urlPDF = "licenciatura_"+Catcampus[campusSelect]+".pdf";
						}
								
					break;
					case 'SIS': case 'OIN': case 'DIS':
						origen = campusOrigen[2][(campusSelect-1)];	
						//console.log(campusOrigen[2][(campusSelect-1)]+"-----------"+campusSelect);
						positionCostos = origen;		
						costoFinal = Array(costos[origen][5],costos[origen][6],costos[origen][0]);	
						minMateria	= costos[origen][1];	
						maxMateria = costos[origen][2];	
						totalArray = costos[origen].length;
						info += "<span style='font-size:13px; color:#006FBA; font-weight:bold;'>Colegiaturas para el ciclo "+costos[origen][3];
						info += "<br>*Vigente a partir del "+ costos[origen][4]+"</span>";
						urlPDF = "ingenieria_"+Catcampus[campusSelect]+".pdf";
						if(campusSelect==8){
							urlPDF = "ingenieria_leon.pdf";
						}else{
							urlPDF = "ingenieria_"+Catcampus[campusSelect]+".pdf";
						}
					break;
					case 'TURI':
						costoFinal = Array(costos[31][5],costos[31][6],costos[31][0]);	
						minMateria	= costos[31][1];	
						maxMateria = costos[31][2];	
						totalArray = costos[31].length;
						positionCostos = 31;
						info += "<span style='font-size:13px; color:#006FBA; font-weight:bold;'>Colegiaturas para el ciclo "+costos[31][3];
						info += "<br>Vigente a partir del "+ costos[31][4]+"</span>";
						urlPDF = "turismo.pdf";						
					break;
					case 'NUTRI':
						origen = campusOrigen[3][(campusSelect-1)];	
						costoFinal = Array(costos[origen][5],costos[origen][6],costos[origen][0]);	
						minMateria	= costos[origen][1];	
						maxMateria = costos[origen][2];	
						totalArray = costos[origen].length;
						positionCostos = origen;
						info += "<span style='font-size:13px; color:#006FBA; font-weight:bold;'>Colegiaturas para el ciclo "+costos[origen][3];
						info += "<br>Vigente a partir del "+ costos[origen][4]+"</span>";
						
						urlPDF = "nutricion_"+Catcampus[campusSelect]+".pdf";//carreras[programa][4];						
						if(campusSelect==8){
						urlPDF = "nutricion_leon.pdf";//carreras[programa][4];						
						}
					break;
					case'LON':
						//Añadido para las 2 nuevas ingenierias online
						if(programa ==  1149  || programa == 1146){
							costoFinal = Array(costos[200][5],costos[200][6],costos[200][0]);	
							minMateria	= costos[200][1];	
							maxMateria = costos[200][2];	
							totalArray = costos[200].length;
							positionCostos = 23;
							info += "<span style='font-size:13px; color:#006FBA; font-weight:bold;'>Colegiaturas para el ciclo "+costos[200][3];
							info += "<br>Vigente a partir del "+ costos[200][4]+"</span>";
							urlPDF = "ingenieria_online.pdf";
						}
						else{
							costoFinal = Array(costos[23][5],costos[23][6],costos[23][0]);	
							minMateria	= costos[23][1];	
							maxMateria = costos[23][2];	
							totalArray = costos[23].length;
							positionCostos = 23;
							info += "<span style='font-size:13px; color:#006FBA; font-weight:bold;'>Colegiaturas para el ciclo "+costos[23][3];
							info += "<br>Vigente a partir del "+ costos[23][4]+"</span>";
							urlPDF = "licenciatura_online.pdf";
						}
					break;
					case 'ENFER':
					//console.log("")
						if(campusSelect==8){ //LEON
						costoFinal = Array(costos[50][5],costos[50][6],costos[50][0]);	
						costoFinal2 = Array(costos[50][5],costos[50][6],costos[50][0]);	
						minMateria	= costos[50][1];	
						maxMateria = costos[50][2];
						totalArray = costos[50].length;	
						positionCostos = 50;
						info += "<span style='font-size:13px; color:#006FBA; font-weight:bold;'>Colegiaturas para el ciclo "+costos[29][3];
						info += "<br>Vigente a partir del "+ costos[29][4]+"</span>";
						info += "<br><br>*A partir del 4to Cuatrimestre se incluyen las Clínicas Hospitalarias.";	
						urlPDF = "enfermeria_leon.pdf";	

						}else if(campusSelect==6){
							costoFinal = Array(costos[29][5],costos[29][6],costos[29][0]);	
						//costoFinal2 = Array(costos[30][5],costos[30][6],costos[30][0]);	
						minMateria	= costos[29][1];	
						maxMateria = costos[29][2];
						totalArray = costos[29].length;	
						positionCostos = 29;
						info += "<span style='font-size:13px; color:#006FBA; font-weight:bold;'>Colegiaturas para el ciclo "+costos[29][3];
						info += "<br>Vigente a partir del "+ costos[29][4]+"</span>";
						info += "<br><br>*A partir del 4to Cuatrimestre se incluyen las Clínicas Hospitalarias.";	
						urlPDF = "enfermeria_toluca.pdf";


						}
						else if(campusSelect==9){
							costoFinal = Array(costos[60][5],costos[60][6],costos[60][0]);	
						//costoFinal2 = Array(costos[30][5],costos[30][6],costos[30][0]);	
						minMateria	= costos[60][1];	
						maxMateria = costos[60][2];
						totalArray = costos[60].length;	
						positionCostos = 60;
						info += "<span style='font-size:13px; color:#006FBA; font-weight:bold;'>Colegiaturas para el ciclo "+costos[60][3];
						info += "<br>Vigente a partir del "+ costos[60][4]+"</span>";
						info += "<br><br>*A partir del 4to Cuatrimestre se incluyen las Clínicas Hospitalarias.";	
						urlPDF = "enfermeria_guadalajara.pdf";	


						}						
						else{

						costoFinal = Array(costos[29][5],costos[29][6],costos[29][0]);	
						costoFinal2 = Array(costos[29][5],costos[30][6],costos[29][0]);	
						minMateria	= costos[29][1];	
						maxMateria = costos[29][2];
						totalArray = costos[29].length;	
						positionCostos = 29;
						info += "<span style='font-size:13px; color:#006FBA; font-weight:bold;'>Colegiaturas para el ciclo "+costos[29][3];
						info += "<br>Vigente a partir del "+ costos[29][4]+"</span>";
						info += "<br><br>*A partir del 4to Cuatrimestre se incluyen las Clínicas Hospitalarias.";	
						urlPDF = "enfermeria.pdf";
						}				
						
					break;	
					case 'FISIO':
					if(campusSelect==8){

						costoFinal = Array(costos[51][5],costos[51][6],costos[51][0]);	
						minMateria	= costos[51][1];	
						maxMateria = costos[51][2];
						totalArray = costos[51].length;	
						positionCostos = 51;
						info += "<span style='font-size:13px; color:#006FBA; font-weight:bold;'>Colegiaturas para el ciclo "+costos[51][3];
						info += "<br>Vigente a partir del "+ costos[51][4]+"</span>";
						info += "<br><br>*A partir del 4to Cuatrimestre se incluyen las Clínicas Hospitalarias.";	
						urlPDF = "fisioterapia_leon.pdf";	
					}else{

						origen = campusOrigen[4][(campusSelect-1)];
						if(campusSelect==9){
							origen = campusOrigen[4][7];
						}

						costoFinal = Array(costos[origen][5],costos[origen][6],costos[origen][0]);	
						minMateria	= costos[origen][1];	
						maxMateria = costos[origen][2];	
						totalArray = costos[origen].length;
						positionCostos = origen;
						info += "<span style='font-size:13px; color:#006FBA; font-weight:bold;'>Colegiaturas para el ciclo "+costos[origen][3];
						info += "<br>Vigente a partir del "+ costos[origen][4]+"</span>";
						info += "<br><br>*A partir del 4to Cuatrimestre se incluyen las Clínicas Hospitalarias.";	
						urlPDF = "fisioterapia_"+Catcampus[campusSelect]+".pdf";
					}
							
					break;	
					case 'CD':
						costoFinal = Array(costos[32][5],costos[32][6],costos[32][0]);	
						minMateria	= costos[32][1];	
						maxMateria = costos[32][2];
						totalArray = costos[32].length;
						positionCostos = 32;
						info += "<span style='font-size:13px; color:#006FBA; font-weight:bold;'>Colegiaturas para el ciclo "+costos[32][3];
						info += "<br>Vigente a partir del "+ costos[32][4]+"</span>";
						urlPDF = "odontologia.pdf";	
					break;
					case 'LC': case 'LC2': case 'LC3': case 'LC4': case 'LC5':/*Licenciaturas Corportativas*/
					//console.log("entre a las corporativas");
						if(programa==671 || programa==870 || programa==879){ //ING
							costoFinal = Array(costos[8][5],costos[8][6],costos[8][0]);		
							minMateria	= costos[8][1];	
							maxMateria = costos[8][2];
							totalArray = costos[8].length;
							positionCostos = 8;
							info += "<span style='font-size:13px; color:#006FBA; font-weight:bold;'>Colegiaturas para el ciclo "+costos[8][3];
							info += "<br>Vigente a partir del "+ costos[8][4]+"</span>";	
							urlPDF = "ingenieria_corporativa.pdf";
							if(campusSelect==8){
								urlPDF = "ingenieria_corporativa_leon.pdf";
							}
						}else{ //FACS


							if(campusSelect == 8){
								costoFinal = Array(costos[22][5],costos[22][6],costos[22][0]);	
								minMateria	= costos[22][1];	
								maxMateria = costos[22][2];	
								totalArray = costos[22].length;
								positionCostos = 22;
								info += "<span style='font-size:13px; color:#006FBA; font-weight:bold;'>Colegiaturas para el ciclo "+costos[22][3];
								info += "<br>Vigente a partir del "+ costos[22][4]+"</span>";
								urlPDF = "licenciatura_corporativa_leon.pdf";
							}

							else if(campusSelect == 9){
								costoFinal = Array(costos[202][5],costos[202][6],costos[202][0]);	
								minMateria	= costos[202][1];	
								maxMateria = costos[202][2];	
								totalArray = costos[202].length;
								positionCostos = 202;
								info += "<span style='font-size:13px; color:#006FBA; font-weight:bold;'>Colegiaturas para el ciclo "+costos[202][3];
								info += "<br>Vigente a partir del "+ costos[202][4]+"</span>";								
								urlPDF = "licenciatura_corporativa_guadalajara.pdf";
							}
							else if(campusSelect == 6){
								costoFinal = Array(costos[47][5],costos[47][6],costos[47][0]);	
								minMateria	= costos[47][1];	
								maxMateria = costos[47][2];	
								totalArray = costos[47].length;
								positionCostos = 47;
								info += "<span style='font-size:13px; color:#006FBA; font-weight:bold;'>Colegiaturas para el ciclo "+costos[47][3];
								info += "<br>Vigente a partir del "+ costos[47][4]+"</span>";								
								urlPDF = "licenciatura_corporativa_toluca.pdf";
							}
							else{
								costoFinal = Array(costos[22][5],costos[22][6],costos[22][0]);	
								minMateria	= costos[22][1];	
								maxMateria = costos[22][2];	
								totalArray = costos[22].length;
								positionCostos = 22;
								info += "<span style='font-size:13px; color:#006FBA; font-weight:bold;'>Colegiaturas para el ciclo "+costos[22][3];
								info += "<br>Vigente a partir del "+ costos[22][4]+"</span>";
								urlPDF = "licenciatura_corporativa.pdf";
							}

						}
					break;


					case 'HLX':/*Licenciaturas Corportativas*/
						//Campus toluca con lics hibridas
						///console.log("entro a corporativas");
         				if(campusSelect==6){	
							if(programa==671 || programa==870 || programa==879){ //ING
								costoFinal = Array(costos[22][5],costos[22][6],costos[22][0]);		
								minMateria	= costos[22][1];	
								maxMateria = costos[22][2];
								totalArray = costos[22].length;
								positionCostos = 22;
								info += "<span style='font-size:13px; color:#006FBA; font-weight:bold;'>Colegiaturas para el ciclo "+costos[22][3];
								info += "<br>Vigente a partir del "+ costos[8][4]+"</span>";	
								urlPDF = "ingenieria_corporativa_toluca.pdf";
							}
							else if(programa==3763 || programa==3756 || programa==3751|| programa==3763|| programa==3759|| programa==3753|| programa==3754|| programa==3762|| programa==3761){ //ING
								costoFinal = Array(costos[47][5],costos[47][6],costos[47][0]);	
								minMateria	= costos[47][1];	
								maxMateria = costos[47][2];	
								totalArray = costos[47].length;
								positionCostos = 47;
								info += "<span style='font-size:13px; color:#006FBA; font-weight:bold;'>Colegiaturas para el ciclo "+costos[47][3];
								info += "<br>Vigente a partir del "+ costos[47][4]+"</span>";
								urlPDF = "licenciatura_hibrida_toluca.pdf";

						}
							else{ //FACS
								costoFinal = Array(costos[47][5],costos[47][6],costos[47][0]);	
								minMateria	= costos[47][1];	
								maxMateria = costos[47][2];	
								totalArray = costos[47].length;
								positionCostos = 47;
								info += "<span style='font-size:13px; color:#006FBA; font-weight:bold;'>Colegiaturas para el ciclo "+costos[47][3];
								info += "<br>Vigente a partir del "+ costos[47][4]+"</span>";
								urlPDF = "licenciatura_corporativa_toluca.pdf";
							}

					}
					//Campus leon con lics hibridas
					else if(campusSelect == 8 && (programa==3756 || programa==3754 || programa==3759 || programa==3753)){
								costoFinal = Array(costos[22][5],costos[22][6],costos[22][0]);	
								minMateria	= costos[22][1];	
								maxMateria = costos[22][2];	
								totalArray = costos[22].length;
								positionCostos = 22;
								info += "<span style='font-size:13px; color:#006FBA; font-weight:bold;'>Colegiaturas para el ciclo "+costos[22][3];
								info += "<br>Vigente a partir del "+ costos[22][4]+"</span>";
								urlPDF = "licenciatura_hibrida_leon.pdf";

					}
					//Los demas campus con ingenierias hibridas
					else{
						if(programa==671 || programa==870 || programa==879){ //ING
							costoFinal = Array(costos[22][5],costos[22][6],costos[22][0]);		
							minMateria	= costos[22][1];	
							maxMateria = costos[22][2];
							totalArray = costos[22].length;
							positionCostos = 22;
							info += "<span style='font-size:13px; color:#006FBA; font-weight:bold;'>Colegiaturas para el ciclo "+costos22[3];
							info += "<br>Vigente a partir del "+ costos[8][4]+"</span>";	
							urlPDF = "ingenieria_hibrida.pdf";
					  	}
						  
						  
					  if(campusSelect==6){  //FAcs Toluca
						costoFinal = Array(costos[22][5],costos[22][6],costos[22][0]);	
						minMateria	= costos[47][1];	
						maxMateria = costos[47][2];	
						totalArray = costos[47].length;
						positionCostos = 47;
						info += "<span style='font-size:13px; color:#006FBA; font-weight:bold;'>Colegiaturas para el ciclo "+costos[47][3];
						info += "<br>Vigente a partir del "+ costos[47][4]+"</span>";
						urlPDF = "licenciatura_corporativa_toluca.pdf";
					  	}
						
						
						else{ //FACS
							costoFinal = Array(costos[22][5],costos[22][6],costos[22][0]);	
							minMateria	= costos[22][1];	
							maxMateria = costos[22][2];	
							totalArray = costos[22].length;
							positionCostos = 22;
							info += "<span style='font-size:13px; color:#006FBA; font-weight:bold;'>Colegiaturas para el ciclo "+costos[22][3];
							info += "<br>Vigente a partir del "+ costos[22][4]+"</span>";
							urlPDF = "licenciatura_corporativa.pdf";
							log("el prorama " + programa);
							if(campusSelect == 9){
								urlPDF = programa == "Psicología" ? "licenciatura_corporativa_guadalajara.pdf" : "licenciatura_hibrida_guadalajara.pdf"
							}
						}

						if(programa==3763 || programa==3756 || programa==3751|| programa==3763|| programa==3759|| programa==3753|| programa==3754|| programa==3762|| programa==3761){ //ING
							costoFinal = Array(costos[22][5],costos[22][6],costos[22][0]);	
							minMateria	= costos[22][1];	
							maxMateria = costos[22][2];	
							totalArray = costos[22].length;
							positionCostos = 22;
							//info += "<span style='font-size:13px; color:#006FBA; font-weight:bold;'>Colegiaturas para el ciclo "+costos[22][3];
							//info += "<br>Vigente a partir del "+ costos[22][4]+"</span>";
							urlPDF = "licenciatura_hibrida.pdf";
							if(campusSelect == 9){
								costoFinal = Array(costos[202][5],costos[202][6],costos[202][0]);	
								minMateria	= costos[202][1];	
								maxMateria = costos[202][2];	
								totalArray = costos[202].length;
								positionCostos = 202;
								urlPDF = "licenciatura_hibrida_guadalajara.pdf";
							}

						}





					}

						
					break;

					case 'ESP': /*Especialidad Odontologia*/
						costoFinal = Array(costos[33][5],costos[33][6],costos[33][0]);	
						costoFinal2 = Array(costos[34][5],costos[34][6],costos[34][0]);	
						minMateria	= costos[33][1];	
						maxMateria = costos[33][2];	
						totalArray = costos[33].length;
						positionCostos = 33;
						info += "<span style='font-size:13px; color:#006FBA; font-weight:bold;'>Colegiaturas para el ciclo "+costos[33][3];
						info += "<br>Vigente a partir del "+ costos[33][4]+"</span>";					
						urlPDF = 'especialidad.pdf';	
					break; 
					case 'ON':
						costoFinal = Array(costos[28][5],costos[28][6],costos[28][0],costos[28][7]);	
						minMateria	= costos[28][1];	
						maxMateria = costos[28][2];	
						totalArray = costos[28].length;
						positionCostos = 28;
						positionNumCostos = 8;
						info += "<span style='font-size:13px; color:#006FBA; font-weight:bold;'>Colegiaturas para el ciclo "+costos[28][3];
						info += "<br>Vigente a partir del "+ costos[28][4]+"</span>";
						urlPDF = 'maestria_online.pdf';
					break;
					case 'SP':
				/*	alert('maestria-leon');*/ 
						costoFinal = Array(costos[20][5],costos[20][6],costos[20][0],costos[20][7]);		
						minMateria	= costos[20][1];	
						maxMateria = costos[20][2];	
						totalArray = costos[20].length;
						positionCostos = 20;
						positionNumCostos = 8;
						info += "<span style='font-size:13px; color:#006FBA; font-weight:bold;'>Colegiaturas para el ciclo "+costos[20][3];
						info += "<br>Vigente a partir del "+ costos[20][4]+"</span>";
						info += "<br><br>*Aplican mismos costos para seminario de Titulación";	
						urlPDF = 'maestria.pdf';
						if(campusSelect == 8){
							costoFinal = Array(costos[300][5],costos[300][6],costos[300][0]);		
						minMateria	= costos[300][1];	
						maxMateria = costos[300][2];	
						totalArray = costos[300].length;
						positionCostos = 20;
						positionNumCostos = 8;
						
							urlPDF = "maestria_leon.pdf"
						}
						if(campusSelect == 6){
								costoFinal = Array(costos[400][5],costos[400][6],costos[400][0]);		
						minMateria	= costos[400][1];	
						maxMateria = costos[400][2];	
						totalArray = costos[400].length;
						positionCostos = 20;
						positionNumCostos = 8;
						
							urlPDF = "maestria_toluca.pdf"
						}
					break; 
					case 'DIP': 
						costoFinal = Array(costos[36][5],costos[36][6],costos[36][0],costos[36][7]);		
						minMateria	= costos[36][1];	
						maxMateria = costos[36][2];	
						totalArray = costos[36].length;
						positionCostos = 36;
						positionNumCostos = 8;
						info += "<span style='font-size:13px; color:#006FBA; font-weight:bold;'>Colegiaturas para el ciclo "+costos[36][3];
						info += "<br>Vigente a partir del "+ costos[36][4]+"</span>";
						info += "<br><br>*Aplican mismos costos para seminario de Titulación";	
						urlPDF = 'maestria.pdf';
						if(campusSelect == 8){
							urlPDF = "maestria_leon.pdf"
						}
						if(campusSelect == 6){
							urlPDF = "maestria_toluca.pdf"
						}
					break;
					case 'DIPON':
						costoFinal = Array(costos[37][5],costos[37][6],costos[37][0],costos[37][7]);		
						minMateria	= costos[37][1];	
						maxMateria = costos[37][2];	
						totalArray = costos[37].length;
						positionCostos = 37;
						positionNumCostos = 8;
						info += "<span style='font-size:13px; color:#006FBA; font-weight:bold;'>Colegiaturas para el ciclo "+costos[37][3];
						info += "<br>Vigente a partir del "+ costos[37][4]+"</span>";
						info += "<br><br>*Aplican mismos costos para seminario de Titulación";	
						urlPDF = 'maestria_online.pdf';
					break;	
				}
				info += "<br><br>*Precio de Lista."+
					   "<br>*No incluye Beca,descuentos ni promociones."+
					   "<br>*Colegiaturas para alumnos que ingresaron apartir de Septiembre 2008."+
					   "<br>*Si eres alumno que ingreso antes de Septiembre de 2008 consulta el costo en servicios en línea."+
					   "<br><br>";				
				var costosColeg = totalArray-4;
				//TABS INICIO
				calcPagos += "<div class='section'><h3>Colegiatura para: "+jQuery("#programa option[value="+ofertaSelect+"]").text()+"</h3>"+
					"<ul class='tabs'>";
				var classLi = "",	
					classBox = "";
				for(i=0;i<(costosColeg-1);i++){	
					if(i==2){
						classLi = "class='current'";
					}else{
						classLi = "";
					}
					var pago = (i==0) ? "Pago" : "Pagos";
					calcPagos += "<li "+classLi+">"+costos[positionCostos][positionNumCostos][i]+" "+pago+"</li>";
				}
				calcPagos += "</ul>";
				for(i=0;i<(costosColeg-1);i++){				
					if(i==2){
						classBox= "visible";
					}else{
						classBox = "";
					}												
					calcPagos += "<div class=\"box "+classBox+"\">";																								
						if(i==1){ // CASO PARA 2 y 3 PAGOS
							if(minMateria==1){				
								calcPagos +=	"<div class='numMaterias'>Por <span id='numMaterias_"+i+"'>1 materia</span> pagarás:</div>";
							}else{
								calcPagos +=	"<div class='numMaterias'><span>Carga completa:</span></div>";
							}						


							calcPagos += 	"<div class='costoMateria'>"+
											"<span id='costoMaterias_"+i+"'>$"+CurrencyFormatted(costoFinal[i][0])+"*</span></div>"+
											"<div class='costoMateria2'>y 2 pagos de <span id='costoMaterias_2_"+i+"'>$"+CurrencyFormatted(costoFinal[i][1])+"*</span></div>";
											
											
							//TURISMO PRACTICAS
							if(programa==722 || programa == 723){														
								calcPagos += "<div class='costoMateria' style='padding-top:20px;line-height: 45px; font-size: 25px;'><span style='color:#999;'>Prácticas:</span> 1 Pago de $"+CurrencyFormatted("10870")+"* y <br> 2 Pagos de $"+CurrencyFormatted("8155")+"* </div>";
							}	

							//Enfermeria
							if(programa==1150){	
								//console.log('efnermeria campus:'+campusSelect);
								if(campusSelect==8){
								calcPagos += "<div class='costoMateria' style='padding-top:20px;line-height: 45px; font-size: 25px;'><span style='color:#999;'>Clinicas A:</span> 1 Pago de $"+CurrencyFormatted("2840")+"* y <br> 2 Pagos de $"+CurrencyFormatted("2135")+"* </div>";
								calcPagos += "<div class='costoMateria' style='padding-top:20px;line-height: 45px; font-size: 25px;'><span style='color:#999;'>Clinicas B:</span> 1 Pago de $"+CurrencyFormatted("3790")+"* y <br> 2 Pagos de $"+CurrencyFormatted("2845")+"* </div>";

								}else if(campusSelect==6){
									//console.log('TOLUCAAA');
								calcPagos += "<div class='costoMateria' style='padding-top:20px;line-height: 45px; font-size: 25px;'><span style='color:#999;'>Clinicas A:</span> 1 Pago de $"+CurrencyFormatted("3180")+"* y <br> 2 Pagos de $"+CurrencyFormatted("2385")+"* </div>";
								calcPagos += "<div class='costoMateria' style='padding-top:20px;line-height: 45px; font-size: 25px;'><span style='color:#999;'>Clinicas B:</span> 1 Pago de $"+CurrencyFormatted("4240")+"* y <br> 2 Pagos de $"+CurrencyFormatted("3180")+"* </div>";

								}
								else if(campusSelect==9){
									//console.log('gdl');
								calcPagos += "<div class='costoMateria' style='padding-top:20px;line-height: 45px; font-size: 25px;'><span style='color:#999;'>Clinicas A:</span> 1 Pago de $"+CurrencyFormatted("2865")+"* y <br> 2 Pagos de $"+CurrencyFormatted("2160")+"* </div>";
								calcPagos += "<div class='costoMateria' style='padding-top:20px;line-height: 45px; font-size: 25px;'><span style='color:#999;'>Clinicas B:</span> 1 Pago de $"+CurrencyFormatted("3820")+"* y <br> 2 Pagos de $"+CurrencyFormatted("2880")+"* </div>";

								}
								else{
								calcPagos += "<div class='costoMateria' style='padding-top:20px;line-height: 45px; font-size: 25px;'><span style='color:#999;'>Clinicas A:</span> 1 Pago de $"+CurrencyFormatted("3180")+"* y <br> 2 Pagos de $"+CurrencyFormatted("2385")+"* </div>";
								calcPagos += "<div class='costoMateria' style='padding-top:20px;line-height: 45px; font-size: 25px;'><span style='color:#999;'>Clinicas B:</span> 1 Pago de $"+CurrencyFormatted("4240")+"* y <br> 2 Pagos de $"+CurrencyFormatted("3180")+"* </div>";


								}	

																				

							}	

							//NUTRICION
							if(programa==1040){

								if(campusSelect===1){
									calcPagos += "<div class='costoMateria' style='padding-top:20px;line-height: 45px; font-size: 25px;'><span style='color:#999;'>Prácticas:</span> 1 Pago de $"+CurrencyFormatted("9480")+"* y <br> 2 Pagos de $"+CurrencyFormatted("7110")+"*</div>";

								}else if(campusSelect===2||campusSelect===4){
									calcPagos += "<div class='costoMateria' style='padding-top:20px;line-height: 45px; font-size: 25px;'><span style='color:#999;'>Prácticas:</span> 1 Pago de $"+CurrencyFormatted("8880")+"* y <br> 2 Pagos de $"+CurrencyFormatted("6660")+"*</div>";

								}else if(campusSelect===3){
									calcPagos += "<div class='costoMateria' style='padding-top:20px;line-height: 45px; font-size: 25px;'><span style='color:#999;'>Prácticas:</span> 1 Pago de $"+CurrencyFormatted("8795")+"* y <br> 2 Pagos de $"+CurrencyFormatted("6600")+"*</div>";

								}else if(campusSelect===5){
									calcPagos += "<div class='costoMateria' style='padding-top:20px;line-height: 45px; font-size: 25px;'><span style='color:#999;'>Prácticas:</span> 1 Pago de $"+CurrencyFormatted("9600")+"* y <br> 2 Pagos de $"+CurrencyFormatted("7200")+"*</div>";

								}else if(campusSelect===6){
									calcPagos += "<div class='costoMateria' style='padding-top:20px;line-height: 45px; font-size: 25px;'><span style='color:#999;'>Prácticas:</span> 1 Pago de $"+CurrencyFormatted("8830")+"* y <br> 2 Pagos de $"+CurrencyFormatted("6625")+"*</div>";

								}else if(campusSelect===8){
									calcPagos += "<div class='costoMateria' style='padding-top:20px;line-height: 45px; font-size: 25px;'><span style='color:#999;'>Prácticas:</span> 1 Pago de $"+CurrencyFormatted("7480")+"* y <br> 2 Pagos de $"+CurrencyFormatted("5605")+"*</div>";

								}else if(campusSelect===9){
									calcPagos += "<div class='costoMateria' style='padding-top:20px;line-height: 45px; font-size: 25px;'><span style='color:#999;'>Prácticas:</span> 1 Pago de $"+CurrencyFormatted("7710")+"* y <br> 2 Pagos de $"+CurrencyFormatted("5790")+"*</div>";

								}													
								


								
							}	


							//Especialidades Materia Adicional
							if(positionCostos==33){
								calcPagos += "<div class='materiaAdicional'>Materia Adicional:</div>"+
											 "<div class='costoMateria'><span id='costoMaterias2_"+i+"'>$"+CurrencyFormatted(costoFinal2[i][0])+"*</span></div>"+
											 "<div class='costoMateria2'>y 2 pagos de <span id='costoMaterias_2_"+i+"'>$"+CurrencyFormatted(costoFinal2[i][1])+"*</span></div>";
											 
							//ENFERMERIA					
							}
							if(minMateria>0 && maxMateria>0){			
								calcPagos +=	"<div class='buttonCostos'>"+
												"<a href=\"javascript:cambiarMaterias2(0,"+minMateria+","+maxMateria+","+costoFinal[i][0]+","+costoFinal[i][1]+",'"+nivel+"','"+programa+"','"+i+"');\" class='button'><span style='font-size:20px;'>-</span></a>"+
												"<input name='materias_"+i+"' id='materias_"+i+"' maxlength='1' size='1' type='text' style='border:1px solid #AAAAAA; text-align:center; padding:5px; font-size:17px;' value='1' readonly='readonly'>"+
												"<a href=\"javascript:cambiarMaterias2(1,"+minMateria+","+maxMateria+","+costoFinal[i][0]+","+costoFinal[i][1]+",'"+nivel+"','"+programa+"','"+i+"');\" class='button'><span style='font-size:20px;'>+</span></a>"+
												"</div>";
							}
						}else{ // 1,5,6,10
							if(i!=3){													
								if(minMateria==1){				
									calcPagos +=	"<div class='numMaterias'>Por <span id='numMaterias_"+i+"'>1 materia</span> pagarás:</div>";
								}else{
									calcPagos +=	"<div class='numMaterias'><span>Carga completa:</span></div>";
								}
							}else{
								calcPagos +=	"<div class='numMaterias'><span>Carga completa 5 Materias:</span></div>";
							}

if(programa==35){
if(i==2){
	calcPagos += 	"<div class='costoMateria'>"+
											"<span id='costoMaterias_"+i+"'>5 pagos de $"+CurrencyFormatted(costoFinal[i][0])+"*</span></div>"+
											"<div class='costoMateria2'>y un pago de <span id='costoMaterias_2_"+i+"'>$"+CurrencyFormatted(costoFinal[i][1])+"*</span></div>";
											
		}else{
				//	calcPagos += 	"<div class='cargaCompleta'><span id='costoMaterias_"+i+"'>$"+CurrencyFormatted(costoFinal[i])+"*</span></div>";

		}							
}else{



	if(positionCostos==33){
								
								if(i==2){
								calcPagos += "<div class='costoMateria'><span id='costoMaterias2_"+i+"'>5 pagos de :$18,510*</span></div>"+
											 "<div class='costoMateria2'>y 1 pago de <span id='costoMaterias_2_"+i+"'>$18,515*</span></div>";
										}else{
															calcPagos += 	"<div class='cargaCompleta'><span id='costoMaterias_"+i+"'>$"+CurrencyFormatted(costoFinal[i])+"*</span></div>";

										}	 
							//ENFERMERIA					
							}else{
															//calcPagos += 	"<div class='cargaCompleta'><span id='costoMaterias_"+i+"'>$"+CurrencyFormatted(costoFinal[i])+"*</span></div>";
 
							}

							if(positionCostos==33){
								if(i==2){
									calcPagos += "<div class='materiaAdicional'>Materia Adicional:</div>"+
								"<div class='costoMateria'><span id='costoMaterias2_"+i+"'>5 pagos de :$3,085*</span></div>"+
											 "<div class='costoMateria2'>y 1 pago de <span id='costoMaterias_2_"+i+"'>$3,090 *</span></div>";
										
								}
								
							}


}







							
							//TURISMO PRACTICAS
							if(programa==722 || programa == 723){	
								var costoPracTuri = "", pagosPracTuri = "";
								if(i==0){
									pagosPracTuri = "1 Pago";
									costoPracTuri = 26610;
								}else{
									pagosPracTuri = "4 Pagos";
									costoPracTuri = 6870;
								}
								calcPagos += "<div class='costoMateria' style='padding-top:20px;line-height: 45px; font-size: 25px;'><span style='color:#999;'>Prácticas:</span> "+pagosPracTuri+" de $"+CurrencyFormatted(costoPracTuri)+"* </div>";
							}



							//Enfermeria
							if(programa==1150){	
								var costoPracTuri = "", pagosPracTuri = "";
								if(i==0){
									pagosPracTuri = "1 Pago";
									costoPracTuri = 7650;
								}else{
									pagosPracTuri = "4 Pagos";
									costoPracTuri = 2055;
								}



								if(campusSelect===8){

										var costoPracTuri = "", pagosPracTuri = "";
										if(i==0){
											pagosPracTuri = "1 Pago";
											costoPracTuri = 6900;
										}else{
											pagosPracTuri = "4 Pagos";
											costoPracTuri = 1830;
										}

								}
								if(campusSelect===9){

										var costoPracTuri = "", pagosPracTuri = "";
										if(i==0){
											pagosPracTuri = "1 Pago";
											costoPracTuri = 7005;
										}else{
											pagosPracTuri = "4 Pagos";
											costoPracTuri = 1830;
										}

								}
								calcPagos += "<div class='costoMateria' style='padding-top:20px;line-height: 45px; font-size: 25px;'><span style='color:#999;'>Clinica A:</span> "+pagosPracTuri+" de $"+CurrencyFormatted(costoPracTuri)+"* </div>";
							}

							//Nutricion
							if(programa==1040){	



								if(campusSelect===1){
											var costoPracTuri = "", pagosPracTuri = "";
											if(i==0){
												pagosPracTuri = "1 Pago";
												costoPracTuri = 23010;
											}else{
												pagosPracTuri = "4 Pagos";
												costoPracTuri = 6000;
											}
								}else if(campusSelect===2||campusSelect===4){
											var costoPracTuri = "", pagosPracTuri = "";
											if(i==0){
												pagosPracTuri = "1 Pago";
												costoPracTuri = 21390;
											}else{
												pagosPracTuri = "4 Pagos";
												costoPracTuri = 5640;
											}

								}else if(campusSelect===3){
									var costoPracTuri = "", pagosPracTuri = "";
											if(i==0){
												pagosPracTuri = "1 Pago";
												costoPracTuri = 21330;
											}else{
												pagosPracTuri = "4 Pagos";
												costoPracTuri = 5580;
											}

								}else if(campusSelect===5){
									var costoPracTuri = "", pagosPracTuri = "";
											if(i==0){
												pagosPracTuri = "1 Pago";
												costoPracTuri = 23340;
											}else{
												pagosPracTuri = "4 Pagos";
												costoPracTuri = 6120;
											}
								}else if(campusSelect===6){
											var costoPracTuri = "", pagosPracTuri = "";
											if(i==0){
												pagosPracTuri = "1 Pago";
												costoPracTuri = 21390;
											}else{
												pagosPracTuri = "4 Pagos";
												costoPracTuri = 5610;
											}
								}else if(campusSelect===8){
											var costoPracTuri = "", pagosPracTuri = "";
											if(i==0){
												pagosPracTuri = "1 Pago";
												costoPracTuri = 18120;
											}else{
												pagosPracTuri = "4 Pagos";
												costoPracTuri = 4770;
											}
								}	
								else if(campusSelect===9){
											var costoPracTuri = "", pagosPracTuri = "";
											if(i==0){
												pagosPracTuri = "1 Pago";
												costoPracTuri = 18810;
											}else{
												pagosPracTuri = "4 Pagos";
												costoPracTuri = 4920;
											}
								}


								
								calcPagos += "<div class='costoMateria' style='padding-top:20px;line-height: 45px; font-size: 25px;'><span style='color:#999;'>Prácticas:</span> "+pagosPracTuri+" de $"+CurrencyFormatted(costoPracTuri)+"* </div>";
							}


							//Enfermeria
							if(programa==1150){	
								var costoPracTuri = "", pagosPracTuri = "";
								if(i==0){
									pagosPracTuri = "1 Pago";
									costoPracTuri = 10200;
								}else{
									pagosPracTuri = "4 Pagos";
									costoPracTuri = 2740;
								}

								if(campusSelect===8){

										var costoPracTuri = "", pagosPracTuri = "";
										if(i==0){
											pagosPracTuri = "1 Pago";
											costoPracTuri = 9200;
										}else{
											pagosPracTuri = "4 Pagos";
											costoPracTuri = 2440;
										}

								}
								if(campusSelect===9){

										var costoPracTuri = "", pagosPracTuri = "";
										if(i==0){
											pagosPracTuri = "1 Pago";
											costoPracTuri = 9340;
										}else{
											pagosPracTuri = "4 Pagos";
											costoPracTuri = 2440;
										}

								}
								calcPagos += "<div class='costoMateria' style='padding-top:20px;line-height: 45px; font-size: 25px;'><span style='color:#999;'>Clinica B:</span> "+pagosPracTuri+" de $"+CurrencyFormatted(costoPracTuri)+"* </div>";
							}

							
							
							//Especialidades Materia Adicional				
							/*if(positionCostos==33){
								calcPagos += "<div class='materiaAdicional'>Materia Adicional:</div>"+
										"<div class='costoMateria'><span id='costoMaterias2_"+i+"'>$"+CurrencyFormatted(costoFinal2[i])+"*</span></div>";
							}
*/
if(positionCostos==33){
								/*calcPagos += "<div class='costoMateria'><span id='costoMaterias2_"+i+"'>5 pagos de :$18,511*</span></div>"+
											 "<div class='costoMateria2'>y 1 pago de <span id='costoMaterias_2_"+i+"'>$18,515*</span></div>";
								*/			 
							//ENFERMERIA					
							}else{
								if(programa==35){

if(i==0){
					calcPagos += 	"<div class='cargaCompleta'><span id='costoMaterias_"+i+"'>$"+CurrencyFormatted(costoFinal[i])+"*</span></div>";

}


								}else{

									calcPagos += 	"<div class='cargaCompleta'><span id='costoMaterias_"+i+"'>$"+CurrencyFormatted(costoFinal[i])+"*</span></div>";
 
								}

							}

							/*if(positionCostos==33){
								calcPagos += "<div class='materiaAdicional'>Materia Adicional:</div>"+
								"<div class='costoMateria'><span id='costoMaterias2_"+i+"'>5 pagos de :$3,086*</span></div>"+
											 "<div class='costoMateria2'>y 1 pago de <span id='costoMaterias_2_"+i+"'>$3,090 *</span></div>";
										
							}*/


							if(i!=3){

								if(programa==35){
									if(minMateria>0 && maxMateria>0){			
									calcPagos +=	"<div class='buttonCostos'>"+
													"<a href=\"javascript:cambiarMaterias2(0,"+minMateria+","+maxMateria+","+costoFinal[i]+",'"+nivel+"','"+programa+"','"+i+"');\" class='button'><span style='font-size:20px;'>-</span></a>"+
													"<input name='materias_"+i+"' id='materias_"+i+"' maxlength='1' size='1' type='text' style='border:1px solid #AAAAAA; text-align:center; padding:5px; font-size:17px;' value='1' readonly='readonly'>"+
													"<a href=\"javascript:cambiarMaterias2(1,"+minMateria+","+maxMateria+","+costoFinal[i]+",'"+nivel+"','"+programa+"','"+i+"');\" class='button'><span style='font-size:20px;'>+</span></a>"+
													"</div>";
									}
								}else{

									if(minMateria>0 && maxMateria>0){			
									calcPagos +=	"<div class='buttonCostos'>"+
													"<a href=\"javascript:cambiarMaterias(0,"+minMateria+","+maxMateria+","+costoFinal[i]+",'"+nivel+"','"+programa+"','"+i+"');\" class='button'><span style='font-size:20px;'>-</span></a>"+
													"<input name='materias_"+i+"' id='materias_"+i+"' maxlength='1' size='1' type='text' style='border:1px solid #AAAAAA; text-align:center; padding:5px; font-size:17px;' value='1' readonly='readonly'>"+
													"<a href=\"javascript:cambiarMaterias(1,"+minMateria+","+maxMateria+","+costoFinal[i]+",'"+nivel+"','"+programa+"','"+i+"');\" class='button'><span style='font-size:20px;'>+</span></a>"+
													"</div>";
									}

								}
								
							}
						}
						
						calcPagos += "<div class='otraConsultab'>*Este costo es de car\u00E1cter informativo.<br><br><a href='javascript:otraConsulta();' class='button-naranja'><span>Otra Consulta</span></a></div>";	
									
					calcPagos += "</div>";
				}				
				calcPagos += "</div>";
		
				//TABS FIN				
				//BOTON PARA PDF
				info += "<div align='center' style='width:100%;'><a href='http://www.unitec.mx/coleg_pdf/pdf/"+urlPDF+"' class='button-naranja' target='_blank'><span>Consultar fechas de vencimiento</span></a></div>";							
										
				jQuery("#showCosto").html(calcPagos);

				jQuery(".infoBecas").html(info).fadeIn(500);
				jQuery(".areaFinal").fadeIn(800);
				jQuery("#notaPrincipal").fadeIn(1000);	
				loadTabs();		
				jQuery("#unitecFinal").slideDown(500,function(){					
					jQuery("html, body").animate({ scrollTop: 500 }, "slow");
				});
				
				currentLT = 0;
			},300);			
		}
	}
}
//TABS
function loadTabs(){
	jQuery('ul.tabs').each(function() {
		jQuery(this).find('li').each(function(i) {
			jQuery(this).click(function(){
				jQuery(this).addClass('current').siblings().removeClass('current')
					.parents('div.section').find('div.box').hide().end().find('div.box:eq('+i+')').fadeIn(150);
					//jQuery(self.parent.parent.window).scrollTop(500);
			});
		});
	});
}
//-->
function CurrencyFormatted(amount){
	var i = parseFloat(amount);
	if(isNaN(i)) { i = 0.00; }
	var minus = '';
	if(i < 0) { minus = '-'; }
	i = Math.abs(i);
	i = parseInt((i + .005) * 100);
	i = i / 100;
	s = new String(i);
	if(s.indexOf('.') < 0) { s += '.00'; }
	if(s.indexOf('.') == (s.length - 2)) { s += '0'; }
	s = minus + s;
	return s;
}
//-->
function resetLineaNeg(currentLN){
	var lineaNeg = "";
	switch(currentLN){
		case 'P': case 'PREPA':
			lineaNeg = 'P'; /*Preparatoria*/
		break;
		case 'T': case 'TI': case 'C': case 'SIS': case 'OIN': case 'L': case 'NUTRI': case 'CD': case 'TURI': case'LON': case 'LIC': /*Licenciaturas*/
			lineaNeg = 'LIC';
		break;
		case 'LC': case 'LC2': case 'HLX': case 'LC3': case 'LC4': case 'LC5':/*Licenciaturas Corportativas*/
			lineaNeg = 'LC';
		break;
		case 'DIP': case 'DIPON': /*Diplomados*/
			lineaNeg = 'DIP';
		break;
		case 'SP': case 'ON': case 'POS':
			lineaNeg = 'SP';
		break;
		case 'ESP':
			lineaNeg = 'ESP';
		break;
	};
	return lineaNeg;
}
//-->
function cambiarMaterias(oper,minMat,maxMat,costoFinal,nivel,programa,id){
	var currentMateria = parseInt(jQuery("#materias_"+id).val()),
		newMateria = 1;
	if(oper==0){
		newMateria =  currentMateria - 1;
		newMateria = (newMateria<minMat) ? minMat : newMateria;
	}else{
		newMateria = currentMateria + 1
		newMateria = (newMateria>maxMat) ? maxMat : newMateria;
	}
	var textoMat = (newMateria==1) ? " materia" : " materias";
	jQuery("#materias_"+id).val(newMateria);
	jQuery("#numMaterias_"+id).text(newMateria+textoMat);
	jQuery("#costoMaterias_"+id).text("$"+CurrencyFormatted(parseInt(costoFinal)*parseInt(newMateria))+"*");			
	if(programa==21 || programa==22 || programa==29){
		if(oper==0){
			if(currentMateria<=3){
				newMateria =  currentMateria - 1;
				newMateria = (newMateria<minMat) ? minMat : newMateria;
				jQuery("#numMaterias2_"+id).text(newMateria+textoMat);
				jQuery("#costoMaterias2_"+id).text("$"+CurrencyFormatted(parseInt(costos[34][0])*parseInt(newMateria)));					
			}
		}else{
			newMateria = currentMateria + 1
			newMateria = (newMateria>3) ? 3 : newMateria;
			jQuery("#numMaterias2_"+id).text(newMateria+textoMat);
			jQuery("#costoMaterias2_"+id).text("$"+CurrencyFormatted(parseInt(costos[34][0])*parseInt(newMateria)));	
		}		
	}
}
//-->
//-->
function cambiarMaterias2(oper,minMat,maxMat,costoFinal1,costoFinal2,nivel,programa,id){
	var currentMateria = parseInt(jQuery("#materias_"+id).val()),
		newMateria = 1;
	if(oper==0){
		newMateria =  currentMateria - 1;
		newMateria = (newMateria<minMat) ? minMat : newMateria;
	}else{
		newMateria = currentMateria + 1
		newMateria = (newMateria>maxMat) ? maxMat : newMateria;
	}
	var textoMat = (newMateria==1) ? " materia" : " materias";
	jQuery("#materias_"+id).val(newMateria);
	jQuery("#numMaterias_"+id).text(newMateria+textoMat);
	jQuery("#costoMaterias_"+id).text("$"+CurrencyFormatted(parseInt(costoFinal1)*parseInt(newMateria))+"*");
	jQuery("#costoMaterias_2_"+id).text("$"+CurrencyFormatted(parseInt(costoFinal2)*parseInt(newMateria))+"*");
	//CASO ENFERMERIA
	if(nivel=="ENFER"){
		var costoFinal2 = costos[30][6];
		if(oper==0){
			if(currentMateria<=3){
				newMateria =  currentMateria - 1;
				newMateria = (newMateria<minMat) ? minMat : newMateria;
				jQuery("#numMaterias_2_"+id).text(newMateria+textoMat);
				jQuery("#costoMaterias_2_1_"+id).text("$"+CurrencyFormatted(parseInt(costoFinal2[0])*parseInt(newMateria))+"*");
				jQuery("#costoMaterias_2_2_"+id).text("$"+CurrencyFormatted(parseInt(costoFinal2[1])*parseInt(newMateria))+"*");										
			}
		}else{
			newMateria = currentMateria + 1
			newMateria = (newMateria>3) ? 3 : newMateria;
			jQuery("#numMaterias_2_"+id).text(newMateria+textoMat);
			jQuery("#costoMaterias_2_1_"+id).text("$"+CurrencyFormatted(parseInt(costoFinal2[0])*parseInt(newMateria))+"*");
			jQuery("#costoMaterias_2_2_"+id).text("$"+CurrencyFormatted(parseInt(costoFinal2[1])*parseInt(newMateria))+"*");	
		}		
	}
	if(programa==21 || programa==22 || programa==29){
		if(oper==0){
			if(currentMateria<=3){
				newMateria =  currentMateria - 1;
				newMateria = (newMateria<minMat) ? minMat : newMateria;
				jQuery("#numMaterias2_"+id).text(newMateria+textoMat);
				jQuery("#costoMaterias2_"+id).text("$"+CurrencyFormatted(parseInt(costos[31][0])*parseInt(newMateria)));					
			}
		}else{
			newMateria = currentMateria + 1
			newMateria = (newMateria>3) ? 3 : newMateria;
			jQuery("#numMaterias2_"+id).text(newMateria+textoMat);
			jQuery("#costoMaterias2_"+id).text("$"+CurrencyFormatted(parseInt(costos[31][0])*parseInt(newMateria)));	
		}		
	}

	if(programa=='35'){
		if(oper==0){
			if(currentMateria<=7){
				newMateria =  currentMateria - 1;
				newMateria = (newMateria<minMat) ? minMat : newMateria;
				jQuery("#costoMaterias_"+id).text("5 pagos de: $"+CurrencyFormatted(parseInt(costoFinal1)*parseInt(newMateria))+"*");
				jQuery("#numMaterias2_"+id).text(newMateria+textoMat);
				jQuery("#costoMaterias2_"+id).text("$"+CurrencyFormatted(parseInt(costos[31][0])*parseInt(newMateria)));					
			}
		}else{
			newMateria = currentMateria + 1
			newMateria = (newMateria>7) ? 7 : newMateria;
			jQuery("#numMaterias2_"+id).text(newMateria+textoMat);
			jQuery("#costoMaterias_"+id).text("5 pagos de: $"+CurrencyFormatted(parseInt(costoFinal1)*parseInt(newMateria))+"*");
			jQuery("#costoMaterias2_"+id).text("$"+CurrencyFormatted(parseInt(costos[31][0])*parseInt(newMateria)));	
		}		
	}
}
//-->
function otraConsulta(){
	//Limpiamos area Resultado	
	jQuery("#areaResultado, #areaMaterias, #areaCarreras, #areaPromedio").animate({
		marginTop: '300px'
	  },500,function(){
		 jQuery(this).fadeOut("fast");			
	});
	 jQuery("#unitecFinal").slideUp();
	//RESET VALUES
	jQuery("#calcUNITEC").data("campus","0");
	jQuery("#calcUNITEC").data("promedio","0");	
	jQuery("#calcUNITEC").data("oferta","0");
	jQuery("#calcUNITEC").data("tipoOferta","0");	
	jQuery("#calcUNITEC").data("procesado","0");
	//RESET ELEMENTS		
	jQuery(".imgCampus").removeClass("campSel").removeClass("imgCampus_on").addClass("imgCampus");
	jQuery(".campus").unblock();
	resetCarrera(0);	
	jQuery("#otraConsulta").val(1);	
	addEventOfertaUnitec();
	jQuery("html, body").animate({ scrollTop: 0 }, "slow");
}








/*****************************MORPHING SEARCH***************************************/

var morphSearch = jQuery( '#morphsearch' ),
input = jQuery( '.morphsearch-input' ),
ctrlClose = jQuery( '.morphsearch-close' ),
isOpen = false,

// show/hide search area
toggleSearch = function(evt) {

    if( evt.type.toLowerCase() === 'focus' && isOpen ) //Si esta en pantalla completa el buscador y el foco lo tiene el input, no hace nada
        return false;

    if( isOpen ) {
        if(jQuery(document.body).hasClass('descuentos-comerciales') || jQuery(document.body).hasClass('politicas-de-privacidad') || jQuery(document.body).hasClass('terminos-y-condiciones') || jQuery(document.body).hasClass("page-template-colegiaturas-page")){

            jQuery('html').attr("style","overflow: visible !important");
         }
         else{
            jQuery.fn.fullpage.setAllowScrolling(true);
            jQuery.fn.fullpage.setKeyboardScrolling(true);

        }
        if(navigator.userAgent.search(/Mobi/i)!=-1){
            jQuery('.morphsearch').css({"backgroundColor":"rgba(0,0,0,0)"});
            input.attr("placeholder","");
        }
        jQuery(morphsearch).removeClass('open');
        // Quita el texto del input de la ultima busqueda
        if( jQuery(input).val() !== '' ) {
            setTimeout(function() {
                jQuery(morphsearch).addClass('hideInput');
                setTimeout(function() {
                    jQuery(morphsearch).removeClass('hideInput');
                    jQuery(input).val("");
                    jQuery(".morphsearch-content").html('');

                }, 300 );
            }, 500);
        }

        jQuery(input).blur();
    }
    else {
         if(jQuery(document.body).hasClass('descuentos-comerciales') || jQuery(document.body).hasClass('politicas-de-privacidad') || jQuery(document.body).hasClass('terminos-y-condiciones')){
            jQuery('html').attr("style","overflow: hidden !important");
         }else{
            jQuery.fn.fullpage.setAllowScrolling(false);
            jQuery.fn.fullpage.setKeyboardScrolling(false);
        }
        jQuery(morphsearch).addClass('open');
        
        if(navigator.userAgent.search(/Mobi/i)!=-1){
        jQuery(".morphsearch").css({"backgroundColor":"#fff"});    
            input.attr("placeholder","Buscar...");
        }
    }
    isOpen = !isOpen;
};

if(navigator.userAgent.search(/Mobi/i)!=-1){
input.attr("placeholder","");
morphSearch.css({"backgroundColor":"transparent"});
}
// events
jQuery(input).focus(toggleSearch);
//input.addEventListener( 'focus', toggleSearch );
jQuery(ctrlClose).click(toggleSearch);
//ctrlClose.addEventListener( 'click', toggleSearch );
// esc key closes search overlay
// keyboard navigation events
jQuery(document).keydown(function(ev){
var keyCode = ev.keyCode || ev.which;
if( keyCode === 27 && isOpen ) {
    toggleSearch(ev);
}
});
input.val("");
jQuery(".morphsearch-content").html('');

var options = {};
var orderBy = {};
orderBy['keys'] = [{label: 'Relevancia', key: ''},{label: 'Fecha', key: 'date'}];
options['enableOrderBy'] = true;
options['orderByOptions'] = orderBy;
//var googleAnalyticsOptions = {};
//googleAnalyticsOptions['queryParameter'] = 'q';
//googleAnalyticsOptions['categoryParameter'] = '';
//customSearchOptions['googleAnalyticsOptions'] = googleAnalyticsOptions;          
var customSearchControl = new google.search.CustomSearchControl('012159505712459906802:yacc1u8-pgq', options);
customSearchControl.setResultSetSize(10); //CONSTANTE DE 10 RESULTADOS, ACEPTA CUALQUIER OTRO ENTERO
customSearchControl.setLinkTarget(google.search.Search.LINK_TARGET_SELF);



jQuery("#searchsubmit").click(function(e){
    if (jQuery("#q").val()!=""){
        var drawOptions = new google.search.DrawOptions();
        //drawOptions.setAutoComplete(true);
        drawOptions.enableSearchResultsOnly();
        customSearchControl.draw('cse', drawOptions);
        customSearchControl.setNoResultsString("Lo sentimos, no encontramos nada de lo que buscas en Unitec.");
        customSearchControl.execute(jQuery("#q").val());
    }
});

jQuery(document).ready(function(){
  jQuery.fn.fullpage.destroy('all');
});
/************************************************************************************/