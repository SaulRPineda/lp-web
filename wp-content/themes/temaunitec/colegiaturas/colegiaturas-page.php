<?php
/**
 * Template Name: Colegiaturas Page
 * Description: Pagina de Colegiaturas Template
 *
 * @package WordPress
 * @subpackage themename
 */
unitec_Utilities::get_template_parts( array( 'parts/shared/html-header' ) );
?>
<!-- ESTILOS -->
<link rel="stylesheet" type="text/css" href="<?php bloginfo('template_directory'); ?>/colegiaturas/css/min/jquery-ui.min.css">
<link rel="stylesheet" type="text/css" href="<?php bloginfo('template_directory'); ?>/colegiaturas/css/min/carousel.min.css">
<!-- CSS files Carousel -->
<link rel="stylesheet" type="text/css" href="<?php bloginfo('template_directory'); ?>/colegiaturas/css/min/touchcarousel.min.css" />
<!-- Skin Stylesheet Carousel-->
<link rel="stylesheet" type="text/css" href="<?php bloginfo('template_directory'); ?>/colegiaturas/css/min/grey-blue-skin.min.css" />
<!-- Tamaños Adaptativos de elementos pagina -->
<link rel="stylesheet" type="text/css" href="<?php bloginfo('template_directory'); ?>/colegiaturas/css/min/calculadora_adaptable.min.css" />
<link rel="stylesheet" type="text/css" href="<?php bloginfo('template_directory'); ?>/colegiaturas/css/min/calculadora.min.css" />
<link rel="stylesheet" type="text/css" href="<?php bloginfo('template_directory'); ?>/colegiaturas/css/min/colegiaturas.min.css" />
<!-- TERMINAN ESTILOS -->
<?php  require "sec_header.php"; ?>
<section id="middle" class="container" style="padding-top:80px;">
  <div class="middle_inner"> 
  <h1> Colegiaturas</h1>
    <?php while (have_posts()) : the_post(); ?>
    <?php endwhile;?>
    <div id="contenedorCalculadora" style="padding-bottom:50px;">
      <div class="info_topForm"><p>Aquí podrás consultar de manera rápida y sencilla tu colegiatura. Sólo selecciona tu campus y tu carrera.</p></div>
      <div>
        <h3>Elige tu campus</h3>
        <div id="carouselCampus" class="touchcarousel grey-blue">
          <ul id="campusUNITEC" class="touchcarousel-container">
            <li class="campus touchcarousel-item"> <img src="<?php bloginfo('template_directory'); ?>/colegiaturas/campus_ATZ.jpg" id="imgCampus_1" class="imgCampus" border="0" title="Campus Atizapán" alt="Campus Atizapán" />
              <div class="tituloCampus">Atizapán</div>
            </li>
            <li class="campus touchcarousel-item"> <img src="<?php bloginfo('template_directory'); ?>/colegiaturas/campus_CUI.jpg" id="imgCampus_2" class="imgCampus" border="0" title="Campus Cuitláhuac" alt="Campus Cuitláhuac" />
              <div class="tituloCampus">Cuitláhuac</div>
            </li>
            <li class="campus touchcarousel-item"> <img src="<?php bloginfo('template_directory'); ?>/colegiaturas/campus_ECA.jpg" id="imgCampus_3" class="imgCampus" border="0" title="Campus Ecatepec" alt="Campus Ecatepec" />
              <div class="tituloCampus">Ecatepec</div>
            </li>
            <li class="campus touchcarousel-item"> <img src="<?php bloginfo('template_directory'); ?>/colegiaturas/campus_MAR.jpg" id="imgCampus_4" class="imgCampus" border="0" title="Campus Marina" alt="Campus Marina" />
              <div class="tituloCampus">Marina</div>
            </li>
            <li class="campus touchcarousel-item"> <img src="<?php bloginfo('template_directory'); ?>/colegiaturas/campus_SUR.jpg" id="imgCampus_5" class="imgCampus" border="0" title="Campus Sur" alt="Campus Sur" />
              <div class="tituloCampus">Sur</div>
            </li>
            <li class="campus touchcarousel-item"> <img src="<?php bloginfo('template_directory'); ?>/colegiaturas/campus_TOLUCA.jpg" id="imgCampus_6" class="imgCampus" border="0" title="Campus Toluca" alt="Campus Toluca" />
              <div class="tituloCampus">Toluca</div>
            </li>
            <li class="campus touchcarousel-item"> <img src="<?php bloginfo('template_directory'); ?>/colegiaturas/campus_VIRTUAL.jpg" id="imgCampus_7" class="imgCampus" border="0" title="Campus Virtual" alt="Campus Virtual" />
              <div class="tituloCampus">En Línea</div>
            </li>
            
            <li class="campus touchcarousel-item"> <img src="<?php bloginfo('template_directory'); ?>/colegiaturas/campus_MAR.jpg" id="imgCampus_8" class="imgCampus" border="0" title="Campus Leon" alt="Campus Leon" />
              <div class="tituloCampus">León</div>


            <li class="campus touchcarousel-item"> <img src="<?php bloginfo('template_directory'); ?>/colegiaturas/campus_ATZ.jpg" id="imgCampus_9" class="imgCampus" border="0" title="Campus Guadalajara" alt="Campus Leon" />
              <div class="tituloCampus">Guadalajara</div>
            
           <li class="campus touchcarousel-item"> <img src="<?php bloginfo('template_directory'); ?>/colegiaturas/campus_CUI.jpg" id="imgCampus_10" class="imgCampus" border="0" title="Campus Queretaro" alt="Campus Querétaro" />
              <div class="tituloCampus">Querétaro</div>
            </li>


            </li>              
            </li>


          </ul>
        </div>
      </div>
      <div class="clear"></div>
      <div id="areaCarreras" style="display:none;">
        <h3>Elige la carrera que estudias en la UNITEC</span></h3>
        <div id="carouselCarreras" class="touchcarousel grey-blue">
          <ul id="ofertaCarreras" class="touchcarousel-container">
            <li id="PREPA" class="carrera touchcarousel-item borderR">
              <div class="ofertaUnitec"> </div>
            </li>
            <li id="LIC" class="carrera touchcarousel-item borderR">
              <div class="ofertaUnitec non-draggable"> </div>
            </li>
            <li id="LC" class="carrera touchcarousel-item borderR">
              <div class="ofertaUnitec non-draggable"> </div>
            </li>
            <li id="DIP" class="carrera touchcarousel-item borderR">
              <div class="ofertaUnitec non-draggable"> </div>
            </li>
            <li id="POS" class="carrera touchcarousel-item borderR">
              <div class="ofertaUnitec non-draggable"> </div>
            </li>
            <li id="ESP" class="carrera touchcarousel-item borderR">
              <div class="ofertaUnitec non-draggable"> </div>
            </li>
            <li id="HLX" class="carrera touchcarousel-item borderR">
              <div class="ofertaUnitec non-draggable"> </div>
            </li>
          </ul>
        </div>
        <div class="clear"></div>
      </div>
      <div class="clear"></div>
      <div id="unitecFinal" style="display:none;">
        <div id="infoLegales" class="areaFinal"  style="display:none;">
          <div class="infoBecas" style="display:none;"> </div>
        </div>
        <div id="showCosto" class="areaFinal border"  style="display:none;"> </div>
        <div class="clear"></div>
        <div id="notaPrincipal" style="display:none;"> </div>
      </div>
    </div>
    <div id="colegUNITEC"></div>
  </div>
</section>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.3/jquery.min.js"></script>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
<script type="text/javascript">
    jQuery.browser = {};
    (function () {
        jQuery.browser.msie = false;
        jQuery.browser.version = 0;
        if (navigator.userAgent.match(/MSIE ([0-9]+)\./)) {
            jQuery.browser.msie = true;
            jQuery.browser.version = RegExp.$1;
        }
    })();
</script>
<script type="text/javascript" language="javascript" src="<?php bloginfo('template_directory'); ?>/colegiaturas/jquery.ui.touch-punch.js"></script>
<script type="text/javascript" language="javascript" src="<?php bloginfo('template_directory'); ?>/colegiaturas/jquery.blockUI.js"></script>

<script type="text/javascript" language="javascript" src="<?php bloginfo('template_directory'); ?>/colegiaturas/jquery.easing.1.3.js"></script>

<script type="text/javascript" language="javascript" src="<?php bloginfo('template_directory'); ?>/colegiaturas/jquery.touchcarousel-1.1.min.js"></script>

<script type="text/javascript" language="javascript" src="<?php bloginfo('template_directory'); ?>/colegiaturas/jquery.easing.compatibility.js"></script>


<?php  require "sec_footer.php"; ?>
<?php unitec_Utilities::get_template_parts( array( 'parts/shared/html-footer' ) ); ?>
<script type="text/javascript" language="javascript" src="<?php bloginfo('template_directory'); ?>/colegiaturas/js/colegiaturas.js"></script>