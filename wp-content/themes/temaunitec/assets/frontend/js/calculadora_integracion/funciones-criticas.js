
/**
 *                                                  SECCIONES EN CALCULADORA VIEJA 
 *  
 * data-anchor="sec-formulario" //home principal
 * data-anchor="intro" //
 * data-anchor="frm_mobile"
 * data-anchor="sec-cursar"         2.- URL: calcula-tu-beca/#calculadora/sec-cursar        DESC: Selecciona Linea de Negocio, carrera y modalidad
 * data-anchor="sec-categoria"      
 * data-anchor="sec-modalidades"
 * data-anchor="sec-preguntas"
 * data-anchor="sec-campus"          3.- calculadora/sec-campus                              DESC: Selecciona Campus
 * data-anchor="sec-promedio"        4.- calculadora/sec-promedio                            DESC: Selecciona Promedio
 * data-anchor="resultados"          5.- calculadora/resultados                              DESC: Muestra resultados y envia Info
 * 
 * secciones de home 
 * data-anchor="detalle"
 * data-anchor="contenido"
 * 
 */

$(document).ready(function () {
    sendDatalayer('calculadora/paso0', 'StepCalculator');

    console.log('after render');
    // Al cargar la pagina revisa si ya tiene el formulario completo si ya lo tiene borra la app y manda a sec-cursar

    if (checkCookie('data_frm')) {
        var a = getCookie('data_frm');
        var b = JSON.parse(decodeURIComponent(a));
        jQuery('#frm_nombre').val(b.frm_nombre);
        jQuery('#frm_apaterno').val(b.frm_apaterno);
        jQuery('#frm_amaterno').val(b.frm_amaterno);
        jQuery('#frm_correo').val(b.frm_email);
        jQuery('#frm_celular').val(b.frm_celular);
        jQuery('#revalidacion option[value="' + b.frm_informacion + '"]').attr('selected', true);
        setTimeout(function () { jQuery('form').submit(); }, 1000);

    } else {

        if (checkCookie('regcompleto') == true) {
            if (localStorage.getItem("jsonUsr") === null) {
            }
            else {
                //no existe JSON revisa cookie frmulario 
                resetJsonEleccion();
                removeSections();
                chargeImages();
                jQuery("#calc_form_hide").hide(); //Fix problema auto height
                // jQuery.fn.fullpage.silentMoveTo('calculadora', 'sec-cursar');
            }
        }
        else {
            resetJsonEleccion();
            removeSections();
            chargeImages();
            jQuery("#calc_form_hide").hide(); //Fix problema auto height
            // jQuery.fn.fullpage.silentMoveTo('calculadora', 0);
            document.body.scrollTop = 0;
        }

    }
});

$(window).on('load', function(){
    var cookieBanner = getParameterByName('utm_campaign');
    console.log("Checando los parametros");
    /*Validar si existe utm_campaign*/
    if( getParameterByName('utm_campaign') !== false ) {

        if( checkCookie('banner_activo') === true ){
            //lo asigo a la cookie form_data
            appendCookie('c_form_data', 'banner', getCookie('banner_activo'), 1);
        } else{
            //no existe se da de alta en banner activo
            console.log("Asignando banner activo");
            setCookieForm("banner_activo", cookieBanner, 1);
        }
    }
    /*Si no existe*/
    else {
        if( checkCookie('banner_activo') === true ){
            //lo asigo a la cookie form_data
            appendCookie('c_form_data', 'banner', getCookie('banner_activo'), 1);
        } else{
            if (location.href.indexOf("testvocacional") !== -1 || location.href.indexOf("orientacion-profesional") !== -1) {
                cookieBanner = "TESTVOCACIONAL";
            } else if (location.href.indexOf("impulsa") !== -1) {
                cookieBanner = "IMPULSA";
            } else {
                cookieBanner = "CALCULADORA";
            }
            appendCookie('c_form_data', 'banner', cookieBanner, 1);
            //setCookieForm("banner_activo", cookieBanner, 1);
        }
    }
});

//https://www.unitec.mx/wp-content/themes/temaunitec/calculadora/json/min/json_calc_becas.min.json

var urlJsonBasura = "/assets/frontend/json/min/json_basura.min.json";
var urlJsonBasuraEmail = "/assets/frontend/json/min/json_basuraEmail.min.json";
var urlJsonBasuraTelefonos = "/assets/frontend/json/min/json_basuraTelefonos.min.json";
var urlJsonCarreras = "/assets/frontend/json/min/json_calc_carreras.min.json";
//Se agrega nueva url para obtener los links y nombre de las categorias de las carreras
var urlJsonLinksCategorias = "/assets/frontend/json/min/json_calc_linkPaginas.min.json";
//Se agregan variables para calculadora
//Costos
var urlJsonCalcCostos = "/assets/frontend/json/min/json_calc_costos.min.json";
//Becas
var urlJsonCalcBecas = "/assets/frontend/json/min/json_calc_becas.min.json";

function setCookie(cname, cvalue, exdays) {
    var d = new Date();
    d.setTime(d.getTime() + (exdays * 24 * 60 * 60 * 1000));
    var expires = "expires=" + d.toUTCString();
    document.cookie = cname + "=" + cvalue + "; " + expires;
}

function setCookieSession(cname, cvalue) {
    document.cookie = cname + "=" + cvalue + ";path=/";
}
function isInArray(value, array) {
    return array.indexOf(value) > -1;
}
function getCookie(cname) {
    var name = cname + "=";
    var ca = document.cookie.split(';');
    for (var i = 0; i < ca.length; i++) {
        var c = ca[i];
        while (c.charAt(0) == ' ') c = c.substring(1);
        if (c.indexOf(name) == 0) return c.substring(name.length, c.length);
    }
    return "";
}

function checkCookie(valor) {
    var user = getCookie(valor);
    if (user != "") {
        return true;
    }
    else {
        return false;
    }
}

function getCampo(campo) {
    var usuario = getUsuario();
    return usuario[campo];
}

function frm_SinEspacios(e) {
    tecla = (document.all) ? e.keyCode : e.which;
    if (tecla == 32) {
        return false;
    }
    if (tecla != 32) {
        return true;
    }
}

function sendDatalayer(url_val, event) {
    if (checkCookie('frmunitec') != true) {
        dataLayer.push({
            'url_value': url_val, //dato dinámico
            'event': event//dato estático
        });
    }
}

function sendCalcInteractions() {
    dataLayer.push({
        'event' : 'ScholCalc',  //Static data     
        'CalcCareer': getUsuario().carrera.Link.toLowerCase(), //Dynamic data     
        'prom': getUsuario().promedio.toString(),   //CD26     ‘ScholarTime’: ‘3’,   //CD28     
        'ScholarAVG': getUsuario().promedio.toString(),   //CD29 y CD26     
        'ScholarSubNum': getUsuario().materias.toString(),   //CD30     
        'ScholarPay': $(".btn-dual-y.selected").text().toLowerCase(),   //CD31   
        'InterLvl' : getUsuario().linea.toLowerCase(),  //CD 5   
        'RCampusSelected': campusNombreLargo(getUsuario().campus).toLowerCase(),   //CD8   
        'RMode': modalidadtxt(getUsuario().modalidad.modalidad).toLowerCase(),   //CD25  
        'RCampusRef': campusNombreLargo(getUsuario().campus).toLowerCase()   //CD27 
    });
}