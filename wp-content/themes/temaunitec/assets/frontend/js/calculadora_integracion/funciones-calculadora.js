

//Funciones de usabilidad para calculadora 
function fillExpandible(el) {
    console.log('dio click');
    console.log(el.data('val'));
    guardaUsuario('linea', el.data('val')); 
    if(paso2){
        var objCookie = JSON.parse(decodeURIComponent(getCookie('c_form_data')));
        dataLayer.push({
            'event' : 'InformationForm',   //Static data
            'formStep' : 'Paso 2',   //Dynamic data
            'Estado' : objCookie.estado.toLowerCase(),   //CD4
            'InterLvl' : '',  //CD 5
            'Period' : '',  //CD 6
            'carrera' : '',  //CD 7
            'campus' : '',  //CD 8
            'Formulario' : 'Calculadora',  //CD 16
            'RMode': '',   //CD25
            'prom': '',   //CD26
            'formDev' : 'Mobile'   // Dynamic data
        });
    }

    paso2 = false; //Desactivación para otras consultas posteriores
    if(paso3){
        var objCookie = JSON.parse(decodeURIComponent(getCookie('c_form_data')));
        dataLayer.push({
            'event' : 'InformationForm',   //Static data
            'formStep' : 'Paso 3',   //Dynamic data
            'Estado' : objCookie.estado.toLowerCase(),   //CD4
            'InterLvl' : getUsuario().linea.toLowerCase(),  //CD 5
            'Period' : '',  //CD 6
            'carrera' : '',  //CD 7
            'campus' : '',  //CD 8
            'Formulario' : 'Calculadora',  //CD 16
            'RMode': '',   //CD25
            'prom': '',   //CD26
            'formDev' : 'Mobile'   // Dynamic data
        });
    }

    paso3 = false; //Desactivación para otras consultas posteriores

    //  el.html('<div class="container"><div class="estudios">estudios</div><div class="modalidades">modalidaes</div><div class="botonera">botonera</div><div class="cerrar">Cerrar</div></div>');
    el.html(jQuery('.plantillas>.p_expandible').html());
    //el.find('.estudios').html('cambio estudios');
    //jQuery(".rs-carrera").prepend('<svg class="return" version="1.1" width="16px" height="16px" viewBox="0 0 459 459"><path d="M178.5,140.25v-102L0,216.75l178.5,178.5V290.7c127.5,0,216.75,40.8,280.5,130.05C433.5,293.25,357,165.75,178.5,140.25z" fill="#ff911e"/></svg>&nbsp;&nbsp;');
    if (el.data('val') == "PREPARATORIA") {
        console.warn('Entro a prepa y se precarga');
        var a = getCarrerasBien(getCarreras(el.data('val')));
        render_carreras(a);
        //guardaUsuario('carrera', JSON.parse('{"value": "56", "label": "Preparatoria", "Categoria": "Preparatoria", "IdCategoria": "56", "Link": "Preparatoria"}'));
        //console.log(getCampo('carrera').Link);
        console.log(getCampo('carrera'));
       // jQuery('.rs-carrera').text(getCampo('carrera').Link);
        render_modalidades(getModalidades("56"));
        //jQuery('.v_mod').hide();
        render_opciones_prepa();
        jQuery('.v_prepa').show();
        jQuery('.preg-est').hide();
        changeFuncCarreras(0)
        console.warn('Entro a prepa y se precarga');
        if(paso4){
            var objCookie = JSON.parse(decodeURIComponent(getCookie('c_form_data')));
            dataLayer.push({
                'event' : 'InformationForm',   //Static data
                'formStep' : 'Paso 4',   //Dynamic data
                'Estado' : objCookie.estado.toLowerCase(),   //CD4
                'InterLvl' : getUsuario().linea.toLowerCase(),  //CD 5
                'Period' : '',  //CD 6
                'carrera' : getUsuario().carrera.Link.toLowerCase(),  //CD 7
                'campus' : '',  //CD 8
                'Formulario' : 'Calculadora',  //CD 16
                'RMode': '',   //CD25
                'prom': '',   //CD26
                'formDev' : 'Mobile'   // Dynamic data
            });
        }
    
        paso4 = false; //Desactivación para otras consultas posteriores
    }
    else {
        var a = getCarrerasBien(getCarreras(el.data('val')));
        render_carreras(a);
        jQuery('.preg-est').show();
        jQuery('.v_prepa').hide();
    }
    jQuery('.btn-footer').attr("title","Regresar");
    // 
    console.log(a);
}

function changeFuncCarreras(selectItem){
    var arrayCarreras = JSON.parse($('#projectSelected').val());
    console.log(arrayCarreras[selectItem]);
    $('#project').blur();
    $(".project").val(arrayCarreras[selectItem].label);
    $(".project-id").val(arrayCarreras[selectItem].value);
    //$("#project-description").html(ui.item.desc);
    $(".project-icon").attr("src", "images/" + arrayCarreras[selectItem].icon);
    //var oferta =[{ "Categoria": ui.item.Categoria, "IdCategoria": ui.item.IdCategoria, "Link": ui.item.Link }];
    guardaUsuario('carrera', arrayCarreras[selectItem]);
    // jQuery('.rs-carrera').text(getCampo('carrera').Link);
    render_modalidades(getModalidades(arrayCarreras[selectItem].IdCategoria));
    jQuery('.v_mod').fadeIn();

    if(paso4){
        var objCookie = JSON.parse(decodeURIComponent(getCookie('c_form_data')));
        dataLayer.push({
            'event' : 'InformationForm',   //Static data
            'formStep' : 'Paso 4',   //Dynamic data
            'Estado' : objCookie.estado.toLowerCase(),   //CD4
            'InterLvl' : getUsuario().linea.toLowerCase(),  //CD 5
            'Period' : '',  //CD 6
            'carrera' : getUsuario().carrera.Link.toLowerCase(),  //CD 7
            'campus' : '',  //CD 8
            'Formulario' : 'Calculadora',  //CD 16
            'RMode': '',   //CD25
            'prom': '',   //CD26
            'formDev' : 'Mobile'   // Dynamic data
        });
    }

    paso4 = false; //Desactivación para otras consultas posteriores
}

function render_carreras(carreras) {
    // console.log('las carreras');
    // console.log(carreras);
    var carrerasjson = [];
    var countIndex = 0;
    jQuery.each(carreras, function (index, value) {
        //console.log(value);
        console.log('My array has at position ' + index + ', this value: ' + value.IdCategoria);
        item = {}
        item["count"] = countIndex;
        item["value"] = value.IdCategoria;
        item["label"] = value.Categoria;
        //item["desc"] = value.Categoria;
        item["icon"] = value.IdDynamics;
        item["Categoria"] = value.Categoria;
        item["IdCategoria"] = value.IdCategoria;
        item["Link"] = value.Link;
        carrerasjson.push(item);
        countIndex++;
    });
    carrerasjson.sort(function (a, b) {
        if (a.Categoria.toLowerCase() < b.Categoria.toLowerCase()) return -1;
        if (a.Categoria.toLowerCase() > b.Categoria.toLowerCase()) return 1;
        return 0;
    });


    var isMobile = IsMobile(); //  /iPhone|iPad|iPod|Android/i.test(navigator.userAgent);

    if(isMobile){
        var html = "";
        $("#project").replaceWith('<select id="project" onchange="changeFuncCarreras(value)" class="input sm-col sm-col-5 m0"><option value="" class="input sm-col sm-col-5 m0" selected disabled hidden>Selecciona una opción</option></select>');
        jQuery.each(carrerasjson, function (index, value) {
            //console.log(value);
            html += '<option class="sm-col sm-col-5" value="'+ index +'">'+ value.label +'</option>'
        });
        /*Se comenta para mantener mostrar la oferta en forma de lista*/
        //html += '<optgroup style="font-size:30px;" label=""></optgroup>';
        $('#project').append(html);
        $('#projectSelected').val(JSON.stringify(carrerasjson));
    } else{
        $(".project").autocomplete({
            minLength: 0,
            source: carrerasjson,
            focus: function (event, ui) {
                $(".project").val(ui.item.label);
                return false;
            },
            select: function (event, ui) {
                console.log(ui);
                $(this).blur();
                $(".project").val(ui.item.label);
                $(".project-id").val(ui.item.value);
                //$("#project-description").html(ui.item.desc);
                $(".project-icon").attr("src", "images/" + ui.item.icon);
                //var oferta =[{ "Categoria": ui.item.Categoria, "IdCategoria": ui.item.IdCategoria, "Link": ui.item.Link }];
                guardaUsuario('carrera', ui.item);
                //jQuery('.rs-carrera').text(getCampo('carrera').Link);
                render_modalidades(getModalidades(ui.item.IdCategoria));
                jQuery('.v_mod').fadeIn();
    
    
                //render_campus(getModalidades(ui.item.IdCategoria));
                // console.log(getCarreras(getCampo("carrera").IdCategoria));
    
                // var campus=alert()("modalidad").campus.toString().split(",").sort();
                // console.log(campus);
    
                //muestraPaso('carrera', 'modalidad');
                return false;
            },
            close: function(event, ui)
            {
                $(this).data().term = null; // Limpia el cache, para que haga siempre una búsqueda nueva
                jQuery('body').css('overflow','');
            }
        })
            .autocomplete("instance")._renderItem = function (ul, item) {
                return $("<li>")
                    .append("<div>" + item.label + "</div>")
                    //.append("<div>" + item.label + "<br>" + item.desc + "</div>")
                    .appendTo(ul);
            };
    }

    

}  

function getCarrerasBien(categorias) {
    var objJsonCarreras = JSON.parse(localStorage.getItem("jsonLinksCategorias"));
    var categoriasno = new Array();
    console.log('Obetniendo carreras');
    console.log(categorias);    
    console.log(Object.entries(categorias));    
    Object.entries(categorias).forEach(function (key, value) {
        categoriasno.push(key[1]['Grupo_carreras']);
    });
    //console.log(categoriasno);
    var nombres = new Array();
    Object.entries(objJsonCarreras).forEach(function (key, value) {
        if (categoriasno.includes(key[1]['IdCategoria'])) {
            nombres.push(key[1]);
        }
    });
    return nombres;
}

function removeDuplicates(arr, key) {
    var values = {};
    return arr.filter(function (item) {
        var val = item[key];
        var exists = values[val];
        values[val] = true;
        return !exists;
    });
}

function getCarreras(linea,campus) {
    var keySearch = "lineaweb";
    var arrayCarrerasPorLinea = [];
    var objJsonCarreras = JSON.parse(localStorage.getItem("jsonCarreras"));
    arrayCarrerasPorLinea = this.buscar(keySearch, linea, objJsonCarreras);
    
    if(linea=='LICENCIATURA'){
        arrayCarrerasPorLineaSalud= this.buscar(keySearch, 'SALUD', objJsonCarreras);
        var arrayCarrerasPorLinea=arrayCarrerasPorLinea.concat(arrayCarrerasPorLineaSalud);
        console.log('AGREGANDO LAS DE SALUD');
    }
    console.log('arrayCarrerasPorLinea__');
    console.log(arrayCarrerasPorLinea);




    return arrayCarrerasPorLinea
}

function finaliza() {
    console.log("_______FUNCION FINALIZA_______");
    getDuracion();
    jQuery('.an-slideUp').each(function () {
        jQuery(this).removeClass('an-slideUp')
    }); // QUITA LA ANIMACION PARA QUE CUANDO HAGA RESIZE NO EJECUTE LA ANIMACION DE NUEVO
    jQuery('body').removeClass('inicio'); // EJECUTO EL ULTIMO PASO, SE QUITA LA BANDERA PARA QUE NO HAGA ANIMACIONES
    jQuery('.cont-quest').removeClass('cont-quest');
    jQuery("#selector-float").animate({
        width: '40%'
    }, function () {
        //jQuery(".output").css(");
        jQuery('document').removeClass('an-slideUp');
        jQuery(".hide").fadeIn();
        jQuery(".hide-on-res").hide();
        jQuery(".quest.p-activa").removeClass('p-activa');
        jQuery(".quest").show();
    });
    irA('sec-promedio', 'resultados');
    //Envio a Proc web
    // envioProcWeb();    

}

function getMinimoMatBeca(catCosto) {
    var beca;
    switch (catCosto) {

        case "NUTRI":
        case "BLUEM":
        case "GASTRO":
        case "FISIO":
        case "TURI":
        case "DENT":
        case "ENFER":
        case "PREPA":
        case "ING":
        case "ESP":
        case "ESPD":
        case "LIC":
        case "LICPSICOLOG":
        case "LICPEDAG":
            minmaterias = 5;
            break


        case "LICEJEC":
        case "INGEJEC":
        case "LICON":
        case "INGON":
            minmaterias = 4;
            break;

        case "DIP":
        case "DIPON":
        case "POS":
        case "POSON":
        case "POSONCUAT":
        case "POSCUAT":
        case "POSCUATS":
            minmaterias = 3
            break;
        default:
            minmaterias = 5;
    }
    return minmaterias;
}

function lineaWeb(){

    var t = "Licenciatura";

    switch (getCampo('modalidad').lineaweb) {
        case "PREPARATORIA":
            t="preparatoria";
            break;
        case "LICENCIATURA":
            t = "licenciatura";
            break;
        case "INGENIERIA":
            t = "ingenieria";
            break;
        case "POSGRADO":
            t = "posgrado";
            break;
        case "SALUD":
            t = "licenciatura";
            break;
    
    }
    return t;
}

function removeSections() {
    // $('#fullpage').children().not(':first').remove();
    // jQuery.fn.fullpage.reBuild(); //Causa problemas en mobile
}

function chargeImages() {
    jQuery('#PREPARATORIA,#PREPARATORIA>.expandible').addClass('expandible-desktop');
    jQuery('#LICENCIATURA,#LICENCIATURA>.expandible').addClass('expandible-desktop');
    jQuery('#INGENIERIA,#INGENIERIA>.expandible').addClass('expandible-desktop');
    jQuery('#SALUD,#SALUD>.expandible').addClass('expandible-desktop');
    jQuery('#POSGRADO,#POSGRADO>.expandible').addClass('expandible-desktop');
}

function resetJsonEleccion() {
    var viejo = JSON.parse(localStorage.getItem("jsonUsr"));
    var prepa = false;
    // console.log("Antes de BORRAR");
    // console.log(viejo);
    if(!viejo){
        viejo = {};
    }
    if(viejo.modalidad != undefined){
        if(viejo.modalidad.nombre == "PREPARATORIA" && jQuery('.rs-carrera').text() == "PREPARATORIA"){
            prepa = true;
        }else{
            delete viejo['carrera'];
            delete viejo['modalidad'];
        }
    }
    
    delete viejo['beca'];
    delete viejo['campus'];
    delete viejo['contado'];
    delete viejo['contadoAhorro'];
    delete viejo['costos'];
    delete viejo['limitMat'];
    delete viejo['materias'];
    delete viejo['mensual'];
    delete viejo['mensualAhorro'];
    delete viejo['promedio'];
    delete viejo['padretutor'];
    console.log(viejo);
    localStorage.setItem("jsonUsr", JSON.stringify(viejo));

    cleanAllInputs(prepa);
}

function cleanAllInputs(prepa){
    $("#project").val("");
    $("#render_prepa").val("");
    $("#render_campus").val("0");
    $("#render_carrera").val("0");
    jQuery('.v_mod').hide();

    if(!prepa){
        jQuery('.v_camp').hide();
        jQuery('.v_prepa').hide();
    } else {
        jQuery('.v_camp').show();
    }
}

function render_opciones_prepa() {
    var html_prepa = '<option value="" selected disabled hidden>Selecciona</option>';
    html_prepa += '<option value="si">Si</option>';
    html_prepa += '<option value="no">No</option>';

    jQuery('#render_prepa').append(html_prepa);
}

function render_modalidades(modalidades) {
    console.warn('render_modalidades');
    console.log(modalidades);
    console.log('Existen:' + modalidades.length);

    //alert(JSON.stringify(modalidades));

    /*******MODALIDADES******/
    if (modalidades.length == 1) {
        var html = '<option selected data-Grupo_carreras="' + modalidades[0].Grupo_carreras + '" data-IdDynamics="' + modalidades[0].IdDynamics + '" data-campus="' + modalidades[0].campus + '"  data-catCosto="' + modalidades[0].catCosto + '" data-interes="' + modalidades[0].interes + '" data-lineaweb="' + modalidades[0].lineaweb + '" data-modalidad="' + modalidades[0].modalidad + '"  data-nombre="' + modalidades[0].nombre + '" value="' + modalidades[0].modalidad + '">Solo ' + modalidadtxt(modalidades[0].modalidad) + '</option>';
      

        jQuery('.v_camp').show();
    } else {
        console.log('mas de 3');
        var html = '<option value="0">Modalidad</option>';
        jQuery.each(modalidades, function (index, value) {
            console.log('My array has at position ' + index + ', this value: ' + value.modalidad + modalidadtxt(value.modalidad));
            html += '<option data-Grupo_carreras="' + value.Grupo_carreras + '" data-IdDynamics="' + value.IdDynamics + '" data-campus="' + value.campus + '"  data-catCosto="' + value.catCosto + '" data-interes="' + value.interes + '" data-lineaweb="' + value.lineaweb + '" data-modalidad="' + value.modalidad + '"  data-nombre="' + value.nombre + '" value="' + value.modalidad + '">' + modalidadtxt(value.modalidad) + '</option>';
        });
    }
    jQuery('#render_modalidad').html(html);


    if (modalidades.length == 1) {
        var dataItem = jQuery('#render_modalidad option:selected').data();
        guardaUsuario('modalidad', dataItem);
    }
    


    /*******CAMPUS******/
    console.log('todos los campus que tienen esta carrera');
    var campus = [];
    jQuery.each(modalidades, function (index, value) { campus.push(value.campus); }); //Guarda los arreglos de campus
    var resultArray = Array.prototype.concat.apply([], campus);
    allcampus = resultArray.join(',');
    var b = unique(allcampus.split(",")); // Array de campus unicos 
    // console.log('Campus');
    // console.log(b);
    // console.log(b.lenght);
    var html = '<option value="0"> Campus</option>';
    jQuery.each(b, function (index, value) {
        html += '<option value="' + value + '">' + campustxt(value) + '</option>';
    });
    jQuery('#render_campus').html(html);

    //Si solo existe En Linea se autoselecciona
    if (jQuery('#render_campus option').eq(1).val()=='ONL' && jQuery('#render_modalidad option').length < 3){
        jQuery('#render_campus option').eq(1).prop('selected', true).change();
    }

}


function getModalidades(categoria) {
    var keySearch = "Grupo_carreras";
    var arrayCarrerasPorLinea = [];
    var objJsonCarreras = JSON.parse(localStorage.getItem("jsonCarreras"));
    arrayCarrerasPorLinea = buscar(
        keySearch,
        categoria,
        objJsonCarreras
    );

    return arrayCarrerasPorLinea
}

function guardaUsuario(valor, data) {
    if (localStorage.getItem("jsonUsr") === null) {
        var jsonnew = {};
        jsonnew[valor] = data;
        localStorage.setItem("jsonUsr", JSON.stringify(jsonnew));
        console.log('no existe el Json de usuario');
    } else {
        var viejo = JSON.parse(localStorage.getItem("jsonUsr"));
        viejo[valor] = data;
        localStorage.setItem("jsonUsr", JSON.stringify(viejo));
    }

}
function getUsuario() {
    return JSON.parse(localStorage.getItem("jsonUsr"));
}


function render_selects() {
    jQuery('.select-selected,.select-items').remove();
    var x, i, j, selElmnt, a, b, c;
    /*look for any elements with the class "custom-select":*/
    x = document.getElementsByClassName("custom-select");
    for (i = 0; i < x.length; i++) {
        selElmnt = x[i].getElementsByTagName("select")[0];
        /*for each element, create a new DIV that will act as the selected item:*/
        a = document.createElement("DIV");
        a.setAttribute("class", "select-selected");
        a.innerHTML = selElmnt.options[selElmnt.selectedIndex].innerHTML;
        x[i].appendChild(a);
        /*for each element, create a new DIV that will contain the option list:*/
        b = document.createElement("DIV");
        b.setAttribute("class", "select-items select-hide");
        for (j = 1; j < selElmnt.length; j++) {
            /*for each option in the original select element,
            create a new DIV that will act as an option item:*/
            c = document.createElement("DIV");
            c.innerHTML = selElmnt.options[j].innerHTML;
            c.addEventListener("click", function (e) {
                /*when an item is clicked, update the original select box,
                and the selected item:*/
                var y, i, k, s, h;
                s = this.parentNode.parentNode.getElementsByTagName("select")[0];
                h = this.parentNode.previousSibling;
                for (i = 0; i < s.length; i++) {
                    if (s.options[i].innerHTML == this.innerHTML) {
                        s.selectedIndex = i;
                        h.innerHTML = this.innerHTML;
                        y = this.parentNode.getElementsByClassName("same-as-selected");
                        for (k = 0; k < y.length; k++) {
                            y[k].removeAttribute("class");
                        }
                        this.setAttribute("class", "same-as-selected");
                        break;
                    }
                }
                h.click();
            });
            b.appendChild(c);
        }
        x[i].appendChild(b);



        b.addEventListener("click", function (e) {
            var idSelect = jQuery(this).parent().find('select').prop('id');
            updateSelect(idSelect);

        });

        a.addEventListener("click", function (e) {
            /*when the select box is clicked, close any other select boxes,
            and open/close the current select box:*/
            e.stopPropagation();
            closeAllSelect(this);
            this.nextSibling.classList.toggle("select-hide");
            this.classList.toggle("select-arrow-active");
            console.log('elegiste:')
            console.log(e);



        });
    }
}
function modalidadtxt(num) {
    switch (parseInt(num)) {
        case 1: return "Presencial"; break;
        case 2: return "Ejecutiva"; break;
        case 3: return "En línea"; break;
        case 4: return "Prepa Vespertina";  break;
        case 5: return "Prepa UNAM"; break;
        case 6: return "Prepa UNITEC"; break;
    }
}
function unique(array) {
    return array.filter(function (el, index, arr) {
        return index == arr.indexOf(el);
    });
}

function campustxt(campus) {
    switch (campus) {
        case 'ATZ': return "Atizapán, Edo. Mex."; break;
        case 'MAR': return "Marina, CDMX"; break;
        case 'ECA': return "Ecatepec, Edo. Mex."; break;
        case 'GDL': return "Guadalajara"; break;
        case 'LEO': return "León, Gto"; break;
        case 'SUR': return "Sur Iztapalapa, CDMX "; break;
        case 'CUI': return "Cuitláhuac, CDMX"; break;
        case 'TOL': return "Toluca, Edo. Mex."; break;
        case 'QRO': return "Querétaro, QRO"; break;
        case 'REY': return "Los Reyes, CDMX"; break;
        case 'ONL': return "En Línea"; break;

    }
}

var safeUrl = (/unitec.mx/.test(window.location.href)) ? "https://unitecmx-universidadtecno.netdna-ssl.com/wp-content/themes/temaunitec" : urlwp;

function blinkFieldForm(id) {
    jQuery("#" + id).addClass("blink-animation");
    setTimeout(function () { jQuery("#" + id).removeClass("blink-animation"); }, 2500);
}

Number.prototype.format = function (n, x) {
    var re = '\\d(?=(\\d{' + (x || 3) + '})+' + (n > 0 ? '\\.' : '$') + ')';
    return this.toFixed(Math.max(0, ~~n)).replace(new RegExp(re, 'g'), '$&,');
};

function closeMobileCalculator() {
    jQuery('#no-critical-calc-mobile').remove();
    jQuery('#UnitecSideNav').fadeToggle();
    jQuery('#bootstrapcss-css').prop("disabled",false);
    jQuery('#mdb-css').prop("disabled",false);
    jQuery('#carousel-banner').fadeToggle();
    jQuery('#calculadora-main-mobile').fadeToggle();
    jQuery('main').fadeToggle();
    jQuery('footer').fadeToggle();
    jQuery('#footer').fadeOut();
}