//INICIALIZACION DE JSONS
jQuery(document).ready(function () {
    getJsonCarreras();
    getJsonLinksCategorias();
});
// getJsonBasura();
// getJsonBasuraEmail();
// getJsonTelefonos();
// getJsonCalcCostos();
// getJsonBecasCalc();
//render_lineas_negocio();
var tutorGTM
var safeUrl = (/unitec.mx/.test(window.location.href)) ? "https://unitecmx-universidadtecno.netdna-ssl.com/wp-content/themes/temaunitec" : urlwp;

function irA(section, slide) {
    console.log('La section es: ' + section);
    console.log('solicitando' + slide);
    var error = 0;
    if (slide == 'openThankYou') {

        /*Se agrega funcionalidad para Customizar Thank You Page como Desktop By SRP 01-07-2019*/
        cambioTextoThankYou();
        /*End Se agrega funcionalidad para Customizar Thank You Page como Desktop By SRP 01-07-2019*/

        console.log('se va a Thank You Page');
        console.log(getUsuario().campus);
        if (getUsuario().carrera === undefined) {
            jQuery('.err_cursar').text("Por favor ingresa una carrera"); error++;
            console.log('carrera Indefinido');
        } else if (getUsuario().modalidad === undefined) {
            jQuery('.err_cursar').text("Por favor ingresa una modalidad"); error++;
        // } else if (getUsuario().campus === undefined || getUsuario().campus == 0) {
        } else if (getUsuario().campus === undefined ||jQuery('#render_campus').val() == "0") {
            jQuery('.err_cursar').text("Por favor ingresa un campus"); error++;
        } else if (getUsuario().modalidad.nombre == "PREPARATORIA" && jQuery('#render_prepa').val() === null) {
            jQuery('.err_cursar').text("Por favor selecciona si eres padre o tutor"); error++;
        }

    }
    if (error == 0) {    
        var inpTutor = jQuery('#render_prepa').val()
        if (inpTutor == null) {
            appendCookie('c_form_data', 'aPaternoTutor', "", 1);
        }else{
            appendCookie('c_form_data', 'aPaternoTutor', jQuery('#render_prepa').val(), 1);    
        }          
        
        console.log('se va a:' + slide);
        jQuery('.err_cursar').html('');
        // jQuery.fn.fullpage.moveTo(section, slide);
        jQuery("#" + section).hide();
        jQuery("#" + slide).show('slow');
        if (slide == 'sec-cursar') {
            resetJsonEleccion();
            jQuery("#sec-promedio").hide();
            jQuery("#resultados").hide();
            jQuery('.mat-ctrl').show().css({ "visibility": "hidden" });
            jQuery('.rs-materias').show().css({ "visibility": "hidden" });
            // sendDatalayer('calculadora/sec-cursar', 'StepCalculator');
        }
        if(slide == "openThankYou") {

            // if(paso5){
            //     var objCookie = JSON.parse(decodeURIComponent(getCookie('c_form_data')));
            //     if (objCookie.aPaternoTutor == null || typeof objCookie.aPaternoTutor == 'undefined') {
            //          tutorGTM = "";    
            //     }else{
            //          tutorGTM = objCookie.aPaternoTutor;
            //     }
            //     if(autocompleteMobile) {
            //         $.each(dataLayerAutoComplete,function(key,value){
            //             value['Estado'] = objCookie.estado.toLowerCase();
            //             dataLayer.push(value);
            //         });
            //     }

            //     dataLayer.push({
            //         'event' : 'InformationForm',   //Static data
            //         'formStep' : 'Paso 5',   //Dynamic data
            //         'Estado' : objCookie.estado.toLowerCase(),   //CD4
            //         'InterLvl' : getUsuario().linea.toLowerCase(),  //CD 5
            //         'Period' : '',  //CD 6
            //         'carrera' : getUsuario().carrera.Link.toLowerCase(),  //CD 7
            //         'campus' : campusNombreLargo(getUsuario().campus).toLowerCase(),  //CD 8
            //         'Formulario' : 'Registro Formulario',  //CD 16
            //         'RMode': modalidadtxt(getUsuario().modalidad.modalidad).toLowerCase(),   //CD25
            //         'prom': '',   //CD26
            //         'formDev' : 'Mobile',   // Dynamic data
            //         'tutor' : tutorGTM
            //     });
            // } 
    
            // paso5 = false; //Desactivación para otras consultas posteriores

            // if(getUsuario().campus === "LEO" || getUsuario().campus === "QRO") {
                construyeInputciclos();
                jQuery("#sec0").hide();
                jQuery("#secCiclos").removeClass('hide');
                jQuery("#ciclosTituloMob").removeClass('hide');
                if(jQuery("#secCiclos .row").children().length == 1){
                    jQuery("#secCiclos .row button")[0].click();
                }
            // } else {
            //     openThankYou();
            // }

        }
        if(slide == "openThankYouCiclos") {
            openThankYou();
        }
        if (slide == 'sec-promedio') {
            // sendDatalayer('calculadora/sec-promedio', 'StepCalculator');
            jQuery('.prom-ctrl').show();
            jQuery('.output').html(jQuery('#range').val());

            jQuery('.btn-footer').click(function () {
                jQuery('.rs-carrera').find('svg').remove();
            });
        }
        if (slide == 'resultados') {
            // sendDatalayer('calculadora/sec-Resultados', 'StepCalculator');
            // calcular();
            jQuery('.btn-dual:last').click();  //Carga termino en años menor
            jQuery('.rs-btn-mensual').click(); //Carga pago mensual
            jQuery('.rs-materias').show().css({ "visibility": "visible" });
            if (getUsuario().modalidad.catcosto == 'ESP' ||
                getUsuario().modalidad.catcosto == 'ESPD'

            ) {
                jQuery('.mat-ctrl').show().css({ "visibility": "hidden" });
            } else {
                jQuery('.mat-ctrl').show().css({ "visibility": "visible" });
            }

            if (getUsuario().modalidad.catcosto == 'DENT') { // Cdentista debe empezar con 5 materias 
                setTimeout(function () { jQuery('.mat-ctrl').click(); }, 100); //DEhabilita boton de 4 años
                guardaUsuario('materias', 5);   //Asigna 5 materias
                // calcular(); //calcula
            }



            // if (checkCookie('envioCRM') == false) {
            //     envioCRM(); 
            // }
            // envioCRM();
        } else {
            // jQuery('.mat-ctrl').show().css({ "visibility": "hidden" });
            // jQuery('.rs-materias').show().css({ "visibility": "hidden" });
        }
    }
    else {
        //alert('un error');
        return false;
    }
}

function regresaContenedor() {
    jQuery("#sec0").show();
    jQuery("#secCiclos").addClass('hide');
    jQuery("#ciclosTituloMob").addClass('hide');
}

function select(clase, el) {
    $(clase).each(function () {
        $(this).removeClass('selected')
    });
    $(el).addClass('selected');
}