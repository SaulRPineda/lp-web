/*ACCIONES DE BINDING */

/******* FUNCIONES DE ABRIR Y CERRAR MENU ********/

jQuery(document).ready(function () {
    jQuery(".submen").prev().prepend("<svg viewBox='0 0 35 35' style='transform: rotate(0deg)'><path style='fill:#ffffff' d='M14.83 16.42l9.17 9.17 9.17-9.17 2.83 2.83-12 12-12-12z'></path><path d='M0-.75h48v48h-48z' fill='none'></path></svg>");
    jQuery(".submen").prev().css({ 'display': 'flex', 'justify-content': 'space-between' })
    jQuery('body').on('click', '.act-menu', function () {
        jQuery('#menun').animate({
            'right': '0vw'

        });
    });
    jQuery('body').on('click', '.fp-tableCell', function () {
        jQuery('#menun').animate({
            'right': '-100vw'

        });
    });
    jQuery('body').on('click', '.closeMenu', function () {
        jQuery('#menun').animate({
            'right': '-100vw'

        });
    });
    jQuery('body').on('click', '.padre', function () {

        jQuery(".submen").not(jQuery(this).siblings(".submen")).hide();
        jQuery(this).siblings(".submen").toggle();
    });

    jQuery('body').on('click', '.sendCalculadoraModal', function () {
        appendCookie('c_form_data', 'urlreferrer', window.location, 1);
        sendFormularioTradicional(false);
        jQuery("#calculadoraModal").removeClass("sendCalculadoraModal");
    });



});

$(document).on("jsonCarrerasFilled", function () {
    jsonCarreras = true;
    console.log("YA ESTA EL JSON DE CARRERASA CARGADO");
});

$(document).on("clickCarrerasFilled", function () {
    if (autocompleteMobile === true) {
        // var nodo_encontrado = buscar("Grupo_carreras", jQuery("#h_id_producto").val(), JSON.parse(localStorage.getItem("jsonCarreras")));
        jsonCarreras = true;
        console.log(nodo_autocomplete);
        var busqueda_grupo = buscar_grupo("IdDynamics", nodo_autocomplete.IdDynamics, JSON.parse(localStorage.jsonCarreras));
        console.log("LA BUSQUEDA DEL GRUPO");
        console.log(busqueda_grupo);
        var categoria_grupo = busqueda_grupo[0].Categoria;
        // console.log("LA BUSQUEDA DEL OPTION");
        // console.log(categoria_grupo);
        // $("#project option:contains('"+categoria_grupo+"')").click();
        /* Agregar un timer para la seleccion de carrera */
        var carrera_selected = $("#project option").filter(function () {
            return $(this).text() == categoria_grupo;
        }).prop('selected', true).change();

        var modalidad_size = $("#render_modalidad").children().length;
        if (modalidad_size == 1) {
            // var txt_modalidad = $("#render_modalidad option[value=" + verif_cookie.modalidad + "]").prop("selected", true).change();
        }
        var campus_size = $("#render_campus").children().length;
        if (campus_size == 2) {
            // var txt_campus = $("#render_campus option[value=" + verif_cookie.campus + "]").prop("selected", true).change();
            $("#render_campus option").first().next().prop("selected", true).change();
        }
        // $(".right.button-primaryn")[0].click();
    }

});


/****CLICK EN LOS CONTROLES DE ABAJO */
jQuery(document).ready(function () {
    jQuery('body').on('click', '.ctrl', function (e) {
        console.log(e);
        irA('calculadora', jQuery(this).data('ir'));
    });

    jQuery('body').on("click", ".sel_exp", function () {
        jQuery(this).parent().parent().parent().find('.expandible').click();
    });



    jQuery('body').on("focus", '.project', function () {
        console.log('focus');
        $(this).data("uiAutocomplete").search("");
        /* Esconder para Cargar correctamente los que aplica */
        $(".v_camp").hide();
        $(".v_mod").hide();

        jQuery('body').css('overflow', 'hidden');
    });



    jQuery("body").on('click', '.mat-ctrl', function () {
        var mat = parseInt(getUsuario().materias);
        jQuery('.btn-dual').removeClass("selected");

        if (jQuery(this).data('valor') == 'min') {

            if (mat <= 1) {
                //jQuery(this).css({ "visibility": "hidden" });       //esconde el control de menos mat 
            } else {
                //jQuery('.mat-ctrl').css({ "visibility": "visible" })
                guardaUsuario('materias', mat - 1);
                jQuery('.rs-materias').html(getUsuario().materias);
                // calcular(); 
            }

        } else {

            if (mat >= getUsuario().limitMat) {
                //jQuery(this).css({ "visibility": "hidden" });       //esconde el control de menos mat 
            } else {
                //jQuery('.mat-ctrl').css({ "visibility": "visible" })
                guardaUsuario('materias', mat + 1);
                jQuery('.rs-materias').html(getUsuario().materias);
                // calcular();
            }


        }

        jQuery('.btn-dual[data-materias="' + getUsuario().materias + '"]').addClass('selected');

    });



    jQuery("body").on('click', '.prom-ctrl', function () {
        var prom = Math.round(jQuery('#range').val() * 10) / 10;


        if (jQuery(this).data('valor') == 'min') {

            if (prom <= 5) {
                // jQuery(this).css({ "visibility": "hidden" });       //esconde el control de menos mat 
            } else {
                console.log('menos');
                console.log('valor acti' + prom);
                console.log(prom - 0.1 * 1);
                // jQuery('.prom-ctrl').css({ "visibility": "visible" });

                jQuery('#range').val(prom - .1);
                jQuery('.output').html(jQuery('#range').val());
                guardaUsuario('promedio', jQuery('#range').val());



                // calcular();
            }

        } else {

            if (prom >= 10) {
                // jQuery(this).css({ "visibility": "hidden" });       //esconde el control de menos mat 
            } else {
                // jQuery('.prom-ctrl').css({ "visibility": "visible" });

                jQuery('#range').val(prom + .1);
                jQuery('.output').html(jQuery('#range').val());
                guardaUsuario('promedio', jQuery('#range').val());

                // calcular();
            }


        }

    });


    /********BOTONES SELECT MODALIDAD Y CAMPUS********/
    jQuery('body').on('change', '#render_modalidad,#render_campus', function () {
        console.log('Guardando modalidad');
        // sendDatalayer('calculadora/sec-modalidad', 'StepCalculator');
        console.log(jQuery('#render_modalidad option:selected').val());



        var dataItem = jQuery('#render_modalidad option:selected').data();
        console.log(dataItem);
        guardaUsuario('modalidad', dataItem);
        //jQuery('.rs-mod').text('Modalidad '+jQuery('#render_modalidad option:selected').text());
        guardaUsuario('campus', jQuery('#render_campus option:selected').val());

        if (jQuery('#render_campus').val() != 0 && jQuery('#render_modalidad').val() != 0) {

            guardaUsuario('campus', jQuery('#render_campus').val());
            jQuery('.rs-campus').text('Campus ' + jQuery('#render_campus option:selected').text());



            //guardaUsuario('linea', dataItem);  
        }

        var nivelAnterior = getNivelAnterior(getCampo('modalidad').interes);
        jQuery('.rs-canterior').text(nivelAnterior);
        jQuery('.v_camp').fadeIn();
        jQuery('.err_cursar').html('');
        // calcular_uno();
    });


    jQuery('body').on('change', '#render_modalidad', function () {
        var a = getModalidades(getCampo("carrera").IdCategoria); // Obtengo de nuevo todas las carreras para filtrar por campus 
        var html = "";
        var campus_select = jQuery('#render_campus').val();
        var modalidad_select = jQuery('#render_modalidad').val();



        if (jQuery('#render_modalidad').val() != 0) { //si cambia modalidad pinta campus 
            var campus = [];
            var html2 = "";
            jQuery.each(a, function (index, value) {
                console.log(value.modalidad + " Busca modalidad:" + modalidad_select);
                if (value.modalidad == modalidad_select) {
                    console.log('Encontro');
                    console.log(value);
                    console.log("campus encontrados");
                    var ArrCamp = value.campus.toString().split(",");
                    html2 += '<option value="0">Campus</option>';
                    jQuery.each(ArrCamp, function (index1, value1) {
                        console.log(value1);
                        html2 += '<option value="' + value1 + '">' + campustxt(value1) + '</option>';
                    });

                }
            }); //Guarda los arreglos de campus

            jQuery('#render_campus').html(html2);
            console.log('OINTA CAMPUS');
            console.log(a);
            console.log(a.length);

            if (jQuery('#render_campus option').length == 2) {
                jQuery('#render_campus option:last').prop('selected', true).change();
            } else {
                jQuery('#render_campus option[value="' + campus_select + '"]').attr('selected', 'selected')
            }



        }


    });

    jQuery('body').on('change', '#render_campus', function () {
        // sendDatalayer('calculadora/sec-campus', 'StepCalculator');
        var a = getModalidades(getCampo("carrera").IdCategoria); // Obtengo de nuevo todas las carreras para filtrar por campus 
        var html = "";
        var campus_select = jQuery('#render_campus').val();
        var modalidad_select = jQuery('#render_modalidad').val();

        if (jQuery('#render_campus').val() != 0) {   //Si Cambia el campus
            html += '<option value="0">Modalidad</option>';
            console.log('cambio campus');
            console.log('Campus Elegido:' + campus_select);
            jQuery.each(a, function (index, value) {
                console.log(a);
                console.log('My array has at position ' + index + ', this value: ' + value.campus);
                console.log(value);
                var c = value.campus.toString();

                // if(c.search(campus_select)!=-1){
                //     console.log('My array has at position ' + index + ', this value: ' + value.modalidad + modalidadtxt(value.modalidad));
                //      html +='<option data-Grupo_carreras="'+value.Grupo_carreras+'" data-IdDynamics="'+value.IdDynamics+'" data-campus="'+value.campus+'"  data-catCosto="'+value.catCosto+'" data-interes="'+value.interes+'" data-lineaweb="'+value.lineaweb+'" data-modalidad="'+value.modalidad+'"  data-nombre="'+value.nombre+'" value="' + value.modalidad + '">' + modalidadtxt(value.modalidad) + '</option>';
                // }; 
            });
            // jQuery('#render_modalidad').html(html);
            // jQuery('#render_modalidad option[value="'+modalidad_select+'"]').attr('selected', 'selected')



        }

        guardaUsuario('campus', jQuery('#render_campus option:selected').val());
        // fillPromedio();

        jQuery(".rs-carrera svg").remove();
        jQuery(".rs-carrera").prepend('<svg class="return" version="1.1" width="16px" height="16px" viewBox="0 0 459 459"><path d="M178.5,140.25v-102L0,216.75l178.5,178.5V290.7c127.5,0,216.75,40.8,280.5,130.05C433.5,293.25,357,165.75,178.5,140.25z" fill="#ff911e"/></svg>');
        // if(getUsuario().modalidad.nombre != "PREPARATORIA"){
        //     irA('calculadora', 'sec-promedio');
        // } else if (getUsuario().modalidad.nombre == "PREPARATORIA" && jQuery('#render_prepa').val() != null){
        //     irA('calculadora', 'sec-promedio');
        // }
    });


    jQuery('body').on('change', '#render_prepa', function () {

        guardaUsuario('padretutor', jQuery('#render_prepa option:selected').val());
        // fillPromedio();

        // if(jQuery('#render_prepa').val() != null && jQuery('#render_campus').val() != 0) {
        //     irA('calculadora', 'sec-promedio');
        // }


    });



    /****CLICK EN EXPANDIBLE */
    jQuery('body').on('click', '.expandible.cerrado', function () {

        // sendDatalayer('calculadora/sec-categoriaclik', 'StepCalculator');
        var ele = jQuery(this);

        jQuery('.rs-carrera').text(ele.data('val'));
        ele.html("");
        //ele.removeClass('cerrado').addClass('abierto');
        ele.css({ 'position': 'absolute', 'left': '0vw', 'z-index': '8' });
        var top = 0;

        var getEstado = JSON.parse(decodeURIComponent(getCookie('c_form_data')));
        var estado = getEstado.estado.toLowerCase();
        
        //guardaUsuarioTest('linea', ele.data('val'));                   //guarda en LocalStorage
        guardaUsuario('linea', ele.data('val'));
        //alert( "Estado: " + estado + " Linea: " + ele.data('val') );

        /*Copies Personalizables By SRP 03-06-2019*/
        // var getCopy = copyPersonalizableMobile( ele.data('val') );
        // jQuery('#subtituloInteres').html( getCopy );
        /*End Copies Personalizables By SRP 03-06-2019*/


        /*Asignmos Carrera Aleatoria By SRP 23-07-2019*/
        /*Considerar solo carreras presenciales*/

        var campus = asignaCampusCorrecto( estado );
        var lineaOpc = getParametros( ele.data('val'), campus );

        /*Campus Correcto por PREPARATORIA By SRP*/
        campus = lineaOpc.campus;

        /*Primera Versión Liberada*/
        //var jsonCarreras = JSON.parse(localStorage.getItem("jsonCarreras"));
        //var jsonLinks = JSON.parse(localStorage.getItem("jsonLinksCategorias"));

        //var resultCarreraFiltro = buscar( lineaOpc.nodo, lineaOpc.busqueda, jsonCarreras);
        //var resultCarrera = buscar( lineaOpc.nodo2, lineaOpc.busquedaLinea, resultCarreraFiltro );
        //var resultCampus = buscarCampus( campus, resultCarrera );

        //guardaUsuarioTest('campus', campus);
        guardaUsuario('campus', campus);

        /*Primera Versión Liberada*/
        //var min = 0;
        //var max = resultCampus.length - parseInt(1) ;
        //var carreraSeleccionada = Math.floor(Math.random() * (max - min)) + min;

        // alert("IdDinamics: " + resultCampus[carreraSeleccionada]['IdDynamics'] + " SubNivel: " + resultCampus[carreraSeleccionada]['interes'] + " Carrera: " + resultCampus[carreraSeleccionada]['nombre'] + " Campus: " + resultCampus[carreraSeleccionada]['campus'] );

        //guardaUsuarioTest('modalidad', resultCampus[carreraSeleccionada]);
        guardaUsuario('modalidad', lineaOpc);
        jQuery("#subInteresTitle").html('');

        /*Primera Versión Liberada*/
        //var getCategoria =  buscar('IdCategoria', resultCampus[carreraSeleccionada]['Grupo_carreras'], jsonLinks);
        //var categoria = { "value": getCategoria[0].IdCategoria, "label": getCategoria[0].Categoria, "Categoria:" : getCategoria[0].Categoria, "IdCategoria": getCategoria[0].IdCategoria, "Link": getCategoria[0].Link };

        //guardaUsuarioTest('carrera', categoria);
        //guardaUsuario('carrera', categoria);

        /*Disparo de DataLayer Paso 3*/
        //fillExpandible(ele);

        if(paso3){
            var objCookie = JSON.parse(decodeURIComponent(getCookie('c_form_data')));
            dataLayer.push({
                'event' : 'InformationForm',   //Static data
                'formStep' : 'Paso 3',   //Dynamic data
                'Estado' : objCookie.estado.toLowerCase(),   //CD4
                'InterLvl' : getUsuario().linea.toLowerCase(),  //CD 5
                'Period' : 'enero',  //CD 6
                'carrera' : getUsuario().modalidad.Link.toLowerCase(),  //CD 7
                'campus' : campusNombreLargo(getUsuario().campus).toLowerCase(),  //CD 8
                'Formulario' : 'Formulario Test Vocacional',  //CD 16
                'RMode': modalidadtxt(getUsuario().modalidad.modalidad).toLowerCase(),   //CD25
                'prom': '',   //CD26
                'formDev' : 'Mobile'   // Dynamic data
            });
        }

        paso3 = false; //Desactivación para otras consultas posteriores

        envioData(campus, 'Mobile');
        cambioTextoThankYou();

        /*Asignmos Carrera Aleatoria By SRP 23-07-2019*/

        //cambioTextoThankYou();

        //irA('calculadora','openThankYou');

        // jQuery('.v_camp').hide();
        // jQuery('.v_mod').hide();

        // if (ele.data('pos') == "c") { var left = "-33.3333333vw"; }
        // if (ele.data('pos') == "i") { var left = "0vw"; }
        // if (ele.data('pos') == "d") { var left = "-66.7vw"; }

        // if (ele.data('pos') == "da") { var left = "-66.7vw"; top = '-43vh' }
        // if (ele.data('pos') == "ia") { var left = "0vw"; top = '-43vh' }

        // // Para MOBILE
        // if (ele.data('pos') == "pr") { var left = "0vw"; top = '0vh' }
        // if (ele.data('pos') == "li") { var left = "0vw"; top = '0vh' }
        // if (ele.data('pos') == "in") { var left = "0vw"; top = '0vh' }
        // if (ele.data('pos') == "sa") { var left = "0vw"; top = '0vh' }
        // if (ele.data('pos') == "po") { var left = "0vw"; top = '0vh' }

        // ele.parent().parent().parent().siblings().hide();


        /*Actualización Para mostrar input de Modalidad y Campus Directamente By SRP 04-07-2019*/
        // jQuery('.v_camp').show();
        // jQuery('.v_mod').show();
        // jQuery('#render_modalidad').append('<option>Modalidad</option>');
        // jQuery('#render_campus').append('<option>Campus</option>');
        /*End Actualización Para mostrar input de Modalidad y Campus Directamente By SRP 04-07-2019*/

        // jQuery(this).animate({
        //     // 'width': '100vw',
        //     'left': '0',
        //     'height': '90vh',
        //     'opacity': '1',
        //     'top': '0'
        // }, function () {
        //     fillExpandible(jQuery(this));
        // });
    });

    /*REVISAR By SRP*/
    /**Cerrando expandible */
    // jQuery('body').on('click', '.cerrar', function () {
    //     var ele = jQuery('.expandible.abierto');
    //     jQuery('.rs-carrera').text('');
    //     ele.html("");
    //     ele.removeClass('abierto').addClass('cerrado');
    //     ele.css({ 'position': 'relative', 'left': '0vw', 'z-index': '0' });

    //     ele.parent().parent().parent().siblings().show();

    //     ele.animate({
    //         'width': '100%',
    //         'height': '100%',
    //         'opacity': '0',
    //         'top': '0'
    //     });

    //     ele.attr('style', ''); //TODO: Luego quitar esto y poner una animacion mejor
    //     jQuery('.btn-footer').removeAttr("title", "Regresar");
    //     // jQuery('.rs-carrera').empty();
    //     // jQuery("#sec-promedio").hide();
    //     // jQuery("#resultados").hide();

    // });


    /* Cambio de menu redes sociales */
    jQuery('.menu-social-container ul').first().appendTo('#menu-menu-dos-niveles');
    /* Cambio de menu redes sociales */

})


