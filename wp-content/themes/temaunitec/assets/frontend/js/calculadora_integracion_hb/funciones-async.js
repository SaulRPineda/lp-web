//INICIALIZACION DE JSONS
getJsonCarreras();
getJsonLinksCategorias();
// getJsonBasura();
// getJsonBasuraEmail();
// getJsonTelefonos();
getJsonCalcCostos();
getJsonBecasCalc();
//render_lineas_negocio();
var tutorGTM
var safeUrl = (/unitec.mx/.test(window.location.href)) ? "https://unitecmx-universidadtecno.netdna-ssl.com/wp-content/themes/temaunitec" : urlwp;

function irA(section, slide) {
    console.log('La section es: ' + section);
    console.log('solicitando' + slide);
    var error = 0;
    if (slide == 'sec-promedio') {
        console.log('se va a sec-promedio');
        console.log(getUsuario().campus);
        if (getUsuario().carrera === undefined) {
            jQuery('.err_cursar').text("Por favor ingresa una carrera"); error++;
            console.log('carrera Indefinido');
        } else if (getUsuario().modalidad === undefined) {
            jQuery('.err_cursar').text("Por favor ingresa una modalidad"); error++;
        } else if (getUsuario().campus === undefined || getUsuario().campus == 0) {
            jQuery('.err_cursar').text("Por favor ingresa un campus"); error++;
        } else if (getUsuario().modalidad.nombre == "PREPARATORIA" && jQuery('#render_prepa').val() === null) {
            jQuery('.err_cursar').text("Por favor selecciona si eres padre o tutor"); error++;
        }

    }
    if (error == 0) {
        var tutorValue = jQuery('#render_prepa').val();
        if (tutorValue == null) {
             tutorGTM = "";    
        }else{
             tutorGTM = tutorValue.charAt(0).toUpperCase() + tutorValue.slice(1);
        }
        console.log('se va a: -- ' + slide);
        jQuery('.err_cursar').html('');
        // jQuery.fn.fullpage.moveTo(section, slide);
        jQuery("#" + section).hide();
        jQuery("#" + slide).show('slow');
        if (slide == 'sec-cursar') {
            console.warn('sec-cursar');
            resetJsonEleccion();
            jQuery("#sec-promedio").hide();
            jQuery("#resultados").hide();
            jQuery('.mat-ctrl').show().css({ "visibility": "hidden" });
            jQuery('.rs-materias').show().css({ "visibility": "hidden" });
            // sendDatalayer('calculadora/sec-cursar', 'StepCalculator');           
        }
        if (slide == 'sec-promedio') {
            console.warn('sec-promedio');
            // if(paso5){
            //     var objCookie = JSON.parse(decodeURIComponent(getCookie('c_form_data')));               
                
            //     // dataLayer.push({
            //     //     'event' : 'InformationForm',   //Static data
            //     //     'formStep' : 'Paso 5',   //Dynamic data
            //     //     'Estado' : objCookie.estado.toLowerCase(),   //CD4
            //     //     'InterLvl' : getUsuario().linea.toLowerCase(),  //CD 5
            //     //     'Period' : 'septiembre',  //CD 6
            //     //     'carrera' : getUsuario().carrera.Link.toLowerCase(),  //CD 7
            //     //     'campus' : campusNombreLargo(getUsuario().campus).toLowerCase(),  //CD 8
            //     //     'Formulario' : 'Calculadora',  //CD 16
            //     //     'RMode': modalidadtxt(getUsuario().modalidad.modalidad).toLowerCase(),   //CD25
            //     //     'prom': '',   //CD26
            //     //     'formDev' : 'Mobile',  // Dynamic data
            //     //     'tutor': tutorGTM
            //     // });                 
            // }
    
            // paso5 = false; //Desactivación para otras consultas posteriores


            // sendDatalayer('calculadora/sec-promedio', 'StepCalculator');
            jQuery("#promedio").show()
            jQuery('.prom-ctrl').show();
            jQuery('.output').html(jQuery('#range').val());

            jQuery('.btn-footer').click(function () {
                jQuery('.rs-carrera').find('svg').remove();
                // jQuery('.v_camp').removeAttr('style');
                // jQuery('.v_mod').removeAttr('style');
                // alert('ya mostradas');
            });
        }
        if (slide == 'resultados') {
            // sendDatalayer('calculadora/sec-Resultados', 'StepCalculator');
console.warn('resultados');
            // if(paso6){
            //     var objCookie = JSON.parse(decodeURIComponent(getCookie('c_form_data')));
            //     // dataLayer.push({
            //     //     'event' : 'InformationForm',   //Static data
            //     //     'formStep' : 'Paso 6',   //Dynamic data
            //     //     'Estado' : objCookie.estado.toLowerCase(),   //CD4
            //     //     'InterLvl' : getUsuario().linea.toLowerCase(),  //CD 5
            //     //     'Period' : 'septiembre',  //CD 6
            //     //     'carrera' : getUsuario().carrera.Link.toLowerCase(),  //CD 7
            //     //     'campus' : campusNombreLargo(getUsuario().campus).toLowerCase(),  //CD 8
            //     //     'Formulario' : 'Calculadora',  //CD 16
            //     //     'RMode': modalidadtxt(getUsuario().modalidad.modalidad).toLowerCase(),   //CD25
            //     //     'prom': getUsuario().promedio.toString(),   //CD26
            //     //     'formDev' : 'Mobile',   // Dynamic data
            //     //     'tutor': tutorGTM
            //     // });
            // }
        
            // paso6 = false; //Desactivación para otras consultas posteriores

            // if(paso7){
            //     console.warn('pasos 7');
            //     var objCookie = JSON.parse(decodeURIComponent(getCookie('c_form_data')));
            //     // dataLayer.push({
            //     //     'event' : 'InformationForm',   //Static data
            //     //     'formStep' : 'Paso 7',   //Dynamic data
            //     //     'Estado' : objCookie.estado.toLowerCase(),   //CD4
            //     //     'InterLvl' : getUsuario().linea.toLowerCase(),  //CD 5
            //     //     'Period' : 'septiembre',  //CD 6
            //     //     'carrera' : getUsuario().carrera.Link.toLowerCase(),  //CD 7
            //     //     'campus' : campusNombreLargo(getUsuario().campus).toLowerCase(),  //CD 8
            //     //     'Formulario' : 'Calculadora',  //CD 16
            //     //     'RMode': modalidadtxt(getUsuario().modalidad.modalidad).toLowerCase(),   //CD25
            //     //     'prom': getUsuario().promedio.toString(),   //CD26
            //     //     'formDev' : 'Mobile',   // Dynamic data
            //     //     'tutor': tutorGTM
            //     // });
            // }
        
            // paso7 = false; //Desactivación para otras consultas posteriores
console.warn('prev calc');
            calcular();
            console.warn('prev btn');
            jQuery('.btn-dual:last').click();  //Carga termino en años menor
            console.warn('prev mens');

            var catActual =  getUsuario().modalidad.catcosto;



            jQuery('.rs-btn-mensual').click(); //Carga pago mensual
            console.warn('prev mat');
            jQuery('.rs-materias').show().css({ "visibility": "visible" });
            console.warn('post calc');
            if (catActual == 'ESP' ||
                catActual == 'ESPD'

            ) {
                jQuery('.mat-ctrl').show().css({ "visibility": "hidden" });
            } else {
                jQuery('.mat-ctrl').show().css({ "visibility": "visible" });
            }

            if (catActual == 'DENT') { // Cdentista debe empezar con 5 materias 
                setTimeout(function () { jQuery('.mat-ctrl').click(); 
                    // jQuery(".aniosduracion button").addClass("btn-disabled");
                }, 100); //DEhabilita boton de 4 años
                guardaUsuario('materias', 7);  //Asigna 5 materias
                calcular(); //calcula
            }
            //mostrar 
            jQuery(".rs-btn-contado").show()
            jQuery(".rs-btn-mensual").text('Mensual')
            jQuery(".reinsctext").text('Reinscripción/Inscripción:')
            jQuery(".reinsccosto").text('Sin Costo*')
            jQuery('.rs-tipopago').parent().text("").html('Pago <span class="rs-tipopago">Mensual</span>:');
            jQuery(".rs-btn-mensual").removeClass("right").addClass("left")
            jQuery('#footercalc1').removeClass('col-6').addClass('col-4');
            jQuery('#footercalc2').removeClass('col-6').addClass('col-4');
            jQuery('#materiasTira').removeClass('hide');

            if(catActual != "DIPEC" && catActual != "PREPAUNAM"){
                console.warn("no dipec")
                jQuery("#footercalc2").show()
                jQuery("#footercalc3").show()
                jQuery(".term-beca").show()
                jQuery(".r-beca").show()
                jQuery(".r-ahorro").show()
                
                //jQuery(".rs-btn-mensual").text('Mensual')                
                if(jQuery('.btn-dual-y.selected').text().toLowerCase() == "mensual"){
                    jQuery('.rs-tipopago').text('Mensual');
                }else{
                    jQuery('.rs-tipopago').text('Contado');
                }            
            }else{
                
                console.warn("si dipec")
                jQuery("#footercalc2").hide()
                jQuery("#footercalc3").hide()
                jQuery(".term-beca").hide()
                jQuery(".r-beca").hide()
                jQuery(".r-ahorro").hide()
                console.warn("1 modulo")
                switch(catActual){
                    case "PREPAUNAM":
                        jQuery(".rs-btn-contado").hide()
                        //jQuery(".rs-btn-mensual").text('Mensual')
                        jQuery(".rs-btn-mensual").removeClass("left").addClass("right")
                        jQuery(".reinsctext").text('Inscripción anual:')
                        jQuery(".reinsccosto").text('$3,950')
                        if(jQuery('.btn-dual-y.selected').text().toLowerCase() == "mensual"){
                            jQuery('.rs-tipopago').parent().text("").html('<span class="rs-tipopago">11 pagos mensuales de:</span>');
                            //jQuery('.rs-tipopago').html(' <span class="rs-tipopago">11 pagos mensuales de:</span>');
                        }else{
                            jQuery('.rs-tipopago').text('Contado');
                        }
                    break;
                    case "DIPEC":
                        jQuery('#footercalc1').removeClass('col-4').addClass('col-6');
                        jQuery('#footercalc2').removeClass('col-4').addClass('col-6');
                        jQuery('#materiasTira').addClass('hide');
                        jQuery(".rs-btn-mensual").text('1 Módulo')
                        if(jQuery('.btn-dual-y.selected').text().toLowerCase() == "1 módulo"){
                            jQuery('.rs-tipopago').text('1 Módulo');
                        }else{
                            jQuery('.rs-tipopago').text('Contado');
                        }
                    break;
                    default:

                    break;

                }
                         
            }
            if(getUsuario().modalidad.catcosto == "PREPAVESP"){
                jQuery(".prepavesp-term").show()
            }else{
                jQuery(".prepavesp-term").hide()
            }

            // sendCalcInteractions();

            // if (checkCookie('envioCRM') == false) {
            //     envioCRM(); 
            // }
            envioCRM();
        } else {
            // jQuery('.mat-ctrl').show().css({ "visibility": "hidden" });
            // jQuery('.rs-materias').show().css({ "visibility": "hidden" });
        }
    }
    else {
        //alert('un error');
        return false;
    }
}

function estadoRandom(telefono) {
    var ladas_edomex = ['42', '58', '59', '71', '72', '74', '76'];
    var top_5_estados = ['GUANAJUATO', 'JALISCO', 'VERACRUZ', 'PUEBLA', 'HIDALGO'];
    // TRAIGO EL VALOR DEL ESTADO
    var prefijo_telefono = telefono.substring(0, 2);
    if (prefijo_telefono === '55') {
        return 'CIUDAD DE MEXICO';
    }
    else if (jQuery.inArray(prefijo_telefono, ladas_edomex) !== -1) {
        return 'ESTADO DE MEXICO';
    }
    else {
        return top_5_estados[Math.floor(Math.random() * top_5_estados.length)];
        // estado='ZACATECAS';
    }
}

function campusDynamics(campus) {
    switch (campus) {
        case "ATZ":
            return "ATIZAPAN";
            break;
        case "REY":
            return "LOS REYES";
            break;
        case "MAR":
            return "MARINA";
            break;
        case "ECA":
            return "ECATEPEC";
            break;
        case "SUR":
            return "SUR";
            break;
        case "CUI":
            return "CUITLAHUAC";
            break;
        case "LEO":
            return "LEON";
            break;
        case "REY":
            return "Los Reyes";
            break;
        case "TOL":
            return "TOLUCA";
            break;
        case "GDL":
            return "GUADALAJARA";
            break;
        case "ONL":
            return "EN LINEA";
            break
        case "QRO":
            return "QUERETARO";
            break
        default:
            return "MARINA";
    }
}

function envioCRM() {

    sendCalcInteractions();

    if( checkCookie('cid_unitec') === true ) {
        appendCookie('c_form_data', 'CID', getCookie("cid_unitec"), 1);
    }

    var seleccionJson = JSON.parse(localStorage.getItem("jsonUsr"));
    if (seleccionJson.form != undefined) {
        if (seleccionJson.form.frm_amaterno == "telemarketer") {
            seleccionJson.form["estado"] = "AGUASCALIENTES";
            seleccionJson.form["banner"] = seleccionJson.form.frm_banner;
            seleccionJson.form["nombre"] = seleccionJson.form.frm_nombre;
            seleccionJson.form["apaterno"] = seleccionJson.form.frm_apaterno;
            seleccionJson.form["amaterno"] = seleccionJson.form.frm_amaterno;
            seleccionJson.form["email"] = seleccionJson.form.frm_correo;
            seleccionJson.form["celular"] = seleccionJson.form.frm_celular;
            seleccionJson.form["tipoRegistro"] = seleccionJson.form.revalidacion;
            var form_cookie = seleccionJson.form;
        }
    } else {
        var form_cookie = JSON.parse(decodeURIComponent(getCookie('c_form_data')));
    }

    /*Implementación Variables para envio a Hubspot se obtiene las que se muestran en primera instancia By SRP 09-07-2019*/
    //num_carrera: materiasH,
    //costoMensual: costoXmateriaMensH,
    var campusH = getCampo('campus');
    var costosH = getCampo('costos');
    var materiasH;
    var costoXmateriaMensH = costosH['costoMateria']['4pagos'][campusH];

    materiasH = parseInt( getCampo('materias') );

    // if( getCampo('modalidad').interes == 2 || getCampo('modalidad').interes == 3 || getCampo('modalidad').interes == 4 ){
    //     materiasH = parseInt( getCampo('materias') );
    // } else{
    //     materiasH = parseInt( getCampo('materias') );
    // }
    //alert( "Materias: " + materiasH + "Costo 1M: " + costoXmateriaMensH );
/*End Implementación Variables para envio  Hubspot se obtiene las que se muestran en primera instancia By SRP 09-07-2019*/

    var nivelOferta = getCampo('modalidad').interes == 7 ? "P" : "L";
    var Herramienta = (getCampo('modalidad').interes == 4 || getCampo('modalidad').interes == 6) ? "ONLINEP" : getCampo('modalidad').interes == 7 ? "ASPIRANTES PREPA" : "ASPIRANTES LIC";
    var Campana = 1;
    var Origen = (getCampo('modalidad').interes == 4 || getCampo('modalidad').interes == 6) ? "ONLINEP" : getCampo('modalidad').interes == 7 ? "ASPIRANTES PREPA" : "ASPIRANTES LIC";
    var Banner = getCookie('banner_activo') != "" ? getCookie('banner_activo') : form_cookie.banner;
    var Calidad = 3;
    //var L_Negocio = "U"; //TODO: validar si es necesario ya que este camp se calcula en procweb
    var L_Negocio = getNivelInteres(seleccionJson.modalidad.lineaweb);
    var F_Nacim = "01/01/2000";
    var Ciclo = "19-1";
    var Alumno = 1;
    var Estad = estadoRandom(jQuery('#frm_celular').val());
    var C_Carrera = getCampo('modalidad').iddynamics;
    var Carrera = getCampo('modalidad').nombre;
    var banner_id = getCookie('banner_activo') != "" ? getCookie('banner_activo') : form_cookie.banner;
    var CP = (jQuery('#calcApp').data('campus') == 'QRO' || jQuery('#calcApp').data('selectLinea') == 'POSGRADO') ? 10040 : jQuery("#revalidacion").val()
    var Pobla = getCampo('modalidad').interes;
    var name_form = "";
    var Sexo = "M";
    var nom_carrera = "";
    var nom_campus = campusDynamics(getCampo('campus'));
    var promedioF = getCampo('promedio');
    var Pdescuento = getCampo('beca');
    var nivelF = "L";
    var CostoUnico = 0;
    var num_carrera = materiasH;
    var costoMensual = costoXmateriaMensH;
    var descuentoF = 0;
    var planSelec = "4pagos";
    var CostoSelec = getCampo('mensual');
    var planRecom = "4pagos";
    var CostoRecom = getCampo('mensual');
    var envioB = 1;
    var C_Carrera_WP = "";
    var Direcc = "WEBCALL";
    var Colon = "";
    var Nombre2 = "";
    var email2 = "";
    var URLreferer = window.location.href;
    var debugs = 1;
    var gclid_field = "";
    var userId_field = "";
    var CID = form_cookie.CID;
    var costoMateria = getCampo('costos').costoMateria['1pago'][getCampo('campus')];
    var email_unitec = "";
    var otraConsulta = (checkCookie('envioCRM') == false) ? 0 : 1;
    var SERVER_URL = 0;
    var frm_LadaLenght = 0;
    var frm_LadaAllow = 0;
    var alum_2_2 = 0;
    var Nombre = form_cookie.nombre;
    var Ap_Pat = form_cookie.apaterno;
    var Ap_Mat = form_cookie.amaterno;
    var email = form_cookie.email;
    var informacion_2 = 1;
    var Estado = Estad;
    var masculino = "M";
    var TipoTel = "cel_calc";
    var cel_calc = "";
    var Telef = form_cookie.celular;
    var periodo_interes_sel = "19-1";
    var politica = "on";
    var promedio = getCampo('promedio');
    var programa = getCampo('modalidad').iddynamics;
    var materias = getCampo('materias');
    var hubspotutk = getCookie('hubspotutk');
    var cookieHubspot = getCookie('hubspotutk');
    var padretutor = "NO";
    var termino = "";
    var pago_mensual_beca = getCampo('mensual');
    var pago_mens_beca = getCampo('mensual');
    var formulario_completado = "paso_2";
    var form_origen = "Calculadora";
    var anos_termino = "";
    //TODO: comentar para quitar pregunta  
    if (tutorGTM != 'Si' || tutorGTM != 'No') {
        if (tutorGTM == 'Si') {
            var padretutor = 'TUTOR';
        }
        else {
            var padretutor = 'No';
        }
    }
    else {
        var padretutor = 'No';
    }
    var fdata = "nivelOferta=" + nivelOferta + "&Herramienta=" + Herramienta + "&Campana=" + Campana + "&Origen=" + Origen + "&Banner=" + Banner + "&Calidad=" + Calidad + "&L_Negocio=" + L_Negocio + "&F_Nacim=" + F_Nacim + "&Ciclo=" + Ciclo + "&Alumno=" + Alumno + "&Estad=" + Estad + "&C_Carrera=" + C_Carrera + "&Carrera=" + Carrera + "&banner_id=" + banner_id + "&CP=" + CP + "&Pobla=" + Pobla + "&name_form=" + name_form + "&Sexo=" + Sexo + "&nom_carrera=" + nom_carrera + "&nom_campus=" + nom_campus + "&promedioF=" + promedioF + "&Pdescuento=" + Pdescuento + "&nivelF=" + nivelF + "&CostoUnico=" + CostoUnico + "&num_carrera=" + num_carrera + "&costoMensual=" + costoMensual + "&descuentoF=" + descuentoF + "&planSelec=" + planSelec + "&CostoSelec=" + CostoSelec + "&planRecom=" + planRecom + "&CostoRecom=" + CostoRecom + "&envioB=" + envioB + "&C_Carrera_WP=" + C_Carrera_WP + "&Direcc=" + Direcc + "&Colon=" + Colon + "&Nombre2=" + Nombre2 + "&email2=" + email2 + "&URLreferer=" + URLreferer + "&debugs=" + debugs + "&gclid_field=" + gclid_field + "&userId_field=" + userId_field + "&CID=" + CID + "&costoMateria=" + costoMateria + "&email_unitec=" + email_unitec + "&otraConsulta=" + otraConsulta + "&SERVER_URL=" + SERVER_URL + "&frm_LadaLenght=" + frm_LadaLenght + "&frm_LadaAllow=" + frm_LadaAllow + "&alum_2_2=" + alum_2_2 + "&Nombre=" + Nombre + "&Ap_Pat=" + Ap_Pat + "&Ap_Mat=" + Ap_Mat + "&email=" + email + "&informacion_2=" + informacion_2 + "&Estado=" + Estado + "&masculino=" + masculino + "&TipoTel=" + TipoTel + "&Telef=" + Telef + "&periodo_interes_sel=" + periodo_interes_sel + "&politica=" + politica + "&promedio=" + promedio + "&programa=" + programa + "&materias=" + materias + "&hubspotutk=" + cookieHubspot + "&aPaternoTutor=" + padretutor + "&termino=" + anos_termino + "&pago_mensual_beca=" + pago_mens_beca;
    console.log("ANTES DE ENVIAR:");
    console.log(fdata);
    var urlCalc = (checkCookie('envioCRM') == false) ? "/wp-content/themes/temaunitec/calculadora/v2_calc/almacen1.php" : "/wp-content/themes/temaunitec/calculadora/v2_calc/otra_consulta.php";
    console.log(urlCalc);
    jQuery.ajax({
        url: urlCalc,
        data: fdata,
        dataType: "JSON",
        cache: false,
        type: "POST",
        success: function (msg) {
            //console.log("Ejecutado"+msg);            
            //Cuando retorne el valor debe considerarse como otra consulta ya que seguira enviando sus datos.
            setCookie('frmunitec', "calculadora", 1);
            if (msg.tipo == "nuevo") {
                // dataLayer.push({
                //     'event': 'CALCULADORA_REGISTRO'
                // });
                // dataLayer.push({
                //     'origenLead': 'frmCalcu', //dato dinámico
                //     'isAlumn': Alumno, //dato dinámico
                //     'ingress': CP, //dato dinámico
                //     'state': Estado, //dato dinámico
                //     'levelStudies': L_Negocio, //dato dinámico
                //     'Period': Ciclo, //dato dinámico
                //     'carrera': Carrera, //dato dinámico
                //     'campus': nom_campus, //dato dinámico
                //     'date': new Date(), //dato dinámico
                //     'event': 'leadGenerationCalcu' //dato estático
                // });

                // if (typeof dataLayer != 'undefined') {            
                //   dataLayer.push({
                //           'event' : 'leadGeneration', //Static data
                //           //'ClientId' : '12afgh34', //sacar de analytics
                //           'leadId' : msg.leadId, //CD 15
                //           'Formulario' : 'Formulario Calculadora', //CD 16
                //           'campus' : nom_campus, //CD 8
                //           'carrera' :  Carrera, //CD 7
                //           'InterLvl' : getUsuario().modalidad.lineaweb, //CD 5
                //           'Period' : "septiembre", //CD 6
                //           'RMode' :  modalidadtxt(getUsuario().modalidad.modalidad),
                //           'prom': promedio,
                //           'Estado' : Estad,
                //           'NumbrReg' : '1', //CD 24
                //           'tutor': tutorGTM
                //   });
                // }
            }
            else {
                // dataLayer.push({
                //     'event': 'CALCULADORA_DUPLICADO'
                // });

                // dataLayer.push({
                //     'url_value': 'formularioCalculadora/Duplicado', //dato dinámico
                //     'event': 'StepForm' //dato estático
                // });

                // if (typeof dataLayer != 'undefined') {            
                //   dataLayer.push({
                //           'event' : 'leadGeneration', //Static data
                //           //'ClientId' : '12afgh34', //sacar de analytics
                //           'leadId' : msg.leadId, //CD 15
                //           'Formulario' : 'Formulario Calculadora', //CD 16
                //           'campus' : nom_campus, //CD 8
                //           'carrera' :  Carrera, //CD 7
                //           'InterLvl' : getUsuario().modalidad.lineaweb, //CD 5
                //           'Period' : "septiembre", //CD 6
                //           'RMode' :  modalidadtxt(getUsuario().modalidad.modalidad),
                //           'prom': promedio,
                //           'Estado' : Estad,
                //           'NumbrReg' : '2', //CD 24
                //           'tutor': tutorGTM
                //   });
                // }
            }
            if (checkCookie('envioCRM') == false) {
                setCookie('envioCRM', 1, 1);
            }
            document.cookie = "utm_campaign=; expires=Thu, 01 Jan 1970 00:00:00 UTC;path=/"; //elimina la cookie utm_campaign una vez enviado
        }
    });
}

function envioHubspot() {
    var form_step1 = jQuery("#frm-calculadora");

    if (form_step1.valid() === false) {
        console.log("No enviamos existen Errores");
        return false;
    } else {
        if (checkCookie('regcompleto')) {
            console.log("Ya no lo Enviamos Ya existe Cookie");
        } else {
            console.log("Enviamos Registro Primera Vez");

            var nombre = jQuery("#frm_nombre").val();
            var apaterno = jQuery("#frm_apaterno").val();
            var amaterno = jQuery("#frm_amaterno").val();
            var email = jQuery("#frm_correo").val();
            var phone = jQuery("#frm_celular").val();
            var formulario_completado = "paso_1";
            var form_origen = "Calculadora";

            var CadenaDatos = "nombre=" + nombre +
                "&apaterno=" + apaterno +
                "&amaterno=" + amaterno +
                "&email=" + email +
                "&phone=" + phone +
                "&formulario_completado=" + formulario_completado +
                "&msgoff=" + form_origen;

            var urlmicroregistro = "https://www.unitec.mx/desk/procWeb/microregistro.php";

            jQuery.ajax({
                url: urlmicroregistro,
                type: 'POST',
                data: CadenaDatos,
                success: function (res) {
                    console.log("RESULTADO: " + res);
                },
            });

            return true;
        }
        return true;
    }
}

function select(clase, el) {
    $(clase).each(function () {
        $(this).removeClass('selected')
    });
    $(el).addClass('selected');
}