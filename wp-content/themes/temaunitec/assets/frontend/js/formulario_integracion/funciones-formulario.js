

//Funciones de usabilidad para calculadora 
function fillExpandible(el) {
    console.log('dio click');
    console.log(el.data('val'));
    guardaUsuario('linea', el.data('val')); 
    if(paso3){
        var objCookie = JSON.parse(decodeURIComponent(getCookie('c_form_data')));
        var objectDataLayer = {
            'event' : 'InformationForm',   //Static data
            'formStep' : 'Paso 3',   //Dynamic data
            'Estado' : (autocompleteMobile) ? 'definir' : objCookie.estado.toLowerCase(),   //CD4
            'InterLvl' : getUsuario().linea.toLowerCase(),  //CD 5
            'Period' : '',  //CD 6
            'carrera' : '',  //CD 7
            'campus' : '',  //CD 8
            'Formulario' : 'Registro Formulario',  //CD 16
            'RMode': '',   //CD25
            'prom': '',   //CD26
            'formDev' : 'Mobile'   // Dynamic data
        };

        if(!autocompleteMobile){
            dataLayer.push(objectDataLayer);
        } else {
            dataLayerAutoComplete.push(objectDataLayer);
        }
    }

    paso3 = false; //Desactivación para otras consultas posteriores

    //  el.html('<div class="container"><div class="estudios">estudios</div><div class="modalidades">modalidaes</div><div class="botonera">botonera</div><div class="cerrar">Cerrar</div></div>');
    el.html(jQuery('.plantillas>.p_expandible').html());
    //el.find('.estudios').html('cambio estudios');
    //jQuery(".rs-carrera").prepend('<svg class="return" version="1.1" width="16px" height="16px" viewBox="0 0 459 459"><path d="M178.5,140.25v-102L0,216.75l178.5,178.5V290.7c127.5,0,216.75,40.8,280.5,130.05C433.5,293.25,357,165.75,178.5,140.25z" fill="#ff911e"/></svg>&nbsp;&nbsp;');

    /*SRP*/
    //alert(el.data('val'));

    if (el.data('val') == "PREPARATORIA") {
        console.log('Entro a prepa y se precarga');
         var a = getCarrerasBien(getCarreras(el.data('val')));
        render_carreras_mobile(a);
        //guardaUsuario('carrera', JSON.parse('{"value": "56", "label": "Preparatoria", "Categoria": "Preparatoria", "IdCategoria": "56", "Link": "Preparatoria"}'));
        //console.log(getCampo('carrera').Link);
        //console.log(getCampo('carrera'));
        // jQuery('.rs-carrera').text(getCampo('carrera').Link);
        render_modalidades_mobile(getModalidades("56"));
        //jQuery('.v_mod').hide();
        render_opciones_prepa();
        jQuery('.v_prepa').show();
        jQuery('.preg-est').hide();
        changeFuncCarreras(0)

        if(paso4){
            var objCookie = JSON.parse(decodeURIComponent(getCookie('c_form_data')));
            var objectDataLayer = {
                'event' : 'InformationForm',   //Static data
                'formStep' : 'Paso 4',   //Dynamic data
                'Estado' : (autocompleteMobile) ? 'definir' : objCookie.estado.toLowerCase(),   //CD4
                'InterLvl' : getUsuario().linea.toLowerCase(),  //CD 5
                'Period' : '',  //CD 6
                'carrera' : getUsuario().carrera.Link.toLowerCase(),  //CD 7
                'campus' : '',  //CD 8
                'Formulario' : 'Registro Formulario',  //CD 16
                'RMode': '',   //CD25
                'prom': '',   //CD26
                'formDev' : 'Mobile'   // Dynamic data
            };

            if(!autocompleteMobile){
                dataLayer.push(objectDataLayer);
            } else {
                dataLayerAutoComplete.push(objectDataLayer);
            }
        }
    
        paso4 = false; //Desactivación para otras consultas posteriores
        
    }
    else {
        var a = getCarrerasBien(getCarreras(el.data('val')));
        render_carreras_mobile(a);
        jQuery('.preg-est').show();
        jQuery('.v_prepa').hide();
    }
    jQuery('.btn-footer').attr("title", "Regresar");
    // 
    console.log(a);
    $( document ).trigger( "clickCarrerasFilled");
}

function changeFuncCarreras(selectItem) {
    var arrayCarreras = JSON.parse($('#projectSelected').val());
    console.log(arrayCarreras[selectItem]);
    $('#project').blur();
    $(".project").val(arrayCarreras[selectItem].label);
    $(".project-id").val(arrayCarreras[selectItem].value);
    //$("#project-description").html(ui.item.desc);
    $(".project-icon").attr("src", "images/" + arrayCarreras[selectItem].icon);
    //var oferta =[{ "Categoria": ui.item.Categoria, "IdCategoria": ui.item.IdCategoria, "Link": ui.item.Link }];
    guardaUsuario('carrera', arrayCarreras[selectItem]);
    // jQuery('.rs-carrera').text(getCampo('carrera').Link);
    render_modalidades_mobile(getModalidades(arrayCarreras[selectItem].IdCategoria));
    jQuery('.v_mod').fadeIn();

    if(paso4){
        var objCookie = JSON.parse(decodeURIComponent(getCookie('c_form_data')));
        var objectDataLayer = {
            'event' : 'InformationForm',   //Static data
            'formStep' : 'Paso 4',   //Dynamic data
            'Estado' : (autocompleteMobile) ? 'definir' : objCookie.estado.toLowerCase(),   //CD4
            'InterLvl' : getUsuario().linea.toLowerCase(),  //CD 5
            'Period' : '',  //CD 6
            'carrera' : getUsuario().carrera.Link.toLowerCase(),  //CD 7
            'campus' : '',  //CD 8
            'Formulario' : 'Registro Formulario',  //CD 16
            'RMode': '',   //CD25
            'prom': '',   //CD26
            'formDev' : 'Mobile'   // Dynamic data
        };

        if(!autocompleteMobile){
            dataLayer.push(objectDataLayer);
        } else {
            dataLayerAutoComplete.push(objectDataLayer);
        }
    }

    paso4 = false; //Desactivación para otras consultas posteriores
}

function render_carreras_mobile(carreras) {
    // console.log('las carreras');
    // console.log(carreras);
    var carrerasjson = [];
    var countIndex = 0;
    jQuery.each(carreras, function (index, value) {
        //console.log(value);
        console.log('My array has at position ' + index + ', this value: ' + value.IdCategoria);
        item = {}
        item["count"] = countIndex;
        item["value"] = value.IdCategoria;
        item["label"] = value.Categoria;
        //item["desc"] = value.Categoria;
        item["icon"] = value.IdDynamics;
        item["Categoria"] = value.Categoria;
        item["IdCategoria"] = value.IdCategoria;
        item["Link"] = value.Link;
        carrerasjson.push(item);
        countIndex++;
    });
    carrerasjson.sort(function (a, b) {
        if (a.Categoria.toLowerCase() < b.Categoria.toLowerCase()) return -1;
        if (a.Categoria.toLowerCase() > b.Categoria.toLowerCase()) return 1;
        return 0;
    });

    var html = "";
    $("#project").replaceWith('<select id="project" onchange="changeFuncCarreras(value)" class="input sm-col sm-col-5 m0"><option value="" class="input sm-col sm-col-5 m0" selected disabled hidden>Selecciona una opción</option></select>');
    jQuery.each(carrerasjson, function (index, value) {
        //console.log(value);
        html += '<option class="sm-col sm-col-5" value="' + index + '">' + value.label + '</option>'
    });
    /*Se comenta para mantener mostrar la oferta en forma de lista*/
    //html += '<optgroup style="font-size:30px;" label=""></optgroup>';
    $('#project').append(html);
    $('#projectSelected').val(JSON.stringify(carrerasjson));

}

function getCarrerasBien(categorias) {
    var objJsonCarreras = JSON.parse(localStorage.getItem("jsonLinksCategorias"));
    var categoriasno = new Array();
    console.log('Obetniendo carreras');
    console.log(categorias);
    console.log(Object.entries(categorias));
    Object.entries(categorias).forEach(function (key, value) {
        categoriasno.push(key[1]['Grupo_carreras']);
    });
    //console.log(categoriasno);
    var nombres = new Array();
    Object.entries(objJsonCarreras).forEach(function (key, value) {
        if (categoriasno.includes(key[1]['IdCategoria'])) {
            nombres.push(key[1]);
        }
    });
    return nombres;
}

function removeDuplicates(arr, key) {
    var values = {};
    return arr.filter(function (item) {
        var val = item[key];
        var exists = values[val];
        values[val] = true;
        return !exists;
    });
}

function getCarreras(linea, campus) {
    var keySearch = "lineaweb";
    var arrayCarrerasPorLinea = [];
    var objJsonCarreras = JSON.parse(localStorage.getItem("jsonCarreras"));
    arrayCarrerasPorLinea = this.buscar(keySearch, linea, objJsonCarreras);

    if (linea == 'LICENCIATURA') {
        arrayCarrerasPorLineaSalud = this.buscar(keySearch, 'SALUD', objJsonCarreras);
        var arrayCarrerasPorLinea = arrayCarrerasPorLinea.concat(arrayCarrerasPorLineaSalud);
        console.log('AGREGANDO LAS DE SALUD');
    }
    console.log('arrayCarrerasPorLinea__');
    console.log(arrayCarrerasPorLinea);




    return arrayCarrerasPorLinea
}

function chargeImages() {
    jQuery('#PREPARATORIA,#PREPARATORIA>.expandible').addClass('expandible-desktop');
    jQuery('#LICENCIATURA,#LICENCIATURA>.expandible').addClass('expandible-desktop');
    jQuery('#INGENIERIA,#INGENIERIA>.expandible').addClass('expandible-desktop');
    jQuery('#SALUD,#SALUD>.expandible').addClass('expandible-desktop');
    jQuery('#POSGRADO,#POSGRADO>.expandible').addClass('expandible-desktop');
}

function resetJsonEleccion() {
    var viejo = JSON.parse(localStorage.getItem("jsonUsr"));
    var prepa = false;
    // console.log("Antes de BORRAR");
    // console.log(viejo);
    if (!viejo) {
        viejo = {};
    }
    if (viejo.modalidad != undefined) {
        if (viejo.modalidad.nombre == "PREPARATORIA" && jQuery('.rs-carrera').text() == "PREPARATORIA") {
            prepa = true;
        } else {
            delete viejo['carrera'];
            delete viejo['modalidad'];
        }
    }

    delete viejo['beca'];
    delete viejo['campus'];
    delete viejo['contado'];
    delete viejo['contadoAhorro'];
    delete viejo['costos'];
    delete viejo['limitMat'];
    delete viejo['materias'];
    delete viejo['mensual'];
    delete viejo['mensualAhorro'];
    delete viejo['promedio'];
    delete viejo['padretutor'];
    console.log(viejo);
    localStorage.setItem("jsonUsr", JSON.stringify(viejo));

    cleanAllInputs(prepa);
}

function cleanAllInputs(prepa) {
    $("#project").val("");
    $("#render_prepa").val("");
    $("#render_campus").val("0");
    $("#render_carrera").val("0");
    jQuery('.v_mod').hide();

    if (!prepa) {
        jQuery('.v_camp').hide();
        jQuery('.v_prepa').hide();
    } else {
        jQuery('.v_camp').show();
    }
}

function render_opciones_prepa() {
    var html_prepa = '<option value="" selected disabled hidden>Selecciona</option>';
    html_prepa += '<option value="Si">Si</option>';
    html_prepa += '<option value="No">No</option>';

    jQuery('#render_prepa').append(html_prepa);
}

function render_modalidades_mobile(modalidades) {
    console.log('render_modalidades');
    console.log(modalidades);
    console.log('Existen:' + modalidades.length);

    /*******MODALIDADES******/
    if (modalidades.length == 1) {
        var html = '<option selected data-Grupo_carreras="' + modalidades[0].Grupo_carreras + '" data-IdDynamics="' + modalidades[0].IdDynamics + '" data-campus="' + modalidades[0].campus + '"  data-catCosto="' + modalidades[0].catCosto + '" data-interes="' + modalidades[0].interes + '" data-lineaweb="' + modalidades[0].lineaweb + '" data-modalidad="' + modalidades[0].modalidad + '"  data-nombre="' + modalidades[0].nombre + '" value="' + modalidades[0].modalidad + '">Solo ' + modalidadtxt(modalidades[0].modalidad) + '</option>';


        jQuery('.v_camp').show();
    } else {
        console.log('mas de 3');
        var html = '<option value="0">Modalidad</option>';
        jQuery.each(modalidades, function (index, value) {
            console.log('My array has at position ' + index + ', this value: ' + value.modalidad + modalidadtxt(value.modalidad));
            html += '<option data-Grupo_carreras="' + value.Grupo_carreras + '" data-IdDynamics="' + value.IdDynamics + '" data-campus="' + value.campus + '"  data-catCosto="' + value.catCosto + '" data-interes="' + value.interes + '" data-lineaweb="' + value.lineaweb + '" data-modalidad="' + value.modalidad + '"  data-nombre="' + value.nombre + '" value="' + value.modalidad + '">' + modalidadtxt(value.modalidad) + '</option>';
        });
    }
    jQuery('#render_modalidad').html(html);


    if (modalidades.length == 1) {
        var dataItem = jQuery('#render_modalidad option:selected').data();
        guardaUsuario('modalidad', dataItem);
    }



    /*******CAMPUS******/
    console.log('todos los campus que tienen esta carrera');
    var campus = [];
    jQuery.each(modalidades, function (index, value) { campus.push(value.campus); }); //Guarda los arreglos de campus
    var resultArray = Array.prototype.concat.apply([], campus);
    allcampus = resultArray.join(',');
    var b = unique(allcampus.split(",")); // Array de campus unicos 
    // console.log('Campus');
    // console.log(b);
    // console.log(b.lenght);
    var html = '<option value="0"> Campus</option>';
    jQuery.each(b, function (index, value) {
        html += '<option value="' + value + '">' + campustxt(value) + '</option>';
    });
    jQuery('#render_campus').html(html);

    //Si solo existe En Linea se autoselecciona
    if (jQuery('#render_campus option').eq(1).val() == 'ONL' && jQuery('#render_modalidad option').length < 3) {
        jQuery('#render_campus option').eq(1).prop('selected', true).change();
    }

}


function getModalidades(categoria) {
    var keySearch = "Grupo_carreras";
    var arrayCarrerasPorLinea = [];
    var objJsonCarreras = JSON.parse(localStorage.getItem("jsonCarreras"));
    arrayCarrerasPorLinea = buscar(
        keySearch,
        categoria,
        objJsonCarreras
    );

    return arrayCarrerasPorLinea
}

var safeUrl = (/unitec.mx/.test(window.location.href)) ? "https://unitecmx-universidadtecno.netdna-ssl.com/wp-content/themes/temaunitec" : urlwp;

function blinkFieldForm(id) {
    jQuery("#" + id).addClass("blink-animation");
    setTimeout(function () { jQuery("#" + id).removeClass("blink-animation"); }, 2500);
}

function openThankYou() {

    var form_cookie = JSON.parse(decodeURIComponent(getCookie('c_form_data')));

    appendCookie('c_form_data', 'linea', getUsuario().linea, 1);
    appendCookie('c_form_data', 'categoria', getUsuario().modalidad.grupo_carreras, 1);
    appendCookie('c_form_data', 'carrera', getUsuario().modalidad.nombre, 1);
    appendCookie('c_form_data', 'carreraInteres', getUsuario().modalidad.iddynamics, 1);
    appendCookie('c_form_data', 'subNivelInteres', getUsuario().modalidad.interes, 1);
    appendCookie('c_form_data', 'nivelInteres', getNivelInteres(getUsuario().modalidad.lineaweb), 1);
    appendCookie('c_form_data', 'modalidad', getUsuario().modalidad.modalidad, 1);
    appendCookie('c_form_data', 'hubspotutk', getCookie('hubspotutk'), 1);
    appendCookie('c_form_data', 'campus', jQuery('#render_campus').val(), 1);
    appendCookie('c_form_data', 'campusLargo', campusDynamics(jQuery('#render_campus').val()), 1);
    appendCookie('c_form_data', 'urlreferrer', window.location, 1);
    appendCookie('c_form_data', 'ciclo', "19-2", 1);
    appendCookie('c_form_data', 'cicloTexto', "En Enero", 1);

    $('#form-css').prop("disabled", true);
    $(".todoElBody").show();
    $('#bootstrapcss-css').prop("disabled", false);
    $("#formulario-main-mobile").hide();
    $(".nvct").first().remove();
    $("#UnitecSideNav").first().remove();
    $("#nocritical-css").remove();

    jQuery(".modal-header-app").hide();
    jQuery(".modal-header-formulario").hide();
    jQuery(".modal-body-form").hide();
    //Llenar los campos de la thankyou page con los valores de la cookie
    jQuery(".tercer-texto-thankyou").html(
        // formCookiesService.getCookieByKey("c_form_data", "carrera")
        getUsuario().modalidad.nombre
    );
    jQuery("#nombre-thankyou").html(
        // formCookiesService.getCookieByKey("c_form_data", "nombre")
        form_cookie.nombre
    );
    //Mostrar la thankyou page
    jQuery(".modal-header-thankyou").show();
    jQuery(".modal-body-thankyou").show();
    jQuery(".img-header-thankyou").css({
        "background-image": "url(" + jQuery("#h_horizontal_url_imagen_destacada").val() + ")"
    });

    jQuery("#modal_frm_app").modal();

    if(paso6){
        var objCookie = JSON.parse(decodeURIComponent(getCookie('c_form_data')));
        dataLayer.push({
            'event' : 'InformationForm',   //Static data
            'formStep' : 'Paso 6',   //Dynamic data
            'Estado' : objCookie.estado.toLowerCase(),   //CD4
            'InterLvl' : getUsuario().linea.toLowerCase(),  //CD 5
            'Period' : 'enero',  //CD 6
            'carrera' : getUsuario().carrera.Link.toLowerCase(),  //CD 7
            'campus' : campusNombreLargo(getUsuario().campus).toLowerCase(),  //CD 8
            'Formulario' : 'Registro Formulario',  //CD 16
            'RMode': modalidadtxt(getUsuario().modalidad.modalidad).toLowerCase(),   //CD25
            'prom': '',   //CD26
            'formDev' : 'Mobile'   // Dynamic data
        });
    }

    paso6 = false; //Desactivación para otras consultas posteriores
    
    if(paso7){
        var objCookie = JSON.parse(decodeURIComponent(getCookie('c_form_data')));
        dataLayer.push({
            'event' : 'InformationForm',   //Static data
            'formStep' : 'Paso 7',   //Dynamic data
            'Estado' : objCookie.estado.toLowerCase(),   //CD4
            'InterLvl' : getUsuario().linea.toLowerCase(),  //CD 5
            'Period' : 'enero',  //CD 6
            'carrera' : getUsuario().carrera.Link.toLowerCase(),  //CD 7
            'campus' : campusNombreLargo(getUsuario().campus).toLowerCase(),  //CD 8
            'Formulario' : 'Registro Formulario',  //CD 16
            'RMode': modalidadtxt(getUsuario().modalidad.modalidad).toLowerCase(),   //CD25
            'prom': '',   //CD26
            'formDev' : 'Mobile'   // Dynamic data
        });
    }

    paso7 = false; //Desactivación para otras consultas posteriores

    /*Envio de Datos a Ciclo Activo*/
    sendFormularioTradicional();
    /*End Envio de Datos a Ciclo Activo*/
}