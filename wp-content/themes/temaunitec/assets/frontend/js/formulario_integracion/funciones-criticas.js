
/**
 *                                                  SECCIONES EN CALCULADORA VIEJA 
 *  
 * data-anchor="sec-formulario" //home principal
 * data-anchor="intro" //
 * data-anchor="frm_mobile"
 * data-anchor="sec-cursar"         2.- URL: calcula-tu-beca/#calculadora/sec-cursar        DESC: Selecciona Linea de Negocio, carrera y modalidad
 * data-anchor="sec-categoria"      
 * data-anchor="sec-modalidades"
 * data-anchor="sec-preguntas"
 * data-anchor="sec-campus"          3.- calculadora/sec-campus                              DESC: Selecciona Campus
 * data-anchor="sec-promedio"        4.- calculadora/sec-promedio                            DESC: Selecciona Promedio
 * data-anchor="resultados"          5.- calculadora/resultados                              DESC: Muestra resultados y envia Info
 * 
 * secciones de home 
 * data-anchor="detalle"
 * data-anchor="contenido"
 * 
 */
// jQuery("#formulario-css").prop("disabled", true);
// jQuery(document).ready(function () {
    // sendDatalayer('calculadora/paso0', 'StepCalculator');



    // console.log('after render');
    // Al cargar la pagina revisa si ya tiene el formulario completo si ya lo tiene borra la app y manda a sec-cursar

    // if (jQuery("#h_id_producto").val() != "" && jQuery("#h_id_producto").val() != undefined) {
    //     autocompleteMobile = true;
    //     var nodo_encontrado = buscar("Grupo_carreras", jQuery("#h_id_producto").val(), JSON.parse(localStorage.getItem("jsonCarreras")));
    //     nodo_autocomplete = nodo_encontrado;
    //     nodo_encontrado = nodo_encontrado[0];
    //     if (nodo_encontrado.lineaweb != undefined) {
    //         setTimeout(function () {
    //             console.log("DANDO CLICK EN: " + nodo_encontrado.lineaweb);
    //             console.log(jQuery("div").find("[data-val='" + nodo_encontrado.lineaweb + "']"));
    //             jQuery("div").find("[data-val='" + nodo_encontrado.lineaweb + "']").click();
    //         }, 400);
    //     }
    // }
    // if (jQuery("#h_id_producto").val() != "" && jQuery("#h_id_producto").val() != undefined) {
    //     var nodo_encontrado = buscar("Grupo_carreras", jQuery("#h_id_producto").val(), JSON.parse(localStorage.getItem("jsonCarreras")));
    //     nodo_encontrado = nodo_encontrado[0];
    //     if (nodo_encontrado.lineaweb != undefined) {
    //         setTimeout(function () {
    //             console.log("DANDO CLICK EN: " + nodo_encontrado.lineaweb);
    //             console.log(jQuery("div").find("[data-val='" + nodo_encontrado.lineaweb + "']"));
    //             jQuery("div").find("[data-val='" + nodo_encontrado.lineaweb + "']").click();
    //             var busqueda_grupo = buscar_grupo("IdDynamics", nodo_encontrado.IdDynamics, JSON.parse(localStorage.jsonCarreras));
    //             console.log("LA BUSQUEDA DEL GRUPO");
    //             console.log(busqueda_grupo);
    //             var categoria_grupo = busqueda_grupo[0].Categoria;
    //             // console.log("LA BUSQUEDA DEL OPTION");
    //             // console.log(categoria_grupo);
    //             // jQuery("#project option:contains('"+categoria_grupo+"')").click();
    //             /* Agregar un timer para la seleccion de carrera */
    //             setTimeout(function () {
    //                 var carrera_selected = jQuery("#project option").filter(function () {
    //                     return jQuery(this).text() == categoria_grupo;
    //                 }).prop('selected', true).change();

    //                 var modalidad_size = jQuery("#render_modalidad").children().length;
    //                 if (modalidad_size == 1) {
    //                     // var txt_modalidad = jQuery("#render_modalidad option[value=" + verif_cookie.modalidad + "]").prop("selected", true).change();
    //                 }
    //                 var campus_size = jQuery("#render_campus").children().length;
    //                 if (campus_size == 2) {
    //                     // var txt_campus = jQuery("#render_campus option[value=" + verif_cookie.campus + "]").prop("selected", true).change();
    //                     jQuery("#render_campus option").first().next().prop("selected", true).change();
    //                 }
    //                 // jQuery(".right.button-primaryn")[0].click();
    //             }, 400);
    //         }, 1000);

    //     }


    //     //.prop('selected', true).change();

    // }
// });


window.onload = function () {
    /*Cookie UTM_CAMPAINGN BY SRP 29-10-2018*/
    var cookieBanner = getParameterByName('utm_campaign');
    /*Validar si existe utm_campaign*/
    if( getParameterByName('utm_campaign') !== false ) {

        if( checkCookie('banner_activo') === true ){
            //lo asigo a la cookie form_data
            appendCookie('c_form_data', 'banner', getCookie('banner_activo'), 1);
        } else{
            //no existe se da de alta en banner activo
            setCookieForm("banner_activo", cookieBanner, 1);
        }
    }
    /*Si no existe*/
    else {
        if( checkCookie('banner_activo') === true ){
            //lo asigo a la cookie form_data
            appendCookie('c_form_data', 'banner', getCookie('banner_activo'), 1);
        } else{
            if (location.href.indexOf("testvocacional") !== -1 || location.href.indexOf("orientacion-profesional") !== -1) {
                cookieBanner = "TESTVOCACIONAL";
            } else if (location.href.indexOf("impulsa") !== -1) {
                cookieBanner = "IMPULSA";
            } else {
                cookieBanner = "ASPIRANTES LIC";
            }
            appendCookie('c_form_data', 'banner', cookieBanner, 1);
        }
    }
    /*End Cookie UTM_CAMPAINGN BY SRP 29-10-2018*/
};



//https://www.unitec.mx/wp-content/themes/temaunitec/calculadora/json/min/json_calc_becas.min.json

var urlJsonBasura = "/assets/frontend/json/min/json_basura.min.json";
var urlJsonBasuraEmail = "/assets/frontend/json/min/json_basuraEmail.min.json";
var urlJsonBasuraTelefonos = "/assets/frontend/json/min/json_basuraTelefonos.min.json";
var urlJsonCarreras = "/assets/frontend/json/min/json_calc_carreras.min.json";
//Se agrega nueva url para obtener los links y nombre de las categorias de las carreras
var urlJsonLinksCategorias = "/assets/frontend/json/min/json_calc_linkPaginas.min.json";
//Se agregan variables para calculadora
//Costos
var urlJsonCalcCostos = "/assets/frontend/json/min/json_calc_costos.min.json";
//Becas
var urlJsonCalcBecas = "/assets/frontend/json/min/json_calc_becas.min.json";

function setCookieForm(cname, cvalue, exdays) {
    var d = new Date();
    d.setTime(d.getTime() + (exdays * 24 * 60 * 60 * 1000));
    var expires = "expires=" + d.toUTCString();
    document.cookie = cname + "=" + cvalue + "; " + expires + ";path=/";
}

function setCookieSession(cname, cvalue) {
    document.cookie = cname + "=" + cvalue + ";path=/";
}
function isInArray(value, array) {
    return array.indexOf(value) > -1;
}
function getCookie(cname) {
    var name = cname + "=";
    var ca = document.cookie.split(';');
    for (var i = 0; i < ca.length; i++) {
        var c = ca[i];
        while (c.charAt(0) == ' ') c = c.substring(1);
        if (c.indexOf(name) == 0) return c.substring(name.length, c.length);
    }
    return "";
}

function checkCookie(valor) {
    var user = getCookie(valor);
    if (user != "") {
        return true;
    }
    else {
        return false;
    }
}


function getCampo(campo) {
    var usuario = getUsuario();
    return usuario[campo];
}

function frm_SinEspacios(e) {
    tecla = (document.all) ? e.keyCode : e.which;
    if (tecla == 32) {
        return false;
    }
    if (tecla != 32) {
        return true;
    }
}

function sendDatalayer(url_val, event) {
    if (checkCookie('frmunitec') != true) {
        dataLayer.push({
            'url_value': url_val, //dato dinámico
            'event': event//dato estático
        });
    }
}