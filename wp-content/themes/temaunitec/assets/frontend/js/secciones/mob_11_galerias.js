$(document).ready(function(){
    $galeriaActiva = $("#galeria-activa").val();
    $galeriaName = $("#galeria-name").val();

    /*Update 1.1 destaca el titulo al seleccionar una Galeria*/
    $(".title-galerias").removeAttr('style');
    $(".galeria-title-" + $galeriaActiva).css({'font-weight': '500'});
    /*Update 1.1 destaca el titulo al seleccionar una Galeria*/ 
    muestra_galeria(galery_json, $galeriaActiva, $galeriaName, youtubeLazy);
});

$(".portada-galeria").on("click", function(){
    /*Update 1.1 destaca el titulo al seleccionar una Galeria*/
    var id_galeria = $(this).attr('id');
    $(".title-galerias").removeAttr('style');
    $(".galeria-title-" + id_galeria).css({'font-weight': '500'});
    /*Update 1.1 destaca el titulo al seleccionar una Galeria*/
    muestra_galeria(galery_json, $(this).attr("name"), $(this).attr("title"), youtubeLazy);
});

$("#modal-tarjeta-galeria-carrusel").on('hidden.bs.modal', function () {

    if ( $("#h_tab-contiene-video").val() == "S" ) {
        switch( $("#h_video-activo").val() ) {
            case 'primero':
                try{
                    $('.iframe-galeria')[0].contentWindow.postMessage( '{"event":"command","func":"' + "stopVideo" + '","args":""}', "*");
                } catch (e) {
                    console.log(e);
                }                
            break;

            default:
                $( ".youtube" ).remove( $("h_video-activo").val() );
            break;
        }

        $("#h_tab-contiene-video").val("N");
        $("#h_video-activo").val("");
    }
    
});

function muestra_galeria(galery_json, galeria, galeriaName, mycallback, mycallback2) {
    var video = null;
    var $grid = $('#load_galery').masonry({
                    itemSelector: '.grid-item .grid-item-video',
                    columnWidth: 80
                });

    $("#galeria-activa").val(galeria);
    $("#galeria-name").val(galeriaName);
    $("#load_galery").html('');

    $("#load_galery").prepend('<input type="hidden" name="ModalName" value="modal-tarjeta-galeria-carrusel">');
                
    galery_json = JSON.parse(galery_json);

    galery_json[galeria].forEach(function(imagen, index) {
        /*console.log(imagen.url);*/
        video = imagen.description;
        video.toLowerCase();

        /* # de img Preview a Mostrar BY SRP*/
        if (index <= 6) {
            if (  video.indexOf("youtube") !== -1 ) {
                /*$('<div />', {
                    'id': 'wrapper-video-' + index,
                    'class': 'wrapper grid-item-video'
                }).appendTo('#load_galery');

                $('<div />', {
                    'id': 'preview-video-'+index,
                    'class': "youtube",
                    'data-embed': video
                }).appendTo('#wrapper-video-'+index);

                $('<div class="play-button"></div>').appendTo('#preview-video-'+index);*/

            $grid.append( '<div id="preview-video-'+ index +'" class="wrapper grid-item-video youtube" data-embed="' + video + '"><div class="play-button"></div></div>' )
            // add and lay out newly appended items
            .masonry( 'appended', '<div id="preview-video-'+ index +'" class="wrapper grid-item-video youtube" data-embed="' + video + '"><div class="play-button"></div></div>' );

            } else if(video.indexOf("videos") !== -1){

                /*$('<div />', {
                    'id': 'video-' + index,
                    'class': 'wrapper grid-item-video'
                }).appendTo('#load_galery');

                $('<video />', {
                    'id': 'video-preview-'+index,
                    'controls': 'yes'
                }).appendTo('#video-'+index);

                $('<source />', {
                    'id': 'video-source-'+index,
                    'src': video,
                    'type': 'video/webm',
                }).appendTo('#video-preview-'+index);*/

                $grid.append( '<div id="video-' + index + '" class="wrapper grid-item-video"><video id="video-preview-' + index + '" controls><source id="video-source-' + index + '" src="' + video + '" type="video/webm"></source></video></div>' )
                // add and lay out newly appended items
                .masonry( 'appended', '<div id="video-' + index + '" class="wrapper grid-item-video"><video id="video-preview-' + index + '" controls><source id="video-source-' + index + '" src="' + video + '" type="video/webm"></source></video></div>' );              

            }  else{
                /*Cambiar el index para manipular desde donde inica la Galeria BY SRP*/
                if (index > 1) {

                   /*$('<div />', {
                        'id': 'preview-'+index,
                        'class':'grid-item tab-tarjeta galeria-child animated flipInX preview-'+index
                    }).appendTo('#load_galery');

                    $('<img />', {
                        'src': imagen.url, 
                        'alt':imagen.alt,
                        'class': 'p-1'
                    }).appendTo('#preview-'+index);*/

                    /*Cambiarlo por la Imagen que se desea ver en Grande BY SRP*/
                    if(index == 4){
                        $grid.append( '<div id="preview-' + index + '" class="grid-item galeria-child animated flipInX preview-' + index + '" style="width:100% !important;"><img src="' + imagen.urlthumbnail + '" alt="' + imagen.alt + '" class="p-1"></div>' )
                        // add and lay out newly appended items
                        .masonry( 'appended', '<div id="preview-' + index + '" class="grid-item galeria-child animated flipInX preview-' + index + '" style="width:100% !important;"><img src="' + imagen.urlthumbnail + '" alt="' + imagen.alt + '" class="p-1"></div>' );
                    }  else{
                        $grid.append( '<div id="preview-' + index + '" class="grid-item galeria-child animated flipInX preview-' + index + '"><img src="' + imagen.urlthumbnail + '" alt="' + imagen.alt + '" class="p-1"></div>' )
                    // add and lay out newly appended items
                    .masonry( 'appended', '<div id="preview-' + index + '" class="grid-item galeria-child animated flipInX preview-' + index + '"><img src="' + imagen.urlthumbnail + '" alt="' + imagen.alt + '" class="p-1"></div>' );
                    }         

                    

                }
            }
            
        }
    });

    $( ".grid-item" ).bind( "click", function() { /*openModal("#modal-tarjeta-galeria-carrusel");*/ ver_galeria(galery_json, $(this).attr('id'), insertar_imagenes); openModal("#modal-tarjeta-galeria-carrusel"); }); 

    if (mycallback && typeof(mycallback) === "function") {
        mycallback();
    }

    /*if (mycallback2 && typeof(mycallback2) === "function") {
        mycallback2();
    }*/
}

function ver_galeria(galery_json, imgSeleccionada, callbakImg){  
    $imgSeleccionada = imgSeleccionada;
    $galeriaActiva = $("#galeria-activa").val();
    $galeriaName = $("#galeria-name").val();
    $('#gallery-title').html( $galeriaName  );
    $("#slides-carusel").html('');
    $("#indicadores-carusel").html('');

    galery_json[$galeriaActiva].forEach(function(imagen, index) {
        /*Cambiar el index para manipular desde donde inica la Galeria BY SRP*/
        if (index > 1) {
            $('<div />', { 
                'id': 'slide-'+index, 
                'class': 'carousel-item '+ $galeriaActiva +' preview-'+index
            }).appendTo('#slides-carusel');
        }

        if (index === 1) {$("#indicadores-carusel").css('display','none');} else{$("#indicadores-carusel").removeAttr('style');}

        /*Cambiar el index para manipular desde donde inica la Galeria BY SRP*/
        if (index > 1) {
            $("#indicadores-carusel").append('<li data-target="#carousel-tarjeta" id="indicador-'+ index + '" data-slide-to="'+ index +'" class="preview-'+ index + '"></li>');
        }
    });

    if (callbakImg && typeof(callbakImg) === "function") {
        callbakImg(galery_json, $imgSeleccionada);
    }
    
    $("#indicadores-carusel > li."+$imgSeleccionada).addClass("active");
}

function insertar_imagenes(galery_json, imgSeleccionada){
    console.log(imgSeleccionada);
    $imgSeleccionada = imgSeleccionada;
    $galeriaActiva = $("#galeria-activa").val();

    galery_json[$galeriaActiva].forEach(function(imagen, index) {
        video = imagen.description;
        video.toLowerCase();

        if (index == 0) {
            $("#portada-galeria").attr('src', imagen.urlthumbnail);
        }

        if (  video.indexOf("youtube") !== -1 ) {            

            $('#slide-'+index).prepend('<div class="col-1 div-yt-video-left"><i class="icon-u-ver-izquierda"></i></div>');

            $('<div />', {
                'id': 'div-video-'+index,
                'class':'col-10 div-yt-video'
            }).appendTo('#slide-'+index);

            /*Se comenta debido a que en la galeria el video de tou tube no habre Modal por lo tanto no necesita Autoplay hasta que se realice la petición*/

            switch (index) {
                case 1:
                    $('<iframe />', {
                    'id': 'video-youtube-'+index,
                    'class': 'iframe-galeria',
                    'controls': 'controls',
                    'x-webkit-airplay': 'allow',
                    'allowscriptaccess': 'always',
                    'scrolling': 'no',
                    'src': video + "?enablejsapi=1&rel=0&showinfo=0&autoplay=1",
                    'frameborder': '0',
                    'allowfullscreen': 'yes'
                }).appendTo('#div-video-'+index);

                $("#h_video-activo").val('primero');
                setTimeout(function(){ $('#video-youtube-'+index)[0].contentWindow.postMessage( '{"event":"command","func":"' + "stopVideo" + '","args":""}', "*"); }, 2000);
                break;

                default:
                    $('<div id="video-youtube-' + index + '" class="wrapper youtube" data-embed="' + video + '" title="iframe-galeria"><div class="play-button"></div></div>').appendTo('#div-video-'+index);
                break;
            }

            /*Se comenta debido a que en la galeria el video de tou tube no habre Modal por lo tanto no necesita Autoplay hasta que se realice la petición*/

            /*$('<div id="video-youtube-' + index + '" class="wrapper youtube" data-embed="' + video + '" title="iframe-galeria"><div class="play-button"></div></div>').appendTo('#div-video-'+index);*/
            
            $('#slide-'+index).append('<div class="col-1 div-yt-video-right"><i class="icon-u-ver-derecha"></i></div>');

            /*Bandera que define si el testimonio Contiene Video*/
            $("#h_video-activo").val('primero');
            $("#h_tab-contiene-video").val("S");
            /*Bandera que define si el testimonio Contiene Video*/

        } else if(video.indexOf("videos") !== -1){
            /*<i class="icon-u-ver-izquierda"></i>*/
            $('#slide-'+index).prepend('<div class="col-1 div-yt-video-left"><i class="icon-u-ver-izquierda"></i></div>');

            $('<div />', {
                'id': 'div-video-ext'+index,
                'class':'col-10 div-yt-video'
            }).appendTo('#slide-'+index);

            $('<video />', {
                'id': 'video-full-'+index,
                'controls': 'yes'
            }).appendTo('#div-video-ext'+index);

            $('<source />', {
                'id': 'video-source-full'+index,
                'src': video,
                /*'type': 'video/mp4',*/
                'type': 'video/webm',
            }).appendTo('#video-full-'+index);

            $('#slide-'+index).append('<div class="col-1 div-yt-video-right"><i class="icon-u-ver-derecha"></i></div>');
            /*<i class="icon-u-ver-derecha"></i>*/

        } else{
            /*Cambiar el index para manipular desde donde inica la Galeria BY SRP*/
            if (index > 1) {

                $('<img />', { 
                        'src': imagen.url, 
                        'alt':imagen.alt, 
                        'class':'grid-item galeria-child animated flipInX' 
                }).appendTo('#slide-'+index);

                $('<div />',{
                    'id': 'footer-content-'+index,
                    'class': 'text-center mx-auto',
                    'style': 'font-weight:200;'
                }).appendTo('#slide-'+index);

                $('#footer-content-'+index).append('<p>'+imagen.caption+'</p>');

                /*$('<img />', { 
                        'src': imagen.urlthumbnail, 
                        'alt':imagen.alt, 
                        'class':'img-fluid' 
                }).appendTo('#indicador-'+index);*/
            }
        }

        
    });
    youtubeLazy();
    $('div.'+$imgSeleccionada).addClass("active");
}