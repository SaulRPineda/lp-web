$(document).ready(function(){
	var carrera = $("#ciclos").text();
	var noAnos = $("#h_duracion").val();
	var tipoPlan = $("#h_plan").val();
	var nombreDePlan = $("#nombrePlan").text();
	//console.log( "Ciclos: " + noAnos + " Duración: " + carrera );
	filtroTipoPlan(tipoPlan, nombreDePlan);
	filtroNoAnos(noAnos, carrera, tipoPlan);

	setTimeout(function(){
		$(".nav a:contains('Plan de estudios')")[0].click();
		$('.rightTabNav.icon-u-ver-derecha').click();
	},200)
	
});

/* Funcionalidad en iOS */
$(".prodColapsables").click(function(){
	$(".prodColapsables").unbind();
	$("#prodTipoPlan").click();
});

function filtroNoAnos(tipo, noAnos, tipoPlan) {
	var actual = "duracion-" + $("#actual").val();
	var duracion = "duracion-"+ tipo;
	var plan = "plan-" + tipoPlan;

	var ciclo = $("#ciclo").val();
	var nombreCiclo = ciclo.substring( 3, parseInt(ciclo.length) ) + 's';	
	var total_cuatrimestres = $("div." + duracion + "." + plan).length;

	var duracionTitle = $("#h_duracionTitle").val() + noAnos;
	var duracionTotal = $("#h_duracionOferta").val();

	if (duracionTotal == "" && duracion != null) {
		duracionTotal = total_cuatrimestres +' '+ nombreCiclo;
	}

	switch (duracion) {
		case 'duracion-' + tipo:
			//$("#ciclos").html( "Duración: " + noAnos );
			$("#ciclos").html( duracionTitle );
			$("#duracionPor").html(' ('+ duracionTotal +')');
			//$("."+ actual).addClass("d-none").removeClass("active");
			$(".mapa-curricular").addClass("d-none").removeClass("active");
			// $(".btn-materia:first-child").css('margin-left','5px');
			//$("div." + actual).removeClass("carousel-item");
			$(".mapa-curricular").removeClass("carousel-item");
			$("."+ duracion + "." + plan).removeClass("d-none");
			$("."+ duracion + "." + plan + ":first").addClass("active");
			/*$("ol > li."+ duracion + ":first").addClass("active");*/
			$("div."+ duracion + "." + plan).addClass("carousel-item");			
			$("#actual").val( tipo );			
		break;
	}	
}

function filtroMaterias(tipo, materia) {

	tipoFiltro = tipo.split("_");

	$("#filtrarPor").html( materia );
	$(".btn-materia").addClass("d-none").removeClass('visible');
	$("."+ tipoFiltro[1]).removeClass("d-none").addClass('visible');
	
	if( materia === "Todo" ) {
		$("#filtrarPor").html( materia );
		$(".btn-materia").removeClass("d-none").removeClass('visible');
	}
}

function filtroTipoPlan(keyPlan, nombrePlan) {
	var plan = "plan-" + keyPlan;

	var copyPresencial = "(Ideal para vivir la experiencia en campus)";
	var copyEjecutivo = "(Para mayores de 24 años que desean combinar trabajo y estudio)";
	var copyLinea = "(Pensada en personas autodidactas que desean superar distancias)";
	var copyPresencialLinea = "(Planeados para ayudarte a crecer profesionalmente)";
	var copyEjecutivoLinea = "Ejecutiva: Para mayores de 24 años que desean trabajar y estudiar. <br> L&iacute;nea: Pensada en personas autodidactas que desean superar distancias.";
	var copyPresencialVespertina = "(Para revalidar estudios previos y acudir al campus a partir de las 16:00 horas.)";

	$("#descModalidad").html( "" );
	switch (keyPlan) {
		case "plan_presencial":
			$("#descModalidad").html( copyPresencial );
			$("#descarga_folleto_mapa").attr( "href", descarga.plan_presencial );
		break;

		case "plan_ejecutiva":
			$("#descModalidad").html( copyEjecutivo );
			$("#descarga_folleto_mapa").attr( "href", descarga.plan_ejecutiva );
		break;
		
		case "plan_enlinea":
			$("#descModalidad").html( copyLinea );
			$("#descarga_folleto_mapa").attr( "href", descarga.plan_enlinea );
		break;

		case "plan_esbelta":
			$("#descModalidad").html(copyEjecutivoLinea);
			$("#descarga_folleto_mapa").attr("href", descarga.plan_ejecutiva);
		break;

		case "plan_presencial_enlinea":
			$("#descModalidad").html( copyPresencialLinea );
			$("#descarga_folleto_mapa").attr( "href", descarga.plan_presencial_enlinea );
		break;

		case "plan_presencial_vespertina":
			$("#descModalidad").html(copyPresencialVespertina);
			$("#descarga_folleto_mapa").attr("href", descarga.plan_presencial_vespertina);
		break;

		default:
			$("#descModalidad").html( copyPresencial );
			$("#descarga_folleto_mapa").attr( "href", descarga.plan_presencial );
		break;
	}


	$("#prodCampusDropdown .dropdown-item.destacar").addClass("d-none");
	$("#prodCampusDropdown .dropdown-item.destacar.plan-"+keyPlan).removeClass("d-none");

	$("#prodCampusDropdown .dropdown-item.destacar.plan-"+keyPlan+":first").click();
	$("#nombrePlan").text("Modalidad: " + nombrePlan);
}