/*para pintar metricas en analitics*/
$(document).ready(function () {
    // contador de intenciones para CHAT by AMC
    $("#openChat").click(function () {     
        dataLayer.push({ 
            'event': 'Activador Dinamico',
            'category':'Chat',
            'accion':'intencion',
            'label': 'con formulario',
            'location': 'Widget'
        });
    });

    // $(".bot-contacto").click(function () {
    //     var pos = $(this).data('gtm-pos')
    //     dataLayer.push({
    //         'event': 'BOTONERA',
    //         'category': 'Widget de contacto',
    //         'action': 'click',
    //         'label': pos
    //     });
    // });

    $('#botonera-gral').on('mouseenter', function () { return false; });
    $('.tooltiptext-ctc').addClass('d-none');

});

// $zopim(function () {
//     setInterval('$zopim.livechat.setOnStatus(contador)', 10000);
//     $zopim.livechat.button.hide();
//     $zopim.livechat.window.hide();
//     $zopim.livechat.window.onHide(function () {
//         // $zopim.livechat.button.show();
//     });
//     var numberGlobal = 0;
//     $zopim.livechat.setOnUnreadMsgs(function (number) {
//         if (number > 0) {
//             // alert(number);
//             // document.getElementById("chatMsj").innerHTML = "<div class='notificacion'>" + number + "</div>";
//             document.getElementById("chatMsj-int").innerHTML = "<div class='notificacion-int'>" + number + "</div>";
//             $("#chatMsj-int").addClass("chatMsj-int");
//             document.getElementById("chatMsj-int-botonera").innerHTML = "<div class='notificacion-int'>" + number + "</div>";
//             $("#chatMsj-int-botonera").addClass("chatMsj-int-botonera");
//         } else {
//             // alert("Sin mensajes");
//             document.getElementById("chatMsj-int").innerHTML = "";
//             $("#chatMsj-int").removeClass("chatMsj-int");
//             document.getElementById("chatMsj-int-botonera").innerHTML = "";
//             $("#chatMsj-int-botonera").removeClass("chatMsj-int-botonera");
//         }
//         // if (number > 0) {
//         //     // alert(number);
//         //     numberGlobal = number;
//         //     if ($("#chatMsj div").length > 0) {
//         //         var notifActualChat = parseInt($("#chatMsj div").text());
//         //         number += notifActualChat;
//         //         document.getElementById("chatMsj").innerHTML = "<div class='notificacion'>" + number + "</div>";
//         //         document.getElementById("chatMsj-int").innerHTML = "<div class='notificacion-int'>" + number + "</div>";
//         //         $("#chatMsj-int").addClass("chatMsj-int");
//         //     } else {
//         //         document.getElementById("chatMsj").innerHTML = "<div class='notificacion'>" + number + "</div>";
//         //         document.getElementById("chatMsj-int").innerHTML = "<div class='notificacion-int'>" + number + "</div>";
//         //         $("#chatMsj-int").addClass("chatMsj-int");
//         //     }
//         // } else {
//         //     // alert("Sin mensajes");
//         //     var notifActualChat;
//         //     ($("#chatMsj div").length > 0) ? notifActualChat = parseInt($("#chatMsj div").text()) - numberGlobal : notifActualChat = "";

//         //     if (notifActualChat == "" || notifActualChat == 0) {
//         //         document.getElementById("chatMsj").innerHTML = ""
//         //     } else {
//         //         document.getElementById("chatMsj").innerHTML = "<div class='notificacion'>" + notifActualChat + "</div>";
//         //     }
//         //     document.getElementById("chatMsj-int").innerHTML = "";
//         //     $("#chatMsj-int").removeClass("chatMsj-int");
//         // }
//     });
// });

// /*Actualización de Funcion Contador para disparar evento cuando no se solicite correo en el chat By Ramiro and SRP 22-11-2018*/
// function contador(status) {
//     if (status == 'online') { // online
//         if (typeof ($zopim.livechat.isChatting()) != "undefined") {
//             if ($zopim.livechat.isChatting() == true) {
//                 if (getCookie("zop_google") != 1) {
//                     if ($zopim.livechat.getEmail() != "") {
//                         sendHB(status);
//                         setCookie("zop_google", 1);
//                         dataLayer.push({ 'event': 'CHAT_SERVIDO_DC' });
//                         dataLayer.push({
//                             'originFormChat': 'footer', //dato din·mico 
//                             'department': 'no obtenido', //dato din·mico
//                             'status': 'Chat Online', //dato din·mico
//                             'event': 'leadGenerationChat'//dato est·tico
//                         });
//                     } // Cierra valida email
//                     // else{ // Conversaciones abordadas/ trigger
//                     //   setCookie("zop_google",1);
//                     //   dataLayer.push({'event': 'CHAT_SERVIDO_DC'});            
//                     //   dataLayer.push({
//                     //     'originFormChat': 'footer', //dato din·mico 
//                     //     'department': 'no obtenido', //dato din·mico
//                     //     'status': 'Chat Online s/correo', //dato din·mico
//                     //     'event': 'leadGenerationChat'//dato est·tico
//                     //   });
//                     // }
//                 } // Cierra cookie
//             } // Cierra isChatting
//         }
//     }
//     else { // offline
//         if (getCookie("zop_google") != 1) {
//             if ($zopim.livechat.getEmail() != "") {
//                 sendHB(status);
//                 console.log('Chat offline');
//                 setCookie("zop_google", 1);
//                 dataLayer.push({ 'event': 'CHAT_SERVIDO_DC' });
//                 dataLayer.push({
//                     'originFormChat': 'footer', //dato din·mico 
//                     'department': 'no obtenido', //dato din·mico
//                     'status': 'Chat Offline', //dato din·mico
//                     'event': 'leadGenerationChat'//dato est·tico
//                 });
//             } // Cierra valida email
//         } // Cierra cookie
//     }
// } // Cierra contador()
// /*End Actualización de Funcion Contador para disparar evento cuando no se solicite correo en el chat By Ramiro and SRP 22-11-2018*/

// function sendHB(flag) {
//     var name = $zopim.livechat.getName();
//     var email = $zopim.livechat.getEmail();
//     var phone = $zopim.livechat.getPhone();
//     jQuery.ajax({
//         type: "POST",
//         //
//         url: "https://www.unitec.mx/assets/zopim/service_chat_qa.php",
//         data: { email: email, name: name, phone: phone, flag: flag }
//     });
// }

// function setCookie(cname, cvalue, exdays) {
//     var d = new Date();
//     d.setTime(d.getTime() + (exdays * 24 * 60 * 60 * 1000));
//     var expires = "expires=" + d.toUTCString();
//     document.cookie = cname + "=" + cvalue + "; " + expires;
// }

// function getCookie(cname) {
//     var name = cname + "=";
//     var ca = document.cookie.split(';');
//     for (var i = 0; i < ca.length; i++) {
//         var c = ca[i];
//         while (c.charAt(0) == ' ') c = c.substring(1);
//         if (c.indexOf(name) == 0) return c.substring(name.length, c.length);
//     }
//     return "";
// }

// function checkCookie(valor) {
//     var user = getCookie(valor);
//     if (user != "") {
//         return true;
//     }
//     else {
//         return false;
//     }
// }

// function openChatLive() {
//     $zopim.livechat.window.show();
// }

function clickTel(activarTel) {
    window.location.assign('tel:8007864832');
    // location.href = activarTel.href;
    return false;
}
function clickCalcu(activarCalcu) {
    location.href = '/calcula-tu-beca';
    return false;
}