$(document).ready(function () {
    // Aquí comienzan las funciones de click to call
    $('.modal-ctc').addClass('fade');
    $("#modal-ctc").on("shown.bs.modal", function (e) {
        if ($('#modal-ctc').hasClass("show") == true) {
            $('.modal-ctc').removeClass('fade');
        }
    });
    $("#modal-ctc").on("hide.bs.modal", function (e) {
        $('.modal-ctc').addClass('fade')
    });

    $('#ctc-phone').keypress(validateNumber);
    $('#msg-success').hide();
    $('#msg-alert').hide();
    $('#proceso-ctc').hide();
    //$('#llamada-ctc').attr('onclick', 'SolicitudCTC()');
    $('#llamada-ctc').click(function(){          
        var seleccionValor;
      var ingresaTelefono;
      var error = 0;

      if ($('#customRadio1').is(":checked")) {
          seleccionValor = $('#customRadio1').val();
      } else if ($('#customRadio2').is(":checked")) {
          seleccionValor = $('#customRadio2').val();
      } else {
          seleccionValor = false;
          error++;
      }

      if ($("#ctc-phone").val() != "" && $("#ctc-phone").val() != undefined) {
          ingresaTelefono = $("#ctc-phone").val();
      } else {
          ingresaTelefono = false;
          error++;
      }

      if (seleccionValor == false) {
          $("#err-valor").addClass("block");
      } else {
          $("#err-valor").removeClass("block");
      }
      if (ingresaTelefono == false) {
          $("#err-tel").addClass("block");
      } else {
          $("#err-tel").removeClass("block");
      }

      if (error == 0) {                
        var phonen = String(seleccionValor) + String(ingresaTelefono);
          //envioDatosCTC(datosCompletos);
        dataLayer.push({ 'event': 'ENVIO_FRM_CTC' });
        jQuery
        .ajax({
            type: "POST",
            url: "/wp-content/themes/temaunitec/clickToCall.php",
            data: { "theData": { "phonenumber": phonen } },
            dataType: 'json',
            success: function (res) {
                if (res.success) {
                    //mensaje satisfactorio
                    var html = "";

                    html += "<div class='d-flex justify-content-end'>";
                    html += "<button type='button' class='p-0' data-dismiss='modal' aria-label='Close' style='background:0 0; border:0'>";
                    html += "<div class='icons-desktop d-flex align-items-center justify-content-center'>";
                    html += "<a href=''>";
                    html += "<i class='icon-u icon-u-cerrar'></i>";
                    html += "</a>";
                    html += "</div>";
                    html += "</button>";
                    html += "</div>";
                    html += "<div class='col-lg-12 p-3 pt-2 d-flex flex-column align-items-center'>";
                    html += "<h3 class='m-0 titleSection text-tnks text-center'>¡Gracias por dejarnos tus datos!</h3>";
                    html += "<h5 class='m-0 descriptionSection text-tnks mt-2 mb-2'>Uno de nuestros asesores educativos te está marcando.</h5>";
                    html += "</div>";

                    $('#msg-success').empty();
                    $('#msg-success').append(html);
                    $('#proceso-ctc').hide();
                    $('#blur-ctc').removeClass('blur-ctc');

                    $('#frm-ctc').hide(function () {
                        $('#msg-success').show()
                    });
                    $("#ctc-phone").val('');
                    $("#formulario-ctc input[type='radio']").prop("checked", false);

                } else {
                    //mensaje intentar de nuevo
                    //$('#llamada-ctc').attr('onclick', 'SolicitudCTC()');
                    $('#msg-alert').show();
                    $('#proceso-ctc').hide();
                    $('#blur-ctc').removeClass('blur-ctc');
                }
                console.log("Datos Enviados");
                console.log(res);
            },
            error: function (xhr, textStatus, error) {
                console.log(xhr);
                console.log(xhr.statusText);
                console.log(textStatus);
                console.log(error);
                //mensaje intentar de nuevo
                //$('#llamada-ctc').attr('onclick', 'SolicitudCTC()');
                $('#msg-alert').show();
                $('#proceso-ctc').hide();
                $('#blur-ctc').removeClass('blur-ctc');
            },
            beforeSend: function () {
                var html = "";
                html += "<div class='col-lg-12 p-3 pt-2 d-flex flex-column align-items-center justify-content-center overlay-ctc'>";
                html += "<h3 class='m-0 titleSection text-tnks'>Tu llamada se encuentra en proceso.</h3>";
                html += "<h5 class='m-0 descriptionSection text-tnks mt-2 mb-2'><img src='assets/cargando.gif' class='img-fluid m-auto' style='width:100%; max-width:60px;'></h5>";
                html += "</div>";
                $('#blur-ctc').addClass('blur-ctc');
                $('#proceso-ctc').empty();
                $('#proceso-ctc').append(html);
                $('#proceso-ctc').show();
            }
        });
      }
    })
    $("#modal-ctc").on("hidden.bs.modal", function (e) {
        $('#frm-ctc').show(function () {
            $('#msg-success').hide()
            //$('#llamada-ctc').attr('onclick', 'SolicitudCTC()');
        })
    });
    $("#modal_frm_app").on("shown.bs.modal", function (e) {
        $("#botonera-gral").css("display", "none");
        $("#modal-ctc").css("display", "none");
    });

    $(document).on("ocultaBotonera", function (e) {
        $("#botonera-gral").css("display", "none");
        $("#modal-ctc").css("display", "none");
    });

    $(window).on('load', function (e) {
        $("#modal_frm_app").on("hide.bs.modal", function (e) {
            $("#botonera-gral").removeAttr("style");
            $("#modal-ctc").removeAttr("style");
        });
    });
});
/*
function SolicitudCTC() {
    var seleccionValor;
    var ingresaTelefono;
    var error = 0;

    if ($('#customRadio1').prop("checked") == true) {
        seleccionValor = $('#customRadio1').val();
    } else if ($('#customRadio2').prop("checked") == true) {
        seleccionValor = $('#customRadio2').val();
    } else {
        seleccionValor = false;
        error++;
    }

    if ($("#ctc-phone").val() != "" && $("#ctc-phone").val() != undefined) {
        ingresaTelefono = $("#ctc-phone").val();
    } else {
        ingresaTelefono = false;
        error++;
    }

    if (seleccionValor == false) {
        $("#err-valor").addClass("block");
    } else {
        $("#err-valor").removeClass("block");
    }
    if (ingresaTelefono == false) {
        $("#err-tel").addClass("block");
    } else {
        $("#err-tel").removeClass("block");
    }

    if (error == 0) {
        var datosCompletos = seleccionValor.concat(ingresaTelefono);
        envioDatosCTC(datosCompletos);
    }
}
*/
function validateNumber(event) {
    var key = window.event ? event.keyCode : event.which;
    if (event.keyCode === 8 || event.keyCode === 46) {
        return true;
    } else if (key < 48 || key > 57) {
        return false;
    } else {
        return true;
    }
}
/*
function envioDatosCTC(phonen) {
    $('#llamada-ctc').removeAttr('onclick');
    dataLayer.push({ 'event': 'ENVIO_FRM_CTC' });
    jQuery
    .ajax({
        type: "POST",
        url: "/wp-content/themes/temaunitec/clickToCall.php",
        data: { "theData": { "phonenumber": phonen } },
        dataType: 'json',
        success: function (res) {
            if (res.success) {
                //mensaje satisfactorio
                var html = "";

                html += "<div class='d-flex justify-content-end'>";
                html += "<button type='button' class='p-0' data-dismiss='modal' aria-label='Close' style='background:0 0; border:0'>";
                html += "<div class='icons-desktop d-flex align-items-center justify-content-center'>";
                html += "<a href=''>";
                html += "<i class='icon-u icon-u-cerrar'></i>";
                html += "</a>";
                html += "</div>";
                html += "</button>";
                html += "</div>";
                html += "<div class='col-lg-12 p-3 pt-2 d-flex flex-column align-items-center'>";
                html += "<h3 class='m-0 titleSection text-tnks'>¡Gracias por dejarnos tus datos!</h3>";
                html += "<h5 class='m-0 descriptionSection text-tnks mt-2 mb-2'>Uno de nuestros asesores educativos te está marcando.</h5>";
                html += "</div>";

                $('#msg-success').empty();
                $('#msg-success').append(html);
                $('#proceso-ctc').hide();
                $('#blur-ctc').removeClass('blur-ctc');

                $('#frm-ctc').hide(function () {
                    $('#msg-success').show()
                });
                $("#formulario-ctc input").val('');
                $("#formulario-ctc input").prop("checked", false);

            } else {
                //mensaje intentar de nuevo
                $('#llamada-ctc').attr('onclick', 'SolicitudCTC()');
                $('#msg-alert').show();
                $('#proceso-ctc').hide();
                $('#blur-ctc').removeClass('blur-ctc');
            }
            console.log("Datos Enviados");
            console.log(res);
        },
        error: function (xhr, textStatus, error) {
            console.log(xhr);
            console.log(xhr.statusText);
            console.log(textStatus);
            console.log(error);
            //mensaje intentar de nuevo
            $('#llamada-ctc').attr('onclick', 'SolicitudCTC()');
            $('#msg-alert').show();
            $('#proceso-ctc').hide();
            $('#blur-ctc').removeClass('blur-ctc');
        },
        beforeSend: function () {
            var html = "";
            html += "<div class='col-lg-12 p-3 pt-2 d-flex flex-column align-items-center justify-content-center overlay-ctc'>";
            html += "<h3 class='m-0 titleSection text-tnks'>Tu llamada se encuentra en proceso.</h3>";
            html += "<h5 class='m-0 descriptionSection text-tnks mt-2 mb-2'><img src='assets/cargando.gif' class='img-fluid m-auto' style='width:100%; max-width:60px;'></h5>";
            html += "</div>";
            $('#blur-ctc').addClass('blur-ctc');
            $('#proceso-ctc').empty();
            $('#proceso-ctc').append(html);
            $('#proceso-ctc').show();
        }
    });
}
*/