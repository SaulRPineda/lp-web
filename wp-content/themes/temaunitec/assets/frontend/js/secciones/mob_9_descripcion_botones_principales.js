jQuery(document).ready(function(){
    //Autoajustar el dropdown dependiendo la altura y el ancho del div padre
    //jQuery('.dropdown-content').css({'margin-top': $('.select-box').outerHeight() -24});

    try{
        jQuery('.dropdown-content').css({'min-width': $('.select-box').width() + 10});
        //Obtener medidas del padre
        var id_parent = jQuery('.mdb-select > ul > li').parent().parent().parent().attr('id');
        var arr_id_parent = id_parent.split("select_");
        var alto_label = jQuery("label[for='" + arr_id_parent[1] + "']").outerHeight();
        jQuery('.dropdown-content').css({'margin-top':  - (alto_label + 6)})
        //Evento onChange para colocar el icono
        jQuery('.mdb-select > ul > li').on('click', function(){
            var parent_to_change = $(this).parent().parent().parent().attr('id');
            var icon_class = jQuery(this)[0].attributes[0].nodeValue;


            function sanitize_js(icon_class)
            {
                icon_class = icon_class.replace(/^\s+|\s+$/g, ''); // trim
                icon_class = icon_class.toLowerCase();
            
                // remove accents, swap ñ for n, etc
                var from = "àáäâèéëêìíïîòóöôùúüûñç·/_,:;";
                var to   = "aaaaeeeeiiiioooouuuunc------";
            
                for (var i=0, l=from.length ; i<l ; i++)
                {
                    icon_class = icon_class.replace(new RegExp(from.charAt(i), 'g'), to.charAt(i));
                }
            
                icon_class = icon_class.replace(/[^a-z0-9 -]/g, '') // remove invalid chars
                    .replace(/\s+/g, '-') // collapse whitespace and replace by -
                    .replace(/-+/g, '-'); // collapse dashes
            
                return icon_class;
            }
            var icon_class = sanitize_js(icon_class);
            jQuery("#" + parent_to_change + " > .icon-u").removeClass();
            jQuery("#" + parent_to_change + " > span").addClass("icon-u");
            jQuery("#" + parent_to_change + " > .icon-u").addClass(icon_class);
        });
    }catch(err) {
        console.log("Dropdown Select sec_ficha_tecnica: " + err)
    }
    
});


//jQuery(label[for='pa_modalidades']).outerHeight()