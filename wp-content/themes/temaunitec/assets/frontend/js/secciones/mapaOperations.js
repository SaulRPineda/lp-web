function mapaOperations() {
	// This example requires the Places library. Include the libraries=places
    // parameter when you first load the API. For example:
    // <script src="https://maps.googleapis.com/maps/api/js?key=YOUR_API_KEY&libraries=places">

	Ops = this;
	var map;
    var marker;
	var infowindow;
	var directionsDisplay;
    var directionsService;
    var input;
    var image = "//"+ document.domain +"/wp-content/themes/temaunitec/assets/frontend/img/indicador-maps.png";
    var coordenadas;
    var service;
    var autocomplete;
    var permiso
    var cookieUbicacion = "c_ubicacion";

    var opt_ubicacion = {
        enableHighAccuracy : true,
            timeout : Infinity,
            maximumAge : 0
    };

    /*Preguntamos por la Ubicacion actual del usuario*/
    this.askPosition = function(mycall_1, mycall_2) {
        if(!!navigator.geolocation) {
            navigator.geolocation.getCurrentPosition(Ops.getPosition, Ops.getError, opt_ubicacion);
            
            if (mycall_1 && typeof(mycall_1) === "function") {
                mycall_1();
            }

            if (mycall_2 && typeof(mycall_2) === "function") {
                mycall_2();
            }


        } else {
            console.log('Geolocation is not supported in your browser');
            }    
    };

    this.registerCookie = function(map_cookie, valor){
        document.cookie = map_cookie + "=" + valor;
    };

    this.getCookie = function (map_cookie) {
        var name = map_cookie + "=";
        var decodedCookie = decodeURIComponent(document.cookie);
        var ca = decodedCookie.split(';');
        for(var i = 0; i < ca.length; i++) {
            var c = ca[i];
            while (c.charAt(0) == ' ') {
                c = c.substring(1);
            }
            if (c.indexOf(name) == 0) {
                return c.substring(name.length, c.length);
            }
        }
        return null;
    };

    this.checkCookie = function (map_cookie) {
        var cookie = Ops.getCookie(map_cookie);
        var cookieVal = false;

        if (cookie != null) {
            cookieVal = true;
            console.log( "Ubicacion: " + document.cookie );
            $("#mi-posicion").val("Mi Ubicación");
        }
        return cookieVal;
    };

    this.deleteCookie = function(map_cookie) {
        document.cookie = map_cookie + "=; expires=Thu, 01 Jan 1970 00:00:00 UTC";
    };    

    this.getPosition = function(position) {        
        coordenadas =  {
            lat: position.coords.latitude,
            lng: position.coords.longitude            
        };

        var valorCookie = coordenadas.lat+','+coordenadas.lng;
        $("#h_posicion").val(valorCookie);
        $("#mi-posicion").val("Mi Ubicación");
        $("#icon-maps").attr( "href", "//www.google.com.mx/maps/dir/" + valorCookie + "/UNITEC, " + $("#mi-destino").val() );

        Ops.registerCookie(cookieUbicacion, valorCookie);        
    };

    this.getError = function(error) {           
        switch(error.code) {
            case error.PERMISSION_DENIED:
                console.log("El usuario no ha querido compartir su ubicación.");
                $("#permiso").val(0);
                break;
            case error.POSITION_UNAVAILABLE:
                console.log("El cliente no ha sido capaz de ubicar su posición.");
                $("#permiso").val(0);
                break;
            case error.TIMEOUT:
                console.log("Se ha agotado el tiempo que se había establecido para obtener la posición.");
                $("#permiso").val(0);
                break;
            case error.UNKNOWN_ERROR:
                console.log("A ocurrido un error.");
                $("#permiso").val(0);
                break;
        }      
    };    
    
	this.initMap = function () {
		var objeto = new Object();
		var latlng = $("#h_destino").val();
        var posicion = $("#posicion").val();
	
		if (latlng == "undefined" || latlng == null || latlng == "") {
			latlng = "19.5455604,-99.24097970000003";	/*Coordenadas Campus Atizapan*/
            $("#destino").val(latlng);
		}

		var coord = latlng.split(",");
		objeto.lat = parseFloat(coord[0]);
		objeto.lng = parseFloat(coord[1]);		
		coordenadas = objeto;

        map = new google.maps.Map(document.getElementById('map'), {
            center: coordenadas,
            zoom: 15,
            streetview: false
        });

        /*Implementación autocomplete*/
        input = document.getElementById('mi-posicion');
        var options = {            
            componentRestrictions: {country: 'mx'}
        };

        autocomplete = new google.maps.places.Autocomplete(input, options);
        /*End Implementación autocomplete*/
        Ops.createMark();
    };    

    this.createMark = function () {
        /*Se Genera el Marker del Destino*/
        marker = new google.maps.Marker({
            map: map,
            draggable: false,
            animation: google.maps.Animation.DROP,
            position: coordenadas,
            title: coordenadas.lat + "," + coordenadas.lng,
            icon: image
        });
    };

    this.searchPlace = function () {
        infowindow = new google.maps.InfoWindow();
        service = new google.maps.places.PlacesService(map);
        service.nearbySearch({
            location: coordenadas,
            radius: 7000,
            name: "UNITEC",
            keyword: "UNITEC",
            type: ['university']
        }, Ops.callback);
	};

	//Verificar que solo sea Universidad UNITEC
    this.callback = function (results, status) {
        if (status === google.maps.places.PlacesServiceStatus.OK) {
            for (var i = 0; i < results.length; i++) {
                console.log(JSON.stringify(results[i]));
                Ops.createMarker(results[i]);
            }
        }
    };

    this.createMarker = function (place) {
        var placeLoc = place.geometry.location;
        marker = new google.maps.Marker({
            map: map,
            animation: google.maps.Animation.DROP,
            position: place.geometry.location,
            title: place.name + " " + place.geometry.location,
            /*icon: image*/
        });

        google.maps.event.addListener(marker, 'click', function() {
            infowindow.setContent(place.name);
            infowindow.open(map, this);
        });
    };

    /*callback al hacer clic en el marcador lo que hace es quitar y poner la animacion BOUNCE*/
    this.toggleBounce = function () {
        if (marker.getAnimation() !== null) {
            marker.setAnimation(null);
        } else {
            marker.setAnimation(google.maps.Animation.BOUNCE);
        }
    };    

    this.calculateAndDisplayRoute = function () {
        var ubicacion = $("#mi-posicion").val();
        var start = $("#h_posicion").val();
        var end = $("#h_destino").val();

        /*Obtengo la posicion que el usuario teclea*/
        if(start == null || start == "" || ubicacion != "Mi Ubicación") {
            start = $("#mi-posicion").val();
        }

        console.log("Ubicacion: " + start + " Destino:" + end);

        /*Renderizamos nuevamente el Mapa Para borrar la trayectoria anterior*/
        Ops.initMap();

        directionsDisplay = new google.maps.DirectionsRenderer;
        directionsService = new google.maps.DirectionsService;

        directionsDisplay.setMap(map);

        /*Se muestran las calles como llegar en un panel*/
        /*directionsDisplay.setPanel(document.getElementById('right-panel'));
    	$("#right-panel").html('');*/

        directionsService.route({
          	origin: start,
          	destination: end,
          	travelMode: 'DRIVING'
        	}, function(response, status) {
          
	          	if (status === 'OK') {
	            	directionsDisplay.setDirections(response);
	          	} else {
	            	window.alert('Directions request failed due to ' + status);
	          	}
	        });
    };

    /*Conocer si esta ubicado en CDMX*/
    this.geocode = function() {
        jQuery.ajax({ url:'https://maps.googleapis.com/maps/api/geocode/json?latlng='+coordenadas.lat+','+coordenadas.lng+'&sensor=true',
            success: function(data){
                var validaforaneo = "true";
                console.log(data.results[0].address_components[5]);
                         
                for(var i = 0; i < data.results[0].address_components.length ; i ++) {
                    console.log(data.results[0].address_components[i]);
                    
                    for(var e = 0; e < Object.keys(data.results[0].address_components[i]).length; e ++) {
                        
                        if(data.results[0].address_components[i].long_name.indexOf("CDMX") != -1 || data.results[0].address_components[i].short_name.indexOf("CDMX") != -1) {
                            validaforaneo = "false";
                            console.log("Es ciudad de mexico");
                            break;
                        }
                    }

                }
                         
                //Si es foraneo ocultamos las materias que son para foraneos
                if(validaforaneo == "true") {
                    console.log("ocultar materias");
                    jQuery(".foraneos").hide()
                }   else {
                        console.log("no ocultamos por que es CDMX");
                    }

            }
        });
    };   
}