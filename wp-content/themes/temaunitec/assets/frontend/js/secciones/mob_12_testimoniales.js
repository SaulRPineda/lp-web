$(document).ready(function(){

    if ( testimoniales_json != "" || testimoniales_json != null || testimoniales_json != 'undefined' ) {
        muestraTestimonios(testimoniales_json, tab_activo, "content-testimonio-0");
        $("#tab-testimonio-activo").val(tab_activo);
    } else{
        return false;
    }
        
});

/*Revisar cuando no hay video ya que el modal se traba*/
$("#modal-tarjeta-testimoniales").on('hidden.bs.modal', function () {

    if ( $("#h_tab-contiene-video").val() == "S" ) {

        switch( $("#h_video-activo").val() ) {
            case 'primero':
                try{
                    $('.iframe-testimonios')[0].contentWindow.postMessage( '{"event":"command","func":"' + "stopVideo" + '","args":""}', "*");
                } catch (e) {
                    console.log(e);
                }                
            break;

            default:
                $( ".youtubetestimonios" ).remove( $("h_video-activo").val() );
            break;
        }

        $("#h_tab-contiene-video").val("N");
        $("#h_video-activo").val("");
       
    }    
});


function muestraTestimonios(testimoniales_json, tab_seleccionado, tabdiv) {
        var nodoJson = testimoniales_json[tab_seleccionado][0];
        /*var cnt_testimoniales = Object.values(nodoJson);*/
        var vals = Object.keys(nodoJson).map(function(key) {
            nodoJson[key];
        });

        $("#" + tabdiv).html("");

        console.log( nodoJson );

        for(var i=0; i < vals.length; i++) {
            $('<input type="hidden" name="ModalName" value="modal-tarjeta-testimoniales"><a class="col-4 nav-item tab-tarjeta testimoniales-mob p-0 mr-2" data-toggle="tab" role="tab" name="' + tab_seleccionado + '-' + i + '" data-gtm-tr="Testimony" data-gtm-testimonio="' + nodoJson[i][0].perfil + '" data-gtm-nombre="' + nodoJson[i][0].nombre + '" style="background-image:url('+ nodoJson[i][0].urlthumbnail +')" id="'+ nodoJson[i][0].postName +'"><div class="title-testimoniales h-100 d-flex align-items-end p-1 title-testimonio-'+nodoJson[i][0].postName+'">' + nodoJson[i][0].nombre + '</div></a>').appendTo("#" + tabdiv);

           /* nodoJson[i].forEach(function(item, index) {
                // console.log(item.url);
            });*/
        }

        $( ".tab-tarjeta" ).bind( "click", function() { openModal("#modal-tarjeta-testimoniales"); ver_galeria_testimonios(testimoniales_json, $(this).attr('name'), insertar_imagenes_testimonios, destaca_titulo, $(this).attr('id') ); });
}

function ver_galeria_testimonios(testimoniales_json, testimonio_seleccionado, callbakImg, callbackdestaca, id_portada) {
    $tab_activ = $("#tab-testimonio-activo").val();
    $testimonio_seleccionado = testimonio_seleccionado;
    $indice = $testimonio_seleccionado.replace($tab_activ + '-', '');

    $('#testimonio-title').html('');
    $('#perfil-title').html('');
    $('#licenciatura-title').html('');
    $("#content-item-testimonial").html('');
    $("#indicadores-carusel-testimoniales").html('');

    var nodoJson = testimoniales_json[$tab_activ][0];
    /*Se obtiene el array del array Seleccionado*/
    /*var cnt_testimoniales = Object.values(nodoJson);*/ 
    /*var vals = Object.keys(cnt_testimoniales).map(function(key) {
            return cnt_testimoniales[key];
    });*/

    /*Se obtienen los elementos de la posicion seleccionada*/
    nodoJson[$indice].forEach(function(item, index) {
        $('#testimonio-title').html( item.nombre );
        $('#perfil-title').html( item.perfil );
        $('#licenciatura-title').html( item.licenciatura );
        $('#share-testimoniales div ul li a').attr("seccion", "Testimonial: "+item.nombre );
        $('#footer-testimonio').html( '❝ ' +item.contenido + ' ❞' );

        if (index === 0) {
            $('#img-perfil-testimonial').attr('src', item.urlthumbnail);
        }

        if (index > 0) {
            $('<div />', { 
                'id': 'slide-testimonial-'+index, 
                'class': 'carousel-item'
            }).appendTo('#content-item-testimonial');            
        }

        if (index === 1) {$("#indicadores-carusel-testimoniales").css('display','none');} else{$("#indicadores-carusel-testimoniales").removeAttr('style');}

        if (index > 0) {
            $("#indicadores-carusel-testimoniales").append('<li data-target="#carousel-tarjeta-testimonial" data-slide-to="'+ index +'" class="preview-'+ index+'"></li>');
            /*console.log(item.url);*/
        }           
    });

    if (callbakImg && typeof(callbakImg) === "function") {
        callbakImg(testimoniales_json, $indice);
    }

    if (callbackdestaca && typeof(callbackdestaca) === "function") {
        callbackdestaca(id_portada);
    } 

    $("#indicadores-carusel-testimoniales > li:first").addClass("active");
}

function destaca_titulo(id_portada){
    /*Update 1.1 destaca el titulo al seleccionar un Testimonio*/
    $(".title-testimoniales").removeAttr('style');
    $(".title-testimonio-" + id_portada).css({'font-weight': '500'});
    /*Update 1.1 destaca el titulo al seleccionar una Testimonio*/
}

function insertar_imagenes_testimonios(testimoniales_json, indice){
    $tab_activ = $("#tab-testimonio-activo").val();
    var nodoJson = testimoniales_json[$tab_activ][0];
    /*Se obtiene el array del array Seleccionado*/
    /*var cnt_testimoniales = Object.values(nodoJson);*/
    $indice = indice; 

    /*Se obtienen los elementos de la posicion seleccionada*/
    nodoJson[$indice].forEach(function(item, index) {
        video = item.description;
        video.toLowerCase();

        if (  video.indexOf("youtube") !== -1 ) {            
            /*<i class="icon-u-ver-izquierda"></i>*/
            $('#slide-testimonial-'+index).prepend('<div class="col-1 div-yt-video-left"><i class="icon-u-ver-izquierda"></i></div>');

            $('<div />', {
                'id': 'div-video-testimonial-'+index,
                'class':'col-10 div-yt-video'
            }).appendTo('#slide-testimonial-'+index);

            switch(index){
                case 1:
                    $('<iframe />', {
                    'id': 'testimonio-youtube-'+index,
                    'class': 'iframe-testimonios youtubetestimonios',
                    'controls': 'controls',
                    'x-webkit-airplay': 'allow',
                    'allowscriptaccess': 'always',
                    'scrolling': 'no',
                    'src': video + "?enablejsapi=1&rel=0&showinfo=0&amp;autoplay=1", 
                    'frameborder': '0',
                    'allowfullscreen': 'yes'
                }).appendTo('#div-video-testimonial-'+index);

                $("#h_video-activo").val('primero');
                setTimeout(function(){ $('#testimonio-youtube-'+index)[0].contentWindow.postMessage( '{"event":"command","func":"' + "playVideo" + '","args":""}', "*"); }, 3000);
                break;

                default:
                    $('<div id="testimonio-youtube-' + index + '" class="wrapper youtube youtubetestimonios" data-embed="' + video + '" title="iframe-testimonios"><div class="play-button"></div></div>').appendTo('#div-video-testimonial-'+index);
                break;

            }

            /*Bandera que define si el testimonio Contiene Video*/
            $("#h_tab-contiene-video").val("S");
            /*Bandera que define si el testimonio Contiene Video*/
            

            $('#slide-testimonial-'+index).append('<div class="col-1 div-yt-video-right"><i class="icon-u-ver-derecha"></i></div>');
            /*<i class="icon-u-ver-derecha"></i>*/            

        } else if(video.indexOf("videos") !== -1){
            $('#slide-testimonial-'+index).prepend('<div class="col-1 div-yt-video-left"><i class="icon-u-ver-izquierda"></i></div>');

            $('<div />', {
                'id': 'div-video-ext-testimonial-'+index,
                'class':'col-10 div-yt-video'
            }).appendTo('#slide-testimonial-'+index);

            $('<video />', {
                'id': 'video-full-testimonial'+index,
                'controls': 'yes'
            }).appendTo('#div-video-ext-testimonial-'+index);

            $('<source />', {
                'id': 'video-source-full-testimonial-'+index,
                'src': video,
                /*'type': 'video/mp4',*/
                'type': 'video/webm',
            }).appendTo('#video-full-testimonial'+index);

            $('#slide-testimonial-'+index).append('<div class="col-1 div-yt-video-right"><i class="icon-u-ver-derecha"></i></div>');

        } else{
            if (index > 0) {
                $('<img />', { 
                        'src': item.url, 
                        'alt':item.alt, 
                        'class':'tab-tarjeta galeria-child animated flipInX' 
                }).appendTo('#slide-testimonial-'+index);
            }
        }
        youtubeLazy();
        $('#slide-testimonial-1').addClass("active");
    });

}