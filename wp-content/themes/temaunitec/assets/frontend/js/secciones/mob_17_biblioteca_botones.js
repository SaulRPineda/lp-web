$(document).ready(function() {
	
    var desbloquea = checkCookie( 'c_matricula' );

	if ( desbloquea ) {

		jQuery("#seccion-biblioteca-recursos").removeAttr('style');
        jQuery("#seccion-biblioteca-apoyo").removeAttr('style');

	} else {

        jQuery("#seccion-biblioteca-recursos").css("style","none");
        jQuery("#seccion-biblioteca-apoyo").css("style","none");

		$(".url-library").click(function(){
            jQuery("#modal_frm_biblioteca").modal();
        });
	}

    if( location.href.indexOf( "biblioteca-virtual" ) !== -1 && desbloquea != true ) {
        $("#login-biblioteca").removeClass('d-none');
        $("#logout-biblioteca").addClass('d-none');
    } else{
        $("#login-biblioteca").addClass('d-none');
        $("#logout-biblioteca").removeClass('d-none');
    }
});

/*Funcion para cerrar Sesion Biblioteca Virtual*/
function cerrarSesion(){

    document.cookie = "c_matricula=;expires=Thu, 01 Jan 1970 00:00:00 GMT;path=/";
    document.cookie = "c_nombre=;expires=Thu, 01 Jan 1970 00:00:00 GMT;path=/";
    document.cookie = "c_perfil=;expires=Thu, 01 Jan 1970 00:00:00 GMT;path=/";

    //Se eliminan variables de Local Storage
    localStorage.removeItem("biblioteca_libri");
    localStorage.removeItem("biblioteca_pearson");
    localStorage.removeItem("biblioteca_cengage");
    localStorage.removeItem("biblioteca_ebrary");
    localStorage.removeItem("biblioteca_kivuto");

    //Se remueven la url asignadas
    jQuery("#biblioteca_libri_0").removeAttr("href");
    jQuery("#biblioteca_libri_1").removeAttr("href");
    jQuery("#biblioteca_libri_2").removeAttr("href");
    jQuery("a#pearson").removeAttr("href");

    //Redireccion a la página de Home
    location.reload();
}
/*End Funcion para cerrar Sesion Biblioteca Virtual*/

function getCookie(cname) {
    var name = cname + "=";
    var ca = document.cookie.split(';');
    for (var i = 0; i < ca.length; i++) {
        var c = ca[i];
        while (c.charAt(0) == ' ') c = c.substring(1);
        if (c.indexOf(name) == 0) return c.substring(name.length, c.length);
    }
    return "";
}

function checkCookie(valor) {
    var user = getCookie(valor);
    if (user != "") {
        return true;
    }
    else {
        return false;
    }
}

//Eliminar la cookie
function eliminarCookie(nombre_cookie)
{
    var cookieActivaAlt = document.cookie = nombre_cookie + "=;expires=Thu, 01 Jan 1970 00:00:00 GMT;path=/";
    console.log("La cookie ahora con javascript: " + cookieActivaAlt);
}