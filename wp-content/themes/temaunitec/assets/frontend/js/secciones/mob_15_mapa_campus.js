$(document).ready(function() {
	$('#activarMapaGoogle').on('click',function(){
		$("#map").addClass('mapa');
		$(this).unbind('click');
		$('.img-mapa').hide();
		startMapEverything();
	});
	function startMapEverything(){
		var Ops = new mapaOperations();
		var validaCookie = Ops.checkCookie("c_ubicacion");

		/*Preguntar si existe la cookie*/
		if (validaCookie) {
			Ops.initMap();
		} else{
			//Si no existe pregunto por su ubicación e inicializar mediante el callback el Mapa
			// Ops.askPosition();
			Ops.askPosition( Ops.initMap() );
		}

		if(validaCookie ) {
			$("#mi-posicion").val("Mi Ubicación");
			//alert('mostrar posicion');
			$("#getDirections").attr( "href", "//www.google.com.mx/maps/dir/" + $("#h_posicion").val() + "/UNITEC, " + $("#mi-destino").val() );
		}

		$("#mi-posicion").click(function() {
			if(validaCookie ) {
				$("#mi-posicion").val("Mi Ubicación").removeAttr("style");
			} else{
				$("#mi-posicion").val("");
				$("#h_posicion").val("");
			}
		});

		/*Caso cuando se obtiene Ubicación*/
		$( "#getDirections" ).click(function() {
			if ( $( "#mi-posicion" ).val() === "" || $( "#mi-posicion" ).val() === null ) {
				$("#mi-posicion").css({
					'border-color' : '#F00',
					'background-color' : '#FFFF99'
				});
			} else if($( "#mi-posicion" ).val() === "Mi Ubicación"){
				$("#getDirections").attr( "href", "//www.google.com.mx/maps/dir/" + $("#h_posicion").val() + "/UNITEC, " + $("#mi-destino").val() );
			} else{
				
			}

			/*if( $("#h_posicion").val() !== "" || $("#h_posicion").val() !== null ){
				Ops.calculateAndDisplayRoute();
			}*/
		});
		/*End Caso cuando se obtiene Ubicación*/

		var input = document.getElementById('mi-posicion');
		var autocomplete = new google.maps.places.Autocomplete(input);
		var geocoder = new google.maps.Geocoder();

		autocomplete.addListener('place_changed', function() {
			var place = autocomplete.getPlace();

			var direccion = place.address_components[0].short_name + ", " + place.address_components[1].short_name + ", " + place.address_components[2].short_name + ", " + place.address_components[3].short_name;

			console.log(  place.address_components[0].short_name + ", " + place.address_components[1].short_name + ", " + place.address_components[2].short_name + ", " + place.address_components[3].short_name );

			$("#getDirections").attr( "href", "//www.google.com.mx/maps/dir/" + direccion + "/UNITEC, " + $("#mi-destino").val() );

			/*Opcion 2: place.formatted_address*/
			console.log( place );

			//Obtenemos las coordenadas de la dirección capturada
			/*geocoder.geocode({ 'address': place.formatted_address}, function(results, status) {
				if (status == 'OK') {
					$("#h_posicion").val( results[0].geometry.location.lat()+', '+results[0].geometry.location.lng() );
					Ops.calculateAndDisplayRoute();
				}
			});*/	
		});

		$("#mi-posicion").keypress(function() {
			$(this).removeAttr("style");
		});
	}
    
});