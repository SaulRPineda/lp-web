$(document).ready(function () {
    $galeriaActiva = $("#galeria-activa").val();
    $galeriaName = $("#galeria-name").val();

    discounts_json = JSON.parse(discounts_json);
    category_array_json = JSON.parse(category_array_json);
    category_contact_array_json = JSON.parse(category_contact_array_json);
    category_link_array_json = JSON.parse(category_link_array_json);
});

$(".portada-galeria-descuentos").on("click", function () {
    ver_descuentos(discounts_json, $(this).attr("name"), $(this).attr("title"));
});

function ver_descuentos(discounts_json, descuento, descuentoName) {
    var inicio = 2;

    console.log(discounts_json);
    console.log(category_array_json);
    console.log(category_contact_array_json);

    var html = "";
    var html_contacto = "";

    $('.modal-titulo-descuentos').html(discounts_json[descuento]['title']);
    $('.modal-categoria-descuentos').html(category_array_json[discounts_json[descuento]['categoria']]);
    $('.modal-terminos-descuentos').html(discounts_json[descuento]['terminos_condiciones']);
    // $('.modal-titulo-descuentos').html(discounts_json[descuento]['title']);

    // var caracteristicas_modal = discounts_json[descuento]['caracteristicas'].split("<br />");
    // console.log("Aqui va el bueno");
    // console.log(caracteristicas_modal);

    // caracteristicas_modal.forEach(function(carac, index) {
    //     html += "<li class='desc-ind d-flex align-items-center'> <i class='icon-u icon-u-bullet bullet pr-2'></i>" + carac + "</li>";
    // });

    // $("#modal-descripcion-descuentos li").remove();
    // $("#modal-descripcion-descuentos").append(html);

    var contacto_modal = discounts_json[descuento]['contacto'];
    var link_modal = discounts_json[descuento]['links'];
    var accion_contacto;

    /* ForEach especÃ­fico para los objetos */
    $.each(contacto_modal, function (index, value) {
        switch (index) {
            case "telefono":
                accion_contacto = "";
                break;
            case "correo":
                accion_contacto = "href='mailto:" + value + "'";
                break;
            default:
                accion_contacto = "href='#'";
                break;
        }
        html_contacto += "<li class='descuentos-lista'>" + category_contact_array_json[index] + ": <a " + accion_contacto + "><b>" + value + "</b></a></li>";
    });

    // html_contacto += "<li class='descuentos-lista'>QR de descuento: <a href='" + discounts_json[descuento]['qr-code-descuento'] + "'><b><span type='button' class='descargar-descuento'>Descargalo aquÃ­</span></b></a></li>";

    /* ForEach especÃ­fico para los links */
    $.each(link_modal, function (index, value) {
        html_contacto += "<li class='descuentos-lista'>" + category_link_array_json[index] + ": <a href='" + value[1] + "'><b>" + value[0] + "</b></a></li>";
    });

    $("#modal-contacto-descuentos li").remove();
    $("#modal-contacto-descuentos").append(html_contacto);

    $('.modal-imagen-descuento').css('background-image', 'url(' + discounts_json[descuento]['url'] + ')');


    openModal("#modal-tarjeta-galeria-carrusel-descuentos");
}

function filtroTipoCategorias(llave, nombre) {
    $('#nameCategoriasDescuentos').html(nombre);
    $('#nameCategoriasDescuentos').attr('data-descuentos-categorias', llave);

    var check_campus_discount = $('#nameCampusDescuentos').attr('data-descuentos-campus');

    $('#available-discounts').prop('hidden', true);
    // console.log(llave);
    /* Todos los elementos cuya clase empiece con categoria- */
    $('div').filter(function () { return this.className.match(/\bcategoria-/) }).prop('hidden', true);
    // $( "div" ).filter( ".categoria-" + llave ).siblings().prop('hidden',true);
    if (check_campus_discount) {
        $("div").filter(".campus-" + check_campus_discount).filter(".categoria-" + llave).prop('hidden', false);
    } else {
        $("div").filter(".categoria-" + llave).prop('hidden', false);
    }

    var check_available = $('div').filter(function () { return this.className.match(/\bcategoria-/) }).filter(':visible').length;
    if (check_available == 0)
        $('#available-discounts').prop('hidden', false);


}

function filtroTipoCampus(llave, nombre) {
    $('#nameCampusDescuentos').html(nombre);
    $('#nameCampusDescuentos').attr('data-descuentos-campus', llave);

    var check_categories_discount = $('#nameCategoriasDescuentos').attr('data-descuentos-categorias');

    $('#available-discounts').prop('hidden', true);
    // console.log(llave);
    /* Todos los elementos cuya clase empiece con campus- */
    $('div').filter(function () { return this.className.match(/\bcampus-/) }).prop('hidden', true);
    // $( "div" ).filter( ".categoria-" + llave ).siblings().prop('hidden',true);
    if (check_categories_discount) {
        $("div").filter(".campus-" + llave).filter(".categoria-" + check_categories_discount).prop('hidden', false);
    } else {
        $("div").filter(".campus-" + llave).prop('hidden', false);
    }

    var check_available = $('div').filter(function () { return this.className.match(/\bcategoria-/) }).filter(':visible').length;
    if (check_available == 0)
        $('#available-discounts').prop('hidden', false);

}
function abreGaleriaDesc() {
    jQuery('html').addClass('scroll-hiden');
}
function cierraGaleriaDesc() {
    jQuery('html').removeClass('scroll-hiden');
}