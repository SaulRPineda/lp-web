var topeleft = true;
var topeRight = false;
var right = false;

$(document).ready( function(){

    jQuery('#calculadoraModal').click(function(){
        getJsonCarreras();
        getJsonLinksCategorias();
    });

    /*$(window).panleft(function(){
        if(Math.abs($("#side-nav-content").offset().left) >= 100) { 
            enableScrolling();
        }
    });*/
    var sidenav = document.getElementById('UnitecSideNav');
    var sidenavcontent = document.getElementById('side-nav-content');
    var mc = new Hammer( sidenav );
    var sdnvct = new Hammer( sidenavcontent );
    
    mc.get('pan').set({ direction: Hammer.DIRECTION_HORIZONTAL, threshold: 5 });

    mc.on("panleft panright panend", function(ev) {

        var offset = $('#UnitecSideNav').position();

        if( ev.type === "panright" ){

            //$("html").css({ "overflow-y" : "hidden" });
            //$("body").css({ "overflow-y" : "hidden" });
            //console.log("quitar scroll a la derecha");
            /*if(Math.abs($("#side-nav-content").offset().left) >= 100) {
                enableScrolling();
            }else {
                disableScrolling();
            }*/
            
            topeleft = false;
            right = true;
            if( topeRight == true){
                return;
            }
            if( offset.left <= 0 ){
                topeRight = false;
                $("#UnitecSideNav").css('left', parseInt( offset.left ) + parseInt( ev.center.x / 32) );
            }else{
                $("#UnitecSideNav").css('left', 0);
                topeRight = true;
            }
        }
        if( ev.type === "panleft" ){
            //console.log($('#UnitecSideNav').position());
            //$("html").css({ "overflow-y" : "" });
            //$("body").css({ "overflow-y" : "" });
            //console.log("poner scroll a la izquierda");
            /*if(Math.abs($("#side-nav-content").offset().left) >= 100) {
                enableScrolling();
            }else {
                disableScrolling();
            }*/
            right = false;
            topeRight = false;
            sideNavWidth = ( ( $("#UnitecSideNav").width() ) * -1 ) + 30;
            if( topeleft == true){
                return;
            }
            if( offset.left >= sideNavWidth ){
                topeleft = false;
                $("#UnitecSideNav").css('left', parseInt( offset.left ) - parseInt( ev.center.x / 32) );
            }else{
                $("#UnitecSideNav").css('left', '-95%');
                topeleft = true;
            }
        }
        
        if( ev.type == "panend" ){

            /*if(Math.abs($("#side-nav-content").offset().left) >= 100) {
                enableScrolling();
            }else {
                disableScrolling();
            }*/


            if( right == true ){
                //$("html").css({ "overflow-y" : "hidden" });
                //$("body").css({ "overflow-y" : "hidden" });
                //console.log("quitar scroll a la derecha");
                /*if(Math.abs($("#side-nav-content").offset().left) >= 100) {
                    enableScrolling();
                }else {
                    disableScrolling();
                }*/
                $("#UnitecSideNav").animate({ 'left' : "0px" }, 700);
            }else{
                //$("html").css({ "overflow-y" : "" });
                //$("body").css({ "overflow-y" : "" });
                //console.log("Poner scroll a la izquierda");
                /*if(Math.abs($("#side-nav-content").offset().left) >= 100) {
                    enableScrolling();
                }else {
                    disableScrolling();
                }*/
                $("#UnitecSideNav").animate({ 'left' : "-95%" }, 700);
            }
        }
    });
});//Fin document ready


$(".btnMenuMobile").on( "click", function(ev){
    //Evitamos el scrolltop
    $("#UnitecSideNav").css("display", "block");
    $("#UnitecSideNav").animate({ 'left' : "0%" }, 500);
    ev.preventDefault();
    //$("html").css({ "overflow-y" : "hidden" });
    //$("body").css({ "overflow-y" : "hidden" });
    //console.log("Quitar scroll al abrir el sidenav");
    /*if(Math.abs($("#side-nav-content").offset().left) >= 100) {
        enableScrolling();
    }else {
        disableScrolling();
    }*/
    topeleft = false;
    topeRight = true;
    /*$("#UnitecSideNav").animate({ 'left' : "0%" }, 1200);*/
});

function closeSideNav(){
    $("#UnitecSideNav").animate({ 'left' : "-95%" }, 500 );
    //$("html").css({ "overflow-y" : "" });
    //$("body").css({ "overflow-y" : "" });
    //console.log("Poner scroll al cerrar el sidenav");
    //enableScrolling();
    topeleft = true;
    topeRight = false;
    /*$("#UnitecSideNav").animate({ 'left' : "-95%" }, 700 );*/
}


function disableScrolling(){
    //console.log("Disable SCroll dentro de la funcion");
    var x=window.scrollX;
    var y=window.scrollY;
    window.onscroll=function(){window.scrollTo(x, y);};
}

function enableScrolling(){
    //console.log("Enable scroll dentro de la funcion");
    window.onscroll=function(){};
}