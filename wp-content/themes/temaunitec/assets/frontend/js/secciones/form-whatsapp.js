// function clickCalcu(activarCalcu) {
//     location.href = '/calcula-tu-beca';
//     return false;
// }

$(document).ready(function () {
    // Aquí comienzan las funciones del formulario whatsapp
    $('.modal-whatsapp').addClass('fade');
    $("#modal-whatsapp").on("shown.bs.modal", function (e) {
        if ($('#modal-whatsapp').hasClass("show") == true) {
            $('.modal-whatsapp').removeClass('fade');
        }
    });
    // $("#modal-whatsapp").on("show.bs.modal", function (e) {
    //     if (location.href.indexOf("calcula-tu-beca") !== -1) {
    //         closeMobileCalculator();
    //     } else {
    //         $('#icon-exit-form').click();
    //     }
    // });
    $("#modal-whatsapp").on("hide.bs.modal", function (e) {
        $('.modal-whatsapp').addClass('fade')
    });

    // if (location.href.indexOf("calcula-tu-beca") !== -1) {
    //     $("#modal-whatsapp").css("display", "none");
    // }
    

    $('#phone-whatsapp').keypress(validateNumber);
    $('#whatsapp-success').hide();
    $('#whatsapp-alert').hide();
    $('#proceso-whatsapp').hide();
    //$('init-whatsapp').attr('onclick', 'SolicitudCTC()');
    $('#init-whatsapp').click(function () {
        // var seleccionValor;
        var ingresaTelefono;
        var error = 0;

        // if ($('#customRadio1').is(":checked")) {
        //     seleccionValor = $('#customRadio1').val();
        // } else if ($('#customRadio2').is(":checked")) {
        //     seleccionValor = $('#customRadio2').val();
        // } else {
        //     seleccionValor = false;
        //     error++;
        // }

        if ($("#phone-whatsapp").val() != "" && $("#phone-whatsapp").val() != undefined) {
            ingresaTelefono = $("#phone-whatsapp").val();
            if(ingresaTelefono.length < 10){
                ingresaTelefono = false;
                error++;
            }
        } else {
            ingresaTelefono = false;
            error++;
        }

        // if (seleccionValor == false) {
        //     $("#err-valor").addClass("block");
        // } else {
        //     $("#err-valor").removeClass("block");
        // }

        if (ingresaTelefono == false) {
            $("#telefono-erroneo").addClass("block");
        } else {
            $("#telefono-erroneo").removeClass("block");
        }

        var clientId;
        var tipoDispositivo;
        var bannerAhora;

        // ga('create', 'UA-9351414-14', 'auto');
        // ga(function(tracker) { 
        //     clientId = tracker.get('clientId');
        // });

        try {
            var trackers = ga.getAll();
            var i, len;
            for (i = 0, len = trackers.length; i < len; i += 1) {
              if (trackers[i].get('trackingId') === "UA-9351414-14") {
                // alert(trackers[i].get('clientId'));
                // setCookieForm("cid_unitec", trackers[i].get('clientId'), 1);
                // jQuery("#CID").val(trackers[i].get('clientId'));
                clientId = trackers[i].get('clientId');
              }
            }
        } catch(e) {} 

        if(IsMobile()){
            tipoDispositivo = "mobile"
        } else {
            tipoDispositivo = "desktop";
        }  
        console.log(tipoDispositivo);

        if( checkCookie('banner_activo') === true ){
            //lo asigo a la cookie form_data
            bannerAhora = getCookie('banner_activo');
        } else {
            var decoding = JSON.parse(decodeURIComponent(getCookie('c_form_data')));
            bannerAhora = decoding.banner;
        }

        var url_referrer = window.location.href;
        if (error === 0) {
            var redirectWindow = window.open('https://api.whatsapp.com/send?phone=525529688928&text=Hola%2C%20estoy%20en%20la%20página%20de%20internet%20y%20quiero%20más%20información', '_blank');
            var phonen = String(ingresaTelefono);
            var exito = false;
            //envioDatosCTC(datosCompletos);
            dataLayer.push({ 'event': 'ENVIO_FRM_CTC' });
            jQuery
                .ajax({
                    type: "POST",
                    // url: "/wp-content/themes/temaunitec/insertWS.php",
                    url: "https://www.unitec.mx/procWebLeads/insertWS.php",
                    data: { "theData": { "telefono": phonen, "urlreferrer": url_referrer, "CID": clientId, "tipoDispositivo": tipoDispositivo, "banner": bannerAhora } },
                    dataType: 'json',
                    success: function (res) {
                        if (res.success) {
                            exito = true;
                            dataLayer.push({'event': 'whats_success'});
                            //mensaje satisfactorio
                            var html = "";

                            html += "<div class='d-flex justify-content-end'>";
                            html += "<button type='button' class='p-0' data-dismiss='modal' aria-label='Close' style='background:0 0; border:0'>";
                            html += "<div class='icons-desktop d-flex align-items-center justify-content-center'>";
                            html += "<a href=''>";
                            html += "<i class='icon-u icon-u-cerrar'></i>";
                            html += "</a>";
                            html += "</div>";
                            html += "</button>";
                            html += "</div>";
                            html += "<div class='col-lg-12 p-3 pt-2 pb-4 d-flex flex-column align-items-center'>";
                            html += "<h3 class='m-0 titleSection text-tnks'>�Gracias por escribirnos!</h3>";
                            // html += "<h5 class='m-0 descriptionSection text-tnks mt-2 mb-2'>Uno de nuestros asesores educativos te está marcando.</h5>";
                            html += "</div>";

                            $('#whatsapp-success').empty();
                            $('#whatsapp-success').append(html);
                            $('#proceso-whatsapp').hide();
                            $('#blur-ctc').removeClass('blur-ctc');

                            $('#frm-whatsapp').hide(function () {
                                $('#whatsapp-success').show()
                            });
                            $("#phone-whatsapp").val('');
                            $("#formulario-whatsapp input[type='radio']").prop("checked", false);

                        } else {
                            //mensaje intentar de nuevo
                            //$('init-whatsapp').attr('onclick', 'SolicitudCTC()');
                            $('#whatsapp-alert').show();
                            $('#proceso-whatsapp').hide();
                            $('#blur-ctc').removeClass('blur-ctc');
                        }
                        console.log("Datos Enviados");
                        console.log(res);
                    },
                    error: function (xhr, textStatus, error) {
                        console.log(xhr);
                        console.log(xhr.statusText);
                        console.log(textStatus);
                        console.log(error);
                        //mensaje intentar de nuevo
                        //$('init-whatsapp').attr('onclick', 'SolicitudCTC()');
                        $('#whatsapp-alert').show();
                        $('#proceso-whatsapp').hide();
                        $('#blur-ctc').removeClass('blur-ctc');
                    },
                    beforeSend: function () {
                        var html = "";
                        html += "<div class='col-lg-12 p-3 pt-2 d-flex flex-column align-items-center justify-content-center overlay-ctc'>";
                        html += "<h3 class='m-0 titleSection text-tnks text-center'>Un momento por favor, estamos abriendo Whatsapp.</h3>";
                        html += "<h5 class='m-0 descriptionSection text-tnks mt-2 mb-2'><img src='assets/whatsapp-load.gif' class='img-fluid m-auto' style='width:100%; max-width:60px;'></h5>";
                        html += "</div>";
                        $('#blur-ctc').addClass('blur-ctc');
                        $('#proceso-whatsapp').empty();
                        $('#proceso-whatsapp').append(html);
                        $('#proceso-whatsapp').show();
                    }
                }).done(function() {
                    if(exito){
                        // window.open('https://clxt.ch/unitecg',"_blank");
                        // myWindow.open('http://google.com',"_blank");
                        redirectWindow.location;
                    }
                $("#modal-whatsapp").modal('hide');
                });
        }
    });
    $("#modal-whatsapp").on("hidden.bs.modal", function (e) {
        $('#frm-whatsapp').show(function () {
            $('#whatsapp-success').hide()
            //$('init-whatsapp').attr('onclick', 'SolicitudCTC()');
        })
    });
    $("#modal_frm_app").on("shown.bs.modal", function (e) {
        $("#botonera-gral").css("display", "none");
        $("#modal-whatsapp").css("display", "none");
    });

    $(document).on("ocultaBotonera", function (e) {
        $("#botonera-gral").css("display", "none");
        $("#modal-whatsapp").css("display", "none");
    });

    // $(document).on("notificacionWhats", function (e) {
    //     if(checkCookie('notif_whatsapp')){
    //         var numeroNotifWhats = 0;
    //     } else {
    //         var numeroNotifWhats = 1;
    //     }

    //     setTimeout(function () {
    //         if (numeroNotifWhats > 0) {
    //             document.getElementById("whatsMsj-int").innerHTML = "<div class='notificacion-int'>" + numeroNotifWhats + "</div>";
    //             $("#whatsMsj-int").addClass("whatsMsj-int");
    //             $("#openWhatsapp").click(function () {
    //                 setCookieForm('notif_whatsapp',1,1); 
    //                 var notifActual;
    //                 ($("#chatMsj div").length > 0) ? notifActual = parseInt($("#chatMsj div").text()) - numeroNotifWhats : notifActual = "";

    //                 if (document.getElementById("whatsMsj-int").innerHTML != "") {
    //                     if (notifActual == "" || notifActual == 0) {
    //                         document.getElementById("chatMsj").innerHTML = "";
    //                     } else {
    //                         document.getElementById("chatMsj").innerHTML = "<div class='notificacion'>" + notifActual + "</div>";
    //                     }
    //                     document.getElementById("whatsMsj-int").innerHTML = "";
    //                     $("#whatsMsj-int").removeClass("whatsMsj-int");
    //                 }
    //             });
    //             if ($("#chatMsj div").length > 0) {
    //                 var notifActual = parseInt($("#chatMsj div").text());
    //                 var calculoWhats = numeroNotifWhats + notifActual;
    //                 document.getElementById("chatMsj").innerHTML = "<div class='notificacion'>" + calculoWhats + "</div>";
    //             } else {

    //                 document.getElementById("chatMsj").innerHTML = "<div class='notificacion'>" + numeroNotifWhats + "</div>";
    //                 document.getElementById("whatsMsj-int").innerHTML = "<div class='notificacion-int'>" + numeroNotifWhats + "</div>";
    //                 $("#whatsMsj-int").addClass("whatsMsj-int");
    //             }
    //         } else {
    //             document.getElementById("whatsMsj-int").innerHTML = "";
    //             $("#whatsMsj-int").removeClass("whatsMsj-int");
    //         }
    //     }, 10000);
    // });

    $(window).on('load', function (e) {
        $("#modal_frm_app").on("hide.bs.modal", function (e) {
            $("#botonera-gral").removeAttr("style");
            $("#modal-whatsapp").removeAttr("style");
        });
    });
    $(window).on('load', function () {
        $(document).trigger('notificacionWhats');
    });

});

function validateNumber(event) {
    var key = window.event ? event.keyCode : event.which;
    if (event.keyCode === 8 || event.keyCode === 46) {
        return true;
    } else if (key < 48 || key > 57) {
        return false;
    } else {
        return true;
    }
}