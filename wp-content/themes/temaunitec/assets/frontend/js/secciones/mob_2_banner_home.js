/* ----------------------- Inicia codigo encargado de condumir la API webspeach ------------------- */
if (!('webkitSpeechRecognition' in window)) {
    //Navegador no soportado...
} else { 

    recognition = new webkitSpeechRecognition();
    recognition.lang = "es";

    recognition.onstart = function() {
        recognizing = true;
        console.log("comenzo a escuchar");
    }
    recognition.onresult = function(event) {
        for (var i = event.resultIndex; i < event.results.length; i++) {
            if(event.results[i].isFinal){
                document.getElementById("texto").value = event.results[i][0].transcript;
                //texto += event.results[i][0].transcript;
            }
        }
    }
    recognition.onerror = function(event) {}
    recognition.onend = function() {
        recognizing = false;
        console.log("termino de escuchar, llego a su fin");
    }
}
/* ----------------------- Termina codigo encargado de condumir la API webspeach ------------------- */
$(document).ready(function(){
    var TodasLasCarreras = new Bloodhound({
        datumTokenizer: Bloodhound.tokenizers.obj.whitespace('Categoria'),
        queryTokenizer: Bloodhound.tokenizers.whitespace,
        prefetch: '/wp-content/themes/temaunitec/assets/frontend/json/min/json_col_linkPaginas.min.json?action=autocomplete&search=%QUERY',
        remote: {
          url: '/wp-content/themes/temaunitec/assets/frontend/json/min/?action=autocomplete&search=%QUERY',
          wildcard: '%QUERY'
        }
    });
    $('.with-autocommplete').typeahead(null, {
        name: 'autocomplete-banner-carreras',
        display: 'Categoria',
        source: TodasLasCarreras.ttAdapter(),
        limit: 0,
        templates: {
          empty: [
            '<div class="empty-message">No se encontraron resultados para tu busqueda...</div>'
          ].join('\n'),
          suggestion: function(data){
              return "<div id='carrera-" + data.IdCategoria + "'><a href='" + data.Link + "'>" + data.Categoria + "</div>";
          }
        }
    });
    $('.with-autocommplete').on("keyup", function(){
        $(this).parent(".twitter-typeahead").removeAttr("style");
        $(this).css({
            "position": "relative",
            "width": "calc(100vw - 4.8rem)"
        });
        $(this).parent().siblings(".segment-icons").css({
            "position": "relative",
            "margin-left" : "calc( 100% - 24px )"
        });

        $(this).siblings(".typeahead-menu").css({
            "position": "absolute!important",
            "top": "58px",
            "left": "-0.3rem",
            "width": "calc(100% - 2.1rem)"
        });
    })

    // $(".item-banner").css({
    //     "height": ( $( ".item-banner" ).height() -  ( $(".navContent").height() + $(".nvct").height() ) )
    // });

    console.log( $( window ).height() );
});
function procesar() {
    if (recognizing == false) {
        recognition.start();
        recognizing = true;
    } else {
        recognition.stop();
        recognizing = false;

    }
}

/*Implementacion para la sección de banners Calculo de pantalla para mostrar el banner completo*/

function detectaOrientacion(){
    console.log("DetectaOrientacion");

    var orientacion = "vertical";
    var width = $(window).width();
    /*var mql = window.matchMedia("(orientation: portrait)");*/

    if (window.orientation === 0 || window.screen.orientation.angle === 0 || width < 600) {
        orientacion = "vertical";
    } else if(window.orientation === 90 || window.screen.orientation.angle === 90 || width > 600) {
        orientacion = "horizontal";
    } else{
        orientacion = "horizontal";
    }
    console.log(orientacion);
    return orientacion;
}

function ajusteBanner(orient, jsonBanner, tipobanner){
    switch(tipobanner){
        case 'imagen':
            if(orient=="vertical") {
                jQuery('#banner-imagen').css('background-image','url('+jsonBanner.vertical+')').promise().done(function(){
                    // Callback of the callback :)
                    jQuery(this).animate({
                        opacity: 1
                    }, 600)
                });

            } else if(orient=="horizontal"){
                jQuery('#banner-imagen').css('background-image','url('+jsonBanner.horizontal+')').promise().done(function(){
                    // Callback of the callback :)
                    jQuery(this).animate({
                        opacity: 1
                    }, 600)
                }); 

            } else{
                jQuery('#banner-imagen').css('background-image','url('+jsonBanner.vertical+')').promise().done(function(){
                    // Callback of the callback :)
                    jQuery(this).animate({
                        opacity: 1
                    }, 600)
                });
            }
        break;

        case 'video':
            var banner_video = document.getElementById("banner-video");

            if(orient=="vertical") {
                if ( banner_video.canPlayType("video/mp4") ) {
                    /*jQuery("#banner-video").html('<source src="'+jsonBanner.vertical+'" type="video/mp4" media="max-width: 600px">');*/
                    console.log(jsonBanner.vertical);

                    /*setTimeout(function(){
                        jQuery("#banner-video").attr({
                            'src': jsonBanner.vertical,
                            'media':'max-width: 600px'
                        });
                        banner_video.load();
                    }, 500);*/
                    
                    jQuery("#banner-video").attr({
                        'src': jsonBanner.vertical,
                        'media':'screen and (max-device-width: 600px)'
                    });
                    banner_video.load();
                } else{
                    console.log("el navegador no soporta el tipo de archivo");
                }
                /*document.getElementById("banner-video").src = banner_video.canPlayType("video/mp4") ? jsonBanner.vertical : "";*/
            } else if(orient=="horizontal") {
                if ( banner_video.canPlayType("video/mp4") ) {
                    /*jQuery("#banner-video").html('<source src="'+jsonBanner.horizontal+'" type="video/mp4" media="min-width: 600px">');*/
                    console.log(jsonBanner.horizontal);

                    /*setTimeout(function(){
                        jQuery("#banner-video").attr({
                            'src': jsonBanner.horizontal,
                            'media':'min-width: 600px'
                        });
                        banner_video.load();
                    }, 500);*/

                    jQuery("#banner-video").attr({
                        'src': jsonBanner.horizontal,
                        'media':'screen and (min-device-width: 600px)'
                    });
                    banner_video.load();
                } else{
                    console.log("el navegador no soporta el tipo de archivo");
                }
                /*document.getElementById("banner-video").src = banner_video.canPlayType("video/mp4") ? jsonBanner.horizontal : "";*/
            } else{
                if ( banner_video.canPlayType("video/mp4") ) {
                    /*jQuery("#banner-video").html('<source src="'+jsonBanner.vertical+'" type="video/mp4" media="max-width:600px">');*/
                    console.log(jsonBanner.vertical);

                    /*setTimeout(function(){
                        jQuery("#banner-video").attr({
                            'src': jsonBanner.vertical,
                            'media':'max-width: 600px'
                        });
                        banner_video.load();
                    }, 500);*/

                    jQuery("#banner-video").attr({
                        'src': jsonBanner.vertical,
                        'media':'screen and (max-device-width: 600px)'
                    });
                    banner_video.load();
                } else{
                    console.log("el navegador no soporta el tipo de archivo");
                }
                /*document.getElementById("banner-video").src = banner_video.canPlayType("video/mp4") ? jsonBanner.vertical : "";*/
            }
        break;
    }
    bannerHeight();  
    /*console.log(orient);
    console.log(banner_video);
    console.log( $(window).width() );
    console.log( $("#"+banner_id).attr("src") );*/
    
}

function ajusteBannerCarrousel(orient, jsonBanner, noSlider){
    if(orient=="vertical") {

        for (var i = 0; i < noSlider; i++) {
            jQuery('#banner-item-slider-'+ i).attr('data-src',jsonBanner.vertical[i]).promise().done(function(){
                console.log(jsonBanner.vertical[i]);
                // Callback of the callback :)
                jQuery(this).animate({
                    opacity: 1
                }, 600)
            });
        }      

    } else if(orient=="horizontal"){

        for (var i = 0; i < noSlider; i++) {
            jQuery('#banner-item-slider-'+ i).attr('data-src',jsonBanner.horizontal[i]).promise().done(function(){
                console.log(jsonBanner.horizontal[i]);
                // Callback of the callback :)
                jQuery(this).animate({
                    opacity: 1
                }, 600)
            });
        }

    } else{

        for (var i = 0; i < noSlider; i++) {
            jQuery('#banner-item-slider-'+ i).attr('data-src',jsonBanner.vertical[i]).promise().done(function(){
                console.log(jsonBanner.vertical[i]);
                // Callback of the callback :)
                jQuery(this).animate({
                    opacity: 1
                }, 600)
            });
        }

    }

    bannerHeight();
}

function bannerHeight(){
    setTimeout(function(){
        var heightNavbar = jQuery(".nvct").height();
        var height = jQuery(window).height() - parseInt(heightNavbar);  //getting windows height
        jQuery('.carousel-banner').css('height', height+'px');
        jQuery(".carousel-banner").find(".carousel-item").css('height', height+'px');   //and setting height of carousel
    }, 500);
}
/*End Implementacion para la sección de banners Calculo de pantalla para mostrar el banner completo*/