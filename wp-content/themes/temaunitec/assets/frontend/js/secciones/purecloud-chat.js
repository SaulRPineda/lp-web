var minimizado = false;
var firstValidOnline = false;
var locationchat = window.location.toString()
var pageName = document.title;
var chatSettings = {
  online:{
    Title:"¡Chatea con nosotros!",
    Message : "¡Hola! estamos para apoyarte, ingresa tus datos para iniciar el chat en vivo y te atiendan de manera personalizada.",
    dataType:"onlineForm",
    btnText: "&nbsp;&nbsp;INICIA UNA CONVERSACIÓN",
    fixedButtonText: "Chatea con nosotros"
  },
  offline:{
    Title:"¡Déjanos tu mensaje!",
    Message : "Por favor deja un mensaje y te responderemos vía correo electrónico. ¡Gracias!",
    dataType:"offlineForm",
    btnText: "&nbsp;&nbsp;ENVIAR MENSAJE",
    fixedButtonText: "Déjanos un mensaje"
  },
  gen:{
    btnSelector:"#chatButton_pc"
  }  
}
window._genesys = {
  widgets: {
    main: {     
      themes:  {
          "unitec": "cx-theme-unitec"
      },
      theme: "unitec",      
      lang: "es",      
      i18n: {
        "es": {
          "webchat": {
            "ChatButton": "Chat",
            "ChatStarted": "Chat iniciado",
            "ChatEnded": "Chat finalizado",
            "AgentNameDefault": "Agente",
            "AgentConnected": "<%Agent%> Conectado",
            "AgentDisconnected": "<%Agent%> Desconectado",
            "SupervisorNameDefault": "Supervisor",
            "SupervisorConnected": "<%Agent%> Conectado",
            "SupervisorDisconnected": "<%Agent%> Desconectar",
            "AgentTyping": "Escribiendo ...",
            "AgentUnavailable": "Lo siento. Por el momento no hay agentes disponibles. Por favor inténtalo más tarde",
            "ChatTitle": "Chat UNITEC",
            "ChatEnd": "X",
            "ChatClose": "X",
            "ChatMinimize": "Min",
            "ChatFormFirstName": "Nombre",
            "ChatFormLastName": "Apellido",
            "ChatFormNickname": "Sobrenombre",
            "ChatFormEmail": "Email",
            "ChatFormSubject": "Subject",
            "ChatFormPlaceholderFirstName": "Requerido",
            "ChatFormPlaceholderLastName": "Requerido",
            "ChatFormPlaceholderNickname": "Requerido",
            "ChatFormPlaceholderEmail": "Requerido",
            "ChatFormPlaceholderSubject": "Requerido",
            "ChatFormSubmit": "Iniciar chat",
            "ChatFormCancel": "Cancelar",
            "ChatFormClose": "Cerrar",
            "ChatInputPlaceholder": "Escribe tu mensaje aquí...",
            "ChatInputSend": "Enviar",
            "ChatEndQuestion": "¿Estás seguro de que quieres terminar esta sesión de chat?",
            "ChatEndCancel": "Cancelar",
            "ChatEndConfirm": "Cerrar chat",
            "ConfirmCloseWindow": "¿Estás seguro de que quieres cerrar el chat?",
            "ConfirmCloseCancel": "Cancelar",
            "ConfirmCloseConfirm": "Cerrar",
            "ActionsDownload": "Descargar transcripción",
            "ActionsEmail": "Transcribir email ",
            "ActionsPrint": "Transcribir impresión ",
            "ActionsCobrowseStart": "Iniciar Co-browse",
            "ActionsSendFile": "Enviar archivo",
            "ActionsCobrowseStop": "Cerrar Co-browse",
            "ActionsVideo": "Invitar al videochat",
            "ActionsTransfer": "Transferir",
            "ActionsInvite": "Invitar",
            "InstructionsTransfer": "Abre este enlace en otro dispositivo para transferir tu sesión de chat </br></br><%link%>",
            "InstructionsInvite": "Comparte este enlace con otra persona para agregarlos a esta sesión de chat. </br></br><%link%>",
            "InviteTitle": "¿Podemos ayudarte?",
            "InviteBody": "Estamos a tus órdenes para resolver tus dudas ¡Iniciemos una conversación!",
            "InviteReject": "No, gracias",
            "InviteAccept": "Iniciar chat",
            "ChatError": "Hubo un problema al iniciar la sesión de chat. Por favor, inténtalo de nuevo.",
            "ChatErrorButton": "OK",
            "DownloadButton": "Descargar",
            "FileSent": "ha enviado:",
            "FileTransferRetry": "Procesar de nuevo",
            "FileTransferError": "OK",
            "FileTransferCancel": "Cancelar",
            "RestoreTimeoutTitle": "Chat finalizado",
            "RestoreTimeoutBody": "Tu sesión de chat anterior ha expirado. ¿Te gustaría empezar uno nuevo?",
            "RestoreTimeoutReject": "No, gracias",
            "RestoreTimeoutAccept": "Iniciar chat",
            "EndConfirmBody": "¿Realmente te gustaría terminar tu sesión de chat?",
            "EndConfirmAccept": "Chat finalizado",
            "EndConfirmReject": "Cancelar",
            "SurveyOfferQuestion": "¿Te gustaría participar en una encuesta?",
            "ShowSurveyAccept": "Sí",
            "ShowSurveyReject": "No",
            "UnreadMessagesTitle": "No leído",
            "AriaYouSaid": "Tú dijiste",
            "AriaSaid": "Dijo",
            "AriaSystemSaid": "Sistema dijo",
            "AriaMinimize": "Minimizar chat",
            "AriaMaximize": "Maximizar chat",
            "AriaClose": "Cerrar chat",
            "AsyncChatEnd": "Fin del Chat",
            "AsyncChatClose": "Cerrar ventana",
            "DayLabels": [
              "Domingo",
              "Lunes",
              "Martes",
              "Miércoles",
              "Jueves",
              "Viernes",
              "Sábado"
            ],
            "MonthLabels": [
              "Enero",
              "Febrero",
              "Marzo",
              "Abril",
              "Mayo",
              "Junio",
              "Julio",
              "Agosto",
              "Septiembre",
              "Octubre",
              "Noviembre",
              "Diciembre"
            ],
            "todayLabel": "Hoy",
            "Errors": {
              "102": "Necesitamos tu nombre",
              "103": "Necesitamos tu apellido",
              "161": "Por favor ingresa tu nombre",
              "201": "El archivo no pudo ser enviado.<br/><strong><p class='filename' title='<%FilenameFull%>'>'<%FilenameTruncated%>'</p></strong><p class='cx-advice'>The maximum number of attached files would be exceeded (<%MaxFilesAllowed%>)</p>",
              "202": "El archivo no pudo ser enviado.<br/><strong><p class='filename' title='<%FilenameFull%>'>'<%FilenameTruncated%>'</p></strong><p class='cx-advice'>Upload limit and/or maximum number of attachments would be exceeded (<%MaxAttachmentsSize%>)</p>",
              "203": "El archivo no pudo ser enviado.<br/><strong><p class='filename' title='<%FilenameFull%>'>'<%FilenameTruncated%>'</p></strong><p class='cx-advice'>File type is not allowed.</p>",
              "204": "Lo sentimos pero tu mensaje es demasiado largo. Por favor, escribe un mensaje más corto",
              "240": "Lo sentimos, pero no podemos iniciar un nuevo chat en este momento. Por favor, inténtalo de nuevo más tarde",
              "364": "Dirección de correo electrónico no válida",
              "ChatUnavailable": "Lo sentimos, pero no podemos iniciar un nuevo chat en este momento. Por favor, inténtalo más tarde",
              "CriticalFault": "Tu sesión de chat ha finalizado inesperadamente debido a un problema desconocido. Te pedimos disculpas por las molestias",
              "StartFailed": "Hubo un problema al iniciar tu sesión de chat. Verifica tu conexión y que hayas enviado correctamente toda la información requerida, luego inténtalo nuevamente",
              "MessageFailed": "Tu mensaje no fue recibido con éxito. Inténtalo de nuevo",
              "RestoreFailed": "Lo sentimos, pero no pudimos restaurar tu sesión de chat debido a un error desconocido",
              "TransferFailed": "No se puede transferir el chat en este momento. Por favor, inténtalo de nuevo más tarde",
              "FileTransferSizeError": "El archivo no pudo ser enviado.<br/><strong><p class='filename' title='<%FilenameFull%>'>'<%FilenameTruncated%>'</p></strong><p class='cx-advice'>File size is larger than the allowed size (<%MaxSizePerFile%>)</p>",
              "InviteFailed": "No se puede generar invitar en este momento. Por favor, inténtelo de nuevo más tarde",
              "ChatServerWentOffline": "Los mensajes se están demorando más de lo normal para comunicarse. Lamentamos el retraso.",
              "RestoredOffline": "Los mensajes se están demorando más de lo normal para comunicarse. Lamentamos el retraso.",
              "Disconnected": "<div style='text-align:center'>Conexión perdida</div>",
              "Reconnected": "<div style='text-align:center'>Conexión restaurada</div>",
              "FileSendFailed": "El archivo no pudo ser enviado.<br/><strong><p class='filename' title='<%FilenameFull%>'><%FilenameTruncated%></p></strong><p class='cx-advice'>There was an unexpected disconnection. Try again?</p>",
              "Generic": "<div style='text-align:center'>Ocurrió un error inesperado</div>"
            }
          }
        }
      }
    },        
    webchat: {  
      form: {
        autoSubmit: false,        
        inputs: [        
          {
            id: 'cx_webchat_form_firstname',
            name: 'firstname',
            maxlength: '100',
            placeholder: 'Requerido',
            label: 'Nombre',
            validate: function(event, form, input, label, $, CXBus, Common){
              if (typeof $ !== "undefined") {
                if($(input).val()==""){
                  return false
                }else{
                  return true
                }
              }
            }
          },
          {
            id: 'cx_webchat_form_phoneNumber',
            name: 'phoneNumber',
            maxlength: '10',
            placeholder: 'Requerido',
            label: 'Celular',
            validate: function(event, form, input, label, $, CXBus, Common){
              if (typeof $ !== "undefined") {
                var pattern = /^\d+$/;        
                if($(input).val()=="" || pattern.test($(input).val())==false){
                  return false
                }else{
                  return true
                }
              }
            }
          },
          {
            id: 'cx_webchat_form_email',
            name: 'email', 
            maxlength: '100',
            placeholder: 'Requerido',
            label: 'Correo',
            validate: function(event, form, input, label, $, CXBus, Common){
              if (typeof $ !== "undefined") {
                if($(input).val()==""){
                  return false
                }else{
                  return true
                }
              }
            }
          },
          {
            id: 'cx_webchat_form_departamento', 
            name: 'customField2', 
            maxlength: '100',
            placeholder: 'Optional',
            label: 'Departamento',
            type:"select",
            options:[
              {text: 'Servicios Escolares', value: '4'},
              {text: 'Estudios en línea', value: '2'},
              {text: 'Licenciatura', value: '1'},
              {text: 'Preparatoria', value: '1'},
              {text: 'Ciencias de la Salud', value: '1'},
              {text: 'Posgrados', value: '3'},
              {text: 'Quiero titularme por seminario', value: '3'},
              {text: 'Otro', value: '1'}       
            ],
            validateWhileTyping: true,
            validate: function(event, form, input, label, $, CXBus, Common){
              //var res = validaHorario()          
              if (typeof $ !== "undefined") {            
                var todayis = new Date
                //weekday as a number (0 Domingo - 6 Sabado)
                var d = todayis.getDay();
                var h = todayis.getHours();
                var validResp = false;                                    
                var encontrado = false;
                for (var i = 1, len = Object.keys(hor).length; i < len; i++) {              
                  var departamentos = hor[i].Departamentos;                        
                  var keys = Object.entries(departamentos)
                  for (const key of keys) {                                                                
                    if (key[1][1] == $(input).val()) {                   
                       window._genesys.widgets.webchat.transport.interactionData.routing.targetAddress = hor[i].Nombre
                       encontrado = true;                   
                        if (hor[i].QueueRespaldo !== null) {                      
                          var horarioResp = hor[i].HorariosRespaldo[d]
                          for (var k = 0, len3 = horarioResp.length; k < len3; k++) {
                            var horarioqueueResp = horarioResp[k];          
                            if (h>=horarioqueueResp[0] && h<horarioqueueResp[1]) {                                    
                              window._genesys.widgets.webchat.transport.interactionData.routing.targetAddress = hor[hor[i].QueueRespaldo].Nombre
                            }
                          }
                        }
                       break;
                    }                       
                  }
                  if(encontrado){
                    break;
                  }        
                }
              }          
              return true; // or false
            }
          },
          {
            id: 'cx_webchat_form_cid',
            name: 'customerId', 
            maxlength: '100',
            placeholder: 'Requerido',
            label: 'customerId',
            type:"hidden",
            value:""
          }
        ]
      },  
      emojis: true,    
      uploadsEnabled: true,    
      confirmFormCloseEnabled: true,     
      timeFormat:12,    
      actionsMenu:true,      
      chatButton: { 
        enabled: false,        
      },
      autoInvite: {
        enabled: false,
        timeToInviteSeconds: 30,
        inviteTimeoutSeconds: 30
      },
      minimizarOnMobileRestore: false,
      transport: {
        type: 'purecloud-v2-sockets',
        dataURL: 'https://api.mypurecloud.com',
        deploymentKey : '6a4d704d-3a59-42ec-bd4b-c834a9b18cec',
        orgGuid : '8f0acef1-6aa3-48fb-b5b0-86e64e8c5a51',
        interactionData: {
          routing: {
            targetType: 'QUEUE',
            targetAddress: 'CHAT',
            priority: 8
          }
        }
      },
      userData: {
        //"customerId": "",
        "customField2Label": "Departamento",
        "phoneType" : "Cell"
      }
    },
    onReady: function(){
      console.warn("genesys ready")
      setTimeout(function() {
        if (firstValidOnline) {
          UnitecChat.command('WebChat.invite').done(function(e){
            $( ".cx-common-container.cx-toast .cx-button-container .cx-btn-primary" ).click(function(){
              dataLayer.push({
                'event': 'Activador Dinamico',
                'category': 'Chat',
                'accion': 'intencion',
                'label': 'con formulario',
                'location': 'Invite(Trigger)'
              });
              // dataLayer.push({
              //   "event": "iniciarChattrigger",
              //   "accion": "intencion",
              //   "etiqueta": "con formulario",
              //   "section": "Invite(Trigger)"
              // });
            })                      
          }).fail(function(e){});
        }              
      }, window._genesys.widgets.webchat.autoInvite.timeToInviteSeconds*1000);     
    }
  }
};

const UnitecChat = CXBus.registerPlugin('Custom');
var deptosel;
UnitecChat.subscribe('WebChat.submitted', function (e) {  
  //console.warn('Chat submitted', e);    
  deptosel = e.data.form.customField2
  dataLayer.push({
    'event': 'Activador Dinamico',
    'category': 'Chat',
    'accion': 'llenado de formulario',
    'label': 'solicitar conversacion',
    'location': 'formulario online'
  });
  // dataLayer.push({
  //   'event': 'Click',
  //   'category': 'Chat',
  //   'action': 'llenado de formulario',
  //   'label': 'Solicitar Conversación'
  // });
  
  var datafrmonline = {
    "action":"chat_HS",
    "name-chat":e.data.form.firstname,
    "phone-chat":e.data.form.phoneNumber,
    "email":e.data.form.email,
    "cid":window._genesys.widgets.webchat.userData.customerId,
    "depto": deptosel,
    "tipoForm":"online",
    "urlreferrer":window.location.toString(),
    "pageName":pageName
  };
  $.ajax({
    type: "POST",
    url: "/wp-content/themes/temaunitec/assets/html/Purecloud/horariosChat.php",
    data: datafrmonline,
    success: function (res) {            
      console.log(res);            
      res = JSON.parse(res);
      if (res.success) {        
      }else{
        console.log("try again")            
      }          
    },
    error: function (xhr, textStatus, error) {
      console.log(xhr);
      console.log(xhr.statusText);
      console.log(textStatus);
      console.log(error);
    },
    beforeSend: function () {          
    },
    complete: function () {
        
    }
  });
});
UnitecChat.subscribe('WebChatService.started', function (e) {  
  //console.warn('Chat started', e);
  dataLayer.push({
    'event': 'leadGenerationChat',//dato est·tico 
    'accion': 'chat iniciado',
    'etiqueta': 'apertura aplicacion chat',
    'section': 'formulario chat online', //dato din·mico 
    'Departamento': deptosel
  })
});
UnitecChat.subscribe('WebChat.minimized', function (e) {    
  minimizado=true;
});
UnitecChat.subscribe('WebChatService.ended', function (e) {
  //console.log('Chat ended', e);
});
UnitecChat.subscribe('WebChat.ready', function(e){
  //console.warn("ready: ")  
});
UnitecChat.subscribe('WebChat.opened', function(e){
  //console.warn('Chat open', e);
  $('.cx-titlebar > .cx-icon').addClass('icon-u-chat');
//  $(chatSettings.gen.btnSelector).removeClass('d-flex').addClass('d-none')
//  $("#chatButton_pc .notificacion-chat-purecloud").remove()
  var todayis = new Date
  //weekday as a number (0 Domingo - 6 Sabado)
  var d = todayis.getDay();
  var h = todayis.getHours();
  var deptoavailable = [];
  for (var i = 1, len = Object.keys(hor).length; i < len; i++) {
    var horario = hor[i].Horarios[d];          
    for (var j = 0, len2 = horario.length; j < len2; j++) {
      var horarioqueue = horario[j];          
      if (h>=horarioqueue[0] && h<horarioqueue[1]) {                        
        const entries = Object.entries(hor[i].Departamentos)
        deptoavailable = deptoavailable.concat(entries);
      }
    }          
  } 
  $("#cx_webchat_form_departamento").empty()              
  deptoavailable=deptoavailable.sort() 
  deptoavailable.forEach(function(element) {    
    $("#cx_webchat_form_departamento").append('<option text="'+element[1][1]+'" value="'+element[1][1]+'">'+element[1][1]+'</option>')
  }); 
  var trackers = ga.getAll();
  var x, lenx;
  var cid
  for (x = 0, lenx = trackers.length; x < lenx; x += 1) {
    if (trackers[x].get('trackingId') === 'UA-9351414-14') {
      cid = trackers[x].get('clientId');
      break;
    }
  }
   $("#cx_webchat_form_cid").val(cid)
});
UnitecChat.subscribe('WebChat.closed', function(e){ /* sample code */ 
  //console.warn('Chat closed', e);
//  $(chatSettings.gen.btnSelector).removeClass('d-none').addClass('d-flex')
  minimizado=false;
});
function openChatLive() {  
  abrirchat()
}
function validarform(){
  var errors = 0;
  $(".invalid-feedback").removeClass('d-block')      
  var vnom= genValidaNombres( $("#name-chat")[0] );       
  if (vnom.fieldError){
    $("#name-chat").siblings( ".myFeedBackError" ).remove()
    $("#name-chat").after(
      '<div class="myFeedBackError">\
      '+decodeURIComponent(vnom.customErrorMessage)+'\
      </div>'
    )
    errors++
  }else{
    $("#name-chat").siblings('.myFeedBackError').remove()
  }  
  var vcel= genvalidaCelular( $("#phone-chat").val() );       
  if (vcel.fieldError){
    $("#phone-chat").siblings( ".myFeedBackError" ).remove()
    $("#phone-chat").after(
      '<div class="myFeedBackError">\
      '+decodeURIComponent(vcel.customErrorMessage)+'\
      </div>'
    )
    errors++
  }else{
    $("#phone-chat").siblings('.myFeedBackError').remove()
  }
  var vmail= genvalidaEmail( $("#email-chat").val() );       
  if (vmail.fieldError){
    $("#email-chat").siblings( ".myFeedBackError" ).remove()
    $("#email-chat").after(
      '<div class="myFeedBackError">\
      '+decodeURIComponent(vmail.customErrorMessage)+'\
      </div>'
    )
    errors++
  }else{
    $("#email-chat").siblings('.myFeedBackError').remove()
  }
  if ($(".inp-depto .dropdown-item.sel").length==0){
    $(".inp-depto").siblings( ".myFeedBackError" ).remove()
    $(".inp-depto").after(
      '<div class="myFeedBackError">\
      Selecciona un departamento\
      </div>'
    )
    errors++
  }else{
    $(".inp-depto").siblings('.myFeedBackError').remove()
  }           
  if ($("#msj-chat").val().length == 0){
    $("#msj-chat").siblings( ".myFeedBackError" ).remove()
    $("#msj-chat").after(
      '<div class="myFeedBackError">\
      Escribe un mensaje\
      </div>'
    )
    errors++
  }else{
    $("#msj-chat").siblings('.myFeedBackError').remove()
  }
  if (errors==0) {
    return true
  }
}
function abrirchat(){
  var todayis = new Date
  //weekday as a number (0 Domingo - 6 Sabado)
  var d = todayis.getDay();
  var h = todayis.getHours();
  var validOnline = false;
  var sett = chatSettings.offline
  var deptoavailable = [];
  var horCorr = hor.general[d]    
  var newoptions = [];
  for (var i = 0, len = horCorr.length; i < len; i++) {          
    var horario = horCorr[i];          
    if (h>=horario[0] && h<horario[1]) {
      validOnline =  true        
      break;
    }
  }    
  /*Lanzamos el Chat correspondiente dependiendo del Horario de Atención By SRP 29-07_2019*/
  if (validOnline) {
    UnitecChat.command('WebChat.open')
    sett = chatSettings.online
  }else{
    openModal('#modal-chat')
  }  
}
$(document).ready(function () {

  var todayis = new Date
  //weekday as a number (0 Domingo - 6 Sabado)
  var d = todayis.getDay();
  var h = todayis.getHours();
  var validOnline = false;
  var sett = chatSettings.offline  
  /*Comprobar horario para lanzar el chat correspondiente By SRP 25-07-2019*/
  $.ajax({
    type: "GET",
    url: "/wp-content/themes/temaunitec/assets/html/Purecloud/horariosChat.php",
    data: {
      action: "chat_rules"        
    },
    dataType: "json",
    success: function (res) {
/*      var para = document.createElement("a");
      para.setAttribute("id", "chatButton_pc");      
      para.setAttribute("class", "cardBtnChat d-flex align-items-center justify-content-between");
      para.setAttribute("data-gtm-tr", "chat");
      para.setAttribute("data-gtm-pos", "clic");
      para.setAttribute("data-gtm-location", "boton fijo");
      para.setAttribute("data-gtm-etiqueta", "con formulario");  */                  
      hor = res.data  
      var horCorr = hor.general[d]    
      var newoptions = [];
      for (var i = 0, len = horCorr.length; i < len; i++) {          
        var horario = horCorr[i];          
        if (h>=horario[0] && h<horario[1]) {
          validOnline =  true        
          break;
        }
      }    
      if (validOnline) {     
        firstValidOnline = true;   
        sett = chatSettings.online
      }
/*      para.setAttribute("onclick", "abrirchat()");              
      para.innerHTML = '<div id="draggable-snap-2" class="px-1">\
                        <div class="d-flex align-items-center justify-content-around h-100" style="width:250px; color:#fff;padding-right: 10px;">\
                          <i class="icon-u icon-u-chat "></i>\
                          <span class="ml-3">'+sett.fixedButtonText+'</span>\
                        </div>\
                    </div>';
      document.body.insertBefore(para, document.body.firstChild);*/
    },
    error: function (xhr, textStatus, error) {
    },
    beforeSend: function () {          
    },
    complete: function () {          
    }
  });
  var zp = function(){
    var self = this    
    self.livechat = {
      window:{
        show: function(){
          if (minimizado) {
            UnitecChat.command('WebChat.minimize',{minimized:false}).done(function(e){            
            }).fail(function(e){                           
            });
          }else{
            abrirchat();  
          }          
        }
      }
    }
  }
  $zopim = new zp();  
  $("#openChat").click(function() {
    abrirchat();
  })
  $('#name-chat').on( "keyup", function(e){            
    var val = genValidaNombres($(this)[0])    
    if (val.fieldError){
      $(this).siblings( ".myFeedBackError" ).remove()
      $(this).after(
        '<div class="myFeedBackError">\
        '+decodeURIComponent(val.customErrorMessage)+'\
        </div>'
      )
    }else{
      $(this).siblings('.myFeedBackError').remove()
    }
  }); 
  $(".inp-depto .dropdown-item").click(function(){
    $(".inp-depto .dropdown-item").removeClass('sel');
    var StateName = $(this).text();
    $("#depto-chat").text(StateName);
    $(this).addClass('sel');
    //nuevoChat.validUX();
  })

  var substringMat = function (strs) {
        return function findMatches(q, cb) {
          var matches, substringRegex;
          matches = [];
          var substrRegex = new RegExp(q, "i");
          $.each(strs, function (i, str) {
            if (substrRegex.test(str)) {
              matches.push(str);
            }
          });
          cb(matches);
        };
      };
  var domains = [
        "hotmail.com",
        "yahoo.com.mx",
        "msn.com",
        "yahoo.com",
        "gmail.com",
        "outlook.com",
        "live.com.mx",
        "live.com",
        "prodigy.net.mx",
        "icloud.com"
      ];
  $("#email-chat").typeahead(
    {
      hint: true,
      highlight: true,
      minLength: 1
    },
    {
      name: "domains",
      source: substringMat(domains)
    }
  );
  $("#email-chat").on('typeahead:selected', function (evt, item) {                
    var elements = $("#email-chat").val().split("@")        
    $('#email-chat').typeahead('val', item);     
    $('#depto-chat').focus();
  });
  $("#email-chat").on("focusin",function(){
    $("#email-chat").parent().parent().siblings("label").addClass("active");
  })
  $("#init-chat").click(function(){
    if(validarform()){
      var trackers = ga.getAll();
      var i, len;
      var cid
      for (i = 0, len = trackers.length; i < len; i += 1) {
        if (trackers[i].get('trackingId') === 'UA-9351414-14') {
          cid = trackers[i].get('clientId');
        }
      }
      var depto = $(".inp-depto .dropdown-item.sel").text();
      var deptoqueue = $(".inp-depto .dropdown-item.sel").data("queue");
      var tipoForm = "offline";
      $.ajax({
        type: "POST",
        url: "/wp-content/themes/temaunitec/assets/html/Purecloud/horariosChat.php",
        data: 'action=chat_HS&'+$("#formulario-chats").serialize()+'&cid='+cid+'&depto='+depto + '&deptoqueue='+deptoqueue+'&tipoForm='+tipoForm+'&urlreferrer='+window.location.toString()+"&pageName="+pageName
        ,
        success: function (res) {            
          console.log(res);            
          res = JSON.parse(res);
          if (res.success) {
            console.log("saved");           
            dataLayer.push({
              'event': 'leadGenerationChat',//dato est·tico    
              'accion': 'envio de mensaje',   
              'etiqueta' : 'mensaje offline', 
              'section': 'formulario chat offline', //dato din·mico 
              'Departamento': depto
            })
            $("#formulario-chats").hide()
            $("#frm-chat .chat-title-text").text("¡Gracias por comunicarte con nosotros!")
            $("#frm-chat .chat-message-text").text("Pronto nos pondremos en contacto contigo.")
            $("#msg-svg").removeClass('d-none');            
          }else{
            console.log("try again")            
          }          
        },
        error: function (xhr, textStatus, error) {
          console.log(xhr);
          console.log(xhr.statusText);
          console.log(textStatus);
          console.log(error);
        },
        beforeSend: function () {          
        },
        complete: function () {
            
        }
      });
    }
  })
});