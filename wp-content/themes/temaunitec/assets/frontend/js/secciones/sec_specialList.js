$(document).ready(function(){
    specialList = $(".specialList");
    itemsMin = 5;
    $plus = '<i class="icon-u icon-u-ver-mas"></i>';
    $minus = '<i class="icon-u icon-u-ver-menos"></i>';

    for( list = 0; list < specialList.length; list++ ){
        if( $(specialList[list]).children("div").length > itemsMin ){
            $(specialList[list]).children("div:nth-child(+n+6)").hide();
            $(specialList[list]).append('<a class="btn-floating special waves-effect waves-light">' + $plus + '</a>');
        }
    }

    $("a.special").on("click", function(){
        list = $(this).parent();
        console.log( list.attr("class") );
        if( list.attr("class").indexOf("show") > -1 ){
            list.removeClass("show");
            list.children("div:nth-child(+n+6)").hide();
            $(this).html($plus);
        }else{
            list.addClass("show")
            list.children("div:nth-child(+n+6)").show();
            $(this).html($minus);
        }
    });
});