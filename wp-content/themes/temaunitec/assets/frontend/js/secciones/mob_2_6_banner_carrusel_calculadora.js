jQuery(document).ready(function(){
    window.onorientationchange = orientacionDispositivo; 
    orientacionDispositivo();
});

function orientacionDispositivo() {

    if (Math.abs(window.orientation) === 90) {
        // Landscape
        //document.getElementById("orientation").innerHTML = "LANDSCAPE";
        var tipo= "horizontal";
    } else {
        // Portrait
        var tipo= "vertical";
        //document.getElementById("orientation").innerHTML = "PORTRAIT";
    }
    var orient=tipo;
    ajusteBannerCarrousel(orient, jsonBannerCarrusel, noSlider);
    bannerHeight();
}
