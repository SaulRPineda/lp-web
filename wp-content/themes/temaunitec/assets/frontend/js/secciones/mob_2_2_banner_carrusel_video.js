jQuery('document').ready(function(){
        
    var orient = detectaOrientacion();
    ajusteBanner(orient, jsonBannerVideo, 'video');
    bannerHeight();
    
});

// Listen for orientation changes
window.addEventListener("orientationchange", function() {
// Announce the new orientation number
    orient = detectaOrientacion();
    ajusteBanner(orient, jsonBannerVideo, 'video');
    bannerHeight();
}, false);