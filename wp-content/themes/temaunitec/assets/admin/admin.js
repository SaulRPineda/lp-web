jQuery.noConflict();

var UNITEC_ADMIN;

(function($) {
    UNITEC_ADMIN = {

        post_data: function(data) {

            $('.spinner').css('display', 'inline-block');

            
            
            $.post(ajaxurl, data, function() {
                $('.spinner').hide();
            });
        }

    };
})(jQuery);

jQuery(document).ready(function($) {


    var data = {
        action: 'update-page-section-order',
        security: $('#page_sections_noncename').val(),
        page_id: $('#single_page_id').val()
    };



    /*****DRAGABLE PARA DESK*****/
    jQuery("#sortable1").children('li').draggable({
        'cursor': 'pointer',
        zIndex: 100,
        helper: 'clone',
        distance: 2,
        connectToSortable: "#sortable2",
        containment: '#order-post-type',
        start: function(event, ui) {
            var width = $('#order-post-type #sortable2 li').first().width();
            $('#sortable1 li.ui-draggable-dragging').css('max-width', width + 'px');
        },
        stop: function(event, ui){
           // alert('stop');
        }
    });

    jQuery("#sortable2").sortable({
        'tolerance': 'intersect',
        'cursor': 'pointer',
        'items': 'li',
        'placeholder': 'placeholder',
        stop: function(event, ui) {
            console.log(data);
            console.log('encivando');
           // alert("guadando valores");
            data['order'] = jQuery("#sortable2").sortable("serialize", {
                attribute: "rel"
            });
            data['tipo']="desk";
            data['add_or_del']='add';
            UNITEC_ADMIN.post_data(data);
        },
        /*receive: function(event, ui) {
            alert("guadando valores");
            UNITEC_ADMIN.post_data(data);
        }*/
    });

    jQuery("#sortable2").disableSelection();

    /*****DRAGABLE PARA MOBILE*****/
    jQuery("#sortable1m").children('li').draggable({
        'cursor': 'pointer',
        zIndex: 100,
        helper: 'clone',
        distance: 2,
        connectToSortable: "#sortable2m",
        containment: '#order-post-typem',
        start: function(event, ui) {
            var width = $('#order-post-typem #sortable2m li').first().width();
            $('#sortable1m li.ui-draggable-dragging').css('max-width', width + 'px');
        },
        stop: function(event, ui){
           // alert('stop');
        }
    });

    jQuery("#sortable2m").sortable({
        'tolerance': 'intersect',
        'cursor': 'pointer',
        'items': 'li',
        'placeholder': 'placeholder',
        stop: function(event, ui) {
            console.log(data);
            console.log('encivando');
            //alert("guadando valores");
            data['tipo']="mob";
            data['order'] = jQuery("#sortable2m").sortable("serialize", {
                attribute: "rel"
            });
            UNITEC_ADMIN.post_data(data);
        },
        /*receive: function(event, ui) {
            alert("guadando valores");
            UNITEC_ADMIN.post_data(data);
        }*/
    });

    jQuery("#sortable2m").disableSelection();








    /*------- Open/close action -------*/

    $('#sortable2, #sortable1, #sortable2m, #sortable1m').on('click', 'a.block-edit, .block-control-actions a.close', function(event) {
        var block_element = $(this).closest("li");
        block_element.find('.block-settings').slideToggle('fast');
        if (block_element.hasClass('block-edit-active') == false) {
            block_element.addClass('block-edit-active');
        } else {
            block_element.removeClass('block-edit-active');
        };

        return false;

    });

    $('#sortable2').on('click', '.block-control-actions a.remove', function(event) {
//alert("aaa");
        var block_element = $(this).closest("li");

        block_element.remove();
        
         data['order'] = jQuery("#sortable2").sortable("serialize", {
                attribute: "rel"
            });
            data['tipo']="desk";
            data['add_or_del']="del";
        console.log("borando");
        console.log(data);

        UNITEC_ADMIN.post_data(data);

        return false;

    });
     $('#sortable2m').on('click', '.block-control-actions a.remove', function(event) {
//alert("bbb");
        var block_element = $(this).closest("li");

        block_element.remove();
    
         data['order'] = jQuery("#sortable2m").sortable("serialize", {
                attribute: "rel"
            });
            data['tipo']="mob";
        console.log("borando");
        console.log(data);

        UNITEC_ADMIN.post_data(data);

        return false;

    });


}); // document ready