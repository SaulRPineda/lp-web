<?php

function getHubspotContact($email,$properties=array(),$propMode="value_only",$submMode="none",$showList="false"){	
	$prop = "";
	$HAPIKey = 'a02f7942-c782-4a98-9bf2-1e8ede4fca3b';
	if($properties){
		$prefixed_array = preg_filter('/^/', '&property=', $properties);
		$prop = implode("", $prefixed_array);
	}
    $endpoint = 'https://api.hubapi.com/contacts/v1/contact/email/'.$email.'/profile?hapikey='.$HAPIKey.$prop."&propertyMode=".$propMode."&formSubmissionMode=".$submMode."&showListMemberships=".$showList;
  $ch = curl_init();
  curl_setopt($ch, CURLOPT_URL, $endpoint);
  curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
  curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
  //curl_setopt($ch, CURLOPT_USERAGENT, $this->userAgent);    // new
  $output = curl_exec($ch);
  $errno = curl_errno($ch);
  $error = curl_error($ch);
  $http_status = curl_getinfo($ch, CURLINFO_HTTP_CODE);
  //$this->setLastStatusFromCurl($ch);
  curl_close($ch);
//var_dump($output);
  if ( $errno > 0 ){
      //throw new HubSpot_Exception('cURL error: ' . $error);  	
  	return false;
  }else{
  	if ($http_status==200) {
  		return $output;
  	}else{
  		return false;
  	}
	}	
  
}
if(!isset($_SESSION['getEmail'])){
    $correo = $_GET['email'];
    if ($correo != '') {
        $HSContact = getHubspotContact($correo,array('email','firstname','lastname','campus','nopersona','subnivelinteres'));
        if ($HSContact !== false) {          
          $existeHS = true;
          $contactProps = json_decode($HSContact,true);
          $email = $contactProps['properties']['email']['value'];
          $nombre = $contactProps['properties']['firstname']['value'];
          $lastname = $contactProps['properties']['lastname']['value'];
          $campus = $contactProps['properties']['campus']['value'];
          $campusHbpst = $contactProps['properties']['campus']['value'];                
          $noPersona = $contactProps['properties']['nopersona']['value'];
          $subnivel = $contactProps['properties']['subnivelinteres']['value'];          
          $publicContactURL = $contactProps['profile-url'];
          $contactProps['existeHS'] = true;
        }
        else{
            $contactProps['existeHS'] = false;
        }
    }
    echo json_encode($contactProps);
	die();
}  
?>