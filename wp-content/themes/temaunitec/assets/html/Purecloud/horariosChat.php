<?php

if (isset($_REQUEST['action'])) {
	$action = $_REQUEST['action'];	
	$action();
}else{
	exit();
}

function chat_rules()
{		
	$horarios = obtenerQueueParams();					
	$horariosq1 = obtenerQueueParams(1);
	$horariosq2 = obtenerQueueParams(2);
	$horariosq3 = obtenerQueueParams(3);
	$horariosq4 = obtenerQueueParams(4);

	$data_array = ["success"=>true,"data"=>["general"=>$horarios["Horarios"],"1"=>$horariosq1,"2"=>$horariosq2,"3"=>$horariosq3,"4"=>$horariosq4]];
  	echo json_encode($data_array);
  	//echo json_encode($horarios,true);  	
	die(); 
}
function obtenerQueueParams($queue=""){
	switch ($queue) {
		case 1:
			$horarios["Nombre"] = "CHAT";
			$horarios["QueueRespaldo"] = null;
			$horarios["Horarios"] = array(0=>[[10,14]],
				1=>[[8,24]],
				2=>[[0,2],[8,24]],
				3=>[[0,2],[8,24]],
				4=>[[0,2],[8,24]],
				5=>[[0,2],[8,24]],
				6=>[[9,19]]				
				);
			$horarios["Departamentos"] = array("0" =>[1,'Licenciatura'], "1" => [1,'Preparatoria'], "2" =>[1,'Ciencias de la Salud'],"7" => [1,'Sin departamento']);
		break;
		case 2:
			$horarios["Nombre"] = "CHAT ONLINE";
			$horarios["QueueRespaldo"] = null;
			$horarios["Horarios"] = array(0=>[[10,14]],
				1=>[[8,24]],
				2=>[[0,2],[8,24]],
				3=>[[0,2],[8,24]],
				4=>[[0,2],[8,24]],
				5=>[[0,2],[8,24]],
				6=>[[9,19]]				
				);
			$horarios["HorariosRespaldo"] = array(0=>[],
				1=>[[21,24]],
				2=>[[0,2],[21,24]],
				3=>[[0,2],[21,24]],
				4=>[[0,2],[21,24]],
				5=>[[0,2],[21,24]],
				6=>[[14,19]]				
				);		
			$horarios["Departamentos"] = array("4"=>[2,'Estudios en línea']);
		break;
		case 3:
			$horarios["Nombre"] = "CHAT INTERNO";
			$horarios["QueueRespaldo"] = 1;
			$horarios["Horarios"] = array(0=>[[10,14]],
				1=>[[8,24]],
				2=>[[0,2],[8,24]],
				3=>[[0,2],[8,24]],
				4=>[[0,2],[8,24]],
				5=>[[0,2],[8,24]],
				6=>[[9,19]]				
				);			
			$horarios["HorariosRespaldo"] = array(0=>[],
				1=>[[21,24]],
				2=>[[0,2],[21,24]],
				3=>[[0,2],[21,24]],
				4=>[[0,2],[21,24]],
				5=>[[0,2],[21,24]],
				6=>[[14,19]]				
				);		
			$horarios["Departamentos"] = array("5"=>[3,'Posgrados'],"6"=>[3,'Quiero titularme por seminario']);
		break;
		case 4:
			$horarios["Nombre"] = "CHAT REGRESOS";
			$horarios["QueueRespaldo"] = 1;
			$horarios["Horarios"] = array(0=>[[10,14]],
				1=>[[8,24]],
				2=>[[0,2],[8,24]],
				3=>[[0,2],[8,24]],
				4=>[[0,2],[8,24]],
				5=>[[0,2],[8,24]],
				6=>[[9,19]]				
				);		
			$horarios["HorariosRespaldo"] = array(0=>[],
				1=>[[21,24]],
				2=>[[0,2],[21,24]],
				3=>[[0,2],[21,24]],
				4=>[[0,2],[21,24]],
				5=>[[0,2],[21,24]],
				6=>[[14,19]]				
				);		
			$horarios["Departamentos"] = array("3"=>[4,'Servicios Escolares']);
		break;
		default:
			$horarios["Nombre"] = "";
			$horarios["QueueRespaldo"] = "";
			$horarios["Horarios"] = array(0=>[[10,14]],
				1=>[[8,24]],
				2=>[[0,2],[8,24]],
				3=>[[0,2],[8,24]],
				4=>[[0,2],[8,24]],
				5=>[[0,2],[8,24]],
				6=>[[9,19]]				
				);				
		break;
	}	
	return $horarios;
}
function chat_HS()
{	
	ini_set('display_errors',0);
	error_reporting(0);	
	date_default_timezone_set('America/Mexico_City');
	$nombre = $_POST['name-chat'];
	$celular = $_POST['phone-chat'];
	$correo = $_POST['email'];
	$mensaje = isset($_POST['msj-chat'])?$_POST['msj-chat']:"";
	$cid = $_POST['cid'];
	$depto = $_POST['depto'];
	$urlreferrer = $_POST['urlreferrer'];
	$pageName = $_POST['pageName'];
	$tipoForm = $_POST['tipoForm'];    
    if ($tipoForm == "online") {    
	    $data['msgoff'] = trim("chat_online");
    }elseif ($tipoForm == "offline") {    	
	    $data['msgoff'] = trim("chat_offline");	    
    }
    $link = (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on' ? 
            "https" : "http") . "://" . $_SERVER['HTTP_HOST'] ;
	$data['nombre'] = trim( $nombre );
    $data['mobilephone'] = trim($celular );
    $data['email'] = trim($correo);
    $data['depto'] = $depto;
    $data['mensaje_chat'] = trim($mensaje);
    $data['cid'] = trim($cid);
    $data['urlreferrer'] = trim($urlreferrer);
    $data['pageName'] = trim($pageName);
    $responsews = file_get_contents($link."/desk/procWeb/microregistroChat.php"."?".http_build_query($data) );
    $response = array('success' =>true ,'data'=> array('response' => "") );
	echo json_encode($response);
	die(); 
}
?>	