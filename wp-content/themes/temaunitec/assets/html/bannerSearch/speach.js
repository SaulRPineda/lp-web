    
    var recognition;
    var recognizing = false;
    if (!('webkitSpeechRecognition' in window)) {
        alert("¡API no soportada!");
    } else {

        recognition = new webkitSpeechRecognition();
        recognition.lang = "es";
      

        recognition.onstart = function() {
            recognizing = true;
            console.log("empezando a escuchar");
        }
        recognition.onresult = function(event) {
            var texto = "";
            for (var i = event.resultIndex; i < event.results.length; i++) {
                if(event.results[i].isFinal){
                    document.getElementById("texto").value = event.results[i][0].transcript;
                    texto += event.results[i][0].transcript;
                    buscarGCS();
                }
            }
            
        }
        recognition.onerror = function(event) {}
        recognition.onend = function() {
            recognizing = false;
            document.getElementById("procesar").innerHTML = "M--";
            console.log("terminó de escuchar, llegó a su fin");
        }
    }
    
    $(document).ready(function(){
        $("#procesar").on("click", function(){
            procesar();
            return false;
        });
        $("#btnGCScerrar").on('click', function(){
            $("#resultados").css({ display: "none" });
        })
    })

    function procesar() {
        if (recognizing == false) {
            recognition.start();
            recognizing = true;
            document.getElementById("procesar").innerHTML = "M++";
        } else {
            recognition.stop();
            recognizing = false;
            document.getElementById("procesar").innerHTML = "M--";
        }
    }

    function buscarGCS(){
        var texto = document.getElementById("texto").value;
        var filtro = document.getElementById("filtro").value;
        filtroCategoria = "";
        if( (filtro == "-- --") || (filtro == "") || (filtro == "null") ){
            filtroCategoria = "";
        }else{
            filtroCategoria = "+more:pagemap:document-categoria:" + filtro;
        }

        var urlGoogleCustomSearch = "https://www.googleapis.com/customsearch/v1?key=AIzaSyDaXorUVaC5oa53KwL4hnmx7NQDG82zC4g&cx=002268575599804991473:n-g-9uohyde&q={" + texto + "}" + filtroCategoria;

        $.ajax({
            //url : urlGoogleCustomSearch,
            url: "mockup.json",
            type: "GET",
            success: function( res ){
                if( Object.keys( res ).indexOf( "items" ) >= 0 ){
                    $("#resultados").css({ display: "block" });
                    $("#resultados div").html( solveItems( res ) );
                    console.log("El valor si exite, en estas coincidencias: ");
                }else{
                    console.log("el valor no existe");
                }
            }
        }); 

        function solveItems( items ){
            var res = "<div>";
            for( item = 0; item < items["items"].length; item++){
                res += "<a href='" + items["items"][item]["link"] + "'>" +
                       "<h4 style='color:blue'>" + items["items"][item]["htmlTitle"] + "</h4>"+
                       "</a>" +
                       "<p style='color:green'>" + items["items"][item]["htmlTitle"] + "</p>"+
                       "<p style='color:black'>" + items["items"][item]["snippet"] + "</p>";
            }
            return res += "</div>";
        }
    }



    