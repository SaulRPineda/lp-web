<?php

    class BibliotecaVirtual{

        public function __construct( ){}

        public function redirecciona( $dataAjax ){
            $seccion = $dataAjax["seccion"];
            unset( $dataAjax["seccion"] );
            //De esta manera pasare valores
            //$dataAjax["respuesta"] = "Esta respuesta va desde Biblioteca Virtual";
            
            switch( $seccion ){
                case 'login':
                    include_once "Login.php";
                    $login = new Login( $dataAjax );
                    $logeado = $login->logeame(  );
                    return  $logeado ;
                break;
            }
            return;
        }

    }