<?php

    class Login{

        private $dataAjax;
        private $correo;
        private $cuenta;
        private $origen;
        private $perfil;
        private $usuario;

        private $connection;

        public function __construct( $dataAjax ){
            $this->dataAjax = $dataAjax;
            $this->correo = $dataAjax["correo"];
            $this->cuenta = $dataAjax["cuenta"];
            $this->origen = $dataAjax["origen"];
            $this->perfil = $dataAjax["perfil"];
            $this->usuario = $dataAjax["usuario"];

            $this->dataAjax = $this->forgetDataInt();
        }

        public function logeame( ){
            $userValData = [];

            //echo "No Usr: ". count($this->validateMe() );

            if( count( $this->validateMe() ) > 0){
                $userValData = $this->validateMe();

                // setcookie('perfil', $this->perfil, time() + 3600, '/');
                // setcookie('nombre', $userValData[0]["nombre"], time() + 3600, '/');

                $perfil = $this->getPerfil( $this->perfil );

                $this->dataAjax[ 'biblioteca_status'	] = $this->origen;
                $this->dataAjax[ 'biblioteca_libri'		] = $this->biblioteca_libri( $this->usuario );
                $this->dataAjax[ 'biblioteca_pearson'   ] = $this->biblioteca_pearson( $this->usuario );
                $this->dataAjax[ 'biblioteca_cengage'	] = $this->biblioteca_cengage( $this->usuario );
                $this->dataAjax[ 'biblioteca_ebrary'	] = $this->biblioteca_ebrary( $this->usuario );
                $this->dataAjax[ 'biblioteca_kivuto'    ] = $this->biblioteca_kivuto( $this->usuario, $this->cuenta );
                $this->dataAjax[ 'biblioteca_buscador'    ] = '<main class="col-12">
                                                                <section id="elegant-card">
                                                                    <div class="row">
                                                                        <div class="col-md-12">
                                                                            <div class="card">
                                                                                <h4 class="card-title">Búsqueda Integral</h4>                    
                                                                                <div class="card-block pb16">
                                                                                    <div class="row">
                                                                                        <div class="col-12">
                                                                                            <h4 class="card-content-title">                                        
                                                                                            </h4>
                                                                                            <p class="card-text">Bienvenido al servicio de busqueda integrada, tendrás la oportunidad de conocer la literatura sin salir de casa</p>
                                                                                            <input type="search" id="input-busqueda-integrada" placeholder="  Buscar ..." class="busqueda-integrada"
                                                                                            style="border: 1px solid #006fba;" autocomplete="off" (keydown.enter)="busqueda_integrada($event)">
                                                                                            <span class="icon-u-microfono microfono"></span>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                            </div>                
                                                                        </div>
                                                                    </div>
                                                                </section>
                                                            </main>';

                $this->dataAjax[ 'nombre'    ] = $this->usuario;
                $this->dataAjax[ 'perfil'    ] = $this->perfil;
                $this->dataAjax[ 'cuenta'    ] =  $this->cuenta;

                $this->dataAjax[ 'titulo_menu_serv'		] = $perfil['submenu'];

                $this->dataAjax["respuesta"] = 1;

            }else{
                $this->dataAjax["estaus"] = "Usuario no valido...";
            }

            return $this->dataAjax;
        }

        private function validateMe(){
            include_once 'config.php';
            /*$theMail = ( $this->perfil == 'profesor' ) ? "mail" : "correo";*/

            try{
				$conn = new PDO('mysql:host=' . DBHOST . ';dbname=' . DBNAME, 
								DBUSER, 
								DBPASS);
				 
				$conn->setAttribute( PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION );

                switch ($this->perfil) {
                    case 'alumno':
                        $sql = $conn->prepare(
                            'SELECT * FROM alumnos WHERE correo = :Username AND pass = :Matricula LIMIT 1'
                        );
                    break;

                    case 'profesor':
                        $sql = $conn->prepare(
                            'SELECT * FROM profesor WHERE mail = :Username AND pass = :Matricula LIMIT 1'
                        );
                    break;
                    
                    default:
                        
                    break;
                }

                /*$sql = $conn->prepare(
                    'SELECT * FROM alumnos WHERE correo = :Username LIMIT 1'
                );*/

                $sql->execute( array('Username' => $this->correo,
                                     'Matricula' => $this->cuenta) );				
                $resultado = $sql->fetchAll();
                $conn = null;
                return $resultado;

			}catch(PDOException $e){
				echo $e;
			}

        }

        function getPerfil( $sesion ){

            $result=array();
                switch ($sesion) {
                    case 'alumno':
                        $submenu="Servicios Estudiantiles";
                        $perfil = $sesion;
                        $pagina = "alumnos";
                        break;

                    case 'profesor':
                        $submenu="Servicio a Profesores";
                        $perfil = $sesion;
                        $pagina = "profesores";
                        break;

                    case 'exalumno':
                        $submenu="Servicios a Egresados";
                        $perfil = $sesion;
                        $pagina = "exalumnos";
                        break;

                    case 'padre':
                        $submenu="Servicios a Padres";
                        $perfil = $sesion;
                        $pagina = "padres";
                        break;

                    case 'prospectopg':
                        $submenu="Conócenos";
                        $perfil = $sesion;
                        $pagina = "";
                        break;

                    case 'prospectoug':
                        $submenu="Conócenos";
                        $perfil = $sesion;
                        $pagina = "";
                        break;
                    
                    default:
                        $submenu = 'Conócenos';
                        $perfil = 'general';
                        $pagina = "";
                        break;
                }

                $result["submenu"]=$submenu;
                $result["perfil"]=$perfil;
                $result["pagina"]=$pagina;

            return $result;
        }

        function biblioteca_libri( $nombre ){
            $fecha     = time();
            $matricula = $nombre."@my.unitec.edu.mx";
            $servicio  = "ls2U0nI14Te020c3Mx";
            $hash      = sha1($matricula."|".$servicio."|".$fecha);
            $url       = "http://unitecmx.libri.mx/usuario_valida.php?matricula=".$matricula."&fecha=".$fecha."&carrera=&tipodeusuario=Alumno&nombreusuario=".$nombre."&libroId=&hash=".$hash;
            return $url;
        }

        function biblioteca_pearson( $nombre ){ 

            $fecha     = time();
            $matricula = $nombre."@my.unitec.edu.mx";
            $servicio  = "ls2U0nI14Te020c3Mx";
            $hash      = sha1($matricula."|".$servicio."|".$fecha);
            //$url       ="http://unitecmx.libri.mx/usuario_valida.php?matricula=".$matricula."&fecha=".$fecha."&carrera=&tipodeusuario=Alumno&nombreusuario=".$nombre."&libroId=&hash=".$hash;
            $url="https://www.biblionline.pearson.com/Services/GenerateURLAccess.svc/GetUrl?firstname=".$nombre."&lastname1=&lastname2=&email=".$matricula."&ip=&idInstitution=327&idCampus=&institutionKey=clunit3cmex2105"; //&$callback=successCall&$format=json
            $result=json_decode(file_get_contents($url));
            return $result->GetUrlAccessResult->Message;
        }

        function biblioteca_cengage( $nombre ){ 

            $matricula = $nombre."@my.unitec.edu.mx";
            
            if(strpos($nombre, ".")!==false){
                $separa=explode(".", $nombre);
            }else if(strpos($nombre, "-")!==false){
                $separa=explode("-", $nombre);
            }else if(strpos($nombre, "_")!==false){
                $separa=explode("_", $nombre);
            }

            $url="https://bibliotecavirtual.cengage.com/laureate/unitecmx-laureate?pais=Mexico&mail=".$matricula."&nombre=".$separa[0]."&apellido=".$separa[1];
            return $url;
        }

        function biblioteca_ebrary($nombre){
            // $url="http://site.ebrary.com/lib/laureatemhe";
            $url = "https://ebookcentral.proquest.com/lib/unitecmhe";
            return $url;
        }

        /*Implementacion BY SRP 17-09-2018*/
        function biblioteca_kivuto( $nombre, $matricula ) {
            ob_start();
            //echo "Datos para Kivuto: ".$nombre . " - ". $matricula;

            $key = '7a6bc0dd'; // replace with your webstore key
            $account = '100079109';
            $host = 'https://e5.onthehub.com/WebStore/Security/AuthenticateUser.ashx';
            

            if(strpos($nombre, ".")!==false){
                $name=explode(".", $nombre);
            }else if(strpos($nombre, "-")!==false){
                $name=explode("-", $nombre);
            }else if(strpos($nombre, "_")!==false){
                $name=explode("_", $nombre);
            }

            $email = $nombre."@my.unitec.edu.mx";

            // Build query body
            $data = array(
                'username'          => $matricula,
                'account'           => $account,
                //'email'             => $email,
                //'first_name'        => $name[0],
                //'last_name'         => $name[1],
                'academic_statuses' => $_SESSION['academic_statuses'],
                'shopper_ip'        => $_SERVER['REMOTE_ADDR'],
                'key'               => $key
            );

            //print_r( $data );

            $options = array('http' =>
                array(
                    'method'    => 'POST',
                    'header'    => 'Content-type: application/json',
                    'content'   => json_encode($data)
                )
            );

            $context = stream_context_create($options);
            $e5LoginRedirectURL = urldecode( file_get_contents($host, false, $context) );

            // check response status code
            $http_status = $http_response_header[0];
            //echo "RESPUESTA: ". $http_status;

            if (strpos($http_status, '200 OK') === false)   // NOTE $http_status may not always be $http_response_header[0]
            {
                // we have an error
                echo '<p><b>A handshake error has occured</b></p>';
                echo '<p><b>Response received:<br><font color="red">'.$http_status.'</font></b></p>';
                //$url = "#";
            }
            else if ( strlen($e5LoginRedirectURL ) == 0)
            {
                // HTTP status code was OK but server didn't return a redirect URL; may be some other error, such as incorrectly configured server IP for your server
                echo '<p><b>A handshake error has occured; invalid redirection URL</b></p>';
                //$url = "#";
            }
            else 
            {
                // status code looks good and we have a redirection URL; set redirect location in header
                $url = $e5LoginRedirectURL;
                //header('Location: '.$e5LoginRedirectURL);
            }

            // clear session variables and destroy
            // $_SESSION=array();
            // if(isset($_COOKIE[session_name()])) 
            // {
            //     setcookie(session_name(),'',time()-42000,'/');
            // }
            // @session_destroy();
            // ob_end_flush();
            // exit;

            return $url;

        }
        /*End Implementacion BY SRP 17-09-2018*/

        private function forgetDataInt(){
            unset($this->dataAjax[ "correo"  ]);
            unset($this->dataAjax[ "cuenta"  ]);
            unset($this->dataAjax[ "origen"  ]);
            unset($this->dataAjax[ "perfil"  ]);
            unset($this->dataAjax[ "usuario" ]);
        }

    }