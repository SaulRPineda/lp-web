<?php

    if ( isset( $_POST[ "route" ] ) ){
        if( in_array( $_POST['route'], array( "BibliotecaVirtual", "buscadorGCS", "Impulsa" ) ) ){
            return json_encode( Router::getRouter() );
        }
    } else {
        return json_encode( [ $error = 'Error la peticion no es valida...'] );
    }

    class Router{

        private static $instRouter;
        private $respuesta = [];

        public static function getRouter(){
            if( !( self::$instRouter instanceof self ) ){
                self::$instRouter = new self();
            }
            return self::$instRouter;
        }

        private function __construct(){

            $route = $_POST["route"];
            unset( $_POST["route"] );

            switch( $route ){
                case 'BibliotecaVirtual':
                    include_once "BibliotecaVirtual/BibliotecaVirtual.php";
                    $BibliotecaVirtual = new BibliotecaVirtual();
                    $respuesta = $BibliotecaVirtual->redirecciona( $_POST );
                    echo json_encode( $respuesta );
                break;

                case 'Impulsa':
                    include_once "Impulsa/Impulsa.php";
                    $Impulsa = new Impulsa();
                    $respuesta = $Impulsa->redirecciona( $_POST );
                    echo json_encode( $respuesta );
                break;
            }
        }
        
    }