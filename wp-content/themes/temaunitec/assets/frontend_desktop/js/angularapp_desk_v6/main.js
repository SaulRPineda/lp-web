(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["main"],{

/***/ "./src/$$_lazy_route_resource lazy recursive":
/*!**********************************************************!*\
  !*** ./src/$$_lazy_route_resource lazy namespace object ***!
  \**********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

function webpackEmptyAsyncContext(req) {
	// Here Promise.resolve().then() is used instead of new Promise() to prevent
	// uncaught exception popping up in devtools
	return Promise.resolve().then(function() {
		var e = new Error('Cannot find module "' + req + '".');
		e.code = 'MODULE_NOT_FOUND';
		throw e;
	});
}
webpackEmptyAsyncContext.keys = function() { return []; };
webpackEmptyAsyncContext.resolve = webpackEmptyAsyncContext;
module.exports = webpackEmptyAsyncContext;
webpackEmptyAsyncContext.id = "./src/$$_lazy_route_resource lazy recursive";

/***/ }),

/***/ "./src/app/app.module.ngfactory.js":
/*!*****************************************!*\
  !*** ./src/app/app.module.ngfactory.js ***!
  \*****************************************/
/*! exports provided: AppModuleNgFactory */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AppModuleNgFactory", function() { return AppModuleNgFactory; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/esm5/core.js");
/* harmony import */ var _app_module__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./app.module */ "./src/app/app.module.ts");
/* harmony import */ var _formularios_forms_parent_components_formulario_login_formulario_login_component_ngfactory__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./formularios/forms_parent_components/formulario-login/formulario-login.component.ngfactory */ "./src/app/formularios/forms_parent_components/formulario-login/formulario-login.component.ngfactory.js");
/* harmony import */ var _buscador_formularioGCS_formularioGCS_component_ngfactory__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./buscador/formularioGCS/formularioGCS.component.ngfactory */ "./src/app/buscador/formularioGCS/formularioGCS.component.ngfactory.js");
/* harmony import */ var _utilities_modal_modal_component_ngfactory__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./utilities/modal/modal.component.ngfactory */ "./src/app/utilities/modal/modal.component.ngfactory.js");
/* harmony import */ var _buscador_search_side_nav_search_side_nav_component_ngfactory__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./buscador/search-side-nav/search-side-nav.component.ngfactory */ "./src/app/buscador/search-side-nav/search-side-nav.component.ngfactory.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/esm5/common.js");
/* harmony import */ var _angular_platform_browser__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @angular/platform-browser */ "./node_modules/@angular/platform-browser/esm5/platform-browser.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/esm5/forms.js");
/* harmony import */ var _angular_http__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! @angular/http */ "./node_modules/@angular/http/esm5/http.js");
/* harmony import */ var ngx_cookie_service_cookie_service_cookie_service__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! ngx-cookie-service/cookie-service/cookie.service */ "./node_modules/ngx-cookie-service/cookie-service/cookie.service.js");
/* harmony import */ var _services_getJson_service__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! ./services/getJson.service */ "./src/app/services/getJson.service.ts");
/* harmony import */ var _formularios_forms_services_validaciones_service__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! ./formularios/forms_services/validaciones.service */ "./src/app/formularios/forms_services/validaciones.service.ts");
/* harmony import */ var _services_readJson_service__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(/*! ./services/readJson.service */ "./src/app/services/readJson.service.ts");
/* harmony import */ var _formularios_forms_services_formCookies_service__WEBPACK_IMPORTED_MODULE_14__ = __webpack_require__(/*! ./formularios/forms_services/formCookies.service */ "./src/app/formularios/forms_services/formCookies.service.ts");
/* harmony import */ var _formularios_forms_services_service_pipe__WEBPACK_IMPORTED_MODULE_15__ = __webpack_require__(/*! ./formularios/forms_services/service.pipe */ "./src/app/formularios/forms_services/service.pipe.ts");
/* harmony import */ var _services_generic_service__WEBPACK_IMPORTED_MODULE_16__ = __webpack_require__(/*! ./services/generic.service */ "./src/app/services/generic.service.ts");
/* harmony import */ var _formularios_forms_services_send_service__WEBPACK_IMPORTED_MODULE_17__ = __webpack_require__(/*! ./formularios/forms_services/send.service */ "./src/app/formularios/forms_services/send.service.ts");
/* harmony import */ var _formularios_forms_services_reglasInputs_service__WEBPACK_IMPORTED_MODULE_18__ = __webpack_require__(/*! ./formularios/forms_services/reglasInputs.service */ "./src/app/formularios/forms_services/reglasInputs.service.ts");
/* harmony import */ var _services_webspeach_service__WEBPACK_IMPORTED_MODULE_19__ = __webpack_require__(/*! ./services/webspeach.service */ "./src/app/services/webspeach.service.ts");
/* harmony import */ var _services_autocomplete_service__WEBPACK_IMPORTED_MODULE_20__ = __webpack_require__(/*! ./services/autocomplete.service */ "./src/app/services/autocomplete.service.ts");
/**
 * @fileoverview This file was generated by the Angular template compiler. Do not edit.
 *
 * @suppress {suspiciousCode,uselessCode,missingProperties,missingOverride,checkTypes}
 * tslint:disable
 */
/** PURE_IMPORTS_START _angular_core,_app.module,_formularios_forms_parent_components_formulario_login_formulario_login.component.ngfactory,_buscador_formularioGCS_formularioGCS.component.ngfactory,_utilities_modal_modal.component.ngfactory,_buscador_search_side_nav_search_side_nav.component.ngfactory,_angular_common,_angular_platform_browser,_angular_forms,_angular_http,ngx_cookie_service_cookie_service_cookie.service,_services_getJson.service,_formularios_forms_services_validaciones.service,_services_readJson.service,_formularios_forms_services_formCookies.service,_formularios_forms_services_service.pipe,_services_generic.service,_formularios_forms_services_send.service,_formularios_forms_services_reglasInputs.service,_services_webspeach.service,_services_autocomplete.service PURE_IMPORTS_END */
/** PURE_IMPORTS_START _angular_core,_app.module,_formularios_forms_parent_components_formulario_login_formulario_login.component.ngfactory,_buscador_formularioGCS_formularioGCS.component.ngfactory,_utilities_modal_modal.component.ngfactory,_buscador_search_side_nav_search_side_nav.component.ngfactory,_angular_common,_angular_platform_browser,_angular_forms,_angular_http,ngx_cookie_service_cookie_service_cookie.service,_services_getJson.service,_formularios_forms_services_validaciones.service,_services_readJson.service,_formularios_forms_services_formCookies.service,_formularios_forms_services_service.pipe,_services_generic.service,_formularios_forms_services_send.service,_formularios_forms_services_reglasInputs.service,_services_webspeach.service,_services_autocomplete.service PURE_IMPORTS_END */





















var AppModuleNgFactory = /*@__PURE__*/ /*@__PURE__*/ _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵcmf"](_app_module__WEBPACK_IMPORTED_MODULE_1__["AppModule"], [], function (_l) { return _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵmod"]([_angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵmpd"](512, _angular_core__WEBPACK_IMPORTED_MODULE_0__["ComponentFactoryResolver"], _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵCodegenComponentFactoryResolver"], [[8, [_formularios_forms_parent_components_formulario_login_formulario_login_component_ngfactory__WEBPACK_IMPORTED_MODULE_2__["FormularioLoginComponentNgFactory"], _buscador_formularioGCS_formularioGCS_component_ngfactory__WEBPACK_IMPORTED_MODULE_3__["FormularioGCSComponentNgFactory"], _utilities_modal_modal_component_ngfactory__WEBPACK_IMPORTED_MODULE_4__["ModalComponentNgFactory"], _buscador_search_side_nav_search_side_nav_component_ngfactory__WEBPACK_IMPORTED_MODULE_5__["SearchSideNavComponentNgFactory"]]], [3, _angular_core__WEBPACK_IMPORTED_MODULE_0__["ComponentFactoryResolver"]], _angular_core__WEBPACK_IMPORTED_MODULE_0__["NgModuleRef"]]), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵmpd"](5120, _angular_core__WEBPACK_IMPORTED_MODULE_0__["LOCALE_ID"], _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵq"], [[3, _angular_core__WEBPACK_IMPORTED_MODULE_0__["LOCALE_ID"]]]), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵmpd"](4608, _angular_common__WEBPACK_IMPORTED_MODULE_6__["NgLocalization"], _angular_common__WEBPACK_IMPORTED_MODULE_6__["NgLocaleLocalization"], [_angular_core__WEBPACK_IMPORTED_MODULE_0__["LOCALE_ID"], [2, _angular_common__WEBPACK_IMPORTED_MODULE_6__["ɵa"]]]), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵmpd"](4608, _angular_core__WEBPACK_IMPORTED_MODULE_0__["Compiler"], _angular_core__WEBPACK_IMPORTED_MODULE_0__["Compiler"], []), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵmpd"](5120, _angular_core__WEBPACK_IMPORTED_MODULE_0__["APP_ID"], _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵi"], []), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵmpd"](5120, _angular_core__WEBPACK_IMPORTED_MODULE_0__["IterableDiffers"], _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵn"], []), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵmpd"](5120, _angular_core__WEBPACK_IMPORTED_MODULE_0__["KeyValueDiffers"], _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵo"], []), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵmpd"](4608, _angular_platform_browser__WEBPACK_IMPORTED_MODULE_7__["DomSanitizer"], _angular_platform_browser__WEBPACK_IMPORTED_MODULE_7__["ɵe"], [_angular_common__WEBPACK_IMPORTED_MODULE_6__["DOCUMENT"]]), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵmpd"](6144, _angular_core__WEBPACK_IMPORTED_MODULE_0__["Sanitizer"], null, [_angular_platform_browser__WEBPACK_IMPORTED_MODULE_7__["DomSanitizer"]]), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵmpd"](4608, _angular_platform_browser__WEBPACK_IMPORTED_MODULE_7__["HAMMER_GESTURE_CONFIG"], _angular_platform_browser__WEBPACK_IMPORTED_MODULE_7__["HammerGestureConfig"], []), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵmpd"](5120, _angular_platform_browser__WEBPACK_IMPORTED_MODULE_7__["EVENT_MANAGER_PLUGINS"], function (p0_0, p0_1, p1_0, p2_0, p2_1) { return [new _angular_platform_browser__WEBPACK_IMPORTED_MODULE_7__["ɵDomEventsPlugin"](p0_0, p0_1), new _angular_platform_browser__WEBPACK_IMPORTED_MODULE_7__["ɵKeyEventsPlugin"](p1_0), new _angular_platform_browser__WEBPACK_IMPORTED_MODULE_7__["ɵHammerGesturesPlugin"](p2_0, p2_1)]; }, [_angular_common__WEBPACK_IMPORTED_MODULE_6__["DOCUMENT"], _angular_core__WEBPACK_IMPORTED_MODULE_0__["NgZone"], _angular_common__WEBPACK_IMPORTED_MODULE_6__["DOCUMENT"], _angular_common__WEBPACK_IMPORTED_MODULE_6__["DOCUMENT"], _angular_platform_browser__WEBPACK_IMPORTED_MODULE_7__["HAMMER_GESTURE_CONFIG"]]), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵmpd"](4608, _angular_platform_browser__WEBPACK_IMPORTED_MODULE_7__["EventManager"], _angular_platform_browser__WEBPACK_IMPORTED_MODULE_7__["EventManager"], [_angular_platform_browser__WEBPACK_IMPORTED_MODULE_7__["EVENT_MANAGER_PLUGINS"], _angular_core__WEBPACK_IMPORTED_MODULE_0__["NgZone"]]), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵmpd"](135680, _angular_platform_browser__WEBPACK_IMPORTED_MODULE_7__["ɵDomSharedStylesHost"], _angular_platform_browser__WEBPACK_IMPORTED_MODULE_7__["ɵDomSharedStylesHost"], [_angular_common__WEBPACK_IMPORTED_MODULE_6__["DOCUMENT"]]), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵmpd"](4608, _angular_platform_browser__WEBPACK_IMPORTED_MODULE_7__["ɵDomRendererFactory2"], _angular_platform_browser__WEBPACK_IMPORTED_MODULE_7__["ɵDomRendererFactory2"], [_angular_platform_browser__WEBPACK_IMPORTED_MODULE_7__["EventManager"], _angular_platform_browser__WEBPACK_IMPORTED_MODULE_7__["ɵDomSharedStylesHost"]]), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵmpd"](6144, _angular_core__WEBPACK_IMPORTED_MODULE_0__["RendererFactory2"], null, [_angular_platform_browser__WEBPACK_IMPORTED_MODULE_7__["ɵDomRendererFactory2"]]), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵmpd"](6144, _angular_platform_browser__WEBPACK_IMPORTED_MODULE_7__["ɵSharedStylesHost"], null, [_angular_platform_browser__WEBPACK_IMPORTED_MODULE_7__["ɵDomSharedStylesHost"]]), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵmpd"](4608, _angular_core__WEBPACK_IMPORTED_MODULE_0__["Testability"], _angular_core__WEBPACK_IMPORTED_MODULE_0__["Testability"], [_angular_core__WEBPACK_IMPORTED_MODULE_0__["NgZone"]]), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵmpd"](4608, _angular_platform_browser__WEBPACK_IMPORTED_MODULE_7__["Meta"], _angular_platform_browser__WEBPACK_IMPORTED_MODULE_7__["Meta"], [_angular_common__WEBPACK_IMPORTED_MODULE_6__["DOCUMENT"]]), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵmpd"](4608, _angular_platform_browser__WEBPACK_IMPORTED_MODULE_7__["Title"], _angular_platform_browser__WEBPACK_IMPORTED_MODULE_7__["Title"], [_angular_common__WEBPACK_IMPORTED_MODULE_6__["DOCUMENT"]]), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵmpd"](4608, _angular_forms__WEBPACK_IMPORTED_MODULE_8__["ɵi"], _angular_forms__WEBPACK_IMPORTED_MODULE_8__["ɵi"], []), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵmpd"](4608, _angular_forms__WEBPACK_IMPORTED_MODULE_8__["FormBuilder"], _angular_forms__WEBPACK_IMPORTED_MODULE_8__["FormBuilder"], []), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵmpd"](4608, _angular_http__WEBPACK_IMPORTED_MODULE_9__["BrowserXhr"], _angular_http__WEBPACK_IMPORTED_MODULE_9__["BrowserXhr"], []), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵmpd"](4608, _angular_http__WEBPACK_IMPORTED_MODULE_9__["ResponseOptions"], _angular_http__WEBPACK_IMPORTED_MODULE_9__["BaseResponseOptions"], []), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵmpd"](5120, _angular_http__WEBPACK_IMPORTED_MODULE_9__["XSRFStrategy"], _angular_http__WEBPACK_IMPORTED_MODULE_9__["ɵa"], []), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵmpd"](4608, _angular_http__WEBPACK_IMPORTED_MODULE_9__["XHRBackend"], _angular_http__WEBPACK_IMPORTED_MODULE_9__["XHRBackend"], [_angular_http__WEBPACK_IMPORTED_MODULE_9__["BrowserXhr"], _angular_http__WEBPACK_IMPORTED_MODULE_9__["ResponseOptions"], _angular_http__WEBPACK_IMPORTED_MODULE_9__["XSRFStrategy"]]), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵmpd"](4608, _angular_http__WEBPACK_IMPORTED_MODULE_9__["RequestOptions"], _angular_http__WEBPACK_IMPORTED_MODULE_9__["BaseRequestOptions"], []), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵmpd"](5120, _angular_http__WEBPACK_IMPORTED_MODULE_9__["Http"], _angular_http__WEBPACK_IMPORTED_MODULE_9__["ɵb"], [_angular_http__WEBPACK_IMPORTED_MODULE_9__["XHRBackend"], _angular_http__WEBPACK_IMPORTED_MODULE_9__["RequestOptions"]]), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵmpd"](4608, _angular_http__WEBPACK_IMPORTED_MODULE_9__["ɵe"], _angular_http__WEBPACK_IMPORTED_MODULE_9__["ɵe"], []), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵmpd"](4608, _angular_http__WEBPACK_IMPORTED_MODULE_9__["JSONPBackend"], _angular_http__WEBPACK_IMPORTED_MODULE_9__["JSONPBackend"], [_angular_http__WEBPACK_IMPORTED_MODULE_9__["ɵe"], _angular_http__WEBPACK_IMPORTED_MODULE_9__["ResponseOptions"]]), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵmpd"](5120, _angular_http__WEBPACK_IMPORTED_MODULE_9__["Jsonp"], _angular_http__WEBPACK_IMPORTED_MODULE_9__["ɵc"], [_angular_http__WEBPACK_IMPORTED_MODULE_9__["JSONPBackend"], _angular_http__WEBPACK_IMPORTED_MODULE_9__["RequestOptions"]]), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵmpd"](4608, ngx_cookie_service_cookie_service_cookie_service__WEBPACK_IMPORTED_MODULE_10__["CookieService"], ngx_cookie_service_cookie_service_cookie_service__WEBPACK_IMPORTED_MODULE_10__["CookieService"], [_angular_common__WEBPACK_IMPORTED_MODULE_6__["DOCUMENT"]]), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵmpd"](4608, _services_getJson_service__WEBPACK_IMPORTED_MODULE_11__["getJson"], _services_getJson_service__WEBPACK_IMPORTED_MODULE_11__["getJson"], [_angular_http__WEBPACK_IMPORTED_MODULE_9__["Http"], _angular_platform_browser__WEBPACK_IMPORTED_MODULE_7__["DOCUMENT"]]), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵmpd"](4608, _formularios_forms_services_validaciones_service__WEBPACK_IMPORTED_MODULE_12__["userValidations"], _formularios_forms_services_validaciones_service__WEBPACK_IMPORTED_MODULE_12__["userValidations"], [_services_getJson_service__WEBPACK_IMPORTED_MODULE_11__["getJson"], _angular_platform_browser__WEBPACK_IMPORTED_MODULE_7__["DOCUMENT"]]), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵmpd"](4608, _services_readJson_service__WEBPACK_IMPORTED_MODULE_13__["readJson"], _services_readJson_service__WEBPACK_IMPORTED_MODULE_13__["readJson"], []), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵmpd"](4608, _formularios_forms_services_formCookies_service__WEBPACK_IMPORTED_MODULE_14__["formCookiesService"], _formularios_forms_services_formCookies_service__WEBPACK_IMPORTED_MODULE_14__["formCookiesService"], [ngx_cookie_service_cookie_service_cookie_service__WEBPACK_IMPORTED_MODULE_10__["CookieService"]]), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵmpd"](4608, _formularios_forms_services_service_pipe__WEBPACK_IMPORTED_MODULE_15__["CapitalizePipe"], _formularios_forms_services_service_pipe__WEBPACK_IMPORTED_MODULE_15__["CapitalizePipe"], []), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵmpd"](4608, _services_generic_service__WEBPACK_IMPORTED_MODULE_16__["GenericService"], _services_generic_service__WEBPACK_IMPORTED_MODULE_16__["GenericService"], [_services_readJson_service__WEBPACK_IMPORTED_MODULE_13__["readJson"]]), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵmpd"](4608, _formularios_forms_services_send_service__WEBPACK_IMPORTED_MODULE_17__["sendService"], _formularios_forms_services_send_service__WEBPACK_IMPORTED_MODULE_17__["sendService"], [_formularios_forms_services_formCookies_service__WEBPACK_IMPORTED_MODULE_14__["formCookiesService"], _formularios_forms_services_service_pipe__WEBPACK_IMPORTED_MODULE_15__["CapitalizePipe"], _formularios_forms_services_validaciones_service__WEBPACK_IMPORTED_MODULE_12__["userValidations"], _services_readJson_service__WEBPACK_IMPORTED_MODULE_13__["readJson"], _formularios_forms_services_formCookies_service__WEBPACK_IMPORTED_MODULE_14__["formCookiesService"], _services_generic_service__WEBPACK_IMPORTED_MODULE_16__["GenericService"]]), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵmpd"](4608, _formularios_forms_services_reglasInputs_service__WEBPACK_IMPORTED_MODULE_18__["reglasInputsService"], _formularios_forms_services_reglasInputs_service__WEBPACK_IMPORTED_MODULE_18__["reglasInputsService"], []), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵmpd"](4608, _services_webspeach_service__WEBPACK_IMPORTED_MODULE_19__["WebspeachService"], _services_webspeach_service__WEBPACK_IMPORTED_MODULE_19__["WebspeachService"], [_angular_core__WEBPACK_IMPORTED_MODULE_0__["NgZone"]]), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵmpd"](135680, _services_autocomplete_service__WEBPACK_IMPORTED_MODULE_20__["AutocompleteService"], _services_autocomplete_service__WEBPACK_IMPORTED_MODULE_20__["AutocompleteService"], [_angular_http__WEBPACK_IMPORTED_MODULE_9__["Http"]]), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵmpd"](512, _angular_common__WEBPACK_IMPORTED_MODULE_6__["CommonModule"], _angular_common__WEBPACK_IMPORTED_MODULE_6__["CommonModule"], []), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵmpd"](1024, _angular_core__WEBPACK_IMPORTED_MODULE_0__["ErrorHandler"], _angular_platform_browser__WEBPACK_IMPORTED_MODULE_7__["ɵa"], []), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵmpd"](1024, _angular_core__WEBPACK_IMPORTED_MODULE_0__["APP_INITIALIZER"], function (p0_0) { return [_angular_platform_browser__WEBPACK_IMPORTED_MODULE_7__["ɵh"](p0_0)]; }, [[2, _angular_core__WEBPACK_IMPORTED_MODULE_0__["NgProbeToken"]]]), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵmpd"](512, _angular_core__WEBPACK_IMPORTED_MODULE_0__["ApplicationInitStatus"], _angular_core__WEBPACK_IMPORTED_MODULE_0__["ApplicationInitStatus"], [[2, _angular_core__WEBPACK_IMPORTED_MODULE_0__["APP_INITIALIZER"]]]), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵmpd"](131584, _angular_core__WEBPACK_IMPORTED_MODULE_0__["ApplicationRef"], _angular_core__WEBPACK_IMPORTED_MODULE_0__["ApplicationRef"], [_angular_core__WEBPACK_IMPORTED_MODULE_0__["NgZone"], _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵConsole"], _angular_core__WEBPACK_IMPORTED_MODULE_0__["Injector"], _angular_core__WEBPACK_IMPORTED_MODULE_0__["ErrorHandler"], _angular_core__WEBPACK_IMPORTED_MODULE_0__["ComponentFactoryResolver"], _angular_core__WEBPACK_IMPORTED_MODULE_0__["ApplicationInitStatus"]]), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵmpd"](512, _angular_core__WEBPACK_IMPORTED_MODULE_0__["ApplicationModule"], _angular_core__WEBPACK_IMPORTED_MODULE_0__["ApplicationModule"], [_angular_core__WEBPACK_IMPORTED_MODULE_0__["ApplicationRef"]]), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵmpd"](512, _angular_platform_browser__WEBPACK_IMPORTED_MODULE_7__["BrowserModule"], _angular_platform_browser__WEBPACK_IMPORTED_MODULE_7__["BrowserModule"], [[3, _angular_platform_browser__WEBPACK_IMPORTED_MODULE_7__["BrowserModule"]]]), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵmpd"](512, _angular_forms__WEBPACK_IMPORTED_MODULE_8__["ɵba"], _angular_forms__WEBPACK_IMPORTED_MODULE_8__["ɵba"], []), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵmpd"](512, _angular_forms__WEBPACK_IMPORTED_MODULE_8__["FormsModule"], _angular_forms__WEBPACK_IMPORTED_MODULE_8__["FormsModule"], []), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵmpd"](512, _angular_forms__WEBPACK_IMPORTED_MODULE_8__["ReactiveFormsModule"], _angular_forms__WEBPACK_IMPORTED_MODULE_8__["ReactiveFormsModule"], []), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵmpd"](512, _angular_http__WEBPACK_IMPORTED_MODULE_9__["HttpModule"], _angular_http__WEBPACK_IMPORTED_MODULE_9__["HttpModule"], []), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵmpd"](512, _angular_http__WEBPACK_IMPORTED_MODULE_9__["JsonpModule"], _angular_http__WEBPACK_IMPORTED_MODULE_9__["JsonpModule"], []), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵmpd"](512, _app_module__WEBPACK_IMPORTED_MODULE_1__["AppModule"], _app_module__WEBPACK_IMPORTED_MODULE_1__["AppModule"], [])]); });



/***/ }),

/***/ "./src/app/app.module.ts":
/*!*******************************!*\
  !*** ./src/app/app.module.ts ***!
  \*******************************/
/*! exports provided: AppModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AppModule", function() { return AppModule; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/esm5/core.js");
/* harmony import */ var _utilities_modal_modal_component__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./utilities/modal/modal.component */ "./src/app/utilities/modal/modal.component.ts");
/* harmony import */ var _formularios_forms_parent_components_formulario_login_formulario_login_component__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./formularios/forms_parent_components/formulario-login/formulario-login.component */ "./src/app/formularios/forms_parent_components/formulario-login/formulario-login.component.ts");
/* harmony import */ var _buscador_search_side_nav_search_side_nav_component__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./buscador/search-side-nav/search-side-nav.component */ "./src/app/buscador/search-side-nav/search-side-nav.component.ts");

//Componente modal
//Servico que proporcionara animaciones.
// import { Animaciones } from './utilities/animaciones';


//import {TypewriterModule} from 'ng2-typewriter';
// import { MiniSimuladorComponent } from './mini-simulador/mini-simulador.component';

//comentado por Luis import { formularioTallerComponent } from './formularios/forms_parent_components/formulario-taller/formulario-taller.component';
var AppModule = /** @class */ /*@__PURE__*/ (function () {
    function AppModule() {
    }
    //Carga condicional de componentes padre
    AppModule.prototype.ngDoBootstrap = function (ref) {
        //Configuracion para hacer funcionar en producion el elemento buscadorGCS
        //comentado por Luis      if (location.href.indexOf( "buscador" ) !== -1 ) {
        //comentado por Luis        ref.bootstrap( FormularioGCSComponent );
        //comentado por Luis      } else 
        if (location.href.indexOf("biblioteca-virtual") !== -1) {
            //Carga el componente Login para ingresar a la biblioteca Virtual
            ref.bootstrap(_formularios_forms_parent_components_formulario_login_formulario_login_component__WEBPACK_IMPORTED_MODULE_2__["FormularioLoginComponent"]);
            ref.bootstrap(_utilities_modal_modal_component__WEBPACK_IMPORTED_MODULE_1__["ModalComponent"]);
            // ref.bootstrap( ModalFormularioTradicionalComponent );
            //comentado por Luis        ref.bootstrap( ModalAppFormularioTradicionalComponent );
            ref.bootstrap(_buscador_search_side_nav_search_side_nav_component__WEBPACK_IMPORTED_MODULE_3__["SearchSideNavComponent"]);
            //comentado por Luis      } else if (location.href.indexOf( "calcula-tu-beca" ) !== -1 ){
            //comentado por Luis        ref.bootstrap( ModalCalculadoraFormularioTradicionalComponent );
            //comentado por Luis      } else if (location.href.indexOf( "calcula" ) !== -1 ){
            //comentado por Luis        ref.bootstrap( ModalCalculadoraFormularioTradicionalComponent );
            //comentado por Luis      } else if (location.href.indexOf( "prueba-performance" ) !== -1 ){
            //comentado por Luis        ref.bootstrap(ModalAppFormularioTallerComponent );
            //comentado por Luis      } else if ((location.href.indexOf("conexion-unitec") !== -1) || (location.href.indexOf("agendar-cita") !== -1)){
        }
        else {
            //Formulario
            ref.bootstrap(_utilities_modal_modal_component__WEBPACK_IMPORTED_MODULE_1__["ModalComponent"]);
            // ref.bootstrap( ModalFormularioTradicionalComponent );
            //comentado por Luis        ref.bootstrap( ModalAppFormularioTradicionalComponent );
            ref.bootstrap(_buscador_search_side_nav_search_side_nav_component__WEBPACK_IMPORTED_MODULE_3__["SearchSideNavComponent"]);
        }
    };
    return AppModule;
}());



/***/ }),

/***/ "./src/app/buscador/formularioGCS/formularioGCS.component.ngfactory.js":
/*!*****************************************************************************!*\
  !*** ./src/app/buscador/formularioGCS/formularioGCS.component.ngfactory.js ***!
  \*****************************************************************************/
/*! exports provided: RenderType_FormularioGCSComponent, View_FormularioGCSComponent_0, View_FormularioGCSComponent_Host_0, FormularioGCSComponentNgFactory */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "RenderType_FormularioGCSComponent", function() { return RenderType_FormularioGCSComponent; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "View_FormularioGCSComponent_0", function() { return View_FormularioGCSComponent_0; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "View_FormularioGCSComponent_Host_0", function() { return View_FormularioGCSComponent_Host_0; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "FormularioGCSComponentNgFactory", function() { return FormularioGCSComponentNgFactory; });
/* harmony import */ var _formularioGCS_component_scss_shim_ngstyle__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./formularioGCS.component.scss.shim.ngstyle */ "./src/app/buscador/formularioGCS/formularioGCS.component.scss.shim.ngstyle.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/esm5/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/esm5/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/esm5/forms.js");
/* harmony import */ var _formularios_forms_services_validaciones_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../../formularios/forms_services/validaciones.service */ "./src/app/formularios/forms_services/validaciones.service.ts");
/* harmony import */ var _services_getJson_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../../services/getJson.service */ "./src/app/services/getJson.service.ts");
/* harmony import */ var _angular_platform_browser__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @angular/platform-browser */ "./node_modules/@angular/platform-browser/esm5/platform-browser.js");
/* harmony import */ var _services_webspeach_service__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ../../services/webspeach.service */ "./src/app/services/webspeach.service.ts");
/* harmony import */ var _services_autocomplete_service__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ../../services/autocomplete.service */ "./src/app/services/autocomplete.service.ts");
/* harmony import */ var _angular_http__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! @angular/http */ "./node_modules/@angular/http/esm5/http.js");
/* harmony import */ var _services_navegador_service__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! ../../services/navegador.service */ "./src/app/services/navegador.service.ts");
/* harmony import */ var _formularioGCS_component__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! ./formularioGCS.component */ "./src/app/buscador/formularioGCS/formularioGCS.component.ts");
/**
 * @fileoverview This file was generated by the Angular template compiler. Do not edit.
 *
 * @suppress {suspiciousCode,uselessCode,missingProperties,missingOverride,checkTypes}
 * tslint:disable
 */
/** PURE_IMPORTS_START _formularioGCS.component.scss.shim.ngstyle,_angular_core,_angular_common,_angular_forms,_.._formularios_forms_services_validaciones.service,_.._services_getJson.service,_angular_platform_browser,_.._services_webspeach.service,_.._services_autocomplete.service,_angular_http,_.._services_navegador.service,_formularioGCS.component PURE_IMPORTS_END */
/** PURE_IMPORTS_START _formularioGCS.component.scss.shim.ngstyle,_angular_core,_angular_common,_angular_forms,_.._formularios_forms_services_validaciones.service,_.._services_getJson.service,_angular_platform_browser,_.._services_webspeach.service,_.._services_autocomplete.service,_angular_http,_.._services_navegador.service,_formularioGCS.component PURE_IMPORTS_END */












var styles_FormularioGCSComponent = [_formularioGCS_component_scss_shim_ngstyle__WEBPACK_IMPORTED_MODULE_0__["styles"]];
var RenderType_FormularioGCSComponent = /*@__PURE__*/ /*@__PURE__*/ _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵcrt"]({ encapsulation: 0, styles: styles_FormularioGCSComponent, data: {} });

function View_FormularioGCSComponent_2(_l) {
    return _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵvid"](0, [(_l()(), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵeld"](0, 0, null, null, 4, "span", [["class", "icon-u-microfono microfono"], ["id", "micGCS"]], null, [[null, "click"]], function (_v, en, $event) {
            var ad = true;
            var _co = _v.component;
            if (("click" === en)) {
                var pd_0 = (_co.mic() !== false);
                ad = (pd_0 && ad);
            }
            return ad;
        }, null, null)), (_l()(), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵted"](-1, null, ["\n                                "])), (_l()(), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵeld"](2, 0, null, null, 0, "span", [["class", "path1"]], null, null, null, null, null)), (_l()(), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵeld"](3, 0, null, null, 0, "span", [["class", "path2"]], null, null, null, null, null)), (_l()(), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵted"](-1, null, ["\n                            "]))], null, null);
}
function View_FormularioGCSComponent_1(_l) { return _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵvid"](0, [(_l()(), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵeld"](0, 0, null, null, 4, "span", [], null, null, null, null, null)), (_l()(), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵted"](-1, null, ["\n                            "])), (_l()(), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵand"](16777216, null, null, 1, null, View_FormularioGCSComponent_2)), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵdid"](3, 16384, null, 0, _angular_common__WEBPACK_IMPORTED_MODULE_2__["NgIf"], [_angular_core__WEBPACK_IMPORTED_MODULE_1__["ViewContainerRef"], _angular_core__WEBPACK_IMPORTED_MODULE_1__["TemplateRef"]], { ngIf: [0, "ngIf"] }, null), (_l()(), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵted"](-1, null, ["\n                        "]))], function (_ck, _v) { var _co = _v.component; var currVal_0 = (_co.whatsbrowser == "Chrome"); _ck(_v, 3, 0, currVal_0); }, null); }
function View_FormularioGCSComponent_3(_l) {
    return _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵvid"](0, [(_l()(), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵeld"](0, 0, null, null, 3, "span", [], null, null, null, null, null)), (_l()(), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵted"](-1, null, ["\n                            "])), (_l()(), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵeld"](2, 0, null, null, 0, "span", [["class", "icon-u-cerrar"]], null, [[null, "click"]], function (_v, en, $event) {
            var ad = true;
            var _co = _v.component;
            if (("click" === en)) {
                var pd_0 = (_co.setField("") !== false);
                ad = (pd_0 && ad);
            }
            return ad;
        }, null, null)), (_l()(), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵted"](-1, null, ["\n                        "]))], null, null);
}
function View_FormularioGCSComponent_5(_l) { return _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵvid"](0, [(_l()(), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵeld"](0, 0, null, null, 1, "div", [["class", "text-center h6"]], null, null, null, null, null)), (_l()(), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵted"](1, null, ["\n                    ", "\n                "]))], null, function (_ck, _v) { var _co = _v.component; var currVal_0 = _co.formGCS.get("faseUno.frm_GCS").errors.customErrorMessage; _ck(_v, 1, 0, currVal_0); }); }
function View_FormularioGCSComponent_4(_l) { return _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵvid"](0, [(_l()(), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵeld"](0, 0, null, null, 4, "div", [["class", "myFeedBackError"]], null, null, null, null, null)), (_l()(), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵted"](-1, null, ["\n                "])), (_l()(), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵand"](16777216, null, null, 1, null, View_FormularioGCSComponent_5)), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵdid"](3, 16384, null, 0, _angular_common__WEBPACK_IMPORTED_MODULE_2__["NgIf"], [_angular_core__WEBPACK_IMPORTED_MODULE_1__["ViewContainerRef"], _angular_core__WEBPACK_IMPORTED_MODULE_1__["TemplateRef"]], { ngIf: [0, "ngIf"] }, null), (_l()(), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵted"](-1, null, ["\n            "]))], function (_ck, _v) { var _co = _v.component; var currVal_0 = _co.formGCS.get("faseUno.frm_GCS").errors.fieldError; _ck(_v, 3, 0, currVal_0); }, null); }
function View_FormularioGCSComponent_7(_l) {
    return _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵvid"](0, [(_l()(), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵeld"](0, 0, null, null, 6, "li", [["class", "page-item"]], null, [[null, "click"]], function (_v, en, $event) {
            var ad = true;
            var _co = _v.component;
            if (("click" === en)) {
                var pd_0 = (_co.cambiaPagina(_v.context.$implicit) !== false);
                ad = (pd_0 && ad);
            }
            return ad;
        }, null, null)), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵdid"](1, 278528, null, 0, _angular_common__WEBPACK_IMPORTED_MODULE_2__["NgClass"], [_angular_core__WEBPACK_IMPORTED_MODULE_1__["IterableDiffers"], _angular_core__WEBPACK_IMPORTED_MODULE_1__["KeyValueDiffers"], _angular_core__WEBPACK_IMPORTED_MODULE_1__["ElementRef"], _angular_core__WEBPACK_IMPORTED_MODULE_1__["Renderer2"]], { klass: [0, "klass"], ngClass: [1, "ngClass"] }, null), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵpod"](2, { "active": 0 }), (_l()(), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵted"](-1, null, ["\n                            "])), (_l()(), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵeld"](4, 0, null, null, 1, "a", [["class", "page-link"]], null, null, null, null, null)), (_l()(), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵted"](5, null, ["", ""])), (_l()(), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵted"](-1, null, ["\n                    "]))], function (_ck, _v) { var currVal_0 = "page-item"; var currVal_1 = _ck(_v, 2, 0, _v.context.$implicit.active); _ck(_v, 1, 0, currVal_0, currVal_1); }, function (_ck, _v) { var currVal_2 = _v.context.$implicit.number; _ck(_v, 5, 0, currVal_2); });
}
function View_FormularioGCSComponent_6(_l) {
    return _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵvid"](0, [(_l()(), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵeld"](0, 0, null, null, 32, "nav", [["aria-label", "Page navigation example"], ["class", "d-flex justify-content-center paginador my-3"], ["id", "pagination-buscador"]], null, null, null, null, null)), (_l()(), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵted"](-1, null, ["\n                "])), (_l()(), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵeld"](2, 0, null, null, 29, "ul", [["class", "pagination pg-blue"]], null, null, null, null, null)), (_l()(), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵted"](-1, null, ["\n                    "])), (_l()(), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵted"](-1, null, ["\n                    "])), (_l()(), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵted"](-1, null, ["\n                    "])), (_l()(), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵeld"](6, 0, null, null, 8, "li", [["class", "page-item d-none"], ["id", "pag-prev-buscador"]], null, [[null, "click"]], function (_v, en, $event) {
            var ad = true;
            var _co = _v.component;
            if (("click" === en)) {
                var pd_0 = (_co.changeToPreviousPage() !== false);
                ad = (pd_0 && ad);
            }
            return ad;
        }, null, null)), (_l()(), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵted"](-1, null, ["\n                        "])), (_l()(), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵeld"](8, 0, null, null, 5, "a", [["aria-label", "Previous"], ["class", "page-link"]], null, null, null, null, null)), (_l()(), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵted"](-1, null, ["\n                            "])), (_l()(), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵeld"](10, 0, null, null, 1, "span", [["aria-hidden", "true"]], null, null, null, null, null)), (_l()(), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵted"](-1, null, ["\u00AB"])), (_l()(), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵted"](-1, null, ["\n                            "])), (_l()(), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵted"](-1, null, ["\n                        "])), (_l()(), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵted"](-1, null, ["\n                    "])), (_l()(), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵted"](-1, null, ["\n                    "])), (_l()(), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵand"](16777216, null, null, 1, null, View_FormularioGCSComponent_7)), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵdid"](17, 802816, null, 0, _angular_common__WEBPACK_IMPORTED_MODULE_2__["NgForOf"], [_angular_core__WEBPACK_IMPORTED_MODULE_1__["ViewContainerRef"], _angular_core__WEBPACK_IMPORTED_MODULE_1__["TemplateRef"], _angular_core__WEBPACK_IMPORTED_MODULE_1__["IterableDiffers"]], { ngForOf: [0, "ngForOf"] }, null), (_l()(), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵted"](-1, null, ["\n                    "])), (_l()(), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵted"](-1, null, ["\n                    "])), (_l()(), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵted"](-1, null, ["\n                    "])), (_l()(), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵeld"](21, 0, null, null, 8, "li", [["class", "page-item d-none"], ["id", "pag-next-buscador"]], null, [[null, "click"]], function (_v, en, $event) {
            var ad = true;
            var _co = _v.component;
            if (("click" === en)) {
                var pd_0 = (_co.changeToNextPage() !== false);
                ad = (pd_0 && ad);
            }
            return ad;
        }, null, null)), (_l()(), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵted"](-1, null, ["\n                        "])), (_l()(), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵeld"](23, 0, null, null, 5, "a", [["aria-label", "Next"], ["class", "page-link"]], null, null, null, null, null)), (_l()(), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵted"](-1, null, ["\n                            "])), (_l()(), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵeld"](25, 0, null, null, 1, "span", [["aria-hidden", "true"]], null, null, null, null, null)), (_l()(), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵted"](-1, null, ["\u00BB"])), (_l()(), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵted"](-1, null, ["\n                            "])), (_l()(), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵted"](-1, null, ["\n                        "])), (_l()(), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵted"](-1, null, ["\n                    "])), (_l()(), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵted"](-1, null, ["\n                    "])), (_l()(), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵted"](-1, null, ["\n                "])), (_l()(), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵted"](-1, null, ["\n            "]))], function (_ck, _v) { var _co = _v.component; var currVal_0 = _co.numberPages; _ck(_v, 17, 0, currVal_0); }, null);
}
function View_FormularioGCSComponent_0(_l) {
    return _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵvid"](0, [(_l()(), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵeld"](0, 0, null, null, 140, "div", [["class", "container d-flex flex-column align-items-end BuscadorGCS"]], null, null, null, null, null)), (_l()(), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵted"](-1, null, ["\n    "])), (_l()(), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵeld"](2, 0, null, null, 0, "span", [["class", "icon-u-cerrar pull-right mdlclose mdlc"], ["data-dismiss", "modal"]], null, null, null, null, null)), (_l()(), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵted"](-1, null, ["\n    "])), (_l()(), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵeld"](4, 0, null, null, 118, "div", [["class", "w-100 d-flex flex-column align-items-center"]], null, null, null, null, null)), (_l()(), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵted"](-1, null, ["\n        "])), (_l()(), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵeld"](6, 0, null, null, 4, "div", [["class", "logoBuscador text-center col-9"]], null, null, null, null, null)), (_l()(), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵted"](-1, null, ["\n            "])), (_l()(), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵeld"](8, 0, null, null, 1, "div", [["class", "logo-unitec"]], null, null, null, null, null)), (_l()(), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵted"](-1, null, ["\n                \n            "])), (_l()(), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵted"](-1, null, ["\n        "])), (_l()(), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵted"](-1, null, ["\n\n        "])), (_l()(), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵted"](-1, null, ["\n        "])), (_l()(), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵeld"](13, 0, null, null, 32, "form", [["class", "col-9 p-3"], ["novalidate", ""]], [[2, "ng-untouched", null], [2, "ng-touched", null], [2, "ng-pristine", null], [2, "ng-dirty", null], [2, "ng-valid", null], [2, "ng-invalid", null], [2, "ng-pending", null]], [[null, "submit"], [null, "reset"]], function (_v, en, $event) {
            var ad = true;
            if (("submit" === en)) {
                var pd_0 = (_angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵnov"](_v, 15).onSubmit($event) !== false);
                ad = (pd_0 && ad);
            }
            if (("reset" === en)) {
                var pd_1 = (_angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵnov"](_v, 15).onReset() !== false);
                ad = (pd_1 && ad);
            }
            return ad;
        }, null, null)), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵdid"](14, 16384, null, 0, _angular_forms__WEBPACK_IMPORTED_MODULE_3__["ɵbf"], [], null, null), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵdid"](15, 540672, null, 0, _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormGroupDirective"], [[8, null], [8, null]], { form: [0, "form"] }, null), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵprd"](2048, null, _angular_forms__WEBPACK_IMPORTED_MODULE_3__["ControlContainer"], null, [_angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormGroupDirective"]]), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵdid"](17, 16384, null, 0, _angular_forms__WEBPACK_IMPORTED_MODULE_3__["NgControlStatusGroup"], [_angular_forms__WEBPACK_IMPORTED_MODULE_3__["ControlContainer"]], null, null), (_l()(), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵted"](-1, null, ["\n            "])), (_l()(), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵeld"](19, 0, null, null, 25, "div", [["formGroupName", "faseUno"]], [[2, "ng-untouched", null], [2, "ng-touched", null], [2, "ng-pristine", null], [2, "ng-dirty", null], [2, "ng-valid", null], [2, "ng-invalid", null], [2, "ng-pending", null]], null, null, null, null)), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵdid"](20, 212992, null, 0, _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormGroupName"], [[3, _angular_forms__WEBPACK_IMPORTED_MODULE_3__["ControlContainer"]], [8, null], [8, null]], { name: [0, "name"] }, null), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵprd"](2048, null, _angular_forms__WEBPACK_IMPORTED_MODULE_3__["ControlContainer"], null, [_angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormGroupName"]]), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵdid"](22, 16384, null, 0, _angular_forms__WEBPACK_IMPORTED_MODULE_3__["NgControlStatusGroup"], [_angular_forms__WEBPACK_IMPORTED_MODULE_3__["ControlContainer"]], null, null), (_l()(), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵted"](-1, null, [" "])), (_l()(), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵted"](-1, null, ["\n                "])), (_l()(), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵeld"](25, 0, null, null, 18, "div", [["class", "input-group newSearch align-items-center"]], null, null, null, null, null)), (_l()(), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵted"](-1, null, [" "])), (_l()(), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵted"](-1, null, ["\n                    "])), (_l()(), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵeld"](28, 0, null, null, 5, "input", [["autocomplete", "on"], ["class", "inputGCS w-100"], ["formControlName", "frm_GCS"], ["id", "frm_GCS"], ["placeholder", "Buscar..."], ["type", "search"]], [[2, "ng-untouched", null], [2, "ng-touched", null], [2, "ng-pristine", null], [2, "ng-dirty", null], [2, "ng-valid", null], [2, "ng-invalid", null], [2, "ng-pending", null]], [[null, "keydown"], [null, "input"], [null, "blur"], [null, "compositionstart"], [null, "compositionend"]], function (_v, en, $event) {
            var ad = true;
            var _co = _v.component;
            if (("input" === en)) {
                var pd_0 = (_angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵnov"](_v, 29)._handleInput($event.target.value) !== false);
                ad = (pd_0 && ad);
            }
            if (("blur" === en)) {
                var pd_1 = (_angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵnov"](_v, 29).onTouched() !== false);
                ad = (pd_1 && ad);
            }
            if (("compositionstart" === en)) {
                var pd_2 = (_angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵnov"](_v, 29)._compositionStart() !== false);
                ad = (pd_2 && ad);
            }
            if (("compositionend" === en)) {
                var pd_3 = (_angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵnov"](_v, 29)._compositionEnd($event.target.value) !== false);
                ad = (pd_3 && ad);
            }
            if (("keydown" === en)) {
                var pd_4 = (_co.keyDownFunction($event) !== false);
                ad = (pd_4 && ad);
            }
            return ad;
        }, null, null)), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵdid"](29, 16384, null, 0, _angular_forms__WEBPACK_IMPORTED_MODULE_3__["DefaultValueAccessor"], [_angular_core__WEBPACK_IMPORTED_MODULE_1__["Renderer2"], _angular_core__WEBPACK_IMPORTED_MODULE_1__["ElementRef"], [2, _angular_forms__WEBPACK_IMPORTED_MODULE_3__["COMPOSITION_BUFFER_MODE"]]], null, null), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵprd"](1024, null, _angular_forms__WEBPACK_IMPORTED_MODULE_3__["NG_VALUE_ACCESSOR"], function (p0_0) { return [p0_0]; }, [_angular_forms__WEBPACK_IMPORTED_MODULE_3__["DefaultValueAccessor"]]), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵdid"](31, 671744, null, 0, _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormControlName"], [[3, _angular_forms__WEBPACK_IMPORTED_MODULE_3__["ControlContainer"]], [8, null], [8, null], [2, _angular_forms__WEBPACK_IMPORTED_MODULE_3__["NG_VALUE_ACCESSOR"]]], { name: [0, "name"] }, null), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵprd"](2048, null, _angular_forms__WEBPACK_IMPORTED_MODULE_3__["NgControl"], null, [_angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormControlName"]]), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵdid"](33, 16384, null, 0, _angular_forms__WEBPACK_IMPORTED_MODULE_3__["NgControlStatus"], [_angular_forms__WEBPACK_IMPORTED_MODULE_3__["NgControl"]], null, null), (_l()(), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵted"](-1, null, ["\n                    "])), (_l()(), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵeld"](35, 0, null, null, 7, "span", [["class", "input-group-addon"]], null, null, null, null, null)), (_l()(), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵted"](-1, null, ["\n                        "])), (_l()(), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵand"](16777216, null, null, 1, null, View_FormularioGCSComponent_1)), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵdid"](38, 16384, null, 0, _angular_common__WEBPACK_IMPORTED_MODULE_2__["NgIf"], [_angular_core__WEBPACK_IMPORTED_MODULE_1__["ViewContainerRef"], _angular_core__WEBPACK_IMPORTED_MODULE_1__["TemplateRef"]], { ngIf: [0, "ngIf"] }, null), (_l()(), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵted"](-1, null, ["\n                        "])), (_l()(), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵand"](16777216, null, null, 1, null, View_FormularioGCSComponent_3)), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵdid"](41, 16384, null, 0, _angular_common__WEBPACK_IMPORTED_MODULE_2__["NgIf"], [_angular_core__WEBPACK_IMPORTED_MODULE_1__["ViewContainerRef"], _angular_core__WEBPACK_IMPORTED_MODULE_1__["TemplateRef"]], { ngIf: [0, "ngIf"] }, null), (_l()(), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵted"](-1, null, ["\n                    "])), (_l()(), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵted"](-1, null, ["\n                "])), (_l()(), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵted"](-1, null, ["  \n            "])), (_l()(), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵted"](-1, null, ["\n        "])), (_l()(), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵted"](-1, null, ["\n\n        "])), (_l()(), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵeld"](47, 0, null, null, 40, "div", [["class", "text-center"], ["id", "preloadBuscador"], ["style", "display:none"]], null, null, null, null, null)), (_l()(), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵted"](-1, null, ["\n            "])), (_l()(), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵeld"](49, 0, null, null, 37, ":svg:svg", [["class", "uil-default"], ["height", "120px"], ["preserveAspectRatio", "xMidYMid"], ["viewBox", "0 0 100 100"], ["width", "120px"], ["xmlns", "//www.w3.org/2000/svg"]], null, null, null, null, null)), (_l()(), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵeld"](50, 0, null, null, 0, ":svg:rect", [["class", "bk"], ["fill", "none"], ["height", "100"], ["width", "100"], ["x", "0"], ["y", "0"]], null, null, null, null, null)), (_l()(), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵeld"](51, 0, null, null, 2, ":svg:rect", [["fill", "#00b2ff"], ["height", "20"], ["rx", "5"], ["ry", "5"], ["transform", "rotate(0 50 50) translate(0 -30)"], ["width", "7"], ["x", "46.5"], ["y", "40"]], null, null, null, null, null)), (_l()(), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵted"](-1, null, ["  "])), (_l()(), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵeld"](53, 0, null, null, 0, ":svg:animate", [["attributeName", "opacity"], ["begin", "-1s"], ["dur", "1s"], ["from", "1"], ["repeatCount", "indefinite"], ["to", "0"]], null, null, null, null, null)), (_l()(), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵeld"](54, 0, null, null, 2, ":svg:rect", [["fill", "#00b2ff"], ["height", "20"], ["rx", "5"], ["ry", "5"], ["transform", "rotate(30 50 50) translate(0 -30)"], ["width", "7"], ["x", "46.5"], ["y", "40"]], null, null, null, null, null)), (_l()(), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵted"](-1, null, ["  "])), (_l()(), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵeld"](56, 0, null, null, 0, ":svg:animate", [["attributeName", "opacity"], ["begin", "-0.9166666666666666s"], ["dur", "1s"], ["from", "1"], ["repeatCount", "indefinite"], ["to", "0"]], null, null, null, null, null)), (_l()(), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵeld"](57, 0, null, null, 2, ":svg:rect", [["fill", "#00b2ff"], ["height", "20"], ["rx", "5"], ["ry", "5"], ["transform", "rotate(60 50 50) translate(0 -30)"], ["width", "7"], ["x", "46.5"], ["y", "40"]], null, null, null, null, null)), (_l()(), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵted"](-1, null, ["  "])), (_l()(), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵeld"](59, 0, null, null, 0, ":svg:animate", [["attributeName", "opacity"], ["begin", "-0.8333333333333334s"], ["dur", "1s"], ["from", "1"], ["repeatCount", "indefinite"], ["to", "0"]], null, null, null, null, null)), (_l()(), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵeld"](60, 0, null, null, 2, ":svg:rect", [["fill", "#00b2ff"], ["height", "20"], ["rx", "5"], ["ry", "5"], ["transform", "rotate(90 50 50) translate(0 -30)"], ["width", "7"], ["x", "46.5"], ["y", "40"]], null, null, null, null, null)), (_l()(), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵted"](-1, null, ["  "])), (_l()(), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵeld"](62, 0, null, null, 0, ":svg:animate", [["attributeName", "opacity"], ["begin", "-0.75s"], ["dur", "1s"], ["from", "1"], ["repeatCount", "indefinite"], ["to", "0"]], null, null, null, null, null)), (_l()(), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵeld"](63, 0, null, null, 2, ":svg:rect", [["fill", "#00b2ff"], ["height", "20"], ["rx", "5"], ["ry", "5"], ["transform", "rotate(120 50 50) translate(0 -30)"], ["width", "7"], ["x", "46.5"], ["y", "40"]], null, null, null, null, null)), (_l()(), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵted"](-1, null, ["  "])), (_l()(), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵeld"](65, 0, null, null, 0, ":svg:animate", [["attributeName", "opacity"], ["begin", "-0.6666666666666666s"], ["dur", "1s"], ["from", "1"], ["repeatCount", "indefinite"], ["to", "0"]], null, null, null, null, null)), (_l()(), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵeld"](66, 0, null, null, 2, ":svg:rect", [["fill", "#00b2ff"], ["height", "20"], ["rx", "5"], ["ry", "5"], ["transform", "rotate(150 50 50) translate(0 -30)"], ["width", "7"], ["x", "46.5"], ["y", "40"]], null, null, null, null, null)), (_l()(), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵted"](-1, null, ["  "])), (_l()(), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵeld"](68, 0, null, null, 0, ":svg:animate", [["attributeName", "opacity"], ["begin", "-0.5833333333333334s"], ["dur", "1s"], ["from", "1"], ["repeatCount", "indefinite"], ["to", "0"]], null, null, null, null, null)), (_l()(), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵeld"](69, 0, null, null, 2, ":svg:rect", [["fill", "#00b2ff"], ["height", "20"], ["rx", "5"], ["ry", "5"], ["transform", "rotate(180 50 50) translate(0 -30)"], ["width", "7"], ["x", "46.5"], ["y", "40"]], null, null, null, null, null)), (_l()(), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵted"](-1, null, ["  "])), (_l()(), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵeld"](71, 0, null, null, 0, ":svg:animate", [["attributeName", "opacity"], ["begin", "-0.5s"], ["dur", "1s"], ["from", "1"], ["repeatCount", "indefinite"], ["to", "0"]], null, null, null, null, null)), (_l()(), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵeld"](72, 0, null, null, 2, ":svg:rect", [["fill", "#00b2ff"], ["height", "20"], ["rx", "5"], ["ry", "5"], ["transform", "rotate(210 50 50) translate(0 -30)"], ["width", "7"], ["x", "46.5"], ["y", "40"]], null, null, null, null, null)), (_l()(), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵted"](-1, null, ["  "])), (_l()(), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵeld"](74, 0, null, null, 0, ":svg:animate", [["attributeName", "opacity"], ["begin", "-0.4166666666666667s"], ["dur", "1s"], ["from", "1"], ["repeatCount", "indefinite"], ["to", "0"]], null, null, null, null, null)), (_l()(), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵeld"](75, 0, null, null, 2, ":svg:rect", [["fill", "#00b2ff"], ["height", "20"], ["rx", "5"], ["ry", "5"], ["transform", "rotate(240 50 50) translate(0 -30)"], ["width", "7"], ["x", "46.5"], ["y", "40"]], null, null, null, null, null)), (_l()(), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵted"](-1, null, ["  "])), (_l()(), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵeld"](77, 0, null, null, 0, ":svg:animate", [["attributeName", "opacity"], ["begin", "-0.3333333333333333s"], ["dur", "1s"], ["from", "1"], ["repeatCount", "indefinite"], ["to", "0"]], null, null, null, null, null)), (_l()(), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵeld"](78, 0, null, null, 2, ":svg:rect", [["fill", "#00b2ff"], ["height", "20"], ["rx", "5"], ["ry", "5"], ["transform", "rotate(270 50 50) translate(0 -30)"], ["width", "7"], ["x", "46.5"], ["y", "40"]], null, null, null, null, null)), (_l()(), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵted"](-1, null, ["  "])), (_l()(), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵeld"](80, 0, null, null, 0, ":svg:animate", [["attributeName", "opacity"], ["begin", "-0.25s"], ["dur", "1s"], ["from", "1"], ["repeatCount", "indefinite"], ["to", "0"]], null, null, null, null, null)), (_l()(), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵeld"](81, 0, null, null, 2, ":svg:rect", [["fill", "#00b2ff"], ["height", "20"], ["rx", "5"], ["ry", "5"], ["transform", "rotate(300 50 50) translate(0 -30)"], ["width", "7"], ["x", "46.5"], ["y", "40"]], null, null, null, null, null)), (_l()(), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵted"](-1, null, ["  "])), (_l()(), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵeld"](83, 0, null, null, 0, ":svg:animate", [["attributeName", "opacity"], ["begin", "-0.16666666666666666s"], ["dur", "1s"], ["from", "1"], ["repeatCount", "indefinite"], ["to", "0"]], null, null, null, null, null)), (_l()(), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵeld"](84, 0, null, null, 2, ":svg:rect", [["fill", "#00b2ff"], ["height", "20"], ["rx", "5"], ["ry", "5"], ["transform", "rotate(330 50 50) translate(0 -30)"], ["width", "7"], ["x", "46.5"], ["y", "40"]], null, null, null, null, null)), (_l()(), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵted"](-1, null, ["  "])), (_l()(), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵeld"](86, 0, null, null, 0, ":svg:animate", [["attributeName", "opacity"], ["begin", "-0.08333333333333333s"], ["dur", "1s"], ["from", "1"], ["repeatCount", "indefinite"], ["to", "0"]], null, null, null, null, null)), (_l()(), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵted"](-1, null, ["\n        "])), (_l()(), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵted"](-1, null, ["\n\n        "])), (_l()(), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵted"](-1, null, ["\n        "])), (_l()(), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵted"](-1, null, ["\n            "])), (_l()(), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵted"](-1, null, ["\n            "])), (_l()(), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵted"](-1, null, ["\n            "])), (_l()(), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵted"](-1, null, ["\n        "])), (_l()(), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵted"](-1, null, ["\n\n        \n        "])), (_l()(), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵted"](-1, null, ["\n\n        "])), (_l()(), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵted"](-1, null, ["\n\n        "])), (_l()(), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵted"](-1, null, ["\n        "])), (_l()(), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵeld"](98, 0, null, null, 22, "div", [["class", "col-9"], ["id", "GCSResultados"]], null, null, null, null, null)), (_l()(), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵted"](-1, null, [" \n            "])), (_l()(), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵeld"](100, 0, null, null, 10, "div", [["class", "msgGCS"], ["style", "display: none"]], null, null, null, null, null)), (_l()(), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵted"](-1, null, ["\n                "])), (_l()(), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵeld"](102, 0, null, null, 2, "p", [["class", "msgResults"]], null, null, null, null, null)), (_l()(), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵted"](-1, null, ["No se encontrar\u00F3n resultados para tu busqueda"])), (_l()(), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵeld"](104, 0, null, null, 0, "b", [], null, null, null, null, null)), (_l()(), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵted"](-1, null, ["\n                "])), (_l()(), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵeld"](106, 0, null, null, 0, "div", [["class", "clearfix"]], null, null, null, null, null)), (_l()(), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵted"](-1, null, ["\n                "])), (_l()(), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵeld"](108, 0, null, null, 1, "b", [], null, null, null, null, null)), (_l()(), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵted"](-1, null, ["Busquedas recientes en UNITEC..."])), (_l()(), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵted"](-1, null, ["\n            "])), (_l()(), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵted"](-1, null, ["\n\n            "])), (_l()(), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵand"](16777216, null, null, 1, null, View_FormularioGCSComponent_4)), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵdid"](113, 16384, null, 0, _angular_common__WEBPACK_IMPORTED_MODULE_2__["NgIf"], [_angular_core__WEBPACK_IMPORTED_MODULE_1__["ViewContainerRef"], _angular_core__WEBPACK_IMPORTED_MODULE_1__["TemplateRef"]], { ngIf: [0, "ngIf"] }, null), (_l()(), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵted"](-1, null, ["\n            "])), (_l()(), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵted"](-1, null, ["\n            "])), (_l()(), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵeld"](116, 0, null, null, 0, "div", [["class", "GCSresultItem"]], null, null, null, null, null)), (_l()(), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵted"](-1, null, ["\n            "])), (_l()(), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵand"](16777216, null, null, 1, null, View_FormularioGCSComponent_6)), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵdid"](119, 16384, null, 0, _angular_common__WEBPACK_IMPORTED_MODULE_2__["NgIf"], [_angular_core__WEBPACK_IMPORTED_MODULE_1__["ViewContainerRef"], _angular_core__WEBPACK_IMPORTED_MODULE_1__["TemplateRef"]], { ngIf: [0, "ngIf"] }, null), (_l()(), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵted"](-1, null, ["\n        "])), (_l()(), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵted"](-1, null, ["\n        "])), (_l()(), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵted"](-1, null, ["\n    "])), (_l()(), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵted"](-1, null, ["\n\n    "])), (_l()(), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵeld"](124, 0, null, null, 15, "div", [["class", "text-center col-12"], ["style", "display: none"]], null, null, null, null, null)), (_l()(), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵted"](-1, null, ["\n        "])), (_l()(), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵeld"](126, 0, null, null, 0, "span", [["class", "icon-u-cerrar pull-right mdlclose"]], null, [[null, "click"]], function (_v, en, $event) {
            var ad = true;
            var _co = _v.component;
            if (("click" === en)) {
                var pd_0 = (_co.mic() !== false);
                ad = (pd_0 && ad);
            }
            return ad;
        }, null, null)), (_l()(), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵted"](-1, null, ["\n        "])), (_l()(), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵeld"](128, 0, null, null, 7, "section", [["class", "speachMic"]], null, null, null, null, null)), (_l()(), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵted"](-1, null, ["   \n            "])), (_l()(), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵeld"](130, 0, null, null, 4, "span", [["class", "icon-u-microfono microfono rotating"], ["id", "micGCS"]], null, [[null, "click"]], function (_v, en, $event) {
            var ad = true;
            var _co = _v.component;
            if (("click" === en)) {
                var pd_0 = (_co.mic() !== false);
                ad = (pd_0 && ad);
            }
            return ad;
        }, null, null)), (_l()(), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵted"](-1, null, ["\n                "])), (_l()(), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵeld"](132, 0, null, null, 0, "span", [["class", "path1"]], null, null, null, null, null)), (_l()(), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵeld"](133, 0, null, null, 0, "span", [["class", "path2"]], null, null, null, null, null)), (_l()(), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵted"](-1, null, ["\n            "])), (_l()(), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵted"](-1, null, [" \n        "])), (_l()(), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵted"](-1, null, ["\n        "])), (_l()(), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵeld"](137, 0, null, null, 1, "h3", [], null, null, null, null, null)), (_l()(), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵted"](-1, null, ["Escuchando..."])), (_l()(), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵted"](-1, null, ["\n    "])), (_l()(), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵted"](-1, null, ["        \n"]))], function (_ck, _v) { var _co = _v.component; var currVal_7 = _co.formGCS; _ck(_v, 15, 0, currVal_7); var currVal_15 = "faseUno"; _ck(_v, 20, 0, currVal_15); var currVal_23 = "frm_GCS"; _ck(_v, 31, 0, currVal_23); var currVal_24 = (_co.formGCS.get("faseUno.frm_GCS").value == ""); _ck(_v, 38, 0, currVal_24); var currVal_25 = (_co.formGCS.get("faseUno.frm_GCS").value != ""); _ck(_v, 41, 0, currVal_25); var currVal_26 = (_co.formGCS.get("faseUno.frm_GCS").errors && _co.formGCS.get("faseUno.frm_GCS").dirty); _ck(_v, 113, 0, currVal_26); var currVal_27 = (_co.muestraNav !== false); _ck(_v, 119, 0, currVal_27); }, function (_ck, _v) { var currVal_0 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵnov"](_v, 17).ngClassUntouched; var currVal_1 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵnov"](_v, 17).ngClassTouched; var currVal_2 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵnov"](_v, 17).ngClassPristine; var currVal_3 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵnov"](_v, 17).ngClassDirty; var currVal_4 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵnov"](_v, 17).ngClassValid; var currVal_5 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵnov"](_v, 17).ngClassInvalid; var currVal_6 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵnov"](_v, 17).ngClassPending; _ck(_v, 13, 0, currVal_0, currVal_1, currVal_2, currVal_3, currVal_4, currVal_5, currVal_6); var currVal_8 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵnov"](_v, 22).ngClassUntouched; var currVal_9 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵnov"](_v, 22).ngClassTouched; var currVal_10 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵnov"](_v, 22).ngClassPristine; var currVal_11 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵnov"](_v, 22).ngClassDirty; var currVal_12 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵnov"](_v, 22).ngClassValid; var currVal_13 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵnov"](_v, 22).ngClassInvalid; var currVal_14 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵnov"](_v, 22).ngClassPending; _ck(_v, 19, 0, currVal_8, currVal_9, currVal_10, currVal_11, currVal_12, currVal_13, currVal_14); var currVal_16 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵnov"](_v, 33).ngClassUntouched; var currVal_17 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵnov"](_v, 33).ngClassTouched; var currVal_18 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵnov"](_v, 33).ngClassPristine; var currVal_19 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵnov"](_v, 33).ngClassDirty; var currVal_20 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵnov"](_v, 33).ngClassValid; var currVal_21 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵnov"](_v, 33).ngClassInvalid; var currVal_22 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵnov"](_v, 33).ngClassPending; _ck(_v, 28, 0, currVal_16, currVal_17, currVal_18, currVal_19, currVal_20, currVal_21, currVal_22); });
}
function View_FormularioGCSComponent_Host_0(_l) { return _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵvid"](0, [(_l()(), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵeld"](0, 0, null, null, 5, "formularioGCS", [], null, null, null, View_FormularioGCSComponent_0, RenderType_FormularioGCSComponent)), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵprd"](512, null, _formularios_forms_services_validaciones_service__WEBPACK_IMPORTED_MODULE_4__["userValidations"], _formularios_forms_services_validaciones_service__WEBPACK_IMPORTED_MODULE_4__["userValidations"], [_services_getJson_service__WEBPACK_IMPORTED_MODULE_5__["getJson"], _angular_platform_browser__WEBPACK_IMPORTED_MODULE_6__["DOCUMENT"]]), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵprd"](512, null, _services_webspeach_service__WEBPACK_IMPORTED_MODULE_7__["WebspeachService"], _services_webspeach_service__WEBPACK_IMPORTED_MODULE_7__["WebspeachService"], [_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgZone"]]), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵprd"](131584, null, _services_autocomplete_service__WEBPACK_IMPORTED_MODULE_8__["AutocompleteService"], _services_autocomplete_service__WEBPACK_IMPORTED_MODULE_8__["AutocompleteService"], [_angular_http__WEBPACK_IMPORTED_MODULE_9__["Http"]]), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵprd"](512, null, _services_navegador_service__WEBPACK_IMPORTED_MODULE_10__["NavegadorService"], _services_navegador_service__WEBPACK_IMPORTED_MODULE_10__["NavegadorService"], []), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵdid"](5, 245760, null, 0, _formularioGCS_component__WEBPACK_IMPORTED_MODULE_11__["FormularioGCSComponent"], [_angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormBuilder"], _formularios_forms_services_validaciones_service__WEBPACK_IMPORTED_MODULE_4__["userValidations"], _services_webspeach_service__WEBPACK_IMPORTED_MODULE_7__["WebspeachService"], _angular_http__WEBPACK_IMPORTED_MODULE_9__["Http"], _services_autocomplete_service__WEBPACK_IMPORTED_MODULE_8__["AutocompleteService"], _services_navegador_service__WEBPACK_IMPORTED_MODULE_10__["NavegadorService"]], null, null)], function (_ck, _v) { _ck(_v, 5, 0); }, null); }
var FormularioGCSComponentNgFactory = /*@__PURE__*/ /*@__PURE__*/ _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵccf"]("formularioGCS", _formularioGCS_component__WEBPACK_IMPORTED_MODULE_11__["FormularioGCSComponent"], View_FormularioGCSComponent_Host_0, { gcsExtrangero: "gcsExtrangero" }, {}, []);



/***/ }),

/***/ "./src/app/buscador/formularioGCS/formularioGCS.component.scss.shim.ngstyle.js":
/*!*************************************************************************************!*\
  !*** ./src/app/buscador/formularioGCS/formularioGCS.component.scss.shim.ngstyle.js ***!
  \*************************************************************************************/
/*! exports provided: styles */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "styles", function() { return styles; });
/**
 * @fileoverview This file was generated by the Angular template compiler. Do not edit.
 *
 * @suppress {suspiciousCode,uselessCode,missingProperties,missingOverride,checkTypes}
 * tslint:disable
 */
/** PURE_IMPORTS_START  PURE_IMPORTS_END */
/** PURE_IMPORTS_START  PURE_IMPORTS_END */
var styles = ["@-webkit-keyframes rotating {\n  0% {\n    -webkit-transform: rotate(0deg);\n    transform: rotate(0deg); }\n  20% {\n    -webkit-transform: rotate(360deg);\n    transform: rotate(360deg); }\n  40% {\n    -webkit-transform: rotateX(0deg);\n    \n    transform: rotateX(0deg); }\n  60% {\n    -webkit-transform: rotateX(360deg);\n    \n    transform: rotateX(360deg); }\n  80% {\n    -webkit-transform: rotateY(360deg);\n    \n    transform: rotateY(360deg); } }\n\n@keyframes rotating {\n  0% {\n    -webkit-transform: rotate(0deg);\n    transform: rotate(0deg); }\n  20% {\n    -webkit-transform: rotate(360deg);\n    transform: rotate(360deg); }\n  40% {\n    -webkit-transform: rotateX(0deg);\n    \n    transform: rotateX(0deg); }\n  60% {\n    -webkit-transform: rotateX(360deg);\n    \n    transform: rotateX(360deg); }\n  80% {\n    -webkit-transform: rotateY(360deg);\n    \n    transform: rotateY(360deg); } }\n\n.rotating[_ngcontent-%COMP%] {\n  -webkit-animation: rotating 5s linear infinite;\n  animation: rotating 5s linear infinite; }\n\n.input-group-addon[_ngcontent-%COMP%] {\n  background-color: transparent !important; }\n\n[_nghost-%COMP%]     span.mdlclose:before {\n  color: #F68B1F !important;\n  font-size: 40px; }\n\n[_nghost-%COMP%]     .BuscadorGCS {\n  width: 100%; }\n\n[_nghost-%COMP%]     .BuscadorGCS div .logoBuscador {\n    position: relative;\n    width: 100%;\n    height: 125px;\n    align-items: center;\n    justify-content: center;\n    -ms-display: flex;\n    display: flex; }\n\n[_nghost-%COMP%]     .BuscadorGCS div .logoBuscador img {\n      width: 200px;\n      height: auto; }\n\n[_nghost-%COMP%]     .BuscadorGCS div .input-group.newSearch {\n    height: 50px;\n    border: 1px solid #006fba; }\n\n[_nghost-%COMP%]     .BuscadorGCS div .input-group.newSearch .twitter-typeahead {\n      width: 100% !important; }\n\n[_nghost-%COMP%]     .BuscadorGCS div .input-group.newSearch input[type=\"search\"] {\n      border: none;\n      font-size: 1.5rem;\n      font-family: inherit;\n      box-shadow: none;\n      opacity: 0.3;\n      padding-left: 1rem; }\n\n[_nghost-%COMP%]     .BuscadorGCS div .input-group.newSearch input[type=\"search\"]:focus:not([readonly]) {\n        opacity: 1; }\n\n[_nghost-%COMP%]     .BuscadorGCS div .input-group.newSearch input[type=\"search\"]:focus:not([readonly]).mySuccess {\n        border: none;\n        box-shadow: none; }\n\n[_nghost-%COMP%]     .BuscadorGCS div .input-group.newSearch input[type=\"search\"].mySuccess {\n        border: none;\n        box-shadow: none; }\n\n[_nghost-%COMP%]     .BuscadorGCS div .input-group.newSearch input[type=\"search\"]:focus:not([readonly]).myError {\n        border: none;\n        box-shadow: none; }\n\n[_nghost-%COMP%]     .BuscadorGCS div .input-group.newSearch input[type=\"search\"].myError {\n        border: none;\n        box-shadow: none; }\n\n[_nghost-%COMP%]     .BuscadorGCS div .input-group.newSearch input[type=\"search\"].valid {\n        border: none;\n        box-shadow: none; }\n\n[_nghost-%COMP%]     .BuscadorGCS div .input-group.newSearch .input-group-addon {\n      border: none; }\n\n[_nghost-%COMP%]     .BuscadorGCS div .input-group.newSearch .microfono {\n      font-size: 24px; }\n\n[_nghost-%COMP%]     .BuscadorGCS div .GCSCintillo {\n    padding: 0 1rem;\n    overflow-x: auto;\n    width: 100%; }\n\n[_nghost-%COMP%]     .BuscadorGCS div .GCSCintillo .GCSNav {\n      display: inline-flex; }\n\n[_nghost-%COMP%]     .BuscadorGCS div .GCSCintillo .GCSNav span.active {\n        border-bottom: 1px solid #006fba;\n        font-weight: 500;\n        color: #006fba; }\n\n[_nghost-%COMP%]     .BuscadorGCS div .GCSCintillo .GCSNav span {\n        padding: 5px 24px;\n        border-bottom: 1px solid #757575; }\n\n[_nghost-%COMP%]     .BuscadorGCS div .GCSCintillo::-webkit-scrollbar-track {\n      -webkit-box-shadow: inset 0 0 6px transparent;\n      background-color: transparent; }\n\n[_nghost-%COMP%]     .BuscadorGCS div .GCSCintillo::-webkit-scrollbar {\n      width: 0px;\n      background-color: transparent; }\n\n[_nghost-%COMP%]     .BuscadorGCS div .GCSCintillo::-webkit-scrollbar-thumb {\n      background-color: transparent; }\n\n[_nghost-%COMP%]     .BuscadorGCS div #GCSResultados {\n    padding: 0 16px 0 16px; }\n\n[_nghost-%COMP%]     .BuscadorGCS div #GCSResultados div.msgGCS .msgResults {\n      position: relative;\n      padding: 0 0 16px;\n      padding-top: 0;\n      margin: 16px 0;\n      border-bottom: 1px solid #CCC; }\n\n[_nghost-%COMP%]     .BuscadorGCS div #GCSResultados div.msgGCS b {\n      position: relative;\n      padding-bottom: 40px; }\n\n[_nghost-%COMP%]     .BuscadorGCS div #GCSResultados .GCSresultItem {\n      width: 100%;\n      clear: both; }\n\n[_nghost-%COMP%]     .BuscadorGCS div #GCSResultados .GCSresultItem > header, [_nghost-%COMP%]     .BuscadorGCS div #GCSResultados .GCSresultItem section {\n        padding: 16px;\n        padding-left: 0px; }\n\n[_nghost-%COMP%]     .BuscadorGCS div #GCSResultados .GCSresultItem header {\n        padding-bottom: 0px; }\n\n[_nghost-%COMP%]     .BuscadorGCS div #GCSResultados .GCSresultItem header .titulo {\n          font-weight: 400;\n          font-size: 18px;\n          color: #F68B1F;\n          overflow: hidden;\n          text-overflow: ellipsis;\n          -webkit-line-clamp: 2;\n          display: -webkit-inline-box; }\n\n[_nghost-%COMP%]     .BuscadorGCS div #GCSResultados .GCSresultItem header .link {\n          color: #006fba;\n          overflow: hidden;\n          text-overflow: ellipsis;\n          -webkit-line-clamp: 1;\n          display: -webkit-inline-box;\n          font-size: .95rem !important; }\n\n[_nghost-%COMP%]     .BuscadorGCS div #GCSResultados .GCSresultItem section {\n        padding-top: 0px;\n        max-height: 78px;\n        overflow: hidden;\n        text-overflow: ellipsis;\n        -webkit-line-clamp: 3;\n        display: -webkit-inline-box;\n        border-bottom: 1px solid #CCC;\n        font-weight: 400; }\n\n[_nghost-%COMP%]     .BuscadorGCS div #GCSResultados .GCSresultItem:hover header {\n      padding-bottom: 0px;\n      margin-bottom: 0px; }\n\n[_nghost-%COMP%]     .BuscadorGCS div #GCSResultados .paginador .pagination.pg-blue .active .page-link {\n      background-color: #006fba;\n      color: #fff; }\n\n[_nghost-%COMP%]     .BuscadorGCS div #GCSResultados .paginador .pagination.pg-blue .page-link {\n      border-radius: 50%;\n      line-height: 1; }\n\n[_nghost-%COMP%]     .typeahead-menu {\n  width: calc( 100% + 40px) !important;\n  color: #757575 !important;\n  top: 100% !important;\n  padding: 0px 16px !important;\n  overflow-y: auto !important;\n  top: 101% !important;\n  left: -1px !important;\n  border: 1px solid #aba99e !important; }\n\n[_nghost-%COMP%]     .tt-suggestion.tt-selectable {\n  padding: 8px 0px;\n  border-bottom: 1px solid #aba99e !important; }\n\n[_nghost-%COMP%]     .tt-suggestion.tt-selectable:hover {\n  background: rgba(240, 240, 240, 0.5); }\n\n[_nghost-%COMP%]     .tt-dataset > .tt-suggestion.tt-selectable:last-child {\n  border-bottom: 1px solid transparent !important; }\n\n.speachMic[_ngcontent-%COMP%] {\n  display: none;\n  font-size: 24px;\n  position: relative;\n  width: 100%;\n  height: 125px;\n  align-items: center;\n  justify-content: center;\n  -ms-display: flex;\n  display: flex; }\n\n.speachMic[_ngcontent-%COMP%]   .microfono[_ngcontent-%COMP%] {\n    font-size: 74px; }\n\n  .frm_GCS::-webkit-input-placeholder {\n  color: #CCC;\n  text-transform: initial !important; }\n\n  .frm_GCS::-ms-input-placeholder {\n  \n  color: #CCC;\n  text-transform: initial !important; }\n\n  .frm_GCS::placeholder {\n  \n  color: #CCC;\n  text-transform: initial !important; }\n\n  ::-webkit-input-placeholder {\n  text-transform: initial; }\n\n  ::-moz-placeholder {\n  text-transform: initial; }\n\n  ::-moz-placeholder {\n  text-transform: initial; }\n\n  ::-ms-input-placeholder {\n  text-transform: initial; }\n\n\n\n\n\n  .select-buscador {\n  font-weight: 400; }\n\n  .select-buscador .select-wrapper {\n    position: relative;\n    display: flex;\n    flex-direction: row-reverse;\n    width: 120px; }\n\n  .select-buscador .select-wrapper i {\n      display: flex;\n      align-items: center;\n      color: #F68B1F;\n      font-size: 1.4rem; }\n\n  .select-buscador .select-wrapper input.select-dropdown {\n      height: auto;\n      font-family: inherit;\n      font-weight: 500;\n      color: #757575; }\n\n  .select-buscador .select-wrapper ul {\n      width: auto !important; }"];



/***/ }),

/***/ "./src/app/buscador/formularioGCS/formularioGCS.component.ts":
/*!*******************************************************************!*\
  !*** ./src/app/buscador/formularioGCS/formularioGCS.component.ts ***!
  \*******************************************************************/
/*! exports provided: FormularioGCSComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "FormularioGCSComponent", function() { return FormularioGCSComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/esm5/core.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/esm5/forms.js");
/* harmony import */ var _formularios_forms_services_validaciones_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../formularios/forms_services/validaciones.service */ "./src/app/formularios/forms_services/validaciones.service.ts");
/* harmony import */ var _services_webspeach_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../services/webspeach.service */ "./src/app/services/webspeach.service.ts");
/* harmony import */ var _services_autocomplete_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../../services/autocomplete.service */ "./src/app/services/autocomplete.service.ts");
/* harmony import */ var _services_navegador_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../../services/navegador.service */ "./src/app/services/navegador.service.ts");
/* harmony import */ var _angular_http__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @angular/http */ "./node_modules/@angular/http/esm5/http.js");
/* harmony import */ var rxjs_Rx__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! rxjs/Rx */ "./node_modules/rxjs/_esm5/Rx.js");



//import { sendService } from '../../formularios/forms_services/send.service';



// import { Animaciones} from '../../utilities/animaciones';


var FormularioGCSComponent = /** @class */ /*@__PURE__*/ (function () {
    function FormularioGCSComponent(formBuilder, userValidations, 
    //private sendService : sendService,
    speechRecognitionService, http, autocomplete, navegador) {
        this.formBuilder = formBuilder;
        this.userValidations = userValidations;
        this.speechRecognitionService = speechRecognitionService;
        this.http = http;
        this.autocomplete = autocomplete;
        this.navegador = navegador;
        this.headers = new _angular_http__WEBPACK_IMPORTED_MODULE_6__["Headers"]({ 'Accept': 'application/json' });
        this.options = new _angular_http__WEBPACK_IMPORTED_MODULE_6__["RequestOptions"]({ headers: this.headers });
        this.titulo = 'Buscador general...';
        this.mensajError = false;
        this.elemVacio = false;
        this.whatsbrowser = null;
        this.muestraNav = false;
        this.saveNewSearch = false;
        this.tolerancia = 25;
        this.numberPages = [];
        // numberPages = [
        //     {number: 1, startIndex: 1, active: true},
        //     {number: 2, startIndex: 11, active: false}
        // ]
        this.initialValue = "";
        this.lastIndexPages = 0;
        this.dataPageMap = '';
        this.categoria = '';
        this.primerPaso = 'faseUno';
        this.filtros = [
            { "id": "", "valor": "TODO", "class": "class='active'" },
            { "id": "Licenciaturas", "valor": "LICENCIATURAS", "class": "" },
            { "id": "Ingenierias", "valor": "INGENIERIAS", "class": "" }
        ];
        this.whatsbrowser = navegador.whatsBrowser();
        this.showSearchButton = true;
        this.speechData = "";
        var that = this;
        this.formGCS = formBuilder.group({
            faseUno: formBuilder.group({
                frm_GCS: ["", _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].compose([userValidations.validaBuscador])]
            })
        });
        jQuery(document).ready(function () {
            jQuery(".mdb-select").material_select();
            jQuery(".logo-unitec").html("<img src='//" + document.domain + "/wp-content/themes/temaunitec/assets/frontend_desktop/img/header/logo.svg' >");
            // jQuery('#buscador').on('shown.bs.modal', function (e) {
            //     jQuery('#tabs-buscador').ready(function(){
            //         that.viewTabsAndControls( jQuery("#tabs-buscador"), 0 );
            //         jQuery(".GCSCintillo").hide();
            //     })
            // })
            jQuery("#gcsextrangero").change(function () {
                var valor = jQuery(this).val();
                if (valor === "") {
                    return;
                }
                jQuery(".typeahead-menu").hide();
                that.setField(valor);
                jQuery("#gcsextrangero").attr("value", "");
                jQuery("#buscador").modal("show");
                // that.makeRequets();
                // const event = new KeyboardEvent("keydown",{
                //     "key": "Enter"
                // });
                // var e = jQuery.Event("keypress");
                // e.which = 13;
                // jQuery("#frm_GCS").trigger(e);
                // document.getElementById("frm_GCS").dispatchEvent(event);
                console.log(that.formGCS);
                that.formGCS.get('faseUno.frm_GCS').markAsTouched({ onlySelf: true });
                that.formGCS.get('faseUno.frm_GCS').markAsDirty();
                var error_frmgcs = that.formGCS.get('faseUno.frm_GCS').errors;
                if (!error_frmgcs) {
                    that.makeRequets();
                }
                return;
            });
            jQuery("#micExtrangero").click(function () {
                that.mic();
                jQuery("#buscador").modal("show");
                return;
            });
        });
    }
    FormularioGCSComponent.prototype.ngOnInit = function () {
        var that = this;
        jQuery(document).ready(function () {
            //Esta funcion manipula la seleccion de los tab de filtro
            jQuery('.GCSNav span').click(function () {
                jQuery(this).addClass('active').siblings().removeClass('active');
                that.categoria = jQuery(this).attr("id");
                that.makeRequets();
            });
            jQuery('.param-filter-gcs').click(function () {
                that.categoria = jQuery(this).attr("id");
                that.dataPageMap = jQuery(this).attr("dataPageMap");
                that.makeRequets();
            });
            //Llamada a autocomplete service.
            //that.autocomplete.callAutocomplete( '.inputGCS', "//www.googleapis.com/customsearch/v1?q=x&cx=002268575599804991473:n-g-9uohyde&ds&key=AIzaSyDaXorUVaC5oa53KwL4hnmx7NQDG82zC4g&dateRestrict=5&num=5&top=5" );
            // that.autocomplete.callAutocomplete( '.inputGCS', "//clients1.google.com/complete/search?hl=en&output=toolbar&client=partner&source=gcsc&partnerid=002268575599804991473:n-g-9uohyde&ds=cse&nocache" );
            //Funcion implementada para cuando se selecciona desde autocomplete...
            jQuery('.inputGCS').bind('typeahead:selected', function (obj, datum, name) {
                that.setField(datum);
                that.makeRequets();
                jQuery(".inputGCS").blur();
            });
            jQuery('#micExtrangero').bind('typeahead:selected', function (obj, datum, name) {
                that.setField(datum);
                that.makeRequets();
                jQuery(".inputGCS").blur();
            });
        });
        return;
    };
    FormularioGCSComponent.prototype.ngOnDestroy = function () {
        this.speechRecognitionService.DestroySpeechObject();
        this.resultGCS.unsubscribe();
        return;
    };
    FormularioGCSComponent.prototype.activateSpeechSearch = function () {
        var _this = this;
        this.showSearchButton = true;
        this.speechRecognitionService.record()
            .subscribe(
        //listener
        function (value) {
            _this.speechData = value;
            jQuery(".typeahead-menu").hide(500);
            _this.desactivaMicro();
            _this.setField(value);
            _this.makeRequets();
            return;
        }, function (err) {
            if (err.error == "no-speech") {
                _this.activateSpeechSearch();
            }
            return;
        }, function () {
            _this.showSearchButton = false;
            return;
        });
    };
    FormularioGCSComponent.prototype.obsSearchWithGoogle = function (value, page) {
        var _this = this;
        //variables para query string...
        var cx = "&cx=002268575599804991473:n-g-9uohyde&ds";
        var key = "&key=AIzaSyB80ECb_j8MYaVlfhuwF8-p7sIzOUj-jGo";
        var q = "q=" + value;
        var dateRestrict = "";
        var num = "";
        var top = "";
        var start = (page) ? "&start=" + page : "";
        var urlGoogleCustomSearch = "";
        var theCategory = this.categoria;
        var filtroCategoria = "";
        if (!(theCategory == "") && !(theCategory == null)) {
            filtroCategoria = "+more:pagemap:document-" + this.dataPageMap + ":" + theCategory;
        }
        else {
            this.dataPageMap = null;
        }
        var urlGoogleCustomSearchMockfull = "//www.calidadacademica.mx/wp-content/themes/temaunitec/assets/html/bannerSearch/mockupFull.json";
        var urlGoogleCustomSearchMockEmpty = "//www.calidadacademica.mx/wp-content/themes/temaunitec/assets/html/bannerSearch/mockupEmpty.json";
        if (value == "top") {
            q = "q=x", dateRestrict = "&dateRestrict=5", num = "&num=5", top = "&top=5";
            filtroCategoria = "";
        }
        urlGoogleCustomSearch = urlGoogleCustomSearchMockfull;
        //urlGoogleCustomSearch = urlGoogleCustomSearchMockEmpty;
        urlGoogleCustomSearch = "https://www.googleapis.com/customsearch/v1?" + q + cx + key + dateRestrict + num + filtroCategoria + top + start;
        console.log(urlGoogleCustomSearch);
        if (value == "" || value == null) {
            jQuery("#preloadBuscador").hide();
            jQuery(".GCSCintillo").hide();
            this.setItemsSearchResult("vacio");
            jQuery(".msgGCS").hide();
            return;
        }
        else {
            if (value == "tes" || value == "test") {
                value = "test vocacional";
            }
            var topPages_1 = ["calendario", "blackboard", "servicio social", "carreras", "servicios en linea", "facturacion", "calificaciones", "correo", "factura", "maestrias", "licenciaturas", "arquitectura", "tira de materias", "psicologia", "derecho", "mision", "bolsa de trabajo", "ingenieria en software", "reincripcion", "vision", "fisioterapia", "gastromonia", "horario", "enfermeria", "ventanilla virtual", "test vocacional"];
            this.http.get("/wp-content/resultsGCS/checkFile.php?page=" + value.toLowerCase())
                .map(function (res) { return res.json(); })
                .subscribe(function (rescheck) {
                _this.file_check = rescheck;
                if (topPages_1.indexOf(value) > -1 && _this.file_check == 1 && (start === "" || start === "&start=1")) {
                    _this.resultGCS = _this.http.get("/wp-content/resultsGCS/" + value + ".txt")
                        .map(function (res) { return res.json(); })
                        .catch(function (error) {
                        return rxjs_Rx__WEBPACK_IMPORTED_MODULE_7__["Observable"].throw(error.statusText);
                    });
                    _this.saveNewSearch = false;
                }
                else {
                    _this.resultGCS = _this.http.get(urlGoogleCustomSearch, _this.options)
                        .map(function (res) { return res.json(); })
                        .catch(function (error) {
                        return rxjs_Rx__WEBPACK_IMPORTED_MODULE_7__["Observable"].throw(error.statusText);
                    });
                    if (topPages_1.indexOf(value) > -1 && (start === "" || start === "&start=1")) {
                        _this.saveNewSearch = true;
                    }
                    else {
                        _this.saveNewSearch = false;
                    }
                }
                var response;
                _this.resultGCS.subscribe(function (res) {
                    // alert("respuesta");
                    console.log("SUCCESS OBSERVABLE");
                    response = res;
                    console.log(response);
                    jQuery("#preloadBuscador").hide();
                    /*Guardado de resultados en Archivos */
                    if (_this.saveNewSearch === true) {
                        jQuery
                            .ajax({
                            type: "POST",
                            url: "/wp-content/resultsGCS/save.php",
                            data: { "Data": response, "Consult": value },
                            success: function (res) {
                                console.log("Datos Enviados");
                                console.log(res);
                            },
                            error: function (xhr, textStatus, error) {
                                console.log(xhr);
                                console.log(xhr.statusText);
                                console.log(textStatus);
                                console.log(error);
                            }
                        });
                    }
                    /*Guardado de resultados en Archivos */
                    if (Object.keys(response).indexOf("items") >= 0) {
                        if (_this.mensajError == true) {
                            jQuery(".GCSCintillo").hide(500);
                            _this.mensajError = false;
                            _this.muestraNav = false;
                        }
                        else if (_this.elemVacio == true) {
                            jQuery(".tabs-buscador li").first().addClass('active').siblings().removeClass('active');
                            _this.categoria = "";
                            jQuery(".msgGCS").show(500);
                            jQuery(".GCSCintillo").hide(500);
                            _this.elemVacio = false;
                            _this.muestraNav = false;
                            // alert("respuesta no encontrada");
                        }
                        else {
                            _this.muestraNav = true;
                            //Solo va a llenar paginas cuando la busqueda sea diferente
                            if (_this.initialValue !== value) {
                                //Se vacian las páginas y se guarda la busqueda actual
                                _this.numberPages = [];
                                _this.initialValue = value;
                                //Se obtienen el numero total de resultados
                                var totalResults = parseInt(response.queries.request[0].totalResults);
                                var resultsDivision = 10;
                                var indexStart = -9;
                                var activeStart = true;
                                //La API solo provee de 100 resultados por lo que si es mayor sera 100
                                //Lo restringimos a 30 resultados como maximo para minimizar las llamadas a la API
                                if (totalResults >= 10) {
                                    totalResults = 10;
                                }
                                //Se crean los objetos de las paginas con los nuevos indices
                                for (var i = 0; i < totalResults / resultsDivision; i++) {
                                    _this.numberPages.push({ number: i + 1, startIndex: indexStart + 10, active: activeStart });
                                    indexStart += 10;
                                    activeStart = false;
                                    _this.lastIndexPages = i + 1;
                                }
                                //Se oculta el previo 
                                jQuery("#pag-prev-buscador").hide();
                            }
                            jQuery(".GCSCintillo").show();
                            jQuery(".msgGCS").hide(500);
                            // alert("respuesta si encontrada");
                            if (typeof dataLayer != 'undefined') {
                                dataLayer.push({
                                    'event': 'NoIntSearch',
                                    'SearchT': value,
                                    'boolSearch': 'Si',
                                });
                                // alert('Enviando: ' + 'section: ' + jQuery('#post_title').val() + 'location: ' + locationCTC);
                            }
                        }
                        _this.setItemsSearchResult(response["items"], value);
                        jQuery(".inputGCS").focus();
                        return;
                    }
                    else {
                        _this.initialValue = value;
                        if (typeof dataLayer != 'undefined') {
                            dataLayer.push({
                                'event': 'NoIntSearch',
                                'SearchT': value,
                                'boolSearch': 'No',
                            });
                            // alert('Enviando: ' + 'section: ' + jQuery('#post_title').val() + 'location: ' + locationCTC);
                        }
                        _this.numberPages = [];
                        _this.elemVacio = true;
                        _this.muestraNav = false;
                        //this.obsSearchWithGoogle( "top" );
                        _this.categoria = "";
                        jQuery(".msgGCS").show(500);
                        jQuery(".GCSCintillo").hide(500);
                        _this.elemVacio = false;
                        _this.muestraNav = false;
                        return;
                    }
                }, function (err) {
                    //alert("error");
                    console.log("NOT SUCCESS OBSERVABLE");
                    console.log(err);
                    _this.elemVacio = true;
                    jQuery("#preloadBuscador").hide();
                    // jQuery("#preloadBuscador").html("<img src='//" + document.domain + "/assets/loading.svg' class='img-fluid m-auto' style='width:100%; max-width:45px;'><br /><div style='font-size:.8rem;'>Cargando.. Por favor espere</div>");
                    jQuery("#preloadBuscador").html("En mantenimiento, intente más tarde.");
                    jQuery("#preloadBuscador").show();
                    //this.obsSearchWithGoogle( "top" );
                    return;
                });
            });
        } //Termina else de resultado vacio
    };
    FormularioGCSComponent.prototype.setField = function (valor) {
        this.formGCS.setValue({
            'faseUno': {
                'frm_GCS': valor
            }
        });
        return;
    };
    FormularioGCSComponent.prototype.makeRequets = function (page) {
        var value = this.formGCS.get("faseUno.frm_GCS").value;
        if (value != "" && value != null) {
            // if (typeof dataLayer != 'undefined') {
            //     dataLayer.push({
            //         'event': 'NoIntSearch', //Static data
            //         'SearchT': value, // Dynamic data
            //         // 'boolSearch': , //Dynamic Data
            //     });
            //     // alert('Enviando: ' + 'section: ' + jQuery('#post_title').val() + 'location: ' + locationCTC);
            // }
            this.obsSearchWithGoogle(value, page);
            jQuery("#preloadBuscador").show(500);
            jQuery(".typeahead-menu").hide(500);
        }
    };
    //Esta funcion manipula el front del microfono y lo activa
    FormularioGCSComponent.prototype.mic = function () {
        if (jQuery("#micGCS").attr("class").indexOf("active") > -1) {
            this.desactivaMicro();
            if (this.formGCS.get("faseUno.frm_GCS").value == "") {
                jQuery("#GCSCintillo").hide(500);
            }
        }
        else {
            this.activaMicro();
        }
        return;
    };
    FormularioGCSComponent.prototype.activaMicro = function () {
        jQuery("#micGCS").addClass("active");
        jQuery(".BuscadorGCS:first-child div").hide(500);
        jQuery(".speachMic").parent().show(500);
        jQuery(".mdlc").hide(500);
        this.setItemsSearchResult("vacio");
        this.activateSpeechSearch();
        return;
    };
    FormularioGCSComponent.prototype.desactivaMicro = function () {
        jQuery("#micGCS").removeClass("active");
        jQuery(".BuscadorGCS:first-child div").show();
        jQuery(".speachMic").parent().hide(500);
        jQuery(".mdlc").show(500);
        jQuery(".typeahead-menu").hide();
        jQuery(".GCSCintillo").hide();
        jQuery(".msgGCS").hide();
        jQuery("#preloadBuscador").hide();
        this.speechRecognitionService.DestroySpeechObject();
        this.setItemsSearchResult("vacio");
        return;
    };
    //Esta funcion devuelve los resultados de la busqueda
    FormularioGCSComponent.prototype.setItemsSearchResult = function (data, value_input) {
        if (value_input === void 0) {
            value_input = "";
        }
        this.itemsSearchResult = data;
        console.log(data);
        var resultados = "";
        if (data !== "vacio") {
            for (var item = 0; item < data.length; item++) {
                resultados +=
                    '<header class="d-flex flex-column">' +
                        '<a href="' + data[item]['link'] + '" class="titulo">' + data[item]["htmlTitle"] + '</a>' +
                        '<a href="' + data[item]['link'] + '" class="link">' + data[item]["link"] + '</a>' +
                        '</header>' +
                        '<section class="w-100">' + data[item]["htmlSnippet"].replace("<br>", "") + '</section>';
            }
        }
        console.log("value input: " + value_input);
        if (value_input != "top") {
            jQuery(".inputGCS").val(value_input);
        }
        jQuery(".GCSresultItem").html(resultados);
    };
    //Esta funcion manipula los eventos del teclado 
    FormularioGCSComponent.prototype.keyDownFunction = function (event) {
        if (event.keyCode == 13) {
            var error_frmgcs = this.formGCS.get('faseUno.frm_GCS').errors;
            if (!error_frmgcs) {
                this.makeRequets();
                jQuery(".typeahead-menu").hide(500);
            }
        }
    };
    FormularioGCSComponent.prototype.cambiaPagina = function (objPage) {
        console.log(objPage);
        this.makeRequets(objPage.startIndex);
        //Oculta el boton previo si el index es igual a 1
        if (objPage.startIndex !== 1) {
            jQuery("#pag-prev-buscador").show();
        }
        else {
            jQuery("#pag-prev-buscador").hide();
        }
        //Oculta el boton de siguiente si el numero es igual a la ultima pagina 
        if (objPage.number !== this.lastIndexPages) {
            jQuery("#pag-next-buscador").show();
        }
        else {
            jQuery("#pag-next-buscador").hide();
        }
        //Desactiva el active a las paginas no seleccionadas
        jQuery.each(this.numberPages, function (key, obj) {
            if (obj !== objPage) {
                obj.active = false;
            }
        });
        //Cambia el activo a la pagina actual
        objPage.active = true;
    };
    FormularioGCSComponent.prototype.changeToPreviousPage = function () {
        jQuery("#pagination-buscador li.active").prev().click();
    };
    FormularioGCSComponent.prototype.changeToNextPage = function () {
        jQuery("#pagination-buscador li.active").next().click();
    };
    return FormularioGCSComponent;
}());



/***/ }),

/***/ "./src/app/buscador/search-side-nav/search-side-nav.component.ngfactory.js":
/*!*********************************************************************************!*\
  !*** ./src/app/buscador/search-side-nav/search-side-nav.component.ngfactory.js ***!
  \*********************************************************************************/
/*! exports provided: RenderType_SearchSideNavComponent, View_SearchSideNavComponent_0, View_SearchSideNavComponent_Host_0, SearchSideNavComponentNgFactory */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "RenderType_SearchSideNavComponent", function() { return RenderType_SearchSideNavComponent; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "View_SearchSideNavComponent_0", function() { return View_SearchSideNavComponent_0; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "View_SearchSideNavComponent_Host_0", function() { return View_SearchSideNavComponent_Host_0; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SearchSideNavComponentNgFactory", function() { return SearchSideNavComponentNgFactory; });
/* harmony import */ var _search_side_nav_component_scss_shim_ngstyle__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./search-side-nav.component.scss.shim.ngstyle */ "./src/app/buscador/search-side-nav/search-side-nav.component.scss.shim.ngstyle.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/esm5/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/esm5/common.js");
/* harmony import */ var _services_autocomplete_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../services/autocomplete.service */ "./src/app/services/autocomplete.service.ts");
/* harmony import */ var _angular_http__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/http */ "./node_modules/@angular/http/esm5/http.js");
/* harmony import */ var _services_navegador_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../../services/navegador.service */ "./src/app/services/navegador.service.ts");
/* harmony import */ var _search_side_nav_component__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./search-side-nav.component */ "./src/app/buscador/search-side-nav/search-side-nav.component.ts");
/**
 * @fileoverview This file was generated by the Angular template compiler. Do not edit.
 *
 * @suppress {suspiciousCode,uselessCode,missingProperties,missingOverride,checkTypes}
 * tslint:disable
 */
/** PURE_IMPORTS_START _search_side_nav.component.scss.shim.ngstyle,_angular_core,_angular_common,_.._services_autocomplete.service,_angular_http,_.._services_navegador.service,_search_side_nav.component PURE_IMPORTS_END */
/** PURE_IMPORTS_START _search_side_nav.component.scss.shim.ngstyle,_angular_core,_angular_common,_.._services_autocomplete.service,_angular_http,_.._services_navegador.service,_search_side_nav.component PURE_IMPORTS_END */







var styles_SearchSideNavComponent = [_search_side_nav_component_scss_shim_ngstyle__WEBPACK_IMPORTED_MODULE_0__["styles"]];
var RenderType_SearchSideNavComponent = /*@__PURE__*/ /*@__PURE__*/ _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵcrt"]({ encapsulation: 0, styles: styles_SearchSideNavComponent, data: {} });

function View_SearchSideNavComponent_1(_l) { return _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵvid"](0, [(_l()(), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵeld"](0, 0, null, null, 4, "i", [["class", "icon-u-microfono d-flex justify-content-center align-items-center"], ["id", "micExtrangero"]], null, null, null, null, null)), (_l()(), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵted"](-1, null, ["\n                "])), (_l()(), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵeld"](2, 0, null, null, 0, "span", [["class", "path1"]], null, null, null, null, null)), (_l()(), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵeld"](3, 0, null, null, 0, "span", [["class", "path2"]], null, null, null, null, null)), (_l()(), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵted"](-1, null, ["\n            "]))], null, null); }
function View_SearchSideNavComponent_0(_l) { return _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵvid"](0, [(_l()(), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵted"](-1, null, ["\n\n"])), (_l()(), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵeld"](1, 0, null, null, 12, "div", [["class", "buscador-desk d-flex align-items-center"]], null, null, null, null, null)), (_l()(), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵted"](-1, null, ["\n    "])), (_l()(), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵeld"](3, 0, null, null, 9, "div", [["class", "buscador-desk-input d-flex w-100"]], null, null, null, null, null)), (_l()(), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵted"](-1, null, ["\n        "])), (_l()(), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵeld"](5, 0, null, null, 0, "input", [["class", "input-search-sidenav"], ["id", "gcsextrangero"], ["placeholder", "Buscar..."], ["type", "search"]], null, null, null, null, null)), (_l()(), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵted"](-1, null, ["\n        "])), (_l()(), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵeld"](7, 0, null, null, 4, "button", [["class", "microfono"], ["id", ""], ["type", "submit"]], null, null, null, null, null)), (_l()(), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵted"](-1, null, ["\n            "])), (_l()(), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵand"](16777216, null, null, 1, null, View_SearchSideNavComponent_1)), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵdid"](10, 16384, null, 0, _angular_common__WEBPACK_IMPORTED_MODULE_2__["NgIf"], [_angular_core__WEBPACK_IMPORTED_MODULE_1__["ViewContainerRef"], _angular_core__WEBPACK_IMPORTED_MODULE_1__["TemplateRef"]], { ngIf: [0, "ngIf"] }, null), (_l()(), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵted"](-1, null, ["\n        "])), (_l()(), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵted"](-1, null, ["\n    "])), (_l()(), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵted"](-1, null, ["\n"]))], function (_ck, _v) { var _co = _v.component; var currVal_0 = (_co.whatsbrowser == "Chrome"); _ck(_v, 10, 0, currVal_0); }, null); }
function View_SearchSideNavComponent_Host_0(_l) { return _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵvid"](0, [(_l()(), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵeld"](0, 0, null, null, 3, "search-side-nav", [], null, null, null, View_SearchSideNavComponent_0, RenderType_SearchSideNavComponent)), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵprd"](131584, null, _services_autocomplete_service__WEBPACK_IMPORTED_MODULE_3__["AutocompleteService"], _services_autocomplete_service__WEBPACK_IMPORTED_MODULE_3__["AutocompleteService"], [_angular_http__WEBPACK_IMPORTED_MODULE_4__["Http"]]), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵprd"](512, null, _services_navegador_service__WEBPACK_IMPORTED_MODULE_5__["NavegadorService"], _services_navegador_service__WEBPACK_IMPORTED_MODULE_5__["NavegadorService"], []), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵdid"](3, 114688, null, 0, _search_side_nav_component__WEBPACK_IMPORTED_MODULE_6__["SearchSideNavComponent"], [_services_autocomplete_service__WEBPACK_IMPORTED_MODULE_3__["AutocompleteService"], _services_navegador_service__WEBPACK_IMPORTED_MODULE_5__["NavegadorService"]], null, null)], function (_ck, _v) { _ck(_v, 3, 0); }, null); }
var SearchSideNavComponentNgFactory = /*@__PURE__*/ /*@__PURE__*/ _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵccf"]("search-side-nav", _search_side_nav_component__WEBPACK_IMPORTED_MODULE_6__["SearchSideNavComponent"], View_SearchSideNavComponent_Host_0, {}, {}, []);



/***/ }),

/***/ "./src/app/buscador/search-side-nav/search-side-nav.component.scss.shim.ngstyle.js":
/*!*****************************************************************************************!*\
  !*** ./src/app/buscador/search-side-nav/search-side-nav.component.scss.shim.ngstyle.js ***!
  \*****************************************************************************************/
/*! exports provided: styles */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "styles", function() { return styles; });
/**
 * @fileoverview This file was generated by the Angular template compiler. Do not edit.
 *
 * @suppress {suspiciousCode,uselessCode,missingProperties,missingOverride,checkTypes}
 * tslint:disable
 */
/** PURE_IMPORTS_START  PURE_IMPORTS_END */
/** PURE_IMPORTS_START  PURE_IMPORTS_END */
var styles = [".input-search-sidenav::-webkit-input-placeholder {\n  color: #CCC;\n  text-transform: initial !important; }\n\n  .input-search-sidenav::-ms-input-placeholder {\n  \n  color: #CCC;\n  text-transform: initial !important; }\n\n  .input-search-sidenav::placeholder {\n  \n  color: #CCC;\n  text-transform: initial !important; }"];



/***/ }),

/***/ "./src/app/buscador/search-side-nav/search-side-nav.component.ts":
/*!***********************************************************************!*\
  !*** ./src/app/buscador/search-side-nav/search-side-nav.component.ts ***!
  \***********************************************************************/
/*! exports provided: SearchSideNavComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SearchSideNavComponent", function() { return SearchSideNavComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/esm5/core.js");
/* harmony import */ var _services_autocomplete_service__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../../services/autocomplete.service */ "./src/app/services/autocomplete.service.ts");
/* harmony import */ var _services_navegador_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../services/navegador.service */ "./src/app/services/navegador.service.ts");



var SearchSideNavComponent = /** @class */ /*@__PURE__*/ (function () {
    function SearchSideNavComponent(autocomplete, navegador) {
        this.autocomplete = autocomplete;
        this.navegador = navegador;
        this.whatsbrowser = navegador.whatsBrowser();
    }
    SearchSideNavComponent.prototype.ngOnInit = function () {
        var that = this;
        // this.autocomplete.callAutocomplete( '#gcsextrangero', "//clients1.google.com/complete/search?hl=en&client=partner&source=gcsc&partnerid=002268575599804991473:n-g-9uohyde&ds=cse&nocache=" );
    };
    return SearchSideNavComponent;
}());



/***/ }),

/***/ "./src/app/formularios/forms_child_components/frm-aviso-de-privacidad/frm-aviso-de-privacidad.component.ngfactory.js":
/*!***************************************************************************************************************************!*\
  !*** ./src/app/formularios/forms_child_components/frm-aviso-de-privacidad/frm-aviso-de-privacidad.component.ngfactory.js ***!
  \***************************************************************************************************************************/
/*! exports provided: RenderType_FrmAvisoDePrivacidadComponent, View_FrmAvisoDePrivacidadComponent_0, View_FrmAvisoDePrivacidadComponent_Host_0, FrmAvisoDePrivacidadComponentNgFactory */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "RenderType_FrmAvisoDePrivacidadComponent", function() { return RenderType_FrmAvisoDePrivacidadComponent; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "View_FrmAvisoDePrivacidadComponent_0", function() { return View_FrmAvisoDePrivacidadComponent_0; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "View_FrmAvisoDePrivacidadComponent_Host_0", function() { return View_FrmAvisoDePrivacidadComponent_Host_0; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "FrmAvisoDePrivacidadComponentNgFactory", function() { return FrmAvisoDePrivacidadComponentNgFactory; });
/* harmony import */ var _frm_aviso_de_privacidad_component_scss_shim_ngstyle__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./frm-aviso-de-privacidad.component.scss.shim.ngstyle */ "./src/app/formularios/forms_child_components/frm-aviso-de-privacidad/frm-aviso-de-privacidad.component.scss.shim.ngstyle.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/esm5/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/esm5/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/esm5/forms.js");
/* harmony import */ var _frm_aviso_de_privacidad_component__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./frm-aviso-de-privacidad.component */ "./src/app/formularios/forms_child_components/frm-aviso-de-privacidad/frm-aviso-de-privacidad.component.ts");
/**
 * @fileoverview This file was generated by the Angular template compiler. Do not edit.
 *
 * @suppress {suspiciousCode,uselessCode,missingProperties,missingOverride,checkTypes}
 * tslint:disable
 */
/** PURE_IMPORTS_START _frm_aviso_de_privacidad.component.scss.shim.ngstyle,_angular_core,_angular_common,_angular_forms,_frm_aviso_de_privacidad.component PURE_IMPORTS_END */
/** PURE_IMPORTS_START _frm_aviso_de_privacidad.component.scss.shim.ngstyle,_angular_core,_angular_common,_angular_forms,_frm_aviso_de_privacidad.component PURE_IMPORTS_END */





var styles_FrmAvisoDePrivacidadComponent = [_frm_aviso_de_privacidad_component_scss_shim_ngstyle__WEBPACK_IMPORTED_MODULE_0__["styles"]];
var RenderType_FrmAvisoDePrivacidadComponent = /*@__PURE__*/ /*@__PURE__*/ _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵcrt"]({ encapsulation: 0, styles: styles_FrmAvisoDePrivacidadComponent, data: {} });

function View_FrmAvisoDePrivacidadComponent_2(_l) { return _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵvid"](0, [(_l()(), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵeld"](0, 0, null, null, 1, "div", [], null, null, null, null, null)), (_l()(), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵted"](1, null, ["\n        ", "\n    "]))], null, function (_ck, _v) { var _co = _v.component; var currVal_0 = _co.formularioPadre.get(_co.inputComponent).errors.customErrorMessage; _ck(_v, 1, 0, currVal_0); }); }
function View_FrmAvisoDePrivacidadComponent_1(_l) { return _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵvid"](0, [(_l()(), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵeld"](0, 0, null, null, 4, "div", [["class", "myFeedBackError"]], null, null, null, null, null)), (_l()(), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵted"](-1, null, ["\n    "])), (_l()(), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵand"](16777216, null, null, 1, null, View_FrmAvisoDePrivacidadComponent_2)), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵdid"](3, 16384, null, 0, _angular_common__WEBPACK_IMPORTED_MODULE_2__["NgIf"], [_angular_core__WEBPACK_IMPORTED_MODULE_1__["ViewContainerRef"], _angular_core__WEBPACK_IMPORTED_MODULE_1__["TemplateRef"]], { ngIf: [0, "ngIf"] }, null), (_l()(), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵted"](-1, null, ["\n"]))], function (_ck, _v) { var _co = _v.component; var currVal_0 = _co.formularioPadre.get(_co.inputComponent).errors.fieldError; _ck(_v, 3, 0, currVal_0); }, null); }
function View_FrmAvisoDePrivacidadComponent_0(_l) {
    return _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵvid"](0, [(_l()(), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵeld"](0, 0, null, null, 26, "span", [], [[2, "ng-untouched", null], [2, "ng-touched", null], [2, "ng-pristine", null], [2, "ng-dirty", null], [2, "ng-valid", null], [2, "ng-invalid", null], [2, "ng-pending", null]], [[null, "submit"], [null, "reset"]], function (_v, en, $event) {
            var ad = true;
            if (("submit" === en)) {
                var pd_0 = (_angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵnov"](_v, 1).onSubmit($event) !== false);
                ad = (pd_0 && ad);
            }
            if (("reset" === en)) {
                var pd_1 = (_angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵnov"](_v, 1).onReset() !== false);
                ad = (pd_1 && ad);
            }
            return ad;
        }, null, null)), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵdid"](1, 540672, null, 0, _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormGroupDirective"], [[8, null], [8, null]], { form: [0, "form"] }, null), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵprd"](2048, null, _angular_forms__WEBPACK_IMPORTED_MODULE_3__["ControlContainer"], null, [_angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormGroupDirective"]]), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵdid"](3, 16384, null, 0, _angular_forms__WEBPACK_IMPORTED_MODULE_3__["NgControlStatusGroup"], [_angular_forms__WEBPACK_IMPORTED_MODULE_3__["ControlContainer"]], null, null), (_l()(), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵted"](-1, null, ["\n    "])), (_l()(), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵeld"](5, 0, null, null, 20, "div", [["class", "row"]], [[2, "ng-untouched", null], [2, "ng-touched", null], [2, "ng-pristine", null], [2, "ng-dirty", null], [2, "ng-valid", null], [2, "ng-invalid", null], [2, "ng-pending", null]], null, null, null, null)), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵdid"](6, 212992, null, 0, _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormGroupName"], [[3, _angular_forms__WEBPACK_IMPORTED_MODULE_3__["ControlContainer"]], [8, null], [8, null]], { name: [0, "name"] }, null), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵprd"](2048, null, _angular_forms__WEBPACK_IMPORTED_MODULE_3__["ControlContainer"], null, [_angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormGroupName"]]), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵdid"](8, 16384, null, 0, _angular_forms__WEBPACK_IMPORTED_MODULE_3__["NgControlStatusGroup"], [_angular_forms__WEBPACK_IMPORTED_MODULE_3__["ControlContainer"]], null, null), (_l()(), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵted"](-1, null, ["\n      "])), (_l()(), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵeld"](10, 0, null, null, 14, "fieldset", [["class", "pl-3"], ["id", "politicas"]], null, null, null, null, null)), (_l()(), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵted"](-1, null, ["\n          "])), (_l()(), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵeld"](12, 0, null, null, 5, "input", [["formControlName", "frm_aviso_de_privacidad"], ["id", "privacidad"], ["name", "privacidad"], ["type", "checkbox"], ["value", "privacidad"]], [[2, "ng-untouched", null], [2, "ng-touched", null], [2, "ng-pristine", null], [2, "ng-dirty", null], [2, "ng-valid", null], [2, "ng-invalid", null], [2, "ng-pending", null]], [[null, "change"], [null, "blur"]], function (_v, en, $event) {
            var ad = true;
            if (("change" === en)) {
                var pd_0 = (_angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵnov"](_v, 13).onChange($event.target.checked) !== false);
                ad = (pd_0 && ad);
            }
            if (("blur" === en)) {
                var pd_1 = (_angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵnov"](_v, 13).onTouched() !== false);
                ad = (pd_1 && ad);
            }
            return ad;
        }, null, null)), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵdid"](13, 16384, null, 0, _angular_forms__WEBPACK_IMPORTED_MODULE_3__["CheckboxControlValueAccessor"], [_angular_core__WEBPACK_IMPORTED_MODULE_1__["Renderer2"], _angular_core__WEBPACK_IMPORTED_MODULE_1__["ElementRef"]], null, null), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵprd"](1024, null, _angular_forms__WEBPACK_IMPORTED_MODULE_3__["NG_VALUE_ACCESSOR"], function (p0_0) { return [p0_0]; }, [_angular_forms__WEBPACK_IMPORTED_MODULE_3__["CheckboxControlValueAccessor"]]), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵdid"](15, 671744, null, 0, _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormControlName"], [[3, _angular_forms__WEBPACK_IMPORTED_MODULE_3__["ControlContainer"]], [8, null], [8, null], [2, _angular_forms__WEBPACK_IMPORTED_MODULE_3__["NG_VALUE_ACCESSOR"]]], { name: [0, "name"] }, null), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵprd"](2048, null, _angular_forms__WEBPACK_IMPORTED_MODULE_3__["NgControl"], null, [_angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormControlName"]]), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵdid"](17, 16384, null, 0, _angular_forms__WEBPACK_IMPORTED_MODULE_3__["NgControlStatus"], [_angular_forms__WEBPACK_IMPORTED_MODULE_3__["NgControl"]], null, null), (_l()(), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵted"](-1, null, ["\n          "])), (_l()(), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵeld"](19, 0, null, null, 4, "label", [["class", "m-0"], ["for", "privacidad"]], null, null, null, null, null)), (_l()(), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵted"](-1, null, ["\n              Acepto las  "])), (_l()(), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵeld"](21, 0, null, null, 1, "a", [["target", "_blank"]], [[8, "href", 4]], null, null, null, null)), (_l()(), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵted"](-1, null, ["Pol\u00EDticas de Privacidad"])), (_l()(), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵted"](-1, null, ["\n          "])), (_l()(), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵted"](-1, null, ["\n      "])), (_l()(), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵted"](-1, null, ["\n    "])), (_l()(), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵted"](-1, null, ["\n"])), (_l()(), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵted"](-1, null, ["\n"])), (_l()(), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵand"](16777216, null, null, 1, null, View_FrmAvisoDePrivacidadComponent_1)), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵdid"](29, 16384, null, 0, _angular_common__WEBPACK_IMPORTED_MODULE_2__["NgIf"], [_angular_core__WEBPACK_IMPORTED_MODULE_1__["ViewContainerRef"], _angular_core__WEBPACK_IMPORTED_MODULE_1__["TemplateRef"]], { ngIf: [0, "ngIf"] }, null)], function (_ck, _v) { var _co = _v.component; var currVal_7 = _co.formularioPadre; _ck(_v, 1, 0, currVal_7); var currVal_15 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵinlineInterpolate"](1, "", _co.fase, ""); _ck(_v, 6, 0, currVal_15); var currVal_23 = "frm_aviso_de_privacidad"; _ck(_v, 15, 0, currVal_23); var currVal_25 = (_co.formularioPadre.get(_co.inputComponent).errors && _co.formularioPadre.get(_co.inputComponent).dirty); _ck(_v, 29, 0, currVal_25); }, function (_ck, _v) { var _co = _v.component; var currVal_0 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵnov"](_v, 3).ngClassUntouched; var currVal_1 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵnov"](_v, 3).ngClassTouched; var currVal_2 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵnov"](_v, 3).ngClassPristine; var currVal_3 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵnov"](_v, 3).ngClassDirty; var currVal_4 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵnov"](_v, 3).ngClassValid; var currVal_5 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵnov"](_v, 3).ngClassInvalid; var currVal_6 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵnov"](_v, 3).ngClassPending; _ck(_v, 0, 0, currVal_0, currVal_1, currVal_2, currVal_3, currVal_4, currVal_5, currVal_6); var currVal_8 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵnov"](_v, 8).ngClassUntouched; var currVal_9 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵnov"](_v, 8).ngClassTouched; var currVal_10 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵnov"](_v, 8).ngClassPristine; var currVal_11 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵnov"](_v, 8).ngClassDirty; var currVal_12 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵnov"](_v, 8).ngClassValid; var currVal_13 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵnov"](_v, 8).ngClassInvalid; var currVal_14 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵnov"](_v, 8).ngClassPending; _ck(_v, 5, 0, currVal_8, currVal_9, currVal_10, currVal_11, currVal_12, currVal_13, currVal_14); var currVal_16 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵnov"](_v, 17).ngClassUntouched; var currVal_17 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵnov"](_v, 17).ngClassTouched; var currVal_18 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵnov"](_v, 17).ngClassPristine; var currVal_19 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵnov"](_v, 17).ngClassDirty; var currVal_20 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵnov"](_v, 17).ngClassValid; var currVal_21 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵnov"](_v, 17).ngClassInvalid; var currVal_22 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵnov"](_v, 17).ngClassPending; _ck(_v, 12, 0, currVal_16, currVal_17, currVal_18, currVal_19, currVal_20, currVal_21, currVal_22); var currVal_24 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵinlineInterpolate"](1, "", _co.url, ""); _ck(_v, 21, 0, currVal_24); });
}
function View_FrmAvisoDePrivacidadComponent_Host_0(_l) { return _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵvid"](0, [(_l()(), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵeld"](0, 0, null, null, 1, "app-frm-aviso-de-privacidad", [], null, null, null, View_FrmAvisoDePrivacidadComponent_0, RenderType_FrmAvisoDePrivacidadComponent)), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵdid"](1, 114688, null, 0, _frm_aviso_de_privacidad_component__WEBPACK_IMPORTED_MODULE_4__["FrmAvisoDePrivacidadComponent"], [], null, null)], function (_ck, _v) { _ck(_v, 1, 0); }, null); }
var FrmAvisoDePrivacidadComponentNgFactory = /*@__PURE__*/ /*@__PURE__*/ _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵccf"]("app-frm-aviso-de-privacidad", _frm_aviso_de_privacidad_component__WEBPACK_IMPORTED_MODULE_4__["FrmAvisoDePrivacidadComponent"], View_FrmAvisoDePrivacidadComponent_Host_0, { formularioPadre: "formularioPadre", asterisk: "asterisk", fase: "fase" }, {}, []);



/***/ }),

/***/ "./src/app/formularios/forms_child_components/frm-aviso-de-privacidad/frm-aviso-de-privacidad.component.scss.shim.ngstyle.js":
/*!***********************************************************************************************************************************!*\
  !*** ./src/app/formularios/forms_child_components/frm-aviso-de-privacidad/frm-aviso-de-privacidad.component.scss.shim.ngstyle.js ***!
  \***********************************************************************************************************************************/
/*! exports provided: styles */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "styles", function() { return styles; });
/**
 * @fileoverview This file was generated by the Angular template compiler. Do not edit.
 *
 * @suppress {suspiciousCode,uselessCode,missingProperties,missingOverride,checkTypes}
 * tslint:disable
 */
/** PURE_IMPORTS_START  PURE_IMPORTS_END */
/** PURE_IMPORTS_START  PURE_IMPORTS_END */
var styles = ["a[_ngcontent-%COMP%] {\n  color: #0275d8; }\n\na[_ngcontent-%COMP%]:hover {\n  color: #0275d8; }"];



/***/ }),

/***/ "./src/app/formularios/forms_child_components/frm-aviso-de-privacidad/frm-aviso-de-privacidad.component.ts":
/*!*****************************************************************************************************************!*\
  !*** ./src/app/formularios/forms_child_components/frm-aviso-de-privacidad/frm-aviso-de-privacidad.component.ts ***!
  \*****************************************************************************************************************/
/*! exports provided: FrmAvisoDePrivacidadComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "FrmAvisoDePrivacidadComponent", function() { return FrmAvisoDePrivacidadComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/esm5/core.js");

var FrmAvisoDePrivacidadComponent = /** @class */ /*@__PURE__*/ (function () {
    function FrmAvisoDePrivacidadComponent() {
        this.url = "//www.unitec.mx/politicas-de-privacidad/";
    }
    FrmAvisoDePrivacidadComponent.prototype.ngOnInit = function () {
        this.inputComponent = this.fase + '.frm_aviso_de_privacidad';
    };
    return FrmAvisoDePrivacidadComponent;
}());



/***/ }),

/***/ "./src/app/formularios/forms_child_components/frm-busqueda-integrada/frm-busqueda-integrada.component.ngfactory.js":
/*!*************************************************************************************************************************!*\
  !*** ./src/app/formularios/forms_child_components/frm-busqueda-integrada/frm-busqueda-integrada.component.ngfactory.js ***!
  \*************************************************************************************************************************/
/*! exports provided: RenderType_frmBusquedaIntegradaComponent, View_frmBusquedaIntegradaComponent_0, View_frmBusquedaIntegradaComponent_Host_0, frmBusquedaIntegradaComponentNgFactory */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "RenderType_frmBusquedaIntegradaComponent", function() { return RenderType_frmBusquedaIntegradaComponent; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "View_frmBusquedaIntegradaComponent_0", function() { return View_frmBusquedaIntegradaComponent_0; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "View_frmBusquedaIntegradaComponent_Host_0", function() { return View_frmBusquedaIntegradaComponent_Host_0; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "frmBusquedaIntegradaComponentNgFactory", function() { return frmBusquedaIntegradaComponentNgFactory; });
/* harmony import */ var _frm_busqueda_integrada_component_scss_ngstyle__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./frm-busqueda-integrada.component.scss.ngstyle */ "./src/app/formularios/forms_child_components/frm-busqueda-integrada/frm-busqueda-integrada.component.scss.ngstyle.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/esm5/core.js");
/* harmony import */ var _services_navegador_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../services/navegador.service */ "./src/app/services/navegador.service.ts");
/* harmony import */ var _forms_services_formCookies_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../forms_services/formCookies.service */ "./src/app/formularios/forms_services/formCookies.service.ts");
/* harmony import */ var ngx_cookie_service_cookie_service_cookie_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ngx-cookie-service/cookie-service/cookie.service */ "./node_modules/ngx-cookie-service/cookie-service/cookie.service.js");
/* harmony import */ var _frm_busqueda_integrada_component__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./frm-busqueda-integrada.component */ "./src/app/formularios/forms_child_components/frm-busqueda-integrada/frm-busqueda-integrada.component.ts");
/* harmony import */ var _forms_services_reglasInputs_service__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../../forms_services/reglasInputs.service */ "./src/app/formularios/forms_services/reglasInputs.service.ts");
/**
 * @fileoverview This file was generated by the Angular template compiler. Do not edit.
 *
 * @suppress {suspiciousCode,uselessCode,missingProperties,missingOverride,checkTypes}
 * tslint:disable
 */
/** PURE_IMPORTS_START _frm_busqueda_integrada.component.scss.ngstyle,_angular_core,_.._.._services_navegador.service,_.._forms_services_formCookies.service,ngx_cookie_service_cookie_service_cookie.service,_frm_busqueda_integrada.component,_.._forms_services_reglasInputs.service PURE_IMPORTS_END */
/** PURE_IMPORTS_START _frm_busqueda_integrada.component.scss.ngstyle,_angular_core,_.._.._services_navegador.service,_.._forms_services_formCookies.service,ngx_cookie_service_cookie_service_cookie.service,_frm_busqueda_integrada.component,_.._forms_services_reglasInputs.service PURE_IMPORTS_END */







var styles_frmBusquedaIntegradaComponent = [_frm_busqueda_integrada_component_scss_ngstyle__WEBPACK_IMPORTED_MODULE_0__["styles"]];
var RenderType_frmBusquedaIntegradaComponent = /*@__PURE__*/ /*@__PURE__*/ _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵcrt"]({ encapsulation: 2, styles: styles_frmBusquedaIntegradaComponent, data: {} });

function View_frmBusquedaIntegradaComponent_0(_l) {
    return _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵvid"](0, [(_l()(), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵeld"](0, 0, null, null, 46, "main", [["class", "container-fluid wow fadeIn d-flex justify-content-center align-items-center"]], null, null, null, null, null)), (_l()(), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵted"](-1, null, ["\n"])), (_l()(), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵted"](-1, null, ["\n"])), (_l()(), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵeld"](3, 0, null, null, 41, "div", [["class", "col-lg-11 justify-content-center mt-2 mb-2"]], null, null, null, null, null)), (_l()(), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵted"](-1, null, ["\n\n    "])), (_l()(), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵeld"](5, 0, null, null, 4, "article", [["class", "col-12 text-center mb-1 d-flex justify-content-center"], ["data-wow-offset", "50"]], null, null, null, null, null)), (_l()(), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵted"](-1, null, ["\n        "])), (_l()(), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵeld"](7, 0, null, null, 1, "h4", [["class", "w-75 titulo-buscador"]], null, null, null, null, null)), (_l()(), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵted"](8, null, ["\n            ", "\n        "])), (_l()(), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵted"](-1, null, ["\n    "])), (_l()(), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵted"](-1, null, ["\n\n    "])), (_l()(), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵted"](-1, null, ["\n    "])), (_l()(), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵeld"](12, 0, null, null, 4, "article", [["class", "col-12 text-center mb-2 mt-0 d-flex justify-content-center"], ["data-wow-offset", "50"]], null, null, null, null, null)), (_l()(), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵted"](-1, null, ["\n        "])), (_l()(), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵeld"](14, 0, null, null, 1, "h6", [["class", "w-100 descripcion"]], null, null, null, null, null)), (_l()(), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵted"](15, null, ["\n            ", "\n        "])), (_l()(), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵted"](-1, null, ["\n    "])), (_l()(), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵted"](-1, null, ["\n    "])), (_l()(), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵted"](-1, null, ["\n\n    "])), (_l()(), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵted"](-1, null, ["\n    "])), (_l()(), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵeld"](20, 0, null, null, 22, "div", [["class", "contenedor-buscador ml-3 mr-3 d-flex justify-content-center"]], null, null, null, null, null)), (_l()(), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵted"](-1, null, ["\n        "])), (_l()(), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵeld"](22, 0, null, null, 19, "div", [["class", "col-xl-8 col-md-10"]], null, null, null, null, null)), (_l()(), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵted"](-1, null, ["\n            "])), (_l()(), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵted"](-1, null, ["\n            "])), (_l()(), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵeld"](25, 0, null, null, 15, "div", [["class", "buscador-desk d-flex align-items-center"]], null, null, null, null, null)), (_l()(), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵted"](-1, null, ["\n                "])), (_l()(), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵeld"](27, 0, null, null, 0, "input", [["class", "inputSearch"], ["id", ""], ["name", "_token"], ["type", "hidden"], ["value", ""]], null, null, null, null, null)), (_l()(), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵted"](-1, null, ["\n                "])), (_l()(), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵeld"](29, 0, null, null, 10, "div", [["class", "buscador-desk-input d-flex align-items-center w-100"]], null, null, null, null, null)), (_l()(), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵted"](-1, null, ["\n                    "])), (_l()(), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵeld"](31, 0, null, null, 0, "input", [["autocomplete", "off"], ["id", "input-busqueda-integrada"], ["placeholder", "Buscar en biblioteca..."], ["type", "search"]], null, [[null, "keydown.enter"]], function (_v, en, $event) {
            var ad = true;
            var _co = _v.component;
            if (("keydown.enter" === en)) {
                var pd_0 = (_co.busqueda_integrada($event) !== false);
                ad = (pd_0 && ad);
            }
            return ad;
        }, null, null)), (_l()(), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵted"](-1, null, ["\n                    "])), (_l()(), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵeld"](33, 0, null, null, 3, "button", [["class", "microfono"], ["id", ""], ["type", "submit"]], null, [[null, "click"]], function (_v, en, $event) {
            var ad = true;
            var _co = _v.component;
            if (("click" === en)) {
                var pd_0 = (_co.busqueda_integrada($event) !== false);
                ad = (pd_0 && ad);
            }
            return ad;
        }, null, null)), (_l()(), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵted"](-1, null, ["\n                        "])), (_l()(), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵeld"](35, 0, null, null, 0, "i", [["class", "icon-u icon-u-buscar d-flex justify-content-center align-items-center"]], null, null, null, null, null)), (_l()(), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵted"](-1, null, ["\n                    "])), (_l()(), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵted"](-1, null, ["\n                    "])), (_l()(), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵeld"](38, 0, null, null, 0, "a", [["class", "buscador-desk-clear"], ["href", "#"]], null, null, null, null, null)), (_l()(), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵted"](-1, null, ["\n                "])), (_l()(), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵted"](-1, null, ["\n            "])), (_l()(), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵted"](-1, null, ["\n        "])), (_l()(), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵted"](-1, null, ["\n\n    "])), (_l()(), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵted"](-1, null, ["\n    "])), (_l()(), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵted"](-1, null, ["\n"])), (_l()(), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵted"](-1, null, ["\n"])), (_l()(), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵted"](-1, null, ["\n"]))], null, function (_ck, _v) { var _co = _v.component; var currVal_0 = _co.titulo_buscador; _ck(_v, 8, 0, currVal_0); var currVal_1 = _co.contenido_buscador; _ck(_v, 15, 0, currVal_1); });
}
function View_frmBusquedaIntegradaComponent_Host_0(_l) { return _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵvid"](0, [(_l()(), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵeld"](0, 0, null, null, 3, "frm-busqueda-integrada", [], null, null, null, View_frmBusquedaIntegradaComponent_0, RenderType_frmBusquedaIntegradaComponent)), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵprd"](512, null, _services_navegador_service__WEBPACK_IMPORTED_MODULE_2__["NavegadorService"], _services_navegador_service__WEBPACK_IMPORTED_MODULE_2__["NavegadorService"], []), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵprd"](512, null, _forms_services_formCookies_service__WEBPACK_IMPORTED_MODULE_3__["formCookiesService"], _forms_services_formCookies_service__WEBPACK_IMPORTED_MODULE_3__["formCookiesService"], [ngx_cookie_service_cookie_service_cookie_service__WEBPACK_IMPORTED_MODULE_4__["CookieService"]]), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵdid"](3, 114688, null, 0, _frm_busqueda_integrada_component__WEBPACK_IMPORTED_MODULE_5__["frmBusquedaIntegradaComponent"], [_forms_services_reglasInputs_service__WEBPACK_IMPORTED_MODULE_6__["reglasInputsService"], _services_navegador_service__WEBPACK_IMPORTED_MODULE_2__["NavegadorService"], _forms_services_formCookies_service__WEBPACK_IMPORTED_MODULE_3__["formCookiesService"]], null, null)], function (_ck, _v) { _ck(_v, 3, 0); }, null); }
var frmBusquedaIntegradaComponentNgFactory = /*@__PURE__*/ /*@__PURE__*/ _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵccf"]("frm-busqueda-integrada", _frm_busqueda_integrada_component__WEBPACK_IMPORTED_MODULE_5__["frmBusquedaIntegradaComponent"], View_frmBusquedaIntegradaComponent_Host_0, { formularioPadre: "formularioPadre", asterisk: "asterisk", fase: "fase" }, {}, []);



/***/ }),

/***/ "./src/app/formularios/forms_child_components/frm-busqueda-integrada/frm-busqueda-integrada.component.scss.ngstyle.js":
/*!****************************************************************************************************************************!*\
  !*** ./src/app/formularios/forms_child_components/frm-busqueda-integrada/frm-busqueda-integrada.component.scss.ngstyle.js ***!
  \****************************************************************************************************************************/
/*! exports provided: styles */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "styles", function() { return styles; });
/**
 * @fileoverview This file was generated by the Angular template compiler. Do not edit.
 *
 * @suppress {suspiciousCode,uselessCode,missingProperties,missingOverride,checkTypes}
 * tslint:disable
 */
/** PURE_IMPORTS_START  PURE_IMPORTS_END */
/** PURE_IMPORTS_START  PURE_IMPORTS_END */
var styles = [".card-title {\n  font-weight: 500;\n  font-size: 1.125rem; }\n\n.card-text {\n  color: #757575;\n  font-weight: 300; }\n\n::ng-deep .busqueda-integrada :focus {\n  outline-style: none;\n  box-shadow: none;\n  border-color: transparent;\n  border: 0;\n  outline: 0; }\n\n.busqueda-integrada {\n  border-top: 1px solid #0099CC !important;\n  border-bottom: 1px solid #0099CC !important;\n  border-left: 1px solid #0099CC !important;\n  border-right: 1px solid #0099CC !important; }\n"];



/***/ }),

/***/ "./src/app/formularios/forms_child_components/frm-busqueda-integrada/frm-busqueda-integrada.component.ts":
/*!***************************************************************************************************************!*\
  !*** ./src/app/formularios/forms_child_components/frm-busqueda-integrada/frm-busqueda-integrada.component.ts ***!
  \***************************************************************************************************************/
/*! exports provided: frmBusquedaIntegradaComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "frmBusquedaIntegradaComponent", function() { return frmBusquedaIntegradaComponent; });
/* harmony import */ var _forms_services_reglasInputs_service__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../../forms_services/reglasInputs.service */ "./src/app/formularios/forms_services/reglasInputs.service.ts");
/* harmony import */ var _services_navegador_service__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../../../services/navegador.service */ "./src/app/services/navegador.service.ts");
/* harmony import */ var _forms_services_formCookies_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../forms_services/formCookies.service */ "./src/app/formularios/forms_services/formCookies.service.ts");

//Servicio de reconocimiento por voz

//Servicio de cookie

var frmBusquedaIntegradaComponent = /** @class */ /*@__PURE__*/ (function () {
    function frmBusquedaIntegradaComponent(regIn, navegador, formCookiesService) {
        this.regIn = regIn;
        this.navegador = navegador;
        this.formCookiesService = formCookiesService;
        this.titulo_buscador = "Búsqueda Integrada";
        this.contenido_buscador = "Por medio de la búsqueda integrada encuentra resultados de los múltiples recursos físicos y electrónicos de forma inmediata:";
        this.whatsbrowser = navegador.whatsBrowser();
        console.log(this.whatsbrowser);
    }
    //Inicializacion del componente
    frmBusquedaIntegradaComponent.prototype.ngOnInit = function () { };
    //Reglas de los input no se permiten caracteres especiales y tampoco mas de 2 letras consecutivas
    //Este metodo es diferente a las validaciones simplemente se bloquea el input para no permitir la entrada
    frmBusquedaIntegradaComponent.prototype.reglasInputs = function (objInput, keyCode, key) {
        var serviceResponse = this.regIn.EvaluateTextP(objInput, keyCode, key);
        return serviceResponse;
    };
    frmBusquedaIntegradaComponent.prototype.sanitize_search = function (cadena) {
        // Definimos los caracteres que queremos eliminar
        var specialChars = "!@#$^&%*()+=-[]\/{}|:<>?,\"\ ";
        // Los eliminamos todos
        for (var i = 0; i < specialChars.length; i++) {
            cadena = cadena.replace(new RegExp("\\" + specialChars[i], 'gi'), '');
        }
        // Lo queremos devolver limpio en minusculas
        cadena = cadena.toLowerCase();
        // Quitamos espacios y los sustituimos por _ porque nos gusta mas asi
        cadena = cadena.replace(/ /g, "_");
        // Quitamos acentos y "ñ". Fijate en que va sin comillas el primer parametro
        cadena = cadena.replace(/á/gi, "a");
        cadena = cadena.replace(/é/gi, "e");
        cadena = cadena.replace(/í/gi, "i");
        cadena = cadena.replace(/ó/gi, "o");
        cadena = cadena.replace(/ú/gi, "u");
        cadena = cadena.replace(/ñ/gi, "n");
        cadena = cadena.replace(/%3A/gi, ":");
        cadena = cadena.replace(/%3a/gi, ":");
        cadena = cadena.replace(/3a/gi, "");
        return cadena;
    };
    frmBusquedaIntegradaComponent.prototype.busqueda_integrada = function (event) {
        var that = this;
        var admitido = false;
        var matricula;
        var busqueda = ((jQuery("#input-busqueda-integrada").val()) != null || jQuery("#input-busqueda-integrada").val() != "") ? jQuery("#input-busqueda-integrada").val() : false;
        if (that.formCookiesService.checkCookie("c_matricula")) {
            matricula = that.formCookiesService.getCookieValues("c_matricula");
            // console.log(matricula);
            matricula['c_matricula'] = that.sanitize_search(matricula['c_matricula']);
            admitido = true;
        }
        busqueda = that.sanitize_search(busqueda);
        console.log("matricula: " + matricula['c_matricula'] + " Admitido: " + admitido);
        if (admitido && busqueda) {
            window.open('https://primo4.gsl.com.mx/pds?func=load-login&calling_system=primo&institute=52unitec_inst&user=' + matricula['c_matricula'] + '&password=' + matricula['c_matricula'] + '&url=https://unitec-primo.hosted.exlibrisgroup.com:443/primo_library/libweb/pdsLogin?targetURL=https://unitec-primo.hosted.exlibrisgroup.com/primo-explore/search?vid=52UNITEC_INST&lang=es_ES&sortby=rank&from-new-ui=1&authenticationProfile=PDS&query=any,contains,' + busqueda + '&tab=unitec_tab&search_scope=unitec_scope&offset=0', '_blank');
            console.log('https://primo4.gsl.com.mx/pds?func=load-login&calling_system=primo&institute=52unitec_inst&user=' + matricula['c_matricula'] + '&password=' + matricula['c_matricula'] + '&url=https://unitec-primo.hosted.exlibrisgroup.com:443/primo_library/libweb/pdsLogin?targetURL=https://unitec-primo.hosted.exlibrisgroup.com/primo-explore/search?vid=52UNITEC_INST&lang=es_ES&sortby=rank&from-new-ui=1&authenticationProfile=PDS&query=any,contains,' + busqueda + '&tab=unitec_tab&search_scope=unitec_scope&offset=0');
        }
        else {
            alert("favor de llenar todos los campos para una mejor búsqueda");
            return false;
        }
    };
    return frmBusquedaIntegradaComponent;
}());



/***/ }),

/***/ "./src/app/formularios/forms_child_components/frm-mail-login/frm-mail-login.component.ngfactory.js":
/*!*********************************************************************************************************!*\
  !*** ./src/app/formularios/forms_child_components/frm-mail-login/frm-mail-login.component.ngfactory.js ***!
  \*********************************************************************************************************/
/*! exports provided: RenderType_frmMailLoginComponent, View_frmMailLoginComponent_0, View_frmMailLoginComponent_Host_0, frmMailLoginComponentNgFactory */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "RenderType_frmMailLoginComponent", function() { return RenderType_frmMailLoginComponent; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "View_frmMailLoginComponent_0", function() { return View_frmMailLoginComponent_0; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "View_frmMailLoginComponent_Host_0", function() { return View_frmMailLoginComponent_Host_0; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "frmMailLoginComponentNgFactory", function() { return frmMailLoginComponentNgFactory; });
/* harmony import */ var _frm_mail_login_component_scss_shim_ngstyle__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./frm-mail-login.component.scss.shim.ngstyle */ "./src/app/formularios/forms_child_components/frm-mail-login/frm-mail-login.component.scss.shim.ngstyle.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/esm5/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/esm5/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/esm5/forms.js");
/* harmony import */ var _frm_mail_login_component__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./frm-mail-login.component */ "./src/app/formularios/forms_child_components/frm-mail-login/frm-mail-login.component.ts");
/* harmony import */ var _forms_services_reglasInputs_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../../forms_services/reglasInputs.service */ "./src/app/formularios/forms_services/reglasInputs.service.ts");
/**
 * @fileoverview This file was generated by the Angular template compiler. Do not edit.
 *
 * @suppress {suspiciousCode,uselessCode,missingProperties,missingOverride,checkTypes}
 * tslint:disable
 */
/** PURE_IMPORTS_START _frm_mail_login.component.scss.shim.ngstyle,_angular_core,_angular_common,_angular_forms,_frm_mail_login.component,_.._forms_services_reglasInputs.service PURE_IMPORTS_END */
/** PURE_IMPORTS_START _frm_mail_login.component.scss.shim.ngstyle,_angular_core,_angular_common,_angular_forms,_frm_mail_login.component,_.._forms_services_reglasInputs.service PURE_IMPORTS_END */






var styles_frmMailLoginComponent = [_frm_mail_login_component_scss_shim_ngstyle__WEBPACK_IMPORTED_MODULE_0__["styles"]];
var RenderType_frmMailLoginComponent = /*@__PURE__*/ /*@__PURE__*/ _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵcrt"]({ encapsulation: 0, styles: styles_frmMailLoginComponent, data: {} });

function View_frmMailLoginComponent_2(_l) { return _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵvid"](0, [(_l()(), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵeld"](0, 0, null, null, 1, "div", [], null, null, null, null, null)), (_l()(), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵted"](1, null, ["\n                    ", "\n                "]))], null, function (_ck, _v) { var _co = _v.component; var currVal_0 = _co.formularioPadre.get(_co.inputComponent).errors.customErrorMessage; _ck(_v, 1, 0, currVal_0); }); }
function View_frmMailLoginComponent_1(_l) { return _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵvid"](0, [(_l()(), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵeld"](0, 0, null, null, 4, "div", [["class", "myFeedBackError"]], null, null, null, null, null)), (_l()(), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵted"](-1, null, ["\n                "])), (_l()(), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵand"](16777216, null, null, 1, null, View_frmMailLoginComponent_2)), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵdid"](3, 16384, null, 0, _angular_common__WEBPACK_IMPORTED_MODULE_2__["NgIf"], [_angular_core__WEBPACK_IMPORTED_MODULE_1__["ViewContainerRef"], _angular_core__WEBPACK_IMPORTED_MODULE_1__["TemplateRef"]], { ngIf: [0, "ngIf"] }, null), (_l()(), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵted"](-1, null, ["\n            "]))], function (_ck, _v) { var _co = _v.component; var currVal_0 = _co.formularioPadre.get(_co.inputComponent).errors.fieldError; _ck(_v, 3, 0, currVal_0); }, null); }
function View_frmMailLoginComponent_0(_l) {
    return _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵvid"](0, [(_l()(), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵeld"](0, 0, null, null, 31, "span", [], [[2, "ng-untouched", null], [2, "ng-touched", null], [2, "ng-pristine", null], [2, "ng-dirty", null], [2, "ng-valid", null], [2, "ng-invalid", null], [2, "ng-pending", null]], [[null, "submit"], [null, "reset"]], function (_v, en, $event) {
            var ad = true;
            if (("submit" === en)) {
                var pd_0 = (_angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵnov"](_v, 1).onSubmit($event) !== false);
                ad = (pd_0 && ad);
            }
            if (("reset" === en)) {
                var pd_1 = (_angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵnov"](_v, 1).onReset() !== false);
                ad = (pd_1 && ad);
            }
            return ad;
        }, null, null)), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵdid"](1, 540672, null, 0, _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormGroupDirective"], [[8, null], [8, null]], { form: [0, "form"] }, null), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵprd"](2048, null, _angular_forms__WEBPACK_IMPORTED_MODULE_3__["ControlContainer"], null, [_angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormGroupDirective"]]), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵdid"](3, 16384, null, 0, _angular_forms__WEBPACK_IMPORTED_MODULE_3__["NgControlStatusGroup"], [_angular_forms__WEBPACK_IMPORTED_MODULE_3__["ControlContainer"]], null, null), (_l()(), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵted"](-1, null, ["\n    "])), (_l()(), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵeld"](5, 0, null, null, 25, "div", [], [[2, "ng-untouched", null], [2, "ng-touched", null], [2, "ng-pristine", null], [2, "ng-dirty", null], [2, "ng-valid", null], [2, "ng-invalid", null], [2, "ng-pending", null]], null, null, null, null)), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵdid"](6, 212992, null, 0, _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormGroupName"], [[3, _angular_forms__WEBPACK_IMPORTED_MODULE_3__["ControlContainer"]], [8, null], [8, null]], { name: [0, "name"] }, null), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵprd"](2048, null, _angular_forms__WEBPACK_IMPORTED_MODULE_3__["ControlContainer"], null, [_angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormGroupName"]]), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵdid"](8, 16384, null, 0, _angular_forms__WEBPACK_IMPORTED_MODULE_3__["NgControlStatusGroup"], [_angular_forms__WEBPACK_IMPORTED_MODULE_3__["ControlContainer"]], null, null), (_l()(), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵted"](-1, null, ["\n        "])), (_l()(), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵeld"](10, 0, null, null, 19, "div", [["class", "md-form div-content"]], null, null, null, null, null)), (_l()(), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵted"](-1, null, ["\n        "])), (_l()(), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵeld"](12, 0, null, null, 5, "input", [["class", "form-control"], ["formControlName", "frm_mail_login"], ["onpaste", "return false;"], ["tabindex", "4"], ["type", "text"]], [[2, "ng-untouched", null], [2, "ng-touched", null], [2, "ng-pristine", null], [2, "ng-dirty", null], [2, "ng-valid", null], [2, "ng-invalid", null], [2, "ng-pending", null]], [[null, "keypress"], [null, "input"], [null, "blur"], [null, "compositionstart"], [null, "compositionend"]], function (_v, en, $event) {
            var ad = true;
            var _co = _v.component;
            if (("input" === en)) {
                var pd_0 = (_angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵnov"](_v, 13)._handleInput($event.target.value) !== false);
                ad = (pd_0 && ad);
            }
            if (("blur" === en)) {
                var pd_1 = (_angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵnov"](_v, 13).onTouched() !== false);
                ad = (pd_1 && ad);
            }
            if (("compositionstart" === en)) {
                var pd_2 = (_angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵnov"](_v, 13)._compositionStart() !== false);
                ad = (pd_2 && ad);
            }
            if (("compositionend" === en)) {
                var pd_3 = (_angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵnov"](_v, 13)._compositionEnd($event.target.value) !== false);
                ad = (pd_3 && ad);
            }
            if (("keypress" === en)) {
                var pd_4 = (_co.reglasInputs($event) !== false);
                ad = (pd_4 && ad);
            }
            return ad;
        }, null, null)), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵdid"](13, 16384, null, 0, _angular_forms__WEBPACK_IMPORTED_MODULE_3__["DefaultValueAccessor"], [_angular_core__WEBPACK_IMPORTED_MODULE_1__["Renderer2"], _angular_core__WEBPACK_IMPORTED_MODULE_1__["ElementRef"], [2, _angular_forms__WEBPACK_IMPORTED_MODULE_3__["COMPOSITION_BUFFER_MODE"]]], null, null), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵprd"](1024, null, _angular_forms__WEBPACK_IMPORTED_MODULE_3__["NG_VALUE_ACCESSOR"], function (p0_0) { return [p0_0]; }, [_angular_forms__WEBPACK_IMPORTED_MODULE_3__["DefaultValueAccessor"]]), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵdid"](15, 671744, null, 0, _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormControlName"], [[3, _angular_forms__WEBPACK_IMPORTED_MODULE_3__["ControlContainer"]], [8, null], [8, null], [2, _angular_forms__WEBPACK_IMPORTED_MODULE_3__["NG_VALUE_ACCESSOR"]]], { name: [0, "name"] }, null), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵprd"](2048, null, _angular_forms__WEBPACK_IMPORTED_MODULE_3__["NgControl"], null, [_angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormControlName"]]), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵdid"](17, 16384, null, 0, _angular_forms__WEBPACK_IMPORTED_MODULE_3__["NgControlStatus"], [_angular_forms__WEBPACK_IMPORTED_MODULE_3__["NgControl"]], null, null), (_l()(), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵted"](-1, null, ["\n            "])), (_l()(), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵeld"](19, 0, null, null, 6, "label", [["for", "frm_mail_login"]], null, null, null, null, null)), (_l()(), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵted"](-1, null, ["Ingresa Correo My Unitec: \n                    "])), (_l()(), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵeld"](21, 0, null, null, 3, "span", [["style", "color: red"]], null, null, null, null, null)), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵdid"](22, 278528, null, 0, _angular_common__WEBPACK_IMPORTED_MODULE_2__["NgClass"], [_angular_core__WEBPACK_IMPORTED_MODULE_1__["IterableDiffers"], _angular_core__WEBPACK_IMPORTED_MODULE_1__["KeyValueDiffers"], _angular_core__WEBPACK_IMPORTED_MODULE_1__["ElementRef"], _angular_core__WEBPACK_IMPORTED_MODULE_1__["Renderer2"]], { ngClass: [0, "ngClass"] }, null), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵpod"](23, { asteriskDisplay: 0 }), (_l()(), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵted"](-1, null, ["*"])), (_l()(), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵted"](-1, null, ["\n            "])), (_l()(), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵted"](-1, null, ["           \n            \n            "])), (_l()(), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵand"](16777216, null, null, 1, null, View_frmMailLoginComponent_1)), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵdid"](28, 16384, null, 0, _angular_common__WEBPACK_IMPORTED_MODULE_2__["NgIf"], [_angular_core__WEBPACK_IMPORTED_MODULE_1__["ViewContainerRef"], _angular_core__WEBPACK_IMPORTED_MODULE_1__["TemplateRef"]], { ngIf: [0, "ngIf"] }, null), (_l()(), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵted"](-1, null, ["\n        "])), (_l()(), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵted"](-1, null, ["\n    "])), (_l()(), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵted"](-1, null, ["\n    "]))], function (_ck, _v) { var _co = _v.component; var currVal_7 = _co.formularioPadre; _ck(_v, 1, 0, currVal_7); var currVal_15 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵinlineInterpolate"](1, "", _co.fase, ""); _ck(_v, 6, 0, currVal_15); var currVal_23 = "frm_mail_login"; _ck(_v, 15, 0, currVal_23); var currVal_24 = _ck(_v, 23, 0, !_co.asterisk); _ck(_v, 22, 0, currVal_24); var currVal_25 = (_co.formularioPadre.get(_co.inputComponent).errors && _co.formularioPadre.get(_co.inputComponent).dirty); _ck(_v, 28, 0, currVal_25); }, function (_ck, _v) { var currVal_0 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵnov"](_v, 3).ngClassUntouched; var currVal_1 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵnov"](_v, 3).ngClassTouched; var currVal_2 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵnov"](_v, 3).ngClassPristine; var currVal_3 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵnov"](_v, 3).ngClassDirty; var currVal_4 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵnov"](_v, 3).ngClassValid; var currVal_5 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵnov"](_v, 3).ngClassInvalid; var currVal_6 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵnov"](_v, 3).ngClassPending; _ck(_v, 0, 0, currVal_0, currVal_1, currVal_2, currVal_3, currVal_4, currVal_5, currVal_6); var currVal_8 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵnov"](_v, 8).ngClassUntouched; var currVal_9 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵnov"](_v, 8).ngClassTouched; var currVal_10 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵnov"](_v, 8).ngClassPristine; var currVal_11 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵnov"](_v, 8).ngClassDirty; var currVal_12 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵnov"](_v, 8).ngClassValid; var currVal_13 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵnov"](_v, 8).ngClassInvalid; var currVal_14 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵnov"](_v, 8).ngClassPending; _ck(_v, 5, 0, currVal_8, currVal_9, currVal_10, currVal_11, currVal_12, currVal_13, currVal_14); var currVal_16 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵnov"](_v, 17).ngClassUntouched; var currVal_17 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵnov"](_v, 17).ngClassTouched; var currVal_18 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵnov"](_v, 17).ngClassPristine; var currVal_19 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵnov"](_v, 17).ngClassDirty; var currVal_20 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵnov"](_v, 17).ngClassValid; var currVal_21 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵnov"](_v, 17).ngClassInvalid; var currVal_22 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵnov"](_v, 17).ngClassPending; _ck(_v, 12, 0, currVal_16, currVal_17, currVal_18, currVal_19, currVal_20, currVal_21, currVal_22); });
}
function View_frmMailLoginComponent_Host_0(_l) { return _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵvid"](0, [(_l()(), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵeld"](0, 0, null, null, 1, "frm-mail-login", [], null, null, null, View_frmMailLoginComponent_0, RenderType_frmMailLoginComponent)), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵdid"](1, 114688, null, 0, _frm_mail_login_component__WEBPACK_IMPORTED_MODULE_4__["frmMailLoginComponent"], [_forms_services_reglasInputs_service__WEBPACK_IMPORTED_MODULE_5__["reglasInputsService"]], null, null)], function (_ck, _v) { _ck(_v, 1, 0); }, null); }
var frmMailLoginComponentNgFactory = /*@__PURE__*/ /*@__PURE__*/ _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵccf"]("frm-mail-login", _frm_mail_login_component__WEBPACK_IMPORTED_MODULE_4__["frmMailLoginComponent"], View_frmMailLoginComponent_Host_0, { formularioPadre: "formularioPadre", asterisk: "asterisk", fase: "fase" }, {}, []);



/***/ }),

/***/ "./src/app/formularios/forms_child_components/frm-mail-login/frm-mail-login.component.scss.shim.ngstyle.js":
/*!*****************************************************************************************************************!*\
  !*** ./src/app/formularios/forms_child_components/frm-mail-login/frm-mail-login.component.scss.shim.ngstyle.js ***!
  \*****************************************************************************************************************/
/*! exports provided: styles */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "styles", function() { return styles; });
/**
 * @fileoverview This file was generated by the Angular template compiler. Do not edit.
 *
 * @suppress {suspiciousCode,uselessCode,missingProperties,missingOverride,checkTypes}
 * tslint:disable
 */
/** PURE_IMPORTS_START  PURE_IMPORTS_END */
/** PURE_IMPORTS_START  PURE_IMPORTS_END */
var styles = [".div-content[_ngcontent-%COMP%]   .input-login[_ngcontent-%COMP%] {\n  display: inline; }\n\n.div-content[_ngcontent-%COMP%]   label[_ngcontent-%COMP%] {\n  font-size: .7rem; }"];



/***/ }),

/***/ "./src/app/formularios/forms_child_components/frm-mail-login/frm-mail-login.component.ts":
/*!***********************************************************************************************!*\
  !*** ./src/app/formularios/forms_child_components/frm-mail-login/frm-mail-login.component.ts ***!
  \***********************************************************************************************/
/*! exports provided: frmMailLoginComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "frmMailLoginComponent", function() { return frmMailLoginComponent; });
/* harmony import */ var _forms_services_reglasInputs_service__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../../forms_services/reglasInputs.service */ "./src/app/formularios/forms_services/reglasInputs.service.ts");

var frmMailLoginComponent = /** @class */ /*@__PURE__*/ (function () {
    function frmMailLoginComponent(regIn) {
        this.regIn = regIn;
    }
    frmMailLoginComponent.prototype.ngOnInit = function () {
        this.inputComponent = this.fase + '.frm_mail_login';
    };
    //Reglas de los input no se permiten caracteres especiales y tampoco mas de 2 letras consecutivas
    //Este metodo es diferente a las validaciones simplemente se bloquea el input para no permitir la entrada
    frmMailLoginComponent.prototype.reglasInputs = function (evento) {
        var serviceResponse = this.regIn.sinEspacios(evento);
        //Validar el input por la implementacion del autocomplete
        jQuery("#lbl-frm-email").addClass("active");
        //this.execChildInputTypeAhead.emit(jQuery("#EmailbyPass").val());
        //Retornamos la respuesta del servicio
        return serviceResponse;
    };
    return frmMailLoginComponent;
}());



/***/ }),

/***/ "./src/app/formularios/forms_child_components/frm-ncuenta/frm-ncuenta.component.ngfactory.js":
/*!***************************************************************************************************!*\
  !*** ./src/app/formularios/forms_child_components/frm-ncuenta/frm-ncuenta.component.ngfactory.js ***!
  \***************************************************************************************************/
/*! exports provided: RenderType_FrmNcuentaComponent, View_FrmNcuentaComponent_0, View_FrmNcuentaComponent_Host_0, FrmNcuentaComponentNgFactory */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "RenderType_FrmNcuentaComponent", function() { return RenderType_FrmNcuentaComponent; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "View_FrmNcuentaComponent_0", function() { return View_FrmNcuentaComponent_0; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "View_FrmNcuentaComponent_Host_0", function() { return View_FrmNcuentaComponent_Host_0; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "FrmNcuentaComponentNgFactory", function() { return FrmNcuentaComponentNgFactory; });
/* harmony import */ var _frm_ncuenta_component_scss_shim_ngstyle__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./frm-ncuenta.component.scss.shim.ngstyle */ "./src/app/formularios/forms_child_components/frm-ncuenta/frm-ncuenta.component.scss.shim.ngstyle.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/esm5/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/esm5/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/esm5/forms.js");
/* harmony import */ var _frm_ncuenta_component__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./frm-ncuenta.component */ "./src/app/formularios/forms_child_components/frm-ncuenta/frm-ncuenta.component.ts");
/**
 * @fileoverview This file was generated by the Angular template compiler. Do not edit.
 *
 * @suppress {suspiciousCode,uselessCode,missingProperties,missingOverride,checkTypes}
 * tslint:disable
 */
/** PURE_IMPORTS_START _frm_ncuenta.component.scss.shim.ngstyle,_angular_core,_angular_common,_angular_forms,_frm_ncuenta.component PURE_IMPORTS_END */
/** PURE_IMPORTS_START _frm_ncuenta.component.scss.shim.ngstyle,_angular_core,_angular_common,_angular_forms,_frm_ncuenta.component PURE_IMPORTS_END */





var styles_FrmNcuentaComponent = [_frm_ncuenta_component_scss_shim_ngstyle__WEBPACK_IMPORTED_MODULE_0__["styles"]];
var RenderType_FrmNcuentaComponent = /*@__PURE__*/ /*@__PURE__*/ _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵcrt"]({ encapsulation: 0, styles: styles_FrmNcuentaComponent, data: {} });

function View_FrmNcuentaComponent_1(_l) { return _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵvid"](0, [(_l()(), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵeld"](0, 0, null, null, 6, "label", [["for", "frm_ncuenta"], ["id", "lbl-frm-cuenta-alumno"]], null, null, null, null, null)), (_l()(), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵted"](-1, null, ["Ingresa N\u00FAmero de cuenta:\n                    "])), (_l()(), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵeld"](2, 0, null, null, 3, "span", [["style", "color: red"]], null, null, null, null, null)), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵdid"](3, 278528, null, 0, _angular_common__WEBPACK_IMPORTED_MODULE_2__["NgClass"], [_angular_core__WEBPACK_IMPORTED_MODULE_1__["IterableDiffers"], _angular_core__WEBPACK_IMPORTED_MODULE_1__["KeyValueDiffers"], _angular_core__WEBPACK_IMPORTED_MODULE_1__["ElementRef"], _angular_core__WEBPACK_IMPORTED_MODULE_1__["Renderer2"]], { ngClass: [0, "ngClass"] }, null), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵpod"](4, { asteriskDisplay: 0 }), (_l()(), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵted"](-1, null, ["*"])), (_l()(), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵted"](-1, null, ["\n                "]))], function (_ck, _v) { var _co = _v.component; var currVal_0 = _ck(_v, 4, 0, !_co.asterisk); _ck(_v, 3, 0, currVal_0); }, null); }
function View_FrmNcuentaComponent_2(_l) { return _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵvid"](0, [(_l()(), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵeld"](0, 0, null, null, 6, "label", [["for", "frm_ncuenta"], ["id", "lbl-frm-cuenta-profesor"]], null, null, null, null, null)), (_l()(), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵted"](-1, null, ["Favor de Ingresar su RFC: \n                   "])), (_l()(), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵeld"](2, 0, null, null, 3, "span", [["style", "color: red"]], null, null, null, null, null)), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵdid"](3, 278528, null, 0, _angular_common__WEBPACK_IMPORTED_MODULE_2__["NgClass"], [_angular_core__WEBPACK_IMPORTED_MODULE_1__["IterableDiffers"], _angular_core__WEBPACK_IMPORTED_MODULE_1__["KeyValueDiffers"], _angular_core__WEBPACK_IMPORTED_MODULE_1__["ElementRef"], _angular_core__WEBPACK_IMPORTED_MODULE_1__["Renderer2"]], { ngClass: [0, "ngClass"] }, null), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵpod"](4, { asteriskDisplay: 0 }), (_l()(), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵted"](-1, null, ["*"])), (_l()(), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵted"](-1, null, ["\n            "]))], function (_ck, _v) { var _co = _v.component; var currVal_0 = _ck(_v, 4, 0, !_co.asterisk); _ck(_v, 3, 0, currVal_0); }, null); }
function View_FrmNcuentaComponent_4(_l) { return _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵvid"](0, [(_l()(), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵeld"](0, 0, null, null, 1, "div", [], null, null, null, null, null)), (_l()(), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵted"](1, null, ["\n                    ", "\n                "]))], null, function (_ck, _v) { var _co = _v.component; var currVal_0 = _co.formularioPadre.get(_co.inputComponent).errors.customErrorMessage; _ck(_v, 1, 0, currVal_0); }); }
function View_FrmNcuentaComponent_3(_l) { return _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵvid"](0, [(_l()(), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵeld"](0, 0, null, null, 4, "div", [["class", "myFeedBackError"]], null, null, null, null, null)), (_l()(), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵted"](-1, null, ["\n                "])), (_l()(), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵand"](16777216, null, null, 1, null, View_FrmNcuentaComponent_4)), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵdid"](3, 16384, null, 0, _angular_common__WEBPACK_IMPORTED_MODULE_2__["NgIf"], [_angular_core__WEBPACK_IMPORTED_MODULE_1__["ViewContainerRef"], _angular_core__WEBPACK_IMPORTED_MODULE_1__["TemplateRef"]], { ngIf: [0, "ngIf"] }, null), (_l()(), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵted"](-1, null, ["\n            "]))], function (_ck, _v) { var _co = _v.component; var currVal_0 = _co.formularioPadre.get(_co.inputComponent).errors.fieldError; _ck(_v, 3, 0, currVal_0); }, null); }
function View_FrmNcuentaComponent_0(_l) {
    return _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵvid"](0, [(_l()(), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵted"](-1, null, ["  "])), (_l()(), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵeld"](1, 0, null, null, 29, "span", [], [[2, "ng-untouched", null], [2, "ng-touched", null], [2, "ng-pristine", null], [2, "ng-dirty", null], [2, "ng-valid", null], [2, "ng-invalid", null], [2, "ng-pending", null]], [[null, "submit"], [null, "reset"]], function (_v, en, $event) {
            var ad = true;
            if (("submit" === en)) {
                var pd_0 = (_angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵnov"](_v, 2).onSubmit($event) !== false);
                ad = (pd_0 && ad);
            }
            if (("reset" === en)) {
                var pd_1 = (_angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵnov"](_v, 2).onReset() !== false);
                ad = (pd_1 && ad);
            }
            return ad;
        }, null, null)), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵdid"](2, 540672, null, 0, _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormGroupDirective"], [[8, null], [8, null]], { form: [0, "form"] }, null), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵprd"](2048, null, _angular_forms__WEBPACK_IMPORTED_MODULE_3__["ControlContainer"], null, [_angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormGroupDirective"]]), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵdid"](4, 16384, null, 0, _angular_forms__WEBPACK_IMPORTED_MODULE_3__["NgControlStatusGroup"], [_angular_forms__WEBPACK_IMPORTED_MODULE_3__["ControlContainer"]], null, null), (_l()(), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵted"](-1, null, ["\n    "])), (_l()(), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵeld"](6, 0, null, null, 23, "div", [["class", "form-group"]], [[2, "ng-untouched", null], [2, "ng-touched", null], [2, "ng-pristine", null], [2, "ng-dirty", null], [2, "ng-valid", null], [2, "ng-invalid", null], [2, "ng-pending", null]], null, null, null, null)), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵdid"](7, 212992, null, 0, _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormGroupName"], [[3, _angular_forms__WEBPACK_IMPORTED_MODULE_3__["ControlContainer"]], [8, null], [8, null]], { name: [0, "name"] }, null), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵprd"](2048, null, _angular_forms__WEBPACK_IMPORTED_MODULE_3__["ControlContainer"], null, [_angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormGroupName"]]), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵdid"](9, 16384, null, 0, _angular_forms__WEBPACK_IMPORTED_MODULE_3__["NgControlStatusGroup"], [_angular_forms__WEBPACK_IMPORTED_MODULE_3__["ControlContainer"]], null, null), (_l()(), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵted"](-1, null, ["\n        "])), (_l()(), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵeld"](11, 0, null, null, 17, "div", [["class", "md-form form-sm div-content-cuenta"]], null, null, null, null, null)), (_l()(), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵted"](-1, null, ["\n        "])), (_l()(), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵeld"](13, 0, null, null, 5, "input", [["autocomplete", "off"], ["class", "form-control"], ["formControlName", "frm_ncuenta"], ["onpaste", "return false;"], ["type", "text"]], [[2, "ng-untouched", null], [2, "ng-touched", null], [2, "ng-pristine", null], [2, "ng-dirty", null], [2, "ng-valid", null], [2, "ng-invalid", null], [2, "ng-pending", null]], [[null, "input"], [null, "blur"], [null, "compositionstart"], [null, "compositionend"]], function (_v, en, $event) {
            var ad = true;
            if (("input" === en)) {
                var pd_0 = (_angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵnov"](_v, 14)._handleInput($event.target.value) !== false);
                ad = (pd_0 && ad);
            }
            if (("blur" === en)) {
                var pd_1 = (_angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵnov"](_v, 14).onTouched() !== false);
                ad = (pd_1 && ad);
            }
            if (("compositionstart" === en)) {
                var pd_2 = (_angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵnov"](_v, 14)._compositionStart() !== false);
                ad = (pd_2 && ad);
            }
            if (("compositionend" === en)) {
                var pd_3 = (_angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵnov"](_v, 14)._compositionEnd($event.target.value) !== false);
                ad = (pd_3 && ad);
            }
            return ad;
        }, null, null)), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵdid"](14, 16384, null, 0, _angular_forms__WEBPACK_IMPORTED_MODULE_3__["DefaultValueAccessor"], [_angular_core__WEBPACK_IMPORTED_MODULE_1__["Renderer2"], _angular_core__WEBPACK_IMPORTED_MODULE_1__["ElementRef"], [2, _angular_forms__WEBPACK_IMPORTED_MODULE_3__["COMPOSITION_BUFFER_MODE"]]], null, null), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵprd"](1024, null, _angular_forms__WEBPACK_IMPORTED_MODULE_3__["NG_VALUE_ACCESSOR"], function (p0_0) { return [p0_0]; }, [_angular_forms__WEBPACK_IMPORTED_MODULE_3__["DefaultValueAccessor"]]), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵdid"](16, 671744, null, 0, _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormControlName"], [[3, _angular_forms__WEBPACK_IMPORTED_MODULE_3__["ControlContainer"]], [8, null], [8, null], [2, _angular_forms__WEBPACK_IMPORTED_MODULE_3__["NG_VALUE_ACCESSOR"]]], { name: [0, "name"] }, null), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵprd"](2048, null, _angular_forms__WEBPACK_IMPORTED_MODULE_3__["NgControl"], null, [_angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormControlName"]]), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵdid"](18, 16384, null, 0, _angular_forms__WEBPACK_IMPORTED_MODULE_3__["NgControlStatus"], [_angular_forms__WEBPACK_IMPORTED_MODULE_3__["NgControl"]], null, null), (_l()(), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵted"](-1, null, ["\n                "])), (_l()(), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵand"](16777216, null, null, 1, null, View_FrmNcuentaComponent_1)), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵdid"](21, 16384, null, 0, _angular_common__WEBPACK_IMPORTED_MODULE_2__["NgIf"], [_angular_core__WEBPACK_IMPORTED_MODULE_1__["ViewContainerRef"], _angular_core__WEBPACK_IMPORTED_MODULE_1__["TemplateRef"]], { ngIf: [0, "ngIf"] }, null), (_l()(), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵted"](-1, null, ["\n            "])), (_l()(), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵand"](16777216, null, null, 1, null, View_FrmNcuentaComponent_2)), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵdid"](24, 16384, null, 0, _angular_common__WEBPACK_IMPORTED_MODULE_2__["NgIf"], [_angular_core__WEBPACK_IMPORTED_MODULE_1__["ViewContainerRef"], _angular_core__WEBPACK_IMPORTED_MODULE_1__["TemplateRef"]], { ngIf: [0, "ngIf"] }, null), (_l()(), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵted"](-1, null, ["\n            "])), (_l()(), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵand"](16777216, null, null, 1, null, View_FrmNcuentaComponent_3)), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵdid"](27, 16384, null, 0, _angular_common__WEBPACK_IMPORTED_MODULE_2__["NgIf"], [_angular_core__WEBPACK_IMPORTED_MODULE_1__["ViewContainerRef"], _angular_core__WEBPACK_IMPORTED_MODULE_1__["TemplateRef"]], { ngIf: [0, "ngIf"] }, null), (_l()(), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵted"](-1, null, ["\n        "])), (_l()(), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵted"](-1, null, ["\n    "])), (_l()(), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵted"](-1, null, ["\n  "]))], function (_ck, _v) { var _co = _v.component; var currVal_7 = _co.formularioPadre; _ck(_v, 2, 0, currVal_7); var currVal_15 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵinlineInterpolate"](1, "", _co.fase, ""); _ck(_v, 7, 0, currVal_15); var currVal_23 = "frm_ncuenta"; _ck(_v, 16, 0, currVal_23); var currVal_24 = ((_co.formularioPadre.get("faseUno.frm_radio_perfil_AlumnoMaestro").value == "alumno") || !_co.formularioPadre.get("faseUno.frm_radio_perfil_AlumnoMaestro").value); _ck(_v, 21, 0, currVal_24); var currVal_25 = (_co.formularioPadre.get("faseUno.frm_radio_perfil_AlumnoMaestro").value == "profesor"); _ck(_v, 24, 0, currVal_25); var currVal_26 = (_co.formularioPadre.get(_co.inputComponent).errors && _co.formularioPadre.get(_co.inputComponent).dirty); _ck(_v, 27, 0, currVal_26); }, function (_ck, _v) { var currVal_0 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵnov"](_v, 4).ngClassUntouched; var currVal_1 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵnov"](_v, 4).ngClassTouched; var currVal_2 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵnov"](_v, 4).ngClassPristine; var currVal_3 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵnov"](_v, 4).ngClassDirty; var currVal_4 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵnov"](_v, 4).ngClassValid; var currVal_5 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵnov"](_v, 4).ngClassInvalid; var currVal_6 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵnov"](_v, 4).ngClassPending; _ck(_v, 1, 0, currVal_0, currVal_1, currVal_2, currVal_3, currVal_4, currVal_5, currVal_6); var currVal_8 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵnov"](_v, 9).ngClassUntouched; var currVal_9 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵnov"](_v, 9).ngClassTouched; var currVal_10 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵnov"](_v, 9).ngClassPristine; var currVal_11 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵnov"](_v, 9).ngClassDirty; var currVal_12 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵnov"](_v, 9).ngClassValid; var currVal_13 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵnov"](_v, 9).ngClassInvalid; var currVal_14 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵnov"](_v, 9).ngClassPending; _ck(_v, 6, 0, currVal_8, currVal_9, currVal_10, currVal_11, currVal_12, currVal_13, currVal_14); var currVal_16 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵnov"](_v, 18).ngClassUntouched; var currVal_17 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵnov"](_v, 18).ngClassTouched; var currVal_18 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵnov"](_v, 18).ngClassPristine; var currVal_19 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵnov"](_v, 18).ngClassDirty; var currVal_20 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵnov"](_v, 18).ngClassValid; var currVal_21 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵnov"](_v, 18).ngClassInvalid; var currVal_22 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵnov"](_v, 18).ngClassPending; _ck(_v, 13, 0, currVal_16, currVal_17, currVal_18, currVal_19, currVal_20, currVal_21, currVal_22); });
}
function View_FrmNcuentaComponent_Host_0(_l) { return _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵvid"](0, [(_l()(), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵeld"](0, 0, null, null, 1, "frm-ncuenta", [], null, null, null, View_FrmNcuentaComponent_0, RenderType_FrmNcuentaComponent)), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵdid"](1, 114688, null, 0, _frm_ncuenta_component__WEBPACK_IMPORTED_MODULE_4__["FrmNcuentaComponent"], [], null, null)], function (_ck, _v) { _ck(_v, 1, 0); }, null); }
var FrmNcuentaComponentNgFactory = /*@__PURE__*/ /*@__PURE__*/ _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵccf"]("frm-ncuenta", _frm_ncuenta_component__WEBPACK_IMPORTED_MODULE_4__["FrmNcuentaComponent"], View_FrmNcuentaComponent_Host_0, { formularioPadre: "formularioPadre", asterisk: "asterisk", fase: "fase" }, {}, []);



/***/ }),

/***/ "./src/app/formularios/forms_child_components/frm-ncuenta/frm-ncuenta.component.scss.shim.ngstyle.js":
/*!***********************************************************************************************************!*\
  !*** ./src/app/formularios/forms_child_components/frm-ncuenta/frm-ncuenta.component.scss.shim.ngstyle.js ***!
  \***********************************************************************************************************/
/*! exports provided: styles */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "styles", function() { return styles; });
/**
 * @fileoverview This file was generated by the Angular template compiler. Do not edit.
 *
 * @suppress {suspiciousCode,uselessCode,missingProperties,missingOverride,checkTypes}
 * tslint:disable
 */
/** PURE_IMPORTS_START  PURE_IMPORTS_END */
/** PURE_IMPORTS_START  PURE_IMPORTS_END */
var styles = [".div-content-cuenta[_ngcontent-%COMP%]   label[_ngcontent-%COMP%] {\n  font-size: .7rem; }"];



/***/ }),

/***/ "./src/app/formularios/forms_child_components/frm-ncuenta/frm-ncuenta.component.ts":
/*!*****************************************************************************************!*\
  !*** ./src/app/formularios/forms_child_components/frm-ncuenta/frm-ncuenta.component.ts ***!
  \*****************************************************************************************/
/*! exports provided: FrmNcuentaComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "FrmNcuentaComponent", function() { return FrmNcuentaComponent; });
var FrmNcuentaComponent = /** @class */ /*@__PURE__*/ (function () {
    function FrmNcuentaComponent() {
    }
    FrmNcuentaComponent.prototype.ngOnInit = function () {
        this.inputComponent = this.fase + '.frm_ncuenta';
    };
    return FrmNcuentaComponent;
}());



/***/ }),

/***/ "./src/app/formularios/forms_child_components/frm-problemas-cuenta/frm-problemas-cuenta.component.ngfactory.js":
/*!*********************************************************************************************************************!*\
  !*** ./src/app/formularios/forms_child_components/frm-problemas-cuenta/frm-problemas-cuenta.component.ngfactory.js ***!
  \*********************************************************************************************************************/
/*! exports provided: RenderType_frmProblemasCuenta, View_frmProblemasCuenta_0, View_frmProblemasCuenta_Host_0, frmProblemasCuentaNgFactory */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "RenderType_frmProblemasCuenta", function() { return RenderType_frmProblemasCuenta; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "View_frmProblemasCuenta_0", function() { return View_frmProblemasCuenta_0; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "View_frmProblemasCuenta_Host_0", function() { return View_frmProblemasCuenta_Host_0; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "frmProblemasCuentaNgFactory", function() { return frmProblemasCuentaNgFactory; });
/* harmony import */ var _frm_problemas_cuenta_component_scss_shim_ngstyle__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./frm-problemas-cuenta.component.scss.shim.ngstyle */ "./src/app/formularios/forms_child_components/frm-problemas-cuenta/frm-problemas-cuenta.component.scss.shim.ngstyle.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/esm5/core.js");
/* harmony import */ var _frm_problemas_cuenta_component__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./frm-problemas-cuenta.component */ "./src/app/formularios/forms_child_components/frm-problemas-cuenta/frm-problemas-cuenta.component.ts");
/**
 * @fileoverview This file was generated by the Angular template compiler. Do not edit.
 *
 * @suppress {suspiciousCode,uselessCode,missingProperties,missingOverride,checkTypes}
 * tslint:disable
 */
/** PURE_IMPORTS_START _frm_problemas_cuenta.component.scss.shim.ngstyle,_angular_core,_frm_problemas_cuenta.component PURE_IMPORTS_END */
/** PURE_IMPORTS_START _frm_problemas_cuenta.component.scss.shim.ngstyle,_angular_core,_frm_problemas_cuenta.component PURE_IMPORTS_END */



var styles_frmProblemasCuenta = [_frm_problemas_cuenta_component_scss_shim_ngstyle__WEBPACK_IMPORTED_MODULE_0__["styles"]];
var RenderType_frmProblemasCuenta = /*@__PURE__*/ /*@__PURE__*/ _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵcrt"]({ encapsulation: 0, styles: styles_frmProblemasCuenta, data: {} });

function View_frmProblemasCuenta_0(_l) { return _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵvid"](0, [(_l()(), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵeld"](0, 0, null, null, 12, "div", [["class", "frm-problemas-cuenta"], ["id", "frm_problemas_cuenta"]], null, null, null, null, null)), (_l()(), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵted"](-1, null, ["\n    "])), (_l()(), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵeld"](2, 0, null, null, 9, "label", [["class", "mb-2"]], null, null, null, null, null)), (_l()(), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵted"](-1, null, ["\n        \u00BFTienes problemas con tu cuenta?"])), (_l()(), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵeld"](4, 0, null, null, 1, "a", [["href", "mailto:ayudabv@my.unitec.edu.mx"], ["target", "_top"]], null, null, null, null, null)), (_l()(), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵted"](-1, null, [" Escr\u00EDbenos"])), (_l()(), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵted"](-1, null, ["\n        "])), (_l()(), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵeld"](7, 0, null, null, 0, "br", [], null, null, null, null, null)), (_l()(), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵted"](-1, null, ["\n        \u00BFEres exalumno y tienes problemas con tu cuenta?"])), (_l()(), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵeld"](9, 0, null, null, 1, "a", [["href", "mailto:exalumno@service.unitec.mx"], ["target", "_top"]], null, null, null, null, null)), (_l()(), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵted"](-1, null, [" Escr\u00EDbenos"])), (_l()(), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵted"](-1, null, ["\n    "])), (_l()(), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵted"](-1, null, ["\n"]))], null, null); }
function View_frmProblemasCuenta_Host_0(_l) { return _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵvid"](0, [(_l()(), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵeld"](0, 0, null, null, 1, "frm-problemas-cuenta", [], null, null, null, View_frmProblemasCuenta_0, RenderType_frmProblemasCuenta)), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵdid"](1, 49152, null, 0, _frm_problemas_cuenta_component__WEBPACK_IMPORTED_MODULE_2__["frmProblemasCuenta"], [], null, null)], null, null); }
var frmProblemasCuentaNgFactory = /*@__PURE__*/ /*@__PURE__*/ _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵccf"]("frm-problemas-cuenta", _frm_problemas_cuenta_component__WEBPACK_IMPORTED_MODULE_2__["frmProblemasCuenta"], View_frmProblemasCuenta_Host_0, {}, {}, []);



/***/ }),

/***/ "./src/app/formularios/forms_child_components/frm-problemas-cuenta/frm-problemas-cuenta.component.scss.shim.ngstyle.js":
/*!*****************************************************************************************************************************!*\
  !*** ./src/app/formularios/forms_child_components/frm-problemas-cuenta/frm-problemas-cuenta.component.scss.shim.ngstyle.js ***!
  \*****************************************************************************************************************************/
/*! exports provided: styles */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "styles", function() { return styles; });
/**
 * @fileoverview This file was generated by the Angular template compiler. Do not edit.
 *
 * @suppress {suspiciousCode,uselessCode,missingProperties,missingOverride,checkTypes}
 * tslint:disable
 */
/** PURE_IMPORTS_START  PURE_IMPORTS_END */
/** PURE_IMPORTS_START  PURE_IMPORTS_END */
var styles = [".frm-problemas-cuenta[_ngcontent-%COMP%]   label[_ngcontent-%COMP%]   a[_ngcontent-%COMP%] {\n  color: #006fba;\n  font-weight: 500; }"];



/***/ }),

/***/ "./src/app/formularios/forms_child_components/frm-problemas-cuenta/frm-problemas-cuenta.component.ts":
/*!***********************************************************************************************************!*\
  !*** ./src/app/formularios/forms_child_components/frm-problemas-cuenta/frm-problemas-cuenta.component.ts ***!
  \***********************************************************************************************************/
/*! exports provided: frmProblemasCuenta */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "frmProblemasCuenta", function() { return frmProblemasCuenta; });
var frmProblemasCuenta = /** @class */ /*@__PURE__*/ (function () {
    function frmProblemasCuenta() {
    }
    return frmProblemasCuenta;
}());



/***/ }),

/***/ "./src/app/formularios/forms_child_components/frm-radio-perfil-alumno-maestro/frm-radio-perfil-alumno-maestro.component.ngfactory.js":
/*!*******************************************************************************************************************************************!*\
  !*** ./src/app/formularios/forms_child_components/frm-radio-perfil-alumno-maestro/frm-radio-perfil-alumno-maestro.component.ngfactory.js ***!
  \*******************************************************************************************************************************************/
/*! exports provided: RenderType_FrmRadioPerfilAlumnoMaestroComponent, View_FrmRadioPerfilAlumnoMaestroComponent_0, View_FrmRadioPerfilAlumnoMaestroComponent_Host_0, FrmRadioPerfilAlumnoMaestroComponentNgFactory */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "RenderType_FrmRadioPerfilAlumnoMaestroComponent", function() { return RenderType_FrmRadioPerfilAlumnoMaestroComponent; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "View_FrmRadioPerfilAlumnoMaestroComponent_0", function() { return View_FrmRadioPerfilAlumnoMaestroComponent_0; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "View_FrmRadioPerfilAlumnoMaestroComponent_Host_0", function() { return View_FrmRadioPerfilAlumnoMaestroComponent_Host_0; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "FrmRadioPerfilAlumnoMaestroComponentNgFactory", function() { return FrmRadioPerfilAlumnoMaestroComponentNgFactory; });
/* harmony import */ var _frm_radio_perfil_alumno_maestro_component_scss_shim_ngstyle__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./frm-radio-perfil-alumno-maestro.component.scss.shim.ngstyle */ "./src/app/formularios/forms_child_components/frm-radio-perfil-alumno-maestro/frm-radio-perfil-alumno-maestro.component.scss.shim.ngstyle.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/esm5/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/esm5/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/esm5/forms.js");
/* harmony import */ var _frm_radio_perfil_alumno_maestro_component__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./frm-radio-perfil-alumno-maestro.component */ "./src/app/formularios/forms_child_components/frm-radio-perfil-alumno-maestro/frm-radio-perfil-alumno-maestro.component.ts");
/**
 * @fileoverview This file was generated by the Angular template compiler. Do not edit.
 *
 * @suppress {suspiciousCode,uselessCode,missingProperties,missingOverride,checkTypes}
 * tslint:disable
 */
/** PURE_IMPORTS_START _frm_radio_perfil_alumno_maestro.component.scss.shim.ngstyle,_angular_core,_angular_common,_angular_forms,_frm_radio_perfil_alumno_maestro.component PURE_IMPORTS_END */
/** PURE_IMPORTS_START _frm_radio_perfil_alumno_maestro.component.scss.shim.ngstyle,_angular_core,_angular_common,_angular_forms,_frm_radio_perfil_alumno_maestro.component PURE_IMPORTS_END */





var styles_FrmRadioPerfilAlumnoMaestroComponent = [_frm_radio_perfil_alumno_maestro_component_scss_shim_ngstyle__WEBPACK_IMPORTED_MODULE_0__["styles"]];
var RenderType_FrmRadioPerfilAlumnoMaestroComponent = /*@__PURE__*/ /*@__PURE__*/ _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵcrt"]({ encapsulation: 0, styles: styles_FrmRadioPerfilAlumnoMaestroComponent, data: {} });

function View_FrmRadioPerfilAlumnoMaestroComponent_2(_l) { return _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵvid"](0, [(_l()(), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵeld"](0, 0, null, null, 1, "div", [], null, null, null, null, null)), (_l()(), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵted"](1, null, ["\n        ", "\n    "]))], null, function (_ck, _v) { var _co = _v.component; var currVal_0 = _co.formularioPadre.get(_co.inputComponent).errors.customErrorMessage; _ck(_v, 1, 0, currVal_0); }); }
function View_FrmRadioPerfilAlumnoMaestroComponent_1(_l) { return _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵvid"](0, [(_l()(), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵeld"](0, 0, null, null, 4, "div", [["class", "myFeedBackError"]], null, null, null, null, null)), (_l()(), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵted"](-1, null, ["\n    "])), (_l()(), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵand"](16777216, null, null, 1, null, View_FrmRadioPerfilAlumnoMaestroComponent_2)), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵdid"](3, 16384, null, 0, _angular_common__WEBPACK_IMPORTED_MODULE_2__["NgIf"], [_angular_core__WEBPACK_IMPORTED_MODULE_1__["ViewContainerRef"], _angular_core__WEBPACK_IMPORTED_MODULE_1__["TemplateRef"]], { ngIf: [0, "ngIf"] }, null), (_l()(), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵted"](-1, null, ["\n"]))], function (_ck, _v) { var _co = _v.component; var currVal_0 = _co.formularioPadre.get(_co.inputComponent).errors.fieldError; _ck(_v, 3, 0, currVal_0); }, null); }
function View_FrmRadioPerfilAlumnoMaestroComponent_0(_l) {
    return _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵvid"](0, [(_l()(), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵeld"](0, 0, null, null, 56, "span", [], [[2, "ng-untouched", null], [2, "ng-touched", null], [2, "ng-pristine", null], [2, "ng-dirty", null], [2, "ng-valid", null], [2, "ng-invalid", null], [2, "ng-pending", null]], [[null, "submit"], [null, "reset"]], function (_v, en, $event) {
            var ad = true;
            if (("submit" === en)) {
                var pd_0 = (_angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵnov"](_v, 1).onSubmit($event) !== false);
                ad = (pd_0 && ad);
            }
            if (("reset" === en)) {
                var pd_1 = (_angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵnov"](_v, 1).onReset() !== false);
                ad = (pd_1 && ad);
            }
            return ad;
        }, null, null)), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵdid"](1, 540672, null, 0, _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormGroupDirective"], [[8, null], [8, null]], { form: [0, "form"] }, null), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵprd"](2048, null, _angular_forms__WEBPACK_IMPORTED_MODULE_3__["ControlContainer"], null, [_angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormGroupDirective"]]), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵdid"](3, 16384, null, 0, _angular_forms__WEBPACK_IMPORTED_MODULE_3__["NgControlStatusGroup"], [_angular_forms__WEBPACK_IMPORTED_MODULE_3__["ControlContainer"]], null, null), (_l()(), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵted"](-1, null, ["\n    "])), (_l()(), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵeld"](5, 0, null, null, 50, "div", [["class", "d-flex justify-content-center"]], [[2, "ng-untouched", null], [2, "ng-touched", null], [2, "ng-pristine", null], [2, "ng-dirty", null], [2, "ng-valid", null], [2, "ng-invalid", null], [2, "ng-pending", null]], null, null, null, null)), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵdid"](6, 212992, null, 0, _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormGroupName"], [[3, _angular_forms__WEBPACK_IMPORTED_MODULE_3__["ControlContainer"]], [8, null], [8, null]], { name: [0, "name"] }, null), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵprd"](2048, null, _angular_forms__WEBPACK_IMPORTED_MODULE_3__["ControlContainer"], null, [_angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormGroupName"]]), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵdid"](8, 16384, null, 0, _angular_forms__WEBPACK_IMPORTED_MODULE_3__["NgControlStatusGroup"], [_angular_forms__WEBPACK_IMPORTED_MODULE_3__["ControlContainer"]], null, null), (_l()(), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵted"](-1, null, ["\n        "])), (_l()(), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵeld"](10, 0, null, null, 21, "label", [["class", "btn-alumno-profesor"], ["for", "alumno"]], null, null, null, null, null)), (_l()(), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵted"](-1, null, ["\n            "])), (_l()(), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵeld"](12, 0, null, null, 18, "fieldset", [["class", "radio-input icon-alumno"]], null, null, null, null, null)), (_l()(), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵted"](-1, null, ["\n                "])), (_l()(), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵted"](-1, null, ["\n                "])), (_l()(), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵeld"](15, 0, null, null, 14, "div", [["class", "m-auto d-flex flex-column p-2"]], null, null, null, null, null)), (_l()(), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵted"](-1, null, ["\n                    "])), (_l()(), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵeld"](17, 0, null, null, 6, "input", [["checked", "checked"], ["class", "radio-form"], ["formControlName", "frm_radio_perfil_AlumnoMaestro"], ["id", "alumno"], ["name", "frm_radio_perfil_AlumnoMaestro"], ["type", "radio"], ["value", "alumno"]], [[2, "ng-untouched", null], [2, "ng-touched", null], [2, "ng-pristine", null], [2, "ng-dirty", null], [2, "ng-valid", null], [2, "ng-invalid", null], [2, "ng-pending", null]], [[null, "input"], [null, "blur"], [null, "compositionstart"], [null, "compositionend"], [null, "change"]], function (_v, en, $event) {
            var ad = true;
            if (("input" === en)) {
                var pd_0 = (_angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵnov"](_v, 18)._handleInput($event.target.value) !== false);
                ad = (pd_0 && ad);
            }
            if (("blur" === en)) {
                var pd_1 = (_angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵnov"](_v, 18).onTouched() !== false);
                ad = (pd_1 && ad);
            }
            if (("compositionstart" === en)) {
                var pd_2 = (_angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵnov"](_v, 18)._compositionStart() !== false);
                ad = (pd_2 && ad);
            }
            if (("compositionend" === en)) {
                var pd_3 = (_angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵnov"](_v, 18)._compositionEnd($event.target.value) !== false);
                ad = (pd_3 && ad);
            }
            if (("change" === en)) {
                var pd_4 = (_angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵnov"](_v, 19).onChange() !== false);
                ad = (pd_4 && ad);
            }
            if (("blur" === en)) {
                var pd_5 = (_angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵnov"](_v, 19).onTouched() !== false);
                ad = (pd_5 && ad);
            }
            return ad;
        }, null, null)), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵdid"](18, 16384, null, 0, _angular_forms__WEBPACK_IMPORTED_MODULE_3__["DefaultValueAccessor"], [_angular_core__WEBPACK_IMPORTED_MODULE_1__["Renderer2"], _angular_core__WEBPACK_IMPORTED_MODULE_1__["ElementRef"], [2, _angular_forms__WEBPACK_IMPORTED_MODULE_3__["COMPOSITION_BUFFER_MODE"]]], null, null), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵdid"](19, 212992, null, 0, _angular_forms__WEBPACK_IMPORTED_MODULE_3__["RadioControlValueAccessor"], [_angular_core__WEBPACK_IMPORTED_MODULE_1__["Renderer2"], _angular_core__WEBPACK_IMPORTED_MODULE_1__["ElementRef"], _angular_forms__WEBPACK_IMPORTED_MODULE_3__["ɵi"], _angular_core__WEBPACK_IMPORTED_MODULE_1__["Injector"]], { name: [0, "name"], formControlName: [1, "formControlName"], value: [2, "value"] }, null), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵprd"](1024, null, _angular_forms__WEBPACK_IMPORTED_MODULE_3__["NG_VALUE_ACCESSOR"], function (p0_0, p1_0) { return [p0_0, p1_0]; }, [_angular_forms__WEBPACK_IMPORTED_MODULE_3__["DefaultValueAccessor"], _angular_forms__WEBPACK_IMPORTED_MODULE_3__["RadioControlValueAccessor"]]), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵdid"](21, 671744, null, 0, _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormControlName"], [[3, _angular_forms__WEBPACK_IMPORTED_MODULE_3__["ControlContainer"]], [8, null], [8, null], [2, _angular_forms__WEBPACK_IMPORTED_MODULE_3__["NG_VALUE_ACCESSOR"]]], { name: [0, "name"] }, null), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵprd"](2048, null, _angular_forms__WEBPACK_IMPORTED_MODULE_3__["NgControl"], null, [_angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormControlName"]]), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵdid"](23, 16384, null, 0, _angular_forms__WEBPACK_IMPORTED_MODULE_3__["NgControlStatus"], [_angular_forms__WEBPACK_IMPORTED_MODULE_3__["NgControl"]], null, null), (_l()(), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵted"](-1, null, ["\n                    "])), (_l()(), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵeld"](25, 0, null, null, 0, "i", [["class", "icon-u icon-u-alumnos"]], null, null, null, null, null)), (_l()(), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵted"](-1, null, ["\n                    "])), (_l()(), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵeld"](27, 0, null, null, 1, "span", [["class", "name"]], null, null, null, null, null)), (_l()(), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵted"](-1, null, ["Alumno | Exalumno"])), (_l()(), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵted"](-1, null, ["\n                "])), (_l()(), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵted"](-1, null, ["                     \n            "])), (_l()(), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵted"](-1, null, ["\n        "])), (_l()(), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵted"](-1, null, ["\n        "])), (_l()(), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵeld"](33, 0, null, null, 21, "label", [["class", "btn-alumno-profesor"], ["for", "profesor"]], null, null, null, null, null)), (_l()(), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵted"](-1, null, ["\n            "])), (_l()(), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵeld"](35, 0, null, null, 18, "fieldset", [["class", "radio-input icon-profesor"]], null, null, null, null, null)), (_l()(), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵted"](-1, null, ["\n                "])), (_l()(), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵted"](-1, null, ["\n                "])), (_l()(), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵeld"](38, 0, null, null, 14, "div", [["class", "m-auto d-flex flex-column p-2"]], null, null, null, null, null)), (_l()(), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵted"](-1, null, ["\n                    "])), (_l()(), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵeld"](40, 0, null, null, 6, "input", [["class", "radio-form"], ["formControlName", "frm_radio_perfil_AlumnoMaestro"], ["id", "profesor"], ["name", "frm_radio_perfil_AlumnoMaestro"], ["type", "radio"], ["value", "profesor"]], [[2, "ng-untouched", null], [2, "ng-touched", null], [2, "ng-pristine", null], [2, "ng-dirty", null], [2, "ng-valid", null], [2, "ng-invalid", null], [2, "ng-pending", null]], [[null, "input"], [null, "blur"], [null, "compositionstart"], [null, "compositionend"], [null, "change"]], function (_v, en, $event) {
            var ad = true;
            if (("input" === en)) {
                var pd_0 = (_angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵnov"](_v, 41)._handleInput($event.target.value) !== false);
                ad = (pd_0 && ad);
            }
            if (("blur" === en)) {
                var pd_1 = (_angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵnov"](_v, 41).onTouched() !== false);
                ad = (pd_1 && ad);
            }
            if (("compositionstart" === en)) {
                var pd_2 = (_angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵnov"](_v, 41)._compositionStart() !== false);
                ad = (pd_2 && ad);
            }
            if (("compositionend" === en)) {
                var pd_3 = (_angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵnov"](_v, 41)._compositionEnd($event.target.value) !== false);
                ad = (pd_3 && ad);
            }
            if (("change" === en)) {
                var pd_4 = (_angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵnov"](_v, 42).onChange() !== false);
                ad = (pd_4 && ad);
            }
            if (("blur" === en)) {
                var pd_5 = (_angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵnov"](_v, 42).onTouched() !== false);
                ad = (pd_5 && ad);
            }
            return ad;
        }, null, null)), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵdid"](41, 16384, null, 0, _angular_forms__WEBPACK_IMPORTED_MODULE_3__["DefaultValueAccessor"], [_angular_core__WEBPACK_IMPORTED_MODULE_1__["Renderer2"], _angular_core__WEBPACK_IMPORTED_MODULE_1__["ElementRef"], [2, _angular_forms__WEBPACK_IMPORTED_MODULE_3__["COMPOSITION_BUFFER_MODE"]]], null, null), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵdid"](42, 212992, null, 0, _angular_forms__WEBPACK_IMPORTED_MODULE_3__["RadioControlValueAccessor"], [_angular_core__WEBPACK_IMPORTED_MODULE_1__["Renderer2"], _angular_core__WEBPACK_IMPORTED_MODULE_1__["ElementRef"], _angular_forms__WEBPACK_IMPORTED_MODULE_3__["ɵi"], _angular_core__WEBPACK_IMPORTED_MODULE_1__["Injector"]], { name: [0, "name"], formControlName: [1, "formControlName"], value: [2, "value"] }, null), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵprd"](1024, null, _angular_forms__WEBPACK_IMPORTED_MODULE_3__["NG_VALUE_ACCESSOR"], function (p0_0, p1_0) { return [p0_0, p1_0]; }, [_angular_forms__WEBPACK_IMPORTED_MODULE_3__["DefaultValueAccessor"], _angular_forms__WEBPACK_IMPORTED_MODULE_3__["RadioControlValueAccessor"]]), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵdid"](44, 671744, null, 0, _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormControlName"], [[3, _angular_forms__WEBPACK_IMPORTED_MODULE_3__["ControlContainer"]], [8, null], [8, null], [2, _angular_forms__WEBPACK_IMPORTED_MODULE_3__["NG_VALUE_ACCESSOR"]]], { name: [0, "name"] }, null), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵprd"](2048, null, _angular_forms__WEBPACK_IMPORTED_MODULE_3__["NgControl"], null, [_angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormControlName"]]), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵdid"](46, 16384, null, 0, _angular_forms__WEBPACK_IMPORTED_MODULE_3__["NgControlStatus"], [_angular_forms__WEBPACK_IMPORTED_MODULE_3__["NgControl"]], null, null), (_l()(), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵted"](-1, null, ["\n                    "])), (_l()(), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵeld"](48, 0, null, null, 0, "i", [["class", "icon-u icon-u-profesores"]], null, null, null, null, null)), (_l()(), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵted"](-1, null, ["\n                    "])), (_l()(), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵeld"](50, 0, null, null, 1, "span", [["class", "name"]], null, null, null, null, null)), (_l()(), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵted"](-1, null, ["Profesor"])), (_l()(), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵted"](-1, null, ["\n                "])), (_l()(), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵted"](-1, null, ["\n            "])), (_l()(), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵted"](-1, null, ["\n        "])), (_l()(), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵted"](-1, null, ["\n    "])), (_l()(), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵted"](-1, null, ["\n"])), (_l()(), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵted"](-1, null, ["\n"])), (_l()(), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵand"](16777216, null, null, 1, null, View_FrmRadioPerfilAlumnoMaestroComponent_1)), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵdid"](59, 16384, null, 0, _angular_common__WEBPACK_IMPORTED_MODULE_2__["NgIf"], [_angular_core__WEBPACK_IMPORTED_MODULE_1__["ViewContainerRef"], _angular_core__WEBPACK_IMPORTED_MODULE_1__["TemplateRef"]], { ngIf: [0, "ngIf"] }, null), (_l()(), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵted"](-1, null, ["\n\n"])), (_l()(), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵted"](-1, null, ["\n\n"]))], function (_ck, _v) { var _co = _v.component; var currVal_7 = _co.formularioPadre; _ck(_v, 1, 0, currVal_7); var currVal_15 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵinlineInterpolate"](1, "", _co.fase, ""); _ck(_v, 6, 0, currVal_15); var currVal_23 = "frm_radio_perfil_AlumnoMaestro"; var currVal_24 = "frm_radio_perfil_AlumnoMaestro"; var currVal_25 = "alumno"; _ck(_v, 19, 0, currVal_23, currVal_24, currVal_25); var currVal_26 = "frm_radio_perfil_AlumnoMaestro"; _ck(_v, 21, 0, currVal_26); var currVal_34 = "frm_radio_perfil_AlumnoMaestro"; var currVal_35 = "frm_radio_perfil_AlumnoMaestro"; var currVal_36 = "profesor"; _ck(_v, 42, 0, currVal_34, currVal_35, currVal_36); var currVal_37 = "frm_radio_perfil_AlumnoMaestro"; _ck(_v, 44, 0, currVal_37); var currVal_38 = (_co.formularioPadre.get(_co.inputComponent).errors && _co.formularioPadre.get(_co.inputComponent).dirty); _ck(_v, 59, 0, currVal_38); }, function (_ck, _v) { var currVal_0 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵnov"](_v, 3).ngClassUntouched; var currVal_1 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵnov"](_v, 3).ngClassTouched; var currVal_2 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵnov"](_v, 3).ngClassPristine; var currVal_3 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵnov"](_v, 3).ngClassDirty; var currVal_4 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵnov"](_v, 3).ngClassValid; var currVal_5 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵnov"](_v, 3).ngClassInvalid; var currVal_6 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵnov"](_v, 3).ngClassPending; _ck(_v, 0, 0, currVal_0, currVal_1, currVal_2, currVal_3, currVal_4, currVal_5, currVal_6); var currVal_8 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵnov"](_v, 8).ngClassUntouched; var currVal_9 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵnov"](_v, 8).ngClassTouched; var currVal_10 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵnov"](_v, 8).ngClassPristine; var currVal_11 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵnov"](_v, 8).ngClassDirty; var currVal_12 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵnov"](_v, 8).ngClassValid; var currVal_13 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵnov"](_v, 8).ngClassInvalid; var currVal_14 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵnov"](_v, 8).ngClassPending; _ck(_v, 5, 0, currVal_8, currVal_9, currVal_10, currVal_11, currVal_12, currVal_13, currVal_14); var currVal_16 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵnov"](_v, 23).ngClassUntouched; var currVal_17 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵnov"](_v, 23).ngClassTouched; var currVal_18 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵnov"](_v, 23).ngClassPristine; var currVal_19 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵnov"](_v, 23).ngClassDirty; var currVal_20 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵnov"](_v, 23).ngClassValid; var currVal_21 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵnov"](_v, 23).ngClassInvalid; var currVal_22 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵnov"](_v, 23).ngClassPending; _ck(_v, 17, 0, currVal_16, currVal_17, currVal_18, currVal_19, currVal_20, currVal_21, currVal_22); var currVal_27 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵnov"](_v, 46).ngClassUntouched; var currVal_28 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵnov"](_v, 46).ngClassTouched; var currVal_29 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵnov"](_v, 46).ngClassPristine; var currVal_30 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵnov"](_v, 46).ngClassDirty; var currVal_31 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵnov"](_v, 46).ngClassValid; var currVal_32 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵnov"](_v, 46).ngClassInvalid; var currVal_33 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵnov"](_v, 46).ngClassPending; _ck(_v, 40, 0, currVal_27, currVal_28, currVal_29, currVal_30, currVal_31, currVal_32, currVal_33); });
}
function View_FrmRadioPerfilAlumnoMaestroComponent_Host_0(_l) { return _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵvid"](0, [(_l()(), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵeld"](0, 0, null, null, 1, "frm-radio-perfil-alumno-maestro", [], null, null, null, View_FrmRadioPerfilAlumnoMaestroComponent_0, RenderType_FrmRadioPerfilAlumnoMaestroComponent)), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵdid"](1, 114688, null, 0, _frm_radio_perfil_alumno_maestro_component__WEBPACK_IMPORTED_MODULE_4__["FrmRadioPerfilAlumnoMaestroComponent"], [], null, null)], function (_ck, _v) { _ck(_v, 1, 0); }, null); }
var FrmRadioPerfilAlumnoMaestroComponentNgFactory = /*@__PURE__*/ /*@__PURE__*/ _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵccf"]("frm-radio-perfil-alumno-maestro", _frm_radio_perfil_alumno_maestro_component__WEBPACK_IMPORTED_MODULE_4__["FrmRadioPerfilAlumnoMaestroComponent"], View_FrmRadioPerfilAlumnoMaestroComponent_Host_0, { formularioPadre: "formularioPadre", asterisk: "asterisk", fase: "fase" }, {}, []);



/***/ }),

/***/ "./src/app/formularios/forms_child_components/frm-radio-perfil-alumno-maestro/frm-radio-perfil-alumno-maestro.component.scss.shim.ngstyle.js":
/*!***************************************************************************************************************************************************!*\
  !*** ./src/app/formularios/forms_child_components/frm-radio-perfil-alumno-maestro/frm-radio-perfil-alumno-maestro.component.scss.shim.ngstyle.js ***!
  \***************************************************************************************************************************************************/
/*! exports provided: styles */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "styles", function() { return styles; });
/**
 * @fileoverview This file was generated by the Angular template compiler. Do not edit.
 *
 * @suppress {suspiciousCode,uselessCode,missingProperties,missingOverride,checkTypes}
 * tslint:disable
 */
/** PURE_IMPORTS_START  PURE_IMPORTS_END */
/** PURE_IMPORTS_START  PURE_IMPORTS_END */
var styles = [".btn-alumno-profesor[_ngcontent-%COMP%] {\n  width: 50%;\n  text-align: center;\n  cursor: pointer;\n  border: solid 1px #757575;\n  margin: -1px; }\n\nfieldset[_ngcontent-%COMP%]   div[_ngcontent-%COMP%]   i[_ngcontent-%COMP%] {\n  font-size: 1.25rem; }\n\nfieldset[_ngcontent-%COMP%]   div[_ngcontent-%COMP%]   .name[_ngcontent-%COMP%] {\n  font-size: 0.875rem;\n  font-family: roboto;\n  margin: 0;\n  margin-top: 0.5rem; }"];



/***/ }),

/***/ "./src/app/formularios/forms_child_components/frm-radio-perfil-alumno-maestro/frm-radio-perfil-alumno-maestro.component.ts":
/*!*********************************************************************************************************************************!*\
  !*** ./src/app/formularios/forms_child_components/frm-radio-perfil-alumno-maestro/frm-radio-perfil-alumno-maestro.component.ts ***!
  \*********************************************************************************************************************************/
/*! exports provided: FrmRadioPerfilAlumnoMaestroComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "FrmRadioPerfilAlumnoMaestroComponent", function() { return FrmRadioPerfilAlumnoMaestroComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/esm5/core.js");

var FrmRadioPerfilAlumnoMaestroComponent = /** @class */ /*@__PURE__*/ (function () {
    function FrmRadioPerfilAlumnoMaestroComponent() {
        jQuery(document).ready(function () {
            $(".radio-form").click(function () {
                console.log(".icon-" + $(this).val());
                $("fieldset.radio-input").removeAttr('style');
                $("fieldset div label").removeAttr('style');
                $("fieldset div i.icono").css({
                    'color': '#006fba'
                });
                $("fieldset div i.icon-" + $(this).val()).css({
                    'color': '#ffffff'
                });
                $("fieldset div label.icon-" + $(this).val()).css({
                    'color': '#ffffff'
                });
                $("fieldset.icon-" + $(this).val()).css({
                    'background-color': '#757575',
                    'color': '#ffffff'
                });
            });
        });
    }
    //Inicializacion del componente
    FrmRadioPerfilAlumnoMaestroComponent.prototype.ngOnInit = function () {
        this.inputComponent = this.fase + '.frm_radio_perfil_AlumnoMaestro';
    };
    return FrmRadioPerfilAlumnoMaestroComponent;
}());



/***/ }),

/***/ "./src/app/formularios/forms_parent_components/formulario-login/formulario-login.component.ngfactory.js":
/*!**************************************************************************************************************!*\
  !*** ./src/app/formularios/forms_parent_components/formulario-login/formulario-login.component.ngfactory.js ***!
  \**************************************************************************************************************/
/*! exports provided: RenderType_FormularioLoginComponent, View_FormularioLoginComponent_0, View_FormularioLoginComponent_Host_0, FormularioLoginComponentNgFactory */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "RenderType_FormularioLoginComponent", function() { return RenderType_FormularioLoginComponent; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "View_FormularioLoginComponent_0", function() { return View_FormularioLoginComponent_0; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "View_FormularioLoginComponent_Host_0", function() { return View_FormularioLoginComponent_Host_0; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "FormularioLoginComponentNgFactory", function() { return FormularioLoginComponentNgFactory; });
/* harmony import */ var _formulario_login_component_scss_shim_ngstyle__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./formulario-login.component.scss.shim.ngstyle */ "./src/app/formularios/forms_parent_components/formulario-login/formulario-login.component.scss.shim.ngstyle.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/esm5/core.js");
/* harmony import */ var _forms_child_components_frm_radio_perfil_alumno_maestro_frm_radio_perfil_alumno_maestro_component_ngfactory__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../forms_child_components/frm-radio-perfil-alumno-maestro/frm-radio-perfil-alumno-maestro.component.ngfactory */ "./src/app/formularios/forms_child_components/frm-radio-perfil-alumno-maestro/frm-radio-perfil-alumno-maestro.component.ngfactory.js");
/* harmony import */ var _forms_child_components_frm_radio_perfil_alumno_maestro_frm_radio_perfil_alumno_maestro_component__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../forms_child_components/frm-radio-perfil-alumno-maestro/frm-radio-perfil-alumno-maestro.component */ "./src/app/formularios/forms_child_components/frm-radio-perfil-alumno-maestro/frm-radio-perfil-alumno-maestro.component.ts");
/* harmony import */ var _forms_child_components_frm_mail_login_frm_mail_login_component_ngfactory__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../../forms_child_components/frm-mail-login/frm-mail-login.component.ngfactory */ "./src/app/formularios/forms_child_components/frm-mail-login/frm-mail-login.component.ngfactory.js");
/* harmony import */ var _forms_child_components_frm_mail_login_frm_mail_login_component__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../../forms_child_components/frm-mail-login/frm-mail-login.component */ "./src/app/formularios/forms_child_components/frm-mail-login/frm-mail-login.component.ts");
/* harmony import */ var _forms_services_reglasInputs_service__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../../forms_services/reglasInputs.service */ "./src/app/formularios/forms_services/reglasInputs.service.ts");
/* harmony import */ var _forms_child_components_frm_ncuenta_frm_ncuenta_component_ngfactory__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ../../forms_child_components/frm-ncuenta/frm-ncuenta.component.ngfactory */ "./src/app/formularios/forms_child_components/frm-ncuenta/frm-ncuenta.component.ngfactory.js");
/* harmony import */ var _forms_child_components_frm_ncuenta_frm_ncuenta_component__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ../../forms_child_components/frm-ncuenta/frm-ncuenta.component */ "./src/app/formularios/forms_child_components/frm-ncuenta/frm-ncuenta.component.ts");
/* harmony import */ var _forms_child_components_frm_aviso_de_privacidad_frm_aviso_de_privacidad_component_ngfactory__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ../../forms_child_components/frm-aviso-de-privacidad/frm-aviso-de-privacidad.component.ngfactory */ "./src/app/formularios/forms_child_components/frm-aviso-de-privacidad/frm-aviso-de-privacidad.component.ngfactory.js");
/* harmony import */ var _forms_child_components_frm_aviso_de_privacidad_frm_aviso_de_privacidad_component__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! ../../forms_child_components/frm-aviso-de-privacidad/frm-aviso-de-privacidad.component */ "./src/app/formularios/forms_child_components/frm-aviso-de-privacidad/frm-aviso-de-privacidad.component.ts");
/* harmony import */ var _forms_child_components_frm_problemas_cuenta_frm_problemas_cuenta_component_ngfactory__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! ../../forms_child_components/frm-problemas-cuenta/frm-problemas-cuenta.component.ngfactory */ "./src/app/formularios/forms_child_components/frm-problemas-cuenta/frm-problemas-cuenta.component.ngfactory.js");
/* harmony import */ var _forms_child_components_frm_problemas_cuenta_frm_problemas_cuenta_component__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! ../../forms_child_components/frm-problemas-cuenta/frm-problemas-cuenta.component */ "./src/app/formularios/forms_child_components/frm-problemas-cuenta/frm-problemas-cuenta.component.ts");
/* harmony import */ var _forms_child_components_frm_busqueda_integrada_frm_busqueda_integrada_component_ngfactory__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(/*! ../../forms_child_components/frm-busqueda-integrada/frm-busqueda-integrada.component.ngfactory */ "./src/app/formularios/forms_child_components/frm-busqueda-integrada/frm-busqueda-integrada.component.ngfactory.js");
/* harmony import */ var _services_navegador_service__WEBPACK_IMPORTED_MODULE_14__ = __webpack_require__(/*! ../../../services/navegador.service */ "./src/app/services/navegador.service.ts");
/* harmony import */ var _forms_services_formCookies_service__WEBPACK_IMPORTED_MODULE_15__ = __webpack_require__(/*! ../../forms_services/formCookies.service */ "./src/app/formularios/forms_services/formCookies.service.ts");
/* harmony import */ var ngx_cookie_service_cookie_service_cookie_service__WEBPACK_IMPORTED_MODULE_16__ = __webpack_require__(/*! ngx-cookie-service/cookie-service/cookie.service */ "./node_modules/ngx-cookie-service/cookie-service/cookie.service.js");
/* harmony import */ var _forms_child_components_frm_busqueda_integrada_frm_busqueda_integrada_component__WEBPACK_IMPORTED_MODULE_17__ = __webpack_require__(/*! ../../forms_child_components/frm-busqueda-integrada/frm-busqueda-integrada.component */ "./src/app/formularios/forms_child_components/frm-busqueda-integrada/frm-busqueda-integrada.component.ts");
/* harmony import */ var _forms_services_validaciones_service__WEBPACK_IMPORTED_MODULE_18__ = __webpack_require__(/*! ../../forms_services/validaciones.service */ "./src/app/formularios/forms_services/validaciones.service.ts");
/* harmony import */ var _services_getJson_service__WEBPACK_IMPORTED_MODULE_19__ = __webpack_require__(/*! ../../../services/getJson.service */ "./src/app/services/getJson.service.ts");
/* harmony import */ var _angular_platform_browser__WEBPACK_IMPORTED_MODULE_20__ = __webpack_require__(/*! @angular/platform-browser */ "./node_modules/@angular/platform-browser/esm5/platform-browser.js");
/* harmony import */ var _forms_services_send_service__WEBPACK_IMPORTED_MODULE_21__ = __webpack_require__(/*! ../../forms_services/send.service */ "./src/app/formularios/forms_services/send.service.ts");
/* harmony import */ var _forms_services_service_pipe__WEBPACK_IMPORTED_MODULE_22__ = __webpack_require__(/*! ../../forms_services/service.pipe */ "./src/app/formularios/forms_services/service.pipe.ts");
/* harmony import */ var _services_readJson_service__WEBPACK_IMPORTED_MODULE_23__ = __webpack_require__(/*! ../../../services/readJson.service */ "./src/app/services/readJson.service.ts");
/* harmony import */ var _services_generic_service__WEBPACK_IMPORTED_MODULE_24__ = __webpack_require__(/*! ../../../services/generic.service */ "./src/app/services/generic.service.ts");
/* harmony import */ var _formulario_login_component__WEBPACK_IMPORTED_MODULE_25__ = __webpack_require__(/*! ./formulario-login.component */ "./src/app/formularios/forms_parent_components/formulario-login/formulario-login.component.ts");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_26__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/esm5/forms.js");
/**
 * @fileoverview This file was generated by the Angular template compiler. Do not edit.
 *
 * @suppress {suspiciousCode,uselessCode,missingProperties,missingOverride,checkTypes}
 * tslint:disable
 */
/** PURE_IMPORTS_START _formulario_login.component.scss.shim.ngstyle,_angular_core,_.._forms_child_components_frm_radio_perfil_alumno_maestro_frm_radio_perfil_alumno_maestro.component.ngfactory,_.._forms_child_components_frm_radio_perfil_alumno_maestro_frm_radio_perfil_alumno_maestro.component,_.._forms_child_components_frm_mail_login_frm_mail_login.component.ngfactory,_.._forms_child_components_frm_mail_login_frm_mail_login.component,_.._forms_services_reglasInputs.service,_.._forms_child_components_frm_ncuenta_frm_ncuenta.component.ngfactory,_.._forms_child_components_frm_ncuenta_frm_ncuenta.component,_.._forms_child_components_frm_aviso_de_privacidad_frm_aviso_de_privacidad.component.ngfactory,_.._forms_child_components_frm_aviso_de_privacidad_frm_aviso_de_privacidad.component,_.._forms_child_components_frm_problemas_cuenta_frm_problemas_cuenta.component.ngfactory,_.._forms_child_components_frm_problemas_cuenta_frm_problemas_cuenta.component,_.._forms_child_components_frm_busqueda_integrada_frm_busqueda_integrada.component.ngfactory,_.._.._services_navegador.service,_.._forms_services_formCookies.service,ngx_cookie_service_cookie_service_cookie.service,_.._forms_child_components_frm_busqueda_integrada_frm_busqueda_integrada.component,_.._forms_services_validaciones.service,_.._.._services_getJson.service,_angular_platform_browser,_.._forms_services_send.service,_.._forms_services_service.pipe,_.._.._services_readJson.service,_.._.._services_generic.service,_formulario_login.component,_angular_forms PURE_IMPORTS_END */
/** PURE_IMPORTS_START _formulario_login.component.scss.shim.ngstyle,_angular_core,_.._forms_child_components_frm_radio_perfil_alumno_maestro_frm_radio_perfil_alumno_maestro.component.ngfactory,_.._forms_child_components_frm_radio_perfil_alumno_maestro_frm_radio_perfil_alumno_maestro.component,_.._forms_child_components_frm_mail_login_frm_mail_login.component.ngfactory,_.._forms_child_components_frm_mail_login_frm_mail_login.component,_.._forms_services_reglasInputs.service,_.._forms_child_components_frm_ncuenta_frm_ncuenta.component.ngfactory,_.._forms_child_components_frm_ncuenta_frm_ncuenta.component,_.._forms_child_components_frm_aviso_de_privacidad_frm_aviso_de_privacidad.component.ngfactory,_.._forms_child_components_frm_aviso_de_privacidad_frm_aviso_de_privacidad.component,_.._forms_child_components_frm_problemas_cuenta_frm_problemas_cuenta.component.ngfactory,_.._forms_child_components_frm_problemas_cuenta_frm_problemas_cuenta.component,_.._forms_child_components_frm_busqueda_integrada_frm_busqueda_integrada.component.ngfactory,_.._.._services_navegador.service,_.._forms_services_formCookies.service,ngx_cookie_service_cookie_service_cookie.service,_.._forms_child_components_frm_busqueda_integrada_frm_busqueda_integrada.component,_.._forms_services_validaciones.service,_.._.._services_getJson.service,_angular_platform_browser,_.._forms_services_send.service,_.._forms_services_service.pipe,_.._.._services_readJson.service,_.._.._services_generic.service,_formulario_login.component,_angular_forms PURE_IMPORTS_END */



























var styles_FormularioLoginComponent = [_formulario_login_component_scss_shim_ngstyle__WEBPACK_IMPORTED_MODULE_0__["styles"]];
var RenderType_FormularioLoginComponent = /*@__PURE__*/ /*@__PURE__*/ _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵcrt"]({ encapsulation: 0, styles: styles_FormularioLoginComponent, data: {} });

function View_FormularioLoginComponent_0(_l) {
    return _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵvid"](0, [(_l()(), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵted"](-1, null, ["\n"])), (_l()(), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵeld"](1, 0, null, null, 116, "div", [["aria-hidden", "true"], ["aria-labelledby", "myModalLabel"], ["class", "modal fade modal-login-biblioteca"], ["id", "modal_frm_biblioteca"], ["role", "dialog"], ["tabindex", "-1"]], null, null, null, null, null)), (_l()(), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵted"](-1, null, ["\n    "])), (_l()(), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵted"](-1, null, ["\n    "])), (_l()(), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵted"](-1, null, ["\n    "])), (_l()(), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵeld"](5, 0, null, null, 111, "div", [["class", "h-100 d-flex align-items-center"]], null, null, null, null, null)), (_l()(), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵted"](-1, null, ["\n        "])), (_l()(), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵeld"](7, 0, null, null, 108, "div", [["class", "modal-dialog modal-fluid contenido-modal"], ["role", "document"]], null, null, null, null, null)), (_l()(), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵted"](-1, null, ["\n            "])), (_l()(), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵted"](-1, null, ["\n            "])), (_l()(), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵeld"](10, 0, null, null, 103, "div", [["class", "modal-content modal-header"]], null, null, null, null, null)), (_l()(), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵted"](-1, null, ["\n                "])), (_l()(), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵted"](-1, null, ["\n                "])), (_l()(), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵeld"](13, 0, null, null, 21, "div", [["class", "row adapt-row-biblioteca"]], null, null, null, null, null)), (_l()(), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵted"](-1, null, ["\n                    "])), (_l()(), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵeld"](15, 0, null, null, 1, "div", [["class", "col-11 titulo-formulario title-modal-desk text-center"]], null, null, null, null, null)), (_l()(), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵted"](-1, null, ["\n                    "])), (_l()(), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵted"](-1, null, ["\n                    "])), (_l()(), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵeld"](18, 0, null, null, 15, "div", [["class", "col-1 p-2 titulo-formulario"]], null, null, null, null, null)), (_l()(), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵted"](-1, null, ["\n                        "])), (_l()(), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵeld"](20, 0, null, null, 12, "div", [], null, null, null, null, null)), (_l()(), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵted"](-1, null, ["\n                            "])), (_l()(), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵeld"](22, 0, null, null, 1, "button", [["aria-label", "Close"], ["class", "close cerrar-modal"], ["data-dismiss", "modal"], ["type", "button"]], null, null, null, null, null)), (_l()(), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵted"](-1, null, ["\n                            "])), (_l()(), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵted"](-1, null, ["\n                            "])), (_l()(), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵeld"](25, 0, null, null, 6, "div", [["class", "icons-desktop d-flex align-items-center justify-content-center"]], null, null, null, null, null)), (_l()(), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵted"](-1, null, ["\n                                "])), (_l()(), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵeld"](27, 0, null, null, 3, "a", [["data-dismiss", "modal"], ["href", "javascript:void(0)"]], null, null, null, null, null)), (_l()(), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵted"](-1, null, ["\n                                    "])), (_l()(), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵeld"](29, 0, null, null, 0, "i", [["class", "icon-u icon-u-cerrar"], ["data-dismiss", "modal"]], null, null, null, null, null)), (_l()(), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵted"](-1, null, ["\n                                "])), (_l()(), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵted"](-1, null, ["\n                            "])), (_l()(), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵted"](-1, null, ["\n                        "])), (_l()(), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵted"](-1, null, ["\n                    "])), (_l()(), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵted"](-1, null, ["\n                "])), (_l()(), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵted"](-1, null, ["\n                \n                "])), (_l()(), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵted"](-1, null, ["\n                    "])), (_l()(), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵted"](-1, null, ["\n                "])), (_l()(), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵted"](-1, null, ["\n                \n                "])), (_l()(), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵted"](-1, null, ["            \n                "])), (_l()(), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵted"](-1, null, ["\n                "])), (_l()(), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵeld"](41, 0, null, null, 12, "div", [["class", "row content-logo pt-1 pl-5 pr-5"]], null, null, null, null, null)), (_l()(), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵted"](-1, null, ["\n                    "])), (_l()(), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵeld"](43, 0, null, null, 3, "div", [["class", "col-2 p-0 logo-formulario-biblioteca"]], null, null, null, null, null)), (_l()(), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵted"](-1, null, ["\n                        "])), (_l()(), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵeld"](45, 0, null, null, 0, "i", [["class", "icon-u icon-u-biblioteca icono-biblioteca"]], null, null, null, null, null)), (_l()(), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵted"](-1, null, ["\n                    "])), (_l()(), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵted"](-1, null, ["\n                    \n                    "])), (_l()(), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵeld"](48, 0, null, null, 4, "div", [["class", "col-10"]], null, null, null, null, null)), (_l()(), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵted"](-1, null, ["\n                        "])), (_l()(), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵeld"](50, 0, null, null, 1, "h2", [["class", "titulo-modal"]], null, null, null, null, null)), (_l()(), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵted"](51, null, ["", ""])), (_l()(), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵted"](-1, null, ["\n                    "])), (_l()(), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵted"](-1, null, ["                \n                "])), (_l()(), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵted"](-1, null, ["            \n                \n                "])), (_l()(), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵted"](-1, null, ["            \n                "])), (_l()(), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵeld"](56, 0, null, null, 38, "div", [["class", "modal-body modal-body-login col-12 pl-5 pr-5"]], null, null, null, null, null)), (_l()(), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵted"](-1, null, ["\n                    "])), (_l()(), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵted"](-1, null, ["\n                        "])), (_l()(), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵeld"](59, 0, null, null, 31, "div", [["id", "faseUno"]], null, null, null, null, null)), (_l()(), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵted"](-1, null, ["\n                            "])), (_l()(), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵeld"](61, 0, null, null, 4, "div", [["class", "mt-1"]], null, null, null, null, null)), (_l()(), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵted"](-1, null, ["\n                                "])), (_l()(), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵeld"](63, 0, null, null, 1, "frm-radio-perfil-alumno-maestro", [], null, null, null, _forms_child_components_frm_radio_perfil_alumno_maestro_frm_radio_perfil_alumno_maestro_component_ngfactory__WEBPACK_IMPORTED_MODULE_2__["View_FrmRadioPerfilAlumnoMaestroComponent_0"], _forms_child_components_frm_radio_perfil_alumno_maestro_frm_radio_perfil_alumno_maestro_component_ngfactory__WEBPACK_IMPORTED_MODULE_2__["RenderType_FrmRadioPerfilAlumnoMaestroComponent"])), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵdid"](64, 114688, null, 0, _forms_child_components_frm_radio_perfil_alumno_maestro_frm_radio_perfil_alumno_maestro_component__WEBPACK_IMPORTED_MODULE_3__["FrmRadioPerfilAlumnoMaestroComponent"], [], { formularioPadre: [0, "formularioPadre"], fase: [1, "fase"] }, null), (_l()(), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵted"](-1, null, ["\n                            "])), (_l()(), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵted"](-1, null, ["\n    \n                            "])), (_l()(), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵeld"](67, 0, null, null, 10, "div", [["class", "row mt-2"]], null, null, null, null, null)), (_l()(), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵted"](-1, null, ["\n                                "])), (_l()(), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵeld"](69, 0, null, null, 4, "div", [["class", "col-7"]], null, null, null, null, null)), (_l()(), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵted"](-1, null, ["\n                                    "])), (_l()(), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵeld"](71, 0, null, null, 1, "frm-mail-login", [], null, null, null, _forms_child_components_frm_mail_login_frm_mail_login_component_ngfactory__WEBPACK_IMPORTED_MODULE_4__["View_frmMailLoginComponent_0"], _forms_child_components_frm_mail_login_frm_mail_login_component_ngfactory__WEBPACK_IMPORTED_MODULE_4__["RenderType_frmMailLoginComponent"])), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵdid"](72, 114688, null, 0, _forms_child_components_frm_mail_login_frm_mail_login_component__WEBPACK_IMPORTED_MODULE_5__["frmMailLoginComponent"], [_forms_services_reglasInputs_service__WEBPACK_IMPORTED_MODULE_6__["reglasInputsService"]], { formularioPadre: [0, "formularioPadre"], fase: [1, "fase"] }, null), (_l()(), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵted"](-1, null, ["\n                                "])), (_l()(), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵted"](-1, null, ["\n                                "])), (_l()(), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵeld"](75, 0, null, null, 1, "div", [["class", "col-5 dominio"]], null, null, null, null, null)), (_l()(), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵted"](76, null, ["", ""])), (_l()(), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵted"](-1, null, ["                            \n                            "])), (_l()(), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵted"](-1, null, ["\n    \n                            "])), (_l()(), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵeld"](79, 0, null, null, 4, "div", [["class", "col-12 p-0 mt-1 cuenta"]], null, null, null, null, null)), (_l()(), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵted"](-1, null, ["\n                                "])), (_l()(), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵeld"](81, 0, null, null, 1, "frm-ncuenta", [], null, null, null, _forms_child_components_frm_ncuenta_frm_ncuenta_component_ngfactory__WEBPACK_IMPORTED_MODULE_7__["View_FrmNcuentaComponent_0"], _forms_child_components_frm_ncuenta_frm_ncuenta_component_ngfactory__WEBPACK_IMPORTED_MODULE_7__["RenderType_FrmNcuentaComponent"])), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵdid"](82, 114688, null, 0, _forms_child_components_frm_ncuenta_frm_ncuenta_component__WEBPACK_IMPORTED_MODULE_8__["FrmNcuentaComponent"], [], { formularioPadre: [0, "formularioPadre"], asterisk: [1, "asterisk"], fase: [2, "fase"] }, null), (_l()(), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵted"](-1, null, ["\n                            "])), (_l()(), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵted"](-1, null, ["\n    \n                            "])), (_l()(), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵeld"](85, 0, null, null, 4, "div", [["class", "col-12 p-0 mt-2 aviso-privacidad"]], null, null, null, null, null)), (_l()(), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵted"](-1, null, ["\n                                "])), (_l()(), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵeld"](87, 0, null, null, 1, "app-frm-aviso-de-privacidad", [], null, null, null, _forms_child_components_frm_aviso_de_privacidad_frm_aviso_de_privacidad_component_ngfactory__WEBPACK_IMPORTED_MODULE_9__["View_FrmAvisoDePrivacidadComponent_0"], _forms_child_components_frm_aviso_de_privacidad_frm_aviso_de_privacidad_component_ngfactory__WEBPACK_IMPORTED_MODULE_9__["RenderType_FrmAvisoDePrivacidadComponent"])), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵdid"](88, 114688, null, 0, _forms_child_components_frm_aviso_de_privacidad_frm_aviso_de_privacidad_component__WEBPACK_IMPORTED_MODULE_10__["FrmAvisoDePrivacidadComponent"], [], { formularioPadre: [0, "formularioPadre"], fase: [1, "fase"] }, null), (_l()(), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵted"](-1, null, ["\n                            "])), (_l()(), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵted"](-1, null, ["                            \n                        "])), (_l()(), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵted"](-1, null, ["\n    \n                        "])), (_l()(), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵeld"](92, 0, null, null, 0, "div", [["class", "text-center p-0"], ["id", "preload-library"]], null, null, null, null, null)), (_l()(), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵted"](-1, null, ["    \n                    "])), (_l()(), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵted"](-1, null, ["\n                "])), (_l()(), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵted"](-1, null, ["            \n                "])), (_l()(), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵted"](-1, null, ["\n    \n                "])), (_l()(), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵted"](-1, null, ["\n                "])), (_l()(), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵeld"](98, 0, null, null, 7, "div", [["class", "modal-footer"]], null, null, null, null, null)), (_l()(), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵted"](-1, null, ["\n                    "])), (_l()(), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵeld"](100, 0, null, null, 4, "div", [["class", "div-btn-ingresar pb-1 pl-4 pr-4"]], null, null, null, null, null)), (_l()(), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵted"](-1, null, ["                                \n                        "])), (_l()(), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵeld"](102, 0, null, null, 1, "button", [["class", "btn btn_ingresar"], ["type", "submit"]], [[8, "id", 0]], [[null, "click"]], function (_v, en, $event) {
            var ad = true;
            var _co = _v.component;
            if (("click" === en)) {
                var pd_0 = (_co.enviar() !== false);
                ad = (pd_0 && ad);
            }
            return ad;
        }, null, null)), (_l()(), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵted"](-1, null, ["\n                            Ingresar.\n                        "])), (_l()(), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵted"](-1, null, ["                   \n                    "])), (_l()(), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵted"](-1, null, ["                            \n                "])), (_l()(), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵted"](-1, null, ["\n                "])), (_l()(), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵted"](-1, null, ["\n                "])), (_l()(), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵeld"](108, 0, null, null, 4, "div", [["class", "col-10 text-center"]], null, null, null, null, null)), (_l()(), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵted"](-1, null, ["                    \n                    "])), (_l()(), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵeld"](110, 0, null, null, 1, "frm-problemas-cuenta", [], null, null, null, _forms_child_components_frm_problemas_cuenta_frm_problemas_cuenta_component_ngfactory__WEBPACK_IMPORTED_MODULE_11__["View_frmProblemasCuenta_0"], _forms_child_components_frm_problemas_cuenta_frm_problemas_cuenta_component_ngfactory__WEBPACK_IMPORTED_MODULE_11__["RenderType_frmProblemasCuenta"])), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵdid"](111, 49152, null, 0, _forms_child_components_frm_problemas_cuenta_frm_problemas_cuenta_component__WEBPACK_IMPORTED_MODULE_12__["frmProblemasCuenta"], [], null, null), (_l()(), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵted"](-1, null, ["                   \n                "])), (_l()(), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵted"](-1, null, ["\n            "])), (_l()(), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵted"](-1, null, ["\n            "])), (_l()(), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵted"](-1, null, ["\n        "])), (_l()(), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵted"](-1, null, ["\n    "])), (_l()(), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵted"](-1, null, ["\n"])), (_l()(), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵted"](-1, null, ["\n"])), (_l()(), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵted"](-1, null, ["\n\n"])), (_l()(), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵeld"](120, 0, null, null, 6, "div", [["class", "contenido-buscador d-flex mb-6 mt-6 content-busqueda-integrada"], ["style", "display: none!important"]], null, null, null, null, null)), (_l()(), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵted"](-1, null, ["\n    "])), (_l()(), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵeld"](122, 0, null, null, 3, "frm-busqueda-integrada", [["class", "w-100"]], null, null, null, _forms_child_components_frm_busqueda_integrada_frm_busqueda_integrada_component_ngfactory__WEBPACK_IMPORTED_MODULE_13__["View_frmBusquedaIntegradaComponent_0"], _forms_child_components_frm_busqueda_integrada_frm_busqueda_integrada_component_ngfactory__WEBPACK_IMPORTED_MODULE_13__["RenderType_frmBusquedaIntegradaComponent"])), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵprd"](512, null, _services_navegador_service__WEBPACK_IMPORTED_MODULE_14__["NavegadorService"], _services_navegador_service__WEBPACK_IMPORTED_MODULE_14__["NavegadorService"], []), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵprd"](512, null, _forms_services_formCookies_service__WEBPACK_IMPORTED_MODULE_15__["formCookiesService"], _forms_services_formCookies_service__WEBPACK_IMPORTED_MODULE_15__["formCookiesService"], [ngx_cookie_service_cookie_service_cookie_service__WEBPACK_IMPORTED_MODULE_16__["CookieService"]]), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵdid"](125, 114688, null, 0, _forms_child_components_frm_busqueda_integrada_frm_busqueda_integrada_component__WEBPACK_IMPORTED_MODULE_17__["frmBusquedaIntegradaComponent"], [_forms_services_reglasInputs_service__WEBPACK_IMPORTED_MODULE_6__["reglasInputsService"], _services_navegador_service__WEBPACK_IMPORTED_MODULE_14__["NavegadorService"], _forms_services_formCookies_service__WEBPACK_IMPORTED_MODULE_15__["formCookiesService"]], null, null), (_l()(), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵted"](-1, null, ["\n"])), (_l()(), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵted"](-1, null, ["\n"]))], function (_ck, _v) { var _co = _v.component; var currVal_1 = _co.formularioLogin; var currVal_2 = _co.primerPaso; _ck(_v, 64, 0, currVal_1, currVal_2); var currVal_3 = _co.formularioLogin; var currVal_4 = _co.primerPaso; _ck(_v, 72, 0, currVal_3, currVal_4); var currVal_6 = _co.formularioLogin; var currVal_7 = false; var currVal_8 = _co.primerPaso; _ck(_v, 82, 0, currVal_6, currVal_7, currVal_8); var currVal_9 = _co.formularioLogin; var currVal_10 = _co.primerPaso; _ck(_v, 88, 0, currVal_9, currVal_10); _ck(_v, 125, 0); }, function (_ck, _v) { var _co = _v.component; var currVal_0 = _co.titulo; _ck(_v, 51, 0, currVal_0); var currVal_5 = _co.dominio_Correo; _ck(_v, 76, 0, currVal_5); var currVal_11 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵinlineInterpolate"](1, "", _co.btn_ingresar, ""); _ck(_v, 102, 0, currVal_11); });
}
function View_FormularioLoginComponent_Host_0(_l) { return _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵvid"](0, [(_l()(), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵeld"](0, 0, null, null, 4, "app-formulario-login", [], null, null, null, View_FormularioLoginComponent_0, RenderType_FormularioLoginComponent)), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵprd"](512, null, _forms_services_validaciones_service__WEBPACK_IMPORTED_MODULE_18__["userValidations"], _forms_services_validaciones_service__WEBPACK_IMPORTED_MODULE_18__["userValidations"], [_services_getJson_service__WEBPACK_IMPORTED_MODULE_19__["getJson"], _angular_platform_browser__WEBPACK_IMPORTED_MODULE_20__["DOCUMENT"]]), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵprd"](512, null, _forms_services_formCookies_service__WEBPACK_IMPORTED_MODULE_15__["formCookiesService"], _forms_services_formCookies_service__WEBPACK_IMPORTED_MODULE_15__["formCookiesService"], [ngx_cookie_service_cookie_service_cookie_service__WEBPACK_IMPORTED_MODULE_16__["CookieService"]]), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵprd"](512, null, _forms_services_send_service__WEBPACK_IMPORTED_MODULE_21__["sendService"], _forms_services_send_service__WEBPACK_IMPORTED_MODULE_21__["sendService"], [_forms_services_formCookies_service__WEBPACK_IMPORTED_MODULE_15__["formCookiesService"], _forms_services_service_pipe__WEBPACK_IMPORTED_MODULE_22__["CapitalizePipe"], _forms_services_validaciones_service__WEBPACK_IMPORTED_MODULE_18__["userValidations"], _services_readJson_service__WEBPACK_IMPORTED_MODULE_23__["readJson"], _forms_services_formCookies_service__WEBPACK_IMPORTED_MODULE_15__["formCookiesService"], _services_generic_service__WEBPACK_IMPORTED_MODULE_24__["GenericService"]]), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵdid"](4, 114688, null, 0, _formulario_login_component__WEBPACK_IMPORTED_MODULE_25__["FormularioLoginComponent"], [_angular_forms__WEBPACK_IMPORTED_MODULE_26__["FormBuilder"], _forms_services_validaciones_service__WEBPACK_IMPORTED_MODULE_18__["userValidations"], _forms_services_send_service__WEBPACK_IMPORTED_MODULE_21__["sendService"], _services_getJson_service__WEBPACK_IMPORTED_MODULE_19__["getJson"], _forms_services_formCookies_service__WEBPACK_IMPORTED_MODULE_15__["formCookiesService"]], null, null)], function (_ck, _v) { _ck(_v, 4, 0); }, null); }
var FormularioLoginComponentNgFactory = /*@__PURE__*/ /*@__PURE__*/ _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵccf"]("app-formulario-login", _formulario_login_component__WEBPACK_IMPORTED_MODULE_25__["FormularioLoginComponent"], View_FormularioLoginComponent_Host_0, {}, {}, []);



/***/ }),

/***/ "./src/app/formularios/forms_parent_components/formulario-login/formulario-login.component.scss.shim.ngstyle.js":
/*!**********************************************************************************************************************!*\
  !*** ./src/app/formularios/forms_parent_components/formulario-login/formulario-login.component.scss.shim.ngstyle.js ***!
  \**********************************************************************************************************************/
/*! exports provided: styles */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "styles", function() { return styles; });
/**
 * @fileoverview This file was generated by the Angular template compiler. Do not edit.
 *
 * @suppress {suspiciousCode,uselessCode,missingProperties,missingOverride,checkTypes}
 * tslint:disable
 */
/** PURE_IMPORTS_START  PURE_IMPORTS_END */
/** PURE_IMPORTS_START  PURE_IMPORTS_END */
var styles = [".btn[_ngcontent-%COMP%] {\n  box-shadow: none; }\n\n.content-close[_ngcontent-%COMP%] {\n  background: #000;\n  height: 24px;\n  width: 100%; }\n\n.modal-login-biblioteca[_ngcontent-%COMP%] {\n  overflow-y: hidden !important;\n  padding-right: 0px !important; }\n\n.adapt-row-biblioteca[_ngcontent-%COMP%] {\n  width: 100%; }\n\n.modal-fluid[_ngcontent-%COMP%] {\n  position: relative;\n  margin: 0 auto; }\n\n.titulo-formulario[_ngcontent-%COMP%] {\n  color: #006fba; }\n\n.div-btn-ingresar[_ngcontent-%COMP%] {\n  width: 100%; }\n\n.btn_ingresar[_ngcontent-%COMP%] {\n  background: #f68b1f;\n  width: 100%;\n  margin: 0;\n  cursor: pointer; }\n\n.modal-footer[_ngcontent-%COMP%], .modal-header[_ngcontent-%COMP%] {\n  border: none;\n  width: 100%; }\n\n.modal-footer[_ngcontent-%COMP%] {\n  margin-top: -35px; }\n\n.modal-header[_ngcontent-%COMP%] {\n  padding: 0;\n  overflow: hidden; }\n\n.modal-header[_ngcontent-%COMP%]   img[_ngcontent-%COMP%] {\n    width: 100%; }\n\n.btn-close[_ngcontent-%COMP%] {\n  color: #ffffff; }\n\n.dominio[_ngcontent-%COMP%] {\n  font-size: .9rem;\n  padding-top: 1.75rem; }\n\n.titulo-modal[_ngcontent-%COMP%] {\n  font-weight: 400;\n  padding-top: .1rem;\n  font-size: 1.875rem; }\n\n.contenido-modal[_ngcontent-%COMP%] {\n  width: 100%;\n  margin: auto;\n  max-width: 540px; }\n\n.contenido-modal[_ngcontent-%COMP%]   .modal-content[_ngcontent-%COMP%] {\n    height: 100%;\n    border-radius: 5px; }\n\n.aviso-privacidad[_ngcontent-%COMP%] {\n  padding-top: 0.875rem;\n  margin-bottom: 0; }\n\n.icono-biblioteca[_ngcontent-%COMP%] {\n  font-size: 4rem; }\n\n#preload-library[_ngcontent-%COMP%] {\n  padding-bottom: 1rem; }\n\n#preload-library[_ngcontent-%COMP%]   i[_ngcontent-%COMP%] {\n    font-size: 3rem; }\n\n.content-logo[_ngcontent-%COMP%] {\n  width: 100%;\n  color: #757575; }\n\n.cuenta[_ngcontent-%COMP%] {\n  margin-bottom: 0;\n  margin-top: -.625rem; }"];



/***/ }),

/***/ "./src/app/formularios/forms_parent_components/formulario-login/formulario-login.component.ts":
/*!****************************************************************************************************!*\
  !*** ./src/app/formularios/forms_parent_components/formulario-login/formulario-login.component.ts ***!
  \****************************************************************************************************/
/*! exports provided: FormularioLoginComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "FormularioLoginComponent", function() { return FormularioLoginComponent; });
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/esm5/forms.js");
/* harmony import */ var _forms_services_validaciones_service__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../../forms_services/validaciones.service */ "./src/app/formularios/forms_services/validaciones.service.ts");
/* harmony import */ var _services_getJson_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../services/getJson.service */ "./src/app/services/getJson.service.ts");
/* harmony import */ var _forms_services_send_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../forms_services/send.service */ "./src/app/formularios/forms_services/send.service.ts");
/* harmony import */ var _forms_services_formCookies_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../../forms_services/formCookies.service */ "./src/app/formularios/forms_services/formCookies.service.ts");


// import { Animaciones} from '../../../utilities/animaciones';


//Servicio de cookie

var safeUrl = (/unitec.mx/.test(window.location.href)) ? "https://www.unitec.mx" : "//" + document.domain;
var FormularioLoginComponent = /** @class */ (function () {
    //Variable para emular GET
    // $_GET: any;
    function FormularioLoginComponent(formBuilder, userValidations, sendService, getJsonService, 
        // private animaciones : Animaciones,
        formCookiesService) {
        this.formBuilder = formBuilder;
        this.userValidations = userValidations;
        this.sendService = sendService;
        this.getJsonService = getJsonService;
        this.formCookiesService = formCookiesService;
        //Variables de inicializacion
        this.titulo = 'Ingresa a Biblioteca Virtual';
        this.dominio_Correo = "@my.unitec.edu.mx";
        this.alumno = "Soy Alumno";
        this.profesor = "Soy Profesor";
        //Id de los botones del formulario
        this.btn_ingresar = "btnIngresar";
        this.btn_cerrar = "btnCerrarSession";
        this.btn_login = "btnLogin";
        this.btn_alumno = "alumno";
        this.btn_profesor = "profesor";
        this.c_nombre = "";
        this.c_perfil = "";
        this.c_matricula = "";
        //Errores del flujo de la aplicacion
        this.error_usuario = "Por favor ingrese su correo electronico";
        this.error_password = "Por favor ingrese su contraseña";
        this.primerPaso = 'faseUno';
        var that = this;
        //Invocamos el metodo getJsonCarreras para obtener las carreras y ponerlas en localstorage
        // this.getJsonService.getJsonCarreras();
        //Inicializacion del formulario
        this.formularioLogin = formBuilder.group({
            faseUno: formBuilder.group({
                //Inicializacion de campos con datos reales para realizar pruebas
                frm_radio_perfil_AlumnoMaestro: ['', _angular_forms__WEBPACK_IMPORTED_MODULE_0__["Validators"].compose([
                        userValidations.validaRadioPerfilBV
                    ])
                ],
                frm_mail_login: ['', _angular_forms__WEBPACK_IMPORTED_MODULE_0__["Validators"].compose([
                        userValidations.validaCampoVacio
                    ])
                ],
                frm_ncuenta: ['', _angular_forms__WEBPACK_IMPORTED_MODULE_0__["Validators"].compose([
                        userValidations.validaNumeroDeCuenta
                    ])
                ],
                frm_aviso_de_privacidad: ['', userValidations.validaAviso]
            })
        });
        // ximena.badillo
        // 15214288                
        //Al cargar el DOM
        jQuery(document).ready(function () {
            var y;
            var x;
            //Deshabilidar el scroll al abrir el modal
            jQuery("#modal_frm_biblioteca").on("shown.bs.modal", function (e) {
                //e.preventDefault();
                //var x=window.scrollX;
                //var y=window.scrollY;
                //window.onscroll=function(){window.scrollTo(x, y);};
                console.log(window.scrollY);
                x = window.scrollX;
                y = window.scrollY;
                $('body').css('overflow', 'hidden');
                //jQuery("body").css({ "overflow-y" : "hidden" });
                jQuery("html").css({ "position": "fixed" });
                jQuery("body").css({ "position": "fixed" });
                //window.onscroll=function(){window.scrollTo(x, y);};
            });
            jQuery("#modal_frm_biblioteca").on("hidden.bs.modal", function (e) {
                console.log("Aqui y: " + y);
                //window.onscroll=function(){};
                //jQuery("html").css({ "overflow-y" : "" });
                jQuery("body").css({ "overflow": "" });
                jQuery("html").css({ "position": "" });
                jQuery("body").css({ "position": "" });
                window.scrollTo(x, y);
            });
            //Comprobación de Cookies Activas
            // console.log( "Estas son las cookies activas: " + document.cookie );
            if (formCookiesService.checkCookie("c_matricula") == true) {
                console.log("La cookie existe Contenido Unlocked");
                jQuery("#" + that.btn_cerrar).removeClass("d-none");
                jQuery(".content-busqueda-integrada").attr('style', '');
                jQuery("#login-biblioteca").addClass("d-none");
                jQuery("#logout-biblioteca").removeClass("d-none");
                //Obtenemos las variables de LocalStorage para inyectar URLs
                /*
                1.-Primo
                2.-Cengage
                3.-My unitec
                */
                $("#btn-carousel-0").attr({ href: localStorage.getItem("biblioteca_kivuto"), target: '_blank' });
                jQuery("#biblioteca_libri_1").attr({ href: localStorage.getItem("biblioteca_kivuto"), target: '_blank' });
                //$( "h4:containsIN('cengage')" ).parent().parent().find(".biblioteca-links").attr({href: localStorage.getItem("biblioteca_cengage"), target: '_blank'});
                //$( "h4:containsIN('cengage')" ).parent().parent().find(".biblioteca-links").attr({href: localStorage.getItem("biblioteca_kivuto"), target: '_blank'});
                //$( "h4:containsIN('pearson')" ).parent().parent().find(".biblioteca-links").attr({href: localStorage.getItem("biblioteca_pearson"), target: '_blank'});
                //$( "h4:containsIN('mcgraw')" ).parent().parent().find(".biblioteca-links").attr({href: localStorage.getItem("biblioteca_ebrary"), target: '_blank'});
                jQuery("#logout-biblioteca").click(function () {
                    formCookiesService.removeCookie('c_nombre');
                    formCookiesService.removeCookie('c_perfil');
                    formCookiesService.removeCookie('c_matricula');
                    jQuery("#login-biblioteca").removeClass("d-none");
                    jQuery("#logout-biblioteca").addClass("d-none");
                    $("#btn-carousel-0").attr({ href: '#', target: '' });
                    jQuery("#biblioteca_libri_1").attr({ href: '#', target: '' });
                    //$( "h4:containsIN('cengage')" ).parent().parent().find(".biblioteca-links").attr({href: '#', target: ''});
                    //$( "h4:containsIN('pearson')" ).parent().parent().find(".biblioteca-links").attr({href: '#', target: ''});
                    //$( "h4:containsIN('mcgraw')" ).parent().parent().find(".biblioteca-links").attr({href: '#', target: ''});
                    jQuery(".biblioteca-links").click(function () {
                        jQuery("#modal_frm_biblioteca").modal();
                    });
                    document.location.href = "/";
                });
                //$(".content-busqueda-integrada").html( localStorage.getItem("biblioteca-buscador") );
            }
            else {
                jQuery("#login-biblioteca").removeClass("d-none");
                jQuery("#logout-biblioteca").addClass("d-none");
                jQuery("#" + that.btn_login).removeClass("d-none");
                console.log("La cookie no existe Contenido Locked");
                jQuery(".biblioteca-links").click(function () {
                    jQuery("#modal_frm_biblioteca").modal();
                });
                jQuery("#btn-close").click(function () {
                    $("#modal_frm_biblioteca").modal('hide');
                });
            }
        });
    }
    FormularioLoginComponent.prototype.ngOnInit = function () { };
    FormularioLoginComponent.prototype.enviar = function () {
        //Si el formulario es invalido entra aqui.
        if (this.formularioLogin.controls.faseUno.valid == false) {
            var myControl = this.formularioLogin.controls.faseUno["controls"];
            for (var control in myControl) {
                myControl[control].markAsDirty();
            }
            console.log("Este formulario es invalido...");
            return false;
        }
        else {
            var that_1 = this;
            that_1.email = this.formularioLogin.get("faseUno.frm_mail_login").value;
            jQuery.ajax({
                url: safeUrl + "/wp-content/themes/temaunitec/assets/html/ServidorPHP/main.php",
                method: "POST",
                data: {
                    route: "BibliotecaVirtual",
                    seccion: "login",
                    origen: "pearson",
                    correo: that_1.email + that_1.dominio_Correo,
                    perfil: this.formularioLogin.get("faseUno.frm_radio_perfil_AlumnoMaestro").value,
                    cuenta: this.formularioLogin.get("faseUno.frm_ncuenta").value,
                    usuario: that_1.email
                },
                dataType: "json",
                beforeSend: function () {
                    jQuery('#preload-library').html("<img src='" + safeUrl + "/assets/loading.svg' class='img-fluid m-auto' style='width:100%; max-width:45px;'><br /><div style='font-size:.8rem;'>Cargando.. Por favor espere</div>");
                },
                success: function (data) {
                    console.log(data["respuesta"]);
                    if (data["respuesta"] == 1) {
                        //console.log(document.cookie);
                        //console.log( data );
                        localStorage.setItem("biblioteca_libri", data["biblioteca_libri"]);
                        //localStorage.setItem( "biblioteca_pearson", data["biblioteca_pearson"] );
                        //localStorage.setItem( "biblioteca_cengage", data["biblioteca_cengage"] );
                        localStorage.setItem("biblioteca_ebrary", data["biblioteca_ebrary"]);
                        localStorage.setItem("biblioteca_kivuto", data["biblioteca_kivuto"]);
                        //localStorage.setItem( "biblioteca-buscador",  "'" + data["biblioteca_buscador"] + "'" );
                        //Obtenemos los valores del formulario del usuario para dar de alta las cookies
                        //Se dan de alta las Cookies del sitio
                        console.log("c_nombre: " + data["nombre"] + " c_perfil: " + data["perfil"] + " c_matricula: " + data["cuenta"]);
                        that_1.c_nombre = data["nombre"];
                        that_1.c_perfil = data["perfil"];
                        that_1.c_matricula = data["cuenta"];
                        that_1.formCookiesService.appendCookieValue('c_nombre', 'c_nombre', that_1.c_nombre);
                        that_1.formCookiesService.appendCookieValue('c_perfil', 'c_perfil', that_1.c_perfil);
                        that_1.formCookiesService.appendCookieValue('c_matricula', 'c_matricula', that_1.c_matricula);
                        jQuery("#btn-carousel-0").attr({ href: data["biblioteca_kivuto"], target: '_blank' });
                        jQuery("#biblioteca_libri_1").attr({ href: data["biblioteca_kivuto"], target: '_blank' });
                        // $( "h4:containsIN('cengage')" ).parent().parent().find(".biblioteca-links").attr({href: data["biblioteca_cengage"], target: '_blank'});
                        //$( "h4:containsIN('cengage')" ).parent().parent().find(".biblioteca-links").attr({href: data["biblioteca_kivuto"], target: '_blank'});
                        //$( "h4:containsIN('pearson')" ).parent().parent().find(".biblioteca-links").attr({href: data["biblioteca_pearson"], target: '_blank'});
                        //$( "h4:containsIN('mcgraw')" ).parent().parent().find(".biblioteca-links").attr({href: data["biblioteca_ebrary"], target: '_blank'});
                        //Redireccion para Carga de Contenido Bloqueado
                        location.reload();
                    }
                    else {
                        //console.log("Usuario no Valido");
                        jQuery('#preload-library').html("<div style='font-size:.8rem;'>Usuario no Valido, favor de verificar sus datos</div>");
                    }
                },
                error: function (err) {
                    console.log(err);
                }
            });
        }
    };
    FormularioLoginComponent.prototype.cerrarSession = function () {
        //Se dan de baja las cookies del usuario de la biblioteca virtual
        this.formCookiesService.removeCookie('c_matricula');
        this.formCookiesService.removeCookie('c_nombre');
        this.formCookiesService.removeCookie('c_perfil');
        //Se eliminan variables de Local Storage
        localStorage.removeItem("biblioteca_libri");
        //localStorage.removeItem("biblioteca_pearson");
        //localStorage.removeItem("biblioteca_cengage");
        localStorage.removeItem("biblioteca_ebrary");
        localStorage.removeItem("biblioteca_kivuto");
        //Se remueven la url asignadas
        //jQuery("a#libri").removeAttr("href");
        //jQuery("a#ceng").removeAttr("href");
        //jQuery("a#mcg").removeAttr("href");
        //jQuery("a#pearson").removeAttr("href");
        jQuery("#btn-carousel-0").removeAttr("href");
        jQuery("#biblioteca_libri_0").removeAttr("href");
        jQuery("#biblioteca_libri_1").removeAttr("href");
        jQuery("#biblioteca_libri_2").removeAttr("href");
        this.titulo = "Ingresar.";
        //Redireccion a la página de Home
        location.reload();
    };
    return FormularioLoginComponent;
}());



/***/ }),

/***/ "./src/app/formularios/forms_services/formCookies.service.ts":
/*!*******************************************************************!*\
  !*** ./src/app/formularios/forms_services/formCookies.service.ts ***!
  \*******************************************************************/
/*! exports provided: formCookiesService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "formCookiesService", function() { return formCookiesService; });
/* harmony import */ var ngx_cookie_service__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ngx-cookie-service */ "./node_modules/ngx-cookie-service/index.js");

var formCookiesService = /** @class */ /*@__PURE__*/ (function () {
    function formCookiesService(cookieService) {
        this.cookieService = cookieService;
        //1 día
        this.duracion_form_cookie = new Date(new Date().getTime() + 1 * 1000 * 60 * 60 * 24);
        this.path_form_cookie = '/';
        this.cookies = cookieService;
    }
    //Recibe el nombre de la cookie, clave y valor y añade el valor de la cookie y si ya existe lo reemplaza
    formCookiesService.prototype.appendCookieValue = function (nombre_cookie, clave, valor, timeExpiration) {
        //console.log("Entro al metodo cookie");
        var objCookie;
        //this.cookies.setObject(nombre_cookie, {bar: 'asasr'}); 
        //Obtener la cookie para ver si es null o no tiene valores
        //console.log(this.cookies.getObject(nombre_cookie));
        objCookie = this.cookies.get(nombre_cookie);
        if (objCookie == "" || objCookie == "null" || typeof objCookie == "undefined" || objCookie == null) {
            //Si no existe la cookie se crea con el valor correspondiente y la configuracion global
            // var jsontext = '{"' + clave + '":"' + valor + '", "expDate":"'+this.duracion_form_cookie+'"}';
            //var contact = JSON.parse(jsontext);
            // var contact = jsontext;
            if (timeExpiration) {
                // 2 mins = 0.0138889
                this.duracion_form_cookie = new Date(new Date().getTime() + timeExpiration * 1000 * 60 * 60);
                var jsontext = '{"' + clave + '":"' + valor + '", "expDate":"' + this.duracion_form_cookie + '"}';
                //var contact = JSON.parse(jsontext);
                var contact = jsontext;
                this.cookies.set(nombre_cookie, contact, this.duracion_form_cookie, this.path_form_cookie);
            }
            else {
                this.duracion_form_cookie = new Date(new Date().getTime() + 1 * 1000 * 60 * 60 * 24);
                var jsontext = '{"' + clave + '":"' + valor + '", "expDate":"' + this.duracion_form_cookie + '"}';
                //var contact = JSON.parse(jsontext);
                var contact = jsontext;
                this.cookies.set(nombre_cookie, contact, this.duracion_form_cookie, this.path_form_cookie);
            }
            //console.log("después de setear");
            //console.log(JSON.parse(this.cookies.get(nombre_cookie)));
            //console.log(this.cookies.getObject(nombre_cookie));
        }
        else {
            //console.log("Entro a la validacion por que ya existe");
            //Si ya existe la cookie o tiene valores se debe agregar un valor mas
            var cookie_value = this.cookies.get(nombre_cookie);
            var expDate = JSON.parse(cookie_value)['expDate'];
            // console.log("Obtener la cookie cuando ya exite");
            // console.log(cookie_value);
            var cadena_cookie = "";
            var arrLength = Object.keys(JSON.parse(cookie_value)).length;
            //console.log("Tamaño de la cookie: " + arrLength);
            var valida_reemplazo = false;
            var tmpLen = 0;
            jQuery.each(JSON.parse(cookie_value), function (index, value) {
                tmpLen++;
                //Armar la cadena para la nueva cookie
                //Se valida si la clave de la cookie ya existe para substituir el valor
                if (index != "" || index != "null" || typeof index != "undefined" || index != null) {
                    //Validamos si la clave del valor de la cookie es igual al valor pasado como
                    //parametro al metodo y lo reemplazamos
                    if (tmpLen != arrLength) {
                        //Si es el mismo valor para la cookie se reemplaza
                        if (index == clave) {
                            value = valor;
                            valida_reemplazo = true;
                        }
                        cadena_cookie += '"' + index + '":"' + value + '",';
                    }
                    else {
                        //Si es el mismo valor para la cookie se reemplaza
                        if (index == clave) {
                            value = valor;
                            valida_reemplazo = true;
                        }
                        //Si es el ultimo elemento y ademas no fue reemplazado el valor
                        //concatenamos el nuevo valor
                        if (valida_reemplazo == true) {
                            cadena_cookie += '"' + index + '":"' + value + '"';
                        }
                        else {
                            cadena_cookie += '"' + index + '":"' + value + '",';
                            cadena_cookie += '"' + clave + '":"' + valor + '"';
                        }
                    }
                }
            });
            var contactb = "{" + cadena_cookie + "}";
            cadena_cookie = "";
            if (timeExpiration) {
                // 2 mins = 0.0138889
                this.duracion_form_cookie = new Date(new Date().getTime() + timeExpiration * 1000 * 60 * 60);
                this.cookies.set(nombre_cookie, contactb, new Date(expDate), this.path_form_cookie);
            }
            else {
                this.duracion_form_cookie = new Date(new Date().getTime() + 1 * 1000 * 60 * 60 * 24);
                this.cookies.set(nombre_cookie, contactb, new Date(expDate), this.path_form_cookie);
            }
        }
        //console.log("la cookie al final del metodo");
        //console.log(JSON.parse(contactb));
    };
    //Obtiene el valor de la cookie
    formCookiesService.prototype.getCookieValues = function (nombre_cookie) {
        return JSON.parse(this.cookies.get(nombre_cookie));
    };
    //Se realiza una modificacion debido a que el metodo delete no elimina la cookies al finalizar 
    //la sesión, la manera para eliminarlas es manualmente mediamte javascript asignado una fecha pasada
    // **Update Done By SRP day: 12-12-2017 **
    //Eliminar la cookie
    formCookiesService.prototype.removeCookie = function (nombre_cookie) {
        // console.log("La cookie es:"+ nombre_cookie);
        // let cookieActiva = this.cookies.delete(nombre_cookie);
        // console.log("La cookie despues del metodo delete: " + cookieActiva);
        var cookieActivaAlt = document.cookie = nombre_cookie + "=;expires=Thu, 01 Jan 1970 00:00:00 GMT;path=/";
        console.log("La cookie ahora con javascript: " + cookieActivaAlt);
    };
    //Obtener el valor de una cookie por clave
    formCookiesService.prototype.getCookieByKey = function (nombre_cookie, key) {
        var objCookie;
        var response = false;
        objCookie = this.cookies.get(nombre_cookie);
        if (objCookie == "" || objCookie == "null" || typeof objCookie == "undefined" || objCookie == null) {
            //No existe la cookie retornamos false
            response = false;
        }
        else {
            //Si existe la cookie validamos si existe la clave y retornamos el valor
            var cookie_value = this.cookies.get(nombre_cookie);
            var cadena_cookie = "";
            var arrLength = Object.keys(JSON.parse(cookie_value)).length;
            var valida_reemplazo = false;
            var tmpLen = 0;
            jQuery.each(JSON.parse(cookie_value), function (index, value) {
                tmpLen++;
                if (index != "" || index != "null" || typeof index != "undefined" || index != null) {
                    //Si es el mismo valor para la cookie se reemplaza
                    if (index == key) {
                        response = value;
                        return;
                    }
                }
            });
        } //Termina else de la validacion de cookie
        return response;
    }; //Termina el metodo getcookieByKey
    //Obtener cookie simple
    formCookiesService.prototype.getCookie = function (c_name) {
        var c_value = document.cookie;
        var c_start = c_value.indexOf(" " + c_name + "=");
        if (c_start == -1) {
            c_start = c_value.indexOf(c_name + "=");
        }
        if (c_start == -1) {
            c_value = null;
        }
        else {
            c_start = c_value.indexOf("=", c_start) + 1;
            var c_end = c_value.indexOf(";", c_start);
            if (c_end == -1) {
                c_end = c_value.length;
            }
            c_value = unescape(c_value.substring(c_start, c_end));
        }
        return c_value;
    };
    //Validar si una cookie existe
    formCookiesService.prototype.checkCookie = function (nombre_cookie) {
        var objCookie;
        objCookie = this.cookies.get(nombre_cookie);
        if (objCookie == "" || objCookie == "null" || typeof objCookie == "undefined" || objCookie == null) {
            return false;
        }
        else {
            return true;
        }
    };
    //Obtener el valor de una cookie dentro de una URL
    formCookiesService.prototype.getUrlParameterByName = function (name) {
        name = name.replace(/[\[]/, "\\[").replace(/[\]]/, "\\]");
        var regex = new RegExp("[\\?&]" + name + "=([^&#]*)"), results = regex.exec(location.search);
        return results === null ? "" : decodeURIComponent(results[1].replace(/\+/g, " "));
    };
    return formCookiesService;
}()); //Termina el servicio



/***/ }),

/***/ "./src/app/formularios/forms_services/reglasInputs.service.ts":
/*!********************************************************************!*\
  !*** ./src/app/formularios/forms_services/reglasInputs.service.ts ***!
  \********************************************************************/
/*! exports provided: reglasInputsService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "reglasInputsService", function() { return reglasInputsService; });
var reglasInputsService = /** @class */ /*@__PURE__*/ (function () {
    function reglasInputsService() {
    }
    reglasInputsService.prototype.numVecestexto = function (codigo, campoid, key) {
        if (codigo === void 0) {
            codigo = "";
        }
        if (campoid === void 0) {
            campoid = "";
        }
        if (key === void 0) {
            key = "";
        }
        var arrayValorInput = jQuery('#' + campoid).val().split("");
        var tamArrayValorInput = jQuery('#' + campoid).val().length;
        var flagRepetido = false;
        if (tamArrayValorInput > 0) {
            //console.log(tamArrayValorInput);
            for (var i = 0; i < arrayValorInput.length; i++) {
                //console.log("Valor: " + arrayValorInput[i]);
                if ((arrayValorInput[i] == arrayValorInput[i + 1]) &&
                    (arrayValorInput[i + 1] == key)) {
                    return false;
                }
            }
        }
        if (jQuery('#' + campoid).val().length == 0) {
            var keyAnt = codigo;
            var vecesText = 0;
            return true;
        }
        else if (keyAnt == codigo) {
            if (vecesText >= 1) {
                return false;
            }
            else if (vecesText != 1) {
                keyAnt = codigo;
                vecesText = vecesText + 1;
                return true;
            }
        }
        else {
            if (keyAnt != codigo) {
                keyAnt = codigo;
                vecesText = 0;
                return true;
            }
        }
    };
    reglasInputsService.prototype.EvaluateTextP = function (obj, keycode, key) {
        if (key === void 0) {
            key = "";
        }
        var validoT = false;
        var opc = true;
        var tecla = keycode;
        validoT = this.numVecestexto(tecla, obj.id, key);
        if (tecla == 8) {
            return true;
        }
        else if (tecla == 13 && obj.value.length > 0) {
            var espacio = true;
            return;
        }
        else if (validoT == true) {
            if (tecla == 32 && espacio == true) {
                espacio = false;
                return true;
            }
            else if (espacio = false && tecla == 32) {
                return false;
            }
            if ((tecla != 32) && !((tecla > 8 && tecla < 33) || (tecla < 65 && tecla > 31) || (tecla > 90 && tecla < 97) || tecla > 122)) {
                espacio = true;
            }
            // if ((tecla > 8 && tecla < 33) || (tecla < 65 && tecla > 31) || (tecla > 90 && tecla < 97) || tecla > 122 ){
            //     return  false;
            // }
            if ((tecla > 8 && tecla < 32) || (tecla > 32 && tecla < 65) || (tecla > 90 && tecla < 97) || tecla > 122) {
                return false;
            }
            return true;
        }
        else
            return false;
    };
    reglasInputsService.prototype.sinEspacios = function (e) {
        var tecla = e.keyCode;
        if (tecla == 13) {
            e.preventDefault();
        }
        if (tecla == 32) {
            return false;
        }
        if (tecla != 32) {
            return true;
        }
    };
    reglasInputsService.prototype.esNumero = function (keycode) {
        var tecla = keycode;
        if (tecla > 31 && (tecla < 48 || tecla > 57)) {
            return false;
        }
    };
    return reglasInputsService;
}()); //Termina el servicio



/***/ }),

/***/ "./src/app/formularios/forms_services/send.service.ts":
/*!************************************************************!*\
  !*** ./src/app/formularios/forms_services/send.service.ts ***!
  \************************************************************/
/*! exports provided: sendService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "sendService", function() { return sendService; });
/* harmony import */ var _forms_services_formCookies_service__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../forms_services/formCookies.service */ "./src/app/formularios/forms_services/formCookies.service.ts");
/* harmony import */ var _forms_services_validaciones_service__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../forms_services/validaciones.service */ "./src/app/formularios/forms_services/validaciones.service.ts");
/* harmony import */ var _service_pipe__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./service.pipe */ "./src/app/formularios/forms_services/service.pipe.ts");
/* harmony import */ var _services_readJson_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../services/readJson.service */ "./src/app/services/readJson.service.ts");
/* harmony import */ var _services_generic_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../../services/generic.service */ "./src/app/services/generic.service.ts");





// var safeUrl = (/unitec.mx/.test(window.location.href)) ? "https://unitecmx-universidadtecno.netdna-ssl.com" : "//" + document.domain;
var safeUrl = (/unitec.mx/.test(window.location.href)) ? "https://www.unitec.mx" : "//" + document.domain;
var path = "https://" + document.domain + "/";
var testingSub = "Actualizacion";
var sendService = /** @class */ /*@__PURE__*/ (function () {
    function sendService(formCookieService, capitalizePipe, userValidations, readJsonService, formCookiesService, genericService) {
        this.formCookieService = formCookieService;
        this.capitalizePipe = capitalizePipe;
        this.userValidations = userValidations;
        this.readJsonService = readJsonService;
        this.formCookiesService = formCookiesService;
        this.genericService = genericService;
        this.ciclo_default = "";
        this.urlreferrer = window.location;
        this.url_ajax_php = "/wp-content/phpServeApp/backend0.php";
        this.url_ajax_php_traditionalForm = "/wp-content/phpServeApp/backend4.php";
        this.urlDomain = "localhost:8888/unitec2017";
        this.urlMicroRegistroPrueba = "https://www.unitec.mx/procWeb/microregistro.php";
        this.urlMicroRegistroDeploy = "https://www.unitec.mx/desk/procWeb/microregistro.php";
        this.$_GET = this.genericService.getMethod();
    }
    sendService.prototype.sendMicroRegistro = function (evento) {
        if (evento === void 0) {
            evento = null;
        }
        /*Implementacion enviar datos BY SRP 17-01-2018*/
        //Convertir a JSON la Cookie
        // console.log(JSON.stringify(this.formCookieService.getCookieValues("c_form_data")).replace(/\\n/g,' '));
        // let dataForm = JSON.parse(JSON.stringify(this.formCookieService.getCookieValues("c_form_data")));
        var dataForm = this.formCookieService.getCookieValues("c_form_data");
        var talleres = true;
        // console.log("TODO BIEN AQUI");
        // console.log(dataForm);
        // url: "http://www.unitec.mx/desk/procWeb/microregistro.php",
        // url: "//" + document.domain + this.url_ajax_php,
        console.warn(typeof this.formCookieService.getCookieValues("c_form_data"));
        if (talleres) {
            console.warn("viene de talleres");
        }
        jQuery
            .ajax({
            type: "POST",
            url: this.urlMicroRegistroDeploy,
            data: this.formCookieService.getCookieValues("c_form_data"),
            success: function (res) {
                console.log("Datos Enviados");
                console.log(res);
                //Proceso tag manager
                if (typeof dataLayer != 'undefined') {
                    dataLayer.push({ 'event': 'PASO1_COMPLETADO' });
                    dataLayer.push({
                        'url_value': 'formularioInformacion/paso1',
                        'event': 'StepForm' //dato estático
                    });
                }
                //End Proceso tag manager
            },
            error: function (xhr, textStatus, error) {
                console.log(xhr);
                console.log(xhr.statusText);
                console.log(textStatus);
                console.log(error);
            }
        });
        /*End Implementacion enviar datos BY SRP 17-01-2018*/
    };
    return sendService;
}());



/***/ }),

/***/ "./src/app/formularios/forms_services/service.pipe.ts":
/*!************************************************************!*\
  !*** ./src/app/formularios/forms_services/service.pipe.ts ***!
  \************************************************************/
/*! exports provided: CapitalizePipe */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "CapitalizePipe", function() { return CapitalizePipe; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/esm5/core.js");

var CapitalizePipe = /** @class */ /*@__PURE__*/ (function () {
    function CapitalizePipe() {
    }
    CapitalizePipe.prototype.transform = function (value) {
        if (value) {
            return value.charAt(0).toUpperCase() + value.slice(1);
        }
        return value;
    };
    return CapitalizePipe;
}());

// @Pipe({name: 'ciclos'})
// export class CiclosPipe implements PipeTransform {
//     transform(value: any) {
//         var tipoCiclo: any = value;
//         if(value) {
//             switch(value) {
//                 case "18-2":
//                 tipoCiclo = 
//                 break;
//                 case "18-3":
//                 tipoCiclo = 
//                 break;
//                 case "19-1":
//                 tipoCiclo = 
//                 break;
//             }
//         }
//         return value;
//     }
// } 


/***/ }),

/***/ "./src/app/formularios/forms_services/validaciones.service.ts":
/*!********************************************************************!*\
  !*** ./src/app/formularios/forms_services/validaciones.service.ts ***!
  \********************************************************************/
/*! exports provided: userValidations */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "userValidations", function() { return userValidations; });
/* harmony import */ var rxjs_add_operator_toPromise__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! rxjs/add/operator/toPromise */ "./node_modules/rxjs/_esm5/add/operator/toPromise.js");
/* harmony import */ var rxjs_add_operator_toPromise__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(rxjs_add_operator_toPromise__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var rxjs_add_operator_map__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! rxjs/add/operator/map */ "./node_modules/rxjs/_esm5/add/operator/map.js");
/* harmony import */ var rxjs_add_operator_catch__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! rxjs/add/operator/catch */ "./node_modules/rxjs/_esm5/add/operator/catch.js");
/* harmony import */ var _services_getJson_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../services/getJson.service */ "./src/app/services/getJson.service.ts");



//import { readJson } from '../../services/readJson.service';

var userValidations = /** @class */ /*@__PURE__*/ (function () {
    function userValidations(getJson, document) {
        this.getJson = getJson;
        this.document = document;
        //LLamada a los Json
        // getJson.getJsonBasura();
        // getJson.getJsonBasuraEmail();
        // getJson.getJsonTelefonosBasura();
    }
    //Validacion de buscador
    userValidations.prototype.validaBuscador = function (abstractControl) {
        console.log("Validando el buscador");
        /*let jsonReader = new readJson();
        let jsonBasura = jQuery("#data_json_basura").data();
        let patron = /^[a-zA-Záíúéóñ\s]*$/;
        let arrBasuraComplete = [];
        let minValue = abstractControl.value.toLowerCase();
        let excWords = /trial/;//no validar estas palabras en el biscador
       //Esta vacio el nombre
        if(minValue === ""){
            return {
                fieldError : true,
                customErrorMessage: 'No puede estar vacío el campo'
            }
        //Validamos en el json basura la parte uno del correo
        }else if(minValue.length <= 2 || minValue.length > 30){
            return {
                fieldError : true,
                customErrorMessage: 'Ingrese más de dos letras y menos de 30'
            }
        //Validamos que no existen caracteres especiales en el nombre de usuario
        }else if(minValue.search(patron) == -1) {
            return {
                fieldError : true,
                customErrorMessage: 'Solo se aceptan letras'
            }
        //Validamos nombres basura
        }else if(Object.keys(jsonBasura).length != 0) {
            
            for(var i = 0; i < Object.keys(jsonBasura).length ;i++){
                var arrRes = jsonBasura[i];
                arrBasuraComplete.push(arrRes);
                 
            }
            for(i = 0; i < Object.keys(jsonBasura).length ; i++){
                // console.log(arrBasuraComplete[i]);
                if(minValue.search(arrBasuraComplete[i]) !== -1 && arrBasuraComplete[i].search(excWords) == -1){
                    return {
                        fieldError : true,
                        customErrorMessage: 'Búsqueda inválida'
                    }
                }
            }

        }*/
        return null;
    };
    //Validacion de nombres / apellidos
    userValidations.prototype.validaNombres = function (abstractControl) {
        /*console.log("Validando el buscador nom");
        let jsonReader = new readJson();
        let jsonBasura = jQuery("#data_json_basura").data();
        let patron = /^[a-zA-Záíúéóñ\s]*$/;
        let arrBasuraComplete = [];
        let minValue = abstractControl.value.toLowerCase();
       //Esta vacio el nombre
        if(minValue === ""){
            return {
                fieldError : true,
                customErrorMessage: 'No puede estar vacío el campo'
            }
        //Validamos en el json basura la parte uno del correo
        }else if(minValue.length <= 2 || minValue.length > 30){
            return {
                fieldError : true,
                customErrorMessage: 'Ingrese más de dos letras y menos de 30'
            }
        //Validamos que no existen caracteres especiales en el nombre de usuario
        }else if(minValue.search(patron) == -1) {
            return {
                fieldError : true,
                customErrorMessage: 'Solo se aceptan letras'
            }
        //Validamos nombres basura
        }else if(Object.keys(jsonBasura).length != 0) {
            
            for(var i = 0; i < Object.keys(jsonBasura).length ;i++){
                var arrRes = jsonBasura[i];
                arrBasuraComplete.push(arrRes);
                 
            }
            for(i = 0; i < Object.keys(jsonBasura).length ; i++){
                // console.log(arrBasuraComplete[i]);
                if(minValue.search(arrBasuraComplete[i]) !== -1){
                    return {
                        fieldError : true,
                        customErrorMessage: 'Nombre inválido'
                    }
                }
            }

        }
        return null;  */
    };
    //Nombres basura con observable
    userValidations.prototype.nombresBasuraObs = function (abstractControl) {
        /*if(document.readyState == "complete")
        {
            var input_data_json_basura = (<HTMLInputElement>document.getElementById("data_json_basura"));
            if(input_data_json_basura.value) {
                if(typeof document.getElementById("data_json_basura").dataset.json != 'undefined'){
                    let jsonReader = new readJson();
                    let jsonBasura = input_data_json_basura.dataset.json;
                    if(jsonReader.getKeys(JSON.parse(jsonBasura), abstractControl.value).length > 0 ){
                        return { noBasura : true }
                    }else {
                        return null;
                    }
                }
            }
        }
        return null;*/
    };
    /*
        //Contiene todas las validaciones del formulario unitec 2016 adecuadas a Angular
        validaEmail(abstractControl : AbstractControl)
        {
            //Flag que cambiara cuando sea valido
            let emailValid = false;
            let customErrorMessage;
            let arrEmail = abstractControl.value.split('@');
            let lenUser = arrEmail[0].length;
            let jsonReader = new readJson();
            let jsonBasura = jQuery("#data_json_basura_email").data();
            try {
                var arrDominio = arrEmail[1].split('.');
                var arrLargo = arrDominio.length;
            }catch(err) {}
    
            //Esta vacio el correo
            if(arrEmail[0] === ""){
                return {
                    fieldError : true,
                    customErrorMessage: 'Ingrese su correo'
                }
            //Validamos en el json basura la parte uno del correo
            }else if(jQuery("#data_json_basura_email").val() == 1 && jsonReader.getKeys(jsonBasura, arrEmail[0]).length > 0 ) {
                return {
                    fieldError : true,
                    customErrorMessage: 'Correo inválido'
                }
             //Validar arroba
            }else if(arrEmail.length <= 1){
                return {
                    fieldError : true,
                    customErrorMessage: 'Ingrese el correo completo'
                }
            //Validamos en el json basura la parte 2 del correo
            }else if(jQuery("#data_json_basura_email").val() == 1 && jsonReader.getKeys(jsonBasura, arrEmail[1]).length > 0) {
                
                return {
                    fieldError : true,
                    customErrorMessage: 'Correo inválido'
                }
            //Longitud del nombre de usuario es muy largo o muy corto el nombre
            }else if(lenUser <= 2 || lenUser > 100){
                return {
                    fieldError : true,
                    customErrorMessage: 'Longitud de correo invalida'
                }
            //Validamos que no existen caracteres especiales en el nombre de usuario
            }else if(arrEmail[0].search(/[^a-zA-Z0-9_.-]/)!=-1){
                return {
                    fieldError : true,
                    customErrorMessage: 'Caracter inválido encontrado'
                }
            //Validamos que no existen caracteres especiales en el dominio
            }else if(arrEmail[1].search(/[^a-zA-Z0-9_.-]/)!=-1){
                return {
                    fieldError : true,
                    customErrorMessage: 'Caracter inválido encontrado'
                }
             //Validacion de dominios MS
            }else if(arrEmail[1].search(/(hotmail|outlook|live)/i)!= -1 && (arrEmail[0][lenUser-1].search(/\./)!=-1 || arrEmail[0][0].search(/(-|_|\.)/)!=-1 || arrEmail[0].search(/\.{2}/g) != -1) ){
                return {
                    fieldError : true,
                    customErrorMessage: 'Correo inválido'
                }
            //Validacion de dominios gmail
            }else if(arrEmail[1].search(/gmail/i)!= -1 && (arrEmail[0][lenUser-1].search(/[^a-zA-Z0-9]/)!=-1 || arrEmail[0][0].search(/(-|_|\.)/)!=-1 || arrEmail[0].search(/\.{2}/g) != -1 || arrEmail[0].search(/(-|_)/g) != -1)){
                return {
                    fieldError : true,
                    customErrorMessage: 'Correo inválido'
                }
             //Validacion de dominios yahoo
            }else if(arrEmail[1].search(/yahoo/i)!= -1 && (arrEmail[0][lenUser-1].search(/[^a-zA-Z0-9]/)!=-1 || arrEmail[0][0].search(/(_|\.)/)!=-1 || arrEmail[0].search(/(\.|_){2}/g) != -1 || arrEmail[0].search(/-/g)!=-1 ) ){
                return {
                    fieldError : true,
                    customErrorMessage: 'Correo inválido'
                }
            //Validacion de dominios restantes
            }else if(arrEmail[1].search(/(^hotmail|outlook|gmail|live|yahoo)/i) == -1 && ( arrEmail[0][lenUser-1].search(/(-|_|\.)/)!=-1 || arrEmail[0][0].search(/(-|_|\.|[0-9])/)!=-1 || arrEmail[0].search(/(-|_|\.){2}/g) != -1)){
                return {
                    fieldError : true,
                    customErrorMessage: 'Correo inválido'
                }
             //Validacion de dominios por ejemplo debe tener @mail.com por ejemplo
            }else if (arrLargo < 2){
                return {
                    fieldError : true,
                    customErrorMessage: 'Ingrese el correo completo'
                }
            } else {
                var arrDominio = arrEmail[1].split('.');
                var arrLargo = arrDominio.length;
    
                // Hay emails que tienen dominio  y subdominio, por ejemplo fcastill@mail.unitec.mx,
                // entonces verificamos desde el primer elemento al penultimo, ya que el ultimo el es
                // el nombre de dominio superior y requerimos otras validaciones
                for (var x = 0; x <= arrLargo-2; x++) {
                    //esta vacio el nombre del dominio o subdominio?
                    if (arrDominio[x]===""){
                        return {
                            errorEmail : true,
                            customErrorMessage: 'Correo inválido'
                        }
                    //es muy largo o muy corto el nombre del dominio?
                    }else if(arrDominio[x].length > 36 || arrDominio[x].length < 2){
                        return {
                            errorEmail : true,
                            customErrorMessage: 'Correo inválido'
                        }
                    }
                    //Validación de correo basura en el dominio
                    if(Object.keys(jsonBasura).length != 0) {
                        for(var i = 0; i < Object.keys(jsonBasura).length; i++){
                            if(arrDominio[x].indexOf(jsonBasura[i]) != -1){
                                
                                return {
                                    fieldError : true,
                                    customErrorMessage: 'Correo inválido'
                                }
                            }
                        }
                    }
    
                }
                // Validamos el nombre del dominio superior
                // esta vacio
                if(arrDominio[arrLargo-1]===""){
                    return {
                        fieldError : true,
                        customErrorMessage: 'Correo inválido'
                    }
                }else if(arrDominio[arrLargo-1].length >= 4 || arrDominio[arrLargo-1].length <= 1){
                    return {
                        fieldError : true,
                        customErrorMessage: 'Correo inválido'
                    }
                }
            }//Else de validaciones principales
    
            return null;
        }//Termina validacionesEmail
        //Validación para el campo estado
        validaEstado(abstractControl : AbstractControl)
        {
            if(abstractControl.value == '' || abstractControl.value == null) {
                return {
                    fieldError : true,
                    customErrorMessage: 'Seleccione un estado'
                }
            }
            return null;
        }
        //Validación para el campo ciclo
        validaCicloByPass(abstractControl : AbstractControl)
        {
            return null;
        }
        //Valida estado bypass por integracion con mdbootstrap
        validaEstadoByPass(abstractControl : AbstractControl)
        {
            return null;
            
        }
        validaCampoByPass(abstractControl : AbstractControl)
        {
            console.log("Valida campo");
            return null;
        }
        //Valida un campo vacio
    */
    userValidations.prototype.validaCampoVacio = function (abstractControl) {
        if (abstractControl.value == '' || abstractControl.value == null) {
            return {
                fieldError: true,
                customErrorMessage: 'Este campo no puede estar vacío'
            };
        }
        return null;
    };
    /*
        //Validaciones de celular
        validaCelular(abstractControl : AbstractControl)
        {
            var arrTelefonosBasura = jQuery("#data_json_telefonos_basura").data();
            let arrBasuraCompleteTelefono = [];
    
            //Telefonos basura
            // var arrTelefonosBasura=[
            //     "1010101001",
            //     "0000000000",
            //     "1234567890",
            //     "5556543727",
            //     "5556581111",
            //     "5552074077",
            //     "5552074083",
            //     "5556842142",
            //     "5556849112",
            //     "5556258646",
            //     "5553951111",
            //     "5555575759",
            //     "5552009000",
            //     "5551308000",
            //     "5551308646",
            //     "5555540612",
            //     "5556543210",
            //     "5553532763",
            //     "5553532823",
            //     "5556832222",
            //     "5552295600",
            //     "5556842124",
            //     "5552410245",
            //     "5552305100",
            //     "5557703548",
            //     "5557871540",
            //     "5555606988",
            //     "5555650521",
            //     "5555651039",
            //     "5553731122",
            //     "5555653638"
            // ];
            //var telefono_basura = arrTelefonosBasura.indexOf(abstractControl.value);
            if(abstractControl.value == ""){
                return {
                    fieldError : true,
                    customErrorMessage: 'Ingrese un número de celular'
                }
            
            }else if(abstractControl.value.length < 10){
                return {
                    fieldError : true,
                    customErrorMessage: 'Ingrese los 10 digitos de su celular'
                }
            }
    
            else if(Object.keys(arrTelefonosBasura).length != 0) {
                
                for(var i = 0; i < Object.keys(arrTelefonosBasura).length ;i++){
                    var arrRes = arrTelefonosBasura[i];
                    arrBasuraCompleteTelefono.push(arrRes);
                     
                }
                for(i = 0; i < Object.keys(arrTelefonosBasura).length ; i++){
                    //console.log(arrBasuraComplete[i]);
                    if(abstractControl.value.search(arrBasuraCompleteTelefono[i]) !== -1){
                        return {
                            fieldError : true,
                            customErrorMessage: 'Ingrese un número de celular valido'
                        }
                    }
                }
    
            }
    
            // else if(telefono_basura !== -1) {
            //     return {
            //         fieldError : true,
            //         customErrorMessage: 'Ingrese un número de celular valido'
            //     }
            // }
            
            else if(abstractControl.value.search(/[^0-9]/g) != -1){
                return {
                    fieldError : true,
                    customErrorMessage: 'Ingrese un número de celular valido'
                }
            }else if( abstractControl.value.search(/(\d)\1\1\1\1/g) != -1){
                return {
                    fieldError : true,
                    customErrorMessage: 'Ingrese un número de celular valido'
                }
            }else if(abstractControl.value.charAt(0) == 0){
    
                return {
                    fieldError : true,
                    customErrorMessage: 'Ingrese un número de celular valido'
                }
    
            }else if(
                    jQuery("#frm_estado").val() =='CIUDAD DE MEXICO' &&
                    abstractControl.value.charAt(0)!="5" &&
                    abstractControl.value.charAt(1)!="5") {
    
                return {
                    fieldError : true,
                    customErrorMessage: 'El número ingresado no corresponde a la CIUDAD DE MÉXICO'
                }
    
            }
            return null;
    
        }//Termina valida celular
    */
    userValidations.prototype.validaNumeroDeCuenta = function (abstractControl) {
        if (!/^([a-zA-Z0-9])*$/.test(abstractControl.value)) {
            return {
                fieldError: true,
                customErrorMessage: 'Debes escribir un número de cuenta en este campo'
            };
        }
        if (abstractControl.value == "" || abstractControl.value == null) {
            return {
                fieldError: true,
                customErrorMessage: 'Este campo no puede estar vacio...'
            };
        }
    };
    userValidations.prototype.validaRadioPerfilBV = function (abstractControl) {
        if (!abstractControl.value) {
            return {
                fieldError: true,
                customErrorMessage: 'Debes seleccionar tu perfil'
            };
        }
    };
    userValidations.prototype.validaAviso = function (abstractControl) {
        if (!abstractControl.value) {
            return {
                fieldError: true,
                customErrorMessage: 'Acepta las politicas de privacidad.'
            };
        }
    };
    return userValidations;
}());



/***/ }),

/***/ "./src/app/services/autocomplete.service.ts":
/*!**************************************************!*\
  !*** ./src/app/services/autocomplete.service.ts ***!
  \**************************************************/
/*! exports provided: AutocompleteService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AutocompleteService", function() { return AutocompleteService; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/esm5/core.js");
/* harmony import */ var _angular_http__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/http */ "./node_modules/@angular/http/esm5/http.js");


var AutocompleteService = /** @class */ /*@__PURE__*/ (function () {
    function AutocompleteService(http) {
        this.http = http;
        this.headers = new _angular_http__WEBPACK_IMPORTED_MODULE_1__["Headers"]({ 'Accept': 'application/jsonp', 'Content-Type': 'application/jsonp', 'Access-Control-Allow-Origin': '*' });
        this.options = new _angular_http__WEBPACK_IMPORTED_MODULE_1__["RequestOptions"]({ headers: this.headers });
        this.suggestions = [];
        this.suggest = [];
        this.response = [];
    }
    AutocompleteService.prototype.ngOnDestroy = function () {
        this.suggestions = null;
    };
    AutocompleteService.prototype.callAutocomplete = function (id, url) {
        this.id = id;
        this.url = url; //"//clients1.google.com/complete/searchhl=en&client=partner&source=gcsc&partnerid=002268575599804991473:n-g-9uohyde&ds=cse&nocache="
        var that = this;
        jQuery(id).typeahead({
            hint: false,
            minLength: 1,
            classNames: {
                sugerencia: 'typeahead-hint',
                seleccionable: 'typeahead-menu',
                input: 'typeahead-input',
                hint: 'typeahead-hint',
                menu: 'typeahead-menu',
                open: 'typeahead-open'
            }
        }, {
            name: 'carreras',
            source: function (request, syncresp, asyncresp) {
                that.asyncresp = asyncresp;
                var urlautocomplete = url + Math.random().toString() + "&q=" + request;
                //Aqui se define que subscripcion hara el typeahead mediante el id pasado
                //if( id == ".inputGCS" || id == "#gcsextrangero" ){
                $.ajax(urlautocomplete, {
                    dataType: 'jsonp',
                    timeout: 500,
                    success: function (data, status, xhr) {
                        this.suggest = [];
                        console.log("OK");
                        //console.log(data);
                        for (var item = 0; item < data[1].length; item++) {
                            this.suggest.push(data[1][item][0]);
                        }
                        console.log(this.suggest);
                        that.asyncresp(this.suggest);
                    },
                    error: function (jqXhr, textStatus, errorMessage) {
                        console.log("Error");
                        console.log(jqXhr);
                        console.log(textStatus);
                        console.log(errorMessage);
                    }
                });
                //}
                return this.asyncresp;
            },
            limit: 10
        });
    };
    return AutocompleteService;
}());



/***/ }),

/***/ "./src/app/services/generic.service.ts":
/*!*********************************************!*\
  !*** ./src/app/services/generic.service.ts ***!
  \*********************************************/
/*! exports provided: GenericService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "GenericService", function() { return GenericService; });
/* harmony import */ var _readJson_service__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./readJson.service */ "./src/app/services/readJson.service.ts");

var GenericService = /** @class */ /*@__PURE__*/ (function () {
    function GenericService(readJson) {
        this.readJson = readJson;
    }
    //Validar si una imagen existe
    GenericService.prototype.checkImage = function (url) {
    };
    //Funcion que emula get de php
    GenericService.prototype.getMethod = function (param) {
        if (param === void 0) {
            param = "";
        }
        console.log("param passed" + param);
        if (param != "") {
            console.log("SI trae parametro " + param);
            return vars[param.toLowerCase()] ? vars[param.toLowerCase()] : null;
        }
        var vars = {};
        window.location.href.replace(location.hash, '').replace(/[?&]+([^=&]+)=?([^&]*)?/gi, function (m, key, value) {
            vars[key.toLowerCase()] = value !== undefined ? value : '';
        });
        return vars;
    };
    //Ordena alfabeticamente
    GenericService.prototype.dynamicSort = function (property) {
        var sortOrder = 1;
        if (property[0] === "-") {
            sortOrder = -1;
            property = property.substr(1);
        }
        return function (a, b) {
            var result = (a[property] < b[property]) ? -1 : (a[property] > b[property]) ? 1 : 0;
            return result * sortOrder;
        };
    };
    //Listado de estados
    GenericService.prototype.getEstados = function () {
        var estados = [
            { "name": "Aguascalientes", "crm_name": "AGUASCALIENTES" },
            { "name": "Baja California", "crm_name": "BAJA CALIFORNIA NOR" },
            { "name": "Baja California Sur", "crm_name": "BAJA CALIFORNIA SUR" },
            { "name": "Campeche", "crm_name": "CAMPECHE" },
            { "name": "Chiapas", "crm_name": "CHIAPAS" },
            { "name": "Chihuahua", "crm_name": "CHIHUAHUA" },
            { "name": "Ciudad de México", "crm_name": "CIUDAD DE MEXICO" },
            { "name": "Coahuila", "crm_name": "COAHUILA" },
            { "name": "Colima", "crm_name": "COLIMA" },
            { "name": "Durango", "crm_name": "DURANGO" },
            { "name": "Estado de México", "crm_name": "ESTADO DE MEXICO" },
            { "name": "Guanajuato", "crm_name": "GUANAJUATO" },
            { "name": "Guerrero", "crm_name": "GUERRERO" },
            { "name": "Hidalgo", "crm_name": "HIDALGO" },
            { "name": "Jalisco", "crm_name": "JALISCO" },
            { "name": "Michoacán", "crm_name": "MICHOACAN" },
            { "name": "Morelos", "crm_name": "MORELOS" },
            { "name": "Nayarit", "crm_name": "NAYARIT" },
            { "name": "Nuevo León", "crm_name": "NUEVO LEON" },
            { "name": "Oaxaca", "crm_name": "OAXACA" },
            { "name": "Puebla", "crm_name": "PUEBLA" },
            { "name": "Querétaro", "crm_name": "QUERETARO" },
            { "name": "Quintana Roo", "crm_name": "QUINTANA ROO" },
            { "name": "San Luis Potosí", "crm_name": "SAN LUIS POTOSI" },
            { "name": "Sinaloa", "crm_name": "SINALOA" },
            { "name": "Sonora", "crm_name": "SONORA" },
            { "name": "Tabasco", "crm_name": "TABASCO" },
            { "name": "Tamaulipas", "crm_name": "TAMAULIPAS" },
            { "name": "Tlaxcala", "crm_name": "TLAXCALA" },
            { "name": "Veracruz", "crm_name": "VERACRUZ" },
            { "name": "Yucatán", "crm_name": "YUCATAN" },
            { "name": "Zacatecas", "crm_name": "ZACATECAS" }
        ];
        return estados;
    };
    GenericService.prototype.getLineasNegocio = function () {
        var lineasNegocio = [
            {
                "name": "Preparatoria",
                "id_name": "PREPARATORIA",
                "order": "",
                "abrv": "PREPA"
            },
            {
                "name": "Licenciatura",
                "id_name": "LICENCIATURA",
                "order": "",
                "abrv": "UG"
            },
            {
                "name": "Ingeniería",
                "id_name": "INGENIERIA",
                "order": "",
                "abrv": "ING"
            },
            {
                "name": "Lic. en Salud",
                "id_name": "SALUD",
                "order": "",
                "abrv": "CS"
            },
            {
                "name": "Posgrado",
                "id_name": "POSGRADO",
                "order": "1",
                "abrv": "POS"
            }
        ];
        return lineasNegocio;
    };
    GenericService.prototype.getModalidades = function (categoria) {
        var keySearch = "Grupo_carreras";
        var arrayCarrerasPorLinea = [];
        var objJsonCarreras = JSON.parse(localStorage.getItem("jsonCarreras"));
        arrayCarrerasPorLinea = this.readJson.buscar(keySearch, categoria, objJsonCarreras);
        return arrayCarrerasPorLinea;
    };
    return GenericService;
}());



/***/ }),

/***/ "./src/app/services/getJson.service.ts":
/*!*********************************************!*\
  !*** ./src/app/services/getJson.service.ts ***!
  \*********************************************/
/*! exports provided: getJson */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "getJson", function() { return getJson; });
/* harmony import */ var _angular_http__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/http */ "./node_modules/@angular/http/esm5/http.js");
/* harmony import */ var rxjs_add_operator_toPromise__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! rxjs/add/operator/toPromise */ "./node_modules/rxjs/_esm5/add/operator/toPromise.js");
/* harmony import */ var rxjs_add_operator_toPromise__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(rxjs_add_operator_toPromise__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var rxjs_Rx__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! rxjs/Rx */ "./node_modules/rxjs/_esm5/Rx.js");
/* harmony import */ var rxjs_add_operator_map__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! rxjs/add/operator/map */ "./node_modules/rxjs/_esm5/add/operator/map.js");
/* harmony import */ var rxjs_add_operator_catch__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! rxjs/add/operator/catch */ "./node_modules/rxjs/_esm5/add/operator/catch.js");





var safeUrl = (/www\.unitec\.mx/.test(window.location.href)) ? "https://unitecmx-universidadtecno.netdna-ssl.com" : "//" + document.domain;
var getJson = /** @class */ (function () {
    //Constructor
    function getJson(http, document) {
        this.http = http;
        this.document = document;
        this.headers = new _angular_http__WEBPACK_IMPORTED_MODULE_0__["Headers"]({ Accept: "application/json" });
        this.options = new _angular_http__WEBPACK_IMPORTED_MODULE_0__["RequestOptions"]({ headers: this.headers });
        this.protocol = "https:";
        this.urlDomain = "//localhost:8888/unitec2017";
        this.urlJsonBasura = "/wp-content/themes/temaunitec/assets/frontend/json/min/json_basura.min.json";
        this.urlJsonBasuraEmail = "/wp-content/themes/temaunitec/assets/frontend/json/min/json_basuraEmail.min.json";
        this.urlJsonTelefonosBasura = "/wp-content/themes/temaunitec/assets/frontend/json/min/telefonosBasura.min.json";
        this.urlJsonCarreras = "/wp-content/themes/temaunitec/assets/frontend/json/min/json_calc_carreras.min.json";
        //Se agrega nueva url para obtener los links y nombre de las categorias de las carreras
        this.urlJsonLinksCategorias = "/wp-content/themes/temaunitec/assets/frontend/json/min/json_calc_linkPaginas.min.json";
        //Se agregan variables para calculadora
        //Costos
        this.urlJsonCalcCostos = "/wp-content/themes/temaunitec/assets/frontend/json/min/json_calc_costos.min.json";
        //Becas
        this.urlJsonCalcBecas = "/wp-content/themes/temaunitec/assets/frontend/json/min/json_calc_becas.min.json";
    }
    /**
     * Metodos para los json de formularios
     * **/
    //Para las llamdas ajax https en produccion no se debe colocar el dominio, solo la ruta relativa
    //Funcion para obtener el json de carreras
    getJson.prototype.getJsonCarreras = function () {
        jQuery
            .ajax({
            type: "GET",
            //url: this.urlJsonCarreras,
            url: safeUrl + this.urlJsonCarreras,
            "Content-Type": "application/json",
            dataType: "json",
            success: function (resultado) { }
        })
            .done(function (resultado) {
            localStorage.setItem("jsonCarreras", JSON.stringify(resultado));
        });
    };
    //Funcion para obtener el json de links y categorias
    getJson.prototype.getJsonLinksCategorias = function () {
        jQuery
            .ajax({
            type: "GET",
            //url: this.urlJsonLinksCategorias,
            url: safeUrl + this.urlJsonLinksCategorias,
            "Content-Type": "application/json",
            dataType: "json",
            success: function (resultado) { }
        })
            .done(function (resultado) {
            localStorage.setItem("jsonLinksCategorias", JSON.stringify(resultado));
        });
    };
    //Funcion para traer el Json de basura con jquery ajax
    getJson.prototype.getJsonBasura = function () {
        console.log(safeUrl + this.urlJsonBasura);
        jQuery
            .ajax({
            type: "GET",
            //url: this.urlJsonBasura,
            url: safeUrl + this.urlJsonBasura,
            "Content-Type": "application/json",
            dataType: "json",
            success: function (resultado) {
                console.log("hola");
            }
        })
            .done(function (resultado) {
            console.log("Y heme aqui e resultado");
            console.log(resultado);
            jQuery("#data_json_basura").val(1);
            jQuery("#data_json_basura").data(JSON.parse(JSON.stringify(resultado)));
            //console.log($("#data_json_basura").data());
        });
    };
    //Funcion para traer el Json de email basura con jquery ajax
    getJson.prototype.getJsonTelefonosBasura = function () {
        jQuery
            .ajax({
            type: "GET",
            //url: this.urlJsonBasuraEmail,
            url: safeUrl + this.urlJsonTelefonosBasura,
            "Content-Type": "application/json",
            dataType: "json",
            success: function (resultado) { }
        })
            .done(function (resultado) {
            console.log(resultado);
            jQuery("#data_json_telefonos_basura").val(1);
            jQuery("#data_json_telefonos_basura").data(JSON.parse(JSON.stringify(resultado)));
            //console.log($("#data_json_telefonos_basura").data());
        });
    };
    //Funcion para traer el Json de email basura con jquery ajax
    getJson.prototype.getJsonBasuraEmail = function () {
        jQuery
            .ajax({
            type: "GET",
            //url: this.urlJsonBasuraEmail,
            url: safeUrl + this.urlJsonBasuraEmail,
            "Content-Type": "application/json",
            dataType: "json",
            success: function (resultado) { }
        })
            .done(function (resultado) {
            jQuery("#data_json_basura_email").val(1);
            jQuery("#data_json_basura_email").data(JSON.parse(JSON.stringify(resultado)));
            //console.log($("#data_json_basura").data());
        });
    };
    //Funcion para traer el Json de basura con observable
    getJson.prototype.getBasuraObs = function () {
        var _this = this;
        var response;
        var subscriptionBasuraJson = this.http
            .get(safeUrl + this.urlJsonBasura, this.options)
            .map(function (res) { return res.json(); })
            .catch(function (error) {
            return rxjs_Rx__WEBPACK_IMPORTED_MODULE_2__["Observable"].throw(error.statusText);
        });
        subscriptionBasuraJson.subscribe(function (res) {
            response = res;
        }, function (error) {
            console.log(error);
        }, function () {
            console.log("done");
            if (_this.document.readyState == "complete") {
                _this.document.getElementById("data_json_basura").value = true;
                _this.document.getElementById("data_json_basura").dataset.json = JSON.stringify(response);
                //this.document.body.data = JSON.parse(JSON.stringify(res));
            }
        });
    };
    /**
     * Metodos para los json de calculadora
     * **/
    //Obtener los costos de calculadora
    getJson.prototype.getJsonCalcCostos = function () {
        jQuery
            .ajax({
            type: "GET",
            //url: this.urlJsonCarreras,
            url: safeUrl + this.urlJsonCalcCostos,
            "Content-Type": "application/json",
            dataType: "json",
            success: function (resultado) { }
        })
            .done(function (resultado) {
            localStorage.setItem("jsonCostos", JSON.stringify(resultado));
        });
    };
    //Obtener las becas
    getJson.prototype.getJsonBecasCalc = function () {
        jQuery
            .ajax({
            type: "GET",
            //url: this.urlJsonCarreras,
            url: safeUrl + this.urlJsonCalcBecas,
            "Content-Type": "application/json",
            dataType: "json",
            success: function (resultado) { }
        })
            .done(function (resultado) {
            localStorage.setItem("jsonBecas", JSON.stringify(resultado));
        });
    };
    return getJson;
}());



/***/ }),

/***/ "./src/app/services/navegador.service.ts":
/*!***********************************************!*\
  !*** ./src/app/services/navegador.service.ts ***!
  \***********************************************/
/*! exports provided: NavegadorService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "NavegadorService", function() { return NavegadorService; });
var NavegadorService = /** @class */ /*@__PURE__*/ (function () {
    function NavegadorService() {
    }
    NavegadorService.prototype.whatsBrowser = function () {
        if ((/chrom(e|ium)/.test(navigator.userAgent.toLowerCase()))) {
            return ('Chrome');
        }
        else {
            return ("no definido");
        }
    };
    return NavegadorService;
}());



/***/ }),

/***/ "./src/app/services/readJson.service.ts":
/*!**********************************************!*\
  !*** ./src/app/services/readJson.service.ts ***!
  \**********************************************/
/*! exports provided: readJson */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "readJson", function() { return readJson; });
var readJson = /** @class */ /*@__PURE__*/ (function () {
    function readJson() {
        this.miArray = [];
    }
    readJson.prototype.getJSONDimension = function (data, elemento) {
        var elementos = elemento.split("|");
        var arreglo = data;
        var temparr = arreglo[elementos[0]];
        for (var i = 1; i < elementos.length; i++) {
            temparr = temparr[elementos[i]];
        }
        return temparr;
    };
    readJson.prototype.fillMyJSON = function (route) {
        $.ajax({
            type: 'GET',
            url: route,
            success: function (resultado) {
                //console.log("resultado");
            }
        }).done(function (resultado) {
            //console.log("Hecho",resultado);
        });
    };
    readJson.prototype.getIndex = function (data, elem) {
        for (var i = 0; i < data.length; i++) {
            if (Object.keys(data[i]) == elem) {
                return i;
            }
        }
    };
    // retorna el arreglo idicando ruta especifica dividida en pipes
    readJson.prototype.getJSON = function (data, elemento, withIndex) {
        var elementos = elemento.split("|");
        var arreglo = data;
        var index, secondElemt;
        if (withIndex) {
            index = elementos.shift();
            secondElemt = elementos.shift();
        }
        else {
            secondElemt = elementos.shift();
            index = this.getIndex(arreglo, secondElemt);
        }
        var temparr = arreglo[index];
        temparr = temparr[secondElemt];
        for (var i = 0; i < elementos.length; i++) {
            temparr = temparr[elementos[i]];
        }
        return temparr;
    };
    //retorna en la data desde el primer padre del json los coinsidentes iguales a key => value
    readJson.prototype.buscar = function (key, value, jsonCarreras) {
        var resultadoBusqueda = [];
        for (var i in jsonCarreras) {
            if (this.getObjects(jsonCarreras[i], key, value).length != 0) {
                resultadoBusqueda.push(jsonCarreras[i]);
            }
        }
        return resultadoBusqueda;
    };
    //Metodo que remueve duplicados
    readJson.prototype.removeDuplicates = function (arr, key) {
        var values = {};
        return arr.filter(function (item) {
            var val = item[key];
            var exists = values[val];
            values[val] = true;
            return !exists;
        });
    };
    //Eliminar duplicados nueva
    readJson.prototype.removeDuplicatesN = function (originalArray, prop) {
        var newArray = [];
        var lookupObject = {};
        for (var i in originalArray) {
            lookupObject[originalArray[i][prop]] = originalArray[i];
        }
        for (i in lookupObject) {
            newArray.push(lookupObject[i]);
        }
        return newArray;
    };
    //Retorna los coinsidentes entre key => value desde padre directo
    readJson.prototype.getObjects = function (obj, key, val) {
        var objects = [];
        for (var i in obj) {
            if (!obj.hasOwnProperty(i))
                continue;
            if (typeof obj[i] == 'object') {
                objects = objects.concat(this.getObjects(obj[i], key, val));
            }
            else 
            //if key matches and value matches or if key matches and value is not passed (eliminating the case where key matches but passed value does not)
            if (i == key && obj[i] == val || i == key && val == '') {
                objects.push(obj);
            }
            else if (obj[i] == val && key == '') {
                //only add if the object is not already in the array
                if (objects.indexOf(obj) == -1) {
                    objects.push(obj);
                }
            }
        }
        return objects;
    };
    //Retorna arreglo con el total de elemetos que sean igual a key
    readJson.prototype.getValues = function (obj, key) {
        var objects = [];
        for (var i in obj) {
            if (!obj.hasOwnProperty(i))
                continue;
            if (typeof obj[i] == 'object') {
                objects = objects.concat(this.getValues(obj[i], key));
            }
            else if (i == key) {
                objects.push(obj[i]);
            }
        }
        return objects;
    };
    //Retorna arreglo con el total de elemetos que sean igual a val
    readJson.prototype.getKeys = function (obj, val) {
        var objects = [];
        for (var i in obj) {
            if (!obj.hasOwnProperty(i))
                continue;
            if (typeof obj[i] == 'object') {
                objects = objects.concat(this.getKeys(obj[i], val));
            }
            else if (obj[i] == val) {
                objects.push(i);
            }
        }
        return objects;
    };
    /**
    * getNodosJSON
    *
    * @param      {obj}     JSON Object
    * @return     {Array}   Arreglo con los Nodos Encontrados en el JSON.
    */
    readJson.prototype.getNodosJSON = function (obj) {
        var searchNodos = [];
        for (var x = 0; x < obj.length; x++) {
            searchNodos = Object.keys(obj[x]);
        }
        return searchNodos;
    };
    readJson.prototype.removeDuplas = function (arreglo) {
        var n = {}, r = [];
        for (var i = 0; i < arreglo.length; i++) {
            if (!n[arreglo[i]]) {
                n[arreglo[i]] = true;
                r.push(arreglo[i]);
            }
        }
        return r;
    };
    /**
     * generaArray.
     *
     * @param      {arreglo Multidimensional}            arreglo Multidimensional
     * @return     {arreglo}                             arreglo
     * @descripcion {Genera un array Dimensional de un Multidimensional Para CAMPUS}
     */
    readJson.prototype.generaArray = function (arreglo) {
        var temp = "";
        var temp2 = "";
        var Campus = [];
        for (var i = 0; i <= arreglo.length - 1; i++) {
            temp += arreglo[i] + ",";
        }
        temp2 = temp.substring(0, (temp.length) - 1);
        Campus = temp2.split(',');
        return Campus;
    };
    /**
     * getValuesJSON. (Funciona igual que buscar)
     *
     * @param      {obj}  obj     JSON Object
     * @param      {key}  key     indice a buscar
     * @return     {Array}        The values json.
     * @return     unique({Array})       JSON -> Array sin valores duplicados.
     * @descripcion {Devuelve JSON del key a buscar o el Arreglo dependiendo del Dato Contenido}
     * @Ejemplo Buscar todos los campus contenidos en el JSON
     */
    readJson.prototype.getValuesJSON = function (obj, key) {
        var arrValues = [];
        for (var x = 0; x < obj.length; x++) {
            if (obj[x][key] != null) {
                arrValues.push(obj[x][key]);
            }
        }
        return arrValues;
    };
    /**
     *getValuesCampus.
     *Total de Parametros 4
     *
     * @param      {obj}  JSON Object
     * @param      {val}  Parametros a Buscar: lineaweb|campus|QRO
     * @return     {Array} Array values.
     * @descripcion {obtener info de un array contenido en un JSON}
     */
    readJson.prototype.getValuesCampus = function (obj, val) {
        var arrCampus = [];
        var arrLinea = [];
        var lineas = [];
        var parametros = val.split("|");
        /*JSON.stringify(obj);*/
        for (var x = 0; x < obj.length; x++) {
            arrLinea.push(obj[x][parametros[0]]);
            arrCampus.push(obj[x][parametros[1]].toString()); //Convertir el arreglo en string                                       
        }
        for (var i = 0; i < arrCampus.length; i++) {
            if (arrCampus[i].indexOf(parametros[2]) != -1) {
                lineas.push(arrLinea[i]);
            }
        }
        return lineas;
    };
    /**
     * getJSONxNivel.
     *
     * @param      {obj}            JSON Object
     * @param      {key}            Parametros a Buscar Por Niveles o Nodos GASTRO|costoMateria
     * @return     {JSON Object}    JSON Filtrado
     * @descripcion {Filtra JSON dependiendo el nivel DESEADO Ejemplo 'Nivel1' | 'Nivel2' }
     */
    readJson.prototype.getJSONxNivel = function (obj, key) {
        var nivel = [];
        var nivel2 = [];
        var searchNodos = [];
        var parametros = "";
        if (key.indexOf("|") == -1) {
            parametros = key.split("|");
            /*Se obtiene JSON Completo 1er Nivel*/
            for (var x = 0; x < obj.length; x++) {
                if (obj[x][parametros[0]] != null) {
                    /*Se obtienen los nodos del Primer Nivel*/
                    searchNodos = Object.keys(obj[x][parametros[0]]);
                    nivel.push(obj[x][parametros[0]]);
                }
            }
        }
        else {
            parametros = key.split("|");
            nivel = this.getJSONxNivel(obj, parametros[0]);
            /*alert(JSON.stringify(nivel));*/
            nivel2 = this.getJSONxNivel(nivel, parametros[1]);
            if (nivel2.length == 0) {
                nivel2 = this.getJSONxNivel(nivel, "TODOS");
            }
            /*alert(JSON.stringify(nivel2));*/
            nivel = nivel2;
        }
        return nivel;
    };
    /**
     * getBeca.
     *
     * @param      {obj}    JSON Object Completo
     * @param      {key}    Parametros a Buscar: NUTRI|MAR|8.1
     * @return     {value}  Valor Beca.
     * * @descripcion {Obtener la Beca de una carrera y promedio especifico}
     */
    readJson.prototype.getBeca = function (obj, key) {
        var arrNivel1 = [];
        var arrNivel2 = [];
        var arrNivel3 = [];
        var parametros = key.split("|");
        var searchNodos = [];
        var rangos = [];
        var beca = "";
        var result = "";
        var promedio = "";
        var tempPromedio;
        tempPromedio = this.getJSONxNivel(this.getJSONxNivel(obj, parametros[0] + "|" + parametros[1]), "r1");
        promedio = tempPromedio[0]['promedio'];
        parametros[2] = (parametros[2].length > 0) ? parametros[2] : promedio[0];
        /*Se obtiene JSON Completo 1er Nivel*/
        for (var x = 0; x < obj.length; x++) {
            if (obj[x][parametros[0]] != null) {
                /*Se obtienen los nodos del Primer Nivel*/
                searchNodos = Object.keys(obj[x][parametros[0]]);
                arrNivel1.push(obj[x][parametros[0]]);
            }
        }
        parametros[1] = (searchNodos.indexOf(parametros[1]) != -1) ? parametros[1] : "TODOS";
        /*Se obtiene JSON Completo 2do Nivel apartir del Primero*/
        for (var x = 0; x < arrNivel1.length; x++) {
            if (arrNivel1[x][parametros[1]] != null) {
                rangos = Object.keys(arrNivel1[x][parametros[1]]);
                arrNivel2.push(arrNivel1[x][parametros[1]]);
            }
        }
        //obtiene Beca
        for (var x = 0; x < arrNivel2.length; x++) {
            for (var i = 0; i < rangos.length; i++) {
                if (arrNivel2[x][rangos[i]] != null) {
                    beca = arrNivel2[x][rangos[i]]['promedio'];
                    if (beca[0] <= parametros[2] && (beca[1] >= parametros[2] || beca[1] <= parametros[2])) {
                        result = arrNivel2[x][rangos[i]]['beca'];
                    }
                }
            }
        }
        return result;
    };
    /**
     * getCostoxMateria.
     *
     * @param     {jsonCompleto}        obj Nativo
     * @param      {duracionCarrera}      'POSONCUAT|duracion'
     * @param      {searchCampus}        "POSONCUAT|costoMateria"
     * @param      {obj}                  Object JSON
     * @param      {ccampus}              Nombre del campus
     * @param      {nBeca}                % Beca a aplicar
     * @param      {nMaterias}            Número de materias x default 6
     * @return     {JSON}                 Object JSON
     * @descripcion {Obtiene el descuento x # de materias y tipo de pago}
     */
    //Se arega un nuevo parametro objActual
    readJson.prototype.getCostoxMateria = function (jsonCompleto, duracionCarrera, searchCampus, obj, campus, nBeca, nMaterias) {
        var nodos = [];
        var costosTemp = {};
        var costos = {};
        var precioLista = {};
        var beca = nBeca / 100;
        var costosMaterias = [];
        var costoCompuesto;
        var noMateriasTemp;
        var temp = "";
        var duracion = {};
        var temCampus = "";
        var ccampus;
        //Obtener Todos Los Precios De INGENIERIA               ING|costosMaterias
        var JsonPrimero = this.getJSONxNivel(jsonCompleto, searchCampus);
        //En caso de que la carrera no exista en el campus obtener Campus default
        var posiblesCampus = this.getValuesJSON(JsonPrimero, "1pago");
        var nodosCarrera = this.getNodosJSON(posiblesCampus);
        //Buscar si la carrera se imparte en ese Campus 
        ccampus = (nodosCarrera.indexOf(campus) !== -1) ? campus : nodosCarrera[0];
        //Obtener Nodos de la carrera 1pago, 4pagos.. etc       
        nodos = this.getNodosJSON(JsonPrimero);
        temp = duracionCarrera.split("|");
        duracion = this.getJSONxNivel(this.getJSONxNivel(jsonCompleto, temp[0]), temp[1]);
        noMateriasTemp = this.generaArray(this.getValues(duracion, 'materias'));
        nMaterias = (nMaterias == "" || nMaterias == noMateriasTemp[0] || nMaterias == noMateriasTemp[1]) ? noMateriasTemp[0] : nMaterias;
        for (var i = 0; i < nodos.length; i++) {
            //Obtener Precios Por Campus y por el numero de nodos que lo conforman
            costoCompuesto = this.getJSONxNivel(JsonPrimero, nodos[i] + "|" + ccampus);
            costosMaterias = this.getValuesJSON(obj, nodos[i]);
            //Verificar si es costo Compuesto
            var verificaCostoCompuesto = this.getNodosJSON(costoCompuesto);
            if (verificaCostoCompuesto.length === 2) {
                var costoUno = this.getValues(costoCompuesto, 'uno');
                var costoDos = this.getValues(costoCompuesto, 'dos');
                var costoMateriasCampus = this.getValues(costosMaterias, ccampus);
                costosTemp[nodos[i]] = ((nMaterias * (costoUno - (costoUno * beca))).toFixed(2) + "|" + ((nMaterias) * (costoDos - (costoDos * beca))).toFixed(2)).toString();
                precioLista[nodos[i]] = costoUno + "|" + costoDos;
            }
            else {
                costosTemp[nodos[i]] = ((nMaterias * (costoCompuesto - (costoCompuesto * beca))).toFixed(2)).toString();
                precioLista[nodos[i]] = costoCompuesto.toString();
            }
        }
        costos['campus'] = ccampus;
        costos['duracion'] = duracion;
        costos['precioLista'] = precioLista;
        costos['precioBeca'] = costosTemp;
        return costos;
    };
    return readJson;
}());

/*document.getElementById("resultadoGlobal").innerHTML =
( getJSON( miJSON, "PREPA|TODOS|r1", false ) );*/
// this.buscar("lineaWeb", "INGENIERIA" );
//Solo retorna el objeto desde el padre directo...
//console.log( getObjects( miJSON, 'nombre', 'Arturo' ) );
//Retorna el total de elementos key indicado en todo el JSON 
//console.log( getValues( miJSON, 'nombre' ) );
//Retorna todos los elementos con el valor indicado en todo el JSON
//console.log( getKeys( miJSON, 'Arturo' ) );
//console.log( this.miArray ); 


/***/ }),

/***/ "./src/app/services/webspeach.service.ts":
/*!***********************************************!*\
  !*** ./src/app/services/webspeach.service.ts ***!
  \***********************************************/
/*! exports provided: WebspeachService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "WebspeachService", function() { return WebspeachService; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/esm5/core.js");
/* harmony import */ var rxjs_Rx__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! rxjs/Rx */ "./node_modules/rxjs/_esm5/Rx.js");
/* harmony import */ var lodash__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! lodash */ "./node_modules/lodash/lodash.js");
/* harmony import */ var lodash__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(lodash__WEBPACK_IMPORTED_MODULE_2__);



var WebspeachService = /** @class */ /*@__PURE__*/ (function () {
    function WebspeachService(zone) {
        this.zone = zone;
    }
    WebspeachService.prototype.record = function () {
        var _this = this;
        return rxjs_Rx__WEBPACK_IMPORTED_MODULE_1__["Observable"].create(function (observer) {
            var webkitSpeechRecognition = window.webkitSpeechRecognition;
            _this.speechRecognition = new webkitSpeechRecognition();
            //this.speechRecognition = SpeechRecognition;
            _this.speechRecognition.continuous = true;
            //this.speechRecognition.interimResults = true;
            _this.speechRecognition.lang = 'es';
            _this.speechRecognition.maxAlternatives = 1;
            //Mejoras JEAB 19-09-2017
            //this.speechRecognition.interimResults = true;
            _this.speechRecognition.continuous = false;
            _this.speechRecognition.onresult = function (speech) {
                var term = "";
                if (speech.results) {
                    var result = speech.results[speech.resultIndex];
                    var transcript = result[0].transcript;
                    if (result.isFinal) {
                        if (result[0].confidence < 0.3) {
                            console.log("Resultado no conocido - Intenta de nuevo");
                        }
                        else {
                            term = lodash__WEBPACK_IMPORTED_MODULE_2__["trim"](transcript);
                            //console.log("¿Dijiste algo? -> " + term + " , Si no, entonces di otra cosa...");
                        }
                    }
                    else {
                        console.log("Not is final");
                    }
                }
                _this.zone.run(function () {
                    observer.next(term);
                });
            };
            _this.speechRecognition.onerror = function (error) {
                observer.error(error);
            };
            _this.speechRecognition.onend = function () {
                observer.complete();
            };
            /*this.speechRecognition.onaudioend = () => {
                console.log("Termino");
                this.speechRecognition.start();
            };*/
            _this.speechRecognition.start();
            console.log("habla - estoy preparado para escribir !!!");
        });
    };
    WebspeachService.prototype.DestroySpeechObject = function () {
        if (this.speechRecognition)
            this.speechRecognition.stop();
    };
    return WebspeachService;
}());



/***/ }),

/***/ "./src/app/utilities/modal/modal.component.ngfactory.js":
/*!**************************************************************!*\
  !*** ./src/app/utilities/modal/modal.component.ngfactory.js ***!
  \**************************************************************/
/*! exports provided: RenderType_ModalComponent, View_ModalComponent_0, View_ModalComponent_Host_0, ModalComponentNgFactory */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "RenderType_ModalComponent", function() { return RenderType_ModalComponent; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "View_ModalComponent_0", function() { return View_ModalComponent_0; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "View_ModalComponent_Host_0", function() { return View_ModalComponent_Host_0; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ModalComponentNgFactory", function() { return ModalComponentNgFactory; });
/* harmony import */ var _modal_component_scss_shim_ngstyle__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./modal.component.scss.shim.ngstyle */ "./src/app/utilities/modal/modal.component.scss.shim.ngstyle.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/esm5/core.js");
/* harmony import */ var _formularios_forms_parent_components_formulario_login_formulario_login_component_ngfactory__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../formularios/forms_parent_components/formulario-login/formulario-login.component.ngfactory */ "./src/app/formularios/forms_parent_components/formulario-login/formulario-login.component.ngfactory.js");
/* harmony import */ var _formularios_forms_services_validaciones_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../formularios/forms_services/validaciones.service */ "./src/app/formularios/forms_services/validaciones.service.ts");
/* harmony import */ var _services_getJson_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../../services/getJson.service */ "./src/app/services/getJson.service.ts");
/* harmony import */ var _angular_platform_browser__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/platform-browser */ "./node_modules/@angular/platform-browser/esm5/platform-browser.js");
/* harmony import */ var _formularios_forms_services_formCookies_service__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../../formularios/forms_services/formCookies.service */ "./src/app/formularios/forms_services/formCookies.service.ts");
/* harmony import */ var ngx_cookie_service_cookie_service_cookie_service__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ngx-cookie-service/cookie-service/cookie.service */ "./node_modules/ngx-cookie-service/cookie-service/cookie.service.js");
/* harmony import */ var _formularios_forms_services_send_service__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ../../formularios/forms_services/send.service */ "./src/app/formularios/forms_services/send.service.ts");
/* harmony import */ var _formularios_forms_services_service_pipe__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ../../formularios/forms_services/service.pipe */ "./src/app/formularios/forms_services/service.pipe.ts");
/* harmony import */ var _services_readJson_service__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! ../../services/readJson.service */ "./src/app/services/readJson.service.ts");
/* harmony import */ var _services_generic_service__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! ../../services/generic.service */ "./src/app/services/generic.service.ts");
/* harmony import */ var _formularios_forms_parent_components_formulario_login_formulario_login_component__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! ../../formularios/forms_parent_components/formulario-login/formulario-login.component */ "./src/app/formularios/forms_parent_components/formulario-login/formulario-login.component.ts");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/esm5/forms.js");
/* harmony import */ var _buscador_formularioGCS_formularioGCS_component_ngfactory__WEBPACK_IMPORTED_MODULE_14__ = __webpack_require__(/*! ../../buscador/formularioGCS/formularioGCS.component.ngfactory */ "./src/app/buscador/formularioGCS/formularioGCS.component.ngfactory.js");
/* harmony import */ var _services_webspeach_service__WEBPACK_IMPORTED_MODULE_15__ = __webpack_require__(/*! ../../services/webspeach.service */ "./src/app/services/webspeach.service.ts");
/* harmony import */ var _services_autocomplete_service__WEBPACK_IMPORTED_MODULE_16__ = __webpack_require__(/*! ../../services/autocomplete.service */ "./src/app/services/autocomplete.service.ts");
/* harmony import */ var _angular_http__WEBPACK_IMPORTED_MODULE_17__ = __webpack_require__(/*! @angular/http */ "./node_modules/@angular/http/esm5/http.js");
/* harmony import */ var _services_navegador_service__WEBPACK_IMPORTED_MODULE_18__ = __webpack_require__(/*! ../../services/navegador.service */ "./src/app/services/navegador.service.ts");
/* harmony import */ var _buscador_formularioGCS_formularioGCS_component__WEBPACK_IMPORTED_MODULE_19__ = __webpack_require__(/*! ../../buscador/formularioGCS/formularioGCS.component */ "./src/app/buscador/formularioGCS/formularioGCS.component.ts");
/* harmony import */ var _modal_component__WEBPACK_IMPORTED_MODULE_20__ = __webpack_require__(/*! ./modal.component */ "./src/app/utilities/modal/modal.component.ts");
/**
 * @fileoverview This file was generated by the Angular template compiler. Do not edit.
 *
 * @suppress {suspiciousCode,uselessCode,missingProperties,missingOverride,checkTypes}
 * tslint:disable
 */
/** PURE_IMPORTS_START _modal.component.scss.shim.ngstyle,_angular_core,_.._formularios_forms_parent_components_formulario_login_formulario_login.component.ngfactory,_.._formularios_forms_services_validaciones.service,_.._services_getJson.service,_angular_platform_browser,_.._formularios_forms_services_formCookies.service,ngx_cookie_service_cookie_service_cookie.service,_.._formularios_forms_services_send.service,_.._formularios_forms_services_service.pipe,_.._services_readJson.service,_.._services_generic.service,_.._formularios_forms_parent_components_formulario_login_formulario_login.component,_angular_forms,_.._buscador_formularioGCS_formularioGCS.component.ngfactory,_.._services_webspeach.service,_.._services_autocomplete.service,_angular_http,_.._services_navegador.service,_.._buscador_formularioGCS_formularioGCS.component,_modal.component PURE_IMPORTS_END */
/** PURE_IMPORTS_START _modal.component.scss.shim.ngstyle,_angular_core,_.._formularios_forms_parent_components_formulario_login_formulario_login.component.ngfactory,_.._formularios_forms_services_validaciones.service,_.._services_getJson.service,_angular_platform_browser,_.._formularios_forms_services_formCookies.service,ngx_cookie_service_cookie_service_cookie.service,_.._formularios_forms_services_send.service,_.._formularios_forms_services_service.pipe,_.._services_readJson.service,_.._services_generic.service,_.._formularios_forms_parent_components_formulario_login_formulario_login.component,_angular_forms,_.._buscador_formularioGCS_formularioGCS.component.ngfactory,_.._services_webspeach.service,_.._services_autocomplete.service,_angular_http,_.._services_navegador.service,_.._buscador_formularioGCS_formularioGCS.component,_modal.component PURE_IMPORTS_END */





















var styles_ModalComponent = [_modal_component_scss_shim_ngstyle__WEBPACK_IMPORTED_MODULE_0__["styles"]];
var RenderType_ModalComponent = /*@__PURE__*/ /*@__PURE__*/ _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵcrt"]({ encapsulation: 0, styles: styles_ModalComponent, data: {} });

function View_ModalComponent_0(_l) { return _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵvid"](0, [(_l()(), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵted"](-1, null, ["\n"])), (_l()(), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵeld"](1, 0, null, null, 32, "div", [["aria-hidden", "true"], ["aria-labelledby", "myModalLabel"], ["class", "modal fade"], ["id", "elementoModal2"], ["role", "dialog"], ["tabindex", "-1"]], null, null, null, null, null)), (_l()(), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵted"](-1, null, ["\n    "])), (_l()(), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵeld"](3, 0, null, null, 29, "div", [["class", "modal-dialog modal-fluid"], ["role", "document"]], null, null, null, null, null)), (_l()(), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵted"](-1, null, ["\n        "])), (_l()(), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵted"](-1, null, ["\n        "])), (_l()(), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵeld"](6, 0, null, null, 24, "div", [["class", "modal-content"]], null, null, null, null, null)), (_l()(), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵted"](-1, null, ["\n            "])), (_l()(), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵted"](-1, null, ["\n            "])), (_l()(), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵeld"](9, 0, null, null, 10, "div", [["class", "modal-header"]], null, null, null, null, null)), (_l()(), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵted"](-1, null, ["\n                "])), (_l()(), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵeld"](11, 0, null, null, 4, "button", [["aria-label", "Close"], ["class", "close"], ["data-dismiss", "modal"], ["type", "button"]], null, null, null, null, null)), (_l()(), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵted"](-1, null, ["\n                    "])), (_l()(), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵeld"](13, 0, null, null, 1, "span", [["aria-hidden", "true"]], null, null, null, null, null)), (_l()(), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵted"](-1, null, ["\u00D7"])), (_l()(), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵted"](-1, null, ["\n                "])), (_l()(), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵted"](-1, null, ["\n                "])), (_l()(), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵeld"](17, 0, null, null, 1, "h4", [["class", "titulo modal-title w-100"], ["id", "myModalLabel"]], null, null, null, null, null)), (_l()(), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵted"](-1, null, ["Solicita informes"])), (_l()(), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵted"](-1, null, ["\n            "])), (_l()(), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵted"](-1, null, ["\n\n            "])), (_l()(), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵted"](-1, null, ["\n            "])), (_l()(), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵeld"](22, 0, null, null, 7, "div", [["class", "modal-body"]], null, null, null, null, null)), (_l()(), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵted"](-1, null, ["\n                "])), (_l()(), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵeld"](24, 0, null, null, 4, "app-formulario-login", [], null, null, null, _formularios_forms_parent_components_formulario_login_formulario_login_component_ngfactory__WEBPACK_IMPORTED_MODULE_2__["View_FormularioLoginComponent_0"], _formularios_forms_parent_components_formulario_login_formulario_login_component_ngfactory__WEBPACK_IMPORTED_MODULE_2__["RenderType_FormularioLoginComponent"])), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵprd"](512, null, _formularios_forms_services_validaciones_service__WEBPACK_IMPORTED_MODULE_3__["userValidations"], _formularios_forms_services_validaciones_service__WEBPACK_IMPORTED_MODULE_3__["userValidations"], [_services_getJson_service__WEBPACK_IMPORTED_MODULE_4__["getJson"], _angular_platform_browser__WEBPACK_IMPORTED_MODULE_5__["DOCUMENT"]]), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵprd"](512, null, _formularios_forms_services_formCookies_service__WEBPACK_IMPORTED_MODULE_6__["formCookiesService"], _formularios_forms_services_formCookies_service__WEBPACK_IMPORTED_MODULE_6__["formCookiesService"], [ngx_cookie_service_cookie_service_cookie_service__WEBPACK_IMPORTED_MODULE_7__["CookieService"]]), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵprd"](512, null, _formularios_forms_services_send_service__WEBPACK_IMPORTED_MODULE_8__["sendService"], _formularios_forms_services_send_service__WEBPACK_IMPORTED_MODULE_8__["sendService"], [_formularios_forms_services_formCookies_service__WEBPACK_IMPORTED_MODULE_6__["formCookiesService"], _formularios_forms_services_service_pipe__WEBPACK_IMPORTED_MODULE_9__["CapitalizePipe"], _formularios_forms_services_validaciones_service__WEBPACK_IMPORTED_MODULE_3__["userValidations"], _services_readJson_service__WEBPACK_IMPORTED_MODULE_10__["readJson"], _formularios_forms_services_formCookies_service__WEBPACK_IMPORTED_MODULE_6__["formCookiesService"], _services_generic_service__WEBPACK_IMPORTED_MODULE_11__["GenericService"]]), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵdid"](28, 114688, null, 0, _formularios_forms_parent_components_formulario_login_formulario_login_component__WEBPACK_IMPORTED_MODULE_12__["FormularioLoginComponent"], [_angular_forms__WEBPACK_IMPORTED_MODULE_13__["FormBuilder"], _formularios_forms_services_validaciones_service__WEBPACK_IMPORTED_MODULE_3__["userValidations"], _formularios_forms_services_send_service__WEBPACK_IMPORTED_MODULE_8__["sendService"], _services_getJson_service__WEBPACK_IMPORTED_MODULE_4__["getJson"], _formularios_forms_services_formCookies_service__WEBPACK_IMPORTED_MODULE_6__["formCookiesService"]], null, null), (_l()(), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵted"](-1, null, ["\n            "])), (_l()(), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵted"](-1, null, ["\n        "])), (_l()(), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵted"](-1, null, ["\n        "])), (_l()(), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵted"](-1, null, ["\n    "])), (_l()(), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵted"](-1, null, ["\n"])), (_l()(), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵted"](-1, null, ["\n"])), (_l()(), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵted"](-1, null, ["\n\n\n"])), (_l()(), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵted"](-1, null, ["\n"])), (_l()(), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵeld"](37, 0, null, null, 20, "div", [["aria-hidden", "true"], ["aria-labelledby", "myModalLabel"], ["class", "modal fade modal-buscador"], ["data-backdrop", "false"], ["data-keyboard", "false"], ["id", "buscador"], ["role", "dialog"], ["tabindex", "-1"]], null, null, null, null, null)), (_l()(), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵted"](-1, null, ["\n    "])), (_l()(), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵeld"](39, 0, null, null, 17, "div", [["class", "modal-dialog modal-fluid"], ["role", "document"]], null, null, null, null, null)), (_l()(), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵted"](-1, null, ["\n        "])), (_l()(), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵted"](-1, null, ["\n        "])), (_l()(), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵeld"](42, 0, null, null, 12, "div", [["class", "modal-content-buscador"]], null, null, null, null, null)), (_l()(), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵted"](-1, null, ["\n            "])), (_l()(), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵted"](-1, null, ["\n            "])), (_l()(), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵeld"](45, 0, null, null, 8, "div", [["class", "modal-body"]], null, null, null, null, null)), (_l()(), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵted"](-1, null, ["\n                "])), (_l()(), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵeld"](47, 0, null, null, 5, "formularioGCS", [], null, null, null, _buscador_formularioGCS_formularioGCS_component_ngfactory__WEBPACK_IMPORTED_MODULE_14__["View_FormularioGCSComponent_0"], _buscador_formularioGCS_formularioGCS_component_ngfactory__WEBPACK_IMPORTED_MODULE_14__["RenderType_FormularioGCSComponent"])), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵprd"](512, null, _formularios_forms_services_validaciones_service__WEBPACK_IMPORTED_MODULE_3__["userValidations"], _formularios_forms_services_validaciones_service__WEBPACK_IMPORTED_MODULE_3__["userValidations"], [_services_getJson_service__WEBPACK_IMPORTED_MODULE_4__["getJson"], _angular_platform_browser__WEBPACK_IMPORTED_MODULE_5__["DOCUMENT"]]), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵprd"](512, null, _services_webspeach_service__WEBPACK_IMPORTED_MODULE_15__["WebspeachService"], _services_webspeach_service__WEBPACK_IMPORTED_MODULE_15__["WebspeachService"], [_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgZone"]]), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵprd"](131584, null, _services_autocomplete_service__WEBPACK_IMPORTED_MODULE_16__["AutocompleteService"], _services_autocomplete_service__WEBPACK_IMPORTED_MODULE_16__["AutocompleteService"], [_angular_http__WEBPACK_IMPORTED_MODULE_17__["Http"]]), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵprd"](512, null, _services_navegador_service__WEBPACK_IMPORTED_MODULE_18__["NavegadorService"], _services_navegador_service__WEBPACK_IMPORTED_MODULE_18__["NavegadorService"], []), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵdid"](52, 245760, null, 0, _buscador_formularioGCS_formularioGCS_component__WEBPACK_IMPORTED_MODULE_19__["FormularioGCSComponent"], [_angular_forms__WEBPACK_IMPORTED_MODULE_13__["FormBuilder"], _formularios_forms_services_validaciones_service__WEBPACK_IMPORTED_MODULE_3__["userValidations"], _services_webspeach_service__WEBPACK_IMPORTED_MODULE_15__["WebspeachService"], _angular_http__WEBPACK_IMPORTED_MODULE_17__["Http"], _services_autocomplete_service__WEBPACK_IMPORTED_MODULE_16__["AutocompleteService"], _services_navegador_service__WEBPACK_IMPORTED_MODULE_18__["NavegadorService"]], null, null), (_l()(), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵted"](-1, null, ["\n            "])), (_l()(), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵted"](-1, null, ["\n        "])), (_l()(), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵted"](-1, null, ["\n        "])), (_l()(), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵted"](-1, null, ["\n    "])), (_l()(), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵted"](-1, null, ["\n"])), (_l()(), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵted"](-1, null, ["\n"]))], function (_ck, _v) { _ck(_v, 28, 0); _ck(_v, 52, 0); }, null); }
function View_ModalComponent_Host_0(_l) { return _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵvid"](0, [(_l()(), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵeld"](0, 0, null, null, 1, "app-modal", [], null, null, null, View_ModalComponent_0, RenderType_ModalComponent)), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵdid"](1, 114688, null, 0, _modal_component__WEBPACK_IMPORTED_MODULE_20__["ModalComponent"], [], null, null)], function (_ck, _v) { _ck(_v, 1, 0); }, null); }
var ModalComponentNgFactory = /*@__PURE__*/ /*@__PURE__*/ _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵccf"]("app-modal", _modal_component__WEBPACK_IMPORTED_MODULE_20__["ModalComponent"], View_ModalComponent_Host_0, {}, {}, []);



/***/ }),

/***/ "./src/app/utilities/modal/modal.component.scss.shim.ngstyle.js":
/*!**********************************************************************!*\
  !*** ./src/app/utilities/modal/modal.component.scss.shim.ngstyle.js ***!
  \**********************************************************************/
/*! exports provided: styles */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "styles", function() { return styles; });
/**
 * @fileoverview This file was generated by the Angular template compiler. Do not edit.
 *
 * @suppress {suspiciousCode,uselessCode,missingProperties,missingOverride,checkTypes}
 * tslint:disable
 */
/** PURE_IMPORTS_START  PURE_IMPORTS_END */
/** PURE_IMPORTS_START  PURE_IMPORTS_END */
var styles = [".modal-buscador[_ngcontent-%COMP%] {\n  max-height: calc(100vh - 69px);\n  top: 69px; }\n\n.modal-fluid[_ngcontent-%COMP%] {\n  max-width: none;\n  position: relative;\n  margin: 0 auto; }\n\n.modal-content[_ngcontent-%COMP%] {\n  min-height: calc(100vh); }\n\n.modal-content-buscador[_ngcontent-%COMP%] {\n  background-color: #ffffff;\n  min-height: calc(100vh - 72px);\n  width: 100%; }\n\n.modal-body[_ngcontent-%COMP%] {\n  padding: 0 !important; }"];



/***/ }),

/***/ "./src/app/utilities/modal/modal.component.ts":
/*!****************************************************!*\
  !*** ./src/app/utilities/modal/modal.component.ts ***!
  \****************************************************/
/*! exports provided: ModalComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ModalComponent", function() { return ModalComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/esm5/core.js");

var ModalComponent = /** @class */ /*@__PURE__*/ (function () {
    function ModalComponent() {
        this.componente = "formLogin";
    }
    ModalComponent.prototype.ngOnInit = function () {
        jQuery(document).ready(function () {
            var y;
            var x;
            jQuery("#buscador").on("shown.bs.modal", function (e) {
                //e.preventDefault();
                //var x=window.scrollX;
                //var y=window.scrollY;
                //window.onscroll=function(){window.scrollTo(x, y);};
                console.log(window.scrollY);
                x = window.scrollX;
                y = window.scrollY;
                $('body').css('overflow', 'hidden');
                //jQuery("body").css({ "overflow-y" : "hidden" });
                jQuery("html").css({ "position": "fixed" });
                jQuery("body").css({ "position": "fixed" });
                //window.onscroll=function(){window.scrollTo(x, y);};
                console.log("jeab - pos - open" + y);
            });
            jQuery("#buscador").on("hidden.bs.modal", function (e) {
                console.log("Aqui y: " + y);
                //window.onscroll=function(){};
                //jQuery("html").css({ "overflow-y" : "" });
                jQuery("body").css({ "overflow": "" });
                jQuery("html").css({ "position": "" });
                jQuery("body").css({ "position": "" });
                console.log("jeab - pos closed" + y);
                window.scrollTo(x, y);
            });
        });
    };
    return ModalComponent;
}());



/***/ }),

/***/ "./src/environments/environment.ts":
/*!*****************************************!*\
  !*** ./src/environments/environment.ts ***!
  \*****************************************/
/*! exports provided: environment */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "environment", function() { return environment; });
var environment = {
    production: true,
    api_url: 'https://conduit.productionready.io/api'
};


/***/ }),

/***/ "./src/main.ts":
/*!*********************!*\
  !*** ./src/main.ts ***!
  \*********************/
/*! no exports provided */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/esm5/core.js");
/* harmony import */ var _environments_environment__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./environments/environment */ "./src/environments/environment.ts");
/* harmony import */ var _app_app_module_ngfactory__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./app/app.module.ngfactory */ "./src/app/app.module.ngfactory.js");
/* harmony import */ var _angular_platform_browser__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/platform-browser */ "./node_modules/@angular/platform-browser/esm5/platform-browser.js");




if (_environments_environment__WEBPACK_IMPORTED_MODULE_1__["environment"].production) {
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["enableProdMode"])();
    window.console.log = function () { };
}
_angular_platform_browser__WEBPACK_IMPORTED_MODULE_3__["platformBrowser"]().bootstrapModuleFactory(_app_app_module_ngfactory__WEBPACK_IMPORTED_MODULE_2__["AppModuleNgFactory"]);


/***/ }),

/***/ 0:
/*!***************************!*\
  !*** multi ./src/main.ts ***!
  \***************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__(/*! C:\xampp\htdocs\lpweb\wp-content\angular\unitec-app-desk-v6\src\main.ts */"./src/main.ts");


/***/ })

},[[0,"runtime","vendor"]]]);