//Variables generales para Custom Tabs
var CustomTabs = $("section#tabNav, section#tabNav.free");
var tolerancia = 25;
var hoverUnico = true;

$(document).ready(function(){
  // Esta funci�n es para inyectar la funci�n que dispara el dataLayer
  $('span#ctc-btn-formulario').attr('onclick', 'openModal("#modal_frm_app");callTrackeo()');
  // End funci�n es para inyectar la funci�n que dispara el dataLayer

  // Par a los videos de los vanners
  id = $('.youtube-video').attr("id"); 
  //$("#" + id ).ready( get_youtube_video( id ) );

  /*------------------- Inicia seccion para carruseles ---------------------*/
  $carruseles =  $(".carousel");
  $selectoresCarrusel = "";
  for( carrusel = 0; carrusel < $carruseles.length; carrusel++ ){
    separador = ( carrusel == ( $carruseles.length - 1 ) )? "": ", " ;
    $selectoresCarrusel += "#" + $carruseles[carrusel]["id"] + separador;
  }

  /*Validación de numero de sliders en el carusel para mostrar flecha derecha*/
  var cntTabs = $("#panelContent").val();

  for (var i = 0; i <= cntTabs; i++) {
    if ($("#sliders-" + i).val() > 1) {
      $(".showarrow-" + i).removeClass("d-none");
    }
  }

  /*End Validación de numero de sliders en el carusel para mostrar flecha derecha*/

  /*Funcionlidad lazy para Carruseles BY SRP BY SRP 14-08-2018*/
  $($selectoresCarrusel).on("slid.bs.carousel", function(ev) {
    imgLazy();
  });

  //Funcionalidad para el swipe del iframe de youtube
  $(".div-yt-video-right").hammer().on('click', function () {
    $(this).parents( ".carousel" ).carousel('next');
  });
  $(".div-yt-video-left").hammer().on('click', function () {
    $(this).parents( ".carousel" ).carousel('next');
  });
  /*------------------- Termina seccion para carruseles ---------------------*/

  /*------------------- Inicia seccion para SimpleList ---------------------*/
  //Seccion para collapsables otros.
  $(".simple-list").on("click", function(){
  /*Update by SRP detectar si el collapsable trae un link y agregar clase active (Mejora para experiencia de Usuario)*/
    var items = $(this).children().find( "a" );
    // if (items.length > 0) {
    console.log( items.length );
    if( $(this).attr("class").indexOf("active") > -1 && ($(this).next("div").attr("class").indexOf("show") > -1) ){
        $(this).removeClass("active");
        $(this).next("div.flipInX").collapse('hide').css({ "border-bottom": "none !important"});
        $(this).children(".list-item-container").children(".arrow").css({ "transform" : "rotate(0deg)"});
    }else{
        $(this).siblings().removeClass("active");
        $(this).siblings().children(".list-item-container").children(".arrow").css({ "transform" : "rotate(0deg)"});
        $(this).addClass("active");
        $(this).siblings("div").collapse('hide');
        $(this).next("div.flipInX").collapse('show').css({ "border-bottom": "1px solid rgba(0,0,0,.125) !important"});;
        $(this).children(".list-item-container").children(".arrow").css({ "transform" : "rotate(180deg)"});
    }
    // }
    /*End Update by SRP detectar si el collapsable trae un link y agregar clase active (Mejora para experiencia de Usuario)*/
        
  });

  /*------------------- Inicia seccion para SimpleList ---------------------*/

  /*------------------- Inicia segmiento para CustomTabs ---------------------*/
  activador( 0 );
    
  $(".nav-item:not('.horaSlide')").on( "click", function(){
    var maxScrollLeft = $(".classic-tabs").scrollLeft('.classic-tabs').prop('scrollWidth')- $(".tabs-wrapper").width();
    var left = $(this).offset().left
    var width = $(".tabs-wrapper").width();
    var diff = left - width / 2;
    //console.log($(this).parents(".nav").offset().left);
    $(this).parents(".nav").animate({ scrollLeft: $(".tabs-wrapper").scrollLeft() + diff + 20 },100);
    //console.log($(".tabs-wrapper").scrollLeft() + diff + 20);
    //console.log("KHE: " + left);
    //Para habiliar las flechas con los tabs
    //console.log($(this).parents(".nav").offset().left);
    if(($(".tabs-wrapper").scrollLeft() + diff + 20) > 20) {
        viewTabsAndControls( $(this).parent(".nav"), "right", left );  
    }else {
        viewTabsAndControls( $(this).parent(".nav"), "left", left ); 
    }
    //viewTabsAndControls( $(this).parents(".nav"), "right" );
  });
  $(window).resize(function(){
    activador( 0 );
  });
    
  $( ".leftTabNav" ).on( "click", function() {
    var elemento = $(this).siblings( ".tabs-wrapper" ).children(".nav");
    elemento.animate({ scrollLeft: '-=50' });
    var maxScrollLeft = elemento[0]["scrollWidth"] - elemento[0]["clientWidth"];
    viewTabsAndControls( elemento, "left" );
  });

  $( ".rightTabNav" ).on( "click", function() {
      var elemento = $(this).siblings( ".tabs-wrapper" ).children(".nav");
      elemento.animate({ scrollLeft: '+=50' });
      var maxScrollLeft = elemento[0]["scrollWidth"] - elemento[0]["clientWidth"];
      viewTabsAndControls( elemento, "right" );
  }); 
  /*------------------- Termina segmiento para CustomTabs ---------------------*/

  /*Script para evitar que el # de los tabs se concatene en las url by SRP 10-01-2018*/
  $('.nav-item a').click(function (e) {            
    event.preventDefault();
  });

  /* $('a.back-naranja-unitec').click(function (e) {            
    event.preventDefault();
  });*/
  /*Script para evitar que el # de los tabs se concatene en las url by SRP 10-01-2018*/

  /*Carga General de imagenes Colocar lazy a todas las etiquetas que contengas imagénes BY SRP BY SRP 14-08-2018*/
  imgLazy();
  /*End Carga General de imagenes Colocar lazy a todas las etiquetas que contengas imagénes BY SRP BY SRP 14-08-2018*/


  /*Funcionalidad para enviar DataLayer al abrir Formulario (SOLICITA INFORMACI�N)*/
    

$('a[onclick="openModal(\'#modal_frm_app\')"], a[onclick="javascript:openModal(\'#modal_frm_app\')"]' ).click(function (e) {
    console.warn($(this));
    var location = $(this).attr('data-solicitud-location');
    if (typeof dataLayer != 'undefined') {

        dataLayer.push({
            'event': 'evBtnSol', //Static data
            'section': jQuery('#post_title').val(), //Dynamic Data
            'location': location, //Dynamic Data
        });
    }
  });
  /*Funcionalidad para enviar DataLayer al abrir Formulario (SOLICITA INFORMACI�N)*/

});

/*Funcionlidad lazy para Tabs BY SRP BY SRP 14-08-2018*/
$(".nav-item").on( "click", function(){
    $('img.lazy').lazy({
        bind: "event",
        delay: 0
    }); 
});

function callTrackeo() {
    var locationCTC = $('span#ctc-btn-formulario').attr('data-solicitud-location');
    if (typeof dataLayer != 'undefined') {

        dataLayer.push({
            'event': 'evBtnSol', //Static data
            'section': jQuery('#post_title').val(), //Dynamic Data
            'location': locationCTC, //Dynamic Data
        });
        // alert('Enviando: ' + 'section: ' + jQuery('#post_title').val() + 'location: ' + locationCTC);
    }
}

function imgLazy(){
    $('.lazy').Lazy({
        scrollDirection: 'both',
        effect: 'fadeIn',
        effectTime: 100,
        threshold: 0,
        //delay: 0,
        visibleOnly: true,
        onError: function(element) {
                console.log('error loading ' + element.data('src'));
        }
    });
}
/*Funcionlidad lazy para Carruseles BY SRP 14-08-2018*/

function youtubeLazy() {
    var youtube = document.querySelectorAll( ".youtube" );
    
    for (var i = 0; i < youtube.length; i++) {        
        var source = youtube[i].dataset.embed.replace('//www.youtube.com/embed/', '//img.youtube.com/vi/') +"/sddefault.jpg";
        //var source = "https://img.youtube.com/vi/"+ youtube[i].dataset.embed +"/sddefault.jpg";
        var image = new Image();
        image.src = source;
        image.addEventListener( "load", function() {
            youtube[ i ].appendChild( image );
        }( i ) );
        youtube[i].addEventListener( "click", function() {
            $(this).html("");
            $("#h_tab-contiene-video").val("S");
            var idframe = $(this).attr('id');
            $("#h_video-activo").val('iframe-youtube-'+idframe);
            $(this).html('<iframe id="iframe-youtube-'+idframe+'" width="560" height="315" src="' +this.dataset.embed + '?enablejsapi=1&version=3&playerapiid=ytplayer&mute=0" frameborder="0" allowfullscreen="true" allowscriptaccess="always"></iframe>');
            console.log("despues");
            setTimeout(function(){ 
                $('#iframe-youtube-'+ idframe)[0].contentWindow.postMessage( '{"event":"command","func":"' + "playVideo" + '","args":""}', "*");
            }, 500);
        });    
    }
}


/*Funciones para el manejo de Cookies BY SRP 09-10-2018 Se duplica para asegurar la carga de la funci�n*/
function setCookieForm(cname, cvalue, exdays) {
    var d = new Date();
    d.setTime(d.getTime() + (exdays * 24 * 60 * 60 * 1000));
    var expires = "expires=" + d.toUTCString();
    document.cookie = cname + "=" + cvalue + "; " + expires + ";path=/";
}

/* --------------------- Inicia segmento de ejecucion de la API de Youtube ----------------------------------*/
/*var player, comp, vid;*/

//id_componente --> Es el componente donde de emberbera el video iframe
//id_video --> Es el id del video que proporciona youtube

/*function get_youtube_video( identificador ){
    comp = identificador;
    vid = identificador;
    var tag = document.createElement('script');
    tag.src = "https://www.youtube.com/player_api";
    var firstScriptTag = document.getElementsByTagName('script')[0];
    firstScriptTag.parentNode.insertBefore(tag, firstScriptTag);
}*/

/*function onYouTubePlayerAPIReady() {
    player = new YT.Player( comp, {
        videoId: vid,
        playerVars: { 'autoplay': 1, 'controls': 0, 'loop': 1, "version": 3, 'playlist': vid },
        events:{
            "onReady": iniciar
        }
    });
}*/

//Funcion para iniciar un video con la API de youtube
function iniciar( e ){
    e.target.playVideo();
    e.target.setVolume(0);
}
/* --------------------- Termina segmento de ejecucion de la API de Youtube ----------------------------------*/

//Llamar esta funcion cuando se necesite abrir un modal angular
function openModal( selector ) {
    console.log('Selector modal'+selector);
    if (selector == '#modal_frm_app') {
        document.getElementById(selector.substr(1)).click();
    } else {
        $(selector).modal();
    }
}

//Llamar esta funcion cuando se necesite abrir un modal vue de calculadora
function abreCalculadora() {
    openModal( '#modal_frm_app' );
}

//Llamar esta funcion cuando se necesite cerrar un modal angular
function closeModal( $selector ){
    $(selector).modal('hide');
}

/* Function to animate height: auto */
function autoHeightAnimate(element, time){
        var curHeight = element.height(), // Get Default Height
        autoHeight = element.css('height', 'auto').height(); // Get Auto Height
        element.height(curHeight); // Reset to Default Height
        element.stop().animate({ height: autoHeight }, time); // Animate to Auto Height
}

//Funcion que sirve para activar los custom tabs
function activador( pos ){
    for( var customTab = 0; customTab < CustomTabs.length; customTab++){
        viewTabsAndControls( $(CustomTabs[customTab]).children( ".tabs-wrapper" ).children( ".nav" ), pos );
    }
}
//Funcion para visalizar loscontroles de los custom tabs
function viewTabsAndControls( element, pos, left ){
    //console.log(element[0]);
    //console.log("FUNCION DE LAS FLECHAS: " + left);
    var posicion = 0
    switch( pos ){
        case "left":
            posicion = element.scrollLeft() - 50;
        break;
        case "right":
            posicion = element.scrollLeft() + 50;
        break;
        default:
            posicion = element.scrollLeft();
        break;
    }

    var maxScrollLeft = element[0]["scrollWidth"] - element[0]["clientWidth"];
    //console.log("maxScrollLeft: " + maxScrollLeft);
    //console.log("POSITION: " + posicion);
    //tolerancia = 30;
    if( maxScrollLeft === 0){
        element.parent().siblings("i").css({ "display": "none" });
    }else if( posicion < tolerancia ){
        //console.log("POSICION MENOR QUE TOLERANCIA");
        element.animate({ scrollLeft: 0 });
        element.parent().siblings( ".rightTabNav" ).css({ "display" : "flex" });
        element.parent().siblings( ".leftTabNav"  ).css({ "display" : "none" });
    } else if ( posicion >= maxScrollLeft - tolerancia ){
        //console.log("POSICION MAYOR QUE SCROLLLEFT - TOLERANCIA"); 
        element.parent().siblings( ".rightTabNav" ).css({ "display" : "none" });
        element.parent().siblings( ".leftTabNav"  ).css({ "display" : "flex" });
    }else{   
        //console.log("TODO LO DEMAS");
        element.parent().siblings("i").css({ "display": "flex" });
    }
    if(left >= 580 && pos=="right") {
        element.parent().siblings( ".rightTabNav" ).css({ "display" : "none" });
        element.parent().siblings( ".leftTabNav"  ).css({ "display" : "flex" })
    }

     

}    
// funciones generales de validacion para secciones generales ej: form-chat, form-banner, whatsappp
function genValidaNombres(element){
        console.warn(element.value);
        console.warn("Validando el nombre");
        //let jsonReader = new readJson();
        var jsonBasura = jQuery("#data_json_basura").data();
        var patron = /^[a-zA-Z������\s]*$/;
        var arrBasuraComplete = [];
        var minValue = element.value.toLowerCase();
       //Esta vacio el nombre
        if(minValue === ""){ 
            return { 
                fieldError : true,
                customErrorMessage: 'No puede estar vac&iacute;o el campo' 
            }
        //Validamos en el json basura la parte uno del correo
        }else if(minValue.length <= 2 || minValue.length > 30){ 
            return { 
                fieldError : true,
                customErrorMessage: 'Ingrese m&aacute;s de dos letras y menos de 30' 
            }
        //Validamos que no existen caracteres especiales en el nombre de usuario
        }else if(minValue.search(patron) == -1) {
            return { 
                fieldError : true,
                customErrorMessage: 'Solo se aceptan letras' 
            }
        //Validamos nombres basura
        }else if(Object.keys(jsonBasura).length != 0) {
            
            for(var i = 0; i < Object.keys(jsonBasura).length ;i++){
                var arrRes = jsonBasura[i];
                arrBasuraComplete.push(arrRes);
                 
            }
            for(i = 0; i < Object.keys(jsonBasura).length ; i++){
                // console.log(arrBasuraComplete[i]);
                if(minValue.search(arrBasuraComplete[i]) !== -1){
                    return { 
                        fieldError : true,
                        customErrorMessage: 'Nombre inv&aacute;lido' 
                    }
                }
            }

        }
        return { fieldError : false };  
    }
function genvalidaEmail(inp_value)
    {
        //Flag que cambiara cuando sea valido
        var emailValid = false;
        var customErrorMessage;
        var arrEmail = inp_value.split('@');
        var lenUser = arrEmail[0].length;
        /*var jsonReader = new readJson();
        var jsonBasura = jQuery("#data_json_basura_email").data();*/
        try {
            var arrDominio = arrEmail[1].split('.');
            var arrLargo = arrDominio.length;
        }catch(err) {}

        //Esta vacio el correo
        if(arrEmail[0] === ""){ 
            return { 
                fieldError : true,
                customErrorMessage: 'Ingrese su correo' 
            }
        //Validamos en el json basura la parte uno del correo
        }/*else if(jQuery("#data_json_basura_email").val() == 1 && jsonReader.getKeys(jsonBasura, arrEmail[0]).length > 0 ) {
            return { 
                fieldError : true,
                customErrorMessage: 'Correo inv�lido' 
            }
         //Validar arroba
        }*/else if(arrEmail.length <= 1){
            return { 
                fieldError : true,
                customErrorMessage: 'Ingrese el correo completo' 
            }
        //Validamos en el json basura la parte 2 del correo
        }/*else if(jQuery("#data_json_basura_email").val() == 1 && jsonReader.getKeys(jsonBasura, arrEmail[1]).length > 0) {
            
            return { 
                fieldError : true,
                customErrorMessage: 'Correo inv�lido' 
            }
        //Longitud del nombre de usuario es muy largo o muy corto el nombre
        }*/else if(lenUser <= 2 || lenUser > 100){ 
            return { 
                fieldError : true,
                customErrorMessage: 'Longitud de correo invalida' 
            }
        //Validamos que no existen caracteres especiales en el nombre de usuario
        }else if(arrEmail[0].search(/[^a-zA-Z0-9_.-]/)!=-1){ 
            return { 
                fieldError : true,
                customErrorMessage: 'Caracter inv�lido encontrado' 
            }
        //Validamos que no existen caracteres especiales en el dominio 
        }else if(arrEmail[1].search(/[^a-zA-Z0-9_.-]/)!=-1){ 
            return { 
                fieldError : true,
                customErrorMessage: 'Caracter inv�lido encontrado' 
            }
         //Validacion de dominios MS   
        }else if(arrEmail[1].search(/(hotmail|outlook|live)/i)!= -1 && (arrEmail[0][lenUser-1].search(/\./)!=-1 || arrEmail[0][0].search(/(-|_|\.|[0-9])/)!=-1 || arrEmail[0].search(/\.{2}/g) != -1) ){
            return { 
                fieldError : true,
                customErrorMessage: 'Correo inv�lido' 
            }
        //Validacion de dominios gmail    
        }else if(arrEmail[1].search(/gmail/i)!= -1 && (arrEmail[0][lenUser-1].search(/[^a-zA-Z0-9]/)!=-1 || arrEmail[0][0].search(/(-|_|\.|[0-9])/)!=-1 || arrEmail[0].search(/\.{2}/g) != -1 || arrEmail[0].search(/(-|_)/g) != -1)){
            return { 
                fieldError : true,
                customErrorMessage: 'Correo inv�lido' 
            } 
         //Validacion de dominios yahoo     
        }else if(arrEmail[1].search(/yahoo/i)!= -1 && (arrEmail[0][lenUser-1].search(/[^a-zA-Z0-9]/)!=-1 || arrEmail[0][0].search(/(_|\.|[0-9])/)!=-1 || arrEmail[0].search(/(\.|_){2}/g) != -1 || arrEmail[0].search(/-/g)!=-1 ) ){
            return { 
                fieldError : true,
                customErrorMessage: 'Correo inv�lido' 
            } 
        //Validacion de dominios restantes       
        }else if(arrEmail[1].search(/(^hotmail|outlook|gmail|live|yahoo)/i) == -1 && ( arrEmail[0][lenUser-1].search(/(-|_|\.)/)!=-1 || arrEmail[0][0].search(/(-|_|\.|[0-9])/)!=-1 || arrEmail[0].search(/(-|_|\.){2}/g) != -1)){
            return { 
                fieldError : true,
                customErrorMessage: 'Correo inv�lido' 
            }
         //Validacion de dominios por ejemplo debe tener @mail.com por ejemplo 
        }else if (arrLargo < 2){
            return { 
                fieldError : true,
                customErrorMessage: 'Ingrese el correo completo' 
            }
        } else {
            var arrDominio = arrEmail[1].split('.');
            var arrLargo = arrDominio.length;

            // Hay emails que tienen dominio  y subdominio, por ejemplo fcastill@mail.unitec.mx,
            // entonces verificamos desde el primer elemento al penultimo, ya que el ultimo el es
            // el nombre de dominio superior y requerimos otras validaciones
            for (var x = 0; x <= arrLargo-2; x++) {
                //esta vacio el nombre del dominio o subdominio?
                if (arrDominio[x]===""){
                    return { 
                        fieldError : true,
                        customErrorMessage: 'Correo inv�lido' 
                    }
                //es muy largo o muy corto el nombre del dominio?
                }else if(arrDominio[x].length > 36 || arrDominio[x].length < 2){
                    return { 
                        fieldError : true,
                        customErrorMessage: 'Correo inv�lido' 
                    }
                } 
                /*
                //Validaci�n de correo basura en el dominio
                if(Object.keys(jsonBasura).length != 0) {
                    for(var i = 0; i < Object.keys(jsonBasura).length; i++){
                        if(arrDominio[x].indexOf(jsonBasura[i]) != -1){
                            
                            return { 
                                fieldError : true,
                                customErrorMessage: 'Correo inv�lido' 
                            }
                        }
                    } 
                }
                */
            }
            // Validamos el nombre del dominio superior
            // esta vacio
            if(arrDominio[arrLargo-1]===""){
                return { 
                    fieldError : true,
                    customErrorMessage: 'Correo inv�lido' 
                }
            }else if(arrDominio[arrLargo-1].length >= 4 || arrDominio[arrLargo-1].length <= 1){
                return { 
                    fieldError : true,
                    customErrorMessage: 'Correo inv�lido' 
                }
            }
        }//Else de validaciones principales

        return { fieldError : false }; 
    }

    function genvalidaCelular(inp_value)
    {
        var arrTelefonosBasura = jQuery("#data_json_telefonos_basura").data();
        var arrBasuraCompleteTelefono = [];

        //Telefonos basura
        // var arrTelefonosBasura=[
        //     "1010101001",
        //     "0000000000",
        //     "1234567890",
        //     "5556543727",  
        //     "5556581111",  
        //     "5552074077",  
        //     "5552074083",  
        //     "5556842142",  
        //     "5556849112",  
        //     "5556258646",  
        //     "5553951111",  
        //     "5555575759",  
        //     "5552009000",  
        //     "5551308000",  
        //     "5551308646",  
        //     "5555540612",  
        //     "5556543210",  
        //     "5553532763",  
        //     "5553532823",  
        //     "5556832222",  
        //     "5552295600",  
        //     "5556842124",  
        //     "5552410245",  
        //     "5552305100",  
        //     "5557703548",  
        //     "5557871540",  
        //     "5555606988",  
        //     "5555650521",  
        //     "5555651039",  
        //     "5553731122",  
        //     "5555653638" 
        // ];
        //var telefono_basura = arrTelefonosBasura.indexOf(abstractControl.value);
        if(inp_value == ""){
            return { 
                fieldError : true,
                customErrorMessage: 'Ingrese un n&uacute;mero de celular' 
            }
        
        }else if(inp_value.length != 10){
            return { 
                fieldError : true,
                customErrorMessage: 'Ingrese los 10 digitos de su celular' 
            } 
        }

        else if(Object.keys(arrTelefonosBasura).length != 0) {
            
            for(var i = 0; i < Object.keys(arrTelefonosBasura).length ;i++){
                var arrRes = arrTelefonosBasura[i];
                arrBasuraCompleteTelefono.push(arrRes);
                 
            }
            for(i = 0; i < Object.keys(arrTelefonosBasura).length ; i++){
                //console.log(arrBasuraComplete[i]);
                if(inp_value.search(arrBasuraCompleteTelefono[i]) !== -1){
                    return { 
                        fieldError : true,
                        customErrorMessage: 'Ingrese un n&uacute;mero de celular v&aacute;lido' 
                    }
                }
            }

        }

        // else if(telefono_basura !== -1) {
        //     return { 
        //         fieldError : true,
        //         customErrorMessage: 'Ingrese un n�mero de celular valido' 
        //     }
        // }
        
        else if(inp_value.search(/[^0-9]/g) != -1){
            return { 
                fieldError : true,
                customErrorMessage: 'Ingrese un n&uacute;mero de celular v&aacute;lido' 
            }
        }else if( inp_value.search(/(\d)\1\1\1\1/g) != -1){
            return { 
                fieldError : true,
                customErrorMessage: 'Ingrese un n&uacute;mero de celular v&aacute;lido' 
            }
        }else if(inp_value.charAt(0) == 0){

            return { 
                fieldError : true,
                customErrorMessage: 'Ingrese un n&uacute;mero de celular v&aacute;lido' 
            }

        }
        return { fieldError : false };  

    }//Termina valida celular