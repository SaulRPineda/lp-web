//Social reveal
$(document).ready(function ($) {

	var carrera = $("#ciclos").text();
	var noAnos = $("#h_duracion").val();
	var tipoPlan = $("#h_plan").val();
	var nombreDePlan = $("#nombrePlan").text();
	
	filtroTipoPlan(tipoPlan, nombreDePlan);
	filtroNoAnos(noAnos, carrera, tipoPlan);

});

$('.mapa-share > a').on('click', function (e) {
    e.preventDefault()
    $(this).parent().find('div').toggleClass('social-reveal-active-mapa');
    $(this).toggleClass('share-expanded');
});

$("[data-toggle='toggle']").click(function() {
    var selector = $(this).data("target");
    $(selector).toggleClass('in');
});

function filtroTipoPlan(keyPlan, nombrePlan) {
	var plan = "plan-" + keyPlan;

	console.log(descarga);

	var copyPresencial = "(Ideal para vivir la experiencia en campus)";
	var copyEjecutivo = "(Para mayores de 24 años que desean combinar trabajo y estudio)";
	var copyLinea = "(Pensada en personas autodidactas que desean superar distancias)";
	var copyPresencialLinea = "(Planeados para ayudarte a crecer profesionalmente)";
	var copyEjecutivoLinea = "Ejecutiva: Para mayores de 24 años que desean trabajar y estudiar. <br> L&iacute;nea: Pensada en personas autodidactas que desean superar distancias.";
	var copyPresencialVespertina = "(Para revalidar estudios previos y acudir al campus a partir de las 16:00 horas.)";
	
	$("#descModalidad").html( "" );

	switch (keyPlan) {
		case "plan_presencial":
			$("#descModalidad").html( copyPresencial );
			$("#descarga_folleto_mapa").attr( "href", descarga.plan_presencial );
		break;
		
		case "plan_ejecutiva":
			$("#descModalidad").html( copyEjecutivo );
			$("#descarga_folleto_mapa").attr( "href", descarga.plan_ejecutiva );
		break;

		case "plan_enlinea":
			$("#descModalidad").html( copyLinea );
			$("#descarga_folleto_mapa").attr( "href", descarga.plan_enlinea );
		break;

		case "plan_presencial_enlinea":
			$("#descModalidad").html( copyPresencialLinea );
			$("#descarga_folleto_mapa").attr( "href", descarga.plan_presencial_enlinea );
		break;

		case "plan_esbelta":
			$("#descModalidad").html(copyEjecutivoLinea);
			$("#descarga_folleto_mapa").attr("href", descarga.plan_ejecutiva);
		break;

		case "plan_presencial_vespertina":
			$("#descModalidad").html(copyPresencialVespertina);
			$("#descarga_folleto_mapa").attr("href", descarga.plan_presencial_vespertina);
		break;

		default:
			$("#descModalidad").html( copyPresencial );
			$("#descarga_folleto_mapa").attr( "href", descarga.plan_presencial );
		break;
	}


	$("#prodCampusDropdown .dropdown-item.destacar").addClass("d-none");
	$("#prodCampusDropdown .dropdown-item.destacar.plan-"+keyPlan).removeClass("d-none");

	$("#prodCampusDropdown .dropdown-item.destacar.plan-"+keyPlan+":first").click();
	$("#nombrePlan").text("Modalidad: " + nombrePlan);
}

function filtroNoAnos(tipo, noAnos, tipoPlan) {
	var actual = "duracion-" + $("#actual").val();
	var duracion = "duracion-" + tipo;
	var plan = "plan-" + tipoPlan;

	var ciclo = $("#ciclo").val();
	var nombreCiclo = ciclo.substring( 3, parseInt(ciclo.length) ) + 's';	
	var total_cuatrimestres = $("div." + duracion + "." + plan).length;

	var duracionTitle = $("#h_duracionTitle").val() + noAnos;
	var duracionTotal = $("#h_duracionOferta").val();

	if (duracionTotal == "" && duracion != null) {
		duracionTotal = total_cuatrimestres +' '+ nombreCiclo;
	}

	$("li a.nav-link").removeClass("active");

	switch (duracion) {
		case 'duracion-' + tipo:			
			$("#ciclos").html( duracionTitle );
			// $("#descModalidad").html( copyModlidad )
			$("#duracionPor").html(' ('+ duracionTotal +')');			
			$(".item-cuatrimestre").addClass("d-none").removeClass("active");
			$("li."+ duracion + "." + plan + " a").removeClass("active");

			$(".panel-materias").removeClass("active");
			$("."+ duracion + "." + plan).removeClass("d-none");
			$("li."+ duracion + "." + plan + " a:first").addClass("active");
			$("div." + duracion + "." + plan + ":first").addClass("show active");			
			$("#actual").val( tipo );			
		break;
	}	
}