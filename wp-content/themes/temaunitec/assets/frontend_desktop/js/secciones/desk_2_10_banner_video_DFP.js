$('.banner-share > a').on('click', function (e) {
    e.preventDefault() // prevent default action - hash doesn't appear in url
    $(this).parent().find('div').toggleClass('social-reveal-active');
    $(this).toggleClass('share-expanded');
});

var heightBannerVideo = jQuery(window).height();
jQuery(window).scroll(function(){
    if (jQuery(window).scrollTop() > heightBannerVideo-100) {
        jQuery("#myVideo").fadeOut();
        jQuery("#filter-blue").fadeOut();
    }
    else {
        jQuery("#myVideo").fadeIn();
        jQuery("#filter-blue").fadeIn();
    }
});