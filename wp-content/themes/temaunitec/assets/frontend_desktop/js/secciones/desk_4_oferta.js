var safeUrl = (/unitec.mx/.test(window.location.href)) ? "https://unitecmx-universidadtecno.netdna-ssl.com/" : "//" + document.domain;
$(document).ready(function(){

    $(".card-first:first").find("a").find("h5").addClass("activeOferta");
    $(".lista-nivel-academico:first").find("div").addClass("activeOferta");
    $(".card-first:first").find("a").removeClass("collapsed");

    // var getID = $(this).attr('id');
    // var countElements = $('#' + getID + ' .carrousel_inner_oferta > ul > li').length;
    // alert(getID + ' / ' + countElements);
    // alert( $('#tab-panel-oferta-1 .carrousel_inner_oferta > ul > li').length);
    setup_inicial(careers_json);
});

/*Resaltar cada Hijo Seleccionado*/
$(".lista-nivel-academico").on("click", function(){
  //alert($(this).attr('id') );
    var id_nivel = $(this).attr('id');
    //alert("Nivel: " + id_nivel);
    var parent_node  = $(this).attr('data-parentNode');

    $(".lista-nivel-academico").children().removeClass("activeOferta");
    $(this).find("div").addClass("activeOferta");
    // console.log(id_nivel);

    var actualIdPanel = $(".tab-pane.fade.in.active.show.tabs-oferta").attr('id');
    /*console.log("Panel Actual: " + actualIdPanel + " Padre Nodo: " + parent_node + " Hijo Nodo: " + id_nivel);*/
    cambiar_columnas(careers_json, $(this).attr("name"), actualIdPanel, parent_node, id_nivel);

});
/*Resaltar cada Hijo Seleccionado*/

/*Resaltar cada Padre Seleccionado*/
$(".card-first").on("click", function(){

  /*Extraemos Nombre del Padre (Económico Administrativos) By SRP 24-06-2019*/
  var id_nivel_first = $(this).next('div').find('.lista-nivel-academico').first().attr('id');
  var name_menu_first = $(this).next('div').find('.lista-nivel-academico').first().attr('name');
  var actualIdPanel_first = $(".tab-pane.fade.in.active.show.tabs-oferta").attr('id');
  var parent_node_first  = $(this).find("a").first().attr('title');
  /*End Obtenemos nombre del padre al que pertenece (Licenciaturas) By SRP 24-06-2019*/
  cambiar_columnas(careers_json, name_menu_first, actualIdPanel_first, parent_node_first, id_nivel_first);

  $(".card-first").find("h5").removeClass("activeOferta");
  $(this).find("a").find("h5").addClass("activeOferta");

  var secondCard = $(".card-second");

  $(secondCard).find('#' + id_nivel_first).children().addClass("activeOferta");

});
/*Resaltar cada Padre Seleccionado*/

function resaltar(id){
    var mitab = null;
    mitab = id;
    /*console.log(mitab);*/
    $("#" + mitab +" div.card-first").find("a").find("h5").removeClass("activeOferta");
    $("#" + mitab +" div.card-first:first").find("a").find("h5").addClass("activeOferta");
    $("#" + mitab +" div.lista-nivel-academico:first").find("div").addClass("activeOferta");
    $("#" + mitab +" div.card-first:first").find("a").removeClass("collapsed");
}

function setup_inicial(json_carreras) {
  careers_json = JSON.parse(json_carreras);
  var tabCount = 0;
  $('div[id^="tab-panel-oferta"]').each(function(i, obj) {
    var html = "";
    // console.log(obj.id);
    var nombre_nivel = $('#'+obj.id + ' #nameCareer').first().text();
    // alert(nombre_nivel);
    if(nombre_nivel == ""){
    // if(nombre_nivel == "Conoce Más"){
        var theCareer = [
                  {
                    "title":"Preparatoria",
                    "url":safeUrl+"/wp-content/uploads/2017/08/prepa-unitec-cluster-principal-h.jpg",
                    "alt":"Prepa en UNITEC Universidad de México, estudia tu Prepa en Unitec de una manera eficiente y estructurada, desarrollando tus habilidades de estudio",
                    "permalink":"https://www.unitec.mx/prepa/",
                    "description":"Con el respaldo educativo de más de 50 años de la Universidad Tecnológica de México, Prepa UNITEC implementa el modelo de Prepa Universitaria, brindando al estudiante las herramientas que requiere para asegurar su llegada a la universidad y una acertada elección de carrera."
                  }
              ];
    } else {
        var firstElement = $('#'+obj.id + ' #nameOffer').first().text();
        var theCareer = careers_json['tab-'+tabCount+'-pestanas'][0][firstElement]['areas'][0][nombre_nivel];
    }
    var tab = careers_json['tab-'+tabCount];
    var lvl1 = firstElement;
    var lvl2 = nombre_nivel;
    // console.log("Aqui esta el json");
    // console.log(careers_json['tab-'+tabCount+'-pestanas'][0]);
    // var theCareer = careers_json[nombre_nivel];
    // console.log(theCareer[0]);
    // console.warn(i);
    // console.warn(theCareer.length);
    theCareer.forEach(function(career, index) {
      // console.warn(i);
      // console.warn(theCareer.length);
      if(theCareer.length === 1){
        html += "<li class='col-12 p-0'>";
          html += "<div class='row d-flex justify-content-center m-0'>";
            html += "<div class='col-lg-12 col-xl-7 p-0 d-flex align-items-center justify-content-center'>";
              html += "<a data-gtm-tr='AcademicOffer' data-gtm-Tab='" + tab + "' data-gtm-offer-select='" + lvl1 + "' data-gtm-offer-option='" + lvl2 + "' data-gtm-carrera='" + career.title + "' title='Ver más' href='" + career.permalink + "'>";
                html += "<img src='" + career.url + "' class='img-fluid' alt='" + career.alt + "'>";
              html += "</a>";
            html += "</div>";
            html += "<div class='col-lg-12 col-xl-5 align-self-center p-2'>";
              html += "<div class='card-body card text-md-left p-3 card-left'>";
                html += "<h4 class='mb-1'><span class='filo-inicio-t'>|</span> "+ career.title +"</h4>";
                html += "<div class='card-unica-int'>" + career.description.substring(0, 300); + "</div>";
                html += "<div class='d-flex justify-content-end mt-1'>";
                  html += "<a data-gtm-tr='AcademicOffer' data-gtm-Tab='" + tab + "' data-gtm-offer-select='" + lvl1 + "' data-gtm-offer-option='" + lvl2 + "' data-gtm-carrera='" + career.title + "' title='Ver más' href='" + career.permalink + "' class='verMas pull-right'>VER MÁS</a>";
                html += "</div>";
              html += "</div>";
            html += "</div>";
          html += "</div>";
        html += "</li>";
      }else{
        if (theCareer.length === 2 && index == 0){
          var offset = 'offset-md-2';
        }else{
          var offset = '';
        }
        html += "<li class='col-lg-4 col-md-4 " + offset +" item' id='item-1'>";
          html += "<div class='altura-card card-wrapper'>";
            html += "<div id='card-carreras-de-interes-1' class='card-oferta card-rotating effect__click text-center h-100 w-100'>";
              html += "<div class='face front'>";
                html += "<a data-gtm-tr='AcademicOffer' data-gtm-Tab='"+tab+"' data-gtm-offer-select='"+lvl1+"' data-gtm-offer-option='"+lvl2+"' data-gtm-carrera='"+career.title+"' title='Ver más' href='" + career.permalink + "'>";
                  html += "<div class='cover'>";
                    html += "<img src='" + career.url + "' alt='" + career.alt + "'>";
                  html += "</div>";
                  html += "<div class='content'>";
                    html += "<div class='main'>";
                      html += "<div class='title'>" + career.title + "</div>";
                      if (career.description.length > 240) {
                        html += truncate(career.description, 40);
                      }else{
                        html += career.description;
                      }
                    html += "</div>";
                  html += "</div>";
                  html += "<div class='footer d-flex align-items-center'>";
                    html += "<div class='navegacion d-flex justify-content-end'>";
                      html += "<div class='navegacion d-flex justify-content-end'>";
                        html += "<a data-gtm-tr='AcademicOffer' data-gtm-Tab='"+tab+"' data-gtm-offer-select='"+lvl1+"' data-gtm-offer-option='"+lvl2+"' data-gtm-carrera='"+career.title+"' title='Ver más' href='" + career.permalink + "' class='verMas pull-right'>VER MÁS</a>";
                      html += "</div>";
                    html += "</div>";
                  html += "</div>";
                html += "</a>";
              html += "</div>";
            html += "</div>";
          html += "</div>";
        html += "</li>";
      }
    });
    //console.log(html);
    $("#" + obj.id + " #fillCareers li").remove();
    $("#" + obj.id + " #fillCareers").append(html);
    /*Checar cambios de width aqui */
    actualizaCarrusel(obj.id,1);
    tabCount++;
  });
}

function truncate(str, no_words) {
  return str.split(" ").splice(0, no_words).join(" ") + '...';
}

function cambiar_columnas(careers_json, level_name, panel_id, key_parent, id_nivel) {
    var html = "";
    //careers_json = JSON.parse(careers_json);
    // console.log(careers_json);
    // var theCareer = careers_json[level_name];
    // console.log(level_name);
    // console.log(panel_id);
    // console.log(key_parent);
    // console.log(id_nivel);
    var nombre_nivel = $('.card-menu-oferta #'+id_nivel + ' #nameCareer.activeOferta').text();
      // if(id_nivel == "Preparatoria-conoce-mas" || id_nivel == "Preparatoria"){
      if(id_nivel == "" || id_nivel == ""){
          var theCareer = [
                 {
                    "title":"Preparatoria",
                    "url":safeUrl+"/wp-content/uploads/2017/08/prepa-unitec-cluster-principal-h.jpg",
                    "alt":"Prepa en UNITEC Universidad de México, estudia tu Prepa en Unitec de una manera eficiente y estructurada, desarrollando tus habilidades de estudio",
                    "permalink":"https://www.unitec.mx/prepa/",
                    "description":"Con el respaldo educativo de más de 50 años de la Universidad Tecnológica de México, Prepa UNITEC implementa el modelo de Prepa Universitaria, brindando al estudiante las herramientas que requiere para asegurar su llegada a la universidad y una acertada elección de carrera."
                 }
              ];
          var tab = $(".filtros-oferta #tabNav .nav-item a.active").text();
          var lvl1 = $(".tabs-oferta.active .card-menu-oferta H5.activeOferta").text();
          var lvl2 = nombre_nivel;
      } else {
          var tabCount = panel_id.substr(panel_id.length - 1);
          console.log(careers_json['tab-'+tabCount+'-pestanas'][0][key_parent]);
          var theCareer = careers_json['tab-'+tabCount+'-pestanas'][0][key_parent]['areas'][0][id_nivel];
          var tab = careers_json['tab-'+tabCount];
          var lvl1 = key_parent;
          var lvl2 = level_name;
      }            

      theCareer.forEach(function(career, index) {
        if (theCareer.length === 1) {
          html += "<li class='col-12 p-0'>";
          html += "<div class='row d-flex justify-content-center m-0'>";
          html += "<div class='col-lg-12 col-xl-7 p-0 d-flex align-items-center justify-content-center'>";
          html += "<a data-gtm-tr='AcademicOffer' data-gtm-Tab='" + tab + "' data-gtm-offer-select='" + lvl1 + "' data-gtm-offer-option='" + lvl2 + "' data-gtm-carrera='" + career.title + "' title='Ver más' href='" + career.permalink + "'>";
          html += "<img src='" + career.url + "' class='img-fluid' alt='" + career.alt + "'>";
          html += "</a>";
          html += "</div>";
          html += "<div class='col-lg-12 col-xl-5 align-self-center p-2'>";
          html += "<div class='card-body card text-md-left p-3 card-left'>";
          html += "<h4 class='mb-1'><span class='filo-inicio-t'>|</span> " + career.title + "</h4>";
          html += "<div class='card-unica-int'>" + career.description.substring(0, 300); + "</div>";
          html += "<div class='d-flex justify-content-end mt-1'>";
          html += "<a data-gtm-tr='AcademicOffer' data-gtm-Tab='" + tab + "' data-gtm-offer-select='" + lvl1 + "' data-gtm-offer-option='" + lvl2 + "' data-gtm-carrera='" + career.title + "' title='Ver más' href='" + career.permalink + "' class='verMas pull-right'>VER MÁS</a>";
          html += "</div>";
          html += "</div>";
          html += "</div>";
          html += "</div>";
          html += "</li>";
        } else {
          if (theCareer.length === 2 && index == 0) {
            var offset = 'offset-md-2';
          } else {
            var offset = '';
          }
          html += "<li class='col-lg-4 col-md-4 " + offset+ " item' id='item-1'>";
          html += "<div class='altura-card card-wrapper'>";
          html += "<div id='card-carreras-de-interes-1' class='card-oferta card-rotating effect__click text-center h-100 w-100'>";
          html += "<div class='face front'>";
          html += "<a data-gtm-tr='AcademicOffer' data-gtm-Tab='" + tab + "' data-gtm-offer-select='" + lvl1 + "' data-gtm-offer-option='" + lvl2 + "' data-gtm-carrera='" + career.title + "' title='Ver más' href='" + career.permalink + "'>";
          html += "<div class='cover'>";
          html += "<img src='" + career.url + "' alt='" + career.alt + "'>";
          html += "</div>";
          html += "<div class='content'>";
          html += "<div class='main'>";
          html += "<div class='title'>" + career.title + "</div>";
          if (career.description.length > 240) {
            html += truncate(career.description, 40);
          } else {
            html += career.description;
          }
          html += "</div>";
          html += "</div>";
          html += "<div class='footer d-flex align-items-center'>";
          html += "<div class='navegacion d-flex justify-content-end'>";
          html += "<div class='navegacion d-flex justify-content-end'>";
          html += "<a data-gtm-tr='AcademicOffer' data-gtm-Tab='" + tab + "' data-gtm-offer-select='" + lvl1 + "' data-gtm-offer-option='" + lvl2 + "' data-gtm-carrera='" + career.title + "' title='Ver más' href='" + career.permalink + "' class='verMas pull-right'>VER MÁS</a>";
          html += "</div>";
          html += "</div>";
          html += "</div>";
          html += "</a>";
          html += "</div>";
          html += "</div>";
          html += "</div>";
          html += "</li>";
        }
      });
    // console.log(html);
    // console.log("#" + panel_id + " #fillCareers li");
    $("#" + panel_id + " #fillCareers li").remove();
    $("#" + panel_id + " #fillCareers").append(html);
    actualizaCarrusel(panel_id,0);
}

function actualizaCarrusel(id_panel,num_width) {
    var ventana_ancho_oferta = $(window).width();
    $('#' + id_panel + ' .carrousel_left_oferta').removeClass('left_inactive');
    $('#' + id_panel + ' .carrousel_right_oferta').removeClass('d-none');
    $('#' + id_panel + ' .carrousel_left_oferta').addClass('d-none');
    /*elementos_oferta a Mostrar Update By SRP*/
    var elementos_oferta = 3;
    $('#' + id_panel + ' .carrusel-oferta').attr('data-pos', 0);
    $('#' + id_panel + ' .carrousel_inner_oferta').scrollLeft(0);

    var elementsCount_oferta_inicial=$('#' + id_panel + ' .carrousel_inner_oferta > ul > li').length;
    if(elementsCount_oferta_inicial <= elementos_oferta) {
        $('#' + id_panel + ' .carrousel_right_oferta').addClass('d-none');
    }

    $('#' + id_panel + ' .carrousel_right_oferta').off('click').click(function(){
      var div_ancho_oferta = $('div[id ^= "tab-panel-oferta"].active .carrousel_inner_oferta > ul > li').width();
        var elementsCount_oferta=$('#' + id_panel + ' .carrousel_inner_oferta > ul > li').length;
        var position_oferta =$('#' + id_panel + ' .carrusel-oferta').attr('data-pos');
        position_oferta=parseInt(position_oferta, 10);
        // alert(position_oferta);
        if (position_oferta<elementsCount_oferta)
        {
            elementos_oferta = elementos_oferta + 3;
            position_oferta = position_oferta + 3;
            $('#' + id_panel + ' .carrousel_right_oferta').removeClass('right_inactive');
            $('#' + id_panel + ' .carrousel_left_oferta').removeClass('d-none');

          if ((position_oferta + 3) >= elementsCount_oferta || elementos_oferta == elementsCount_oferta ){
                $(this).addClass('left_inactive');
                $(this).addClass('d-none');
            }
            var pos_oferta = position_oferta*(div_ancho_oferta+30);
            $('#' + id_panel + ' .carrusel-oferta').attr('data-pos',position_oferta);
            $('#' + id_panel + ' .carrousel_inner_oferta').animate({'scrollLeft' : pos_oferta });
        }
    });

    $('#' + id_panel + ' .carrousel_left_oferta').off('click').click(function(){
      var div_ancho_oferta = $('div[id ^= "tab-panel-oferta"].active .carrousel_inner_oferta > ul > li').width();
        var position_oferta = $('#' + id_panel + ' .carrusel-oferta').attr('data-pos');
        var elementsCount_oferta = $('#' + id_panel + ' .carrousel_inner_oferta > ul > li').length;
        position_oferta=parseInt(position_oferta, 10);

        if (position_oferta > 0)
        {
            $('#' + id_panel + ' .carrousel_left_oferta').removeClass('left_inactive');
            $('#' + id_panel + ' .carrousel_right_oferta').removeClass('d-none');
            elementos_oferta = elementos_oferta - 3;
            position_oferta = position_oferta - 3;

            if(position_oferta == 0)
                {
                    $(this).addClass('right_inactive');
                    $(this).addClass('d-none');
                }
            var pos_oferta = position_oferta*(div_ancho_oferta+30);
            $('#' + id_panel + ' .carrusel-oferta').attr('data-pos',position_oferta);
            $('#' + id_panel + ' .carrousel_inner_oferta').animate({'scrollLeft' : pos_oferta });
        }
    });
}
