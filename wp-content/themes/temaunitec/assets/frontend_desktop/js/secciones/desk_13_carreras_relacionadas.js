
var div_ancho = $(".carrousel_inner_carreras ul li").width();
/*Elementos a Mostrar Update By SRP*/
var elementos_carreras = 3;

    if($(window).width() <= 991)
        {
            elementos_carreras = 1;
        };

$('.carrousel_right_carreras').click(function(){
    // var elementsCountCarreras = $('.carrousel_inner_carreras ul li').length;
    var elementsCountCarreras = $('#totalCarreras').val();
    var positionCarreras =$('.carrousel_carreras').attr('data-pos');
    positionCarreras = parseInt(positionCarreras, 10);

    if (positionCarreras < elementsCountCarreras)
    {
        elementos_carreras = elementos_carreras + 1;
        positionCarreras = positionCarreras+1;
        $('.carrousel_right_carreras').removeClass('right_inactive');
        $('.carrousel_left_carreras').removeClass('d-none');         

        /*console.log("Elemento Actual: " + elementos_carreras + " Total: " +elementsCountCarreras);*/   

        if ( positionCarreras == elementsCountCarreras || elementos_carreras == elementsCountCarreras ){
            $(this).addClass('left_inactive');
            $(this).addClass('d-none');
        }
        var posCarreras = positionCarreras*(div_ancho+30);
        $('.carrousel_carreras').attr('data-pos',positionCarreras);
        $('.carrousel_inner_carreras').animate({'scrollLeft' : posCarreras });
    }
});

$('.carrousel_left_carreras').click(function(){
    // var elementsCountCarreras = $('.carrousel_inner_carreras ul li').length;
    var elementsCountCarreras = $('#totalCarreras').val();
    var positionCarreras = $('.carrousel_carreras').attr('data-pos');
    positionCarreras=parseInt(positionCarreras, 10);

    if (positionCarreras > 0)
    {
        $('.carrousel_left_carreras').removeClass('left_inactive');
        $('.carrousel_right_carreras').removeClass('d-none');
        elementos_carreras = elementos_carreras - 1;
        positionCarreras = positionCarreras - 1;            

        /*console.log("Elemento Actual: " + elementos_carreras + " Total: " +elementsCountCarreras);*/

        if(positionCarreras == 0)
            {
                $(this).addClass('right_inactive');
                $(this).addClass('d-none');
            }
        var posCarreras = positionCarreras*(div_ancho+30);
        $('.carrousel_carreras').attr('data-pos',positionCarreras);
        $('.carrousel_inner_carreras').animate({'scrollLeft' : posCarreras });
    }
});