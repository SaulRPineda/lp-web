$(document).ready(function(){
    var ventana_ancho = $(window).width();
    var div_ancho = $(".carrousel_inner ul li").width();
    /*Elementos a Mostrar Update By SRP*/
    var elementos = 3;

    $('.carrousel_right').click(function(){
        var elementsCount=$('.carrousel_inner ul li').length;
        var position =$('.carrousel').attr('data-pos');
        position=parseInt(position, 10);

        if (position<elementsCount)
        {
            elementos = elementos + 1;
            position = position + 1;
            $('.carrousel_right').removeClass('right_inactive');
            $('.carrousel_left').removeClass('d-none');            

            if ( position == elementsCount || elementos == elementsCount ){
                $(this).addClass('left_inactive');
                $(this).addClass('d-none');
            }
            var pos = position*(div_ancho+30);
            $('.carrousel').attr('data-pos',position);
            $('.carrousel_inner').animate({'scrollLeft' : pos });
        }
    });

    $('.carrousel_left').click(function(){
        var position = $('.carrousel').attr('data-pos');
        var elementsCount = $('.carrousel_inner ul li').length;
        position=parseInt(position, 10);

        if (position > 0)
        {
            $('.carrousel_left').removeClass('left_inactive');
            $('.carrousel_right').removeClass('d-none');
            elementos = elementos - 1;
            position = position - 1;            

            if(position == 0)
                {
                    $(this).addClass('right_inactive');
                    $(this).addClass('d-none');
                }
            var pos = position*(div_ancho+30);
            $('.carrousel').attr('data-pos',position);
            $('.carrousel_inner').animate({'scrollLeft' : pos });
        }
    });


    $galeriaActiva = $("#galeria-activa").val();
    $galeriaName = $("#galeria-name").val();

    $(".portada-galeria").on("click", function(){
        /*Update 1.1 destaca el titulo al seleccionar una Galeria*/
        var id_galeria = $(this).attr('id');
        $(".title-galerias").removeAttr('style');
        $(".galeria-title-" + id_galeria).css({'font-weight': '500'});
        /*Update 1.1 destaca el titulo al seleccionar una Galeria*/

        ver_galeria(galery_json, $(this).attr("name"), $(this).attr("title"), insertar_imagenes); 
        
    });

});

$("#modal-tarjeta-galeria-carrusel").on('hidden.bs.modal', function () {

    if ( $("#h_tab-contiene-video").val() == "S" ) {
        switch( $("#h_video-activo").val() ) {
            case 'primero':
                try{
                    $('.iframe-galeria')[0].contentWindow.postMessage( '{"event":"command","func":"' + "stopVideo" + '","args":""}', "*");
                } catch (e) {
                    console.log(e);
                }                
            break;

            default:
                $( ".youtube" ).remove( $("h_video-activo").val() );
            break;
        }

        $("#h_tab-contiene-video").val("N");
        $("#h_video-activo").val("");
    }
    
});

function ver_galeria(galery_json, galeria, galeriaName, callbakImg){ 
    var inicio = 2;
    /*Ajuste para Version Desktop*/
    $("#galeria-activa").val(galeria);
    $("#galeria-name").val(galeriaName);
    $("#load_galery").html('');
    /*End Ajuste para Version Desktop*/

    $('#gallery-title').html( galeriaName );
    $("#slides-carusel").html('');
    $("#indicadores-carusel").html('');

    galery_json = JSON.parse(galery_json);
    galery_json[galeria].forEach(function(imagen, index) {
        if (index > 1) {
            $('<div />', { 
                'id': 'slide-'+index, 
                'class': 'carousel-item '+ $galeriaActiva +' preview-'+index
            }).appendTo('#slides-carusel');
        }

        if (index === 1) {$("#indicadores-carusel").css('display','none');} else{$("#indicadores-carusel").removeAttr('style');}

        if (index > 1) {
            $("#indicadores-carusel").append('<li data-target="#carousel-tarjeta" id="indicador-'+ index + '" data-slide-to="'+ index +'" class="preview-'+ index + '"></li>');
        }
    });

    if (callbakImg && typeof(callbakImg) === "function") {
        callbakImg(galery_json);
    }
    
    $("#indicador-" + inicio).addClass("active");
}

function insertar_imagenes(galery_json){
    var inicio = 2;
    $galeriaActiva = $("#galeria-activa").val();

    galery_json[$galeriaActiva].forEach(function(imagen, index) {
        video = imagen.description;
        video.toLowerCase();

        if (index == 1) {
            $("#portada-galeria").attr('src', imagen.urlthumbnail);
        }

        if (  video.indexOf("youtube") !== -1 ) {            

            $('#slide-'+index).prepend('<div class="col-1 div-yt-video-left"><i class="icon-u-ver-izquierda"></i></div>');

            $('<div />', {
                'id': 'div-video-'+index,
                'class':'col-10 div-yt-video'
            }).appendTo('#slide-'+index);

            switch (index) {
                case 1:
                    $('<iframe />', {
                    'id': 'video-youtube-'+index,
                    'class': 'iframe-galeria',
                    'controls': 'controls',
                    'x-webkit-airplay': 'allow',
                    'allowscriptaccess': 'always',
                    'scrolling': 'no',
                    'src': video + "?enablejsapi=1&rel=0&showinfo=0&autoplay=1",
                    'frameborder': '0',
                    'allowfullscreen': 'yes'
                }).appendTo('#div-video-'+index);

                $("#h_video-activo").val('primero');
                setTimeout(function(){ $('#video-youtube-'+index)[0].contentWindow.postMessage( '{"event":"command","func":"' + "stopVideo" + '","args":""}', "*"); }, 2000);
                break;

                default:
                    $('<div id="video-youtube-' + index + '" class="wrapper youtube" data-embed="' + video + '" title="iframe-galeria"><div class="play-button"></div></div>').appendTo('#div-video-'+index);
                break;
            }

            $('#slide-'+index).append('<div class="col-1 div-yt-video-right"><i class="icon-u-ver-derecha"></i></div>');

            /*Bandera que define si el testimonio Contiene Video*/
            $("#h_video-activo").val('primero');
            $("#h_tab-contiene-video").val("S");
            /*Bandera que define si el testimonio Contiene Video*/

        } else if(video.indexOf("videos") !== -1){
            /*<i class="icon-u-ver-izquierda"></i>*/
            $('#slide-'+index).prepend('<div class="col-1 div-yt-video-left"><i class="icon-u-ver-izquierda"></i></div>');

            $('<div />', {
                'id': 'div-video-ext'+index,
                'class':'col-10 div-yt-video'
            }).appendTo('#slide-'+index);

            $('<video />', {
                'id': 'video-full-'+index,
                'controls': 'yes'
            }).appendTo('#div-video-ext'+index);

            $('<source />', {
                'id': 'video-source-full'+index,
                'src': video,
                /*'type': 'video/mp4',*/
                'type': 'video/webm',
            }).appendTo('#video-full-'+index);

            $('#slide-'+index).append('<div class="col-1 div-yt-video-right"><i class="icon-u-ver-derecha"></i></div>');
            /*<i class="icon-u-ver-derecha"></i>*/

        } else{
            
                if (index > 1) {

                    $('<div />', {
                            'id': 'cont-modal-' + index,
                            'class':'cont-modal',
                            'style': 'background-image: url('+imagen.url+')',
                            'alt':imagen.alt
                    }).appendTo('#slide-'+index);

                    var pieImagen = imagen.caption;

                    if ( pieImagen.length > 4 ) {
                        $('<div />', {
                                'id': 'pie-modal-' + index,
                                'class':'right'
                        }).appendTo('#cont-modal-' + index);


                        $('<div />',{
                            'id': 'footer-content-' + index,
                            'class': 'modal-footer text-left',
                        }).appendTo('#pie-modal-' + index);

                        $('#footer-content-'+index).append('<p><label class="titleGalerias">'+ $("#galeria-name").val() + '</label><br />' + imagen.caption+'</p>');
                    }
                }
           
        }
    });

    // youtubeLazy();
    $('#slide-'+inicio).addClass("active");
    openModal("#modal-tarjeta-galeria-carrusel");    
}


function abreGaleria() {
        jQuery('html').addClass('scroll-hiden');
}
function cierraGaleria() {
    jQuery('html').removeClass('scroll-hiden');
    $('#modal-tarjeta-galeria-carrusel').modal('hide');
}






