var pasoFinalCalculadora = false;
var regresoAnterior = true;

//Triggers
jQuery(document).ready(function () {

    $('#calculadora-css').prop("disabled", true);

    render_lineas_negocio();

    //Traer informacion json
    getJsonCarreras();
    getJsonLinksCategorias();
    //INICIALIZACION DE JSONS
    // render_lineas_negocio();

    jQuery('#project').focus(function () {
        jQuery(this).val("");
        checkAllInputs();
        on();
    });

    jQuery('body').on('click','.sendCalculadoraModal',function(){
        appendCookie('c_form_data', 'urlreferrer', window.location, 1);
        sendFormularioTradicional(false);
        jQuery("#calculadoraModal").removeClass("sendCalculadoraModal");
    });

    /********BOTONES SELECT MODALIDAD Y CAMPUS********/
    jQuery('select').not('.form-control').not('#render_padre_tutor').on('change', function () {
        console.log('Guardando modalidad');
        console.log(jQuery('#render_modalidad option:selected').val());

        //sendDatalayer('calculadora/sec-modalidad', 'StepCalculator');

        var dataItem = jQuery('#render_modalidad option:selected').data();
        console.log(dataItem);
        guardaUsuario('modalidad', dataItem);

        jQuery('.rs-mod').text('Modalidad ' + jQuery('#render_modalidad option:selected').text());

        /*Guardar en la cookie la campus By SRP 08-10-2018*/
        appendCookie('c_form_data', 'campus', jQuery('#render_campus option:selected').val(), 1);

        guardaUsuario('campus', jQuery('#render_campus option:selected').val());
        //sendDatalayer('calculadora/sec-campus', 'StepCalculator');
        
        /*Modalidad 5 PREPA UNAM Se muestra Inicio de Clases Anuak By SRP 19-09-2019*/
        if ( jQuery('#render_modalidad').val() == 5 ) {
            jQuery("#alert-prepaunam").removeClass('d-none');
        } else{
            jQuery("#alert-prepaunam").addClass('d-none');
        }
        /*End Modalidad 5 PREPA UNAM Se muestra Inicio de Clases Anuak By SRP 19-09-2019*/

        render_selects();
        /* Cuando campus y modalidad tienen valores */
        if (jQuery('#render_campus').val() != 0 && jQuery('#render_modalidad').val() != 0) {
            /* Cuando es modalidad en linea y campus en linea */
            if (jQuery('#render_campus').val() == "ONL" && jQuery('#render_modalidad').val() != 3) {
                on();
            } else {
                off();
            }
            regresoAnterior = false;
            /* Avance automático hacia promedio */
            // muestraPaso('modalidad', 'promedio');
            $('.steps').text('6');
            if (jQuery('#render_campus').attr('data-preselected')) {

                /*Guardar en la cookie la campus By SRP 08-10-2018*/
                appendCookie('c_form_data', 'campus', jQuery('#render_campus').attr('data-preselected'), 1);

                guardaUsuario('campus', jQuery('#render_campus').attr('data-preselected'));
            } else {

                /*Guardar en la cookie la campus By SRP 08-10-2018*/
                appendCookie('c_form_data', 'campus', jQuery('#render_campus').val(), 1);

                guardaUsuario('campus', jQuery('#render_campus').val());
            }
            jQuery('.rs-campus').text('Campus ' + jQuery('#render_campus option:selected').text());

            console.log("PRUEBA DE ENVÍO");

            var checkOverlaySend = $("#overlay").attr("data-send");
            if (!jQuery('body').hasClass('inicio')) {
                if (checkOverlaySend == "1") {
                    $("#overlay").attr("data-send", "2");
                    // envioProcWeb();
                } else if (checkOverlaySend == "2") {
                    $("#overlay").attr("data-send", "2");
                    // envioProcWeb();
                } else {
                    $("#overlay").attr("data-send", "1");
                }
            }



            //guardaUsuario('linea', dataItem);  
        }

        /* Auto seleccion cuando se selecciona un campus online */
        if (jQuery('#render_modalidad').val() == 0 && jQuery('#render_campus').val() == "ONL") {
            jQuery('#render_modalidad').next().next().children("div:contains('En línea')").click();
        }

        if (jQuery('#render_modalidad').val() != 0) {
            var dataItem = jQuery('#render_modalidad option:selected').data();
            guardaUsuario('modalidad', dataItem);

            /*Guardar en la cookie de Nivel de Interes By SRP 08-10-2018*/
            //appendCookie('c_form_data', 'modalidad', dataItem, 1);


            var nivelAnterior = getNivelAnterior(getCampo('modalidad').interes);
            jQuery('.rs-canterior').text(nivelAnterior);
            // calcular_uno();
        }

    });

    /*Solicitar recurso de Json al momento de que el usuario de click al formulario By SRP 16-05-2019*/
    // jQuery('#calculadoraModal').click(function(){
    //     getJsonCarreras();
    //     getJsonLinksCategorias();
    // });

    $('#modal_frm_app').on('shown.bs.modal', function () {
        getJsonCarreras();
        getJsonLinksCategorias();
    }); 
    /*End Solicitar recurso de Json al momento de que el usuario de click al formulario By SRP 16-05-2019*/

    jQuery('#render_padre_tutor').on('change', function () {
        appendCookie('c_form_data', 'aPaternoTutor', jQuery('#render_padre_tutor').val(), 1);
        checkAllInputs();
    });

    jQuery('.reset').on('click', function () {
        render_modalidades(getModalidades(getCampo('carrera').IdCategoria));
        on();
    });

    jQuery('#render_campus').on('change', function () {
        var a = getModalidades(getCampo("carrera").IdCategoria); // Obtengo de nuevo todas las carreras para filtrar por campus 
        var html = "";
        var campus_select = jQuery('#render_campus').val();
        var modalidad_select = jQuery('#render_modalidad').val();

        var modalidadesSize = a.length;

        if (jQuery('#render_campus').val() != 0) {   //Si Cambia el campus
            html += '<option value="0">Modalidad</option>';
            console.log('cambio campus');
            console.log('Campus Elegido:' + campus_select);
            jQuery.each(a, function (index, value) {
                console.log(a);
                console.log('My array has at position ' + index + ', this value: ' + value.campus);
                console.log(value);
                var c = value.campus.toString();

                if (c.search(campus_select) != -1) {
                    console.log('My array has at position ' + index + ', this value: ' + value.modalidad + modalidadtxt(value.modalidad));
                    html += '<option data-Grupo_carreras="' + value.Grupo_carreras + '" data-IdDynamics="' + value.IdDynamics + '" data-campus="' + value.campus + '"  data-catCosto="' + value.catCosto + '" data-interes="' + value.interes + '" data-lineaweb="' + value.lineaweb + '" data-modalidad="' + value.modalidad + '"  data-nombre="' + value.nombre + '" value="' + value.modalidad + '">' + modalidadtxt(value.modalidad) + '</option>';
                };
            });
            jQuery('#render_modalidad').html(html).delay(500).queue(function (next) {
                if (modalidadesSize == 1) {
                    jQuery('#render_modalidad').next().addClass('inactive');
                } else if (modalidadesSize > 1) {
                    jQuery('#render_modalidad').next().removeClass('inactive');
                }
                next();
            });
            jQuery('#render_modalidad option[value="' + modalidad_select + '"]').attr('selected', 'selected');


        }
    });



    jQuery('#render_modalidad').on('change', function () {
        var a = getModalidades(getCampo("carrera").IdCategoria); // Obtengo de nuevo todas las carreras para filtrar por campus 
        var html = "";
        var campus_select = jQuery('#render_campus').val();
        var modalidad_select = jQuery('#render_modalidad').val();
        var sizeCampus = 0;
        var campusValue = "";

        if (jQuery('#render_modalidad').val() != 0) { //si cambia modalidad pinta campus 
            var campus = [];
            var html2 = "";
            jQuery.each(a, function (index, value) {
                console.log(value.modalidad + " Busca modalidad:" + modalidad_select);
                if (value.modalidad == modalidad_select) {
                    console.log('Encontro');
                    console.log(value);
                    console.log("campus encontrados");
                    var ArrCamp = value.campus.toString().split(",");
                    sizeCampus = ArrCamp.length;
                    if (ArrCamp.length == 1) {
                        jQuery('#render_campus').attr('data-preselected', ArrCamp[0]);
                        html2 += '<option selected value="' + ArrCamp[0] + '">' + campustxt(ArrCamp[0]) + '</option>';
                    } else {
                        html2 += '<option value="0">Campus</option>';
                        jQuery('#render_campus').removeAttr('data-preselected');
                        jQuery.each(ArrCamp, function (index1, value1) {
                            console.log(value1);
                            html2 += '<option value="' + value1 + '">' + campustxt(value1) + '</option>';
                        });
                    }
                }
            }); //Guarda los arreglos de campus

            jQuery('#render_campus').html(html2).delay(200).queue(function (next) {
               
                console.log("El valor de los campus es de: " + sizeCampus);
                /* Cuando solo hay un campus para autoseleccionarse */
                if (sizeCampus == 1) {
                    jQuery('#render_campus').next().addClass('inactive');
                    /*Avance automático hacia promedio */
                    // muestraPaso('modalidad', 'promedio');
                    regresoAnterior = false;
                    off();
                    // calcular();
                    if (jQuery('#render_campus').attr('data-preselected')) {

                        /*Guardar en la cookie el campus By SRP 08-10-2018*/
                        appendCookie('c_form_data', 'campus', jQuery('#render_campus').attr('data-preselected'), 1);

                        guardaUsuario('campus', jQuery('#render_campus').attr('data-preselected'));
                    } else {

                        guardaUsuario('campus', jQuery('#render_campus').val());
                    }
                    // guardaUsuario('campus', jQuery('#render_campus').attr('data-preselected'));
                    var checkOverlaySend = $("#overlay").attr("data-send");
                    if (!jQuery('inicio').hasClass('inicio')) {
                        if (checkOverlaySend == "1") {
                            $("#overlay").attr("data-send", "2");
                            // envioProcWeb();
                        } else if (checkOverlaySend == "2") {
                            $("#overlay").attr("data-send", "2");
                            // envioProcWeb();
                        } else {
                            $("#overlay").attr("data-send", "1");
                        }
                    }

                } else {
                    jQuery('#render_campus').next().removeClass('inactive');
                }
                next();
            });

            jQuery('#render_campus option[value="' + campus_select + '"]').attr('selected', 'selected');


        }

    });

    /* Trigger (Linea de Negocio)
    * 
    */
    jQuery(".card.card-calculadora").on("mouseup", function () {
        //sendDatalayer('calculadora/sec-categoriaclik', 'StepCalculator');
        var dataItem = jQuery(this).find("div").data();       //Obtiene todo el data del primer div 
        // console.log("EL DATA ITEM ES: ");
        // console.log(dataItem);
        select(".card", this);                               //this=card-body 
        guardaUsuario('linea', dataItem);                   //guarda en LocalStorage

        /*Copies Personalizables By SRP 03-06-2019*/
        var getCopy = copyPersonalizable( getCampo('linea').id_name );
        jQuery('#subtituloInteres').html( getCopy );

        render_selects();
        jQuery('.select-items').empty();
        jQuery('#render_modalidad option, #render_campus option').empty();   
        jQuery('.select-selected').html('Selecciona');
        /*End Copies Personalizables By SRP 03-06-2019*/

        /*Guardar en la cookie la linea By SRP 08-10-2018*/
        // setTimeout(function() {
        //     appendCookie('c_form_data', 'linea', dataItem.id_name, 1);
        // }, 400);
        
        var nivelAnterior = getNivelAnteriorPorCard(getCampo('linea').id_name);
        jQuery('.rs-canterior').text(nivelAnterior);
        var a = getCarrerasBien(getCarreras(dataItem.id_name));     //Solicita las carreras y las pinta en el select  
        // changeVideo(dataItem.id_name.toLowerCase());
        console.log("HACIENDO RENDER DE CARRERAS");
        console.log(a);
        render_carreras(a);

        /*By SRP 03-06-2019*/
        //alert("Mostrar Modalidad y Campus");
        /*En #project se guarda en nombre de la carrera pra filtrar sus modalidades y campus*/

        /*Funcionalidad para mostrar Paso búsqueda de Carreras*/
        if (dataItem.id_name != "PREPARATORIA") {
            // muestraPaso('business-line', 'carrera');             //Se va al siguiente paso
            muestraPaso('business-line', 'container_carrera_modalidad');             //Se va al siguiente paso
            $('.steps').text('4');
            regresaPaso('container_carrera_modalidad', 'business-line');
        }
        /*Funcionalidad para mostrar Paso búsqueda de Carreras*/

        if (jQuery('body').hasClass('inicio') == false) {
            on();
            jQuery('.reset').click();
            jQuery('#project').val('');
            // console.log("EL VALOR DE PROJECT ES: " +  jQuery('#project').val());
            jQuery('#project').focus();
            if (dataItem.id_name == "PREPARATORIA") {
                jQuery('#project').val("Preparatoria");
                // console.log("LA SELECCION ES PREPA: ");
                // console.log( $('.ui-menu-item div').first());
                // $('.ui-menu-item div').first().click();
            }
            checkAllInputs();
        } else {
            if (dataItem.id_name == "PREPARATORIA") {
                jQuery('#project').val("Preparatoria");
            }

            /****IMPORTANTE CAMBIAR AL SUBIR PARA DISPARAR DATALAYER EN ANALITICS****/
            if(paso3){
                var objCookie = JSON.parse(decodeURIComponent(getCookie('c_form_data')));
                var objectDataLayer = {
                    'event' : 'InformationForm',   //Static data
                    'formStep' : 'Paso 3',   //Dynamic data
                    'Estado' : (autocompleteDesktop) ? 'definir' : objCookie.estado.toLowerCase(),   //CD4
                    'InterLvl' : getUsuario().linea.name.toLowerCase(),  //CD 5
                    'Period' : '',  //CD 6
                    'carrera' : '',  //CD 7
                    'campus' : '',  //CD 8
                    'Formulario' : 'Registro Formulario',  //CD 16
                    'RMode': '',   //CD25
                    'prom': '',   //CD26
                    'formDev' : 'Desktop'   // Dynamic data
                };

                if(!autocompleteDesktop){
                    dataLayer.push(objectDataLayer);
                } else {
                    dataLayerAutoComplete.push(objectDataLayer);
                }
                
            }

            paso3 = false; //Desactivación para otras consultas posteriores
        };
    });

    /*******ESQUEMA DE PAGOS CONTADO O MENSUAL********/
    jQuery("body").on("click", ".btn-dual-y", function () {
        select(".btn-dual-y", this);
        jQuery('.rs-tipopago').text(jQuery(this).text());
        if (jQuery(this).text().toLowerCase() == 'mensual') {
            jQuery('.rs-pg-final').text("$" + getCampo('mensual').format());
            jQuery('.rs-ahorro-mns').text("$" + getCampo('mensualAhorro').format());
        } else {
            jQuery('.rs-pg-final').text("$" + getCampo('contado').format());
            jQuery('.rs-ahorro-mns').text("$" + getCampo('contadoAhorro').format());

        }
    });

    /*******INPUT DE BUSQUEDA DE CARRERAS********/
    jQuery('#project').on("focus", function () {
        $('#project').autocomplete('search', $('#project').val())
    });

});

$(window).on('load', function () {
    /*Cookie UTM_CAMPAINGN BY SRP 29-10-2018*/
    var cookieBanner = getParameterByName('utm_campaign');
    /*Validar si existe utm_campaign*/
    if( getParameterByName('utm_campaign') !== false ) {

        if( checkCookie('banner_activo') === true ){
            //lo asigo a la cookie form_data
            appendCookie('c_form_data', 'banner', getCookie('banner_activo'), 1);
        } else{
            //no existe se da de alta en banner activo
            setCookieForm("banner_activo", cookieBanner, 1);
        }
    }
    /*Si no existe*/
    else {
        if( checkCookie('banner_activo') === true ){
            //lo asigo a la cookie form_data
            appendCookie('c_form_data', 'banner', getCookie('banner_activo'), 1);
        } else{
            if (location.href.indexOf("testvocacional") !== -1 || location.href.indexOf("orientacion-profesional") !== -1) {
                cookieBanner = "TESTVOCACIONAL";
            } else if (location.href.indexOf("impulsa") !== -1) {
                cookieBanner = "IMPULSA";
            } else {
                cookieBanner = "ASPIRANTES LIC";
            }
            appendCookie('c_form_data', 'banner', cookieBanner, 1);
        }
    }
    /*End Cookie UTM_CAMPAINGN BY SRP 29-10-2018*/
});