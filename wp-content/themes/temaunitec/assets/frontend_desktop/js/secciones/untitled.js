function setCookie(cname, cvalue, exdays) {
    var d = new Date();
    d.setTime(d.getTime() + (exdays * 24 * 60 * 60 * 1000));
    var expires = "expires=" + d.toUTCString();
    document.cookie = cname + "=" + cvalue + "; " + expires;
}

function getCookie(cname) {
    var name = cname + "=";
    var ca = document.cookie.split(';');
    for (var i = 0; i < ca.length; i++) {
        var c = ca[i];
        while (c.charAt(0) == ' ') c = c.substring(1);
        if (c.indexOf(name) == 0) return c.substring(name.length, c.length);
    }
    return "";
}

function checkCookie(valor) {
    var user = getCookie(valor);
    if (user != "") {
        return true;
    }
    else {
        return false;
    }
}

function sendDatalayer(url_val, event) {
    if (checkCookie('frmunitec') != true) {
        dataLayer.push({
            'url_value': url_val, //dato dinámico
            'event': event//dato estático
        });
    }
}

function quitaAcentos(str) {
    for (var i = 0; i < str.length; i++) {
        //Sustituye "á é í ó ú"
        if (str.charAt(i) == "á") str = str.replace(/á/, "a");
        if (str.charAt(i) == "é") str = str.replace(/é/, "e");
        if (str.charAt(i) == "í") str = str.replace(/í/, "i");
        if (str.charAt(i) == "ó") str = str.replace(/ó/, "o");
        if (str.charAt(i) == "ú") str = str.replace(/ú/, "u");
    }
    return str;
}

function blinkFieldForm(id) {
    jQuery("#" + id).addClass("blink-animation");
    setTimeout(function () { jQuery("#" + id).removeClass("blink-animation"); }, 2500);
}

function clickSeeDown() {
    $('#project').focus();
}

function getRelation(carrera_string) {
    switch (carrera_string) {
        case "preparatoria":
            return "prepa";
            break;
        case "licenciatura":
            return "licenciaturas_ingenierias";
            break;
        case "ingenieria":
            return "licenciaturas_ingenierias";
            break;
        case "enfermeria":
            return "enfermeria";
            break;
        case "enfermería":
            return "enfermeria";
            break;
        case "fisioterapia":
            return "fisioterapia";
            break;
        case "nutrición":
            return "nutricion";
            break;
        case "nutricion":
            return "nutricion";
            break;
        case "odontología":
            return "odontologia";
            break;
        case "odontologia":
            return "odontologia";
            break;
        case "salud":
            return "odontologia";
            break;
        case "gastronomía":
            return "gastronomia";
            break;
        case "gastronomia":
            return "gastronomia";
            break;
        case "turismo":
            return "turismo";
            break;
        case "posgrado":
            return "posgrado";
            break;
    }
}

function muestraPaso(actual, seccion) {

    if (jQuery('body').hasClass('inicio')) {                              //Verifica que este extendido el formulario

        var linea_seleccionada = getUsuario().linea.id_name;
        console.log(linea_seleccionada);

        jQuery('.quest.p-activa').addClass('an-slideUp');               //Agrega SlideUp a la Quest Activa

        setTimeout(function () {
            jQuery('.quest.p-activa').addClass('hide');                 //Esconde la preg Activa 
            jQuery('#' + seccion).removeClass('hide').addClass('p-activa').addClass('an-slideDownToUp');
            jQuery('#' + seccion).removeClass('an-slideUp').removeClass('an-slideDownToUp');

        }, 400);

        setTimeout(function () {
            console.log("Estmoas en:" + actual + "Se va a ir a:" + seccion);
            jQuery('#' + actual).removeClass('an-slideUp');
        }, 400);

        switch (seccion) {
            case "container_carrera_modalidad":
                regresaPaso('container_carrera_modalidad', 'business-line');
                break;
        }

    }

}

function checkOffset() {
    if ($('#selector-float').offset().top + $('#selector-float').height() >= $('#footer').offset().top)
        $('#selector-float').css('position', 'absolute');
    if ($(document).scrollTop() + window.innerHeight < $('#footer').offset().top)
        $('#selector-float').css('position', 'fixed'); // restore when you scroll up
    // $('.px').text($(document).scrollTop() + window.innerHeight);
}

function finalizaBoton() {
    var checkRangeSend = $("#range").attr("data-send");
    if (checkRangeSend == "1") {
        $("#range").attr("data-send", "2");
    } else if (checkRangeSend == "2") {
        $("#range").attr("data-send", "2");
    } else {
        $("#range").attr("data-send", "1");
    }
    // calcular_uno();
    finaliza();
};

function regresaPaso(actual, movingTo) {
    if (jQuery('body').hasClass('inicio')) {
        if (actual == 'promedio' && regresoAnterior == false) {
            console.log('Omito todo');

            /* Se implementa el regreso a carrera */
            jQuery('.controls').removeClass('d-none');
            jQuery('.controls a').unbind();
            jQuery('.controls a').on('click', function () {
                muestraPaso('promedio', 'container_carrera_modalidad');
            });
            $('.steps').text('6');
        } else {
            console.log("Moviendo a " + movingTo);
            switch (movingTo) {
                case "business-line":
                    jQuery('.controls').removeClass('d-none');
                    $('.steps').text('4');
                    jQuery('.controls a').unbind();
                    jQuery('.controls a').on('click', function () {
                        muestraPaso(actual, movingTo);
                        jQuery('.controls').addClass('d-none');
                        jQuery('#project').val("");
                        jQuery("#carrera").removeClass("hide");
                        jQuery("#modalidad").addClass("hide");
                        jQuery("#padre_tutor").addClass("hide");
                    });
                    break;
                case "carrera":
                    jQuery('.controls').removeClass('d-none');
                    jQuery('.controls a').unbind();
                    jQuery('.controls a').on('click', function () {
                        muestraPaso(actual, movingTo);
                    });
                    break;
                default:
                    jQuery('.controls').removeClass('d-none');
                    jQuery('.controls a').unbind();
                    jQuery('.controls a').on('click', function () {
                        muestraPaso(actual, movingTo);
                    });
                    break;
            }
        }
    }


}


function select(clase, el) {
    $(clase).each(function () { $(this).removeClass('selected') });
    //Si no se especifica la clase de btn-dual-y causa conflictos
    if (!pasoFinalCalculadora && clase != ".btn-dual-y") {
        setTimeout(function () { $(el).addClass('selected'); }, 500);
    } else {
        $(el).addClass('selected');
    }


}

function on() {
    //document.getElementById("overlay").style.display = "block";
    jQuery('#results').addClass('blur');
    jQuery('#overlay').fadeIn();
}

function off() {
    //document.getElementById("overlay").style.display = "none";
    jQuery('#results').removeClass('blur');
    jQuery('#overlay').fadeOut();
}

function checkAllInputs() {
    var faltantes = 0;
    /* Controla todos los inputs que faltan */
    if (jQuery('#project').val() != "") {
        jQuery('#calc-apply-career').hide();
        jQuery('#project').removeClass('missing-select');
    } else {
        jQuery('#calc-apply-career').show();
        jQuery('#project').addClass('missing-select');
        on();
        faltantes++;
    }

    if (jQuery('#render_campus').val() != 0) {
        jQuery('#calc-apply-campus').hide();
        jQuery('#render_campus').next().removeClass('missing-select');
        jQuery('.rs-campus').text('Campus ' + jQuery('#render_campus option:selected').text());
    } else {
        jQuery('#calc-apply-campus').show();
        jQuery('#render_campus').next().addClass('missing-select');
        on();
        faltantes++;
    }

    if (jQuery('#render_modalidad').val() != 0) {
        jQuery('#calc-apply-type').hide();
        jQuery('#render_modalidad').next().removeClass('missing-select');
        jQuery('.rs-mod').text('Modalidad ' + jQuery('#render_modalidad option:selected').text());
    } else {
        jQuery('#calc-apply-type').show();
        jQuery('#render_modalidad').next().addClass('missing-select');
        on();
        faltantes++;
    }

    var linea_seleccionada = getUsuario().linea.id_name;
    if (linea_seleccionada == "PREPARATORIA") {
        if (jQuery('#render_padre_tutor').val() != "Selecciona") {
            jQuery('#calc-apply-padre').hide();
            jQuery('#render_padre_tutor').next().removeClass('missing-select');
        } else {
            jQuery('#calc-apply-padre').show();
            jQuery('#render_padre_tutor').next().addClass('missing-select');
            on();
            faltantes++;
        }
    }else{
        jQuery('#calc-apply-padre').hide();
    }
    

    if (faltantes == 0 && jQuery('body').hasClass('inicio')) {
        if(getUsuario().modalidad != undefined){
            if(getUsuario().modalidad.catcosto != "DENT"){
                jQuery('.btn-dual').last().click();
            }
        }
    }

    if(faltantes == 0){
        if(getUsuario().modalidad != undefined){
            if(getUsuario().modalidad.catcosto == "DENT"){
                console.log("DANDO CLICK EN LAS 5 MATERIAS");
                jQuery('.btn-circ').eq(4).click();
            }
        }
    }
}

function cerrarModalCalculadora() {
    $('#calculadora-css').attr('disabled', 'disabled');
    $('#calculadora-css').prop("disabled", true);
    $('#bootstrapcss-css').removeAttr('disabled');
    $('#mdb-css').removeAttr('disabled');
    $('#bootstrapcss-css').prop('disabled', false);
    $('#mdb-css').prop('disabled', false);
    $("#aplicativo-formulario").hide();
    $(".todoElBody").show();

    if (yCalculadora != 0) {
        console.log("Cambiando scroll en y " + yCalculadora);
        setTimeout(function (xCalculadora, yCalculadora) { window.scrollTo(xCalculadora, yCalculadora); }, 700, xCalculadora, yCalculadora);
    }
}



/* SEGUNDA PARTE JS */




function expandControls(){
    if(jQuery(".more-controls").attr('data-expand') == 0){
        /* Funciones cuando se cierran los controles */
        // jQuery("#selector-float").css('left', function(){ return $(this).offset().left; })
        // .animate({"left":"-40%"}, {duration: "slow",queue: "selector-animation", complete: function(){
        //     jQuery("#video_background").addClass('adjust-large-video');
        //     jQuery(".more-controls").attr('data-expand',"1");
        //     alert("TERMINO");
        // }}).dequeue('selector-animation');
        // jQuery("#results").animate({width:'95%'}, {duration:'slow', queue: "selector-animation"}).dequeue('selector-animation');
        jQuery("#video_background").addClass('adjust-large-video');
        jQuery(".overlay-controls").removeClass("d-none");
        jQuery(".more-controls").attr('data-expand',"1");
        jQuery("#selector-float").css('left', function(){ return $(this).offset().left; })
        .animate({"left":"-40%"}, {duration: "slow",queue: false, complete: function(){ 
        }});
        jQuery("#results").animate({width:'95%'}, {duration:'slow', queue: false});
        jQuery(".more-controls").animate({"right":"93.6%"}, {duration: "slow",queue: false, complete: function(){ 
            jQuery(".plus-controls").removeClass("minus"); 
        }});
        // jQuery("#selector-float").animate({ width: '0%' }, function () {
        //     jQuery("#results").css('width','100%');
        //     jQuery("#video_background").addClass('adjust-large-video');
        //     jQuery(".more-controls").attr('data-expand',"1");
        // });
        jQuery("#video-beca").addClass('cont-video-beca-open');
        jQuery("#video-beca").removeClass('cont-video-beca-close');
        jQuery("#calcu-left").addClass('cont-left-open');
        jQuery("#calcu-left").removeClass('cont-left-close');
        jQuery("#calcu-right").addClass('cont-right-open');
        jQuery("#calcu-right").removeClass('cont-right-close');
        jQuery("#reset").addClass('reset-2');
        jQuery("#reset").removeClass('reset');
    }else{
        /* Funciones cuando se abren los controles */
        jQuery(".overlay-controls").addClass("d-none");
        jQuery("#video_background").removeClass('adjust-large-video');
        jQuery(".more-controls").attr('data-expand',"0");
        jQuery("#selector-float").css('left', function(){ return $(this).offset().left; })
        .animate({"left":"0%"}, {duration: "slow",queue: false, complete: function(){ 
        }});
        jQuery("#results").animate({width:'55%'}, {duration:'slow', queue: false});
        jQuery(".more-controls").animate({"right":"53.5%"}, {duration: "slow",queue: false, complete: function(){
            jQuery(".plus-controls").addClass("minus"); 
        }});
        jQuery("#video-beca").addClass('cont-video-beca-close');
        jQuery("#video-beca").removeClass('cont-video-beca-open');
        jQuery("#calcu-left").addClass('cont-left-close');
        jQuery("#calcu-left").removeClass('cont-left-open');
        jQuery("#calcu-right").addClass('cont-right-close');
        jQuery("#calcu-right").removeClass('cont-right-open');
        // jQuery("#reset").addClass('reset-2');
        // jQuery("#reset").removeClass('reset');
    }
}



function finaliza() {
    console.log("_______FUNCION FINALIZA_______");

    getDuracion();

    jQuery('.an-slideUp').each(function () { jQuery(this).removeClass('an-slideUp') }); // QUITA LA ANIMACION PARA QUE CUANDO HAGA RESIZE NO EJECUTE LA ANIMACION DE NUEVO

    jQuery('body').removeClass('inicio');// EJECUTO EL ULTIMO PASO, SE QUITA LA BANDERA PARA QUE NO HAGA ANIMACIONES
    jQuery('.cont-quest').addClass('containerSecond');
    jQuery('.cont-quest').removeClass('cont-quest');
    jQuery('.imagen-lineas-negocio').addClass('d-none');
    jQuery('.boton-lineas-negocio').addClass('d-none');
    jQuery("#selector-float").animate({ width: '45%' }, function () {
        //jQuery(".output").css(");
        pasoFinalCalculadora = true;
        jQuery('.titlecont').addClass('d-none');
        jQuery('.card-calculadora').css('height','2.5vh');
        jQuery('.titlecont').removeClass('d-flex');
        jQuery('.card-calculadora').addClass('w-20');
        jQuery('document').removeClass('an-slideUp');
        jQuery('.containerSecond').removeClass('justify-content-center');
        jQuery('.controls').addClass('d-none');
        jQuery('.hide').not("#padre_tutor").fadeIn();
        jQuery(".hide-on-res").hide();
        jQuery(".quest.p-activa").removeClass('p-activa');
        jQuery(".quest").show();
        jQuery('.quest').removeClass('w-80');
        jQuery('.quest').addClass('w-100');
        jQuery(".questB").addClass("d-flex");
        jQuery(".pasos").hide();
        jQuery("#coming-back").removeClass('d-none');
        jQuery("#btn-verCol").addClass('d-none');
        jQuery("#btn-verCol-avanzar").addClass('d-none');

        jQuery(".more").click(function () {
            $(".switch-arrow").toggleClass("down-window top-window");

        });
        jQuery('.btn-parpadeante').click(function (e) {
            $(this).attr('href', $(this).attr('href') == '#resumen' ? '#first' : '#resumen');
        });
        
        var checkRangeSend = $("#range").attr("data-send");
        if (checkRangeSend == "1") {
            expandControls();
            jQuery(".more-controls").show();
        }
    });
    //Envio a Proc web
    var checkRangeSend = $("#range").attr("data-send");
    if (checkRangeSend == "1") {
        // envioProcWeb();
    } else if (checkRangeSend == "2") {
        // envioProcWeb();
    }

    // if (getUsuario().modalidad.catcosto == 'DENT') { // Cdentista debe empezar con 5 materias 
    //     // setTimeout(function () { jQuery('.mat-ctrl').click(); }, 100); //DEhabilita boton de 4 años
    //     jQuery('.btn-circ').eq(4).click();
    // }



    //jQuery('.btn-dual').last().click();

    // var eligeCarrera = getCampo('contado');
    if (jQuery('body').hasClass('inicio') == false) {
        jQuery('.rs-btn-mensual').click();
        if(getUsuario().modalidad.catcosto != "DENT"){
            jQuery('.btn-dual').last().click();
        }else{
            jQuery('.btn-circ').eq(4).click();
        }
    }

}

function readCookie(name) {
    var nameEQ = name + "=";
    var ca = document.cookie.split(';');
    for (var i = 0; i < ca.length; i++) {
        var c = ca[i];
        while (c.charAt(0) == ' ') c = c.substring(1, c.length);
        if (c.indexOf(nameEQ) == 0) return c.substring(nameEQ.length, c.length);
    }
    return null;
}

function obtenerTextoCampus(campus) {
    switch (campus) {
        case 'ATZ': return "Atizapán"; break;
        case 'MAR': return "Marina"; break;
        case 'ECA': return "Ecatepec"; break;
        case 'GDL': return "Guadalajara"; break;
        case 'LEO': return "León"; break;
        case 'SUR': return "Sur"; break;
        case 'CUI': return "Cuitláhuac"; break;
        case 'TOL': return "Toluca"; break;
        case 'QRO': return "Querétaro"; break;
        case 'REY': return "Los Reyes"; break;
        case 'ONL': return "En Línea"; break;

    }
}

function campusDynamics(campus) {
    switch (campus) {
        case "ATZ":
            return "ATIZAPAN";
            break;
        case "REY":
            return "LOS REYES";
            break;
        case "MAR":
            return "MARINA";
            break;
        case "ECA":
            return "ECATEPEC";
            break;
        case "SUR":
            return "SUR";
            break;
        case "CUI":
            return "CUITLAHUAC";
            break;
        case "LEO":
            return "LEON";
            break;
        case "REY":
            return "Los Reyes";
            break;
        case "TOL":
            return "TOLUCA";
            break;
        case "GDL":
            return "GUADALAJARA";
            break;
        case "ONL":
            return "EN LINEA";
            break
        case "QRO":
            return "QUERETARO";
            break
        default:
            return "MARINA";
    }
}

function envioProcWeb() {
    // if (checkCookie('envioCRM') == false) {
    //     sendDatalayer('calculadora/sec-Resultados', 'StepCalculator');
    // }
    
    var seleccionJson = JSON.parse(localStorage.getItem("jsonUsr"));
    if(seleccionJson.form != undefined){
        if(seleccionJson.form.frm_amaterno == "telemarketer"){
            seleccionJson.form["estado"]="AGUASCALIENTES";
            seleccionJson.form["banner"]=seleccionJson.form.frm_banner;
            seleccionJson.form["nombre"]=seleccionJson.form.frm_nombre;
            seleccionJson.form["apaterno"]=seleccionJson.form.frm_apaterno;
            seleccionJson.form["amaterno"]=seleccionJson.form.frm_amaterno;
            seleccionJson.form["email"]=seleccionJson.form.frm_correo;
            seleccionJson.form["celular"]=seleccionJson.form.frm_celular;
            seleccionJson.form["tipoRegistro"]=seleccionJson.form.revalidacion;
            var seleccionCookie = seleccionJson.form;
        }
    }else{
        var seleccionCookie = JSON.parse(decodeURIComponent(readCookie('c_form_data')));
    }
    
    var bannerCalculadora = jQuery("#frm_banner").val();

    var urlCalc = (checkCookie('envioCRM') == false) ? "//www.unitec.mx/wp-content/themes/temaunitec/calculadora/v2_calc/almacen1.php" : "//www.unitec.mx/wp-content/themes/temaunitec/calculadora/v2_calc/otra_consulta.php";

    var seleccionNivelOferta = "";
    var seleccionHerramienta = "";
    // var seleccionCampus = obtenerTextoCampus(seleccionJson.campus);
    var seleccionCampus = campusDynamics(seleccionJson.campus);

    if (seleccionJson.linea.id_name == "PREPARATORIA") {
        seleccionNivelOferta = "P";
        seleccionHerramienta = "ASPIRANTES PREPA";
    } else {
        seleccionNivelOferta = "L";
        seleccionHerramienta = "ASPIRANTES LIC";
        if (seleccionJson.modalidad.interes == 4 || seleccionJson.modalidad.interes == 6) {
            seleccionHerramienta = "ONLINEP";
        }
    }

    var objectoSeleccion = {
        nivelOferta: seleccionNivelOferta,
        Herramienta: seleccionHerramienta,
        Campana: 1,
        Origen: seleccionHerramienta,
        Banner: seleccionCookie.banner,
        Calidad: 3,
        L_Negocio: getNivelInteres(seleccionJson.linea.id_name),
        F_Nacim: "01/01/2000",
        Ciclo: "19-1",
        Alumno: 0,
        Estad: seleccionCookie.estado,
        C_Carrera: seleccionJson.modalidad.iddynamics,
        Carrera: seleccionJson.modalidad.nombre,
        banner_id: seleccionCookie.banner,
        CP: seleccionCookie.tipoRegistro,
        Pobla: seleccionJson.modalidad.interes,
        name_form: "",
        Sexo: "M",
        nom_carrera: "",
        nom_campus: seleccionCampus,
        promedioF: seleccionJson.promedio,
        Pdescuento: seleccionJson.beca,
        nivelF: "L",
        CostoUnico: 0,
        num_carrera: 0,
        costoMensual: 0,
        descuentoF: 0,
        planSelec: "4pagos",
        CostoSelec: seleccionJson.mensual,
        planRecom: "4pagos",
        CostoRecom: seleccionJson.mensual,
        envioB: 1,
        C_Carrera_WP: "",
        Direcc: "WEBCALL",
        Colon: "",
        Nombre2: "",
        email2: "",
        URLreferer: location.href,
        debugs: 1,
        gclid_field: "",
        userId_field: "",
        CID: jQuery('#CID').val(),
        email_unitec: "",
        otraConsulta: (checkCookie('envioCRM') == false) ? 0 : 1,
        SERVER_URL: "",
        frm_LadaLenght: "",
        frm_LadaAllow: "",
        alum_2_2: "",
        Nombre: seleccionCookie.nombre,
        Ap_Pat: seleccionCookie.apaterno,
        Ap_Mat: seleccionCookie.amaterno,
        email: seleccionCookie.email,
        informacion_2: 1,
        Estado: seleccionCookie.estado,
        masculino: "M",
        TipoTel: "cel_calc",
        cel_calc: "",
        Telef: seleccionCookie.celular,
        periodo_interes_sel: "19-1",
        politica: "on",
        promedio: seleccionJson.promedio,
        programa: seleccionJson.modalidad.iddynamics,
        materias: seleccionJson.materias,
        hubspotutk: "",
        padretutor: "No",
        termino: 3,
        pago_mensual_beca: seleccionJson.mensualAhorro,
        formulario_completado: "paso_2",
        form_origen: "Calculadora"
    };

    console.log(objectoSeleccion);
    // console.log("LO QUE ENVIO");
    // console.log(JSON.stringify(objectoSeleccion));
    $
        .ajax({
            type: "POST",
            url: urlCalc,
            // url: "/wp-content/themes/temaunitec/calculadora/v2_calc/almacen1.php",
            data: objectoSeleccion,
            dataType: "JSON",
            cache: false,
            success: function (res) {
                console.log("Datos Enviados");
                console.log(res);
                if (res.tipo == "nuevo") {
                    console.log('nuevo');
                    dataLayer.push({
                        'event': 'CALCULADORA_REGISTRO'
                    });
                    dataLayer.push({
                        'origenLead': 'frmCalcu', //dato dinámico
                        'isAlumn': objectoSeleccion.Alumno, //dato dinámico
                        'ingress': objectoSeleccion.CP, //dato dinámico
                        'state': objectoSeleccion.Estado, //dato dinámico
                        'levelStudies': objectoSeleccion.L_Negocio, //dato dinámico
                        'Period': objectoSeleccion.Ciclo, //dato dinámico
                        'carrera': objectoSeleccion.Carrera, //dato dinámico
                        'campus': objectoSeleccion.nom_campus, //dato dinámico
                        'date': new Date(), //dato dinámico
                        'event': 'leadGenerationCalcu' //dato estático
                    });
                }
                else {
                    dataLayer.push({
                        'event': 'CALCULADORA_DUPLICADO'
                    });

                    dataLayer.push({
                        'url_value': 'formularioCalculadora/Duplicado', //dato dinámico
                        'event': 'StepForm' //dato estático
                    });
                }
                if (checkCookie('envioCRM') == false) {
                    setCookie('envioCRM', 1, 1);
                }
            },
            error: function (xhr, textStatus, error) {
                console.log(xhr.statusText);
                console.log(textStatus);
                console.log(error);
            }
        });
}




/* FUNCIONES DE LINEA DE NEGOCIO
 * getLineasNegocio         -> Consulta JSON
 * render_lineas_negocio    -> Pinta todos los divs 
 * set_linea_negocio        -> Cuando el usuario selecciona una linea de negocio
*/
function getLineasNegocio() {
    var lineasNegocio =
        [{ "name": "Preparatoria", "id_name": "PREPARATORIA", "order": "", "abrv": "PREPA" },
        { "name": "Licenciatura", "id_name": "LICENCIATURA", "order": "", "abrv": "UG" },
        { "name": "Ingeniería", "id_name": "INGENIERIA", "order": "", "abrv": "ING" },
        { "name": "Lic. en Salud", "id_name": "SALUD", "order": "", "abrv": "CS" },
        { "name": "Posgrado", "id_name": "POSGRADO", "order": "1", "abrv": "POS" }];
    return lineasNegocio;
}
function render_lineas_negocio() {
    var lineas = getLineasNegocio();
    jQuery.each(lineas, function (index, value) {
        console.log('My array has at position ' + index + ', this value: ' + value.name);
        var html = '<div class="card card-calculadora"><div data-name="' + value.name + '" data-id_name="' + value.id_name + '" data-abrv="' + value.abrv + '" class="card-body tc w-100"><h5 class="mt-0" style="letter-spacing: 0px !important;">' + value.name + '</h5><div class="imagen-lineas-negocio imagen-lineas-cuadros cuadro ' + value.abrv + '"></div><button class="btn btn-dtc boton-lineas-negocio waves-effect waves-light"  role="button">Selecciona</button></div></div>';
        jQuery('#render_lineas_negocio').append(html);
    });
}

function getCarreras(linea, campus) {
    var keySearch = "lineaweb";
    var arrayCarrerasPorLinea = [];
    var objJsonCarreras = JSON.parse(localStorage.getItem("jsonCarreras"));
    arrayCarrerasPorLinea = this.buscar(
        keySearch,
        linea,
        objJsonCarreras
    );

    if (linea == 'LICENCIATURA') {
        arrayCarrerasPorLineaSalud = this.buscar(keySearch, 'SALUD', objJsonCarreras);
        var arrayCarrerasPorLinea = arrayCarrerasPorLinea.concat(arrayCarrerasPorLineaSalud);
        console.log('AGREGANDO LAS DE SALUD');
    }

    /*Para identificar las carreras que se obtuvieron, sirve para la autoselección desde formulario */
    // localStorage.setItem("jsonCarrerasSeleccion", JSON.stringify(arrayCarrerasPorLinea));
    return arrayCarrerasPorLinea
}

function getCarrerasBien(categorias) {
    var objJsonCarreras = JSON.parse(localStorage.getItem("jsonLinksCategorias"));
    var categoriasno = new Array();
    Object.entries(categorias).forEach(function (key, value) {
        categoriasno.push(key[1]['Grupo_carreras']);
    });
    //console.log(categoriasno);
    var nombres = new Array();
    Object.entries(objJsonCarreras).forEach(function (key, value) {
        if (categoriasno.includes(key[1]['IdCategoria'])) {
            nombres.push(key[1]);
        }
    });
    return nombres;
}


/* FUNCIONES DE CARRERA
 * render_carreras         -> Pinta las carreras en el autocomplete 
*/
function render_carreras(carreras) {
    // console.log('las carreras');
    // console.log(carreras);
    var carrerasjson = [];
    jQuery.each(carreras, function (index, value) {
        //console.log(value);
        console.log('My array has at position ' + index + ', this value: ' + value.IdCategoria);
        item = {}
        item["value"] = value.IdCategoria;
        item["label"] = value.Categoria;
        //item["desc"] = value.Categoria;
        item["icon"] = value.IdDynamics;
        item["Categoria"] = value.Categoria;
        item["IdCategoria"] = value.IdCategoria;
        item["Link"] = value.Link;
        carrerasjson.push(item);
    });
    carrerasjson.sort(function (a, b) {
        if (a.Categoria.toLowerCase() < b.Categoria.toLowerCase()) return -1;
        if (a.Categoria.toLowerCase() > b.Categoria.toLowerCase()) return 1;
        return 0;
    });

    // console.log("IMPRIMO LAS CARRERAS ");
    // console.log(carrerasjson);
    //Manejo de las carreras cuando es prepa
    var linea_seleccionada = getUsuario().linea.id_name;
    if (linea_seleccionada == "PREPARATORIA") {
        guardaUsuario('carrera', carrerasjson[0]);
        jQuery('.rs-carrera').text(getCampo('carrera').Link.replace(/-/g, ' ').replace(/\//g, ''));
        render_modalidades(getModalidades(carrerasjson[0].IdCategoria));
        muestraPaso('business-line', 'container_carrera_modalidad');
        jQuery("#carrera").addClass("hide");
        jQuery("#carrera").addClass("hide-prepa");
        jQuery("#padre_tutor").removeClass("hide");
        jQuery("#modalidad").removeClass("hide");
        regresaPaso('container_carrera_modalidad', 'business-line');
        $('.steps').text('4');
    } else {
        jQuery("#carrera").removeClass("hide");
        jQuery("#carrera").removeClass("hide-prepa");
        jQuery("#padre_tutor").addClass("hide");
    }

    var accentMap = {
        "á": "a",
        "é": "e",
        "í": "i",
        "ó": "o",
        "ú": "u"
    };
    var normalize = function (term) {
        var ret = "";
        for (var i = 0; i < term.length; i++) {
            ret += accentMap[term.charAt(i)] || term.charAt(i);
        }
        return ret;
    };

    console.log("IMPRIMO LAS CARRERAS ");
    console.log(carrerasjson);
    $("#project").autocomplete({
        minLength: 0,
        // source: carrerasjson,
        source: function (request, response) {
            /* Funcionalidad para hacer la búsqueda sin importar los acentos */
            var matcher = new RegExp($.ui.autocomplete.escapeRegex(request.term), "i");
            response($.grep(carrerasjson, function (value) {
                value = value.label || value.value || value;
                return matcher.test(value) || matcher.test(normalize(value));
            }));
        },
        messages: {
            noResults: '',
            results: function () { $('.ui-helper-hidden-accessible').empty() }
        },
        focus: function (event, ui) {
            $("#project").val(ui.item.label);
            return false;
        },
        select: function (event, ui) {
            console.log(ui);
            $(this).blur();
            $("#project").val(ui.item.label);
            $("#project-id").val(ui.item.value);
            //$("#project-description").html(ui.item.desc);
            $("#project-icon").attr("src", "images/" + ui.item.icon);
            //let oferta =[{ "Categoria": ui.item.Categoria, "IdCategoria": ui.item.IdCategoria, "Link": ui.item.Link }];
            guardaUsuario('carrera', ui.item);
            // jQuery('.rs-carrera').text(getCampo('carrera').Link);
            jQuery('.rs-carrera').text(getCampo('carrera').Link.replace(/-/g, ' ').replace(/\//g, ''));
            render_modalidades(getModalidades(ui.item.IdCategoria));

            //render_campus(getModalidades(ui.item.IdCategoria));
            // console.log(getCarreras(getCampo("carrera").IdCategoria));

            // var campus=alert()("modalidad").campus.toString().split(",").sort();
            // console.log(campus);

            // muestraPaso('carrera', 'modalidad');
            // regresaPaso('modalidad', 'carrera');
            // $('.steps').text('5');
            $('#modalidad').removeClass('hide');
            return false;
        }
    })
        .autocomplete("instance")._renderItem = function (ul, item) {
            return $("<li>")
                .append("<div>" + item.label + "</div>")
                //.append("<div>" + item.label + "<br>" + item.desc + "</div>")
                .appendTo(ul);
        };

        // if (linea_seleccionada == "PREPARATORIA"){
        //     $('.ui-menu-item div').first().click();
        // }

}

function avanzaPaso() {
    var faltantes = 0;
    /* Controla todos los inputs que faltan */
    if (jQuery('#project').val() == "") {
        faltantes++;
    }

    if (jQuery('#render_campus').val() == 0) {
        faltantes++;
    }

    if (jQuery('#render_modalidad').val() == 0) {
        faltantes++;
    }

    var linea_seleccionada = getUsuario().linea.id_name;
    if (linea_seleccionada == "PREPARATORIA") {
        if (jQuery('#render_padre_tutor').val() == "Selecciona") {
            faltantes++;
        }
    }

    if(faltantes > 0){
        console.log("NO PUEDES AVANZAR");
    }else {
        off();
        jQuery(".modal-header-formulario").hide();
        jQuery(".modal-header-thankyou").hide();
        jQuery(".img-header-thankyou").hide();
        jQuery("#enviarTrd").hide();
        jQuery("#body-form-todo").hide();
        jQuery(".modal-body-thankyou").show();
        // jQuery(".tercer-texto-thankyou").html(cookie_form_values["carrera"]);
        
        // jQuery("#nombre-thankyou").html(cookie_form_values["nombre"]);
        /*VALORES DE PRUEBA */
        jQuery("#title-virtualtour").html("Conoce las instalaciones");
        jQuery(".tercer-texto-thankyou").html("SISTEMAS COMP");
        jQuery("#nombre-thankyou").html("PRUEBA");
        jQuery('.second-sub-desk').text("UNITEC Campus " + "ATIZAPAN");
        jQuery('#thank-linea').text("");
        jQuery('#thank-linea').text("SISTEMAS COMP");
        jQuery('#thank-modalidad').text("");
        jQuery('#thank-modalidad').text("EJECUTIVA");
        jQuery('#thank-campus').text("");
        jQuery('#thank-campus').text("Campus " + "ATIZAPAN");
        jQuery('#thank-ciclo').text("");
        jQuery('#thank-ciclo').text("19-2");

        jQuery("#descripcionThanks").html("Impulsamos tu esfuerzo recompensándote con una Beca Académica de Primer Ingreso. Conoce el porcentaje que te corresponde según tu promedio.");
        /*VALORES DE PRUEBA */
        
        jQuery("#head_close_modal").hide();
        jQuery(".titulo-logo").show();

        jQuery("#modal_frm_app").modal();
        $('#calculadora-css').attr("disabled","disabled");
        $('#calculadora-css').prop("disabled",true);
        $('#aplicativo-formulario').hide();
        $(".todoElBody").show();
        // $('.main-calculadora-todo').show();
        
    }
    
}

/* FUNCIONES DE MODALIDADES
* getModalidades         -> OBTIENE MODALIDADES Y RECIBE EL ID DE CATEGORIA
* render_MODALIDADES  -> Obtienes las modalidades y las pinta 
*/

function getModalidades(categoria) {
    var keySearch = "Grupo_carreras";
    var arrayCarrerasPorLinea = [];
    var objJsonCarreras = JSON.parse(localStorage.getItem("jsonCarreras"));
    arrayCarrerasPorLinea = buscar(
        keySearch,
        categoria,
        objJsonCarreras
    );

    return arrayCarrerasPorLinea
}

function render_modalidades(modalidades) {
    console.log('render_modalidades');
    console.log(modalidades);
    console.log('Existen:' + modalidades.length);
    // changeAddInCareers(modalidades[0]);

    var modalidadesSize = modalidades.length;
    /*******MODALIDADES******/
    if (modalidades.length == 1) {
        var html = '<option selected data-Grupo_carreras="' + modalidades[0].Grupo_carreras + '" data-IdDynamics="' + modalidades[0].IdDynamics + '" data-campus="' + modalidades[0].campus + '"  data-catCosto="' + modalidades[0].catCosto + '" data-interes="' + modalidades[0].interes + '" data-lineaweb="' + modalidades[0].lineaweb + '" data-modalidad="' + modalidades[0].modalidad + '"  data-nombre="' + modalidades[0].nombre + '" value="' + modalidades[0].modalidad + '">' + modalidadtxt(modalidades[0].modalidad) + '</option>';
    } else {
        console.log('mas de 3');
        var html = '<option value="0">Modalidad</option>';
        jQuery.each(modalidades, function (index, value) {
            console.log('My array has at position ' + index + ', this value: ' + value.modalidad + modalidadtxt(value.modalidad));
            html += '<option data-Grupo_carreras="' + value.Grupo_carreras + '" data-IdDynamics="' + value.IdDynamics + '" data-campus="' + value.campus + '"  data-catCosto="' + value.catCosto + '" data-interes="' + value.interes + '" data-lineaweb="' + value.lineaweb + '" data-modalidad="' + value.modalidad + '"  data-nombre="' + value.nombre + '" value="' + value.modalidad + '">' + modalidadtxt(value.modalidad) + '</option>';
        });
    }
    jQuery('#render_modalidad').html(html).delay(200).queue(function (next) {
        if (modalidadesSize == 1) {
            jQuery('#render_modalidad').next().addClass('inactive');
        } else if (modalidadesSize > 1) {
            jQuery('#render_modalidad').next().removeClass('inactive');
        }
        next();
    });

    /*******CAMPUS******/
    console.log('todos los campus que tienen esta carrera');
    var campus = [];
    jQuery.each(modalidades, function (index, value) { campus.push(value.campus); }); //Guarda los arreglos de campus
    var resultArray = Array.prototype.concat.apply([], campus);
    allcampus = resultArray.join(',');
    var b = unique(allcampus.split(",")); // Array de campus unicos 
    console.log(b);
    b.sort();
    var html = '<option value="0"> Campus</option>';
    if (b.length == 1) {
        html += '<option selected value="' + b[0] + '">' + campustxt(b[0]) + '</option>';
    } else {
        jQuery.each(b, function (index, value) {
            html += '<option value="' + value + '">' + campustxt(value) + '</option>';
        });
    }

    jQuery('#render_campus').html(html).delay(300).queue(function (next) {
        /* If cuando solo hay un campus para que se autoseleccione */
        if (b.length == 1) {
            jQuery('#render_campus').next().addClass('inactive');
            /* If cuando se autoselecciona la modalidad */
            if (jQuery('#render_modalidad').next().hasClass('inactive')) {
                regresoAnterior = false;
                /* Funcion cuando se quiere avanzar automáticamente a promedio */
                // setTimeout(function () { muestraPaso('modalidad', 'promedio'); regresoAnterior = false; }, 500);
                /* Funcion cuando se avanza hasta dar click en el boton */
                setTimeout(function () { regresoAnterior = false; }, 500);
                // muestraPaso('modalidad', 'promedio');
                guardaUsuario('campus', jQuery('#render_campus').val());
                guardaUsuario('modalidad', jQuery('#render_modalidad option:selected').data());
                var nivelAnterior = getNivelAnterior(getCampo('modalidad').interes);
                jQuery('.rs-canterior').text(nivelAnterior);
                off();
                var checkOverlaySend = $("#overlay").attr("data-send");
                if (checkOverlaySend == "1") {
                    // calcular_uno();
                    if (!jQuery('inicio').hasClass('inicio')) {
                        $("#overlay").attr("data-send", "2");
                        // $('#render_campus').next().next().children().first().click();
                        // envioProcWeb();
                    }
                } else if (checkOverlaySend == "2") {
                    // calcular_uno();
                    if (!jQuery('inicio').hasClass('inicio')) {
                        $("#overlay").attr("data-send", "2");
                        // $('#render_campus').next().next().children().first().click();
                        // envioProcWeb();
                    }
                } else {
                    if (!jQuery('inicio').hasClass('inicio')) {
                        $("#overlay").attr("data-send", "1");
                    }
                }
                // calcular_uno();
            }
            // $('#render_campus').next().next().children().first().click();
        } else {
            /* En caso de que haya más de un campus se le remueve todo lo preseleccionado */
            jQuery('#render_campus').removeAttr('data-preselected');
            jQuery('#render_campus').next().removeClass('inactive');
        }
        next();
    });
    render_selects();
}

function getMinimoMatBeca(catCosto) {
    var beca;
    switch (catCosto) {

        case "NUTRI":
        case "BLUEM":
        case "GASTRO":
        case "FISIO":
        case "TURI":
        case "DENT":
        case "ENFER":
        case "PREPA":
        case "ING":
        case "ESP":
        case "ESPD":
        case "LIC":
        case "LICPSICOLOG":
        case "LICPEDAG":
            minmaterias = 5;
            break


        case "LICEJEC":
        case "INGEJEC":
        case "LICON":
        case "INGON":
            minmaterias = 4;
            break;

        case "DIP":
        case "DIPON":
        case "POS":
        case "POSON":
        case "POSONCUAT":
        case "POSCUAT":
        case "POSCUATS":
            minmaterias = 3
            break;
        default:
            minmaterias = 5;
    }
    return minmaterias;
}


/* FUNCIONES GENERALES
 * guardaUsuario   -> Guarda en localstorage todo el objeto en formato JSON
*/

function modalidadtxt(num) {
    switch (parseInt(num)) {
        case 1: return "Presencial"; break;
        case 2: return "Ejecutiva"; break;
        case 3: return "En línea"; break;
    }
}

function campustxt(campus) {
    switch (campus) {
        case 'ATZ': return "Atizapán, Edo. Méx."; break;
        case 'MAR': return "Marina, CDMX"; break;
        case 'ECA': return "Ecatepec, Edo. Méx."; break;
        case 'GDL': return "Guadalajara"; break;
        case 'LEO': return "León, Gto"; break;
        case 'SUR': return "Sur, CDMX"; break;
        case 'CUI': return "Cuitláhuac, CDMX"; break;
        case 'TOL': return "Toluca, Edo. Méx"; break;
        case 'QRO': return "Querétaro, QRO"; break;
        case 'REY': return "Los Reyes, CDMX"; break;
        case 'ONL': return "En Línea"; break;

    }
}

function unique(array) {
    return array.filter(function (el, index, arr) {
        return index == arr.indexOf(el);
    });
}


function render_selects() {
    jQuery('.select-selected,.select-items').remove();
    var x, i, j, selElmnt, a, b, c;
    /*look for any elements with the class "custom-select":*/
    x = document.getElementsByClassName("custom-select");
    for (i = 0; i < x.length; i++) {
        selElmnt = x[i].getElementsByTagName("select")[0];
        /*for each element, create a new DIV that will act as the selected item:*/
        a = document.createElement("DIV");
        a.setAttribute("class", "select-selected w-100");
        console.log("pasando el primero");
        a.innerHTML = selElmnt.options[selElmnt.selectedIndex].innerHTML;
        x[i].appendChild(a);
        /*for each element, create a new DIV that will contain the option list:*/
        b = document.createElement("DIV");
        b.setAttribute("class", "select-items select-hide");
        for (j = 1; j < selElmnt.length; j++) {
            /*for each option in the original select element,
            create a new DIV that will act as an option item:*/
            c = document.createElement("DIV");
            console.log("pasando el segundo");
            c.innerHTML = selElmnt.options[j].innerHTML;
            c.addEventListener("click", function (e) {
                /*when an item is clicked, update the original select box,
                and the selected item:*/
                var y, i, k, s, h;
                s = this.parentNode.parentNode.getElementsByTagName("select")[0];
                h = this.parentNode.previousSibling;
                for (i = 0; i < s.length; i++) {
                    if (s.options[i].innerHTML == this.innerHTML) {
                        s.selectedIndex = i;
                        h.innerHTML = this.innerHTML;
                        y = this.parentNode.getElementsByClassName("same-as-selected");
                        for (k = 0; k < y.length; k++) {
                            y[k].removeAttribute("class");
                        }
                        this.setAttribute("class", "same-as-selected");
                        break;
                    }
                }
                h.click();
            });
            b.appendChild(c);
        }
        x[i].appendChild(b);



        b.addEventListener("click", function (e) {
            var idSelect = jQuery(this).parent().find('select').prop('id');
            updateSelect(idSelect);

        });

        a.addEventListener("click", function (e) {
            /*when the select box is clicked, close any other select boxes,
            and open/close the current select box:*/
            e.stopPropagation();
            closeAllSelect(this);
            this.nextSibling.classList.toggle("select-hide");
            this.classList.toggle("select-arrow-active");
            console.log('elegiste:')
            console.log(e);



        });
    }
    checkAllInputs();
}
function updateSelect(id) {
    setTimeout(function () { jQuery("#" + id).change(); render_selects(); }, 200);

}

function closeAllSelect(elmnt) {
    /*a function that will close all select boxes in the document,
    except the current select box:*/
    var x, y, i, arrNo = [];
    x = document.getElementsByClassName("select-items");
    y = document.getElementsByClassName("select-selected");
    for (i = 0; i < y.length; i++) {
        if (elmnt == y[i]) {
            arrNo.push(i)
        } else {
            y[i].classList.remove("select-arrow-active");
        }
    }
    for (i = 0; i < x.length; i++) {
        if (arrNo.indexOf(i)) {
            x[i].classList.add("select-hide");
        }
    }
}
function abreCalculadora() {
    var prueba_terminado = $("#modal_frm_app_calculadora").attr('data-terminado');
    var prueba_externo = $("#modal_frm_app_calculadora").attr('data-externo');
    if (prueba_terminado == "1") {
        xCalculadora = window.scrollX;
        yCalculadora = window.scrollY;
        console.log("Guardando y: " + yCalculadora);
        //Se cierra todo para que se muestre la calculadora
        console.log("Cerrando todo");
        window.scrollTo(0, 0);
        $('#bootstrapcss-css').attr('disabled', 'disabled');
        $('#mdb-css').attr('disabled', 'disabled');
        $('#bootstrapcss-css').prop('disabled', true);
        $('#mdb-css').prop('disabled', true);
        $('#calculadora-css').removeAttr("disabled");
        $('#calculadora-css').prop("disabled", false);
        $(".todoElBody").hide();
        $('.main-calculadora-todo').show();
        $('#aplicativo-calculadora').show();

        console.log("AQUI ESTÁ LA COOKIE");
        var data_cookie_form = JSON.parse(decodeURIComponent(getCookie("c_form_data")));
        console.log(data_cookie_form);
        if(data_cookie_form.linea != undefined)
        {
            $("div").find("[data-id_name="+data_cookie_form.linea+"]").mouseup();
            var busqueda_grupo = buscar_grupo("IdDynamics",data_cookie_form.carreraInteres,JSON.parse(localStorage.jsonCarreras));
            console.log("LA BUSQUEDA DEL GRUPO");
            console.log(busqueda_grupo);
            var categoria_grupo = busqueda_grupo[0].Categoria;
            console.log("LA BUSQUEDA DEL DIV");
            $('#project').autocomplete('search', $('#project').val());
            $(".ui-menu-item-wrapper:contains('"+categoria_grupo+"')").click();
            console.log("BUSQUEDA DE MODALIDAD");
            var modalidad_size = $("#render_modalidad").next().next().children().length;
            if(modalidad_size > 0){
                var txt_modalidad = $("#render_modalidad option[value="+data_cookie_form.modalidad+"]").text();
                $("#render_modalidad").next().next().children(":contains("+txt_modalidad+")").click();
            }
            var campus_size = $("#render_campus").next().next().children().length;
            if(campus_size > 0){
                var txt_campus = $("#render_campus option[value="+data_cookie_form.campus+"]").text();
                $("#render_campus").next().next().children(":contains("+txt_campus+")").click();
            }
            $("#btn-verCol-avanzar").click();
        }
        

    } else if(prueba_externo == "1"){
        xCalculadora = window.scrollX;
        yCalculadora = window.scrollY;
        console.log("Guardando y: " + yCalculadora);
        //Se cierra todo para que se muestre la calculadora
        console.log("Cerrando todo");
        window.scrollTo(0, 0);
        $('#bootstrapcss-css').attr('disabled', 'disabled');
        $('#mdb-css').attr('disabled', 'disabled');
        $('#bootstrapcss-css').prop('disabled', true);
        $('#mdb-css').prop('disabled', true);
        $('#calculadora-css').removeAttr("disabled");
        $('#calculadora-css').prop("disabled", false);
        $(".todoElBody").hide();
        $('.main-calculadora-todo').show();
        $('#aplicativo-calculadora').show();
    } else {
        jQuery("#modal_frm_app_calculadora").modal();
    }
}
/*if the user clicks anywhere outside the select box,
then close all select boxes:*/
// document.addEventListener("click", closeAllSelect);

function clickSeeDownModalidad() {
    // console.log("DIO CLICK EN MODALIDAD");
    // $('#render_modalidad').next()[0].nextSibling.classList.toggle("select-hide");
    // $('#render_modalidad').next()[0].classList.toggle("select-arrow-active");
    $('#render_modalidad').next().click();
}

function clickSeeDownCampus() {
    // $('#render_campus').next()[0].nextSibling.classList.toggle("select-hide");
    // $('#render_campus').next()[0].classList.toggle("select-arrow-active");
    $('#render_campus').next().click();
}
/* SEGUNDA PARTE JS */
