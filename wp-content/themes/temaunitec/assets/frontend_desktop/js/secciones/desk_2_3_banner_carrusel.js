//Social reveal
$(document).ready(function ($) {

	/*Inicializamos el Autoplay*/
	$('#carousel-banner').carousel({ 
		interval: 8000, 
		cycle: true,
		ride: true,
		pause: false
	});
	/*End Inicializamos el Autoplay*/
});

$('.banner-share > a').on('click', function (e) {
        e.preventDefault() // prevent default action - hash doesn't appear in url
        $(this).parent().find('div').toggleClass('social-reveal-active');
        $(this).toggleClass('share-expanded');
});