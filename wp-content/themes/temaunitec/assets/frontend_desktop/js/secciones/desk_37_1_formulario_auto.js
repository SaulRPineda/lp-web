/* Funciones compartidas */
var urlJsonBasura = "/assets/frontend/json/min/json_basura.min.json";
var urlJsonBasuraEmail = "/assets/frontend/json/min/json_basuraEmail.min.json";
var urlJsonCarreras = "/assets/frontend/json/min/json_calc_carreras.min.json";
//Se agrega nueva url para obtener los links y nombre de las categorias de las carreras
var urlJsonLinksCategorias = "/assets/frontend/json/min/json_calc_linkPaginas.min.json";
//Se agregan variables para calculadora
//Costos
// var urlJsonCalcCostos = "/assets/frontend/json/min/json_calc_costos.min.json";
//Becas
// var urlJsonCalcBecas = "/assets/frontend/json/min/json_calc_becas.min.json";
var urlwp = "/wp-content/themes/temaunitec";

/*URL Envio a Ciclo Activo BY SRP 09-10-2018*/
var url_ajax_php = "/wp-content/phpServeApp/backend0.php";
/*End URL Envio a Ciclo Activo BY SRP 09-10-2018*/

/*Variables para setear las Coordenadas del Modal*/
var xCalculadora = 0;
var yCalculadora = 0;
/*Variables para setear las Coordenadas del Modal*/

/*Variables para los listener*/
var jsonCarreras = false;
var llenadoCarreras = false;
var llenadoLineas = false;
var autocompleteMobile = false;
var autocompleteDesktop = false;
var nodo_autocomplete;
var dataLayerAutoComplete = [];
/*Variables para los listener*/

/* Pasos funnels */
// var paso1 = true;
// var paso2 = true;
var paso3 = true;
var paso4 = true;
var paso5 = true;
var paso6 = true;
var paso7 = true;
/* Pasos funnels */

/*Estados de México */
var estados_global = [
    {"id": "1","name":"Aguascalientes"},
    {"id": "2","name":"Baja California Norte"},
    {"id": "3","name":"Baja California Sur"},
    {"id": "4","name":"Campeche"},
    {"id": "5","name":"Coahuila"},
    {"id": "6","name":"Colima"},
    {"id": "7","name":"Chiapas"},
    {"id": "8","name":"Chihuahua"},
    {"id": "9","name":"Ciudad de México"},
    {"id": "10","name":"Durango"},
    {"id": "11","name":"Estado de México"},
    {"id": "12","name":"Guanajuato"},
    {"id": "13","name":"Guerrero"},
    {"id": "14","name":"Hidalgo"},
    {"id": "15","name":"Jalisco"},
    {"id": "16","name":"Michoacán"},
    {"id": "17","name":"Morelos"},
    {"id": "18","name":"Nayarit"},
    {"id": "19","name":"Nuevo León"},
    {"id": "20","name":"Oaxaca"},
    {"id": "21","name":"Puebla"},
    {"id": "22","name":"Querétaro"},
    {"id": "23","name":"Quintana Roo"},
    {"id": "24","name":"San Luis Potosí"},
    {"id": "25","name":"Sinaloa"},
    {"id": "26","name":"Sonora"},
    {"id": "27","name":"Tabasco"},
    {"id": "28","name":"Tamaulipas"},
    {"id": "29","name":"Tlaxcala"},
    {"id": "30","name":"Veracruz"},
    {"id": "31","name":"Yucatán"},
    {"id": "32","name":"Zacatecas"}
  ];
/*Estados de México */

function cerrarModalCalculadora() {
    $('#calculadora-css').attr('disabled', 'disabled');
    $('#calculadora-css').prop("disabled", true);
    $('#bootstrapcss-css').removeAttr('disabled');
    $('#mdb-css').removeAttr('disabled');
    $('#bootstrapcss-css').prop('disabled', false);
    $('#mdb-css').prop('disabled', false);
    $("#aplicativo-formulario").hide();
    $(".todoElBody").show();

    if (yCalculadora != 0) {
        console.log("Cambiando scroll en y " + yCalculadora);
        setTimeout(function (xCalculadora, yCalculadora) { window.scrollTo(xCalculadora, yCalculadora); }, 700, xCalculadora, yCalculadora);
    }
}

function getJsonCarreras() {
    jQuery
        .ajax({
            type: "GET",
            //url: this.urlJsonCarreras,
            url: urlwp + urlJsonCarreras,
            "Content-Type": "application/json",
            dataType: "json",
            success: function (resultado) { }
        })
        .done(function (resultado) {
            localStorage.setItem("jsonCarreras", JSON.stringify(resultado));
            if (!IsMobile()) {
                if (jQuery("#h_id_producto").val() != "" && jQuery("#h_id_producto").val() != undefined) {
                    var nodo_encontrado = buscar("Grupo_carreras", jQuery("#h_id_producto").val(), JSON.parse(localStorage.getItem("jsonCarreras")));
                    console.log("EL NODO ENCONTRADO ES:");
                    console.log(nodo_encontrado);
                    setTimeout(autoCompleteFormulario, 400, nodo_encontrado[0]);
                    // autoCompleteFormulario(nodo_encontrado[0]);
                }
            } else {
                console.log("EJECUTANDO EL TRIGGER jsonCarrerasFilled");
                $( document ).trigger( "jsonCarrerasFilled");
                $(window).on('load', function(){
                    if (jQuery("#h_id_producto").val() != "" && jQuery("#h_id_producto").val() != undefined) {
                        autocompleteMobile = true;
                        var nodo_encontrado = buscar("Grupo_carreras", jQuery("#h_id_producto").val(), JSON.parse(localStorage.getItem("jsonCarreras")));
                        nodo_encontrado = nodo_encontrado[0];
                        nodo_autocomplete = nodo_encontrado;
                        if (nodo_encontrado.lineaweb != undefined) {
                            setTimeout(function () {
                                console.log("DANDO CLICK EN: " + nodo_encontrado.lineaweb);
                                console.log($("div").find("[data-val='" + nodo_encontrado.lineaweb + "']"));
                                $("div").find("[data-val='" + nodo_encontrado.lineaweb + "']").click();
                            }, 400);
                        }
                    }
                })
                
                // if (jQuery("#h_id_producto").val() != "" && jQuery("#h_id_producto").val() != undefined) {
                //     var nodo_encontrado = buscar("Grupo_carreras", jQuery("#h_id_producto").val(), JSON.parse(localStorage.getItem("jsonCarreras")));
                //     nodo_encontrado = nodo_encontrado[0];
                //     if (nodo_encontrado.lineaweb != undefined) {
                //         setTimeout(function () {
                //             console.log("DANDO CLICK EN: " + nodo_encontrado.lineaweb);
                //             console.log($("div").find("[data-val='" + nodo_encontrado.lineaweb + "']"));
                //             $("div").find("[data-val='" + nodo_encontrado.lineaweb + "']").click();
                //             var busqueda_grupo = buscar_grupo("IdDynamics", nodo_encontrado.IdDynamics, JSON.parse(localStorage.jsonCarreras));
                //             console.log("LA BUSQUEDA DEL GRUPO");
                //             console.log(busqueda_grupo);
                //             var categoria_grupo = busqueda_grupo[0].Categoria;
                //             // console.log("LA BUSQUEDA DEL OPTION");
                //             // console.log(categoria_grupo);
                //             // $("#project option:contains('"+categoria_grupo+"')").click();
                //             /* Agregar un timer para la seleccion de carrera */
                //             setTimeout(function () {
                //                 var carrera_selected = $("#project option").filter(function () {
                //                     return $(this).text() == categoria_grupo;
                //                 }).prop('selected', true).change();
            
                //                 var modalidad_size = $("#render_modalidad").children().length;
                //                 if (modalidad_size == 1) {
                //                     // var txt_modalidad = $("#render_modalidad option[value=" + verif_cookie.modalidad + "]").prop("selected", true).change();
                //                 }
                //                 var campus_size = $("#render_campus").children().length;
                //                 if (campus_size == 2) {
                //                     // var txt_campus = $("#render_campus option[value=" + verif_cookie.campus + "]").prop("selected", true).change();
                //                     $("#render_campus option").first().next().prop("selected", true).change();
                //                 }
                //                 // $(".right.button-primaryn")[0].click();
                //             }, 400);
                //         }, 1000);
            
                //     }
            
                // }
            }

        });
}
//Funcion para obtener el json de links y categorias
function getJsonLinksCategorias() {
    jQuery
        .ajax({
            type: "GET",
            //url: this.urlJsonLinksCategorias,
            url: urlwp + urlJsonLinksCategorias,
            "Content-Type": "application/json",
            dataType: "json",
            success: function (resultado) { }
        })
        .done(function (resultado) {
            localStorage.setItem("jsonLinksCategorias", JSON.stringify(resultado));
        });
}
//Funcion para traer el Json de basura con jquery ajax
function getJsonBasura() {
    jQuery
        .ajax({
            type: "GET",
            //url: this.urlJsonBasura,
            url: urlwp + urlJsonBasura,
            "Content-Type": "application/json",
            dataType: "json",
            success: function (resultado) { }
        })
        .done(function (resultado) {
            console.log(resultado);
            localStorage.setItem("jsonBasura", JSON.stringify(resultado));

        });
}
//Funcion para traer el Json de email basura con jquery ajax
function getJsonBasuraEmail() {
    jQuery
        .ajax({
            type: "GET",
            //url: this.urlJsonBasuraEmail,
            url: urlwp + urlJsonBasuraEmail,
            "Content-Type": "application/json",
            dataType: "json",
            success: function (resultado) { }
        })
        .done(function (resultado) {
            localStorage.setItem("jsonBasuraEmail", JSON.stringify(resultado));
        });
}

Number.prototype.format = function (n, x) {
    var re = '\\d(?=(\\d{' + (x || 3) + '})+' + (n > 0 ? '\\.' : '$') + ')';
    return this.toFixed(Math.max(0, ~~n)).replace(new RegExp(re, 'g'), '$&,');
};

function getUsuario() {
    return JSON.parse(localStorage.getItem("jsonUsr"));
}

function getUsuarioTest() {
    return JSON.parse(localStorage.getItem("jsonUsrTest"));
}

function getCampo(campo) {
    var usuario = getUsuario();
    return usuario[campo];
}

function buscar(key, value, jsonCarreras) {
    var resultadoBusqueda = [];
    for (var i in jsonCarreras) {
        if (getObjects(jsonCarreras[i], key, value).length != 0) {
            resultadoBusqueda.push(jsonCarreras[i])
        }
    }

    return resultadoBusqueda;
}

function buscar_grupo(key, value, jsonCarreras) {
    console.log("BUSCANDO POR " + key + " POR EL VALOR " + value);
    var resultadoBusqueda = [];
    for (var i in jsonCarreras) {
        if (getObjects(jsonCarreras[i], key, value).length != 0) {
            resultadoBusqueda.push(jsonCarreras[i])
        }
    }

    var jsonLinks = JSON.parse(localStorage.jsonLinksCategorias);
    var resultadoBusquedaGrupo = [];

    for (var i in jsonLinks) {
        if (getObjects(jsonLinks[i], "IdCategoria", resultadoBusqueda[0].Grupo_carreras).length != 0) {
            resultadoBusquedaGrupo.push(jsonLinks[i])
        }
    }
    return resultadoBusquedaGrupo;
}

function getObjects(obj, key, val) {
    var objects = [];
    for (var i in obj) {
        if (!obj.hasOwnProperty(i)) continue;

        if (typeof obj[i] == 'object') {
            objects = objects.concat(this.getObjects(obj[i], key, val));
        } else
            //if key matches and value matches or if key matches and value is not passed (eliminating the case where key matches but passed value does not)
            if (i == key && obj[i] == val || i == key && val == '') { //
                objects.push(obj);
            } else if (obj[i] == val && key == '') {
                //only add if the object is not already in the array
                if (objects.indexOf(obj) == -1) {
                    objects.push(obj);
                }
            }
    }
    return objects;
}

/*Funciones para la limpieza de caracteres BY SRP 09-10-2018*/
function removeAccents(str) {
    var accents = 'ÀÁÂÃÄÅàáâãäåßÒÓÔÕÕÖØòóôõöøÈÉÊËèéêëðÇçÐÌÍÎÏìíîïÙÚÛÜùúûüÑñŠšŸÿýŽž';
    var accentsOut = "AAAAAAaaaaaaBOOOOOOOooooooEEEEeeeeeCcDIIIIiiiiUUUUuuuuNnSsYyyZz";
    str = str.split('');
    str.forEach(function (letter, index) {
        var i = accents.indexOf(letter);
        if (i != -1) {
            str[index] = accentsOut[i];
        }
    })
    return str.join('');
}

function quitaAcentos(str) {
    for (var i = 0; i < str.length; i++) {
        //Sustituye "á é í ó ú"
        if (str.charAt(i) == "á") str = str.replace(/á/, "a");
        if (str.charAt(i) == "é") str = str.replace(/é/, "e");
        if (str.charAt(i) == "í") str = str.replace(/í/, "i");
        if (str.charAt(i) == "ó") str = str.replace(/ó/, "o");
        if (str.charAt(i) == "ú") str = str.replace(/ú/, "u");
    }
    return str;
}
/*End Funciones para la limpieza de caracteres BY SRP 09-10-2018*/

function guardaUsuario(valor, data) {
    if (localStorage.getItem("jsonUsr") === null) {
        var jsonnew = {};
        jsonnew[valor] = data;
        localStorage.setItem("jsonUsr", JSON.stringify(jsonnew));
        console.log('no existe el Json de usuario');
    } else {
        var viejo = JSON.parse(localStorage.getItem("jsonUsr"));
        viejo[valor] = data;
        localStorage.setItem("jsonUsr", JSON.stringify(viejo));
    }

}

function guardaUsuarioTest(valor, data) {
    if (localStorage.getItem("jsonUsrTest") === null) {
        var jsonnewTest = {};
        jsonnewTest[valor] = data;
        localStorage.setItem("jsonUsrTest", JSON.stringify(jsonnewTest));
        console.log('no existe el Json de usuario');
    } else {
        var viejoTest = JSON.parse(localStorage.getItem("jsonUsrTest"));
        viejoTest[valor] = data;
        localStorage.setItem("jsonUsrTest", JSON.stringify(viejoTest));
    }

}

function getNivelAnterior(nivel) {
    var nivelAnt = "";
    switch (nivel) {
        case 1: case 2: case 3: case 4:
            nivelAnt = 'Preparatoria'
            break;
        case 5: case 6:
            nivelAnt = 'Licenciatura'
            break;
        case 7:
            nivelAnt = 'Secundaria'
            break;
        default:
            nivelAnt = 'Licenciatura'
            break;
    }
    return nivelAnt;
}

function getNivelAnteriorPorCard(nivel) {
    var nivelAnt = "";
    switch (nivel) {
        case "LICENCIATURA": case "INGENIERIA": case "SALUD":
            nivelAnt = 'Preparatoria'
            break;
        case "POSGRADO":
            nivelAnt = 'Licenciatura'
            break;
        case "PREPARATORIA":
            nivelAnt = 'Secundaria'
            break;
        default:
            nivelAnt = 'Licenciatura'
            break;
    }
    return nivelAnt;
}

function IsMobile() {
    var Uagent = navigator.userAgent || navigator.vendor || window.opera;
    return (/(android|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|iris|kindle|lge |maemo|midp|mmp|mobile.+firefox|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows ce|xda|xiino|android|ipad|playbook|silk/i.test(Uagent) || /1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-/i.test(Uagent.substr(0, 4)));
};

/*Funciones Relacionadas con Lineas de Negocio y Campus BY SRP 09-10-2018*/
//Calcula el nivelInteres dependiendo la linea
function getNivelInteres(linea) {
    var value;
    switch (linea) {
        case "PREPARATORIA":
            value = "P";
            break;
        case "LICENCIATURA":
            value = "U";
            break;
        case "INGENIERIA":
            value = "U";
            break;
        case "SALUD":
            value = "U";
            break;
        case "POSGRADO":
        case "DIPLOMADO":
            value = "G";
            break;
        default:
            value = "U";
            break;
    }
    return value;
} //Termina get nivel interes

function campusDynamics(campus) {
    switch (campus) {
        case "ATZ":
            return "ATIZAPAN";
            break;
        case "REY":
            return "LOS REYES";
            break;
        case "MAR":
            return "MARINA";
            break;
        case "ECA":
            return "ECATEPEC";
            break;
        case "SUR":
            return "SUR";
            break;
        case "CUI":
            return "CUITLAHUAC";
            break;
        case "LEO":
            return "LEON";
            break;
        case "TOL":
            return "TOLUCA";
            break;
        case "GDL":
            return "GUADALAJARA";
            break;
        case "ONL":
            return "EN LINEA";
            break
        case "QRO":
            return "QUERETARO";
            break
        default:
            return "MARINA";
    }
}


/* FUNCIONES DE LINEA DE NEGOCIO
 * getLineasNegocio         -> Consulta JSON
 * render_lineas_negocio    -> Pinta todos los divs
 * set_linea_negocio        -> Cuando el usuario selecciona una linea de negocio
*/
function getLineasNegocio() {
    var lineasNegocio =
        [
            { "name": "Lic. Económico Administrativo", "id_name": "ECOADMIN", "order": "", "icono": "administrativas", "abvr": "LIC" },
            { "name": "Lic. Ciencias de la Salud", "id_name": "SALUD", "order": "", "icono": "salud", "abvr": "CS" },
            { "name": "Lic. Turismo y Gastronomía", "id_name": "TURISMO", "order": "", "icono": "turismo-gastronomia", "abvr": "BLUEM" },
            { "name": "Lic. Artes para el Diseño", "id_name": "DISENO", "order": "", "icono": "ciencias-artes", "abvr": "ING" },
            { "name": "Lic. Comunicación y Mercadotecnia", "id_name": "MERCADOTECNIA", "order": "", "icono": "comunicacion-merca", "abvr": "LIC" },
            { "name": "Preparatoria", "id_name": "PREPARATORIA", "order": "", "icono": "preparatoria", "abvr": "PREPA" },
            { "name": "Ingeniería", "id_name": "INGENIERIA", "order": "", "icono": "ingenierias", "abvr": "ING" },
            { "name": "Otra", "id_name": "LICENCIATURA", "order": "", "icono": "licenciaturas", "abvr": "LIC" }
        ];
    return lineasNegocio;
}

function render_lineas_negocio() {
    var lineas = getLineasNegocio();
    jQuery.each(lineas, function (index, value) {
        console.log('My array has at position ' + index + ', this value: ' + value.name);
        var html = '<div class="card card-calculadora"><div data-name="' + value.name + '" data-id_name="' + value.id_name + '" data-abrv="' + value.abrv + '" class="card-body tc w-100"><div style="font-size:10vh;" class="imagen-lineas-negocio imagen-lineas-cuadros cuadro icon-u icon-u-' + value.icono + '"></div><button class="btn btn-dtc boton-lineas-negocio waves-effect waves-light w-100"  role="button">'+ value.name +'</button></div></div>';
        jQuery('#render_lineas_negocio').append(html);
    });
}

function getCarreras(linea, campus) {
    var keySearch = "lineaweb";
    var arrayCarrerasPorLinea = [];
    var objJsonCarreras = JSON.parse(localStorage.getItem("jsonCarreras"));
    arrayCarrerasPorLinea = this.buscar(
        keySearch,
        linea,
        objJsonCarreras
    );

    if (linea == 'LICENCIATURA') {
        arrayCarrerasPorLineaSalud = this.buscar(keySearch, 'SALUD', objJsonCarreras);
        var arrayCarrerasPorLinea = arrayCarrerasPorLinea.concat(arrayCarrerasPorLineaSalud);
        console.log('AGREGANDO LAS DE SALUD');
    }

    /*Para identificar las carreras que se obtuvieron, sirve para la autoselección desde formulario */
    // localStorage.setItem("jsonCarrerasSeleccion", JSON.stringify(arrayCarrerasPorLinea));
    return arrayCarrerasPorLinea
}

function getCarrerasBien(categorias) {
    var objJsonCarreras = JSON.parse(localStorage.getItem("jsonLinksCategorias"));
    var categoriasno = new Array();
    Object.entries(categorias).forEach(function (key, value) {
        categoriasno.push(key[1]['Grupo_carreras']);
    });
    //console.log(categoriasno);
    var nombres = new Array();
    Object.entries(objJsonCarreras).forEach(function (key, value) {
        if (categoriasno.includes(key[1]['IdCategoria'])) {
            nombres.push(key[1]);
        }
    });
    return nombres;
}


/* FUNCIONES DE CARRERA
 * render_carreras         -> Pinta las carreras en el autocomplete 
*/
function render_carreras(carreras) {
    // console.log('las carreras');
    // console.log(carreras);
    var carrerasjson = [];
    jQuery.each(carreras, function (index, value) {
        //console.log(value);
        console.log('My array has at position ' + index + ', this value: ' + value.IdCategoria);
        item = {}
        item["value"] = value.IdCategoria;
        item["label"] = value.Categoria;
        //item["desc"] = value.Categoria;
        item["icon"] = value.IdDynamics;
        item["Categoria"] = value.Categoria;
        item["IdCategoria"] = value.IdCategoria;
        item["Link"] = value.Link;
        carrerasjson.push(item);
    });
    carrerasjson.sort(function (a, b) {
        if (a.Categoria.toLowerCase() < b.Categoria.toLowerCase()) return -1;
        if (a.Categoria.toLowerCase() > b.Categoria.toLowerCase()) return 1;
        return 0;
    });

    // console.log("IMPRIMO LAS CARRERAS ");
    // console.log(carrerasjson);
    //Manejo de las carreras cuando es prepa
    var linea_seleccionada = getUsuario().linea.id_name;
    /* Se agrega funcionalidad breadcrumbs AMC*/
    jQuery('#bread1').removeClass('breadActive');
    jQuery('#bread2').addClass('breadActive');
    if (linea_seleccionada == "PREPARATORIA") {
        guardaUsuario('carrera', carrerasjson[0]);
        //guardaUsuarioTest('carrera', carrerasjson[0]);
        //alert("Caso Prepa: " + carrerasjson[0]);

        /*Guardar en la cookie la carrera By SRP 08-10-2018*/
        //appendCookie('c_form_data', 'carrera', carrerasjson[0], 1);

        // if(paso4){
        //     var objCookie = JSON.parse(decodeURIComponent(getCookie('c_form_data')));
        //     var objectDataLayer = {
        //         'event' : 'InformationForm',   //Static data
        //         'formStep' : 'Paso 4',   //Dynamic data
        //         'Estado' : (autocompleteDesktop) ? 'definir' : objCookie.estado.toLowerCase(),   //CD4
        //         'InterLvl' : getUsuario().linea.name.toLowerCase(),  //CD 5
        //         'Period' : '',  //CD 6
        //         'carrera' : getUsuario().carrera.Link.toLowerCase(),  //CD 7
        //         'campus' : '',  //CD 8
        //         'Formulario' : 'Registro Formulario',  //CD 16
        //         'RMode': '',   //CD25
        //         'prom': '',   //CD26
        //         'formDev' : 'Desktop'   // Dynamic data
        //     };

        //     if(!autocompleteDesktop){
        //         dataLayer.push(objectDataLayer);
        //     } else {
        //         dataLayerAutoComplete.push(objectDataLayer);
        //     }
        // }

        // paso4 = false; //Desactivación para otras consultas posteriores

        jQuery('.rs-carrera').text(getCampo('carrera').Link.replace(/-/g, ' ').replace(/\//g, ''));
        render_modalidades(getModalidades(carrerasjson[0].IdCategoria));
        muestraPaso('business-line', 'container_carrera_modalidad');
        jQuery("#carrera").addClass("hide");
        jQuery("#carrera").addClass("hide-prepa");
        jQuery("#padre_tutor").removeClass("hide");

        /*Ya no se requiere ya que se mostrara la modalidad y campus directo By SRP 03-07-2019*/
        //jQuery("#modalidad").removeClass("hide");
        /*End Ya no se requiere ya que se mostrara la modalidad y campus directo By SRP 03-07-2019*/
        regresaPaso('container_carrera_modalidad', 'business-line');
        $('.steps').text('4');
    } else {
        jQuery("#carrera").removeClass("hide");
        jQuery("#carrera").removeClass("hide-prepa");
        jQuery("#padre_tutor").addClass("hide");
    }

    var accentMap = {
        "á": "a",
        "é": "e",
        "í": "i",
        "ó": "o",
        "ú": "u"
    };
    var normalize = function (term) {
        var ret = "";
        for (var i = 0; i < term.length; i++) {
            ret += accentMap[term.charAt(i)] || term.charAt(i);
        }
        return ret;
    };

    console.log("IMPRIMO LAS CARRERAS ");
    console.log(carrerasjson);
    $("#project").autocomplete({
        minLength: 0,
        // source: carrerasjson,
        source: function (request, response) {
            /* Funcionalidad para hacer la búsqueda sin importar los acentos */
            var matcher = new RegExp($.ui.autocomplete.escapeRegex(request.term), "i");
            response($.grep(carrerasjson, function (value) {
                value = value.label || value.value || value;
                return matcher.test(value) || matcher.test(normalize(value));
            }));
        },
        messages: {
            noResults: '',
            results: function () { $('.ui-helper-hidden-accessible').empty() }
        },
        focus: function (event, ui) {
            $("#project").val(ui.item.label);
            return false;
        },
        select: function (event, ui) {
            console.log(ui);
            $(this).blur();
            $("#project").val(ui.item.label);
            $("#project-id").val(ui.item.value);
            //$("#project-description").html(ui.item.desc);
            $("#project-icon").attr("src", "images/" + ui.item.icon);
            //let oferta =[{ "Categoria": ui.item.Categoria, "IdCategoria": ui.item.IdCategoria, "Link": ui.item.Link }];
            guardaUsuario('carrera', ui.item);

            // jQuery('.rs-carrera').text(getCampo('carrera').Link);
            jQuery('.rs-carrera').text(getCampo('carrera').Link.replace(/-/g, ' ').replace(/\//g, ''));
            render_modalidades(getModalidades(ui.item.IdCategoria));

            // if(paso4){
            //     var objCookie = JSON.parse(decodeURIComponent(getCookie('c_form_data')));
            //     var objectDataLayer = {
            //         'event' : 'InformationForm',   //Static data
            //         'formStep' : 'Paso 4',   //Dynamic data
            //         'Estado' : (autocompleteDesktop) ? 'definir' : objCookie.estado.toLowerCase(),   //CD4
            //         'InterLvl' : getUsuario().linea.name.toLowerCase(),  //CD 5
            //         'Period' : '',  //CD 6
            //         'carrera' : getUsuario().carrera.Link.toLowerCase(),  //CD 7
            //         'campus' : '',  //CD 8
            //         'Formulario' : 'Registro Formulario',  //CD 16
            //         'RMode': '',   //CD25
            //         'prom': '',   //CD26
            //         'formDev' : 'Desktop'   // Dynamic data
            //     };

            //     if(!autocompleteDesktop){
            //         dataLayer.push(objectDataLayer);
            //     } else {
            //         dataLayerAutoComplete.push(objectDataLayer);
            //     }
            // }

            // paso4 = false; //Desactivación para otras consultas posteriores

            /*Ya no se requiere ya que se mostrara la modalidad y campus directo By SRP 03-07-2019*/
            //$('#modalidad').removeClass('hide');
            /*End Ya no se requiere ya que se mostrara la modalidad y campus directo By SRP 03-07-2019*/

            return false;
        }
    })
        .autocomplete("instance")._renderItem = function (ul, item) {
            return $("<li>")
                .append("<div>" + item.label + "</div>")
                //.append("<div>" + item.label + "<br>" + item.desc + "</div>")
                .appendTo(ul);
        };

    // if (linea_seleccionada == "PREPARATORIA"){
    //     $('.ui-menu-item div').first().click();
    // }

}

function avanzaPaso() {
    var faltantes = 0;
    /* Controla todos los inputs que faltan */
    if (jQuery('#project').val() == "") {
        faltantes++;
    }

    if (jQuery('#render_campus').val() == 0) {
        faltantes++;
    }

    if (jQuery('#render_modalidad').val() == 0) {
        faltantes++;
    }

    var linea_seleccionada = getUsuario().linea.id_name;
    if (linea_seleccionada == "PREPARATORIA") {
        if (jQuery('#render_padre_tutor').val() == "Selecciona") {
            faltantes++;
        }
    }

    if (faltantes > 0) {
        console.log("NO PUEDES AVANZAR");
    } else {

        cambioTextoThankYou();
        // Funcionalidad Breadcrumbz AMC
        jQuery('#bread2').removeClass('breadActive');
        jQuery('#bread3').addClass('breadActive');

        off();

        // if(paso5){

        //     var objCookie = JSON.parse(decodeURIComponent(getCookie('c_form_data')));
        //     if(autocompleteDesktop) {
        //         $.each(dataLayerAutoComplete,function(key,value){
        //             value['Estado'] = objCookie.estado.toLowerCase();
        //             dataLayer.push(value);
        //         });
        //     }
        //     var pTutor
        //     if (typeof objCookie.aPaternoTutor == 'undefined') {
        //         pTutor = ""
        //     } else{
        //         pTutor = objCookie.aPaternoTutor
        //     }
        //     dataLayer.push({
        //         'event' : 'InformationForm',   //Static data
        //         'formStep' : 'Paso 5',   //Dynamic data
        //         'Estado' : objCookie.estado.toLowerCase(),   //CD4
        //         'InterLvl' : getUsuario().linea.name.toLowerCase(),  //CD 5
        //         'Period' : '',  //CD 6
        //         'carrera' : getUsuario().carrera.Link.toLowerCase(),  //CD 7
        //         'campus' : campusNombreLargo(getUsuario().campus).toLowerCase(),  //CD 8
        //         'Formulario' : 'Registro Formulario',  //CD 16
        //         'RMode': modalidadtxt(getUsuario().modalidad.modalidad).toLowerCase(),   //CD25
        //         'prom': '',   //CD26
        //         'formDev' : 'Desktop',   // Dynamic data
        //         'tutor': pTutor   // Dynamic data
        //     });
        // }

        // paso5 = false; //Desactivación para otras consultas posteriores

        // if(getUsuario().campus === "QRO" || getUsuario().campus === "LEO"){
            construyeInputciclos();
            muestraPaso('container_carrera_modalidad','secCiclos');
            regresaPaso('secCiclos','container_carrera_modalidad');

            if(jQuery("#secCiclos .row").children().length == 1){
                jQuery("#secCiclos .row button")[0].click();
            }
        // } else{
        //     guardaDatosCookie();
        //
        //     jQuery(".modal-header-formulario").hide();
        //     jQuery(".modal-header-thankyou").hide();
        //     jQuery(".img-header-thankyou").hide();
        //     jQuery("#enviarTrd").hide();
        //     jQuery("#body-form-todo").hide();
        //     jQuery(".modal-body-thankyou").show();
        //     // jQuery(".tercer-texto-thankyou").html(cookie_form_values["carrera"]);
        //
        //     // jQuery("#nombre-thankyou").html(cookie_form_values["nombre"]);
        //     /*VALORES DE PRUEBA */
        //     jQuery(".tercer-texto-thankyou").html(getUsuario().carrera.Link);
        //     jQuery('.second-sub-desk').text("UNITEC Campus " + campustxt(jQuery('#render_campus').val()));
        //     jQuery('#thank-linea div.col-10').text("");
        //     jQuery('#thank-linea div.col-10').text(getUsuario().carrera.Link);
        //     jQuery('#thank-modalidad div.col-10').text("");
        //     jQuery('#thank-modalidad div.col-10').text("Modalidad " + modalidadtxt(getUsuario().modalidad.modalidad));
        //     jQuery('#thank-campus div.col-10').text("");
        //     jQuery('#thank-campus div.col-10').text("Campus " + campustxt(jQuery('#render_campus').val()));
        //     // jQuery('#thank-ciclo').text("");
        //     // jQuery('#thank-ciclo').text("19-2");
        //
        //     /*VALORES DE PRUEBA */
        //
        //     jQuery("#head_close_modal").hide();
        //     jQuery(".titulo-logo").show();
        //
        //     jQuery("#modal_frm_app").modal();
        //     $('#calculadora-css').attr("disabled", "disabled");
        //     $('#calculadora-css').prop("disabled", true);
        //     $('#aplicativo-formulario').hide();
        //     $(".todoElBody").show();
        //     // $('.main-calculadora-todo').show();
        //
        //     /*Envio de Datos a Ciclo Activo*/
        //     sendFormularioTradicional();
        //     /*End Envio de Datos a Ciclo Activo*/
        //
        //
        // }

    }

}

/* FUNCIONES DE MODALIDADES
* getModalidades         -> OBTIENE MODALIDADES Y RECIBE EL ID DE CATEGORIA
* render_MODALIDADES  -> Obtienes las modalidades y las pinta 
*/

function getModalidades(categoria) {
    var keySearch = "Grupo_carreras";
    var arrayCarrerasPorLinea = [];
    var objJsonCarreras = JSON.parse(localStorage.getItem("jsonCarreras"));
    arrayCarrerasPorLinea = buscar(
        keySearch,
        categoria,
        objJsonCarreras
    );

    return arrayCarrerasPorLinea
}

function render_modalidades(modalidades) {
    console.log('render_modalidades');
    console.log(modalidades);
    console.log('Existen:' + modalidades.length);
    // changeAddInCareers(modalidades[0]);

    var modalidadesSize = modalidades.length;
    /*******MODALIDADES******/
    if (modalidades.length == 1) {
        var html = '<option selected data-Grupo_carreras="' + modalidades[0].Grupo_carreras + '" data-IdDynamics="' + modalidades[0].IdDynamics + '" data-campus="' + modalidades[0].campus + '"  data-catCosto="' + modalidades[0].catCosto + '" data-interes="' + modalidades[0].interes + '" data-lineaweb="' + modalidades[0].lineaweb + '" data-modalidad="' + modalidades[0].modalidad + '"  data-nombre="' + modalidades[0].nombre + '" value="' + modalidades[0].modalidad + '">' + modalidadtxt(modalidades[0].modalidad) + '</option>';
    } else {
        console.log('mas de 3');
        var html = '<option value="0">Modalidad</option>';
        jQuery.each(modalidades, function (index, value) {
            console.log('My array has at position ' + index + ', this value: ' + value.modalidad + modalidadtxt(value.modalidad));
            html += '<option data-Grupo_carreras="' + value.Grupo_carreras + '" data-IdDynamics="' + value.IdDynamics + '" data-campus="' + value.campus + '"  data-catCosto="' + value.catCosto + '" data-interes="' + value.interes + '" data-lineaweb="' + value.lineaweb + '" data-modalidad="' + value.modalidad + '"  data-nombre="' + value.nombre + '" value="' + value.modalidad + '">' + modalidadtxt(value.modalidad) + '</option>';
        });
    }
    jQuery('#render_modalidad').html(html).delay(200).queue(function (next) {
        if (modalidadesSize == 1) {
            jQuery('#render_modalidad').next().addClass('inactive');
        } else if (modalidadesSize > 1) {
            jQuery('#render_modalidad').next().removeClass('inactive');
        }
        next();
    });

    /*******CAMPUS******/
    console.log('todos los campus que tienen esta carrera');
    var campus = [];
    jQuery.each(modalidades, function (index, value) { campus.push(value.campus); }); //Guarda los arreglos de campus
    var resultArray = Array.prototype.concat.apply([], campus);
    allcampus = resultArray.join(',');
    var b = unique(allcampus.split(",")); // Array de campus unicos 
    console.log(b);
    b.sort();
    var html = '<option value="0"> Campus</option>';
    if (b.length == 1) {
        html += '<option selected value="' + b[0] + '">' + campustxt(b[0]) + '</option>';
    } else {
        jQuery.each(b, function (index, value) {
            html += '<option value="' + value + '">' + campustxt(value) + '</option>';
        });
    }

    jQuery('#render_campus').html(html).delay(300).queue(function (next) {
        /* If cuando solo hay un campus para que se autoseleccione */
        if (b.length == 1) {
            jQuery('#render_campus').next().addClass('inactive');
            /* If cuando se autoselecciona la modalidad */
            if (jQuery('#render_modalidad').next().hasClass('inactive')) {
                regresoAnterior = false;
                /* Funcion cuando se quiere avanzar automáticamente a promedio */
                // setTimeout(function () { muestraPaso('modalidad', 'promedio'); regresoAnterior = false; }, 500);
                /* Funcion cuando se avanza hasta dar click en el boton */
                setTimeout(function () { regresoAnterior = false; }, 500);
                // muestraPaso('modalidad', 'promedio');
                guardaUsuario('campus', jQuery('#render_campus').val());
                guardaUsuario('modalidad', jQuery('#render_modalidad option:selected').data());


                /*Guardar en la cookie la Carrera By SRP 08-10-2018*/
                appendCookie('c_form_data', 'campus', jQuery('#render_campus').val(), 1);

                var nivelAnterior = getNivelAnterior(getCampo('modalidad').interes);
                jQuery('.rs-canterior').text(nivelAnterior);
                off();
                var checkOverlaySend = $("#overlay").attr("data-send");
                if (checkOverlaySend == "1") {
                    // calcular_uno();
                    if (!jQuery('inicio').hasClass('inicio')) {
                        $("#overlay").attr("data-send", "2");
                        // $('#render_campus').next().next().children().first().click();
                        // envioProcWeb();
                    }
                } else if (checkOverlaySend == "2") {
                    // calcular_uno();
                    if (!jQuery('inicio').hasClass('inicio')) {
                        $("#overlay").attr("data-send", "2");
                        // $('#render_campus').next().next().children().first().click();
                        // envioProcWeb();
                    }
                } else {
                    if (!jQuery('inicio').hasClass('inicio')) {
                        $("#overlay").attr("data-send", "1");
                    }
                }
                // calcular_uno();
            }
            // $('#render_campus').next().next().children().first().click();
        } else {
            /* En caso de que haya más de un campus se le remueve todo lo preseleccionado */
            jQuery('#render_campus').removeAttr('data-preselected');
            jQuery('#render_campus').next().removeClass('inactive');
        }
        next();
    });
    render_selects();
}


/* FUNCIONES GENERALES
 * guardaUsuario   -> Guarda en localstorage todo el objeto en formato JSON
*/

function modalidadtxt(num) {
    switch (parseInt(num)) {
        case 1: return "Presencial"; break;
        case 2: return "Ejecutiva"; break;
        case 3: return "En línea"; break;
        case 4: return "Prepa Vespertina"; break;
        case 5: return "Prepa UNAM"; break;
        case 6: return "Prepa UNITEC"; break;
    }
}

function campustxt(campus) {
    switch (campus) {
        case 'ATZ': return "Atizapán, Edo. Méx."; break;
        case 'MAR': return "Marina, CDMX"; break;
        case 'ECA': return "Ecatepec, Edo. Méx."; break;
        case 'GDL': return "Guadalajara"; break;
        case 'LEO': return "León, Gto"; break;
        case 'SUR': return "Sur, CDMX"; break;
        case 'CUI': return "Cuitláhuac, CDMX"; break;
        case 'TOL': return "Toluca, Edo. Méx"; break;
        case 'QRO': return "Querétaro, QRO"; break;
        case 'REY': return "Los Reyes, CDMX"; break;
        case 'ONL': return "En Línea"; break;

    }
}

function campusNombreLargo(campus) {
    var value;
    switch (campus) {
        case "ATZ":
            value = "Atizapán";
            break;
        case "CUI":
            value = "Cuitláhuac";
            break;
        case "ECA":
            value = "Ecatepec";
            break;
        case "MAR":
            value = "Marina";
            break;
        case "SUR":
            value = "Sur";
            break;
        case "LEO":
            value = "León";
            break;
        case "TOL":
            value = "Toluca";
            break;
        case "ONL":
            value = "En Línea";
            break;
        case "GDL":
            value = "Guadalajara";
            break;
        case "QRO":
            value = "Querétaro";
            break;
        case "REY":
            value = "Los Reyes";
            break;
    }
    return value;
}

function unique(array) {
    return array.filter(function (el, index, arr) {
        return index == arr.indexOf(el);
    });
}


function render_selects() {
    jQuery('.select-selected, .select-items').remove();
    var x, i, j, selElmnt, a, b, c;
    /*look for any elements with the class "custom-select":*/
    x = document.getElementsByClassName("custom-select");
    for (i = 0; i < x.length; i++) {
        selElmnt = x[i].getElementsByTagName("select")[0];
        /*for each element, create a new DIV that will act as the selected item:*/
        a = document.createElement("DIV");
        a.setAttribute("class", "select-selected w-100");
        console.log("pasando el primero");
        a.innerHTML = selElmnt.options[selElmnt.selectedIndex].innerHTML;
        x[i].appendChild(a);
        /*for each element, create a new DIV that will contain the option list:*/
        b = document.createElement("DIV");
        b.setAttribute("class", "select-items select-hide");
        for (j = 1; j < selElmnt.length; j++) {
            /*for each option in the original select element,
            create a new DIV that will act as an option item:*/
            c = document.createElement("DIV");
            console.log("pasando el segundo");
            c.innerHTML = selElmnt.options[j].innerHTML;
            c.addEventListener("click", function (e) {
                /*when an item is clicked, update the original select box,
                and the selected item:*/
                var y, i, k, s, h;
                s = this.parentNode.parentNode.getElementsByTagName("select")[0];
                h = this.parentNode.previousSibling;
                for (i = 0; i < s.length; i++) {
                    if (s.options[i].innerHTML == this.innerHTML) {
                        s.selectedIndex = i;
                        h.innerHTML = this.innerHTML;
                        y = this.parentNode.getElementsByClassName("same-as-selected");
                        for (k = 0; k < y.length; k++) {
                            y[k].removeAttribute("class");
                        }
                        this.setAttribute("class", "same-as-selected");
                        break;
                    }
                }
                h.click();
            });
            b.appendChild(c);
        }
        x[i].appendChild(b);



        b.addEventListener("click", function (e) {
            var idSelect = jQuery(this).parent().find('select').prop('id');
            updateSelect(idSelect);

        });

        a.addEventListener("click", function (e) {
            /*when the select box is clicked, close any other select boxes,
            and open/close the current select box:*/
            e.stopPropagation();
            closeAllSelect(this);
            this.nextSibling.classList.toggle("select-hide");
            this.classList.toggle("select-arrow-active");
            console.log('elegiste:')
            console.log(e);



        });
    }
    checkAllInputs();
}
function updateSelect(id) {
    setTimeout(function () { jQuery("#" + id).change(); render_selects(); }, 200);

}

function closeAllSelect(elmnt) {
    /*a function that will close all select boxes in the document,
    except the current select box:*/
    var x, y, i, arrNo = [];
    x = document.getElementsByClassName("select-items");
    y = document.getElementsByClassName("select-selected");
    for (i = 0; i < y.length; i++) {
        if (elmnt == y[i]) {
            arrNo.push(i)
        } else {
            y[i].classList.remove("select-arrow-active");
        }
    }
    for (i = 0; i < x.length; i++) {
        if (arrNo.indexOf(i)) {
            x[i].classList.add("select-hide");
        }
    }
}
function autoCompleteFormulario(nodo) {

    console.log("AQUI ESTAN LAS OPCIONES");
    var data_cookie_form = nodo;
    console.log(data_cookie_form);
    console.log(data_cookie_form.lineaweb);
    if (data_cookie_form.lineaweb != undefined) {
        autocompleteDesktop = true;
        console.log(data_cookie_form.lineaweb);
        $("div").find("[data-id_name=" + data_cookie_form.lineaweb + "]").mouseup();
        var busqueda_grupo = buscar_grupo("IdDynamics", data_cookie_form.IdDynamics, JSON.parse(localStorage.jsonCarreras));
        console.log("LA BUSQUEDA DEL GRUPO");
        console.log(busqueda_grupo);
        var categoria_grupo = busqueda_grupo[0].Categoria;
        console.log("LA BUSQUEDA DEL DIV");
        $('#project').autocomplete('search', $('#project').val());
        console.log($(".ui-menu-item-wrapper:contains('" + categoria_grupo + "')"));
        $(".ui-menu-item-wrapper:contains('" + categoria_grupo + "')").first().click();
        console.log("BUSQUEDA DE MODALIDAD");

    }
}


/*End Funciones Relacionadas con Lineas de Negocio y Campus BY SRP 09-10-2018*/

/*Funciones para el manejo de Cookies BY SRP 09-10-2018*/
function setCookieForm(cname, cvalue, exdays) {
    var d = new Date();
    d.setTime(d.getTime() + (exdays * 24 * 60 * 60 * 1000));
    var expires = "expires=" + d.toUTCString();
    document.cookie = cname + "=" + cvalue + "; " + expires + ";path=/";
}

function getCookie(cname) {
    var name = cname + "=";
    var ca = document.cookie.split(';');
    for (var i = 0; i < ca.length; i++) {
        var c = ca[i];
        while (c.charAt(0) == ' ') c = c.substring(1);
        if (c.indexOf(name) == 0) return c.substring(name.length, c.length);
    }
    return "";
}

function checkCookie(valor) {
    var user = getCookie(valor);
    if (user != "") {
        return true;
    }
    else {
        return false;
    }
}

function readCookie(name) {
    var nameEQ = name + "=";
    var ca = document.cookie.split(';');
    for (var i = 0; i < ca.length; i++) {
        var c = ca[i];
        while (c.charAt(0) == ' ') c = c.substring(1, c.length);
        if (c.indexOf(nameEQ) == 0) return c.substring(nameEQ.length, c.length);
    }
    return null;
}


/** Va agregando valores a una Cookie ya existente 
 * @param Cookie name, Value to add, Value of cookie, Expiración
 * @return Crea la cookie con el nuevo valor
 */
function appendCookie(nombre_cookie, clave, valor, exdays) {
    var objCookie;
    var cadena_cookie = "";
    var cookieActual = decodeURIComponent(getCookie(nombre_cookie));
    var arrLength = Object.keys(JSON.parse(cookieActual)).length;

    //alert("TOTAL: " + totalElementos);

    var valida_reemplazo = false;
    var tmpLen = 0;

    jQuery.each(JSON.parse(cookieActual), function (index, value) {
        tmpLen++;
        console.log("cookie: " + index + " Valor: " + value);

        if (index != "" || index != "null" || typeof index != "undefined" || index != null) {
            //Validamos si la clave del valor de la cookie es igual al valor pasado como
            //parametro al metodo y lo reemplazamos
            if (tmpLen != arrLength) {
                //Si es el mismo valor para la cookie se reemplaza
                if (index == clave) {
                    value = valor;
                    valida_reemplazo = true;
                }
                cadena_cookie += '"' + index + '":"' + value + '",';
            } else {
                //Si es el mismo valor para la cookie se reemplaza
                if (index == clave) {
                    value = valor;
                    valida_reemplazo = true;
                }
                //Si es el ultimo elemento y ademas no fue reemplazado el valor
                //concatenamos el nuevo valor
                if (valida_reemplazo == true) {
                    cadena_cookie += '"' + index + '":"' + value + '"';
                } else {
                    cadena_cookie += '"' + index + '":"' + value + '",';
                    cadena_cookie += '"' + clave + '":"' + valor + '"';
                }

            }

        }

    });

    var contactb = "{" + cadena_cookie + "}";
    cadena_cookie = "";

    if (checkCookie('c_form_data') == true) {
        removeCookie('c_form_data');
    }
    var cookieNew = setCookieForm(nombre_cookie, encodeURIComponent(contactb), 1);
}

/** Eliminar una Cookie existente
 * @param String name
 * @return 
 */
function removeCookie(nombre_cookie) {
    return document.cookie = nombre_cookie + "=;expires=Thu, 01 Jan 1970 00:00:00 GMT;path=/";
}


/** Comprobar si la url trae UTM_CAMPAING
 * @param String name
 * @return String
 */
function getParameterByName(name) {
    name = name.replace(/[\[]/, "\\[").replace(/[\]]/, "\\]");
    var regex = new RegExp("[\\?&]" + name + "=([^&#]*)");
    if(location.search === ""){
        results = regex.exec(location.hash);
    } else {
        results = regex.exec(location.search);
    }
    return results === null ? false : decodeURIComponent(results[1].replace(/\+/g, " "));
}

/*End Funciones para el manejo de Cookies BY SRP 09-10-2018*/

function sendDatalayer(url_val, event) {
    if (checkCookie('frmunitec') != true) {
        dataLayer.push({
            'url_value': url_val, //dato dinámico
            'event': event//dato estático
        });
    }
}

function muestraPaso(actual, seccion) {

    if (jQuery('body').hasClass('inicio')) {                              //Verifica que este extendido el formulario

        var linea_seleccionada = getUsuario().linea.id_name;
        console.log(linea_seleccionada);

        jQuery('.quest.p-activa').addClass('an-slideUp');               //Agrega SlideUp a la Quest Activa

        setTimeout(function () {
            jQuery('.quest.p-activa').addClass('hide');                 //Esconde la preg Activa 
            jQuery('#' + seccion).removeClass('hide').addClass('p-activa').addClass('an-slideDownToUp');
            jQuery('#' + seccion).removeClass('an-slideUp').removeClass('an-slideDownToUp');

        }, 400);

        setTimeout(function () {
            console.log("Estmoas en:" + actual + "Se va a ir a:" + seccion);
            jQuery('#' + actual).removeClass('an-slideUp');
        }, 400);

        switch (seccion) {
            case "container_carrera_modalidad":
                regresaPaso('container_carrera_modalidad', 'business-line');
                break;
        }

    }

}

function regresaPaso(actual, movingTo) {
    if (jQuery('body').hasClass('inicio')) {
        if (actual == 'promedio' && regresoAnterior == false) {
            console.log('Omito todo');

            /* Se implementa el regreso a carrera */
            jQuery('.controls').removeClass('d-none');
            jQuery('.controls a').unbind();
            jQuery('.controls a, #bread2').on('click', function () {
                // Funcionalidad Breadcrumbz AMC
                jQuery('#bread1').removeClass('breadActive');
                jQuery('#bread2').addClass('breadActive');
                jQuery('.select-items').empty();
                // jQuery('#render_modalidad, #render_campus').empty();
                muestraPaso('promedio', 'container_carrera_modalidad');
            });
            $('.steps').text('6');
        } else {
            console.log("Moviendo a " + movingTo);
            switch (movingTo) {
                case "business-line":
                    jQuery('.controls').removeClass('d-none');
                    $('.steps').text('4');
                    jQuery('.controls a').unbind();
                    jQuery('.controls a, #bread1').on('click', function () {
                        // Funcionalidad Breadcrumbz AMC
                        jQuery('#bread2').removeClass('breadActive');
                        jQuery('#bread1').addClass('breadActive');
                        muestraPaso(actual, movingTo);
                        jQuery('.controls').addClass('d-none');
                        jQuery('#project').val("");
                        jQuery("#carrera").removeClass("hide");

                        /*Ya no se requiere ya que se mostrara la modalidad y campus directo By SRP 03-07-2019*/
                        //jQuery("#modalidad").addClass("hide");
                        /*End Ya no se requiere ya que se mostrara la modalidad y campus directo By SRP 03-07-2019*/
                        jQuery("#padre_tutor").addClass("hide");
                    });
                    break;
                case "carrera":
                    jQuery('.controls').removeClass('d-none');
                    jQuery('.controls a').unbind();
                    jQuery('.controls a').on('click', function () {
                        muestraPaso(actual, movingTo);
                    });
                    break;
                default:
                    jQuery('.controls').removeClass('d-none');
                    jQuery('.controls a').unbind();
                    jQuery('.controls a').on('click', function () {
                        muestraPaso(actual, movingTo);
                    });
                    break;
            }
        }
    }


}

function select(clase, el) {
    $(clase).each(function () { $(this).removeClass('selected') });
    //Si no se especifica la clase de btn-dual-y causa conflictos
    if (!pasoFinalCalculadora && clase != ".btn-dual-y") {
        setTimeout(function () { $(el).addClass('selected'); }, 500);
    } else {
        $(el).addClass('selected');
    }


}

function on() {
    //document.getElementById("overlay").style.display = "block";
    jQuery('#results').addClass('blur');
    jQuery('#overlay').fadeIn();
}

function off() {
    //document.getElementById("overlay").style.display = "none";
    jQuery('#results').removeClass('blur');
    jQuery('#overlay').fadeOut();
}

function checkAllInputs() {
    var faltantes = 0;
    /* Controla todos los inputs que faltan */
    if (jQuery('#project').val() != "") {
        jQuery('#calc-apply-career').hide();
        jQuery('#project').removeClass('missing-select');
    } else {
        jQuery('#calc-apply-career').show();
        jQuery('#project').addClass('missing-select');
        on();
        faltantes++;
    }

    if (jQuery('#render_campus').val() != 0) {
        jQuery('#calc-apply-campus').hide();
        jQuery('#render_campus').next().removeClass('missing-select');
        jQuery('.rs-campus').text('Campus ' + jQuery('#render_campus option:selected').text());
    } else {
        jQuery('#calc-apply-campus').show();
        jQuery('#render_campus').next().addClass('missing-select');
        on();
        faltantes++;
    }

    if (jQuery('#render_modalidad').val() != 0) {
        jQuery('#calc-apply-type').hide();
        jQuery('#render_modalidad').next().removeClass('missing-select');
        jQuery('.rs-mod').text('Modalidad ' + jQuery('#render_modalidad option:selected').text());
    } else {
        jQuery('#calc-apply-type').show();
        jQuery('#render_modalidad').next().addClass('missing-select');
        on();
        faltantes++;
    }

    var linea_seleccionada = getUsuario().linea.id_name;
    if (linea_seleccionada == "PREPARATORIA") {
        if (jQuery('#render_padre_tutor').val() != "Selecciona") {
            jQuery('#calc-apply-padre').hide();
            jQuery('#render_padre_tutor').next().removeClass('missing-select');
        } else {
            jQuery('#calc-apply-padre').show();
            jQuery('#render_padre_tutor').next().addClass('missing-select');
            on();
            faltantes++;
        }
    } else {
        jQuery('#calc-apply-padre').hide();
    }


    if (faltantes == 0 && jQuery('body').hasClass('inicio')) {
        if (getUsuario().modalidad != undefined) {
            if (getUsuario().modalidad.catcosto != "DENT") {
                jQuery('.btn-dual').last().click();
            }
        }
    }

    // if(faltantes == 0){
    //     if(getUsuario().modalidad != undefined){
    //         if(getUsuario().modalidad.catcosto == "DENT"){
    //             console.log("DANDO CLICK EN LAS 5 MATERIAS");
    //             jQuery('.btn-circ').eq(4).click();
    //         }
    //     }
    // }
}


/* SEGUNDA PARTE JS */


function cambioTextoThankYou() {
    //alert( getCampo('modalidad').catcosto );
    if (location.href.indexOf("impulsa") !== -1) {
        console.log("impulsa");
        jQuery("#continuarTrdThank").html("CONOCE LAS VACANTES");
        jQuery("#div-btn-continuar a").attr({ "href": "https://www.unitec.mx/folleto/impulsa.pdf", "target": "_blank" });
        jQuery("#descripcionThanks").html("A través de UNITEC Te Impulsa, descubrirás que trabajar y estudiar se vuelve más fácil cuando un equipo de profesionales de apoyan para llegar a tu meta. En breve nos pondremos en contacto contigo para ofrecerte más información.");
        setCookieForm('reg_impulsa', 'cookie', true, 1);
    } else if (location.href.indexOf("testvocacional") !== -1 || location.href.indexOf("orientacion-profesional") !== -1) {
        console.log("Orientacion Profesional");
        var html = "";
        
        html += "<div id='contentTankyouDesk' class='container'>";
        // html += "<h4 class='title-thankyou'>&iexcl;Gracias por registrarte!</h4>";
        html += "<img alt='' class='img-fluid centrar flex-custom' id='image-testvocacional' src='/wp-content/uploads/2018/01/unitec-campus-sur-principal-h.jpg'>";
        html += "<h5 class='h2 subtitle-thankyou'>Descubre cu&aacute;les son tus aptitudes y qu&eacute; profesi&oacute;n se ajusta a tu perfil. Espera unos instantes y comenzar&aacute; el test vocacional.</h5>";
        html += "<h5 class='h2 subtitle-thankyou'>Si no puedes acceder, da clic en el bot&oacute;n de abajo.</h5>";
        html += "<a id='btn-test' class='d-none' data-gtm-tr='Call to action' data-gtm-accion='envio a pagina' data-gtm-etiqueta='app test vocacional' data-gtm-location='Thankyou page'><button type='button' class='button-primaryn'>Comienza el test</button></a>";
        // html += "<a data-gtm-accion='intención' data-gtm-depto='' data-gtm-seccion='formulario' data-gtm-tr='evWhats' href='https://clxt.ch/unitecg' target='_blank'><button type='button' class='button-primaryn btn-whatsapp'><i class='icon-u icon-u-whatsapp'></i>&nbsp;&nbsp;Resuelve tus dudas</button></a>";
        html += "</div>";
        $("#business-line").empty();
        $("#business-line").append(html);
    } else if ( getCampo('modalidad').catcosto == "PREPAUNAM" ) {
        jQuery("#continuarTrdThank").html("CALCULA TU COLEGIATURA");
        jQuery("#div-btn-continuar a#continuarTrdThank").attr("href", "/calcula-tu-beca");
        jQuery("#descripcionThanks").html("Conoce la colegiatura de la Prepa UNAM y todos los beneficios que tendrás como estudiante de la Universidad Tecnológica de México.");
    } else {
        jQuery("#continuarTrdThank").html("CALCULA TU BECA");
        jQuery("#div-btn-continuar a#continuarTrdThank").attr("href", "/calcula-tu-beca");
        jQuery("#descripcionThanks").html("Impulsamos tu esfuerzo recompensándote con una Beca Académica de Primer Ingreso. Conoce el porcentaje que te corresponde según tu promedio.");
    }
}

var getImgCampus = true;

function sendFormularioTradicional(changeGetImg, dispositivo) {
    //var url_multimedia_thankyou_predeterminada = "assets/sidenav.jpg";
    //Si no esta la imagen destacada se asigna la imagen predeterminada
    console.log("ENVIO DE FORMULARIO TRADICIONAL");
    //var url_multimedia_thankyou = jQuery("#h_horizontal_url_imagen_destacada").val();
    console.log("CATEGORIA PARA THNK: " + jQuery("#formApp").data("categoria"));

    console.log("EL VALOR DEL EVENTO");

    if(changeGetImg === false){
        getImgCampus = false;
    }
    //console.log(evento);

    // var ev = null;

    //Evento
    // if (evento != null) {
    //     ev = evento.srcElement || evento.target;
    // }

    //Validacion para saber si el evento viene de la caja de ciclo
    // if (ev == null || typeof ev == "undefined" || ev == "") {
    //     jQuery("#formApp").data("ciclo", "");
    // } else {
    //     jQuery("#formApp").data("ciclo", ev.attributes.value.nodeValue);
    //     jQuery("#formApp").data("cicloTexto", ev.attributes['data-text'].nodeValue);
    // }


    var cookie_form_values = decodeURIComponent(getCookie('c_form_data'));
    cookie_form_values = JSON.parse(cookie_form_values);

    console.log("Carrera Elegida: " + cookie_form_values.carrera);


    //Preparamos los datos para la thank you page
    // jQuery(".modal-header-formulario").hide("slow");
    // jQuery(".modal-header-thankyou").hide("slow");
    // jQuery(".img-header-thankyou").hide();
    // jQuery("#enviarTrd").hide();
    // jQuery("#body-form-todo").hide("slow");
    // jQuery(".modal-body-thankyou").show("slow");
    // jQuery(".tercer-texto-thankyou").html(cookie_form_values["carrera"]);
    // jQuery("#nombre-thankyou").html(cookie_form_values["nombre"]);
    // jQuery("#head_close_modal").hide("slow");
    // jQuery(".titulo-logo").show("slow");

    /*Implementacion enviar datos BY SRP 28-01-2018*/
    //jQuery("#txt-thanks").html("Gracias por proporcionarnos tus datos. En breve te enviaremos la información que nos solicitas de:");
    /*End Implementacion enviar datos BY SRP 28-01-2018*/

    //Si no viene con evento
    //console.log("EL EVENTO DESPUES DE CLICLEAR CICLO: ");
    //console.log(ev);

    // if (ev.id != "null" && ev.id != null && typeof ev.id != "undefined") {
    //     console.log("CICLO: " + ev.attributes.value.nodeValue);
    //     //Setear en la cookie el ciclo y en formApp

    //     this.formCookieService.appendCookieValue(
    //         "c_form_data",
    //         "ciclo",
    //         ev.attributes.value.nodeValue
    //     );

    //     this.formCookieService.appendCookieValue(
    //         "c_form_data",
    //         "cicloTexto",
    //         ev.attributes['data-text'].nodeValue
    //     );
    // } else {
    //             console.log("El evento no esta definido para el ciclo");
    //         }

    // if(paso6){
    //     console.log('sendFormularioTradicional');
    //     var objCookie = JSON.parse(decodeURIComponent(getCookie('c_form_data')));
    //     dataLayer.push({
    //         'event' : 'InformationForm',   //Static data
    //         'formStep' : 'Paso 6',   //Dynamic data
    //         'Estado' : objCookie.estado.toLowerCase(),   //CD4
    //         'InterLvl' : objCookie.linea.toLowerCase(),  //CD 5
    //         'Period' : 'enero',  //CD 6
    //         'carrera' : getUsuario().carrera.Link.toLowerCase(),  //CD 7
    //         'campus' : campusNombreLargo(getUsuario().campus).toLowerCase(),  //CD 8
    //         'Formulario' : 'Registro Formulario',  //CD 16
    //         'RMode': modalidadtxt(getUsuario().modalidad.modalidad).toLowerCase(),   //CD25
    //         'prom': '',   //CD25
    //         'formDev' : dispositivo,   // Dynamic data
    //         'tutor': objCookie.aPaternoTutor
    //     });
    // }

    // paso6 = false; //Desactivación para otras consultas posteriores
    // 
    if(!IsMobile()){
        var inter = getUsuario().linea.name.toLowerCase();
    } else{
        var inter = getUsuario().linea.toLowerCase();
    }

    if(paso4){
        console.log('sendFormularioTradicional');
        var objCookie = JSON.parse(decodeURIComponent(getCookie('c_form_data')));
        dataLayer.push({
            'event' : 'InformationForm',   //Static data
            'formStep' : 'Paso 4',   //Dynamic data
            'Estado' : objCookie.estado.toLowerCase(),   //CD4
            'InterLvl' : inter,  //CD 5
            'Period' : 'enero',  //CD 6
            'carrera' : getUsuario().modalidad.Link.toLowerCase(),  //CD 7
            'campus' : campusNombreLargo(getUsuario().campus).toLowerCase(),  //CD 8
            'Formulario' : 'Formulario Test Vocacional',  //CD 16
            'RMode': modalidadtxt(getUsuario().modalidad.modalidad).toLowerCase(),   //CD25
            //'RMode': '',   //CD26
            'formDev' : dispositivo,   // Dynamic data
            'tutor': objCookie.aPaternoTutor
        });
    }

    paso4 = false; //Desactivación para otras consultas posteriores

    var nombreCampus;
    var nombreLicenciatura;
    var nombreModalidad;
    var nombreCiclo;
    var anclaPerfil = "#perfil";
    var anclaGaleria = "#galeria-campus";

    if (jQuery("#formApp").data("campusLargo") !== "") {
        // nombreCampus = this.formCookieService.getCookieByKey("c_form_data", "campusLargo");
        nombreCampus = jQuery("#formApp").data("campusLargo");
    }

    if (jQuery("#formApp").data("linea") !== "") {
        nombreLicenciatura = jQuery("#formApp").data("linea");
    }

    //nombreLicenciatura = nombreLicenciatura.toLowerCase();
    //nombreLicenciatura = this.capitalizePipe.transform(nombreLicenciatura);

    if (jQuery("#formApp").data("carreraCompleta") != "") {
        nombreLicenciatura += " en " + jQuery("#formApp").data("carreraCompleta");
    }

    if (cookie_form_values.modalidad != "" && !IsMobile()) {
        switch (cookie_form_values.modalidad) {
            case "1":
                nombreModalidad = "Modalidad Presencial";
                /*Validación para campus en línea BY SRP 13-09-2018*/
                $('#title-virtualtour').contents()[1].data = "Conoce las instalaciones";
                anclaPerfil = "#perfil";
                anclaGaleria = "#galeria-campus";
                break;
            case "2":
                nombreModalidad = "Modalidad Ejecutiva";
                $('#title-virtualtour').contents()[1].data = "Conoce las instalaciones";
                anclaPerfil = "#perfil";
                anclaGaleria = "#galeria-campus";
                break;

            case "3":
                nombreModalidad = "Modalidad En Línea";
                $('#title-virtualtour').contents()[1].data = "Conoce más";
                anclaPerfil = "#perfil";
                anclaGaleria = "";
                /*End Validación para campus en línea BY SRP 13-09-2018*/
                break;
        }
    }

    // if(jQuery("#formApp").data("cicloTexto") != "") {
    //     nombreCiclo = "Iniciar " + jQuery("#formApp").data("cicloTexto") + " 2018";
    // }

    /*Se obtiene la URL de la Página BY SRP 09-10-2018*/
    // var arrayCategoriasPorID = this.readJsonService.buscar(
    //     "IdCategoria",
    //     this.formCookieService.getCookieByKey("c_form_data", "categoria"),
    //     JSON.parse(localStorage.getItem("jsonLinksCategorias"))
    // );
    /*End Se obtiene la URL de la PáginaBY SRP 09-10-2018*/

    /*Contenido Thank you Page*/
    // jQuery('.second-sub-desk').text("UNITEC Campus " + nombreCampus);
    // jQuery('#thank-linea').text("");
    // jQuery('#thank-linea').text(nombreLicenciatura);
    // jQuery('#thank-modalidad').text("");
    // jQuery('#thank-modalidad').text(nombreModalidad);
    // jQuery('#thank-campus').text("");
    // jQuery('#thank-campus').text("Campus " + nombreCampus);
    // jQuery('#thank-ciclo').text("");
    // jQuery('#thank-ciclo').text(nombreCiclo);

    /*Implementacion Texto thank you page Formulario Tradicional BY SRP 12-09-2018*/
    // jQuery("#descripcionThanks").html("Impulsamos tu esfuerzo recompensándote con una Beca Académica de Primer Ingreso. Conoce el porcentaje que te corresponde según tu promedio.");

    // var link_carrera;

    // if (arrayCategoriasPorID[0] !== undefined) {
    //     var linkSeleccionado = arrayCategoriasPorID[0].Link;
    //     link_carrera = linkSeleccionado.toLowerCase() + anclaPerfil;
    //     link_carrera = this.userValidations.removeAcentos( link_carrera );
    //     // $('#link-to-campus-study').attr('href', path + link_carrera.replace(/ /g, "-") );
    // }


    /*Implementacion enviar datos BY SRP 17-01-2018*/
    //Convertir a JSON la Cookie
    // var dataForm = JSON.parse( JSON.stringify( this.formCookieService.getCookieValues("c_form_data") ) );
    var dataForm = JSON.parse(decodeURIComponent(getCookie("c_form_data")));

    if(dataForm.banner == null || dataForm.banner == "" ){
        var bannerSecure;
        bannerSecure = getCookie("banner_activo");

        if( bannerSecure == null || bannerSecure == "" ) {
            if (location.href.indexOf("testvocacional") !== -1 || location.href.indexOf("orientacion-profesional") !== -1) {
                bannerSecure = "TESTVOCACIONAL";
            } else if (location.href.indexOf("impulsa") !== -1) {
                bannerSecure = "IMPULSA";
            } else {
                bannerSecure = "ASPIRANTES LIC";
            }
        }

        //Se registra el valor de la cookie utm_campaing
        // this.formCookieService.appendCookieValue(
        //     "c_form_data",
        //     "banner",
        //     bannerSecure
        // );
        
        appendCookie('c_form_data', 'banner', bannerSecure, 1);
    }

    if( checkCookie('banner_activo') === true ){
        //lo asigo a la cookie form_data
        appendCookie('c_form_data', 'banner', getCookie('banner_activo'), 1);
    }


    if( checkCookie('cid_unitec') === true ) {
        appendCookie('c_form_data', 'CID', getCookie("cid_unitec"), 1);
    }

    appendCookie('c_form_data', 'CID', jQuery("#CID").val(), 1);
    appendCookie('c_form_data', 'cid', jQuery("#CID").val(), 1);
    

    var dataForm = JSON.parse(decodeURIComponent(getCookie("c_form_data")));
    //console.log(this.formCookieService.getCookieValues("c_form_data"));

    // console.log("Campus " + jQuery("#formApp").data("campusLargo"));
    if(getImgCampus){
        jQuery.ajax({
            type: "POST",
            url: "/wp-admin/admin-ajax.php",
            data: {
                action: "getCampusBySlug",
                campus_name: "Campus " + campusNombreLargo(jQuery('#render_campus').val())
            },
            success: function (res) {
                console.log("Datos Enviados");
                console.log(res);
                //End Proceso tag manager
                res = JSON.parse(res);
                jQuery("#image-virtualtour").attr("src", res['imagen-grande']);
                jQuery("#image-virtualtour").attr("alt", res['descripcion-grande']);
                jQuery("#link-to-campus").attr("href", res['url-campus'] + anclaGaleria);
                
                /*Implementacion Para los Tour Virtuales*/
                // if(jQuery("#formApp").data("campusLargo") == "Atizapán") {
                //   jQuery("#link-to-campus").attr("href", "https://www.google.com.mx/maps/@19.5467281,-99.2396415,3a,75y,258.7h,82.77t/data=!3m7!1e1!3m5!1s9CEBnYu4l0AQlF7iBnMAxQ!2e0!3e5!7i13312!8i6656");
                // } else {
                //   jQuery("#link-to-campus").attr("href", res['url-campus'] + "#galeria-campus");
                // }
    
            },
            error: function (xhr, textStatus, error) {
                console.log(xhr);
                console.log(xhr.statusText);
                console.log(textStatus);
                console.log(error);
            }
        });
    }
    

    console.log("Formulario Tradicional");
    /*Implementacion enviar datos BY SRP 17-09-2018*/
    //Convertir a JSON la Cookie
    /*Implementacion enviar datos BY SRP 17-01-2018*/
    //Convertir a JSON la Cookie

    //var dataForm = JSON.parse( JSON.stringify( getCookie("c_form_data") ) );
    // var dataForm = JSON.parse(decodeURIComponent(getCookie("c_form_data")));
    console.log("Preparandonos para el envío");

    jQuery.ajax({
        type: "POST",
        url: url_ajax_php,
        data: { theData: dataForm },
        dataType: "JSON",
        success: function (res) {
            //console.log("Datos Enviados");          
            console.log(res);
            console.log("RES: " + res.tipo);

            //Proceso tag manager
            if (typeof dataLayer != 'undefined') { dataLayer.push({ 'event': 'PASO2_COMPLETADO' }); }  
            var pTutor
            if (typeof dataForm.aPaternoTutor == 'undefined') {
                pTutor = ""
            } else{
                pTutor = dataForm.aPaternoTutor
            }           
            if (res.tipo == 'dupli') {                
                if (typeof dataLayer != 'undefined') { 
                    dataLayer.push({ 'event': 'FORMULARIO_DUPLICADO' });                     
                    dataLayer.push({
                        'url_value': 'formularioInformacion/frmDuplicado', //dato dinámico
                        'event': 'StepForm'//dato estático
                    });
                    dataLayer.push({
                      'event' : 'leadGeneration', //Static data
                      //'ClientId' : '12afgh34', //sacar de analytics
                      'leadId' : res.leadId, //CD 15
                      'Formulario' : 'Formulario Tradicional', //CD 16
                      'campus' : dataForm.campusLargo, //CD 8
                      'carrera' : dataForm.carrera, //CD 7
                      'InterLvl' : inter, //CD 5
                      'Period' : dataForm.cicloTexto.replace(/^(En\s)/,""), //CD 6
                      'RMode' :  modalidadtxt(getUsuario().modalidad.modalidad),
                      //'prom':,
                      'Estado' : dataForm.estado,
                      'NumbrReg' : '2', //CD 24
                      'tutor': pTutor
                    });
                }                
            }

            if (res.tipo == 'nuevo') {

                var clientId = "";

                if (typeof dataLayer != 'undefined') {
                    //dataLayer.push({ 'event': dataForm.banner });
                    ga(function (tracker) { clientId = tracker.get('clientId'); });
                    /****** LEADGENERATION ANTERIOR**********/
                    /*
                    dataLayer.push({
                        'leadId': clientId, //dato dinámico
                        'CDClientID': clientId,
                        'origenLead': 'frmTradicional', //dato dinámico
                        'isAlumn': dataForm.esAlumno, //dato dinámico
                        'ingress': dataForm.tipoRegistro, //dato dinámico
                        'state': dataForm.estado, //dato dinámico
                        'levelStudies': dataForm.nivelInteres, //dato dinámico
                        'Period': dataForm.ciclo, //dato dinámico
                        'carrera': dataForm.carreraInteres, //dato dinámico
                        'campus': dataForm.campus, //dato dinámico
                        'date': new Date(), //dato dinámico
                        'event': 'leadGeneration'//dato estático
                    });    */                        
                    dataLayer.push({
                          'event' : 'leadGeneration', //Static data
                          //'ClientId' : '12afgh34', //sacar de analytics
                          'leadId' : res.leadId, //CD 15
                          'Formulario' : 'Formulario Tradicional', //CD 16
                          'campus' : dataForm.campusLargo, //CD 8
                          'carrera' : dataForm.carrera, //CD 7
                          'InterLvl' : inter, //CD 5
                          'Period' : dataForm.cicloTexto.replace(/^(En\s)/,""), //CD 6
                          'RMode' :  modalidadtxt(getUsuario().modalidad.modalidad),
                          //'prom':,
                          'Estado' : dataForm.estado,
                          'NumbrReg' : '1', //CD 24,
                          //CD DE LEADGENERATION ANTERIOR
                          'CDClientID': clientId,
                          'origenLead': 'frmTradicional', //dato dinámico
                          'isAlumn': dataForm.esAlumno, //dato dinámico
                          'ingress': dataForm.tipoRegistro, //dato dinámico
                          'state': dataForm.estado, //dato dinámico
                          'levelStudies': dataForm.nivelInteres, //dato dinámico
                          'date': new Date(), //dato dinámico
                          'tutor': pTutor
                    });                 
                }

                if (typeof dataLayer != 'undefined') {
                    dataLayer.push({ 'event': 'FORMULARIO_REGISTRO' });
                }
            }
            //End Proceso tag manager
            if (location.href.indexOf("testvocacional") !== -1 || location.href.indexOf("orientacion-profesional") !== -1) {
                
                //setCookieForm('accesoCC', true, 1);
                //localStorage.setItem("accesoCC", true);
                appendCookie('c_form_data', 'accesoCC', true, 1);
                //localStorage.setItem("urlcc", urlwp + "/testVocacional/test_vocacional.php");
                jQuery("#btn-test").attr({"href": "/apptestvocacional/","target":"_blank"}).removeClass('d-none');


                //localStorage.setItem("urlcc", res.urlcc);
                //jQuery("#btn-test").attr({"href":res.urlcc,"target":"_blank"});
                
                // alert('href'+res.urlcc);
                // jQuery("#div-btn-continuar").show();
            } else if (location.href.indexOf("impulsa") !== -1) {
                localStorage.setItem("vacantes", "https://www.unitec.mx/folleto/impulsa.pdf");
                jQuery("#div-btn-continuar a").attr({"href":"https://www.unitec.mx/folleto/impulsa.pdf","target":"_blank"});
                jQuery("#div-btn-continuar").show();
                setCookieForm('reg_impulsa', 'cookie', true, 1);
            } else {
                localStorage.removeItem("urlcc");
                localStorage.removeItem("vacantes");
            }



        },
        error: function (xhr, textStatus, error) {
            console.log(xhr.statusText);
            console.log(textStatus);
            console.log(error);
        }
    });
    /*End Implementacion enviar datos BY SRP 17-01-2018*/

    function getMethod(param) {
        console.log("param passed" + param);
        if (param != "") {
            console.log("SI trae parametro " + param);
            return vars[param.toLowerCase()] ? vars[param.toLowerCase()] : null;
        }
        var vars = {};
        window.location.href.replace(location.hash, '').replace(/[?&]+([^=&]+)=?([^&]*)?/gi, function (m, key, value) {
            vars[key.toLowerCase()] = value !== undefined ? value : '';
        });
        return vars;
    }
}

function guardaDatosCookie() {
    /*Guardar en la cookie la modalidad By SRP 08-10-2018*/
    if(!IsMobile()){
        appendCookie('c_form_data', 'linea', guardaUsuario().linea.id_name, 1);
    } else {
        appendCookie('c_form_data', 'linea', guardaUsuario().linea, 1);
    }

    appendCookie('c_form_data', 'categoria', guardaUsuario().modalidad.grupo_carreras, 1);
    appendCookie('c_form_data', 'carrera', guardaUsuario().modalidad.nombre, 1);
    appendCookie('c_form_data', 'carreraInteres', guardaUsuario().modalidad.iddynamics, 1);
    appendCookie('c_form_data', 'subNivelInteres', guardaUsuario().modalidad.interes, 1);
    appendCookie('c_form_data', 'nivelInteres', getNivelInteres(guardaUsuario().modalidad.lineaweb), 1);
    appendCookie('c_form_data', 'modalidad', guardaUsuario().modalidad.modalidad, 1);
    appendCookie('c_form_data', 'hubspotutk', getCookie('hubspotutk'), 1);
    appendCookie('c_form_data', 'campus', jQuery('#render_campus').val(), 1);
    appendCookie('c_form_data', 'campusLargo', campusDynamics(jQuery('#render_campus').val()), 1);
    appendCookie('c_form_data', 'urlreferrer', window.location, 1);
    appendCookie('c_form_data', 'ciclo', "19-2", 1);
    appendCookie('c_form_data', 'cicloTexto', "En Enero", 1);
}

function setDataCookie(campus){
    if(!IsMobile()){
        appendCookie('c_form_data', 'linea', getUsuario().linea.id_name, 1);
        var getLinea = getUsuario().linea.id_name;
    } else {
        appendCookie('c_form_data', 'linea', getUsuario().linea, 1);
        var getLinea = getCampo('linea');
    }
    appendCookie('c_form_data', 'campus', campus, 1);
    //appendCookie('c_form_data', 'categoria', getUsuario().carrera.IdCategoria, 1);
    appendCookie('c_form_data', 'carrera', getUsuario().modalidad.nombre, 1);
    appendCookie('c_form_data', 'carreraInteres', getUsuario().modalidad.IdDynamics, 1);
    appendCookie('c_form_data', 'subNivelInteres', getUsuario().modalidad.interes, 1);
    appendCookie('c_form_data', 'nivelInteres', getNivelInteres(getLinea), 1);
    appendCookie('c_form_data', 'modalidad', getUsuario().modalidad.modalidad, 1);
    appendCookie('c_form_data', 'hubspotutk', getCookie('hubspotutk'), 1);
    appendCookie('c_form_data', 'campusLargo', campusDynamics(campus), 1);
    appendCookie('c_form_data', 'urlreferrer', window.location, 1);
    appendCookie('c_form_data', 'ciclo', "20-1", 1);
    appendCookie('c_form_data', 'cicloTexto', "En Septiembre", 1);
}
/* SEGUNDA PARTE JS */

/* Adiciones */

/* CODIGOS CARRERAS */
/*
Todo lo que es linea debe de ser vigente
1  HSc (Lic de Salud)
2 UG  (Licenciaturas)
3 Lx    (Licenciaturas Ejecutivas)
4 UG OL ( Licenciaturas en línea)
5 PG  (Posgrados)
6 PG  (Posgrados en línea)
7 HS (Prepa) 
*/
/* CODIGOS CARRERAS */
var config_ciclos_arr = [
    {
        ciclo: { valor: "19-2", texto: "Enero" },
        subNivelInteres: ["1","2","3","4","5","6","7"],
        idCarreras: [""],
        excluirCarreras: [""],
        excluirsubNivel: [""],
        incluirCampus: [""]
    },
    {
        ciclo: { valor: "19-2", texto: "Febrero" },
        subNivelInteres: [""],
        idCarreras: [""],
        excluirCarreras: [""],
        excluirsubNivel: [""],
        incluirCampus: [""]
    },
    {
        ciclo: { valor: "19-3", texto: "Junio" },
        subNivelInteres: ["3","4","5","6"],
        idCarreras: [""],
        excluirCarreras: ["35","195","193","197","192","196","1326","1324","1186","1587","1584"],
        excluirsubNivel: [""],
        incluirCampus: ["ATZ","CUI","ECA","GDL","LEO","MAR","ONL","QRO","REY","SUR","TOL"]
    },
    {
        ciclo: { valor: "20-1", texto: "Agosto" },
        subNivelInteres: ["1","5","6"],
        idCarreras: ["35","195","193","197","192","196"],
        excluirCarreras: [""],
        excluirsubNivel: [""],
        incluirCampus: ["ATZ","CUI","ECA","GDL","LEO","MAR","ONL","QRO","REY","SUR","TOL"]
    },
    {
        ciclo: { valor: "20-1", texto: "Septiembre" },
        subNivelInteres: ["1","2","7"],
        idCarreras: [""],
        excluirCarreras: ["35","195","193","197","192","196"],
        excluirsubNivel: [""],
        incluirCampus: ["ATZ","CUI","ECA","GDL","LEO","MAR","ONL","QRO","REY","SUR","TOL"]
    }
];
function guardaCiclo(ciclo,texto) {
    // console.log("Guardando el ciclo");
    // console.log(ciclo);
    // guardaUsuario('ciclo',ciclo);
    // guardaUsuario('cicloTexto',texto);
    guardaDatosCookie();
    appendCookie('c_form_data', 'ciclo', ciclo, 1);
    appendCookie('c_form_data', 'cicloTexto', "En " + texto, 1);

    jQuery(".modal-header-formulario").hide();
    jQuery(".modal-header-thankyou").hide();
    jQuery(".img-header-thankyou").hide();
    jQuery("#enviarTrd").hide();
    jQuery("#body-form-todo").hide();
    jQuery(".modal-body-thankyou").show();
    // jQuery(".tercer-texto-thankyou").html(cookie_form_values["carrera"]);

    // jQuery("#nombre-thankyou").html(cookie_form_values["nombre"]);
    /*VALORES DE PRUEBA */
    jQuery(".tercer-texto-thankyou").html(getUsuario().carrera.Link);
    jQuery('.second-sub-desk').text("UNITEC Campus " + campustxt(jQuery('#render_campus').val()));
    jQuery('#thank-linea div.col-10').text("");
    jQuery('#thank-linea div.col-10').text(getUsuario().carrera.Link);
    jQuery('#thank-modalidad div.col-10').text("");
    jQuery('#thank-modalidad div.col-10').text("Modalidad " + modalidadtxt(getUsuario().modalidad.modalidad));
    jQuery('#thank-campus div.col-10').text("");
    jQuery('#thank-campus div.col-10').text("Campus " + campustxt(jQuery('#render_campus').val()));
    // jQuery('#thank-ciclo').text("");
    // jQuery('#thank-ciclo').text("19-2");

    /*VALORES DE PRUEBA */

    jQuery("#head_close_modal").hide();
    jQuery(".titulo-logo").show();

    jQuery("#modal_frm_app").modal();
    $('#calculadora-css').attr("disabled", "disabled");
    $('#calculadora-css').prop("disabled", true);
    $('#aplicativo-formulario').hide();
    $(".todoElBody").show();
    // $('.main-calculadora-todo').show();

    // if(paso6){
    //     console.warn('guardaCiclo 6');
    //     var objCookie = JSON.parse(decodeURIComponent(getCookie('c_form_data')));
    //     var pTutor
    //     if (typeof objCookie.aPaternoTutor == 'undefined') {
    //         pTutor = ""
    //     } else{
    //         pTutor = objCookie.aPaternoTutor
    //     }
    //     dataLayer.push({
    //         'event' : 'InformationForm',   //Static data
    //         'formStep' : 'Paso 6',   //Dynamic data
    //         'Estado' : objCookie.estado.toLowerCase(),   //CD4
    //         'InterLvl' : getUsuario().linea.name.toLowerCase(),  //CD 5
    //         'Period' : texto.toLowerCase(),  //CD 6
    //         'carrera' : getUsuario().carrera.Link.toLowerCase(),  //CD 7
    //         'campus' : campusNombreLargo(getUsuario().campus).toLowerCase(),  //CD 8
    //         'Formulario' : 'Registro Formulario',  //CD 16
    //         'RMode': modalidadtxt(getUsuario().modalidad.modalidad).toLowerCase(),   //CD25
    //         'prom': '',   //CD26
    //         'formDev' : 'Desktop',  // Dynamic data
    //         'tutor': pTutor
    //     });
    // }

    // paso6 = false; //Desactivación para otras consultas posteriores

    // if(paso7){
    //     console.warn('guardaCiclo 7');
    //     var objCookie = JSON.parse(decodeURIComponent(getCookie('c_form_data')));
    //     var pTutor
    //     if (typeof objCookie.aPaternoTutor == 'undefined') {
    //         pTutor = ""
    //     } else{
    //         pTutor = objCookie.aPaternoTutor
    //     }
    //     dataLayer.push({
    //         'event' : 'InformationForm',   //Static data
    //         'formStep' : 'Paso 7',   //Dynamic data
    //         'Estado' : objCookie.estado.toLowerCase(),   //CD4
    //         'InterLvl' : getUsuario().linea.name.toLowerCase(),  //CD 5
    //         'Period' : texto.toLowerCase(),  //CD 6
    //         'carrera' : getUsuario().carrera.Link.toLowerCase(),  //CD 7
    //         'campus' : campusNombreLargo(getUsuario().campus).toLowerCase(),  //CD 8
    //         'Formulario' : 'Registro Formulario',  //CD 16
    //         'RMode': modalidadtxt(getUsuario().modalidad.modalidad).toLowerCase(),   //CD25
    //         //'RMode': '',   //CD26
    //         'formDev' : 'Desktop',   // Dynamic data
    //         'tutor': pTutor
    //     });
    // }

    // paso7 = false; //Desactivación para otras consultas posteriores

    /*Envio de Datos a Ciclo Activo*/
    //sendFormularioTradicional();
    /*End Envio de Datos a Ciclo Activo*/
}
function guardaCicloMobile(ciclo,texto) {
    var tutorGTM;
    var form_cookie = JSON.parse(decodeURIComponent(getCookie('c_form_data')));
    guardaDatosCookie();
    appendCookie('c_form_data', 'ciclo', ciclo, 1);
    appendCookie('c_form_data', 'cicloTexto', "En " + texto, 1);

    $('#form-css').prop("disabled", true);
    $(".todoElBody").show();
    $('#bootstrapcss-css').prop("disabled", false);
    $("#formulario-main-mobile").hide();
    $(".nvct").first().remove();
    $("#UnitecSideNav").first().remove();
    $("#nocritical-css").remove();

    jQuery(".modal-header-app").hide();
    jQuery(".modal-header-formulario").hide();
    jQuery(".modal-body-form").hide();
    //Llenar los campos de la thankyou page con los valores de la cookie
    jQuery(".tercer-texto-thankyou").html(
        // formCookiesService.getCookieByKey("c_form_data", "carrera")
        getUsuario().modalidad.nombre
    );
    jQuery("#nombre-thankyou").html(
        // formCookiesService.getCookieByKey("c_form_data", "nombre")
        form_cookie.nombre
    );
    //Mostrar la thankyou page
    jQuery(".modal-header-thankyou").show();
    jQuery(".modal-body-thankyou").show();
    jQuery(".img-header-thankyou").css({
        "background-image": "url(" + jQuery("#h_horizontal_url_imagen_destacada").val() + ")"
    });

    jQuery("#modal_frm_app").modal();    
    // if(paso6){
    //     console.warn('guardaCicloMobile 6');
    //     var objCookie = JSON.parse(decodeURIComponent(getCookie('c_form_data')));
    //     if (objCookie.aPaternoTutor == null || typeof objCookie.aPaternoTutor == 'undefined') {
    //          tutorGTM = "";    
    //     }else{
    //          tutorGTM = objCookie.aPaternoTutor;
    //     }
    //     dataLayer.push({
    //         'event' : 'InformationForm',   //Static data
    //         'formStep' : 'Paso 6',   //Dynamic data
    //         'Estado' : objCookie.estado.toLowerCase(),   //CD4
    //         'InterLvl' : getUsuario().linea.toLowerCase(),  //CD 5
    //         'Period' : texto.toLowerCase(),  //CD 6
    //         'carrera' : getUsuario().carrera.Link.toLowerCase(),  //CD 7
    //         'campus' : campusNombreLargo(getUsuario().campus).toLowerCase(),  //CD 8
    //         'Formulario' : 'Registro Formulario',  //CD 16
    //         'RMode': modalidadtxt(getUsuario().modalidad.modalidad).toLowerCase(),   //CD25
    //         'prom': '',   //CD26
    //         'formDev' : 'Mobile',   // Dynamic data
    //         'tutor': tutorGTM
    //     });
    // }

    // paso6 = false; //Desactivación para otras consultas posteriores
    
    // if(paso7){
    //     console.warn('guardaCicloMobile 7');
    //     var objCookie = JSON.parse(decodeURIComponent(getCookie('c_form_data')));
    //     if (objCookie.aPaternoTutor == null || typeof objCookie.aPaternoTutor == 'undefined') {
    //          tutorGTM = "";    
    //     }else{
    //          tutorGTM = objCookie.aPaternoTutor;
    //     }
    //     dataLayer.push({
    //         'event' : 'InformationForm',   //Static data
    //         'formStep' : 'Paso 7',   //Dynamic data
    //         'Estado' : objCookie.estado.toLowerCase(),   //CD4
    //         'InterLvl' : getUsuario().linea.toLowerCase(),  //CD 5
    //         'Period' : texto.toLowerCase(),  //CD 6
    //         'carrera' : getUsuario().carrera.Link.toLowerCase(),  //CD 7
    //         'campus' : campusNombreLargo(getUsuario().campus).toLowerCase(),  //CD 8
    //         'Formulario' : 'Registro Formulario',  //CD 16
    //         'RMode': modalidadtxt(getUsuario().modalidad.modalidad).toLowerCase(),   //CD25
    //         'prom': '',   //CD26
    //         'formDev' : 'Mobile',   // Dynamic data
    //         'tutor': tutorGTM
    //     });
    // }

    // paso7 = false; //Desactivación para otras consultas posteriores

    /*Envio de Datos a Ciclo Activo*/
    //sendFormularioTradicional();
    /*End Envio de Datos a Ciclo Activo*/
}
function construyeInputciclos() {
    var config_ciclos = config_ciclos_arr;
    var ciclos_input = [];
    $.each(config_ciclos, function(key, value) {
        /*Verificacion si el Campus tiene el ciclo */
        if(jQuery.inArray(getUsuario().campus, value.incluirCampus) !== -1){
            /*Verificacion si el subnivel de interes tiene el ciclo */
            if(jQuery.inArray(getUsuario().modalidad.interes.toString(), value.subNivelInteres) !== -1){
                /*Verificacion si la carrera no está excluida del ciclo */
                if(jQuery.inArray(getUsuario().modalidad.iddynamics.toString(), value.excluirCarreras) === -1){
                    /*Verificacion el ciclo aplica solo para carreras específicas */
                    if(value.idCarreras[0] !== ""){
                        /*Verificacion si la carrera aplica para el ciclo */
                        if(jQuery.inArray(getUsuario().modalidad.iddynamics.toString(), value.idCarreras) !== -1){
                            ciclos_input.push({
                                id: value.ciclo["valor"],
                                texto: value.ciclo["texto"]
                            });
                        }
                    } else {
                        ciclos_input.push({
                            id: value.ciclo["valor"],
                            texto: value.ciclo["texto"]
                        });
                    }
                }
            }
            
        }
        //console.log(objCiclo.ciclo["valor"]);
        //Si la carrera seleccionada esta en el array de ciclos entonces es valido el ciclo
        //console.log(objCiclo.idCarreras.indexOf(jQuery("#formApp").data("idCarrera")));
    }); //Fin del for
    console.log("Los ciclos para los que aplica");
    console.log(ciclos_input);
    //jQuery('#frm_ciclo').html("<option value='' selected>" + this.preguntaCiclo + "</option>")
    //Armar el listado y colocarlo en la seccion secProductos
    jQuery("#secCiclos .row").empty();
    if(!IsMobile()){
        for (var i = 0; i < ciclos_input.length; i++) {
            var ciclos_class = "cuadro-periodo";

            var iddiv = "div_ciclos" + i;
            jQuery("#secCiclos .row").append(
                '<div class="col-3 col-cuadros-contenedor">' +
                '<div class="col-12 col-cuadros-ciclos d-flex flex-column align-items-center justify-content-center" onclick="guardaCiclo(\'' + ciclos_input[i].id + '\',\'' + ciclos_input[i].texto +'\')">' +
                '<div class="titulo-lineas-cuadros text-center">'+ciclos_input[i].texto+'</div>' +
                '<div class="imagen-lineas-cuadros '+ciclos_class+'"></div>' +
                '<button id="'+iddiv+'" #div_ciclos value="'+ciclos_input[i].id+'" data-text="'+ciclos_input[i].texto+'" class="btn btn-dtc waves-effect waves-light btn-ciclos" style="display: block;">Selecciona</button>' +
                '</div>' +
                '</div>'
            );
        }
    } else {
        for (var i = 0; i < ciclos_input.length; i++) {
            var ciclos_class = "cuadro-periodo";

            var iddiv = "div_ciclos" + i;
            jQuery("#secCiclos .row").append(
                '<div class="col-6 col-cuadros-contenedor">' +
                '<div class="col-11 col-cuadros-ciclos" onclick="guardaCicloMobile(\'' + ciclos_input[i].id + '\',\'' + ciclos_input[i].texto +'\')">' +
                '<div class="titulo-lineas-cuadros text-center">'+ciclos_input[i].texto+'</div>' +
                '<div class="imagen-lineas-cuadros '+ciclos_class+'"></div>' +
                '<button id="'+iddiv+'" #div_ciclos value="'+ciclos_input[i].id+'" data-text="'+ciclos_input[i].texto+'" class="waves-effect waves-light btn-ciclos">Selecciona</button>' +
                '</div>' +
                '</div>'
            );
        }
    }

}

/*Funcion que personaliza los copies de cada BL By SRP 03-06-2019*/
function copyPersonalizable(linea){
    var text;
    //var style = 'style="color:#0275d8;"';
    var style = 'style="color:#f68b1f;"';

    switch( linea ){
        case 'PREPARATORIA':
            text = 'Escribe ó selecciona el programa académico de tu interés';
        break;

        case 'LICENCIATURA':
            text = 'Escribe ó selecciona la <b ' + style+ '>licenciatura</b> de tu interés';
        break;

        case 'INGENIERIA':
            text = 'Escribe ó selecciona la <b ' + style+ '>ingeniería</b> de tu interés';
        break;

        case 'SALUD':
            text = 'Escribe ó selecciona la <b ' + style+ '>Lic. en salud</b> de tu interés';
        break;

        case 'DIPLOMADO':
            text = 'Escribe ó selecciona el <b ' + style+ '>diplomado</b> de tu interés';
        break;

        case 'POSGRADO':
            text = 'Escribe ó selecciona el <b ' + style+ '>posgrado</b> de tu interés';
        break;

        default:
            text = 'Escribe ó selecciona la <b ' + style+ '>licenciatura</b> de tu interés';
        break;
    }

    return text;
}

function copyPersonalizableMobile(linea){
    var text;
    //var style = 'style="color:#0275d8;"';
    var style = 'style="color:#f68b1f;"';

    switch( linea ){
        case 'PREPARATORIA':
            text = '¿Qué quieres estudiar?';
        break;

        case 'LICENCIATURA':
            text = '¿Qué <b ' + style+ '>licenciatura</b> quieres estudiar?';
        break;

        case 'INGENIERIA':
            text = '¿Qué <b ' + style+ '>ingeniería</b> quieres estudiar?';
        break;

        case 'SALUD':
            text = '¿Qué <b ' + style+ '>Lic. en salud</b> quieres estudiar?';
        break;

        case 'DIPLOMADO':
            text = '¿Qué <b ' + style+ '>diplomado</b> quieres estudiar?';
        break;

        case 'POSGRADO':
            text = '¿Qué <b ' + style+ '>posgrado</b> quieres estudiar?';
        break;

        default:
            text = '¿Qué quieres estudiar?';
        break;
    }

    return text;
}
/*End Funcion que personaliza los copies de cada BL By SRP 03-06-2019*/

/*Funcion para Asignar Campus dependiendo del estado Seleccionado By SRP 02-08-2019*/
function asignaCampusCorrecto( estado ) {
    var verifica;
    var campusCorrecto;
    var campusObtenido;

    //switch (linea) {
        // case '4':
        //     campusCorrecto = "ONL";
        // break;
        
        // default:

    // var campusVig = ['ATZ','CUI','ECA','ONL','LEO','MAR','SUR','TOL','GDL','QRO','REY'];
    // verifica = campusVig.indexOf(estado);

    // if( verifica > 0 ) {
    //     campusCorrecto = estado;

    // } else {

        switch ( estado ) {
            case 'ciudad de méxico':
            case 'ciudad de mexico':
                var campusZM = "MAR,CUI,SUR";
                var campusMetro = campusZM.split(',');
                var min = 0;
                var max = campusMetro.length - parseInt(1) ;
                campusObtenido = Math.floor(Math.random() * (max - min)) + min;
                campusCorrecto = campusMetro[campusObtenido];

                //alert("Random Estado: " + campusAsignado);
            break;

            case 'estado de méxico':
            case 'estado de mexico':
                var campusFR = "ATZ,ECA,REY,TOL";
                var campusForaneo = campusFR.split(',');
                var min = 0;
                var max = campusForaneo.length - parseInt(1) ;
                campusObtenido = Math.floor(Math.random() * (max - min)) + min;
                campusCorrecto = campusForaneo[campusObtenido];

                //alert("Random Estado: " + campusAsignado);
            break;

            case 'guanajuato':
                campusCorrecto = "LEO";
            break;

            case 'querétaro':
            case 'queretaro':
                campusCorrecto = "QRO";
            break;

            case 'jalisco':
                campusCorrecto = "GDL";
            break;
            
            default:
                campusCorrecto = "ONL";
            break;
        }

    // }
        
        // break;
    //}

    return campusCorrecto;
}
/*End Funcion para Asignar Campus dependiendo del estado Seleccionado By SRP 02-08-2019*/

/*Funcion para buscar Carrera de acuerdo al campus obtenido By SRP 02-08-2019*/
function buscarCampus(campus, jsonCarreras) {
    var resultadoBusqueda = [];

    for (var i in jsonCarreras) {
        //alert( JSON.stringify( jsonCarreras[i]['nombre'] ) + JSON.stringify( jsonCarreras[i]['campus'] ) );
        var findCampus = jsonCarreras[i]['campus'][0].split(",");
        if ( findCampus.indexOf(campus) >= 0) {
            resultadoBusqueda.push(jsonCarreras[i]);
        }
    }

    return resultadoBusqueda;
}
/*End Funcion para buscar Carrera de acuerdo al campus obtenido By SRP 05-08-2019*/

function getParametros(linea, campus){

    var lineaParametros = {};
    //alert( "Llega: " + campus );

    switch( linea ){

            case "SALUD":

                var validaCampus = campus;

                if( validaCampus === "ONL"){
                    lineaParametros['IdDynamics'] = 3;
                    lineaParametros['nombre'] = "OTRA (EN LINEA)";
                    lineaParametros['Link'] = "Otra (En Linea)";
                    lineaParametros['campus'] = campus;
                    lineaParametros['interes'] = 4;
                    lineaParametros['modalidad'] = 3;
                    //lineaParametros['Grupo_carreras'] = "2";
                    lineaParametros['catCosto'] = "LICON";
                    lineaParametros['lineaweb'] = "SALUD";

                } else{
                    lineaParametros['IdDynamics'] = 1;
                    lineaParametros['nombre'] = "OTRA (SALUD)";
                    lineaParametros['Link'] = "Otra (Salud)";
                    lineaParametros['campus'] = campus;
                    lineaParametros['interes'] = 1;
                    lineaParametros['modalidad'] = 1;
                    //lineaParametros['Grupo_carreras'] = "2";
                    lineaParametros['catCosto'] = "LIC";
                    lineaParametros['lineaweb'] = "SALUD";
                }
    
            break;

            case "LICENCIATURA":
            
                    lineaParametros['IdDynamics'] = 2;
                    lineaParametros['nombre'] = "OTRA (TRADICIONAL)";
                    lineaParametros['Link'] = "Otra (Tradicional)";
                    lineaParametros['campus'] = campus;
                    lineaParametros['interes'] = 2;
                    lineaParametros['modalidad'] = 1;
                    //lineaParametros['Grupo_carreras'] = "2";
                    lineaParametros['catCosto'] = "LIC";
                    lineaParametros['lineaweb'] = "LICENCIATURA";
            
            break;

            case "INGENIERIA":
                
                    lineaParametros['IdDynamics'] = 9;
                    lineaParametros['nombre'] = "OTRA (INGENIERÍA)";
                    lineaParametros['Link'] = "Otra (Ingenieria)";
                    lineaParametros['campus'] = campus;
                    lineaParametros['interes'] = 2;
                    lineaParametros['modalidad'] = 1;
                    //lineaParametros['Grupo_carreras'] = "2";
                    lineaParametros['catCosto'] = "ING";
                    lineaParametros['lineaweb'] = "INGENIERIA";
                
            break;

            case "TURISMO":
                var validaCampus = campus;

                if( validaCampus === "ONL"){
                    lineaParametros['IdDynamics'] = 3;
                    lineaParametros['nombre'] = "OTRA (EN LINEA)";
                    lineaParametros['Link'] = "Otra (En Linea)";
                    lineaParametros['campus'] = campus;
                    lineaParametros['interes'] = 4;
                    lineaParametros['modalidad'] = 3;
                    //lineaParametros['Grupo_carreras'] = "2";
                    lineaParametros['catCosto'] = "LICON";
                    lineaParametros['lineaweb'] = "LICENCIATURA";

                } else{
                    lineaParametros['IdDynamics'] = 11;
                    lineaParametros['nombre'] = "OTRA (TURISMO)";
                    lineaParametros['Link'] = "Otra (Turismo)";
                    lineaParametros['campus'] = campus;
                    lineaParametros['interes'] = 2;
                    lineaParametros['modalidad'] = 1;
                    //lineaParametros['Grupo_carreras'] = "2";
                    lineaParametros['catCosto'] = "BLUEM";
                    lineaParametros['lineaweb'] = "LICENCIATURA";
                }
            break;

            case "DISENO":
               
                    lineaParametros['IdDynamics'] = 13;
                    lineaParametros['nombre'] = "OTRA (DISENO)";
                    lineaParametros['Link'] = "Otra (Diseno)";
                    lineaParametros['campus'] = campus;
                    lineaParametros['interes'] = 2;
                    lineaParametros['modalidad'] = 1;
                    //lineaParametros['Grupo_carreras'] = "2";
                    lineaParametros['catCosto'] = "ING";
                    lineaParametros['lineaweb'] = "LICENCIATURA";
                
            break;

            case "ECOADMIN":
                
                    lineaParametros['IdDynamics'] = 15;
                    lineaParametros['nombre'] = "OTRA (ECONOMICO-ADMINISTRATIVO)";
                    lineaParametros['Link'] = "Otra (Economico-Administrativo)";
                    lineaParametros['campus'] = campus;
                    lineaParametros['interes'] = 2;
                    lineaParametros['modalidad'] = 1;
                    //lineaParametros['Grupo_carreras'] = "2";
                    lineaParametros['catCosto'] = "LIC";
                    lineaParametros['lineaweb'] = "LICENCIATURA";
                
            break;

            case "MERCADOTECNIA":

                    lineaParametros['IdDynamics'] = 17;
                    lineaParametros['nombre'] = "OTRA (COMUNICACION Y MERCADOTECNIA)";
                    lineaParametros['Link'] = "Otra (Comunicacion y Mercadotecnia)";
                    lineaParametros['campus'] = campus;
                    lineaParametros['interes'] = 2;
                    lineaParametros['modalidad'] = 1;
                    //lineaParametros['Grupo_carreras'] = "2";
                    lineaParametros['catCosto'] = "LIC";
                    lineaParametros['lineaweb'] = "LICENCIATURA";
                
            break;

            case "PREPARATORIA":
                
                var validaCampus = campus;

                if( validaCampus === "ONL"){
                    var campusFR = "GDL,LEO,QRO";
                    var campusForaneo = campusFR.split(',');
                    var min = 0;
                    var max = campusForaneo.length - parseInt(1) ;
                    campusObtenido = Math.floor(Math.random() * (max - min)) + min;
                    campus = campusForaneo[campusObtenido];
                } else{
                    campus = campus
                }

                lineaParametros['IdDynamics'] = 666;
                lineaParametros['nombre'] = "PREPARATORIA";
                lineaParametros['Link'] = "Preparatoria";
                lineaParametros['campus'] = campus;
                lineaParametros['interes'] = 7;
                lineaParametros['modalidad'] = 6;
                lineaParametros['Grupo_carreras'] = 56;
                lineaParametros['catCosto'] = "PREPA";
                lineaParametros['lineaweb'] = "PREPARATORIA";
            break;

            default:
                var validaCampus = campus;

                if( validaCampus === "ONL"){
                    lineaParametros['IdDynamics'] = 3;
                    lineaParametros['nombre'] = "OTRA (EN LINEA)";
                    lineaParametros['Link'] = "Otra (En Linea)";
                    lineaParametros['campus'] = campus;
                    lineaParametros['interes'] = 4;
                    lineaParametros['modalidad'] = 3;
                    //lineaParametros['Grupo_carreras'] = "2";
                    lineaParametros['catCosto'] = "LICON";
                    lineaParametros['lineaweb'] = "LICENCIATURA";

                } else{
                    lineaParametros['IdDynamics'] = 2;
                    lineaParametros['nombre'] = "OTRA (TRADICIONAL)";
                    lineaParametros['Link'] = "Otra (Tradicional)";
                    lineaParametros['campus'] = campus;
                    lineaParametros['interes'] = 2;
                    lineaParametros['modalidad'] = 1;
                    //lineaParametros['Grupo_carreras'] = "2";
                    lineaParametros['catCosto'] = "LIC";
                    lineaParametros['lineaweb'] = "LICENCIATURA";
                }
            break;
        }

        return lineaParametros;
}
/*End Obtener parametros de búsqueda de acuerdo a la Linea de Negocio By SRP 05-08-2019*/

/*Envio de datos y actualización de cookie c_form_data By SRP 05-08-2019*/
function envioData(campus, dispositivo){
    setDataCookie(campus);
    sendFormularioTradicional(false, dispositivo);
}
/*End Envio de datos y actualización de cookie c_form_data By SRP 05-08-2019*/

/* Adiciones */