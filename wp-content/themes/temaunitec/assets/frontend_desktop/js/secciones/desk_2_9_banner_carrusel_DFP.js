$(document).ready(function () {
    if( isMobile() === true ) {
    var isFirefox = typeof InstallTrigger !== 'undefined';
    
    $('.carousel-dfp').addClass('hm-black-light');
    window.onorientationchange = orientacionDispositivo2; 
    orientacionDispositivo2();

        if (isFirefox == true) {
            $('#bannerDinamico').addClass('dfp-firefox');
        }
    
    } else{
        /*Inicializamos el Autoplay*/
        $('#carousel-banner').carousel({
            interval: 8000,
            cycle: true,
            ride: true,
            pause: false
        });
    /*End Inicializamos el Autoplay*/
    }
});

function isMobile() {
    try{ 
        document.createEvent("TouchEvent"); 
        return true; 
    }
    catch(e){ 
        return false;
    }
}

var arrnids = [];
var aleatorio = "";


/*Extraer Todos los Slide Disponibles*/
function imprimeBanner(linea) {
    var b = Handlebars.compile($("#hr-template").html());
    var a = "SELECT * WHERE B = '" + "activado" + "' ";

    // if (linea !== "") {
    //     a += "AND C LIKE '%" + linea + "%' ";
    // }

    //console.log(a);

    $("#bannerDinamico").sheetrock({
        url: mySpreadsheet,
        query: a,
        rowHandler: b,
        //labels: ['id'],
        callback: function(error, options, response) {

            if (!error) {
                if( isMobile() === true ) {
                    orientacionDispositivo2();
                }
                var totalSlides = JSON.stringify(response.rows.length);
                //var carouselDFP = "[";

                // response.rows.forEach(function(imagen, index) {
                //     carouselDFP += "'" + index + "'" + ":" + "'" + imagen.cellsArray + "'" + ",";
                // });

                for (var i = 0; i < totalSlides; i++) {

                    $("#ol-dfp").append('<li data-target="#carousel-banner" data-slide-to="'+ i +'" class="carousel-item-dfp"></li>');

                    // carouselDFP += JSON.stringify( response.rows[i] ) + ",";
                    //carouselDFP += response.rows[i].cellsArray + ",";
                    //console.log( response.rows[i].cellsArray );
                }
                //carouselDFP += "]";
                
                console.log( JSON.stringify(response.rows) );

                //console.log( JSON.stringify(response.rows.length) );

                //console.log( carouselDFP );

                $("div.carousel-item-dfp:first").addClass("show active");
                $("li.carousel-item-dfp:first").addClass("show active");
            }//error
             
        }//Callback

    })
}
/*Extraer Todos los Slide Disponibles*/

function orientacionDispositivo2() {

    if (Math.abs(window.orientation) === 90) {
        // Landscape
        //var tipo = "horizontal";

        $( ".bann_wind" ).each(function() {
            $( this ).css('background-image','url(' + $( this ).attr( "link-mobile-horizontal" ) + ')').promise().done(function() {
                console.log( $( this ).attr( "link-mobile-horizontal" ) );
                $(this).animate({
                    opacity: 1
                }, 600)
            });

        });

    } else {
        // Portrait
        //var tipo = "vertical";
        $( ".bann_wind" ).each(function() {
            $( this ).css('background-image','url(' + $( this ).attr( "link-mobile-vertical" ) + ')').promise().done(function() {
                console.log( $( this ).attr( "link-mobile-vertical" ) );
                $(this).animate({
                    opacity: 1
                }, 600)
            });
        });

    }
}

function ajusteBannerCarrousel(orient, jsonBanner, noSlider){
    if(orient=="vertical") {

        for (var i = 0; i < noSlider; i++) {
            jQuery('#banner-item-slider-'+ i).attr('src',jsonBanner.vertical[i]).promise().done(function(){
                console.log(jsonBanner.vertical[i]);
                // Callback of the callback :)
                jQuery(this).animate({
                    opacity: 1
                }, 600)
            });
        }      

    } else if(orient=="horizontal"){

        for (var i = 0; i < noSlider; i++) {
            jQuery('#banner-item-slider-'+ i).attr('src',jsonBanner.horizontal[i]).promise().done(function(){
                console.log(jsonBanner.horizontal[i]);
                // Callback of the callback :)
                jQuery(this).animate({
                    opacity: 1
                }, 600)
            });
        }

    } else{

        for (var i = 0; i < noSlider; i++) {
            jQuery('#banner-item-slider-'+ i).attr('src',jsonBanner.vertical[i]).promise().done(function(){
                console.log(jsonBanner.vertical[i]);
                // Callback of the callback :)
                jQuery(this).animate({
                    opacity: 1
                }, 600)
            });
        }

    }

    bannerHeight();
}

function bannerHeight(){
    setTimeout(function(){
        var heightNavbar = jQuery(".nvct").height();
        var height = jQuery(window).height() - parseInt(heightNavbar);  //getting windows height
        jQuery('.carousel-banner').css('height', height+'px');
        jQuery(".carousel-banner").find(".carousel-item").css('height', height+'px');   //and setting height of carousel
    }, 500);
}
