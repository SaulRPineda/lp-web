jQuery(document).ready( function(){
	var desbloquea = checkCookie( 'c_matricula' );

	if ( desbloquea == true ) {
		jQuery("#seccion-biblioteca-recursos").removeAttr('style');
		jQuery("#seccion-biblioteca-apoyo").removeAttr('style');

        jQuery("#login-biblioteca").css("style","none");
        jQuery("#logout-biblioteca").removeAttr('style');
	} else{
		jQuery("#seccion-biblioteca-recursos").css("style","none");
		jQuery("#seccion-biblioteca-apoyo").css("style","none");

        jQuery("#login-biblioteca").removeAttr('style');
        jQuery("#logout-biblioteca").css("style","none");
	}

});

function getCookie(cname) {
    var name = cname + "=";
    var ca = document.cookie.split(';');
    for (var i = 0; i < ca.length; i++) {
        var c = ca[i];
        while (c.charAt(0) == ' ') c = c.substring(1);
        if (c.indexOf(name) == 0) return c.substring(name.length, c.length);
    }
    return "";
}

function checkCookie(valor) {
    var user = getCookie(valor);
    if (user != "") {
        return true;
    }
    else {
        return false;
    }
}


$.extend($.expr[":"], {
    "containsIN": function (elem, i, match, array) {
        return (elem.textContent || elem.innerText || "").toLowerCase().indexOf((match[3] || "").toLowerCase()) >= 0;
    }
});
