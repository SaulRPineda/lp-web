var elementos_tabs_dinamicos = 1;

$(window).resize(function () {
    div_ancho_contacto = $(".carrousel_inner_tabs_dinamicos > ul > li").width();
    // var posTestimoniales = positionTestimoniales * (div_ancho + 30);
    $('.carrousel_nuevas_carreras').attr('data-pos', 0);
    $('.carrousel_inner_tabs_dinamicos').animate({ 'scrollLeft': 0 });
    $('.carrousel_right_contacto_carrusel').removeClass('right_inactive left_inactive d-none');
    $('.carrousel_left_contacto_carrusel').addClass('d-none');
    // if ($(window).width() <= 991) {
    //     elementos_tabs_dinamicos = 1;
    // } else {
    //     elementos_tabs_dinamicos = 2;
    // }
});

$('.carrousel_right_contacto_carrusel').click(function(){
    var div_ancho_contacto = $(".carrousel_inner_tabs_dinamicos ul").width();
    // var elementsCountTabsDinamicos = $('carrousel_tabs_dinamicos.active .carrousel_inner_tabs_dinamicos ul').length;
    var elementsCountTabsDinamicos = $('#totalTabsDinamicosContacto').val();
    var positionTabsDinamicos = $('.carrousel_tabs_dinamicos.active').attr('data-pos');
    positionTabsDinamicos = parseInt(positionTabsDinamicos, 10);

    if (positionTabsDinamicos < elementsCountTabsDinamicos)
    {
        // elementos_tabs_dinamicos = elementos_tabs_dinamicos + 1;
        positionTabsDinamicos = positionTabsDinamicos + 1;
        $('.carrousel_right_contacto_carrusel').removeClass('right_inactive');
        $('.carrousel_left_contacto_carrusel').removeClass('d-none');         

        if ( positionTabsDinamicos == elementsCountTabsDinamicos || elementos_tabs_dinamicos == elementsCountTabsDinamicos ){
            $(this).addClass('left_inactive');
            $(this).addClass('d-none');
        }
        var posTabsDinamicos = positionTabsDinamicos*(div_ancho_contacto);
        $('.carrousel_tabs_dinamicos.active').attr('data-pos',positionTabsDinamicos);
        $('.carrousel_inner_tabs_dinamicos').animate({'scrollLeft' : posTabsDinamicos });
    }
    // alert("Elemento Actual: " + elementos_tabs_dinamicos + " Total: " + elementsCountTabsDinamicos + " Position: " + positionTabsDinamicos);
});

$('.carrousel_left_contacto_carrusel').click(function(){
    var div_ancho_contacto = $(".carrousel_inner_tabs_dinamicos ul").width();
    // var elementsCountTabsDinamicos = $('carrousel_tabs_dinamicos.active .carrousel_inner_tabs_dinamicos ul').length;
    var elementsCountTabsDinamicos = $('#totalTabsDinamicosContacto').val();
    var positionTabsDinamicos = $('.carrousel_tabs_dinamicos.active').attr('data-pos');
    positionTabsDinamicos=parseInt(positionTabsDinamicos, 10);

    if (positionTabsDinamicos > 0)
    {
        $('.carrousel_left_contacto_carrusel').removeClass('left_inactive');
        $('.carrousel_right_contacto_carrusel').removeClass('d-none');
        elementos_tabs_dinamicos = elementos_tabs_dinamicos - 1;
        positionTabsDinamicos = positionTabsDinamicos - 1;            

        if(positionTabsDinamicos == 0)
            {
                $(this).addClass('right_inactive');
                $(this).addClass('d-none');
            }
        var posTabsDinamicos = positionTabsDinamicos*(div_ancho_contacto);
        $('.carrousel_tabs_dinamicos.active').attr('data-pos',positionTabsDinamicos);
        $('.carrousel_inner_tabs_dinamicos').animate({'scrollLeft' : posTabsDinamicos });
    }
});

$('a.tab-dinamico-cardbullets').click(function(){
    var id_carrusel = $(this).attr('href');
    var conteo_li = $(id_carrusel + " ul li.cardsbullets").length;
    $('#totalTabsDinamicosContacto').val(conteo_li);
    $('.carrousel_tabs_dinamicos.active').attr('data-pos',0);
    $('.carrousel_inner_tabs_dinamicos').scrollLeft(0);
    if(conteo_li == 1){
        $('.carrousel_left_contacto_carrusel').addClass('right_inactive');
        $('.carrousel_left_contacto_carrusel').addClass('d-none');
        $('.carrousel_right_contacto_carrusel').addClass('left_inactive');
        $('.carrousel_right_contacto_carrusel').addClass('d-none');
    } else {
        $('.carrousel_left_contacto_carrusel').addClass('right_inactive');
        $('.carrousel_left_contacto_carrusel').addClass('d-none');
        $('.carrousel_right_contacto_carrusel').removeClass('left_inactive');
        $('.carrousel_right_contacto_carrusel').removeClass('d-none');
    }
});