jQuery(document).ready(function () {
    'use strict';
    window.addEventListener('load', function () {
        // Fetch all the forms we want to apply custom Bootstrap validation styles to
        var forms = document.getElementsByClassName('needs-validation');
        // Loop over them and prevent submission
        var validation = Array.prototype.filter.call(forms, function (form) {
            form.addEventListener('submit', function (event) {
                console.log(form.checkValidity())
                if (form.checkValidity() === false) {
                    event.preventDefault();
                    event.stopPropagation();
                }
                form.classList.add('was-validated');
            }, false);
        });
    }, false);

    var heightNavbar = jQuery(".nvct").height();
    var height = jQuery(window).height() - parseInt(heightNavbar);  //getting windows height
    jQuery('#altura-section').css('height', height + 'px');
    jQuery('#altura-section').css('margin-top', parseInt(heightNavbar) + 'px');

    jQuery('#proceso-absorcion').hide();
    jQuery('#msj-guardado').hide();

    // var availableTags = [
    //     "hotmail.com",
    //     "yahoo.com.mx",
    //     "mail.unitec.mx",
    //     "my.unitec.edu.mx"
    // ];
    // $("#validationCustomUsername2").autocomplete({
    //     source: availableTags
    // });

    jQuery("#ingresar-cuenta").click(function() {

        var cuenta = jQuery("#form-user").val()
        if (cuenta==''|| $.isNumeric(cuenta)==false) {          
          jQuery("#form-user").next(".invalid-feedback").text("Número de cuenta no válido").addClass('d-block');
        }else{
          $.ajax({
            type: "POST",
            url: "/wp-admin/admin-ajax.php",
            data: {
              action: "abs_CuentaValid",
              'nocuenta': cuenta
            },
            success: function (res) {            
              console.log(res);            
              res = JSON.parse(res);
              if (res.success) {
                jQuery("#guardar").data('cuenta',res.data.cuenta)
                var nombre = res.data.nombre;   
                var correo = res.data.correo;
                jQuery("#paso2-frm-absorcion").removeClass("d-none");
                jQuery("#msj-guardado").removeClass("d-none");
                jQuery("#paso1-frm-absorcion").hide(500);
                jQuery("#img-frm-absorcion").hide(500);
                jQuery("#paso2-frm-absorcion").show(1000);
                jQuery("#validationCustom012").val(nombre).trigger("change");
                jQuery("#validationCustomUsername2").val(correo).trigger("change");
                jQuery('#proceso-absorcion').hide();     
              }else{

                jQuery("#form-user").next(".invalid-feedback").text(res.data.msg).addClass('d-block') 
              }                      
            },
            error: function (xhr, textStatus, error) {
              console.log(xhr);
              console.log(xhr.statusText);
              console.log(textStatus);
              console.log(error);
            },
            beforeSend: function () {
                var html = "";
                html += "<div class='p-3 pt-2 d-flex flex-column align-items-center justify-content-center overlay-absorcion'>";
                html += "<h3 class='m-0 titleSection titleSectionAbsorcion text-tnks'>Tu solicitud está en proceso.</h3>";
                html += "<h5 class='m-0 descriptionSection descriptionSectionAbsorcion text-tnks mt-2 mb-2'><img src='../assets/cargando.gif' class='img-fluid m-auto' style='width:100%; max-width:60px;'></h5>";
                html += "</div>";
                jQuery('#paso1-frm-absorcion').addClass('blur-absorcion');
                jQuery('#proceso-absorcion').empty();
                jQuery('#proceso-absorcion').append(html);
                jQuery('#proceso-absorcion').show();
            },
            complete: function(){
                jQuery('#paso1-frm-absorcion').removeClass('blur-absorcion');
                jQuery('#proceso-absorcion').hide();
            }
          });
        }                
    });

    $("#guardar").click(function(e){
      var errors = 0;
      $(".invalid-feedback").removeClass('d-block')
      e.preventDefault(); 
       var vmail= validaEmail( $("#validationCustomUsername2").val() );       
      if (vmail!==null) {                      
        errors++
        $("#validationCustomUsername2").next(".invalid-feedback").text(vmail.customErrorMessage).addClass('d-block')      
      }
      var vcelular = validaCelular($("#validationCustom032").val())
      if (vcelular!==null) {              
        errors++
        $("#validationCustom032").next(".invalid-feedback").text(vcelular.customErrorMessage).addClass('d-block')
      }
      if (!$("#invalidCheck2").prop('checked')) {
        errors++
        $("#invalidCheck2").nextAll(".invalid-feedback").addClass('d-block')
      }
      if (errors==0) {
        var cuenta = $(this).data('cuenta')
        var email = $("#validationCustomUsername2").val() 
        var cel = $("#validationCustom032").val()
        $.ajax({
            type: "POST",
            url: "/wp-admin/admin-ajax.php",
            data: {
              action: "abs_updt",
              'nocuenta': cuenta ,'email':email,'cel':cel
            },
            success: function (res) {            
              console.log(res);            
              res = JSON.parse(res);
              if (res.success) {
                console.log("saved");
              }else{
                console.log("try again")            
              }                      
            },
            error: function (xhr, textStatus, error) {
              console.log(xhr);
              console.log(xhr.statusText);
              console.log(textStatus);
              console.log(error);
            },
            beforeSend: function () {
                var html = "";
                html += "<div class='p-3 pt-2 d-flex flex-column align-items-center justify-content-center overlay-absorcion'>";
                html += "<h3 class='m-0 titleSection titleSection titleSectionAbsorcion text-tnks'>Tu solicitud está en proceso.</h3>";
                html += "<h5 class='m-0 descriptionSection descriptionSectionAbsorcion text-tnks mt-2 mb-2'><img src='../assets/cargando.gif' class='img-fluid m-auto' style='width:100%; max-width:60px;'></h5>";
                html += "</div>";
                // $('#blur-ctc').addClass('blur-ctc');
                jQuery('#proceso-absorcion').empty();
                jQuery('#proceso-absorcion').append(html);
                jQuery('#proceso-absorcion').show();
            },
            complete: function () {
                jQuery('#proceso-absorcion').hide();
                var html = "";

                html += "<div class='col-lg-12 p-3 pt-2 d-flex flex-column justify-content-center align-items-center h-100'>";
                html += "<h3 class='m-0 titleSection titleSectionAbsorcion text-tnks text-center'>¡Gracias por actualizar tus datos!</h3>";
                html += "<h5 class='m-0 descriptionSection descriptionSectionAbsorcion text-tnks mt-2 mb-2'>Ahora te has sumado a la campaña verde y te podremos enviar información importante a través de correo electrónico.</h5>";
                html += "</div>";

                jQuery('#msj-guardado').empty();
                jQuery('#msj-guardado').append(html);
                jQuery('#msj-guardado').show(500);

                jQuery("#validationCustomUsername2").val('');
                jQuery("#validationCustom032").val('');
                jQuery('#guardar').data('cuenta', '');
                jQuery("#form-user").val('')
            }
          });
      }else{

      }
    })

})



// (function () {
//     'use strict';
//     window.addEventListener('load', function () {
//         // Fetch all the forms we want to apply custom Bootstrap validation styles to
//         var forms = document.getElementsByClassName('needs-validation');
//         // Loop over them and prevent submission
//         var validation = Array.prototype.filter.call(forms, function (form) {
//             form.addEventListener('submit', function (event) {
//               console.log(form.checkValidity())
//                 if (form.checkValidity() === false) {
//                     event.preventDefault();
//                     event.stopPropagation();
//                 }
//                 form.classList.add('was-validated');
//             }, false);
//         });
//     }, false);
// })();

function validaEmail(inp_value)
    {
        //Flag que cambiara cuando sea valido
        var emailValid = false;
        var customErrorMessage;
        var arrEmail = inp_value.split('@');
        var lenUser = arrEmail[0].length;
        /*var jsonReader = new readJson();
        var jsonBasura = jQuery("#data_json_basura_email").data();*/
        try {
            var arrDominio = arrEmail[1].split('.');
            var arrLargo = arrDominio.length;
        }catch(err) {}

        //Esta vacio el correo
        if(arrEmail[0] === ""){ 
            return { 
                fieldError : true,
                customErrorMessage: 'Ingrese su correo' 
            }
        //Validamos en el json basura la parte uno del correo
        }/*else if(jQuery("#data_json_basura_email").val() == 1 && jsonReader.getKeys(jsonBasura, arrEmail[0]).length > 0 ) {
            return { 
                fieldError : true,
                customErrorMessage: 'Correo inválido' 
            }
         //Validar arroba
        }*/else if(arrEmail.length <= 1){
            return { 
                fieldError : true,
                customErrorMessage: 'Ingrese el correo completo' 
            }
        //Validamos en el json basura la parte 2 del correo
        }/*else if(jQuery("#data_json_basura_email").val() == 1 && jsonReader.getKeys(jsonBasura, arrEmail[1]).length > 0) {
            
            return { 
                fieldError : true,
                customErrorMessage: 'Correo inválido' 
            }
        //Longitud del nombre de usuario es muy largo o muy corto el nombre
        }*/else if(lenUser <= 2 || lenUser > 100){ 
            return { 
                fieldError : true,
                customErrorMessage: 'Longitud de correo invalida' 
            }
        //Validamos que no existen caracteres especiales en el nombre de usuario
        }else if(arrEmail[0].search(/[^a-zA-Z0-9_.-]/)!=-1){ 
            return { 
                fieldError : true,
                customErrorMessage: 'Caracter inválido encontrado' 
            }
        //Validamos que no existen caracteres especiales en el dominio 
        }else if(arrEmail[1].search(/[^a-zA-Z0-9_.-]/)!=-1){ 
            return { 
                fieldError : true,
                customErrorMessage: 'Caracter inválido encontrado' 
            }
         //Validacion de dominios MS   
        }else if(arrEmail[1].search(/(hotmail|outlook|live)/i)!= -1 && (arrEmail[0][lenUser-1].search(/\./)!=-1 || arrEmail[0][0].search(/(-|_|\.|[0-9])/)!=-1 || arrEmail[0].search(/\.{2}/g) != -1) ){
            return { 
                fieldError : true,
                customErrorMessage: 'Correo inválido' 
            }
        //Validacion de dominios gmail    
        }else if(arrEmail[1].search(/gmail/i)!= -1 && (arrEmail[0][lenUser-1].search(/[^a-zA-Z0-9]/)!=-1 || arrEmail[0][0].search(/(-|_|\.|[0-9])/)!=-1 || arrEmail[0].search(/\.{2}/g) != -1 || arrEmail[0].search(/(-|_)/g) != -1)){
            return { 
                fieldError : true,
                customErrorMessage: 'Correo inválido' 
            } 
         //Validacion de dominios yahoo     
        }else if(arrEmail[1].search(/yahoo/i)!= -1 && (arrEmail[0][lenUser-1].search(/[^a-zA-Z0-9]/)!=-1 || arrEmail[0][0].search(/(_|\.|[0-9])/)!=-1 || arrEmail[0].search(/(\.|_){2}/g) != -1 || arrEmail[0].search(/-/g)!=-1 ) ){
            return { 
                fieldError : true,
                customErrorMessage: 'Correo inválido' 
            } 
        //Validacion de dominios restantes       
        }else if(arrEmail[1].search(/(^hotmail|outlook|gmail|live|yahoo)/i) == -1 && ( arrEmail[0][lenUser-1].search(/(-|_|\.)/)!=-1 || arrEmail[0][0].search(/(-|_|\.|[0-9])/)!=-1 || arrEmail[0].search(/(-|_|\.){2}/g) != -1)){
            return { 
                fieldError : true,
                customErrorMessage: 'Correo inválido' 
            }
         //Validacion de dominios por ejemplo debe tener @mail.com por ejemplo 
        }else if (arrLargo < 2){
            return { 
                fieldError : true,
                customErrorMessage: 'Ingrese el correo completo' 
            }
        } else {
            var arrDominio = arrEmail[1].split('.');
            var arrLargo = arrDominio.length;

            // Hay emails que tienen dominio  y subdominio, por ejemplo fcastill@mail.unitec.mx,
            // entonces verificamos desde el primer elemento al penultimo, ya que el ultimo el es
            // el nombre de dominio superior y requerimos otras validaciones
            for (var x = 0; x <= arrLargo-2; x++) {
                //esta vacio el nombre del dominio o subdominio?
                if (arrDominio[x]===""){
                    return { 
                        fieldError : true,
                        customErrorMessage: 'Correo inválido' 
                    }
                //es muy largo o muy corto el nombre del dominio?
                }else if(arrDominio[x].length > 36 || arrDominio[x].length < 2){
                    return { 
                        fieldError : true,
                        customErrorMessage: 'Correo inválido' 
                    }
                } 
                /*
                //Validación de correo basura en el dominio
                if(Object.keys(jsonBasura).length != 0) {
                    for(var i = 0; i < Object.keys(jsonBasura).length; i++){
                        if(arrDominio[x].indexOf(jsonBasura[i]) != -1){
                            
                            return { 
                                fieldError : true,
                                customErrorMessage: 'Correo inválido' 
                            }
                        }
                    } 
                }
                */
            }
            // Validamos el nombre del dominio superior
            // esta vacio
            if(arrDominio[arrLargo-1]===""){
                return { 
                    fieldError : true,
                    customErrorMessage: 'Correo inválido' 
                }
            }else if(arrDominio[arrLargo-1].length >= 4 || arrDominio[arrLargo-1].length <= 1){
                return { 
                    fieldError : true,
                    customErrorMessage: 'Correo inválido' 
                }
            }
        }//Else de validaciones principales

        return null;
    }

    function validaCelular(inp_value)
    {
        var arrTelefonosBasura = jQuery("#data_json_telefonos_basura").data();
        var arrBasuraCompleteTelefono = [];

        //Telefonos basura
        // var arrTelefonosBasura=[
        //     "1010101001",
        //     "0000000000",
        //     "1234567890",
        //     "5556543727",  
        //     "5556581111",  
        //     "5552074077",  
        //     "5552074083",  
        //     "5556842142",  
        //     "5556849112",  
        //     "5556258646",  
        //     "5553951111",  
        //     "5555575759",  
        //     "5552009000",  
        //     "5551308000",  
        //     "5551308646",  
        //     "5555540612",  
        //     "5556543210",  
        //     "5553532763",  
        //     "5553532823",  
        //     "5556832222",  
        //     "5552295600",  
        //     "5556842124",  
        //     "5552410245",  
        //     "5552305100",  
        //     "5557703548",  
        //     "5557871540",  
        //     "5555606988",  
        //     "5555650521",  
        //     "5555651039",  
        //     "5553731122",  
        //     "5555653638" 
        // ];
        //var telefono_basura = arrTelefonosBasura.indexOf(abstractControl.value);
        if(inp_value == ""){
            return { 
                fieldError : true,
                customErrorMessage: 'Ingrese un número de celular' 
            }
        
        }else if(inp_value.length != 10){
            return { 
                fieldError : true,
                customErrorMessage: 'Ingrese los 10 digitos de su celular' 
            } 
        }

        else if(Object.keys(arrTelefonosBasura).length != 0) {
            
            for(var i = 0; i < Object.keys(arrTelefonosBasura).length ;i++){
                var arrRes = arrTelefonosBasura[i];
                arrBasuraCompleteTelefono.push(arrRes);
                 
            }
            for(i = 0; i < Object.keys(arrTelefonosBasura).length ; i++){
                //console.log(arrBasuraComplete[i]);
                if(inp_value.search(arrBasuraCompleteTelefono[i]) !== -1){
                    return { 
                        fieldError : true,
                        customErrorMessage: 'Ingrese un número de celular valido' 
                    }
                }
            }

        }

        // else if(telefono_basura !== -1) {
        //     return { 
        //         fieldError : true,
        //         customErrorMessage: 'Ingrese un número de celular valido' 
        //     }
        // }
        
        else if(inp_value.search(/[^0-9]/g) != -1){
            return { 
                fieldError : true,
                customErrorMessage: 'Ingrese un número de celular valido' 
            }
        }else if( inp_value.search(/(\d)\1\1\1\1/g) != -1){
            return { 
                fieldError : true,
                customErrorMessage: 'Ingrese un número de celular valido' 
            }
        }else if(inp_value.charAt(0) == 0){

            return { 
                fieldError : true,
                customErrorMessage: 'Ingrese un número de celular valido' 
            }

        }
        return null;

    }//Termina valida celular