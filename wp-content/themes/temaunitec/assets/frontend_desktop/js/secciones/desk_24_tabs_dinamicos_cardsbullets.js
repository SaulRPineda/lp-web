var div_ancho = $(".carrousel_inner_tabs_dinamicos ul li").width();
var elementos_tabs_dinamicos = 1;

$('.carrousel_right_tabs_dinamicos').click(function(){
    // var elementsCountTabsDinamicos = $('.carrousel_inner_tabs_dinamicos ul li').length;
    var elementsCountTabsDinamicos = $('#totalTabsDinamicos').val();
    var positionTabsDinamicos =$('.carrousel_tabs_dinamicos.active').attr('data-pos');
    positionTabsDinamicos = parseInt(positionTabsDinamicos, 10);

    if (positionTabsDinamicos < elementsCountTabsDinamicos)
    {
        elementos_tabs_dinamicos = elementos_tabs_dinamicos + 1;
        positionTabsDinamicos = positionTabsDinamicos+1;
        $('.carrousel_right_tabs_dinamicos').removeClass('right_inactive');
        $('.carrousel_left_tabs_dinamicos').removeClass('d-none');         

        /*console.log("Elemento Actual: " + elementos_tabs_dinamicos + " Total: " +elementsCountTabsDinamicos);*/   

        if ( positionTabsDinamicos == elementsCountTabsDinamicos || elementos_tabs_dinamicos == elementsCountTabsDinamicos ){
            $(this).addClass('left_inactive');
            $(this).addClass('d-none');
        }
        var posTabsDinamicos = positionTabsDinamicos*(div_ancho+18);
        $('.carrousel_tabs_dinamicos.active').attr('data-pos',positionTabsDinamicos);
        $('.carrousel_inner_tabs_dinamicos').animate({'scrollLeft' : posTabsDinamicos });
    }
});

$('.carrousel_left_tabs_dinamicos').click(function(){
    // var elementsCountTabsDinamicos = $('.carrousel_inner_tabs_dinamicos ul li').length;
    var elementsCountTabsDinamicos = $('#totalTabsDinamicos').val();
    var positionTabsDinamicos = $('.carrousel_tabs_dinamicos.active').attr('data-pos');
    positionTabsDinamicos=parseInt(positionTabsDinamicos, 10);

    if (positionTabsDinamicos > 0)
    {
        $('.carrousel_left_tabs_dinamicos').removeClass('left_inactive');
        $('.carrousel_right_tabs_dinamicos').removeClass('d-none');
        elementos_tabs_dinamicos = elementos_tabs_dinamicos - 1;
        positionTabsDinamicos = positionTabsDinamicos - 1;            

        /*console.log("Elemento Actual: " + elementos_tabs_dinamicos + " Total: " +elementsCountTabsDinamicos);*/

        if(positionTabsDinamicos == 0)
            {
                $(this).addClass('right_inactive');
                $(this).addClass('d-none');
            }
        var posTabsDinamicos = positionTabsDinamicos*(div_ancho+18);
        $('.carrousel_tabs_dinamicos.active').attr('data-pos',positionTabsDinamicos);
        $('.carrousel_inner_tabs_dinamicos').animate({'scrollLeft' : posTabsDinamicos });
    }
});

$('a.tab-dinamico-cardbullets').click(function(){
    elementos_tabs_dinamicos = 1;
    var id_carrusel = $(this).attr('href');
    var conteo_li = $(id_carrusel + " ul li.cardsbullets").length;
    $('#totalTabsDinamicos').val(conteo_li);
    $('.carrousel_tabs_dinamicos.active').attr('data-pos',0);
    $('.carrousel_inner_tabs_dinamicos').scrollLeft(0);
    if(conteo_li == 1){
        $('.carrousel_left_tabs_dinamicos').addClass('right_inactive');
        $('.carrousel_left_tabs_dinamicos').addClass('d-none');
        $('.carrousel_right_tabs_dinamicos').addClass('left_inactive');
        $('.carrousel_right_tabs_dinamicos').addClass('d-none');
    } else {
        $('.carrousel_left_tabs_dinamicos').addClass('right_inactive');
        $('.carrousel_left_tabs_dinamicos').addClass('d-none');
        $('.carrousel_right_tabs_dinamicos').removeClass('left_inactive');
        $('.carrousel_right_tabs_dinamicos').removeClass('d-none');
    }
});