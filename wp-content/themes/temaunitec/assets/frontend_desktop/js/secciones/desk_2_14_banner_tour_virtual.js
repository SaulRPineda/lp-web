//Variables generales para Custom Tabs
var CustomTabs = $("section#tabNav, section#tabNav.free");
var tolerancia = 25;

var dias_semana = new Array("Domingo", "Lunes", "Martes", "Miércoles", "Jueves", "Viernes", "Sábado", "Domingo");
var mes = new Array("Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto","Septiembre","Octubre","Noviembre","Diciembre");

jQuery(document).ready(function () {
    
    var tt = getRangoFecha();
    /*Activar para pruebas By SRP 04-06-2019*/
    //var tt = '2019-08-01';

    //var date = new Date(tt);
    var newdate = new Date(tt);
    //var rangoFechas = new Array; 

    for (var i = 0; i < 7; i++) {
        newdate.setDate(newdate.getDate() + 1);

        var dd = newdate.getDate();
        var mm = newdate.getMonth() + 1;
        var y = newdate.getFullYear();
        var numDia = newdate.getDay();

        //rangoFechas[i] = Array(numDia, someFormattedDate);
        var formatoFecha = dias_semana[numDia] + " " + dd + " de " + mes[mm - 1];

        dd = (dd <= 9)? "0" + dd:dd;
        mm = (mm <= 9)? "0" + mm:mm;
        var someFormattedDate = dd + '/' + mm + '/' + y;

        if (numDia == 0) {
            $('#valorDomingo').val(y + '-' + mm + '-' + dd);
        }

        $('#co-fecha').append("<a data-d='" + numDia + "' id='" + numDia + "' data-fecha='" + someFormattedDate + "' class='dropdown-item align-items-center p-2'>" + formatoFecha + "</a>");

        //console.log("Dia: " + numDia + " Fecha: " + someFormattedDate );
    }

    

    $(".leftTabNavigation").on("click", function () {
        var elemento = $(this).siblings(".tabs-wrapper").children(".nav");
        elemento.animate({ scrollLeft: '-=150' });
        var maxScrollLeft = elemento[0]["scrollWidth"] - elemento[0]["clientWidth"];
        viewTabsAndControlsHorario(elemento, "left");
    });

    $(".rightTabNavigation").on("click", function () {
        var elemento = $(this).siblings(".tabs-wrapper").children(".nav");
        elemento.animate({ scrollLeft: '+=150' });
        var maxScrollLeft = elemento[0]["scrollWidth"] - elemento[0]["clientWidth"];
        viewTabsAndControlsHorario(elemento, "right");
    });

    $("#co-fecha .dropdown-item").click(function(){
        var StateName = $(this).text();
        $("#input-fecha").text(StateName);
        $("#co-fecha .dropdown-item").removeClass('sel');        
        $(this).text();
        $(this).addClass('sel');
        $("#validacion-fecha").removeClass('d-block');
        horarios()
        $("#label-hora").addClass("active");
        $(".nav-item").on("click", function () {
            var carousel = $('#tabNav');
            var viewport = carousel.find('.tabs-wrapper');
            var itemIndex = $(this).index();
            var itemWidth = $(this).outerWidth(true);
            var newLeft = (itemIndex * itemWidth * -1) + Math.floor(($(viewport).width() / itemWidth) / 2) * itemWidth;
            var left = $(this).offset().left;
            var width = $(".tabs-wrapper").width();
            var diff = left - width / 2;
            newLeft = Math.abs(newLeft);
            //   alert(carousel + " / " + newLeft + " / " + diff);
            $(this).parents(".nav").animate({ scrollLeft: newLeft + "px" }, 1000);
            if (($(".tabs-wrapper").scrollLeft() + diff + 20) > 20) {
                viewTabsAndControls($(this).parent(".nav"), "right", left);
            } else {
                viewTabsAndControls($(this).parent(".nav"), "left", left);
            }
            //   $("#input-hora").removeAttr("style");
            //   var StateName = $(this).text();
            //   $("#input-hora").text(StateName);
            //   $("#label-hora").addClass("active");
        });
  })

  $('a.campus').click(function () {
        var StateName = $(this).text();
        $("#input-campus").text(StateName);
        $("a.campus").removeClass('sel');        
        $(this).text();
        $(this).addClass('sel');

        var dateDefault = new Date();
        var numDiaDefault = dateDefault.getDay();
        var did = newdate.getDate();
        var mem = newdate.getMonth() + 1;
        var formatoFechaDefault = dias_semana[numDiaDefault] + " " + did + " de " + mes[mem - 1];

        /*Limpiamos input e Horarios By SRP 30-05-2019*/
        /*Habilitamos el input ya que el anterior ya cuenta con Información By SRP 29-05-2019*/
        document.getElementById('input-fecha').disabled = false;
        $('#input-fecha').removeClass('disabled');
        $('#iconCita').removeClass('icon-disabled');
        $("#validacion-campus").removeClass('d-block');
        $('#0').addClass('d-none');
        $("#co-fecha .dropdown-item.sel").data('d', numDiaDefault);
        $('#input-fecha').text(formatoFechaDefault);
        $('#' + numDiaDefault).click();
        /*End Habilitamos el input ya que el anterior ya cuenta con Información By SRP 29-05-2019*/

        horarios()
  });
  
$(document).on('click', "#co-hora li a", function () {
    $("#co-hora li a").removeClass('sel');
    $(this).addClass('sel');
})
//   alert(validHS);
if (validHS) {
    var camp = $("#agendar-cita").data('cmp')
    $('#co-campus a[data-c=' + camp + ']').click()
}
if (cookieValidation) {
    // if (campus == 'LOS REYES') {
    //     campus = 'LOSREYES';
    // }
    jQuery('#agendar-cita').attr({ 'data-fn': nombre, 'data-ln': lastname, 'data-cmp': campus });
    jQuery('#co-email').val(email);
    jQuery('#co-email').attr('disabled', true);
    var camp = $("#agendar-cita").data('cmp');
    jQuery('#co-campus a[data-c="' + camp + '"]').click();
}

/*******************l************************/


    'use strict';
    window.addEventListener('load', function () {
        // Fetch all the forms we want to apply custom Bootstrap validation styles to
        var forms = document.getElementsByClassName('needs-validation');
        // Loop over them and prevent submission
        var validation = Array.prototype.filter.call(forms, function (form) {
            form.addEventListener('submit', function (event) {
                console.log(form.checkValidity())
                if (form.checkValidity() === false) {
                    event.preventDefault();
                    event.stopPropagation();
                }
                form.classList.add('was-validated');
            }, false);
        });
    }, false);

    var heightNavbar = jQuery(".nvct").height();
    var height = jQuery(window).height() - parseInt(heightNavbar);  //getting windows height
    jQuery('#altura-section').css('height', height + 'px');
    jQuery('#altura-section').css('margin-top', parseInt(heightNavbar) + 'px');

    jQuery('#proceso-absorcion').hide();
    jQuery('#msj-guardado').hide();

    // var availableTags = [
    //     "hotmail.com",
    //     "yahoo.com.mx",
    //     "mail.unitec.mx",
    //     "my.unitec.edu.mx"
    // ];
    // $("#validationCustomUsername2").autocomplete({
    //     source: availableTags
    // });

  $("#agendar-cita").click(function (e) {
        var errors = 0;
        $(".invalid-feedback").removeClass('d-block')
        e.preventDefault();
        var vmail = validaEmail($("#co-email").val());
        if (vmail !== null) {
            errors++

            $("#co-email").next(".invalid-feedback").text(vmail.customErrorMessage).addClass('d-block')
        }
        var campus = $("#co-campus .dropdown-item.sel").data('c')
        // if (campus == 'LOSREYES') {
        //     campus = 'LOS REYES';
        // }      
        if (campus === "" || typeof campus === "undefined") {
            errors++
            console.warn("error 1")
            $(".inp-campus").next(".invalid-feedback").text("Selecciona un campus").addClass('d-block')
        }else{console.warn("valida 1")}
        var fecha = $("#co-fecha .dropdown-item.sel").data('d')        
        if (fecha === "" || typeof fecha === "undefined") {
            errors++
            console.warn("error 2")
            $(".inp-fecha").next(".invalid-feedback").text("Selecciona un día").addClass('d-block')
        }else{console.warn("valida 2")}
        var hora = $("#co-hora li a.sel").data('h')        
        if ((hora === "" || typeof hora === "undefined") && $("#co-hora li").length > 0) {
            errors++
            console.warn("error 3")
            $(".inp-hora").next(".invalid-feedback").text("Selecciona un horario").addClass('d-block')
        }else{console.warn("valida 2")}
        
        if (errors == 0) {  
            var valmail = $("#co-email").val();
            var valfecha = $("#co-fecha .dropdown-item.sel").data('fecha')
            var nombre = $("#agendar-cita").data('fn')
            var lastn = $("#agendar-cita").data('ln')        
            var profileurl = $("#agendar-cita").data('url')
            var fuente = $("#agendar-cita").data('fuente')
            var dataObj = {
                        action: "CitaSend",
                        'email': valmail, 'campus': campus,'fecha':valfecha,'hora':hora,'nombre':nombre,"lastname":lastn,"URL":profileurl
                    };
            if (typeof fuente !== "undefined") {
                dataObj.fuente = fuente
            }
            $.ajax({
                type: "POST",
                //url: "/wp-admin/admin-ajax.php",
                url: "/procWebLeads/citaenlinea/insertCita.php",
                data: dataObj,
                success: function (res) {
                    console.log(res);
                    res = JSON.parse(res);
                    if (res.success) {
                        console.log("saved");
                        dataLayer.push({
                          'event' : 'agendarCita', 
                          'campus' :campus, 
                          'Fecha' : valfecha,
                          'Hora' : hora+':00'                          
                        });
                    } else {
                        console.log("try again")
                    }
                },
                error: function (xhr, textStatus, error) {
                    console.log(xhr);
                    console.log(xhr.statusText);
                    console.log(textStatus);
                    console.log(error);
                },
                beforeSend: function () {
                    var html = "";
                    html += "<div class='p-3 pt-2 d-flex flex-column align-items-center justify-content-center overlay-absorcion'>";
                    html += "<h3 class='m-0 titleSection titleCitaLoad textInfoThankYou'>Tu solicitud está en proceso.</h3>";
                    html += "<h5 class='m-0 descriptionSection descriptionSectionAbsorcion textInfoThankYou mt-2 mb-2'><img src='../assets/cargando.gif' class='img-fluid m-auto' style='width:100%; max-width:60px;'></h5>";
                    html += "</div>";
                    // $('#blur-ctc').addClass('blur-ctc');
                    jQuery('#proceso-absorcion').empty();
                    jQuery('#proceso-absorcion').append(html);
                    jQuery('#proceso-absorcion').show();
                },
                complete: function () {
                    jQuery('#proceso-absorcion').hide();
                    var html = "";
                    var nameThanks = nombre;

                    if (nombre === undefined){
                        nameThanks = 'por agendar tu cita';
                    }else{
                        nameThanks = jsUcfirst( nombre.toLowerCase() );
                    }
                    if (campus === 'LOS REYES') {
                        campus = 'LOS-REYES';
                    }

                    html += "<div class='col-lg-12 p-3 pt-2 d-flex flex-column justify-content-center align-items-center h-100'>";
                    html += "<h3 class='titleCitaThanks text-center my-2'><b>¡Gracias " + nameThanks + "!</b></h3>";
                    html += "<hr class='lineThankyouCita w-100'>";
                    html += "<h5 class='m-0 textThankYou text-center my-4'>En breve recibirás tu confirmación a través de correo electrónico.</h5>";
                    html += "<div class='row w-75 my-1'><div class='col-3 d-flex justify-content-end align-items-center p-0 icon'><i class='icon-u icon-u-campus-" + campus.toLowerCase() + "'></i></div><div class='col-9 campusText'>Campus " +$("#co-campus .dropdown-item.sel").text()+ "</div></div>";
                    html += "<div class='row w-75 my-1'><div class='col-3 d-flex justify-content-end align-items-center p-0 icon'><i class='icon-u icon-u-calendario'></i></div><div class='col-9'>" + jsUcfirst( $("#co-fecha .dropdown-item.sel").text() ) + "</div></div>";
                    html += "<div class='row w-75 my-1'><div class='col-3 d-flex justify-content-end align-items-center p-0 icon'><i class='icon-u icon-u-horarios'></i></div><div class='col-9'>" + hora + ':00' +" horas</div></div>";
                    html += "<h6 class='textThankYou textInfoThankYou text-center my-3 px-4'>Recuerda llevar una identificación oficial para acceder al campus y aquellas preguntas que desees realizar a tu asesor educativo.</h6>";
                    html += "<h6 class='textThankYou textInfoThankYou'>¿Quieres saber cómo llegar al campus? <a href='/campus-" + campus.toLowerCase() + '/#mapa-campus' + "' class='m-0'data-gtm-tr'OurCampus' data-gtm-campus='campus-"+ campus.toLowerCase() +"' data-gtm-ubicacion='Tour Virtual'> VER AQUÍ<i class='icon-u icon-u-ver-derecha'></a></h6>";
                    html += "</div>";

                    jQuery('#urlCampus').attr('href', document.domain + '/campus-' + campus.toLowerCase() + '/#mapa-campus' );

                    jQuery('#msj-guardado').empty();
                    jQuery('#msj-guardado').append(html);
                    jQuery('#msj-guardado').show(500);
/*
                    jQuery("#validationCustomUsername2").val('');
                    jQuery("#validationCustom032").val('');
                    jQuery('#guardar').data('cuenta', '');
                    jQuery("#form-user").val('')                    
*/                    
                }
            });
        } else {

        }
    })
});
function horarios(){

    var d_sel = $("#co-fecha .dropdown-item.sel").data('d')
    var fecha_sel = $("#co-fecha .dropdown-item.sel").text().trim().split(" ")
    var c_sel = $("#co-campus .dropdown-item.sel").data('c')

    /*Mostrar Domingo Permitido By SRP 29-05-2019*/
    var verificaPermitidos = jsonpermitidos[c_sel];
    var domingo = $("#valorDomingo").val();
    
    if ( verificaPermitidos.indexOf(domingo) !== -1 ) {
        $('#0').removeClass('d-none');
        //$('#co-fecha').find('a').removeClass('d-none');
    } else{
        $('#0').addClass('d-none');
    }
    /*End Mostrar Domingo Permitido By SRP 29-05-2019*/

    console.warn(fecha_sel)
    if (d_sel !=="" && c_sel !== "" && typeof d_sel !== "undefined" && typeof d_sel !== "undefined") {

        var horarios = jsonhorarios[c_sel]['horarios'][d_sel]

        //alert( JSON.stringify( jsonhorarios[c_sel]['horarios'] ) );
        
        if (Object.keys(horarios).length > 0) {
            
            var iniciaen
            var today = new Date();
            var horaactual = today.getHours()
            var diaactual = today.getDay()
            var infon =  fecha_sel[0]
            var html= '<li class="px-b m-0 dateBox alturaHora d-flex flex-column justify-content-center align-items-center" style="line-height:1.1;text-transform: uppercase;">\
                        <span class="m-0 px-2"><b>'+fecha_sel[0].substring(0, 3)+'</b></span>\
                        <span class="m-0 px-2 date">'+fecha_sel[1]+'/'+fecha_sel[3].substring(0, 3)+'</span>\
                    </li>';
            //if(diaactual == 0) {diaactual = 7}
            if (horaactual > horarios.inicio && diaactual == d_sel) {
                iniciaen = horaactual + 1 
            }else{
                iniciaen = horarios.inicio
            }            
            for (var i = iniciaen; i <= horarios.fin; i++) {
                if (i<10) {h='0'+i}else{h=i}
                html += '<li class="nav-item horaSlide px-b">\
                    <a class="waves-effect waves-light alturaHora backHora d-flex flex-column justify-content-center text-black" data-toggle="tab" data-h="'+h+'" href="#tab-panel-0" role="tab">'+h+':00 HRS</a></li>';
            }
            $('#row-hora').removeClass('d-none');
        }  
        $("#co-hora").html(html)
        $("#co-hora").animate({ scrollLeft:0 });
        $(".inp-hora .leftTabNavigation").css({'display':'none'})
        $(".inp-hora .rightTabNavigation").css({'display':'flex'})
    }
}
//Funcion que sirve para activar los custom tabs
function activador(pos) {
    for (var customTab = 0; customTab < CustomTabs.length; customTab++) {
        viewTabsAndControlsHorario($(CustomTabs[customTab]).children(".tabs-wrapper").children(".nav"), pos);
    }
}

//Funcion para visalizar loscontroles de los custom tabs
function viewTabsAndControlsHorario(element, pos, left) {
    //console.log(element[0]);
    console.warn("FUNCION DE LAS FLECHAS: " + element);
    var posicion = 0
    switch (pos) {
        case "left":
            posicion = element.scrollLeft() - 150;
            break;
        case "right":
            posicion = element.scrollLeft() + 150;
            break;
        default:
            posicion = element.scrollLeft();
            break;
    }
    var maxScrollLeft = element[0]["scrollWidth"] - element[0]["clientWidth"];
    //console.log("maxScrollLeft: " + maxScrollLeft);
    //console.log("POSITION: " + posicion);
    //tolerancia = 30;
    if (maxScrollLeft === 0) {
        element.parent().siblings("i").css({ "display": "none" });
    } else if (posicion < tolerancia) {
        //console.log("POSICION MENOR QUE TOLERANCIA");
        element.animate({ scrollLeft: 0 });
        element.parent().siblings(".rightTabNavigation").css({ "display": "flex" });
        element.parent().siblings(".leftTabNavigation").css({ "display": "none" });
    } else if (posicion >= maxScrollLeft - tolerancia) {
        //console.log("POSICION MAYOR QUE SCROLLLEFT - TOLERANCIA"); 
        element.parent().siblings(".rightTabNavigation").css({ "display": "none" });
        element.parent().siblings(".leftTabNavigation").css({ "display": "flex" });
    } else {
        //console.log("TODO LO DEMAS");
        element.parent().siblings("i").css({ "display": "flex" });
    }
    if (left >= 580 && pos == "right") {
        element.parent().siblings(".rightTabNavigation").css({ "display": "none" });
        element.parent().siblings(".leftTabNavigation").css({ "display": "flex" })
    }
}

// (function () {
//     'use strict';
//     window.addEventListener('load', function () {
//         // Fetch all the forms we want to apply custom Bootstrap validation styles to
//         var forms = document.getElementsByClassName('needs-validation');
//         // Loop over them and prevent submission
//         var validation = Array.prototype.filter.call(forms, function (form) {
//             form.addEventListener('submit', function (event) {
//               console.log(form.checkValidity())
//                 if (form.checkValidity() === false) {
//                     event.preventDefault();
//                     event.stopPropagation();
//                 }
//                 form.classList.add('was-validated');
//             }, false);
//         });
//     }, false);
// })();

function validaEmail(inp_value) {
    //Flag que cambiara cuando sea valido
    var emailValid = false;
    var customErrorMessage;
    var arrEmail = inp_value.split('@');
    var lenUser = arrEmail[0].length;
    /*var jsonReader = new readJson();
    var jsonBasura = jQuery("#data_json_basura_email").data();*/
    try {
        var arrDominio = arrEmail[1].split('.');
        var arrLargo = arrDominio.length;
    } catch (err) { }

    //Esta vacio el correo
    if (arrEmail[0] === "") {
        return {
            fieldError: true,
            customErrorMessage: 'Ingrese su correo'
        }
        //Validamos en el json basura la parte uno del correo
    }/*else if(jQuery("#data_json_basura_email").val() == 1 && jsonReader.getKeys(jsonBasura, arrEmail[0]).length > 0 ) {
            return { 
                fieldError : true,
                customErrorMessage: 'Correo inválido' 
            }
         //Validar arroba
        }*/else if (arrEmail.length <= 1) {
        return {
            fieldError: true,
            customErrorMessage: 'Ingrese el correo completo'
        }
        //Validamos en el json basura la parte 2 del correo
    }/*else if(jQuery("#data_json_basura_email").val() == 1 && jsonReader.getKeys(jsonBasura, arrEmail[1]).length > 0) {
            
            return { 
                fieldError : true,
                customErrorMessage: 'Correo inválido' 
            }
        //Longitud del nombre de usuario es muy largo o muy corto el nombre
        }*/else if (lenUser <= 2 || lenUser > 100) {
        return {
            fieldError: true,
            customErrorMessage: 'Longitud de correo invalida'
        }
        //Validamos que no existen caracteres especiales en el nombre de usuario
    } else if (arrEmail[0].search(/[^a-zA-Z0-9_.-]/) != -1) {
        return {
            fieldError: true,
            customErrorMessage: 'Caracter inválido encontrado'
        }
        //Validamos que no existen caracteres especiales en el dominio 
    } else if (arrEmail[1].search(/[^a-zA-Z0-9_.-]/) != -1) {
        return {
            fieldError: true,
            customErrorMessage: 'Caracter inválido encontrado'
        }
        //Validacion de dominios MS   
    } else if (arrEmail[1].search(/(hotmail|outlook|live)/i) != -1 && (arrEmail[0][lenUser - 1].search(/\./) != -1 || arrEmail[0][0].search(/(-|_|\.|[0-9])/) != -1 || arrEmail[0].search(/\.{2}/g) != -1)) {
        return {
            fieldError: true,
            customErrorMessage: 'Correo inválido'
        }
        //Validacion de dominios gmail    
    } else if (arrEmail[1].search(/gmail/i) != -1 && (arrEmail[0][lenUser - 1].search(/[^a-zA-Z0-9]/) != -1 || arrEmail[0][0].search(/(-|_|\.|[0-9])/) != -1 || arrEmail[0].search(/\.{2}/g) != -1 || arrEmail[0].search(/(-|_)/g) != -1)) {
        return {
            fieldError: true,
            customErrorMessage: 'Correo inválido'
        }
        //Validacion de dominios yahoo     
    } else if (arrEmail[1].search(/yahoo/i) != -1 && (arrEmail[0][lenUser - 1].search(/[^a-zA-Z0-9]/) != -1 || arrEmail[0][0].search(/(_|\.|[0-9])/) != -1 || arrEmail[0].search(/(\.|_){2}/g) != -1 || arrEmail[0].search(/-/g) != -1)) {
        return {
            fieldError: true,
            customErrorMessage: 'Correo inválido'
        }
        //Validacion de dominios restantes       
    } else if (arrEmail[1].search(/(^hotmail|outlook|gmail|live|yahoo)/i) == -1 && (arrEmail[0][lenUser - 1].search(/(-|_|\.)/) != -1 || arrEmail[0][0].search(/(-|_|\.|[0-9])/) != -1 || arrEmail[0].search(/(-|_|\.){2}/g) != -1)) {
        return {
            fieldError: true,
            customErrorMessage: 'Correo inválido'
        }
        //Validacion de dominios por ejemplo debe tener @mail.com por ejemplo 
    } else if (arrLargo < 2) {
        return {
            fieldError: true,
            customErrorMessage: 'Ingrese el correo completo'
        }
    } else {
        var arrDominio = arrEmail[1].split('.');
        var arrLargo = arrDominio.length;

        // Hay emails que tienen dominio  y subdominio, por ejemplo fcastill@mail.unitec.mx,
        // entonces verificamos desde el primer elemento al penultimo, ya que el ultimo el es
        // el nombre de dominio superior y requerimos otras validaciones
        for (var x = 0; x <= arrLargo - 2; x++) {
            //esta vacio el nombre del dominio o subdominio?
            if (arrDominio[x] === "") {
                return {
                    fieldError: true,
                    customErrorMessage: 'Correo inválido'
                }
                //es muy largo o muy corto el nombre del dominio?
            } else if (arrDominio[x].length > 36 || arrDominio[x].length < 2) {
                return {
                    fieldError: true,
                    customErrorMessage: 'Correo inválido'
                }
            }
            /*
            //Validación de correo basura en el dominio
            if(Object.keys(jsonBasura).length != 0) {
                for(var i = 0; i < Object.keys(jsonBasura).length; i++){
                    if(arrDominio[x].indexOf(jsonBasura[i]) != -1){
                        
                        return { 
                            fieldError : true,
                            customErrorMessage: 'Correo inválido' 
                        }
                    }
                } 
            }
            */
        }
        // Validamos el nombre del dominio superior
        // esta vacio
        if (arrDominio[arrLargo - 1] === "") {
            return {
                fieldError: true,
                customErrorMessage: 'Correo inválido'
            }
        } else if (arrDominio[arrLargo - 1].length >= 4 || arrDominio[arrLargo - 1].length <= 1) {
            return {
                fieldError: true,
                customErrorMessage: 'Correo inválido'
            }
        }
    }//Else de validaciones principales

    return null;
}


function jsUcfirst(string) 
{
    return string.charAt(0).toUpperCase() + string.slice(1);
}


/*Obtener la fecha Actual By SRP 04-06-2019*/
function getRangoFecha(){
    var f = new Date();
    var fixMes;
    var fixDia = f.getDate();

    //alert( f.getMonth() )

    if ( (f.getMonth() +1) <= 9 ) { 
        fixMes = "0" + (f.getMonth() +1);
    }else{
        fixMes = (f.getMonth() +1);
    }

    if ( fixDia <= 9 ) {            
        fixDia = "0" + fixDia;
    }   

    return f.getFullYear() + "-" + fixMes + "-" + fixDia;

}
/*End Obtener la fecha Actual By SRP 04-06-2019*/