var lFollowX = 0,
    lFollowY = 0,
    x = 0,
    y = 0,
    friction = 1 / 30;
function moveBackground() {
    x += (lFollowX - x) * friction;
    y += (lFollowY - y) * friction;

    translate = 'translate(' + x + 'px, ' + y + 'px) scale(1.1)';
    $('.bg-tour-virtual').css({
        '-webit-transform': translate,
        '-moz-transform': translate,
        'transform': translate
    });
    window.requestAnimationFrame(moveBackground);
}
function bannerHeight() {
    setTimeout(function () {
        var heightNavbar = jQuery(".navbar").height();
        var height = jQuery(window).height() - parseInt(heightNavbar);  //getting windows height
        jQuery('#banner-imagen').css({ 'height': height + 'px', 'margin-top': heightNavbar + 'px' });
    }, 500);
}
jQuery(window).on('mousemove click', function (e) {

    var lMouseX = Math.max(-100, Math.min(100, $(window).width() / 2 - e.clientX));
    var lMouseY = Math.max(-100, Math.min(100, $(window).height() / 2 - e.clientY));
    lFollowX = (65 * lMouseX) / 100; // 100 : 12 = lMouxeX : lFollow
    lFollowY = (20 * lMouseY) / 100;

});

bannerHeight();
moveBackground();