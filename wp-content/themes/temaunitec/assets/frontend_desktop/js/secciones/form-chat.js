
var hor;
var chatSettings = {
  online:{
    Title:"¡Chatea con nosotros!",
    Message : "¡Hola! estamos para apoyarte, ingresa tus datos para iniciar el chat en vivo y te atiendan de manera personalizada.",
    dataType:"onlineForm",
    btnText: "&nbsp;&nbsp;INICIA UNA CONVERSACIÓN",
    fixedButtonText: "Chatea con nosotros"
  },
  offline:{
    Title:"¡Déjanos tu mensaje!",
    Message : "Por favor deja un mensaje y te responderemos vía correo electrónico. ¡Gracias!",
    dataType:"offlineForm",
    btnText: "&nbsp;&nbsp;ENVIAR MENSAJE",
    fixedButtonText: "Déjanos un mensaje"
  }  
}
chatObj = function(config){
  var self = this;
  self.config = config;

  this.constructor = function(){
    var todayis = new Date
    //weekday as a number (0 Domingo - 6 Sabado)
    var d = todayis.getDay();
    var h = todayis.getHours();
    var validOnline = false;
    var sett = self.config.offline
    $.ajax({
      type: "GET",
      url: "/wp-admin/admin-ajax.php",
      data: {
        action: "chat_rules"        
      },
      dataType: "json",
      success: function (res) {                     
        hor = res.data  
        var horCorr = hor.general[d]
        for (var i = 0, len = horCorr.length; i < len; i++) {          
          var horario = horCorr[i];          
          if (h>=horario[0] && h<horario[1]) {
            validOnline =  true
            break;
          }
        }    
        if (validOnline) {
          sett = self.config.online
        }    
        var para = document.createElement("a");
        para.setAttribute("id", "chatButton_pc");
        para.setAttribute("onclick", "nuevoChat.mostrar()");
        para.setAttribute("class", "cardBtnChat d-flex align-items-center justify-content-between");
        para.setAttribute("data-gtm-tr", "chat");
        para.setAttribute("data-gtm-pos", "clic");
        para.setAttribute("data-gtm-location", "boton fijo");
        para.setAttribute("data-gtm-etiqueta", "con formulario");
        para.innerHTML = '<div id="draggable-snap-2" class="px-1">\
                          <div class="d-flex align-items-center justify-content-around h-100" style="width:180px; color:#fff;">\
                            <i class="icon-u icon-u-chat "></i>\
                            <span>'+sett.fixedButtonText+'</span>\
                          </div>\
                      </div>';
        document.body.insertBefore(para, document.body.firstChild);
      },
      error: function (xhr, textStatus, error) {
      },
      beforeSend: function () {          
      },
      complete: function () {          
      }
    });
  }

  this.mostrar = function(){
    var todayis = new Date
    //weekday as a number (0 Domingo - 6 Sabado)
    var d = todayis.getDay();
    var h = todayis.getHours();
    var validOnline = false;
    var sett = self.config.offline
    var horCorr = hor.general[d]
    for (var i = 0, len = horCorr.length; i < len; i++) {          
      var horario = horCorr[i];
      
      if (h>=horario[0] && h<horario[1]) {
        validOnline =  true
        break;
      }
    }    
    if (validOnline) {
      sett = self.config.online
    }

    $("#frm-chat .chat-title-text").text(sett.Title)
    $("#frm-chat .chat-message-text").text(sett.Message)    
    $("#init-chat").html('<i class="icon-u icon-u-chat"></i>'+sett.btnText)
    $("#formulario-chats").data("interaccion",sett.dataType)
    
    openModal('#modal-chat')
    console.warn(validOnline);
  }

  this.enviar = function(){
    if(self.validarform()){
      var trackers = ga.getAll();
      var i, len;
      var cid
      for (i = 0, len = trackers.length; i < len; i += 1) {
        if (trackers[i].get('trackingId') === 'UA-9351414-14') {
          cid = trackers[i].get('clientId');
        }
      }
      var depto = $(".inp-depto .dropdown-item.sel").text();
      var deptoqueue = $(".inp-depto .dropdown-item.sel").data("queue");
      var tipoForm = $("#formulario-chats").data("interaccion");
      $.ajax({
        type: "POST",
        url: "/wp-admin/admin-ajax.php",
        data: 'action=chat_init&'+$("#formulario-chats").serialize()+'&cid='+cid+'&depto='+depto + '&deptoqueue='+deptoqueue+'&tipoForm='+tipoForm
        ,
        success: function (res) {            
          console.log(res);            
          res = JSON.parse(res);
          if (res.success) {
            console.log("saved");
            if(res.data.tipoInteraccion =="online"){
              dataLayer.push({
                'event': 'leadGenerationChat',//dato est·tico 
                'accion' : 'apertura',   
                'etiqueta' : 'aplicacion chat', 
                'section': 'formulario chat', //dato din·mico 
                'Departamento': depto
              })
              self.iniciarChatPC(res.data.chatConfig)
            }else if(res.data.tipoInteraccion=="offline"){
              dataLayer.push({
                'event': 'leadGenerationChat',//dato est·tico    
                'accion' : 'envio',   
                'etiqueta' : 'mensaje offline', 
                'section': 'formulario chat', //dato din·mico 
                'Departamento': depto
              })
              $("#formulario-chats").hide()
              $("#frm-chat .chat-title-text").text("¡Gracias por comunicarte con nosotros!")
              $("#frm-chat .chat-message-text").text("Pronto nos pondremos en contacto contigo.")
            }
          }else{
            console.log("try again")            
          }          
        },
        error: function (xhr, textStatus, error) {
          console.log(xhr);
          console.log(xhr.statusText);
          console.log(textStatus);
          console.log(error);
        },
        beforeSend: function () {          
        },
        complete: function () {
            
        }
      });
    }
  }

  this.validarform = function(){
    var errors = 0;
    $(".invalid-feedback").removeClass('d-block')      
    var vnom= genValidaNombres( $("#name-chat")[0] );       
    if (vnom.fieldError){
      $("#name-chat").siblings( ".myFeedBackError" ).remove()
      $("#name-chat").after(
        '<div class="myFeedBackError">\
        '+decodeURIComponent(vnom.customErrorMessage)+'\
        </div>'
      )
      errors++
    }else{
      $("#name-chat").siblings('.myFeedBackError').remove()
    }  
    var vcel= genvalidaCelular( $("#phone-chat").val() );       
    if (vcel.fieldError){
      $("#phone-chat").siblings( ".myFeedBackError" ).remove()
      $("#phone-chat").after(
        '<div class="myFeedBackError">\
        '+decodeURIComponent(vcel.customErrorMessage)+'\
        </div>'
      )
      errors++
    }else{
      $("#phone-chat").siblings('.myFeedBackError').remove()
    }
    var vmail= genvalidaEmail( $("#email-chat").val() );       
    if (vmail.fieldError){
      $("#email-chat").siblings( ".myFeedBackError" ).remove()
      $("#email-chat").after(
        '<div class="myFeedBackError">\
        '+decodeURIComponent(vmail.customErrorMessage)+'\
        </div>'
      )
      errors++
    }else{
      $("#email-chat").siblings('.myFeedBackError').remove()
    }
    if ($(".inp-depto .dropdown-item.sel").length==0){
      $(".inp-depto").siblings( ".myFeedBackError" ).remove()
      $(".inp-depto").after(
        '<div class="myFeedBackError">\
        Selecciona un departamento\
        </div>'
      )
      errors++
    }else{
      $(".inp-depto").siblings('.myFeedBackError').remove()
    }           
    if ($("#msj-chat").val().length == 0){
      $("#msj-chat").siblings( ".myFeedBackError" ).remove()
      $("#msj-chat").after(
        '<div class="myFeedBackError">\
        Escribe un mensaje\
        </div>'
      )
      errors++
    }else{
      $("#msj-chat").siblings('.myFeedBackError').remove()
    }
    if (errors==0) {
      return true
    }
  }
  this.iniciarChatPC = function(chatConfig){
    $("#blur-chat").hide()
    ININ.webchat.create(chatConfig, function(err, webchat) {
            if (err) {
                throw err;
            }
            // Render to frame
            webchat.renderFrame({
                containerEl: 'frm-chat'
            });
            webchat.chatEnded = function () {
              $('#modal-chat').modal("hide")
              $("#frm-chat iframe").remove()            
              $('#formulario-chats')[0].reset();
              $("#blur-chat").show()
            };
        })      
  }
  this.validUX = function(){
    var deptoqueue = $(".inp-depto .dropdown-item.sel").data("queue");
    var todayis = new Date
    //weekday as a number (0 Domingo - 6 Sabado)
    var d = todayis.getDay();
    var h = todayis.getHours();
    var validOnline = false;
    var sett = self.config.offline
    var horCorr = hor[deptoqueue].Horarios[d]
    for (var i = 0, len = horCorr.length; i < len; i++) {          
      var horario = horCorr[i];
      
      if (h>=horario[0] && h<horario[1]) {
        validOnline =  true
        break;
      }
    }    
    if (validOnline) {
      sett = self.config.online
    }else{      
      if (hor[deptoqueue].QueueRespaldo==null) {
        validOnline = false;
      }else{
        var queueresp = hor[deptoqueue].QueueRespaldo; 
        console.warn(queueresp)
        var horCorr = hor[queueresp].Horarios[d]
        for (var i = 0, len = horCorr.length; i < len; i++) {          
          var horario = horCorr[i];
          
          if (h>=horario[0] && h<horario[1]) {
            validOnline =  true
            break;
          }
        }    
      }      
      if (validOnline) {
        sett = self.config.online
      }
    }
    $("#frm-chat .chat-title-text").text(sett.Title)
    $("#frm-chat .chat-message-text").text(sett.Message)    
    $("#init-chat").html('<i class="icon-u icon-u-chat"></i>'+sett.btnText)
    $("#formulario-chats").data("interaccion",sett.dataType)
  }
  self.constructor();
}
$(document).ready(function () {
 nuevoChat = new chatObj(chatSettings);
  
  $('#name-chat').on( "keyup", function(e){            
    var val = genValidaNombres($(this)[0])    
    if (val.fieldError){
      $(this).siblings( ".myFeedBackError" ).remove()
      $(this).after(
        '<div class="myFeedBackError">\
        '+decodeURIComponent(val.customErrorMessage)+'\
        </div>'
      )
    }else{
      $(this).siblings('.myFeedBackError').remove()
    }
  });    

  $("#openChat").click(function(){
    nuevoChat.mostrar()
  })
  $("#init-chat").click(function(){
    nuevoChat.enviar()
  })
  $(".inp-depto .dropdown-item").click(function(){
    $(".inp-depto .dropdown-item").removeClass('sel');
    var StateName = $(this).text();
    $("#depto-chat").text(StateName);
    $(this).addClass('sel');
    nuevoChat.validUX();
  })

  var substringMat = function (strs) {
        return function findMatches(q, cb) {
          var matches, substringRegex;
          matches = [];
          var substrRegex = new RegExp(q, "i");
          $.each(strs, function (i, str) {
            if (substrRegex.test(str)) {
              matches.push(str);
            }
          });
          cb(matches);
        };
      };
  var domains = [
        "hotmail.com",
        "yahoo.com.mx",
        "msn.com",
        "yahoo.com",
        "gmail.com",
        "outlook.com",
        "live.com.mx",
        "live.com",
        "prodigy.net.mx",
        "icloud.com"
      ];
  $("#email-chat").typeahead(
    {
      hint: true,
      highlight: true,
      minLength: 1
    },
    {
      name: "domains",
      source: substringMat(domains)
    }
  );
  $("#email-chat").on('typeahead:selected', function (evt, item) {                
    var elements = $("#email-chat").val().split("@")        
    $('#email-chat').typeahead('val', item);     
    $('#depto-chat').focus();
  });
  $("#email-chat").on("focusin",function(){
    $("#email-chat").parent().parent().siblings("label").addClass("active");
  })
  var zp = function(){
    var self = this    
    self.livechat = {
      window:{
        show: function(){
          nuevoChat.mostrar()
        }
      }
    }
  }
  $zopim = new zp();
});
