/*para pintar metricas en analitics*/
$(document).ready(function () {
    setInterval('$zopim.livechat.setOnStatus(contador)', 10000);
});
$zopim(function () {
    // $zopim.livechat.window.hide();
    $zopim.livechat.window.onHide(function () {
        $zopim.livechat.button.show();
    });
    $zopim.livechat.setOnUnreadMsgs(function (number) {
        if (number > 0) {
            $zopim.livechat.window.show();
        } 
    });
});

/*Actualización de Funcion Contador para disparar evento cuando no se solicite correo en el chat By Ramiro and SRP 22-11-2018*/
function contador(status) {
    if (status == 'online') { // online
        if (typeof ($zopim.livechat.isChatting()) != "undefined") {
            if ($zopim.livechat.isChatting() == true) {
                if (getCookie("zop_google") != 1) {
                    if ($zopim.livechat.getEmail() != "") {
                        sendHB(status);
                        setCookie("zop_google", 1);
                        dataLayer.push({ 'event': 'CHAT_SERVIDO_DC' });
                        dataLayer.push({
                            'originFormChat': 'footer', //dato din·mico 
                            'department': 'no obtenido', //dato din·mico
                            'status': 'Chat Online', //dato din·mico
                            'event': 'leadGenerationChat'//dato est·tico
                        });
                    } // Cierra valida email
                    // else{ // Conversaciones abordadas/ trigger
                    //   setCookie("zop_google",1);
                    //   dataLayer.push({'event': 'CHAT_SERVIDO_DC'});            
                    //   dataLayer.push({
                    //     'originFormChat': 'footer', //dato din·mico 
                    //     'department': 'no obtenido', //dato din·mico
                    //     'status': 'Chat Online s/correo', //dato din·mico
                    //     'event': 'leadGenerationChat'//dato est·tico
                    //   });
                    // }
                } // Cierra cookie
            } // Cierra isChatting
        }
    }
    else { // offline
        if (getCookie("zop_google") != 1) {
            if ($zopim.livechat.getEmail() != "") {
                sendHB(status);
                console.log('Chat offline');
                setCookie("zop_google", 1);
                dataLayer.push({ 'event': 'CHAT_SERVIDO_DC' });
                dataLayer.push({
                    'originFormChat': 'footer', //dato din·mico 
                    'department': 'no obtenido', //dato din·mico
                    'status': 'Chat Offline', //dato din·mico
                    'event': 'leadGenerationChat'//dato est·tico
                });
            } // Cierra valida email
        } // Cierra cookie
    }
} // Cierra contador()
/*End Actualización de Funcion Contador para disparar evento cuando no se solicite correo en el chat By Ramiro and SRP 22-11-2018*/

function sendHB(flag) {
    var name = $zopim.livechat.getName();
    var email = $zopim.livechat.getEmail();
    var phone = $zopim.livechat.getPhone();
    jQuery.ajax({
        type: "POST",
        //
        url: "https://www.unitec.mx/assets/zopim/service_chat_qa.php",
        data: { email: email, name: name, phone: phone, flag: flag }
    });
}

function setCookie(cname, cvalue, exdays) {
    var d = new Date();
    d.setTime(d.getTime() + (exdays * 24 * 60 * 60 * 1000));
    var expires = "expires=" + d.toUTCString();
    document.cookie = cname + "=" + cvalue + "; " + expires;
}

function getCookie(cname) {
    var name = cname + "=";
    var ca = document.cookie.split(';');
    for (var i = 0; i < ca.length; i++) {
        var c = ca[i];
        while (c.charAt(0) == ' ') c = c.substring(1);
        if (c.indexOf(name) == 0) return c.substring(name.length, c.length);
    }
    return "";
}

function checkCookie(valor) {
    var user = getCookie(valor);
    if (user != "") {
        return true;
    }
    else {
        return false;
    }
}

function openChatLive() {
    $zopim.livechat.window.show();
}

function clickTel(activarTel) {
    window.location.assign('018007864832');
    // location.href = activarTel.href;
    return false;
}
function clickCalcu(activarCalcu) {
    location.href = '/calcula-tu-beca';
    return false;
}