$(document).ready(function () {
    youtubeLazy();
});

//animate for steppers
$listo = "<i class='icon-u icon-u-check' aria-hidden='true'></i>";
/*$listo = "<i class='icon-u-check'></i>";*/
$("ul.stepper li").on("click", function(){
    $activo = $(this).siblings(".active").find("a .circle").addClass("listo").html($listo);
    $(this).siblings("li").removeClass( "active" );
    $(this).addClass( "active" );
});
//next stepper button   
$(".nextSeccion").on("click", function(e){
    $lipadre = $(this).parent().parent().parent();
    $ulpadre = $lipadre.parent();
    $lipadre.find("a .circle").addClass("listo").html($listo)
    $ulpadre.find("li").removeClass( "active" );
    $lipadre.next().addClass("active");
    return false;
});