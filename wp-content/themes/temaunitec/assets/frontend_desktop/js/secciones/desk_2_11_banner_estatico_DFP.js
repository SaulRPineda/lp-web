
$('.banner-share > a').on('click', function (e) {
    e.preventDefault() // prevent default action - hash doesn't appear in url
    $(this).parent().find('div').toggleClass('social-reveal-active');
    $(this).toggleClass('share-expanded');
});

var arrnids = [];
var aleatorio = "";

function extraer(linea) {
    console.log("Busco la compilacion");
    var b = Handlebars.compile($("#hr-template-nids").html());
    var a = "SELECT * WHERE B = '" + "activado" + "' ";

    if (linea !== "") {
        a += "AND C LIKE '%" + linea + "%' ";
    }
    console.log(a);
    console.log(b);
    console.log(mySpreadsheet);

    $("#nids").sheetrock({
        url: mySpreadsheet,
        query: a,
        rowHandler: b,
        labels: ['id'],
        // callback: function(error, options, response) { console.log(error); }
        callback: fillArray
    })
}

function fillArray() {
    console.log("Empezando el fill");
    var c;
    var d = [];
    $("#nids :input").each(function () {
        if (this.type === "text") {
            d.push($(this).val())
        }
    });
    console.log("VEO EL ARREGLO");
    console.log(d);
    d = d.sort(function () {
        return Math.random() - 0.5
    });
    c = Math.floor(Math.random() * d.length);
    imprime(d[c]);
    // imprime(1);
}

function orientacionDispositivo() {
    if (Math.abs(window.orientation) === 90) {
        // Landscape
        var tipo= "horizontal";

        $( ".bann_wind" ).each(function() {
            jQuery( this ).css('background-image','url(' + $( this ).attr( "link-mobile-horizontal" ) + ')').promise().done(function() {
                jQuery(this).animate({
                    opacity: 1
                }, 600)
            });
        });

    } else {
        // Portrait
        var tipo= "vertical";

        $( ".bann_wind" ).each(function() {
            jQuery( this ).css('background-image','url(' + $( this ).attr( "link-mobile-vertical" ) + ')').promise().done(function() {
                jQuery(this).animate({
                    opacity: 1
                }, 600)
            });
        });

    }
}

function imprime(c) {
    console.log("Empezando el imprime");
    var b = Handlebars.compile($("#hr-template").html());
    var a = "SELECT * WHERE A = " + c + " LIMIT 1";
    console.log("NUEVA CONSULTA: ");
    console.log(a);
    $("#carousel-banner").sheetrock({
        url: mySpreadsheet,
        query: a,
        rowHandler: b,
        labels: ['id'],
        //callback: actualizaImpresiones
    })
}

function asignaURL(a) {
    $("#" + a).attr("href", url + $("#urlok").val())
}

function getCookie(cname) {
    var name = cname + "=";
    var decodedCookie = decodeURIComponent(document.cookie);
    var ca = decodedCookie.split(';');
    for (var i = 0; i < ca.length; i++) {
        var c = ca[i];
        while (c.charAt(0) == ' ') {
            c = c.substring(1);
        }
        if (c.indexOf(name) == 0) {
            return c.substring(name.length, c.length);
        }
    }
    return "";
}

function checkCookie() {
    var cookie = getCookie("utm_campaign");
    var cookieVal = null;

    if (cookie != "") {
        console.log("Bienvenido Nuevamente " + cookie);
        cookieVal = cookie;
    }
    return cookieVal;
}

function actualizaClics(a, f, c, g, b, d) {
    $("#nclics").val(parseInt($("#nclics").val()) + parseInt(1));
    f = (f === 0 || f === "undefined" || f === "") ? 1 : f;
    c = (c === "") ? parseInt($("#nclics").val()) : c;
    $.ajax({
        method: "POST",
        url: "http://unitecmx-universidadtecno.netdna-ssl.com/DFP/2017/medicionBanner/medicion.php",
        beforeSend: function (h, e) { },
        data: {
            title: a,
            nid: f,
            nimpresiones: parseInt($("#nimpresiones").val()),
            nclics: c,
            campania: g,
            idSpreadsheet: b,
            nombreHoja: d
        }
    }).done(function () {
        $("#nclics").val(c)
    })
}

function actualizaImpresiones(f, c, e, d, g, b) {
    c = (c === 0 || c === "undefined" || c === "") ? 1 : c;
    e = (e === "") ? parseInt($("#nimpresiones").val()) : e;
    $.ajax({
        method: "POST",
        url: "http://unitecmx-universidadtecno.netdna-ssl.com/DFP/2017/medicionBanner/medicion.php",
        beforeSend: function (a, h) { },
        data: {
            title: f,
            nid: c,
            nimpresiones: e,
            nclics: parseInt($("#nclics").val()),
            campania: d,
            idSpreadsheet: g,
            nombreHoja: b
        }
    }).done(function () {
        $("#nimpresiones").val(e)
    })
}