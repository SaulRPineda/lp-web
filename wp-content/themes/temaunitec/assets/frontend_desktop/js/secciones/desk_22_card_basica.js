
var div_ancho_card_basica = $(".carrousel_inner_cards_basicas ul li").width();
var elementos_card_basica = 1;
$('.carrousel_right_card_basica').click(function(){
    imgLazy();
    // var elementsCountCardsBasicas = $('.carrousel_inner_cards_basicas ul li').length;
    var elementsCountCardsBasicas = $('#totalCardsBasicas').val();
    var positionCardsBasicas =$('.carrousel_cards_basicas.active').attr('data-pos');
    positionCardsBasicas = parseInt(positionCardsBasicas, 10);

    if (positionCardsBasicas < elementsCountCardsBasicas) {
        
        elementos_card_basica = elementos_card_basica + 1;
        positionCardsBasicas = positionCardsBasicas+1;
        $('.carrousel_right_card_basica').removeClass('right_inactive');
        $('.carrousel_left_card_basica').removeClass('d-none');         

        /*console.log("Elemento Actual: " + elementos_card_basica + " Total: " +elementsCountCardsBasicas);*/   

        if ( positionCardsBasicas == elementsCountCardsBasicas || elementos_card_basica == elementsCountCardsBasicas ){
            $(this).addClass('left_inactive');
            $(this).addClass('d-none');
        }
        var posCardBasica = positionCardsBasicas*(div_ancho_card_basica+18);
        $('.carrousel_cards_basicas.active').attr('data-pos',positionCardsBasicas);
        $('.carrousel_inner_cards_basicas').animate({'scrollLeft' : posCardBasica });
    }
});

$('.carrousel_left_card_basica').click(function(){
    imgLazy();
    // var elementsCountCardsBasicas = $('.carrousel_inner_cards_basicas ul li').length;
    var elementsCountCardsBasicas = $('#totalCardsBasicas').val();
    var positionCardsBasicas = $('.carrousel_cards_basicas.active').attr('data-pos');
    positionCardsBasicas=parseInt(positionCardsBasicas, 10);

    if (positionCardsBasicas > 0)
    {
        $('.carrousel_left_card_basica').removeClass('left_inactive');
        $('.carrousel_right_card_basica').removeClass('d-none');
        elementos_card_basica = elementos_card_basica - 1;
        positionCardsBasicas = positionCardsBasicas - 1;            

        /*console.log("Elemento Actual: " + elementos_card_basica + " Total: " +elementsCountCardsBasicas);*/

        if(positionCardsBasicas == 0)
            {
                $(this).addClass('right_inactive');
                $(this).addClass('d-none');
            }
        var posCardBasica = positionCardsBasicas*(div_ancho_card_basica+18);
        $('.carrousel_cards_basicas.active').attr('data-pos',positionCardsBasicas);
        $('.carrousel_inner_cards_basicas').animate({'scrollLeft' : posCardBasica });
    }
});

$('a.tab-card-basica-text').click(function(){
    imgLazy();
    elementos_card_basica = 1;
    var id_carrusel_card_basica = $(this).attr('href');
    var conteo_li_card_basica = $(id_carrusel_card_basica + " ul li.cardstext").length;
    $('#totalCardsBasicas').val(conteo_li_card_basica);
    $('.carrousel_cards_basicas.active').attr('data-pos',0);
    $('.carrousel_inner_cards_basicas').scrollLeft(0);
    if(conteo_li_card_basica == 1){
        $('.carrousel_left_card_basica').addClass('right_inactive');
        $('.carrousel_left_card_basica').addClass('d-none');
        $('.carrousel_right_card_basica').addClass('left_inactive');
        $('.carrousel_right_card_basica').addClass('d-none');
    } else {
        $('.carrousel_left_card_basica').addClass('right_inactive');
        $('.carrousel_left_card_basica').addClass('d-none');
        $('.carrousel_right_card_basica').removeClass('left_inactive');
        $('.carrousel_right_card_basica').removeClass('d-none');
    }
});