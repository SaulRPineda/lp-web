
var div_ancho = $(".carrousel_inner_testimoniales ul li").width();
/*Elementos a Mostrar Update By SRP*/
var elementos_testimoniales = 3;

    if($(window).width() <= 991)
    {
        elementos_testimoniales = 1;
    };

$('.carrousel_right_testimoniales').click(function(){
    var elementsCountTestimoniales = $('.carrousel_inner_testimoniales ul li').length;
    // var elementsCountTestimoniales = $('#totalTestimoniales').val();
    var positionTestimoniales =$('.carrousel_testimoniales').attr('data-pos');
    positionTestimoniales = parseInt(positionTestimoniales, 10);

    if (positionTestimoniales < elementsCountTestimoniales)
    {
        elementos_testimoniales = elementos_testimoniales + 1;
        positionTestimoniales = positionTestimoniales+1;
        $('.carrousel_right_testimoniales').removeClass('right_inactive');
        $('.carrousel_left_testimoniales').removeClass('d-none');         

        /*console.log("Elemento Actual: " + elementos_testimoniales + " Total: " +elementsCountTestimoniales);*/   

        if ( positionTestimoniales == elementsCountTestimoniales || elementos_testimoniales == elementsCountTestimoniales ){
            $(this).addClass('left_inactive');
            $(this).addClass('d-none');
        }
        var posTestimoniales = positionTestimoniales*(div_ancho+30);
        $('.carrousel_testimoniales').attr('data-pos',positionTestimoniales);
        $('.carrousel_inner_testimoniales').animate({'scrollLeft' : posTestimoniales });
    }
});

$('.carrousel_left_testimoniales').click(function(){
    var positionTestimoniales = $('.carrousel_testimoniales').attr('data-pos');
    var elementsCountTestimoniales = $('.carrousel_inner_testimoniales ul li').length;
    // var elementsCountTestimoniales = $('#totalTestimoniales').val();
    positionTestimoniales=parseInt(positionTestimoniales, 10);

    if (positionTestimoniales > 0)
    {
        $('.carrousel_left_testimoniales').removeClass('left_inactive');
        $('.carrousel_right_testimoniales').removeClass('d-none');
        elementos_testimoniales = elementos_testimoniales - 1;
        positionTestimoniales = positionTestimoniales - 1;            

        /*console.log("Elemento Actual: " + elementos_testimoniales + " Total: " +elementsCountTestimoniales);*/

        if(positionTestimoniales == 0)
            {
                $(this).addClass('right_inactive');
                $(this).addClass('d-none');
            }
        var posTestimoniales = positionTestimoniales*(div_ancho+30);
        $('.carrousel_testimoniales').attr('data-pos',positionTestimoniales);
        $('.carrousel_inner_testimoniales').animate({'scrollLeft' : posTestimoniales });
    }
});