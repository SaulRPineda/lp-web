/*Elementos a Mostrar AMC*/
var elementos_nuevas_carreras = 2;

$(window).resize(function () {
    div_ancho = $(".carrousel_inner_nuevas_carreras > ul").width() / 2;
    // var posTestimoniales = positionTestimoniales * div_ancho;
    $('.carrousel_nuevas_carreras').attr('data-pos', 0);
    $('.carrousel_inner_nuevas_carreras').animate({ 'scrollLeft': 0 });
    $('.carrousel_right_nuevas_carreras').removeClass('right_inactive left_inactive d-none');
    $('.carrousel_left_nuevas_carreras').addClass('d-none');
    if ($(window).width() <= 991) {
        elementos_nuevas_carreras = 1;
    }else{
        elementos_nuevas_carreras = 2;
    }
});

if ($(window).width() <= 991) {
    elementos_nuevas_carreras = 1;
};
$('.carrousel_right_nuevas_carreras').click(function () {
    // alert(elementos_nuevas_carreras);
    var div_ancho = $(".carrousel_inner_nuevas_carreras > ul").width() / 2;
    var positionTestimoniales = $('.carrousel_nuevas_carreras').attr('data-pos');
    var elementsCountTestimoniales = $('.carrousel_inner_nuevas_carreras > ul > li').length;
    // var elementsCountTestimoniales = $('#totalTestimoniales').val();
    positionTestimoniales = parseInt(positionTestimoniales, 10);

    if (positionTestimoniales < elementsCountTestimoniales) {
        imgLazy();
        elementos_nuevas_carreras = elementos_nuevas_carreras + 1;
        // alert(elementos_nuevas_carreras);
        positionTestimoniales = positionTestimoniales + 1;
        $('.carrousel_right_nuevas_carreras').removeClass('right_inactive');
        $('.carrousel_left_nuevas_carreras').removeClass('d-none');

        /*console.log("Elemento Actual: " + elementos_nuevas_carreras + " Total: " +elementsCountTestimoniales);*/

        if (positionTestimoniales == elementsCountTestimoniales || elementos_nuevas_carreras == elementsCountTestimoniales) {
            $(this).addClass('left_inactive');
            $(this).addClass('d-none');
        }
        var posTestimoniales = positionTestimoniales * div_ancho;
        $('.carrousel_nuevas_carreras').attr('data-pos', positionTestimoniales);
        $('.carrousel_inner_nuevas_carreras').animate({ 'scrollLeft': posTestimoniales });
    }
    // $(".loader").fadeOut("slow");
    // alert(positionTestimoniales + ':' + elementos_nuevas_carreras + ':' +elementsCountTestimoniales);
});

$('.carrousel_left_nuevas_carreras').click(function () {
    var div_ancho = $(".carrousel_inner_nuevas_carreras > ul").width() / 2;
    var positionTestimoniales = $('.carrousel_nuevas_carreras').attr('data-pos');
    // var elementsCountTestimoniales = $('.carrousel_inner_nuevas_carreras ul li').length;
    // var elementsCountTestimoniales = $('#totalTestimoniales').val();
    positionTestimoniales = parseInt(positionTestimoniales, 10);

    if (positionTestimoniales > 0) {
        imgLazy();
        $('.carrousel_left_nuevas_carreras').removeClass('left_inactive');
        $('.carrousel_right_nuevas_carreras').removeClass('d-none');
        elementos_nuevas_carreras = elementos_nuevas_carreras - 1;
        positionTestimoniales = positionTestimoniales - 1;

        /*console.log("Elemento Actual: " + elementos_nuevas_carreras + " Total: " +elementsCountTestimoniales);*/

        if (positionTestimoniales == 0) {
            $(this).addClass('right_inactive');
            $(this).addClass('d-none');
        }
        var posTestimoniales = positionTestimoniales * div_ancho;
        $('.carrousel_nuevas_carreras').attr('data-pos', positionTestimoniales);
        $('.carrousel_inner_nuevas_carreras').animate({ 'scrollLeft': posTestimoniales });
    }
});