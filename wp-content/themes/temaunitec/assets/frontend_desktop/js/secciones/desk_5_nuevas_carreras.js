$(document).ready(function(){
    
	/*Se agrega estilo para Marcar la primera opcion de carreras*/
    $(".grupo-carreras-nuevas li a:first").addClass('active-link');

});

/* Remueve la clase active de todos los <a> y la añade a la que se dio click */
$(".link-nuevas-carreras").on("click", function(id){
    $('.grupo-carreras-nuevas li a').removeClass("active-link");
    $(this).addClass('active-link');
    $('img.lazy').lazy({
        bind: "event",
        delay: 0
    });
    return false;
});

/* Localiza el <div> con el id específico y añade la clase active para mostrar el elemento */
function updateImage(id) {
    var idCarreranueva = '_desk_5_' + id;
    $("#"+idCarreranueva).addClass('active');
    $("#"+idCarreranueva).siblings().removeClass('active');
};
