<?php
/**
 * Template Name: Colegiaturas Page
 * Description: Pagina de Colegiaturas Template
 *
 * @package WordPress
 * @subpackage themename
 */
?>
<!DOCTYPE HTML>

<html itemscope class="no-js" lang="es"><!--<![endif]-->
	<head>
		<title>Colegiaturas</title>
		<meta charset="UTF-8">
		<meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no">
		<meta name="theme-color" content="#f68b1f">
		<!-- Agrega las tres etiquetas siguientes en la sección "head". -->
		<meta itemprop="name" content="Colegiaturas">
		<meta itemprop="description" content="Colegiaturas de UNITEC Universidad de Mexico, conoce el importe de tu colegiatura en unos sencillos pasos a través de nuestra web en sólo unos segundos">

		<link rel="pingback" href="<?php echo get_template_directory_uri()?>/xmlrpc.php">
		<link rel="shortcut icon" type="image/png" href="<?php echo get_template_directory_uri()?>/assets/images/favicon/favicon.png">

		<link rel='dns-prefetch' href='//s.w.org' />
    <link rel="alternate" type="application/rss+xml" title="UNITEC Colegiaturas RSS de los comentarios" href="<?php echo get_template_directory_uri()?>/colegiaturas/feed/" />
    <meta name="generator" content="WordPress 4.9.8" />
    <meta property="fb:admins" content="YOUR USER ID">
    <meta property="og:title" content="Colegiaturas">
    <meta property="og:type" content="article">
    <meta property="og:url" content="<?php echo get_template_directory_uri()?>/colegiaturas/"><meta property="og:site_name" content="UNITEC">
    
    <link rel="EditURI" type="application/rsd+xml" title="RSD" href="<?php echo get_template_directory_uri()?>/xmlrpc.php?rsd" />
    <link rel="wlwmanifest" type="application/wlwmanifest+xml" href="<?php echo get_template_directory_uri()?>/wp-includes/wlwmanifest.xml" /> 
    <!-- <link rel="canonical" href="https://www.unitec.mx/colegiaturas/" /> -->
    
    
    <script type="text/javascript">
			var ajaxurl =	"<?php echo get_template_directory_uri()?>/wp-admin/admin-ajax.php";
			var wpurl =		"<?php echo get_template_directory_uri()?>";
    </script>
    <!-- Universal Analytics -->

    <?php 
      if( wp_is_mobile() ) {
          $menu = 'secciones_mobile/mob_1_menu';
          $footer = 'secciones_mobile/mob_21_footer';
          $carpeta = 'frontend';
          $angular = 'angularapp';
          $estilosMenu = get_template_directory_uri(). '/assets/frontend/css/secciones/min/mob_1_menu.min.css';
          $estilosFooter =  get_template_directory_uri().'/assets/frontend/css/secciones/min/mob_21_footer.min.css';
          $estilosGenerales =  get_template_directory_uri().'/assets/frontend/css/generales/min/generales_mobile_temaunitec.min.css';
          $jsMenu =  get_template_directory_uri().'/assets/frontend/js/secciones/min/mob_1_menu.min.js';
      }
          else{
              $menu = 'secciones_desk/desk_1_menu';
              $footer = 'secciones_desk/desk_21_footer';
              $carpeta = 'frontend_desktop';
              $angular = 'angularapp_desk';
              $estilosMenu = get_template_directory_uri(). '/assets/frontend_desktop/css/secciones/min/desk_1_menu.min.css';
              $estilosFooter =  get_template_directory_uri().'/assets/frontend_desktop/css/secciones/min/desk_21_footer.min.css';
              $estilosGenerales =  get_template_directory_uri().'/assets/frontend_desktop/css/generales/min/generales_mobile_temaunitec.min.css';
              $jsMenu =  get_template_directory_uri().'/assets/frontend_desktop/js/secciones/min/desk_1_menu.min.js';
          } 
      ?>

    <link rel="stylesheet" href="<?php echo get_template_directory_uri()?>/assets/frontend_desktop/css/secciones/min/404.min.css"> 
    <link rel="stylesheet" href="<?php echo get_template_directory_uri()?>/assets/frontend/css/vendor/min/bootstrap.min.css">
    <link rel="stylesheet" href="<?php echo get_template_directory_uri()?>/assets/frontend/css/vendor/min/mdb.min.css">

    <!-- ESTILOS -->
    <link rel="stylesheet" type="text/css" href="<?php echo get_template_directory_uri();?>/colegiaturas/css/min/jquery-ui.min.css">
    <link rel="stylesheet" type="text/css" href="<?php echo get_template_directory_uri();?>/colegiaturas/css/min/carousel.min.css">
    <!-- CSS files Carousel -->
    <link rel="stylesheet" type="text/css" href="<?php echo get_template_directory_uri();?>/colegiaturas/css/min/touchcarousel.min.css" />
    <!-- Skin Stylesheet Carousel-->
    <link rel="stylesheet" type="text/css" href="<?php echo get_template_directory_uri();?>/colegiaturas/css/min/grey-blue-skin.min.css" />
    <!-- Tamaños Adaptativos de elementos pagina -->
    <link rel="stylesheet" type="text/css" href="<?php echo get_template_directory_uri();?>/colegiaturas/css/min/calculadora_adaptable.min.css" />
    <link rel="stylesheet" type="text/css" href="<?php echo get_template_directory_uri();?>/colegiaturas/css/min/calculadora.min.css" />
    <link rel="stylesheet" type="text/css" href="<?php echo get_template_directory_uri();?>/colegiaturas/css/min/colegiaturas.min.css" />
    <!-- TERMINAN ESTILOS -->
    

    <!-- Universal Analytics -->
    <script type='text/javascript' src='<?php echo get_template_directory_uri()?>/assets/<?php echo $carpeta; ?>/js/vendor/min/jquery-2.2.3.min.js'></script>
    <script type='text/javascript' src='<?php echo get_template_directory_uri()?>/assets/<?php echo $carpeta; ?>/js/vendor/min/jquery-3.1.1.min.js'></script>
    <script type='text/javascript' src='<?php echo get_template_directory_uri()?>/assets/<?php echo $carpeta; ?>/js/vendor/min/jquery-ui.min.js'></script>
    <script type='text/javascript' src='<?php echo get_template_directory_uri()?>/assets/<?php echo $carpeta; ?>/js/secciones/min/generales.min.js'></script>
    <script type='text/javascript' src='<?php echo get_template_directory_uri()?>/assets/<?php echo $carpeta; ?>/js/vendor/min/bootstrap3-typeahead.min.js'></script>
    

    <link rel="stylesheet" href="<?php echo get_template_directory_uri()?>/assets/frontend_desktop/css/secciones/min/404.min.css"> 
    <link rel="stylesheet" href="<?php echo get_template_directory_uri()?>/assets/frontend/css/vendor/min/bootstrap.min.css">
    <link rel="stylesheet" href="<?php echo get_template_directory_uri()?>/assets/frontend/css/vendor/min/mdb.min.css">
    <link rel="stylesheet" href="<?php echo $estilosMenu; ?>">
    <link rel="stylesheet" href="<?php echo $estilosGenerales; ?>">
    <link rel="stylesheet" href="<?php echo $estilosFooter; ?> ">


  </head>
  <style>
    .side-nav{left:-100%; width:30%}
  </style>
	<body  class="page-template page-template-colegiaturas-page page-template-colegiaturas-page-php page page-id-1753 chrome colegiaturas" data-spy="scroll" data-target=".navbar-fixed-top" id="page-top" onUnload="/*deleteAllCookies();*/">


    <?php get_template_part( $menu ); ?>


    <section id="middle" class="container" style="padding-top:80px;width:95%">
      <div class="middle_inner"> 
      <h1 class="w-75 mx-auto text-center titleSection"> Colegiaturas</h1>
      <article class="col-12 text-center d-flex justify-content-center" data-wow-offset="50">
        <h4 class="w-100 descriptionSection mt-1">Aquí podrás consultar de manera rápida y sencilla tu colegiatura. Sólo selecciona tu campus y tu carrera.</h4>
      </article>

        <div id="contenedorCalculadora" style="padding-bottom:50px;">
          <div>
          <h4 class="w-100 text-center descriptionSection mt-1 mb-1">Elige tu campus</h4>
            <div id="carouselCampus" class="touchcarousel grey-blue">
              <ul id="campusUNITEC" class="touchcarousel-container h-100 row">
                <li class="campus touchcarousel-item card"> <img src="<?php echo get_template_directory_uri(); ?>/colegiaturas/img/atizapan.svg" id="imgCampus_1" class="imgCampus" title="Campus Atizapán" alt="Campus Atizapán" />
                  <div class="tituloCampus">Atizapán</div>
                </li>
                <li class="campus touchcarousel-item card"> <img src="<?php echo get_template_directory_uri(); ?>/colegiaturas/img/cuitlahuac.svg" id="imgCampus_2" class="imgCampus" title="Campus Cuitláhuac" alt="Campus Cuitláhuac" />
                  <div class="tituloCampus">Cuitláhuac</div>
                </li>
                <li class="campus touchcarousel-item card"> <img src="<?php echo get_template_directory_uri(); ?>/colegiaturas/img/ecatepec.svg" id="imgCampus_3" class="imgCampus" title="Campus Ecatepec" alt="Campus Ecatepec" />
                  <div class="tituloCampus">Ecatepec</div>
                </li>
                <li class="campus touchcarousel-item card"> <img src="<?php echo get_template_directory_uri(); ?>/colegiaturas/img/marina.svg" id="imgCampus_4" class="imgCampus" title="Campus Marina" alt="Campus Marina" />
                  <div class="tituloCampus">Marina</div>
                </li>
                <li class="campus touchcarousel-item card"> <img src="<?php echo get_template_directory_uri(); ?>/colegiaturas/img/sur.svg" id="imgCampus_5" class="imgCampus" title="Campus Sur" alt="Campus Sur" />
                  <div class="tituloCampus">Sur</div>
                </li>
                <li class="campus touchcarousel-item card"> <img src="<?php echo get_template_directory_uri(); ?>/colegiaturas/img/toluca.svg" id="imgCampus_6" class="imgCampus" title="Campus Toluca" alt="Campus Toluca" />
                  <div class="tituloCampus">Toluca</div>
                </li>
                <li class="campus touchcarousel-item card"> <img src="<?php echo get_template_directory_uri(); ?>/colegiaturas/img/enlinea.svg" id="imgCampus_7" class="imgCampus" title="Campus Virtual" alt="Campus Virtual" />
                  <div class="tituloCampus">En Línea</div>
                </li>
                <li class="campus touchcarousel-item card"> <img src="<?php echo get_template_directory_uri(); ?>/colegiaturas/img/marina.svg" id="imgCampus_8" class="imgCampus" title="Campus Leon" alt="Campus Leon" />
                  <div class="tituloCampus">León</div>
                </li>
                <li class="campus touchcarousel-item card"> <img src="<?php echo get_template_directory_uri(); ?>/colegiaturas/img/atizapan.svg" id="imgCampus_9" class="imgCampus" title="Campus Guadalajara" alt="Campus Leon" />
                  <div class="tituloCampus">Guadalajara</div>
                </li>
                <li class="campus touchcarousel-item card"> <img src="<?php echo get_template_directory_uri(); ?>/colegiaturas/img/cuitlahuac.svg" id="imgCampus_10" class="imgCampus" title="Campus Queretaro" alt="Campus Querétaro" />
                  <div class="tituloCampus">Querétaro</div>
                </li>
              </ul>
            </div>
          </div>
          <div class="clear"></div>
          <div id="areaCarreras" style="display:none;">
            <h4 class="w-100 text-center descriptionSection mt-1 mb-1">Elige la carrera que estudias en la UNITEC</h4>
            <div id="carouselCarreras" class="touchcarousel grey-blue">
              <ul id="ofertaCarreras" class="row touchcarousel-container">
                <li id="PREPA" class="carrera touchcarousel-item borderR card">
                  <div class="ofertaUnitec"> </div>
                  <!-- <div class="col-lg-2 col-md-3 col-cuadros-contenedor">
                    <div class="col-12 col-cuadros-lineas d-flex flex-column align-items-center justify-content-center">
                      <div class="titulo-lineas-cuadros" style="text-align:center;">Preparatoria</div>
                      <div class="imagen-lineas-cuadros cuadro-prepa PREPA"></div>
                      <button class="btn-dtc waves-effect waves-light" id="" role="button">Selecciona</button>
                    </div>
                  </div> -->
                </li>
                <li id="LIC" class="carrera touchcarousel-item borderR card">
                  <div class="ofertaUnitec non-draggable"> </div>
                </li>
                <li id="LC" class="carrera touchcarousel-item borderR card">
                  <div class="ofertaUnitec non-draggable"> </div>
                </li>
                <li id="DIP" class="carrera touchcarousel-item borderR card">
                  <div class="ofertaUnitec non-draggable"> </div>
                </li>
                <li id="POS" class="carrera touchcarousel-item borderR card">
                  <div class="ofertaUnitec non-draggable"> </div>
                </li>
                <li id="ESP" class="carrera touchcarousel-item borderR card">
                  <div class="ofertaUnitec non-draggable"> </div>
                </li>
                <li id="HLX" class="carrera touchcarousel-item borderR card">
                  <div class="ofertaUnitec non-draggable"> </div>
                </li>
              </ul>
            </div>
            <div class="clear"></div>
          </div>
          <div class="clear"></div>
          <div id="unitecFinal" style="display:none;">
            <div id="infoLegales" class="areaFinal"  style="display:none;">
              <div class="infoBecas" style="display:none;"> </div>
            </div>
            <div id="showCosto" class="areaFinal border"  style="display:none;"> </div>
            <div class="clear"></div>
            <div id="notaPrincipal" style="display:none;"> </div>
          </div>
        </div>
        <div id="colegUNITEC"></div>
      </div>
    </section>

    <?php get_template_part( $footer ); ?>

    <script type="text/javascript">
        jQuery.browser = {};
        (function () {
            jQuery.browser.msie = false;
            jQuery.browser.version = 0;
            if (navigator.userAgent.match(/MSIE ([0-9]+)\./)) {
                jQuery.browser.msie = true;
                jQuery.browser.version = RegExp.$1;
            }
        })();
    </script>
    <script type="text/javascript" language="javascript" src="<?php echo get_template_directory_uri(); ?>/colegiaturas/jquery.ui.touch-punch.js"></script>
    <script type="text/javascript" language="javascript" src="<?php echo get_template_directory_uri(); ?>/colegiaturas/jquery.blockUI.js"></script>
    <script type="text/javascript" language="javascript" src="<?php echo get_template_directory_uri(); ?>/colegiaturas/jquery.easing.1.3.js"></script>
    <script type="text/javascript" language="javascript" src="<?php echo get_template_directory_uri(); ?>/colegiaturas/jquery.touchcarousel-1.1.min.js"></script>
    <script type="text/javascript" language="javascript" src="<?php echo get_template_directory_uri(); ?>/colegiaturas/jquery.easing.compatibility.js"></script>


		<script>
      document.onload = function() {
        jQuery.noConflict();
      };
		// Code that uses other library's $ can follow here.
		</script> 
  </body>


  <app-modal></app-modal>
    <modal-formulario-tradicional></modal-formulario-tradicional>
    <modal-app-formulario-tradicional></modal-app-formulario-tradicional>
    <modal-calculadora-formulario-tradicional></modal-calculadora-formulario-tradicional>
    <input id="data_json_basura" type="hidden" value="0">
    <input id="data_json_basura_email" type="hidden" value="0">
    <input id="formApp" type="hidden">
    <input id="data_json_basura" type="hidden" value="0">
    <input id="data_json_basura_email" type="hidden" value="0">
    <input id="data_json_telefonos_basura" type="hidden" value="0">
    <!-- Input que almacena el titulo Dinamico del formulario -->
    <input id="h_titulo_modal_formulario" type="hidden" value="">
    <input id="formApp" type="hidden">
    <input id="url_img_destacada" type="hidden" value="">
    <input id="url_video_destacado" type="hidden" value="">
    
    
    <script type='text/javascript' src='<?php echo get_template_directory_uri()?>/assets/frontend/js/vendor/min/jquery.lazy.min.js'></script>
    <script type='text/javascript'defer src='<?php echo get_template_directory_uri()?>/assets/<?php echo $carpeta; ?>/js/vendor/min/tether.min.js'></script>
    <script type='text/javascript'defer src='<?php echo get_template_directory_uri()?>/assets/<?php echo $carpeta; ?>/js/vendor/min/bootstrap.min.js'></script>
    <script type='text/javascript'defer src='<?php echo get_template_directory_uri()?>/assets/<?php echo $carpeta; ?>/js/vendor/min/mdb.min.js'></script>
    <script type='text/javascript' src='<?php echo get_template_directory_uri()?>/assets/<?php echo $carpeta; ?>/js/vendor/min/typeahead.min.js'></script>

    <script type='text/javascript' src='<?php echo get_template_directory_uri()?>/assets/<?php echo $carpeta; ?>/js/<?php echo $angular; ?>/app/formApp.min.js'></script>

    <!-- <script type='text/javascript' src='<?php echo get_template_directory_uri()?>/assets/<?php echo $carpeta; ?>/js/<?php echo $angular; ?>/inline.bundle.js'></script>
    <script type='text/javascript' src='<?php echo get_template_directory_uri()?>/assets/<?php echo $carpeta; ?>/js/<?php echo $angular; ?>/polyfills.bundle.js'></script>
    <script type='text/javascript' src='<?php echo get_template_directory_uri()?>/assets/<?php echo $carpeta; ?>/js/<?php echo $angular; ?>/main.bundle.js'></script> -->
    
    <script type='text/javascript' src='<?php echo $jsMenu; ?>'></script>


</html>
<script type="text/javascript" language="javascript" src="<?php echo get_template_directory_uri(); ?>/colegiaturas/js/colegiaturas.js"></script>