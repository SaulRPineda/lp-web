<?php
/**
 * WP Post Template: mob_9_descripcion_botones_principales
 */
?>

<?php    
    global $woocommerce, $product, $post;

    /*Se insertan los plugins que necesita la seccion tal y como se llama el archivo js*/
    load_script( $pluginsNecesarios = array('mapaOperations', 'sec_specialList') );
    /*End Se insertan los plugins que necesita la seccion tal y como se llama el archivo js*/
    $mes_inicio = NULL;
    $target = NULL;

    /*Primera Version se concatena dependiendo lo que este en el custom field*/

    /*$fecha_ini = get_field('fecha_inicio', $theID);
    foreach($fecha_ini as $key => $meses) {
        $mes_inicio .= $meses;

        switch ($key) {
            case count($fecha_ini)-2:
                $mes_inicio .= " y ";
            break;
            
            default:
                $mes_inicio .= ", ";
            break;
        }
    }*/

    /*$mes_inicio = substr($mes_inicio, 0, strlen($mes_inicio) - 2);   */

    /*Segunda Version se concatena dependiendo lo que este en el custom field*/


    /*Primera Version se concatena dependiendo lo que este en el custom field*/
    /*$fecha_Divisor = explode(",", $fecha_ini);
    $dia_inicio = $fecha_Divisor[0];
    $mes_inicio = $fecha_Divisor[1];*/

    //obtenemos el id del producto
    /*$parentid = get_queried_object_id();*/
    /*Primera Version se concatena dependiendo lo que este en el custom field*/
?>

<?php //echo "Status: ".$product->is_in_stock();
//print_r($product);

$contenidoProducto = get_field('descripcion_del_producto', $theID);

    if( !empty( $contenidoProducto ) || $contenidoProducto != NULL || $contenidoProducto != "" ) {

        //Getting product attributes
        //$product_attributes = $product->get_attributes();
        //var_dump($product_attributes);

        //Getting product attributes
        $atributos = get_post_meta($product->id);
        $product_attributes = unserialize($atributos['_product_attributes'][0]);
        $variacion_producto = 1;


        foreach ($product_attributes as $key => $product_atributo) {
            $product_attribute_labels[] = wc_attribute_label($product_atributo["name"]);
            $product_attribute_slugs[] = $product_atributo["name"];
            $variacion_producto = 0;

            // switch ($product_atributo["is_variation"]) {
            //     case 0:
            //         $product_attribute_labels[] = wc_attribute_label($product_atributo["name"]);
            //         $product_attribute_slugs[] = $product_atributo["name"];

            //         $variacion_producto = 0;
            //     break;
                
            //     default:
            //     break;
            // }

        }

    foreach ($product_attribute_slugs as $key => $valordelatributo) {
        $valores = wp_get_post_terms( $product->id, $valordelatributo );
        foreach ($valores as $key => $value) {

            switch ($value->taxonomy) {
                case 'pa_inicio-de-ciclo':
                    $mes_inicio = $value->name;
                break;
                
                default:
                break;
            }
        }        
    }
?>

<style>
.botones a i{
    padding: 0 .8rem;
}
        
</style>
<div id="descripcion_botones_principales"></div>
<main class="col-12 pt-2">
    <!-- SECTION-->
    <section id="elegant-card">
        <!-- Live preview-->
        <div class="row">
            <div class="col-md-12">
<!-- ***************************-> Inicia tu código ... -->

                <?php 
                     //if( have_rows("carreras",$theID) ) {
                ?>
                <article class="prodDescription " data-wow-offset="50">
                    <?php echo $contenidoProducto; ?>
                </article>

                <?php 
                    //}
                ?>

                <hr>
                    
                <div class="row botones">
                    <div class="center" style="width:100%; max-width: 255px; margin:auto;">
                        <!-- Icono de Telefono -->  
                        <a href="tel:8007864832" data-gtm-tr="call" data-gtm-pos="seccion-contactanos" class="mx-auto" style="font-size: 2rem;"><?php echo do_shortcode( '[icono nombre="'.sanitize_title("llamar-card").'"][/icono]' ); ?></a>

                        <!-- Icono de sobre para contacto Formulario -->               
                        <a onclick="openModal('#modal_frm_app')" class="mx-auto" style="font-size: 2rem;" data-solicitud-location="Middle"><?php echo do_shortcode( '[icono nombre="'.sanitize_title("correo-card").'"][/icono]' ); ?></a>

                        <!-- Icono de chat -->  
                        <a href="javascript:$zopim.livechat.window.show()" data-gtm-tr="chat" data-gtm-pos="intencion" data-gtm-location="Descripci&oacute;n Botones Principales" data-gtm-etiqueta="con formulario" class="mx-auto" style="font-size: 2rem;"><?php echo do_shortcode( '[icono nombre="'.sanitize_title("sms").'"][/icono]' ); ?></a>

                        <!-- Icono de lapiz para contacto Formulario -->               
                        <a onclick="openModal('#modal_frm_app')" class="mx-auto" style="font-size: 2rem;" data-solicitud-location="Middle"><?php echo do_shortcode( '[icono nombre="'.sanitize_title("formularios").'"][/icono]' ); ?></a>
                    </div>                    
                </div>


                <ul class="list-group mt-1 mb-1">
                    <div class="row bt">                        
                        <div class="simple-list no-active">
                            <div class="list-item-container">
                                <!-- <div class="calendario"> -->
                                    <!-- <h1 class="dia"><?php echo $dia_inicio; ?></h1> -->
                                    <!-- <p class="mes"><?php echo strtoupper($mes_inicio); ?></p> -->
                                <!-- </div> -->
                                <div class="item-description inicio-clases">
                                    <h6><?php echo get_field('texto_fecha_de_inicio', $theID); ?></h6>
                                    <h4><b><?php echo $mes_inicio; ?></b></h4> <!-- Se imprimen los meses -->
                                    <!-- <h6><?php echo get_field('texto_fecha_de_inicio', $theID); ?></h6><br> -->
                                    <?php if(get_field('texto_link', $theID) != ""){?>
                                        <a href="<?php echo get_field('link', $theID); ?>" class="linkCalendario mr-3 mt-0" target="_blank"><?php echo do_shortcode( '[icono nombre="'.sanitize_title("calendario").'" clase="mr-2"][/icono]' ); ?> <?php echo get_field('texto_link', $theID); ?></a>
                                    <?php }?>
                                </div>
                            </div>
                        </div>
                    </div>
                </ul>
                
                <section class="botonesUnitec">
                    <?php 

                        $links_botones = get_field( 'call_to_actions_principales', $theID );                 

                        if( $links_botones > 0 ) {
                            $s=1;

                            for ($i=0; $i < 3; $i++) {
                                $texto_call = $links_botones['texto_call_to_action_'.$s];
                                $link_call = $links_botones['link_call_to_action_'.$s];
                                //$target = ( strpos( strtolower( $link_call ), "unitec.mx" ) ) ? "" : "target='_blank'";
                                if ( strpos( strtolower( $link_call ), "://" ) ) {

                                    if( strpos( strtolower( $link_call ), "pdf" ) ){
                                        $target = "target='_blank'";
                                    }

                                    else if ( strpos( strtolower( $link_call ), "unitec.mx" ) ) {
                                        $target = "";
                                    }

                                    else{
                                        $target = "target='_blank'";
                                    }
                                } else{
                                    $target = "";
                                }

                                switch ($s) {
                                    case 1: $clase = "btn-primary"; break;
                                    case 2: $clase = "back-naranja-unitec"; break;
                                    case 3: $clase = "back-azul-secundario"; break;                                    
                                    default: break;
                                }
                    ?>
                    <?php if (strpos($link_call, 'javascript') !== false) { ?>
                    <a onclick="<?php echo $link_call; ?>" class="btn <?php echo $clase; ?> w-100" <?php echo $target; ?> data-solicitud-location="Middle" ><?php echo $texto_call; ?></a>
                    <?php } else { ?>
                    <a href="<?php echo $link_call; ?>" class="btn <?php echo $clase; ?> w-100" <?php echo $target; ?>><?php echo $texto_call; ?></a>
                    <?php } ?>

                    <?php
                                $s++;
                            } 
                        }
                    ?>
                </section>

                <div class="card mt-1 mb-2 ">
                    <?php
                        if( !empty($product_attributes) && $variacion_producto != 1){ 
                    ?>
                    <h4 class="card-title">                            
                            <div class="titulo"><?php echo get_field('titulo_ficha_tecnica', $theID); ?></div>
                    </h4>
                    <div class=" specialList tblSimulador">
                    <?php    
                        //Getting product attributes slugs
                        //$product_attribute_slugs = array_keys($product_attributes);
                        $count_slug = 0;
                        
                        //var_dump($product_attribute_slugs);
                        foreach ($product_attribute_slugs as $key => $product_attribute_slug){
                            $count_slug++;
                    ?>

                            <div class="row">
                                <div class="col-6 backGray"><?php echo $product_attribute_labels[$key]; ?></div>

                       <?php ##  ===>  ===>  // Getting the product attribute values
                            //$attribute_values = get_terms($product_attribute_slug);
                            $attribute_values = wp_get_post_terms( $product->id, $product_attribute_slug );

                            $count_value = 0;
                        ?>
                                <div class="col-6">
                        <?php   foreach($attribute_values as $attribute_value){
                                    $count_value++;
                                    $attribute_name_value = $attribute_value->name; // name value
                                    $attribute_slug_value = $attribute_value->slug; // slug value
                                    $attribute_id_value = $attribute_value->term_id; // ID value

                                    // switch ($attribute_value->taxonomy) {
                                    //     case 'pa_inicio-de-ciclo':
                                    //         echo '<input type="hidden" value="'.$attribute_value->name.'" name="iniciodeclases"id="iniciodeclases">';
                                    //     break;
                                        
                                    //     default:
                                    //         echo '<input type="hidden" value="Agosto" name="iniciodeclases"id="iniciodeclases">';
                                    //     break;
                                    // }

                                    if ( filter_var($attribute_name_value, FILTER_VALIDATE_URL) ){ ?>
                                        <a data-gtm-tr="downloadFiles" data-gtm-pos="folleto-de-carrera" href="<?php echo $attribute_name_value; ?>" target="_blank">Cons&uacute;ltalo aqu&iacute;</a>
                                        <?php //echo get_field('texto_para_folleto', $theID); ?>
                        <?php       } else{
                                        echo $attribute_name_value;
                                        if($count_value != count($attribute_values)) echo ', ';
                                    }
                                } 
                        ?>
                                </div>
                            </div>
                      <?php 
                        }   
                    }              
                    ?>
                    </div>
                </div>

<!-- ***************************-> Termina tu código ... -->
            </div>
        </div><!-- /.Live preview-->
    </section><!-- SECTION-->
</main><!-- /.Main layout-->
<?php } ?>