

<?php
/**
 * WP Post Template: mob_21_footer
 */
    $urlSVG = get_template_directory_uri() . "/assets/frontend/img/footer/";

    /*Implementacion Custom field para Formulario*/
    $id_page = get_the_ID();
    $thumbID = get_post_thumbnail_id( $id_page );
    $urlImg = wp_get_attachment_url( $thumbID );
    $altImg = get_post_meta($thumbID, '_wp_attachment_image_alt', true);

    $contenido_multimedia = get_field('url' ,$theID);
    /*Definir una imagen en caso de no contar ni con Imagen Y Video Destacado*/
    /*End Implementacion Custom field para Formulario*/

    /*Se obtiene el SKU del producto asignado en el backoffice de wordpress (Grupo Carreras)*/
    global $woocommerce, $product, $post;
    $sku = get_post_meta($product->id); 
    $sku_id = $sku['_sku'][0];
    /*End Se obtiene el SKU del producto asignado en el backoffice de wordpress (Grupo Carreras)*/ 

    /*Se obtiene el tipo de post Type Page or Product*/
    $idPage = get_the_ID();
    $pageType = get_post_type( $idPage );
    /*End Se obtiene el tipo de post Type*/

    /*Se obtiene img Horizontal*/
    $img_horizontal = get_field('imagen_destacada_horizontal' ,$theID);
    $url_img_horizontal = $img_horizontal['url'];
    /*Se obtiene img Horizontal*/

    class footer extends Walker_Nav_Menu {

    /**
     * At the start of each element, output a <li> and <a> tag structure.
     * 
     * Note: Menu objects include url and title properties, so we will use those.
     */
    function start_el( &$output, $item, $depth = 0, $args = array(), $id = 0 ) {
        $mas = NULL;
        $clase = NULL;
        $elementoPadre = NULL;

        $output .= ( $depth > 0 ? str_repeat( "\t", $depth ) : '' ); // code indent

        // Passed classes.
        $classes = empty( $item->classes ) ? array() : (array) $item->classes;
        $class_names = esc_attr( implode( ' ', apply_filters( 'nav_menu_css_class', array_filter( $classes ), $item ) ) );

        if ( $item->menu_item_parent == 0 ) {

            if(empty($bandera)) {
                $output .= '</ul>
                    </div>
                </div>';
            }

            if( strrpos( $class_names, "menu-item-has-children") > -1 ){
                $clase = 'footerCollapse';
                $mas = '<span class="pull-right"><i class="icon-u icon-u-ver-mas"></i></span>';
                $elementoPadre = $item->title;
            } else{
                $clase = 'footerCollapse';
                $elementoPadre = '<a  href="'.$item->url.'">'.$item->title.'</a>'; 
            }

            $output .= '<table class="table fullBlue m-0">
                        <tr>
                            <td>                            
                               <h3 class="'.$clase.'">';
            $output .= $elementoPadre.$mas;

            $output .= '</h3>
                            </td>
                        </tr>
                    </table>';

            $output .= '<div class="collapse animated flipInX" aria-expanded="false">
                                <div class="container fix-container">
                                    <ul class="container fix-content">';
            $bandera = 0;
        }

        if( strrpos( $class_names, "menu-item-has-children") != -1 && $item->menu_item_parent != 0 ) {
            $output .= '<li class="py-1"><a  href="'.$item->url.'">'.$item->title.'</a></li>';
            $bandera = 1;
        }
    }

}

?>

<?php //if ( strpos( strtolower($contenido_multimedia), "wistia") !== -1 ) {
    /*Se insertan los plugins que necesita la seccion tal y como se llama el archivo*/
    //load_script( $pluginsNecesarios = array('wistiaAPI') );
    /*End Se insertan los plugins que necesita la seccion tal y como se llama el archivo*/
    //} 
?>

<style>
    .form-control::-moz-placeholder {  
        color: white;
    }
    .form-control:-ms-input-placeholder {
        color: white;
    }
    .form-control::-webkit-input-placeholder {
        color: white;
    }
    input[type=search]:focus.valid, input[type=text]:focus.valid{
        border-bottom: none !important;  
        box-shadow: none !important;
    }
</style>

<footer>
    <input type="hidden" name="h_url_multimedia_formulario" id="h_url_multimedia_formulario" value='<?php echo $contenido_multimedia; ?>'>
    <input type="hidden" name ="h_url_imagen_destacada" id="h_url_imagen_destacada" value='<?php echo $urlImg; ?>'>
    <input type="hidden" name ="h_horizontal_url_imagen_destacada" id="h_horizontal_url_imagen_destacada" value='<?php echo $url_img_horizontal; ?>'>
    <input type="hidden" name="h_id_producto" id="h_id_producto" value='<?php echo $sku_id; ?>'>
    <input id="h_prellenado_formulario_pagina" type="hidden" value="true">
    <input id="h_prellenado_formulario_tipo" type="hidden" value="<?php echo $pageType; ?>">
    <input id="data_json_basura" type="hidden" value="0">
    <input id="data_json_basura_email" type="hidden" value="0">
    <!-- Input que almacena el titulo Dinamico del formulario -->
    <input id="h_titulo_modal_formulario" type="hidden" value="">
    <input type="hidden" id="h_tab-contiene-video" name="h_tab-contiene-video" value="N">
    <input type="hidden" id="h_video-activo" name="h_video-activo">




   
    
    <?php
    $post_slug=$post->post_name;
    if($post_slug!="calcula-tu-beca" && $post_slug!="calcula-tu-beca-y-colegiatura"){?>

        <!-- <script type="text/javascript">
    window.$zopim||(function(d,s){var z=$zopim=function(c){z._.push(c)},$=z.s=
    d.createElement(s),e=d.getElementsByTagName(s)[0];z.set=function(o){z.set.
    _.push(o)};z._=[];z.set._=[];$.async=!0;$.setAttribute("charset","utf-8");
    $.src="https://v2.zopim.com/?gOpEGGCuGfdOWrwyPRDrLEu8bVq8NRno";z.t=+new Date;$.
    type="text/javascript";e.parentNode.insertBefore($,e)})(document,"script");
    </script> -->
    <!-- Script del Chat <?php echo $post->post_name?>   -->
    <!--Start of Zendesk Chat Script-->
    
    <!-- <script type="text/javascript">
    $zopim(function() {
        $zopim.livechat.button.show();
    });
    </script> -->
    <!--End of Zendesk Chat Script-->
    <!-- End Script del Chat    -->
    <!-- Formulario del footer -->
    <?php } ?>


    <form  class="fullBlue">
        <!-- <div class="pb-1">
            <h2>
                <span class="divisor">|</span>
                Recibe nuestro bolet&iacute;n
            </h2>
        </div>
        <div>
            <div class="col-12 p-0">
                <input type="text" class="form-control white-text" id="inputMailFooter" placeholder="&nbsp &nbsp &nbsp Ingresa tu correo">
            </div><br>
            <div class="col-12 p-0">
                <button type="buttton" class="botonSecundario">SUSCRÍBETE</button>
            </div>
        </div>
        
        <hr> -->

        <!-- Inicia información de contacto -->
        <div class="contacto-inscripcion">
            <h2 class="mb-1"><a href="#">Informes de nuevo ingreso</a></h2>
            <?php if(strpos($post->post_name,'calcula-tu-beca')===false) { ?>
            <h2 class="mb-1"><a data-gtm-tr="chat" data-gtm-pos="intencion" data-gtm-location="footer" data-gtm-etiqueta="con formulario" onclick="openChatLive()"><?php echo do_shortcode( '[icono nombre="'.sanitize_title("chat").'"][/icono]' ); ?> Chatea con nosotros</a></h2>
            <?php } ?>
            <h2 class="mb-1"><a href="tel:8007864832" data-gtm-tr="pushCall" data-gtm-accion="intención"><?php echo do_shortcode( '[icono nombre="'.sanitize_title("llamar-footer").'"][/icono]' ); ?> Llámanos</a></h2>
            <?php if(strpos($post->post_name,'calcula-tu-beca')===false) { ?>
            <h2 class="mb-1"><a onclick="openModal('#modal_frm_app')" data-solicitud-location="Footer"><?php echo do_shortcode( '[icono nombre="'.sanitize_title("formularios").'"][/icono]' ); ?> Solicita información</a></h2>
            <h2 class="mb-1"><a onclick="openModal('#modal-whatsapp')"  data-gtm-tr="evWhats" data-gtm-accion="intención" data-gtm-seccion="Footer" data-gtm-depto="" data-gtm-pos="intención whatsapp"><?php echo do_shortcode( '[icono nombre="'.sanitize_title("whatsapp").'"][/icono]' ); ?> Escríbenos</a></h2>
            <?php } ?>
            <!-- <h2><a target="_blank" href="mailto:info@my.unitec.edu.mx"><?php echo do_shortcode( '[icono nombre="'.sanitize_title("contacto-footer").'"][/icono]' ); ?> info@my.unitec.edu.mx</a></h4> -->
            <!-- <h2><a onclick="openModal('#modal_frm_app')"><?php echo do_shortcode( '[icono nombre="'.sanitize_title("formulario").'"][/icono]' ); ?> Solicita Información</a></h4> -->
            <h4 class="fullBlueRedes">
                <a target="_blank" href="//www.facebook.com/unitec" data-gtm-tr="SocNetJmp" data-gtm-value="Facebook"><?php echo do_shortcode( '[icono nombre="'.sanitize_title("facebook-footer").'"][/icono]' ); ?></a>
                <a target="_blank" href="//twitter.com/unitecmx" data-gtm-tr="SocNetJmp" data-gtm-value="Twitter"><?php echo do_shortcode( '[icono nombre="'.sanitize_title("twitter-footer").'"][/icono]' ); ?></a>
                <a target="_blank" href="//www.youtube.com/unitec" data-gtm-tr="SocNetJmp" data-gtm-value="Youtube"><?php echo do_shortcode( '[icono nombre="'.sanitize_title("youtube-footer").'"][/icono]' ); ?></a>
                <a target="_blank" href="//blogs.unitec.mx/" ><?php echo do_shortcode( '[icono nombre="'.sanitize_title("blog").'"][/icono]' ); ?></a>
            </h4>
        </div> 
        <!-- Finaliza información de contacto -->

        <hr>

        <div id="footerCascada p-0">

        <?php  wp_nav_menu(array('walker'  => new footer() //use our custom walker
                           ));
        ?>
            <!--< table class="table fullBlue m-0">
                <tr>
                    <td>
                        <h3 class="footerCollapse">Soporte<span class="pull-right"><i class="icon-u icon-u-ver-mas"></i></span></h3>
                    </td>
                </tr>
            </table>
            <div class="collapse animated flipInX" aria-expanded="false">
                <div class="container">
                    <ul class="container">
                        <li><a target="_blank" href="#">Directorio</a></li>
                        <li><a target="_blank" href="#">Sobre Unitec</a></li>
                        <li><a target="_blank" href="#">Calidad Educativa</a></li>
                        <li><a target="_blank" href="#">Mision y Modelo EducativO</a></li>
                        <li><a target="_blank" href="#">Reconocimientos</a></li>
                        <li><a target="_blank" href="#">Responsabilidad Social</a></li>
                        <li><a target="_blank" href="#">Trabaja con nosotros</a></li>
                    </ul>
                </div>
            </div>
            <table class="table fullBlue m-0">
                <tr>
                    <td>
                        <h3 class="footerCollapse">Asistencia<span class="pull-right"><i class="icon-u icon-u-ver-mas"></i></span></h3>
                    </td>
                </tr>
            </table>
            <div class="collapse animated flipInX" aria-expanded="false">
                <div class="container">
                    <ul class="container">
                        <li><a target="_blank" href="#">Directorio</a></li>
                        <li><a target="_blank" href="#">Sobre Unitec</a></li>
                        <li><a target="_blank" href="#">Calidad Educativa</a></li>
                        <li><a target="_blank" href="#">Mision y Modelo EducativO</a></li>
                        <li><a target="_blank" href="#">Reconocimientos</a></li>
                        <li><a target="_blank" href="#">Responsabilidad Social</a></li>
                        <li><a target="_blank" href="#">Trabaja con nosotros</a></li>
                    </ul>
                </div>
            </div> -->
    
            <!-- <table class="table fullBlue m-0">
                <tr>
                    <td>
                        <h3 class="footerCollapse">Comunidad<span class="pull-right"><i class="icon-u icon-u-ver-mas"></i></span></h3>
                    </td>
                </tr>
            </table>
            <div class="collapse animated flipInX" aria-expanded="false">
                <div class="container">
                    <ul class="container">
                        <li><a target="_blank" href="#">Alumnos</a></li>
                        <li><a target="_blank" href="#">Egresados</a></li>
                        <li><a target="_blank" href="#">Profesores</a></li>
                        <li><a target="_blank" href="#">Administrativos</a></li>
                    </ul>
                </div>
            </div>
            
            <table class="table fullBlue m-0">
                <tr>
                    <td>
                        <h3 class="footerCollapse">Conócenos<span class="pull-right"><i class="icon-u icon-u-ver-mas"></i></span></h3>
                    </td>
                </tr>
            </table>
            <div class="collapse animated flipInX" aria-expanded="false">
                <div class="container">
                    <ul class="container">
                        <li><a target="_blank" href="#">Directorio</a></li>
                        <li><a target="_blank" href="#">Sobre Unitec</a></li>
                        <li><a target="_blank" href="#">Calidad Educativa</a></li>
                        <li><a target="_blank" href="#">Misión y Modelo Educativo</a></li>
                        <li><a target="_blank" href="#">Reconocimientos</a></li>
                        <li><a target="_blank" href="#">Responsabilidad Social</a></li>
                        <li><a target="_blank" href="#">Trabaja con nosotros</a></li>
                    </ul>
                </div>
            </div> -->
            
        </div>

    </form><br>
  
    <!-- Elemento de cascada del footer -->

    <!-- Terminos y condiciones y demas info -->
    <div class="info text-center">
        <p class="m-auto">
            <a class="terminos" href="//www.unitec.mx/terminos-y-condiciones/">Términos y condiciones</a> | 
            <a class="terminos" href="//www.unitec.mx/politicas-de-privacidad/">Políticas de privacidad</a> <!-- | 
            <a href="//www.unitec.mx/pago-seguro/">Pago Seguro</a><br> -->
        </p>
        <p class="acuerdo">Acuerdo Secretarial 142 de la SEP de la fecha 24 de Octubre de 1988</p>
    </div>

    <!-- Imagenes de los sponsors -->
    <div>
        <div class="text-center vertical-centered-text">
            <a href="">
                <img class="sponsors lazy" data-src="<?php echo $urlSVG . "svg/esr.svg" ?>" alt="esr" />
            </a>
            <a href="">
                <img class="sponsors lazy" data-src="<?php echo $urlSVG . "svg/laureate.svg" ?>" alt="laureate" />
            </a>
            <a href="">
                <img class="sponsors lazy" data-src="<?php echo $urlSVG . "svg/amipci.svg" ?>" alt="amipci" />
            </a>
            <a href="">
                <img class="sponsors lazy" data-src="<?php echo $urlSVG . "svg/fimpes.svg" ?>" alt="fimpes" />
            </a>
            <a href="">
                <img class="sponsors lazy" data-src="<?php echo $urlSVG . "svg/geotrust.svg" ?>" alt="geotrust" />
            </a>
        </div>  
    </div>

    <!-- Pie de pagina del footer -->
    <div class="citeFullBlue">
        Universidad Tecnológica de México. Derechos Reservedos 2017 UNITEC<br>
        Direccion: Av. Parque Chapultepec No. 56, Piso 1, Col. El parque. Mun. Naucalpan de Juarez, EdoMéx., 53398
        Laureate Education Inc. 
    </div>
</footer>
