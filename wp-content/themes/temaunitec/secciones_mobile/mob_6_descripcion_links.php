<div id="listado">  </div>
<?php
/**
 * WP Post Template: mob_6_descripcion_links
 */
?>

<?php
    $respaldoEconomicoContent = get_field('respaldo_economico', $theID);
    $respaldoEconomicoBullets = get_field('contenido_respaldo_economico', $theID); 
?>

<main class="col-12 mt-0 wow fadeInLeft"><!-- /.Main <layout-->
    <section id="elegant-card"><!-- SECTION-->
        <div class="row"><!-- Live preview-->
            <div class="col-md-12"><!-- col-md-12 -->
<!-- ***************************-> Inicia tu código ... -->
                <!--Card-->
                <div class="card mb-1">
                    <!--Title-->                              
                    <h4 class="card-title"><?php echo $respaldoEconomicoContent->post_title; ?><!-- Respaldo económico --></h4>
                    <?php $thumbID = get_field('imagen_destacada_horizontal', $respaldoEconomicoContent->ID);
                        $urlImg = $thumbID['url'];
                        $altImg = $urlImg['alt'];
                    ?>
                    <!-- card image -->
                    <div class="view overlay hm-white-slight">
                        <img alt="<?php echo $altImg; ?>" class="img-fluid lazy" data-src="<?php echo $urlImg; ?>">
                        <!-- <amp-img 
                            layout="responsive"
                            width="768" 
                            height="420" 
                            src="<?php echo $urlImg; ?>" 
                            alt="<?php echo $altImg; ?>" />
                            <noscript><img src="<?php echo $urlImg; ?>" alt="<?php echo $altImg; ?>"></noscript>
                        </amp-img> -->
                        <a><!-- <div class="mask waves-effect waves-light"></div> --></a>
                    </div>
                    <!--Button-->
                    <a href="<?php echo get_permalink($respaldoEconomicoContent->ID); ?>" class="btn-floating btn-action">
                        <?php echo do_shortcode( '[icono nombre="'.sanitize_title("ver-mas").'"][/icono]' ); ?>
                    </a>
                    <!-- card image -->
                    
                    <!-- Aqui van los componentes list_icon, list, texto, cascade, footer, image, collapse -->
                    <div class="card-block">
                        <div>
                            <p class="card-text roboto-light pt-1">
                                <?php echo get_field('contenido_personalizado', $theID); ?>
                            </p>
                        </div>
                        <!--/.Accordion wrapper-->
                        <div class="row">
                            <?php foreach ($respaldoEconomicoBullets as $key => $bullet) { ?>
                                    <div class="simple-list">
                                        <div type="button" class="list-item-container border-top-fix">
                                            
                                            <?php 
                                                $target = ( strpos( strtolower($bullet['link']), "unitec.mx" ) ) ? "" : "target='_blank'";
                                            ?>

                                            <div class="item-icon">
                                                <a href="<?php echo $bullet['link']; ?>" <?php echo $target; ?> class="link-simple-list" data-gtm-tr="EcSup" data-gtm-economic="<?php echo $bullet['titulo']; ?>"><?php echo do_shortcode( '[icono nombre="'.sanitize_title($bullet['icono']).'"][/icono]' ); ?></a>                                              
                                            </div>                                            

                                            <div class="item-description">
                                                <a href="<?php echo $bullet['link']; ?>" <?php echo $target; ?> data-gtm-tr="EcSup" data-gtm-economic="<?php echo $bullet['titulo']; ?>"><?php echo $bullet['titulo']; ?></a>
                                            </div>
                                        </div>
                                    </div>                                           
                            <?php } ?>
                        </div>
                    </div>
                </div>
                <!--/.Card-->
<!-- ***************************-> Termina tu código ... -->
            </div><!-- col-md-12 -->
        </div><!-- /.Live preview-->
    </section><!-- SECTION-->
</main><!-- /.Main <layout-->