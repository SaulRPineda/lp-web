<?php
/**
 * WP Post Template: mob_2_14_banner_tour_virtual
 */
?>
<?php
$mob_idPost = get_the_ID(); 

$grupoBanner = get_field('group_banner', $theID);
$grupoBannerContenido = get_field('desk_group_banner_contenido', $theID);

$mob_TituloVirtual = $grupoBannerContenido['desk_titulo_banner'];
$mob_descripcionBannerVirtual = $grupoBannerContenido['descripcion_corta_banner'];

$selectorTipoImg = $grupoBanner['selector_tipo_imagen_video'];

$imgBannerMob = $grupoBanner['banner_fondo_estatico_mob'];
    $urlBannerMob = $imgBannerMob["url"];
    $altBannerMob = $imgBannerMob["alt"];
$imgVideoMob = $grupoBanner['banner_fondo_video_mob'];

$horarios = array(
    'CUITLAHUAC'=> ["name"=>"Cuitláhuac, CDMX","horarios"=>[
            1=>["inicio"=>9,"fin"=>20],//lunes
            2=>["inicio"=>9,"fin"=>20],
            3=>["inicio"=>9,"fin"=>20],
            4=>["inicio"=>9,"fin"=>20],
            5=>["inicio"=>9,"fin"=>20],
            6=>["inicio"=>9,"fin"=>16],
            0=>["inicio"=>10,"fin"=>14]//domingo
        ]],
    'LOS REYES'=> ["name"=>"Los Reyes, CDMX","horarios"=>[
            1=>["inicio"=>9,"fin"=>20],//lunes
            2=>["inicio"=>9,"fin"=>20],
            3=>["inicio"=>9,"fin"=>20],
            4=>["inicio"=>9,"fin"=>20],
            5=>["inicio"=>9,"fin"=>20],
            6=>["inicio"=>9,"fin"=>16],
            0=>["inicio"=>10,"fin"=>14]//domingo
        ]],
    'MARINA'=> ["name"=>"Marina, CDMX","horarios"=>[
            1=>["inicio"=>9,"fin"=>20],//lunes
            2=>["inicio"=>9,"fin"=>20],
            3=>["inicio"=>9,"fin"=>20],
            4=>["inicio"=>9,"fin"=>20],
            5=>["inicio"=>9,"fin"=>20],
            6=>["inicio"=>9,"fin"=>16],
            0=>["inicio"=>10,"fin"=>14]//domingo
            ]],
    'SUR'=> ["name"=>"Sur, CDMX","horarios"=>[
            1=>["inicio"=>9,"fin"=>20],//lunes
            2=>["inicio"=>9,"fin"=>20],
            3=>["inicio"=>9,"fin"=>20],
            4=>["inicio"=>9,"fin"=>20],
            5=>["inicio"=>9,"fin"=>20],
            6=>["inicio"=>9,"fin"=>16],
            0=>["inicio"=>10,"fin"=>14]//domingo
        ]],
    'ATIZAPAN'=> ["name"=>"Atizapán, Edo. Méx.","horarios"=>[
            1=>["inicio"=>9,"fin"=>20],//lunes
            2=>["inicio"=>9,"fin"=>20],
            3=>["inicio"=>9,"fin"=>20],
            4=>["inicio"=>9,"fin"=>20],
            5=>["inicio"=>9,"fin"=>20],
            6=>["inicio"=>9,"fin"=>16],
            0=>["inicio"=>10,"fin"=>14]//domingo
        ]],
    'ECATEPEC'=> ["name"=>"Ecatepec, Edo. Méx","horarios"=>[
            1=>["inicio"=>9,"fin"=>20],//lunes
            2=>["inicio"=>9,"fin"=>20],
            3=>["inicio"=>9,"fin"=>20],
            4=>["inicio"=>9,"fin"=>20],
            5=>["inicio"=>9,"fin"=>20],
            6=>["inicio"=>9,"fin"=>16],
            0=>["inicio"=>10,"fin"=>14]//domingo
        ]],
    'TOLUCA'=> ["name"=>"Toluca, Edo. Méx","horarios"=>[
            1=>["inicio"=>9,"fin"=>20],//lunes
            2=>["inicio"=>9,"fin"=>20],
            3=>["inicio"=>9,"fin"=>20],
            4=>["inicio"=>9,"fin"=>20],
            5=>["inicio"=>9,"fin"=>20],
            6=>["inicio"=>9,"fin"=>16],
            0=>["inicio"=>10,"fin"=>14]//domingo
        ]],
    'GUADALAJARA'=> ["name"=>"Guadalajara","horarios"=>[
            1=>["inicio"=>9,"fin"=>20],//lunes
            2=>["inicio"=>9,"fin"=>20],
            3=>["inicio"=>9,"fin"=>20],
            4=>["inicio"=>9,"fin"=>20],
            5=>["inicio"=>9,"fin"=>20],
            6=>["inicio"=>9,"fin"=>16],
            0=>["inicio"=>10,"fin"=>14]//domingo
        ]],
    'LEON'=> ["name"=>"León, GTO","horarios"=>[
            1=>["inicio"=>9,"fin"=>20],//lunes
            2=>["inicio"=>9,"fin"=>20],
            3=>["inicio"=>9,"fin"=>20],
            4=>["inicio"=>9,"fin"=>20],
            5=>["inicio"=>9,"fin"=>20],
            6=>["inicio"=>9,"fin"=>16],
            0=>["inicio"=>10,"fin"=>14]//domingo
        ]],
    'QUERETARO'=> ["name"=>"Querétaro, QRO","horarios"=>[
            1=>["inicio"=>9,"fin"=>20],//lunes
            2=>["inicio"=>9,"fin"=>20],
            3=>["inicio"=>9,"fin"=>20],
            4=>["inicio"=>9,"fin"=>20],
            5=>["inicio"=>9,"fin"=>20],
            6=>["inicio"=>9,"fin"=>16],
            0=>["inicio"=>10,"fin"=>14]//domingo
        ]]
);

/*Definición de domingos Permitidos By SRP 29-05-2019*/
$permitidos = array(
    'ATIZAPAN'=> [  "2019-06-30",
                    "2019-07-14", 
                    "2019-07-21", 
                    "2019-07-28", 
                    "2019-08-04", 
                    "2019-08-11", 
                    "2019-08-18", 
                    "2019-08-25", 
                    "2019-09-01", 
                    "2019-09-08", 
                    "2019-09-15", 
                    "2019-09-22", 
                    "2019-09-29"],//domingo

    'CUITLAHUAC'=> ["2019-06-30",
                    "2019-07-14", 
                    "2019-07-21", 
                    "2019-07-28", 
                    "2019-08-04", 
                    "2019-08-11", 
                    "2019-08-18", 
                    "2019-08-25", 
                    "2019-09-01", 
                    "2019-09-08", 
                    "2019-09-15", 
                    "2019-09-22", 
                    "2019-09-29"],//domingo

    'ECATEPEC'=> [  "2019-06-30",
                    "2019-07-14", 
                    "2019-07-21", 
                    "2019-07-28", 
                    "2019-08-04", 
                    "2019-08-11", 
                    "2019-08-18", 
                    "2019-08-25", 
                    "2019-09-01", 
                    "2019-09-08", 
                    "2019-09-15", 
                    "2019-09-22", 
                    "2019-09-29"],//domingo

    'GUADALAJARA'=> ["2019-06-30", 
                    "2019-07-14", 
                    "2019-07-21", 
                    "2019-07-28", 
                    "2019-08-04", 
                    "2019-08-11", 
                    "2019-08-18", 
                    "2019-08-25", 
                    "2019-09-01", 
                    "2019-09-08",  
                    "2019-09-15", 
                    "2019-09-22", 
                    "2019-09-29"],//domingo

    'LEON'=> [      "2019-06-30", 
                    "2019-07-28", 
                    "2019-08-18", 
                    "2019-08-25", 
                    "2019-09-01", 
                    "2019-09-08",  
                    "2019-09-15"],//domingo

    'LOS REYES'=> [ "2019-06-30",
                    "2019-07-14", 
                    "2019-07-21", 
                    "2019-07-28", 
                    "2019-08-04", 
                    "2019-08-11", 
                    "2019-08-18", 
                    "2019-08-25", 
                    "2019-09-01", 
                    "2019-09-08", 
                    "2019-09-15", 
                    "2019-09-22", 
                    "2019-09-29"],//domingo

    'MARINA'=> [    "2019-06-30",
                    "2019-07-14", 
                    "2019-07-21", 
                    "2019-07-28", 
                    "2019-08-04", 
                    "2019-08-11", 
                    "2019-08-18", 
                    "2019-08-25", 
                    "2019-09-01", 
                    "2019-09-08", 
                    "2019-09-15", 
                    "2019-09-22", 
                    "2019-09-29"],//domingo

    'QUERETARO'=> [ "2019-07-14", 
                    "2019-07-21", 
                    "2019-07-28", 
                    "2019-08-04", 
                    "2019-08-11", 
                    "2019-08-18", 
                    "2019-08-25", 
                    "2019-09-01", 
                    "2019-09-08",  
                    "2019-09-15", 
                    "2019-09-22", 
                    "2019-09-29"],//domingo

    'SUR'=> [       "2019-06-30",
                    "2019-07-14", 
                    "2019-07-21", 
                    "2019-07-28", 
                    "2019-08-04", 
                    "2019-08-11", 
                    "2019-08-18", 
                    "2019-08-25", 
                    "2019-09-01", 
                    "2019-09-08", 
                    "2019-09-15", 
                    "2019-09-22", 
                    "2019-09-29"],//domingo

    'TOLUCA'=> ["2019-06-02", 
                "2019-06-09", 
                "2019-07-14", 
                "2019-07-21", 
                "2019-07-28", 
                "2019-08-04", 
                "2019-08-11", 
                "2019-08-18", 
                "2019-08-25", 
                "2019-09-01", 
                "2019-09-08",  
                "2019-09-15",  
                "2019-09-22", 
                "2019-09-29"]//domingo
);
/*End Definición de domingos Permitidos By SRP 29-05-2019*/
?>

<script type="text/javascript">
    function checkCookie(valor) {
        var user = getCookie(valor);
        if (user != "") {
            return true;
        }
        else {
            return false;
        }
    }
    function getCookie(cname) {
        var name = cname + "=";
        var ca = document.cookie.split(';');
        for (var i = 0; i < ca.length; i++) {
            var c = ca[i];
            while (c.charAt(0) == ' ') c = c.substring(1);
            if (c.indexOf(name) == 0) return c.substring(name.length, c.length);
        }
        return "";
    }
    function campusDynamics(campus) {
        switch (campus) {
            case "ATZ":
                return "ATIZAPAN";
                break;
            case "REY":
                return "LOS REYES";
                break;
            case "MAR":
                return "MARINA";
                break;
            case "ECA":
                return "ECATEPEC";
                break;
            case "SUR":
                return "SUR";
                break;
            case "CUI":
                return "CUITLAHUAC";
                break;
            case "LEO":
                return "LEON";
                break;
            case "TOL":
                return "TOLUCA";
                break;
            case "GDL":
                return "GUADALAJARA";
                break;
            case "ONL":
                return "EN LINEA";
                break
            case "QRO":
                return "QUERETARO";
                break
            default:
                return "MARINA";
        }
    }
    const params = new URLSearchParams(window.location.search);  
    var validaEmail = params.get("email");
    if(validaEmail !== null){
        email = params.get("email");

        var validHS = false;
        var nombre;
        var email;
        var lastname;
        var campus;
        var campusHbpst;        
        var noPersona;
        var subnivel;    
        var publicContactURL;

        var cookieValidation = false;

        $.ajax({
            type: "GET",
            url: "wp-content/themes/temaunitec/assets/html/AgendarCita/verificaCorreoHB.php",
            data: {
                email: email        
            },
            dataType: 'json',
            success: function (res) {           
                // res = JSON.parse(res);
                if (res['existeHS']) {

                    validHS = res['existeHS'];
                    // contactProps = json_decode(HSContact,true);
                    email = res.properties['email']['value'];
                    nombre = res.properties['firstname']['value']
                    lastname = res.properties['lastname']['value'];
                    campus = res.properties['campus']['value'];
                    campusHbpst = res.properties['campus']['value'];                
                    noPersona = res.properties['nopersona']['value'];
                    subnivel = res.properties['subnivelinteres']['value'];          
                    publicContactURL = res['profile-url'];
    
                    jQuery('#agendar-cita').attr({'data-fn': nombre, 'data-ln': lastname, 'data-np': noPersona, 'data-url': publicContactURL, 'data-cmp': campusHbpst,});
                    jQuery('#co-email').val(email);
                    jQuery('#co-email').attr('disabled', true);
    
                }else{
                    // alert('no existe correo');      
                }          
            },
            error: function (xhr, textStatus, error) {
                console.log(xhr);
                console.log(xhr.statusText);
                console.log(textStatus);
                console.log(error);
            },
            beforeSend: function () {          
            },
            complete: function () {
                
            }
        })
    }else if(checkCookie('c_form_data')) {
        var objCookie = JSON.parse(decodeURIComponent(getCookie('c_form_data')));
        var objLocalstorage = JSON.parse(localStorage.getItem("jsonUsr"));
        console.log(objCookie);
        if(typeof objCookie.email !== 'undefined'){
            email = objCookie.email;
            nombre = objCookie.nombre;
            lastname = objCookie.apaterno;
            campus = campusDynamics(objLocalstorage.campus);
            subnivel = objCookie.subNivelInteres;
            
            cookieValidation = true;
        }
    }
    else{
    }
    var jsonhorarios = <?php echo json_encode($horarios);?>;
    var jsonpermitidos = <?php echo json_encode($permitidos);?>;
</script>
<div id="cita-campus"></div>
<section class="banner-formulario-expuesto d-flex align-items-start">
    <!-- Grid row -->
    <div class="row w-100 m-0">
        <!-- Grid column -->
        <div class="col-md-9 col-lg-7 col-xl-5 p-0">
            <!--Form without header-->
            <?php
                switch ($selectorTipoImg) {
                    case 'fondo_fijo':
                        $background = "<div style='background-image:url(".$urlBannerMob.");' alt='".$altBannerMobile."' class='img-header-formulario-expuesto' id='img-frm-cita'></div>";
                    break;
            
                    case 'fondo_video':
                        $background = "<video autoplay muted loop webkit-playsinline playsinline class='video-tour'><source src='".$imgVideoMob."' type='video/mp4'></video>";
                        // $background = "<video autoplay muted loop webkit-playsinline playsinline class='video-tour' style='background: transparent url(".$imgVideoMob.") no-repeat;'><source src='".$imgVideoMob."' type='video/mp4'></video>";
                    break;
                    
                    default:
                        $background = "<div style='background-image:url(".$urlBannerMob.");' alt='".$altBannerMobile."' class='img-header-formulario-expuesto' id='img-frm-cita'></div>";
                    break;
                }
            ?>
            <?php echo $background ?>
            <div class="card p-0">
                 <div class="card-body mx-3" id="paso1-frm-cita">
                    <div class="row mt-1">
                        <div class="col-12 titulo-formulario-expuesto">
                            <h2 class="m-0 text-center"><?php echo $mob_TituloVirtual; ?></h2>
                        </div>
                    </div>
                    <div class="row mt-1">
                        <div class="col-12 sub-titulo-formulario-expuesto">
                            <h6 class="m-0 text-center">Visita nuestras instalaciones y vive el ambiente universitario.</h6>
                        </div>
                    </div>
                    <div class="md-form my-3">
                        <div class="row justify-content-center mt-1 correo">
                            <div class="col-1 d-flex align-items-center justify-content-center icon-style">
                                <span class="icon-u-correo"></span>
                            </div>
                            <div class="col-10 md-select-form">
                                <label for="email" class="pl-3 m-0">Correo: <span>*</span></label>
                                <input id="co-email" name="email" class="form-control m-0 p-1" type="email" autocomplete="off" maxlength="100" minlength="1" ref="input">
                                <div class="invalid-feedback">Por favor, ingresa un correo valido</div>
                            </div>
                        </div>
                        <div class="row justify-content-center mt-2 campus">
                            <div class="col-1 d-flex align-items-center justify-content-center icon-style">
                                <span class="icon-u-campus"></span>
                            </div>                          
                            <div class="col-10 md-select-form">
                                <label for="campus" class="pl-3 m-0 active">Campus: <span>*</span></label>
                                <div class="dropdown inp-campus">
                                    <button id="input-campus" type="button" class="btn-select dropdown-toggle d-flex align-items-center justify-content-between px-1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" ref="dropdown">
                                        Selecciona un campus
                                    </button>
                                    <div id="co-campus" class="dropdown-menu p-1 m-0 w-100 lineButtonCampus">
                                    <?php
                                      foreach ($horarios as $k => $c) {                                        
                                        $campus = $c['name'];
                                    ?>                                        
                                        <a data-c="<?php echo $k?>" class="dropdown-item align-items-center campus p-2">
                                            <?php echo $c['name'];?>
                                        </a>
                                    <?php                                        
                                      }
                                    ?>                                        
                                    </div>
                                </div>
                                <div id="validacion-campus" class="invalid-feedback">Por favor, ingresa un correo valido</div>
                            </div>
                        </div>
                        <div class="row justify-content-center mt-2 dia">
                            <div id="iconCita" class="col-1 d-flex align-items-center justify-content-center icon-style icon-disabled">
                                <span class="icon-u-calendario"></span>
                            </div>                          
                            <div class="col-10 md-select-form">
                                <label for="dia" class="pl-3 m-0 active">Fecha: <span>*</span></label>
                                <div class="dropdown inp-fecha">
                                    <button id="input-fecha" type="button" class="btn-select dropdown-toggle d-flex align-items-center justify-content-between px-1 disabled" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" ref="dropdown">
                                        Selecciona un día
                                    </button>
                                    <div id="co-fecha" class="dropdown-menu p-1 m-0 w-100 lineButtonFecha">
                                    
                                    </div>
                                    <input type="hidden" name="valorDomingo" id="valorDomingo" value="">
                                </div>
                                <div id="validacion-fecha" class="invalid-feedback">Por favor, ingresa un correo valido</div>
                            </div>
                        </div>
                        <div id="row-hora" class="row justify-content-center mt-1 hora d-none">
                            <div class="col-1 d-flex align-items-center justify-content-center icon-style">
                                <span class="icon-u-horarios"></span>
                            </div>                          
                            <div class="col-10 md-select-form d-flex align-items-center">
                                <p class="m-0 mt-1">Hora<span>*</span></p>                                
                            </div>
                            <div class="col-11 mt-1 px-2">
                                <section class="inp-hora" id="tabNav">
                                    <i class="icon-u icon-u-tab-izquierda leftTabNavigation direccionales dirLeft" style="display: none;"></i>
                                    <div class="tabs-wrapper" id="elementoConScroll">
                                        <ul id="co-hora" class="nav classic-tabs  alturaFila" role="tablist">
                                        </ul>
                                    </div>
                                    <i class="icon-u icon-u-tab-derecha rightTabNavigation direccionales dirRight" style="display: flex;"></i>
                                </section>
                                <div class="invalid-feedback">Por favor, ingresa un correo valido</div>
                            </div>
                        </div>
                    </div>
                    <div class="frm-politicas my-2 px-2">
                        <label class="politicas light text-center">
                            Al hacer clic en <span class="span-continuar">"AGENDAR CITA"</span>, reconoces haber leído las <a href="//www.unitec.mx/politicas-de-privacidad/" target="_blank">Políticas de Privacidad</a> y confirmas estar de acuerdo con el uso de ellas, así como los <a href="//www.unitec.mx/terminos-y-condiciones/" target="_blank">Términos y Condiciones</a> del sitio.
                        </label>
                    </div>
                    <div class="row mb-2">
                        <div class="w-100 px-4">
                            <button <?php if(isset($_GET['fuente_cita']) && trim($_GET['fuente_cita'])=='sitio_web'){echo 'data-fuente="'.trim($_GET['fuente_cita']).'"';}?> class="btn btn-frm-expuesto w-100 m-0" id="agendar-cita">AGENDAR CITA</button>
                        </div>
                    </div>
                </div>
                <div id="proceso-absorcion"></div>    
                <div id="msj-guardado" class="card-thankYouCita h-100 w-100 align-items-center" style="display: none"></div>
            </div>
            <!--/Form without header-->
        </div>
        <!-- Grid column -->
    </div>
    <!-- Grid row -->
</section>