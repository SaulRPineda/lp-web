

<?php
/**
 * WP Post Template: mob_32_footer_landing
 */
    $urlSVG = get_template_directory_uri() . "/assets/frontend/img/footer/";

    /*Implementacion Custom field para Formulario*/
    $id_page = get_the_ID();
    $thumbID = get_post_thumbnail_id( $id_page );
    $urlImg = wp_get_attachment_url( $thumbID );
    $altImg = get_post_meta($thumbID, '_wp_attachment_image_alt', true);

    $contenido_multimedia = get_field('url' ,$theID);
    /*Definir una imagen en caso de no contar ni con Imagen Y Video Destacado*/
    /*End Implementacion Custom field para Formulario*/

    /*Se obtiene el SKU del producto asignado en el backoffice de wordpress (Grupo Carreras)*/
    global $woocommerce, $product, $post;
    $sku = get_post_meta($product->id); 
    $sku_id = $sku['_sku'][0];
    /*End Se obtiene el SKU del producto asignado en el backoffice de wordpress (Grupo Carreras)*/ 

    /*Se obtiene el tipo de post Type Page or Product*/
    $idPage = get_the_ID();
    $pageType = get_post_type( $idPage );
    /*End Se obtiene el tipo de post Type*/

    /*Se obtiene img Horizontal*/
    $img_horizontal = get_field('imagen_destacada_horizontal' ,$theID);
    $url_img_horizontal = $img_horizontal['url'];
    /*Se obtiene img Horizontal*/

    class footer extends Walker {

    /**
     * At the start of each element, output a <li> and <a> tag structure.
     * 
     * Note: Menu objects include url and title properties, so we will use those.
     */
    function start_el( &$output, $item, $depth = 0, $args = array(), $id = 0 ) {
        $mas = NULL;
        $clase = NULL;
        $elementoPadre = NULL;

        $output .= ( $depth > 0 ? str_repeat( "\t", $depth ) : '' ); // code indent

        // Passed classes.
        $classes = empty( $item->classes ) ? array() : (array) $item->classes;
        $class_names = esc_attr( implode( ' ', apply_filters( 'nav_menu_css_class', array_filter( $classes ), $item ) ) );

        if ( $item->menu_item_parent == 0 ) {

            if(empty($bandera)) {
                $output .= '</ul>
                    </div>
                </div>';
            }

            if( strrpos( $class_names, "menu-item-has-children") > -1 ){
                $clase = 'footerCollapse';
                $mas = '<span class="pull-right"><i class="icon-u icon-u-ver-mas"></i></span>';
                $elementoPadre = $item->title;
            } else{
                $clase = 'footerCollapse';
                $elementoPadre = '<a  href="'.$item->url.'">'.$item->title.'</a>'; 
            }

            $output .= '<table class="table fullBlue m-0">
                        <tr>
                            <td>                            
                               <h3 class="'.$clase.'">';
            $output .= $elementoPadre.$mas;

            $output .= '</h3>
                            </td>
                        </tr>
                    </table>';

            $output .= '<div class="collapse animated flipInX" aria-expanded="false">
                                <div class="container fix-container">
                                    <ul class="container fix-content">';
            $bandera = 0;
        }

        if( strrpos( $class_names, "menu-item-has-children") != -1 && $item->menu_item_parent != 0 ) {
            $output .= '<li><a  href="'.$item->url.'">'.$item->title.'</a></li>';
            $bandera = 1;
        }
    }

}

?>


<style>
    .form-control::-moz-placeholder {  
        color: white;
    }
    .form-control:-ms-input-placeholder {
        color: white;
    }
    .form-control::-webkit-input-placeholder {
        color: white;
    }
    input[type=search]:focus.valid, input[type=text]:focus.valid{
        border-bottom: none !important;  
        box-shadow: none !important;
    }
</style>


<footer>
    <input type="hidden" name="h_url_multimedia_formulario" id="h_url_multimedia_formulario" value='<?php echo $contenido_multimedia; ?>'>
    <input type="hidden" name ="h_url_imagen_destacada" id="h_url_imagen_destacada" value='<?php echo $urlImg; ?>'>
    <input type="hidden" name ="h_horizontal_url_imagen_destacada" id="h_horizontal_url_imagen_destacada" value='<?php echo $url_img_horizontal; ?>'>
    <input type="hidden" name="h_id_producto" id="h_id_producto" value='<?php echo $sku_id; ?>'>
    <input id="h_prellenado_formulario_pagina" type="hidden" value="true">
    <input id="h_prellenado_formulario_tipo" type="hidden" value="<?php echo $pageType; ?>">
    <input id="data_json_basura" type="hidden" value="0">
    <input id="data_json_basura_email" type="hidden" value="0">
    <!-- Input que almacena el titulo Dinamico del formulario -->
    <input id="h_titulo_modal_formulario" type="hidden" value="">
    <input type="hidden" id="h_tab-contiene-video" name="h_tab-contiene-video" value="N">
    <input type="hidden" id="h_video-activo" name="h_video-activo">




   
    
    <?php
    $post_slug=$post->post_name;
    if($post_slug!="calcula-tu-beca" && $post_slug!="calcula-tu-beca-y-colegiatura"){?>

    <?php } ?>



    <section class="w-100 mb-1" style="height:4px; background-color:#006fba;"></section>
    <!-- Terminos y condiciones y demas info -->
    <div class="info text-center">
        <p class="m-auto">
            <a class="terminos" href="//www.unitec.mx/terminos-y-condiciones/">Términos y condiciones</a> | 
            <a class="terminos" href="//www.unitec.mx/politicas-de-privacidad/">Políticas de privacidad</a> <!-- | 
            <a href="//www.unitec.mx/pago-seguro/">Pago Seguro</a><br> -->
        </p>
        <p class="acuerdo">Acuerdo Secretarial 142 de la SEP de la fecha 24 de Octubre de 1988</p>
    </div>

    <!-- Imagenes de los sponsors -->
    <div>
        <div class="text-center vertical-centered-text">
            <a href="">
                <img class="sponsors lazy" data-src="<?php echo $urlSVG . "svg/esr.svg" ?>" alt="esr" />
            </a>
            <a href="">
                <img class="sponsors lazy" data-src="<?php echo $urlSVG . "svg/laureate.svg" ?>" alt="laureate" />
            </a>
            <a href="">
                <img class="sponsors lazy" data-src="<?php echo $urlSVG . "svg/amipci.svg" ?>" alt="amipci" />
            </a>
            <a href="">
                <img class="sponsors lazy" data-src="<?php echo $urlSVG . "svg/fimpes.svg" ?>" alt="fimpes" />
            </a>
            <a href="">
                <img class="sponsors lazy" data-src="<?php echo $urlSVG . "svg/geotrust.svg" ?>" alt="geotrust" />
            </a>
        </div>  
    </div>

    <!-- Pie de pagina del footer -->
    <div class="citeFullBlue">
        Universidad Tecnológica de México. Derechos Reservedos 2017 UNITEC<br>
        Direccion: Av. Parque Chapultepec No. 56, Piso 1, Col. El parque. Mun. Naucalpan de Juarez, EdoMéx., 53398
        Laureate Education Inc. 
    </div>
</footer>
