<div id="formulario-main-mobile" style="display:none; padding-top: 52px;">
    <div id="footer">
        <div class="container-full">
            <div class="row items-center justify-center info-footer">
                <div class="col col-4 ctrl btn-footer" data-ir="sec-cursar">Quiero cursar<br><br>
                    <span class="rs-carrera m">
                    </span>
                </div>
                <div class="col col-4 ctrl" >
                    <div>Promedio</div>
                    <div class="clearfix">
                        <div class="col col-4 prom-ctrl right-align ctrlbtn" data-valor="min">-</div>
                        <div class="col col-4 output m"></div>
                        <div class="col col-4 prom-ctrl left-align ctrlbtn" data-valor="mas">+</div>
                    </div>
                </div>
                <div class="col col-4 ctrl">
                    <div>Materias</div>
                    <div class="clearfix">
                        <div class="col col-4 mat-ctrl right-align ctrlbtn" data-valor="min">-</div>
                        <div class="col col-4 ctrlrs-materia m rs-materias display-none"></div>
                        <div class="col col-4 mat-ctrl  left-align ctrlbtn" data-valor="mas">+</div>
                    </div>
                </div>
            </div>
        </div>
    </div>
      <div id="fullpage">
         <div class="section fp-auto-height-responsive" data-anchor="calculadora" id="sec0">
            <div class="slide" data-anchor="sec-cursar" id="sec-cursar">
               <div class="row md-hide lg-hide mobcont">
                  <!-- PREPARATORIA -->
                  <div class="max-width-4 mx-auto mt3 xs-mt1 xs-mb1 linea-mobile">
                     <div class="clearfix pb2 relative center line-b">
                        <div class="col col-2 pl2 ">
                           <div data-pos="pr" data-val="PREPARATORIA" class="expandible cerrado clearfix m100 absolute" style="cursor: pointer;"></div>
                           <div class="imagen-lineas-cuadros cuadro PREPA">
                           </div>
                        </div>
                        <div class="col col-9 line">PREPARATORIA</div>
                     </div>
                  </div>
                  <!-- PREPARATORIA -->
                  <!-- licenciatura -->
                  <div class="max-width-4 mx-auto linea-mobile">
                     <div class="clearfix pb2 relative center line-b">
                        <div class="col col-2 pl2 ">
                           <div data-pos="li" data-val="LICENCIATURA" class="expandible cerrado clearfix m100 absolute" style="cursor: pointer;"></div>
                           <div class="imagen-lineas-cuadros cuadro UG">
                           </div>
                        </div>
                        <div class="col col-9 p2  line">LICENCIATURA</div>
                     </div>
                  </div>
                  <!-- licenciatura -->
                  <!-- ingenieria -->
                  <div class="max-width-4 mx-auto linea-mobile">
                     <div class="clearfix pb2 relative center line-b">
                        <div class="col col-2 pl2 ">
                           <div data-pos="in" data-val="INGENIERIA" class="expandible cerrado clearfix m100 absolute" style="cursor: pointer;"></div>
                           <div class="imagen-lineas-cuadros cuadro ING">
                           </div>
                        </div>
                        <div class="col col-9 line">INGENIERÍA</div>
                     </div>
                  </div>
                  <!-- ingenieria -->
                  <!-- SALUD -->
                  <div class="max-width-4 mx-auto linea-mobile">
                     <div class="clearfix pb2 relative center line-b">
                        <div class="col col-2 pl2 ">
                           <div data-pos="sa" data-val="SALUD" class="expandible cerrado clearfix m100 absolute" style="cursor: pointer;"></div>
                           <div class="imagen-lineas-cuadros cuadro CS">
                           </div>
                        </div>
                        <div class="col col-9 line">SALUD</div>
                     </div>
                  </div>
                  <!-- SALUD -->
                  <!-- DIPLOMADOS -->
                  <div class="max-width-4 mx-auto linea-mobile">
                     <div class="clearfix pb2 relative center line-b">
                        <div class="col col-2 pl2">
                           <div data-pos="po" data-val="DIPLOMADO" class="expandible cerrado clearfix m100 absolute" style="cursor: pointer;"></div>
                           <div class="imagen-lineas-cuadros cuadro DIP">
                           </div>
                        </div>
                        <div class="col col-9 line">DIPLOMADOS</div>
                     </div>
                  </div>
                  <!-- POSGRADO -->
                  <!-- POSGRADO -->
                  <div class="max-width-4 mx-auto linea-mobile">
                     <div class="clearfix pb2 relative center line-b">
                        <div class="col col-2 pl2">
                           <div data-pos="po" data-val="POSGRADO" class="expandible cerrado clearfix m100 absolute" style="cursor: pointer;"></div>
                           <div class="imagen-lineas-cuadros cuadro POS">
                           </div>
                        </div>
                        <div class="col col-9 line">POSGRADO</div>
                     </div>
                  </div>
                  <!-- POSGRADO -->
               </div>
               <!-- TERMINA seccion MOBILEE -->
               <!-- seccion desktop -->
            </div>
         </div>
          <div class="caja px3 pt3 lg-px4 xs-p1 m1">
            <div id="ciclosTituloMob" class="row row-menu-app-form hide">
               <div class="col-12 h2" style="text-align:center">¿Cuál es el periodo de tu interés?</div>  
            </div>
            <div id="secCiclos" class="quest w-80 hide">
               <div class="row row-ciclos adapt-desk-lineas col-12" style="justify-content: center;">
               </div>
               <div class="clearfix pb2">
                  <div class="col col-12 text-left ">
                     <button onclick="regresaContenedor()" class="iconb btn-regresar-ciclos">Regresar</button>
                  </div>
<!--                      <div class="col col-6  sm-col-right ">-->
<!--                          <button  onClick="javascript:irA('secCiclos','openThankYouCiclos')" class=" right button-primaryn">Avanzar</button>-->
<!--                      </div>-->
               </div>
            </div>
          </div>
      </div>
      <div class="plantillas" style="display:none">
         <div class="p_expandible">
            <div class="caja px3 pt3 lg-px4 xs-p1 m1">
               <div class="clearfix preg-est" >
                  <p class="h2 sub xs-left-align" id="subtituloInteres">¿Qué quieres estudiar?</p>
                  <div class="py2 xs-py1">
                     <input class="input sm-col sm-col-5 project" placeholder="Buscar..." id="project">
                     <input type="hidden" class="project-id" id="project-id">
                     <div id="projectSelected" hidden></div>
                  </div>
               </div>
               <div class="clearfix v_mod display-none">
                  <div class="col col-12 pr3 xs-pr0 ">
                     <p class="h2 sub xs-left-align ">¿Qué modalidad te interesa?</p>
                     <div class="py2 xs-py1">
                        <select class="input sm-col sm-col-5" id="render_modalidad"></select>
                     </div>
                     <div id="alert-prepaunam" class="display-none">
                        <div style='margin: 0vh 0 2vh; font-family: "Roboto Light",sans-serif; text-align: left; font-size: 1.8vh; font-weight: 400;' role="alert">
                          *Iniciamos Clases en Agosto de 2020
                        </div>
                    </div>
                  </div>
               </div>
               <div class="clearfix v_camp display-none">
                  <div class="sm-col sm-col-5 pr3 xs-pr0 ">
                     <p class="h2 sub xs-left-align ">¿En qué campus?</p>
                     <div class="py2 xs-py1">
                        <select class="input sm-col sm-col-5" id="render_campus"></select>
                     </div>
                  </div>
               </div>
               <div class="clearfix v_prepa display-none">
                  <div class="sm-col sm-col-5 pr3 xs-pr0 ">
                     <p class="h2 sub xs-left-align ">¿Eres padre o tutor?</p>
                     <div class="py2 xs-py1">
                        <select class="input sm-col sm-col-5" id="render_prepa"></select>
                     </div>
                  </div>
               </div>
               <div class="clearfix  ">
                   <p class="err_cursar"></p>
               </div>
               <div class="clearfix pb2">
                  <div class="col col-6 text-left ">
                     <button class="cerrar iconb">Regresar</button>
                  </div>
                  <div class="col col-6  sm-col-right ">
                     <button  onClick="javascript:irA('calculadora','openThankYou')" class=" right button-primaryn">Avanzar</button>
                  </div>
               </div>
            </div>
         </div>
      </div>

</div>