<div id="nuestro_campus"></div>
<?php
/**
 * WP Post Template: mob_7_nuestros_campus
 */
?>

<?php 
$j=0;
// check if the repeater field has rows of data
if( have_rows('nuestros_campus') ){
    // loop through the rows of data
    while ( have_rows('nuestros_campus') ) { the_row(); 
        // check if the repeater field has rows of data
        if( have_rows('links') ){
            // loop through the rows of data
            while ( have_rows('links') ) { the_row();
                // display a sub field value
                $collapsables_campus[$j] = array( get_sub_field('icono_como_llegar'), get_sub_field('titulo_nuestros_campus'), get_sub_field('link_nuestros_campus') );
                $j++;   
            }
        }
    }
}
?>

<main class="col-12 mt-0"><!-- /.Main <layout-->
    <section id="elegant-card"><!-- SECTION-->
        <div class="row"><!-- Live preview-->
            <div class="col-md-12"><!-- col-md-12 -->
<!-- ***************************-> Inicia tu código ... -->
                <!--Carousel Wrapper-->
                <div id="carouselNuestrosCampus" class="carousel slide carousel-fade" data-ride="carouselNuestrosCampus">

            <?php
            $active = NULL;
            if(have_rows("nuestros_campus",$theID) ) {   
                while( have_rows('nuestros_campus') ) { the_row();
                    //while( have_rows('campus') ) { the_row();
                        $campus = get_sub_field('campus');
                        $links = get_sub_field('links');             
                        
                        //$campusField = get_sub_field('campus');                                

                        $taxonomy = 'product_cat';
                        //$categoty_id = $campusField[0]->term_id;
                        $categoty_id = $campus[0]->term_id;

                        //term_Ids de todas las subcategorias de Campus
                        $campus= get_term_children($categoty_id, $taxonomy);
                        ?>

                        <!--Slides-->
                        <div class="carousel-inner" role="listbox">
                <?php
                        //Con los term_Ids obtenemos los Page Templates Asociados
                        foreach ($campus as $key => $value) {
                            if ($key == 0) { $active = "active"; }
                            if($value != NULL) {
                                $a = get_term_meta($value); 
                                //va por los terms asociados al post
                                $id_post=$a['dhvc_woo_category_page_id'][0];
                                $paginas = get_post($id_post);                     
                                ?>
                            <?php if($id_post != NULL){ ?>
                            <!-- slide -->
                            <div class="carousel-item <?php echo $active; ?>">
                                <!-- Card -->
                                <div class="card">
                                    <!--Title-->
                                    <h4 class="card-title">Nuestros Campus</h4>
                                    <!-- card image -->
                                    <div class="view overlay hm-white-slight">
                                        <?php
                                            /*Obtenemos la imagen destacada horizontal del Filtro seleccionado
                                              Recordando que esta imagen se obtiene del page template asignado
                                              en la categoria de products
                                            */
                                              
                                            $thumbID = get_field('imagen_destacada_horizontal', $paginas->ID);
                                            $urlImg = $thumbID["url"];
                                            $altImg = $thumbID["alt"];
                                            $urlPage = get_permalink($paginas->ID);

                                            /*$thumbID = get_post_thumbnail_id( $paginas->ID );
                                            $urlImg = wp_get_attachment_url( $thumbID );
                                            $altImg = get_post_meta($thumbID, '_wp_attachment_image_alt', true);
                                            $urlPage = get_permalink($paginas->ID);*/
                                            /*End Obtenemos la imagen destacada horizontal del Filtro seleccionado*/

                                        ?>
                                        <img alt="<?php echo $altImg; ?>" class="img-fluid lazy" data-src="<?php echo $urlImg; ?>">
                                        <!-- class="mask waves-effect waves-light" -->
                                        <a><div></div></a>
                                        <span class="campusName"><?php echo $paginas->post_title; ?></span>
                                    </div>
                                    <!-- Button -->
                                    <a class="btn-floating btn-action"><?php echo do_shortcode( '[icono nombre="'.sanitize_title("ver-derecha").'"][/icono]' ); ?></a>
                                    <!-- card image -->

                                    <!-- Aqui van los componentes list_icon, list, texto, cascade, footer, image, collapse -->
                                    <div class="card-block pt-0">
                                        <div class="row">

                                            <?php foreach ($collapsables_campus as $key => $value) { 
                                                if($paginas->post_title == "Campus en Línea" && $collapsables_campus[$key][2] == "mapa-campus"){
                                                    continue;
                                                } else if($paginas->post_title == "Campus en Línea" && $collapsables_campus[$key][2] == "galeria-campus"){
                                                    continue;
                                                }
                                            ?>
                                                <!-- Inicia botones colapsables -->                                 
                                                <div class="simple-list">
                                                    <div type="button" class="list-item-container">
                                                        <div class="item-icon fix-margen-icono">
                                                            <a href="<?php echo $urlPage.'#'.$collapsables_campus[$key][2]; ?>" target="_blank" class="link-simple-list" data-gtm-tr="OurCampus" data-gtm-campus="<?php echo $paginas->post_title; ?>" data-gtm-ubicacion="<?php echo $collapsables_campus[$key][1] ?>"><?php echo do_shortcode( '[icono nombre="'.sanitize_title($collapsables_campus[$key][0]).'"][/icono]' ); ?></a>
                                                        </div>
                                                        <div class="item-description">
                                                            <a href="<?php echo $urlPage.'#'.$collapsables_campus[$key][2]; ?>" class="link-simple-list" data-gtm-tr="OurCampus" data-gtm-campus="<?php echo $paginas->post_title; ?>" data-gtm-ubicacion="<?php echo $collapsables_campus[$key][1] ?>"><?php echo $collapsables_campus[$key][1] ?></a>
                                                        </div>
                                                    </div>                                
                                                </div>
                                           <?php } ?>
                                           <?php
                                                if ($paginas->post_name == "campus-atizapan" || $paginas->post_name == "campus-ecatepec" || $paginas->post_name == "campus-sur" || $paginas->post_name == "campus-toluca"){
                                            ?>
                                                <div class="simple-list">
                                                    <div type="button" class="list-item-container">
                                                        <div class="item-icon fix-margen-icono">
                                                            <a href="<?php echo $urlPage; ?>#tour_virtual" target="_blank" class="link-simple-list" data-gtm-tr="OurCampus" data-gtm-campus="<?php echo $paginas->post_title; ?>" data-gtm-ubicacion="Tour Virtual"><?php echo do_shortcode( '[icono nombre="'.sanitize_title('tour-virtual').'"][/icono]' ); ?></a>
                                                        </div>
                                                        <div class="item-description">
                                                            <a href="<?php echo $urlPage; ?>#tour_virtual" class="link-simple-list" data-gtm-tr="OurCampus" data-gtm-campus="<?php echo $paginas->post_title; ?>" data-gtm-ubicacion="Tour Virtual">Tour Virtual</a>
                                                        </div>
                                                    </div>                                
                                                </div>
                                            <?php 
                                                }
                                            ?>


                                            <!-- <div class="simple-list">
                                                <div type="button" class="list-item-container">
                                                    <div class="item-icon"><a href="<?php echo $urlPage.'#'.$links[0]['como_llegar']; ?>" target="_blank" class="link-simple-list">
                                                    <?php echo do_shortcode( '[icono nombre="'.sanitize_title("transporte-colectivo").'"][/icono]' ); ?></a>
                                                    </div>
                                                    <div class="item-description"><a href="<?php echo $urlPage.'#'.$links[0]['como_llegar']; ?>" target="_blank">¿Como Llegar?</a></div>
                                                </div>                                
                                            </div>
                                            
                                            <div class="simple-list">
                                                <div type="button" class="list-item-container">
                                                    <div class="item-icon">
                                                    <a href="<?php echo $urlPage.'#'.$links[0]['instalaciones']; ?>" target="_blank" class="link-simple-list"><?php echo do_shortcode( '[icono nombre="'.sanitize_title("instalaciones").'"][/icono]' ); ?></a>
                                                    </div>
                                                    <div class="item-description"><a href="<?php echo $urlPage.'#'.$links[0]['instalaciones']; ?>" target="_blank" class="link-simple-list">Instalaciones</a></div>
                                                </div>                                
                                            </div>
                                            
                                            <div class="simple-list">
                                                <div type="button" class="list-item-container">
                                                    <div class="item-icon">
                                                    <a href="<?php echo $urlPage.'#'.$links[0]['laboratorios']; ?>" target="_blank" class="link-simple-list"><?php echo do_shortcode( '[icono nombre="'.sanitize_title("laboratorios").'"][/icono]' ); ?></a>
                                                    </div>
                                                    <div class="item-description"><a href="<?php echo $urlPage.'#'.$links[0]['laboratorios']; ?>" target="_blank">Laboratorios</a></div>
                                                </div>                                
                                            </div>
                                            
                                            <div class="simple-list">
                                                <div type="button" class="list-item-container">
                                                    <div class="item-icon">
                                                    <a href="<?php echo $urlPage.'#'.$links[0]['tour_virtual']; ?>" target="_blank" class="link-simple-list"><?php echo do_shortcode( '[icono nombre="'.sanitize_title("tour-virtual").'"][/icono]' ); ?></a>
                                                    </div>
                                                    <div class="item-description"><a href="<?php echo $urlPage.'#'.$links[0]['tour_virtual']; ?>" target="_blank">Tour Virtual</a></div>
                                                </div>                                
                                            </div> -->

                                        </div>                                                    
                                    </div>
                                </div>
                                <!--/.Card-->
                            </div>
                            <?php } ?>
                            <!--/.First slide-->
                      <?php } /*end if null*/
                        $active = NULL;
                        } /*end foreach*/                                        
                    //} ?> <!-- end while -->
          <?php } ?> <!-- end while -->
                </div>
                <!--/.Slides-->
                </br>
                <!--Indicators-->
                <ol class="carousel-indicators">
                    <?php
                        for ($i=0; $i < count($campus); $i++) {
                            if ($i == 0) {$active = "active"; }
                    ?>
                        <li data-target="#carouselNuestrosCampus" data-slide-to="<?php echo $i; ?>" class="<?php echo $active; ?>"></li>
                    <?php
                            $active = NULL;
                        }
                    ?>
                </ol>
                <!-- Indicators -->
            <?php } ?> <!-- end if -->                                        
                </div>
                <!--/.Carousel Wrapper-->
<!-- ***************************-> Termina tu código ... -->
            </div><!-- col-md-12 -->
        </div><!-- /.Live preview-->
    </section><!-- SECTION-->
</main><!-- /.Main <layout-->