<?php
/*
 * WP Post Template: mob_2_1_banner_estatico
 */
 ?>

<?php

    $field = get_field_object('tipo_de_banner');
    $value = $field['value'];
    $label = $field['choices'][ $value ];

    switch ($value) {
        case 'wordpress':
            $banner = "wordpress";
        break;

        case 'dfp':
         $banner = "dfp";
        break;
         
        default:
            $banner = "wordpress";
        break;
    }

    $idPost = get_the_ID(); 
    $postType = get_post( $idPost );

    if ( $banner == "wordpress" ) {
        
        $imgH=get_field('imagen_destacada_horizontal',$theID);
        $img_by_position =get_the_post_thumbnail_url($theID);

        $bannerimg['vertical']=$img_by_position;
        $bannerimg['horizontal']=$imgH['url'];
        $jsonBanner=json_encode($bannerimg);
?>

<script>
    var jsonBanner=<?php echo $jsonBanner; ?>;
    function orientacionDispositivo() {
        var tipo;
        if (Math.abs(window.orientation) === 90) {
             tipo= "horizontal";
        } else {
             tipo= "vertical";
        }
        return tipo;
    }
    function ajusteBanner(orient, jsonBanner, tipobanner){
    switch(tipobanner){
        case 'imagen':
            if(orient=="vertical") {
                jQuery('#banner-imagen').css('background-image','url('+jsonBanner.vertical+')').promise().done(function(){
                    jQuery(this).animate({
                        opacity: 1
                    }, 600)
                });

            } else if(orient=="horizontal"){
                jQuery('#banner-imagen').css('background-image','url('+jsonBanner.horizontal+')').promise().done(function(){
                    jQuery(this).animate({
                        opacity: 1
                    }, 600)
                }); 

            } else{
                jQuery('#banner-imagen').css('background-image','url('+jsonBanner.vertical+')').promise().done(function(){
                    jQuery(this).animate({
                        opacity: 1
                    }, 600)
                });
            }
        break;

        case 'video':
            var banner_video = document.getElementById("banner-video");
            if(orient=="vertical") {
                if ( banner_video.canPlayType("video/mp4") ) {
                    jQuery("#banner-video").attr({
                        'src': jsonBanner.vertical,
                        'media':'screen and (max-device-width: 600px)'
                    });
                    banner_video.load();
                } else{
                    console.log("el navegador no soporta el tipo de archivo");
                }
            } else if(orient=="horizontal") {
                if ( banner_video.canPlayType("video/mp4") ) {
                    console.log(jsonBanner.horizontal);

                    jQuery("#banner-video").attr({
                        'src': jsonBanner.horizontal,
                        'media':'screen and (min-device-width: 600px)'
                    });
                    banner_video.load();
                } else{
                    console.log("el navegador no soporta el tipo de archivo");
                }
            } else{
                if ( banner_video.canPlayType("video/mp4") ) {
                    jQuery("#banner-video").attr({
                        'src': jsonBanner.vertical,
                        'media':'screen and (max-device-width: 600px)'
                    });
                    banner_video.load();
                } else{
                    console.log("el navegador no soporta el tipo de archivo");
                }
            }
        break;
        }
        bannerHeight();     
    }
    function bannerHeight(){
        setTimeout(function(){
            var heightNavbar = jQuery(".nvct").height();
            var height = jQuery(window).height() - parseInt(heightNavbar);  //getting windows height
            jQuery('.carousel-banner').css('height', height+'px');
            jQuery(".carousel-banner").find(".carousel-item").css('height', height+'px');   //and setting height of carousel
        }, 500);
    }
    jQuery(document).ready(function(){
        var orient = orientacionDispositivo();
        ajusteBanner(orient,jsonBanner,"imagen");
        bannerHeight();

        window.addEventListener("orientationchange", function() {
            // Announce the new orientation number
            orient = orientacionDispositivo();
            ajusteBanner(orient, jsonBanner, 'imagen');
            bannerHeight();
        }, false);
    });

</script>

    <div class="margen-top-menu"></div> 
     <div id="banner-imagen" class="carousel-banner banner banner-imagen mb-2">
        <div class="banner-cinta cinta-end banner-cinta-estatico">
            <div class="banner-cinta wow fadeInLeft">
                    <span class="cintillo">
                        <label class="banner-cintillo-titulo">
                            <span class="orange-text roboto-medium">|</span>
                            <?php echo $postType->post_title; ?>
                        </label>
                    </span>        
                </div>
        </div>
        <a href="#start-page" class="scroll-spy btnB btn-banner"><?php echo do_shortcode( '[icono nombre="'.sanitize_title("ver-abajo").'"][/icono]' ); ?></a>
    </div>

<?php
    } 
    /*End if Banner WordPress By SRP 28-08-2018*/
    else {
?>
        <script>
            var jsonBannerCarrusel = 0;
            // console.log( jsonBannerCarrusel );
            var noSlider = 0;
        </script>

        <div class="result"></div>
        <!-- /7847748/HOMEUNITEC -->

        <script type='text/javascript'>
            var Linea = undefined;
            var mySpreadsheet = undefined;
            var urldfp = undefined;
            var TituloPost = "<?php echo $postType->post_title; ?>";

            function impresion_dfp(linea, spreadsheet, url, urldfp) {
                //console.log(url);
                var self = this;

                $.get("<?php echo get_template_directory_uri(); ?>/secciones_desk/desk_2_11_banner_estatico_DFP.php?titlePost= "+ TituloPost + "&urlDFP=" + urldfp, (data) => {
                        $(".result").html( data );
                    
                    Linea = linea;
                    mySpreadsheet = spreadsheet;
                    $ = jQuery;
                    //console.log("Ejecutando extraer con " + Linea);
                    extraer(Linea);
                });
            };

            var googletag = googletag || {};
            googletag.cmd = googletag.cmd || [];
            (function() {
                var gads = document.createElement('script');
                gads.async = true;
                gads.type = 'text/javascript';
                var useSSL = 'https:' == document.location.protocol;
                gads.src = (useSSL ? 'https:' : 'http:') +
                    '//www.googletagservices.com/tag/js/gpt.js';
                var node = document.getElementsByTagName('script')[0];
                node.parentNode.insertBefore(gads, node);
            })();
        
            googletag.cmd.push(function() {
                googletag.defineSlot('/7847748/BHUNITEC', [1024, 768], 'div-gpt-ad-1534976505516-0').addService(googletag.pubads());
                googletag.pubads().enableSingleRequest();
                googletag.enableServices();
            });
        </script>

        <script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/assets/frontend/js/vendor/min/jqueryLibrariesDFP.min.js"></script>
        <script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/assets/frontend_desktop/js/secciones/min/desk_2_11_banner_estatico_DFP.min.js"></script>
            <div id='div-gpt-ad-1534976505516-0' class='iframedfp' style="height: 0px;">
                <script type='text/javascript'>
                    googletag.cmd.push(function() { googletag.display('div-gpt-ad-1534976505516-0'); });
                </script>
            </div>

<?php
    }
?>
<span id="start-page"></span>