<?php
/**
 * WP Post Template: mob_23_botones_principales
 */
?>

<?php 
    //$nuevaCarrera = get_field('card_nueva_carrera', $theID);
    $nuevaCarrera = get_field('nuevas_carreras', $theID);
    $titulo = get_field('titulo', $theID);
    $boton = get_field('boton', $theID); 
    $active = NULL;   

    $desc = do_shortcode( get_field('descripcion_contenido_botones_principales', $theID) );
    $contenido = explode("|", $desc );
    $descripcion = $contenido[0]. " ".$contenido[1];
?>


<main class="col-12 m-0 bg-light"><!-- /.Main <layout-->
    <section id="elegant-card"><!-- SECTION-->
        <div class="row"><!-- Live preview-->
            <div class="col-md-12 pt-1"><!-- col-md-12 -->
                <article class="prodDescription " data-wow-offset="50">
                    <?php echo $descripcion; ?>
                </article>

            <?php  $links_botones = get_field( 'call_to_actions_pagina', $theID ); 
                if( $links_botones > 0 ) { ?>

                <section class="botonesUnitec">
                    <?php 
                        if( $links_botones > 0 ) {
                            $s=1;

                            for ($i=0; $i < 3; $i++) {
                                $texto_call = $links_botones['pagina_texto_call_to_action_'.$s];
                                $link_call = $links_botones['pagina_link_call_to_action_'.$s];
                                if ( strpos( strtolower( $link_call ), "://" ) ) {

                                    if( strpos( strtolower( $link_call ), "pdf" ) ){
                                        $target = "target='_blank'";
                                    }

                                    else if ( strpos( strtolower( $link_call ), "unitec.mx" ) ) {
                                        $target = "";
                                    }

                                    else{
                                        $target = "target='_blank'";
                                    }
                                } else{
                                    $target = "";
                                }

                                switch ($s) {
                                    case 1: $clase = "btn-primary"; break;
                                    case 2: $clase = "back-naranja-unitec"; break;
                                    case 3: $clase = "back-azul-secundario"; break;                                    
                                    default: break;
                                }
 //Se valida si vienen llenos los botones para que los pueda mostrar
                  if (!empty($texto_call)) {
    
                    ?>
                    <?php if (strpos($link_call, 'javascript') !== false) { ?>
                    <a onclick="<?php echo $link_call; ?>" <?php echo $target; ?> class="btn <?php echo $clase; ?> w-100" data-solicitud-location="Middle"><?php echo $texto_call; ?></a>
                    <?php } else { ?>
                    <a href="<?php echo $link_call; ?>" <?php echo $target; ?> class="btn <?php echo $clase; ?> w-100"><?php echo $texto_call; ?></a>
                    <?php } ?>
                    <?php
                                 $s++; }
                            } 
                        }
                    ?>
                </section>

                <?php } ?>

               
<!-- ***************************-> Inicia tu código ... -->
<!-- ***************************-> Termina tu código ... -->
            </div><!-- col-md-12 -->
        </div><!-- /.Live preview-->
    </section><!-- SECTION-->
</main><!-- /.Main <layout-->