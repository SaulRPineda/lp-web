<?php
/**
 * WP Post Template: mob_33_talleres
 */
?>
<?php
/*Se insertan los plugins que necesita la seccion tal y como se llama el archivo*/
/*load_script( $pluginsNecesarios = array('masonry') );*/
/*End Se insertan los plugins que necesita la seccion tal y como se llama el archivo*/

/*Se insertan los plugins que necesita la seccion tal y como se llama el archivo*/
    load_script( $pluginsNecesarios = array('flipcard') );
/*End Se insertan los plugins que necesita la seccion tal y como se llama el archivo*/

$args = array(
    'post_type'             => 'talleres-unitec',
    'post_status'           => 'publish',
    'ignore_sticky_posts'   => 1,
    'posts_per_page'        => -1
);
$talleres = query_posts($args);
$actualTaller = 0;
$categoryTaller = [];
$categoryTallerCampus = [];
$categoryTallerContactData = [];
$categoryTallerLinkData = [];
$field_key = NULL;
$categoryTalleres = [];
$json_talleres = NULL;
$array_campus = [];

$totalCaracteresShortDesc = 90;

$urlPage = get_permalink($paginas->ID);

$loc=setlocale(LC_TIME, 'es_MX');
if ($loc==false) {
    setlocale(LC_TIME, 'esp_esp');
}

$json_talleres = '{';
//   print_r($talleres);
foreach ($talleres as $key => $taller) {
    $json_talleres .= '"'.$taller->post_name.'":';


    $taller_categoria = get_field('talleres_categoria',$taller->ID);
    $taller_tipo = get_field('talleres_tipo',$taller->ID);
    $taller_shortDescription = get_field('talleres_descripcion_corta',$taller->ID);
    $taller_longDescription = get_field('talleres_descripcion_completa',$taller->ID);
    $taller_campus_original = get_field('talleres_campus',$taller->ID);
    $taller_completeDescription = get_field('talleres_descripcion_completa',$taller->ID);
    $taller_image = get_field('talleres_imagen',$taller->ID);
    $taller_fecha = get_field('talleres_fecha_del_evento',$taller->ID);
    $taller_horario = get_field('talleres_horario',$taller->ID);
    $taller_tipo_evento = get_field('talleres_tipo_de_evento',$taller->ID);
    $taller_url = get_field('url_de_boton',$taller->ID);

    // $d = DateTime::createFromFormat('d/m/Y', $taller_fecha);
    // $fechaTaller = strftime("%A %d de %B del %G", $d->getTimestamp());

    $field = get_field_object('talleres_campus',$taller->ID);

    if( ! is_array($taller_campus_original)){
        $taller_campus[] = $taller_campus_original;
    }else{
        $taller_campus = $taller_campus_original;
    }

    $json_talleres .= '{"title_taller":"'.addslashes(trim($taller->post_title)).'", "url_image":"'.trim($taller_image).'", "categoria": "'.trim($taller_categoria).'", "tipo": "'.trim($taller_tipo).'", "descripcion_corta": "'.trim($taller_shortDescription).'", "descripcion_completa": "'.trim($taller_longDescription).'", "tipo_taller": "'.trim($taller_tipo_evento).'",  "fecha_taller": "'.trim($taller_fecha).'", "hora-inicio": "'.trim($taller_horario['talleres_hora_de_inicio']).'", "hora-fin": "'.trim($taller_horario['talleres_hora_de_fin']).'", "boton-formulario": "'.trim($taller_url).'", "campus_taller": '.json_encode($taller_campus).' },';

    if($actualTaller == 0) {

        $field_key = get_post_meta( $taller->ID, '_talleres_categoria', true);

        $field = get_field_object($field_key);
        $choices = $field['choices'];
        $categoryTaller = $choices;
        $categoryTaller = array("all-tipes" => "Ver todas las Categorías") + $categoryTaller;

        $field_key = get_post_meta( $taller->ID, '_talleres_campus', true);
        $field = get_field_object($field_key);

        $choices = $field['choices'];
        // print_r($choices);
        $categoryTallerCampus = $choices;
        $categoryTallerCampus = array("all-campus" => "Ver todos los Campus") + $categoryTallerCampus;
    }

    $categoryTalleres.array_push($categoryTalleres, $taller->post_name);

    $actualTaller++;
}

$json_talleres = substr($json_talleres, 0, strlen($json_talleres) - 1);
$json_talleres .= '}';

/* Limpia el JSON de caracteres que no son ASCII */
// echo $json_talleres . '<br>';
// $json_talleres = preg_replace('/[\x00-\x1F\x80]/', '', $json_talleres);
$json_talleres = preg_replace('/[\n]/', '<br />', $json_talleres);
$json_talleres = preg_replace('/[\r]/', '', $json_talleres);
// $json_talleres = nl2br($json_talleres);

?>

<script>
    var talleres_json = '<?php echo $json_talleres; ?>';
    var category_array_json = '<?php echo json_encode($categoryTaller) ?>';
</script>

<?php
    // $json_talleres = json_decode(preg_replace('/[\x00-\x1F\x80-\xFF]/', '', $json_talleres), true); // Quita las letras con acentos, no usar si es en español
    /* Se usa striplashes para volver los double quotes en un string a la normalidad */
    $json_talleres = json_decode(stripslashes($json_talleres), true);
?>


<div id="talleres"></div>

<section class="container-fluid wow fadeIn d-flex justify-content-center align-items-center descuentos mt-2"><!-- /.Main <layout-->
    <div class="col-12 contenedor-descuentos card mb-1"> <!-- Contenedor de la galería -->
        <article class="text-center w-100">
            <h4 class="row card-title justify-content-center"><?php echo get_field('titulo_sec_talleres'); ?></h4>
        </article>
        <div class="w-100 centrar-contenido mt-1 mb-3">
            <div class="row filtros mb-1">
                <div class="col-xl-6 col-md-6 col-sm-12">
                    <div class="dropdown">
                        <div class="drop-view filtro-descuentos d-flex align-items-center justify-content-center p-2" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            <div class="col-11"><h4 id="nameCampusDescuentos" data-descuentos-campus="">Ver todos los Campus</h4></div>
                            <div class="col-1 d-flex align-items-center justify-content-end"><?php echo do_shortcode( '[icono nombre="'.sanitize_title("ver-abajo").'" clase="icon-abajo"][/icono]' ); ?></div>
                        </div>
                        <div class="dropdown-menu dropdown-primary menu-filtro-descuentos">
                            <?php foreach ($categoryTallerCampus as $key => $campus_taller) { ?>
                                <a class="dropdown-item destacar" onclick="filtroTipoCampus( '<?php echo $key ?>', '<?php echo $campus_taller; ?>' );" title="<?php echo $campus_taller; ?>"><?php echo $campus_taller; ?></a>
                            <?php
                            } ?>
                        </div>
                    </div>
                </div>

                <div class="col-xl-6 col-md-6 col-sm-12">
                    <div class="dropdown">
                        <div id="" class="drop-view filtro-descuentos d-flex align-items-center justify-content-center p-2 pl-4 pr-4" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            <div class="w-75"><h4 id="nameCategoriasDescuentos" data-descuentos-categorias="">Ver todas las Categorías</h4></div>
                            <div class="w-25 w-25 d-flex align-items-center justify-content-end"><?php echo do_shortcode( '[icono nombre="'.sanitize_title("ver-abajo").'" clase="icon-abajo"][/icono]' ); ?></div>
                        </div>
                        <div class="dropdown-menu dropdown-primary menu-filtro-descuentos">
                            <?php foreach ($categoryTaller as $key => $category_taller) { ?>
                                <a class="dropdown-item destacar" onclick="filtroTipoCategorias( '<?php echo $key ?>', '<?php echo $category_taller; ?>' );" title="<?php echo $category_taller; ?>"><?php echo $category_taller; ?></a>
                            <?php
                            } ?>
                        </div>
                    </div>
                </div>
            </div>
            <section class="row fila-descuentos">
                <article class="col-12 text-center d-flex justify-content-center" data-wow-offset="50">
                    <h4 class="w-75 titulo-descuentos" id="available-discounts" hidden>No hay eventos disponibles</h4>
                </article>
                <?php
                    $i = 0;
                    foreach ($json_talleres as $key_taller => $taller_array) {
                        switch ($taller_array['categoria']) {
                            case 'preparatoira':
                                $colorCategoria = "preparatoira";
                                break;
                            case 'licenciaturas':
                                $colorCategoria = "licenciaturas";
                                break;
                            case 'licenciaturas-en-salud':
                                $colorCategoria = "licenciaturas-en-salud";
                                break;
                            case 'ingenierias':
                                $colorCategoria = "ingenierias";
                                break;
                            case 'diplomados ':
                                $colorCategoria = "diplomados";
                                break;
                            case 'maestrias ':
                                $colorCategoria = "maestrias";
                                break;

                            
                            default:
                                $colorCategoria = "default";
                                break;
                        }
                        $descripcionCorta = ( strlen( $taller_array['descripcion_corta'] ) > $totalCaracteresShortDesc )? substr($taller_array['descripcion_corta'], 0, $totalCaracteresShortDesc)." ...": $taller_array['descripcion_corta'];
                        $d = DateTime::createFromFormat('d/m/Y', $taller_array['fecha_taller']);
                        $fechaTaller = strftime("%A %d de %B del %G", $d->getTimestamp());
                ?>
                <div class="col-lg-4 col-sm-12 mb-2 item-<?php echo $key_taller; ?> categoria-<?php echo $taller_array['categoria']; ?> <?php echo implode(" ",preg_filter('/^/', 'campus-', $taller_array['campus_taller'])); ?> campus-all-campus categoria-all-tipes" id="<?php echo $key_taller; ?>" name="<?php echo $key_taller; ?>" title="<?php echo $taller_array['title_taller']; ?>" style="<?php if ($i++ >= 9) //echo "display:none"; ?>">
                    <div class="w-100 card-wrapper cardHeight m-0">
                        <div id="taller-<?php echo $key_taller; ?>" alt="taller <?php echo $taller_array['title_taller']; ?>" class="card-rotating effect__click text-center h-100 w-100 d-flex align-items-center lazy">
                            <div class="face front mt-2 px-2">
                                <div class="cover">
                                    <div class="etiqueta <?php echo $colorCategoria; ?>"><?php echo $categoryTaller[$taller_array['categoria']]; ?></div>
                                    <img data-src="<?php echo $taller_array['url_image']; ?>" class="w-100 lazy">
                                </div>
                                <div class="content text-left py-0 px-1">
                                    <div class="titleTaller my-2"><?php echo ucfirst($taller_array['tipo']). ': ' .$taller_array['title_taller']; ?></div>
                                    <div class="desc-prod">
                                        <p class="shortDescription m-0 my-2"><?php echo $descripcionCorta; ?></p>                             
                                    </div>
                                    <div class="row rowData my-1">
                                        <div class="col-1 d-flex align-items-center"><?php echo do_shortcode( '[icono nombre="'.sanitize_title("ubicacion").'" clase=""][/icono]' ); ?></div>
                                        <!-- <div class="col-10 tallerCampus"><span><?php print_r() ?></span></div> -->
                                        <div class="col-10 tallerCampus">
                                        <?php if(count($taller_array['campus_taller']) > 1){
                                            echo "<a data-card='taller-".$key_taller."' title='Ver campus disponibles' class='rotate-btn rotateCampus p-0 m-0' data-gtm-tr='evTaller' data-gtm-pos='Ver campus disponibles' data-gtm-taller='".$taller_array['title_taller']."'>Click para ver todos los Campus</a>";
                                        }else{
                                            echo "<span>".$categoryTallerCampus[$taller_array['campus_taller'][0]]."</span>";
                                        } 
                                        ?>
                                        </div>
                                    </div>
                                    <div class="row rowData my-1">
                                        <div class="col-1 d-flex align-items-center"><?php echo do_shortcode( '[icono nombre="'.sanitize_title("calendario").'" clase=""][/icono]' ); ?></div>
                                        <div class="col-10"><span><?php echo utf8_encode(ucfirst($fechaTaller)); ?></span></div>
                                    </div>
                                    <div class="row rowData my-1">
                                        <div class="col-1 d-flex align-items-center"><?php echo do_shortcode( '[icono nombre="'.sanitize_title("horarios").'" clase=""][/icono]' ); ?></div>
                                        <div class="col-10"><span><?php echo substr($taller_array['hora-inicio'], 0, 5); ?> - <?php echo substr($taller_array['hora-fin'], 0, 5); ?></span></div>
                                    </div>
                                    <div class="row rowData my-1">
                                        <div class="col-1 itemIcon d-flex align-items-center"><?php echo do_shortcode( '[icono nombre="'.sanitize_title("financiamientos").'" clase=""][/icono]' ); ?></div>
                                        <div class="col-10 tallerCost"><span><?php echo $taller_array['tipo_taller']; ?></span></div>
                                    </div>
                                </div>
                                <div class="footer w-100 px-3 py-2">
                                    <div class="d-flex justify-content-between">
                                        <!-- Triggering button -->
                                        <a data-card="taller-<?php echo $key_taller; ?>" title="Ver Más" class="rotate-btn verMas-taller d-flex justify-content-left align-items-center" data-gtm-tr="evTaller" data-gtm-pos="Ver Màs" data-gtm-taller="<?php echo $taller_array['title_taller']; ?>"><?php echo do_shortcode( '[icono nombre="'.sanitize_title("ver-todo").'" clase="mr-2"][/icono]' ); ?> Ver más</a>
                                        <?php 
                                            $link = ( strpos( strtolower( trim( $taller_array['boton-formulario'] ) ), "www" )  ) ? 'href=' : 'onclick=';
                                            $target = ( strpos( strtolower( trim( $taller_array['boton-formulario'] ) ), "www" ) ) ? "target='_blank'" : "";
                                            // if ( strpos( strtolower( trim( $taller_array['boton-formulario'] ) ), "#modal_frm_taller") ) {
                                            //     $trackeoLocation =  'data-solicitud-location="Middle"';
                                            // } else if( strpos( strtolower( trim( $taller_array['boton-formulario'] ) ), "www")) {
                                            //     $trackeoLocation =  'data-gtm-tr="evWhats" data-gtm-accion="intención" data-gtm-seccion="contacto" data-gtm-depto=""';
                                            // } else {
                                            //     $trackeoLocation = '';
                                            // }
                                        ?>
                                        <a <?php echo $link.$taller_array['boton-formulario'].' '.$target; ?> title="Asistir" class="asistir d-flex justify-content-center align-items-center" data-gtm-tr="evTaller" data-gtm-pos="Asistir" data-gtm-taller="<?php echo $taller_array['title_taller']; ?>">ASISTIR</a>
                                    </div>
                                </div>
                            </div> <!-- end front panel -->
                            <div class="face back mt-2 d-flex p-0">
                                <div class="content p-0 w-100">
                                    <div class="main p-3">
                                        <div class="title text-center mb-1"><?php echo $taller_array['title_taller']; ?></div>
                                        <div class="text-center longDescription">
                                            <?php echo $taller_array['descripcion_completa']; ?>
                                        </div>
                                        <div class="text-center tallerCampus mt-1">
                                            <?php
                                                foreach ($taller_array['campus_taller'] as $key_campus => $campus) {
                                                    $campusDisponibles .= $categoryTallerCampus[$campus]. ', ';
                                                }
                                                $campusDisponibles = substr($campusDisponibles, 0 , strlen($campusDisponibles) -2);
                                                echo $campusDisponibles;
                                             ?>
                                        </div>
                                        <a <?php echo $link.$taller_array['boton-formulario'].' '.$target; ?> title="Asistir" class="asistir d-flex justify-content-center align-items-center w-100 my-3" data-gtm-tr="evTaller" data-gtm-pos="Asistir" data-gtm-taller="<?php echo $taller_array['title_taller']; ?>">ASISTIR</a>
                                        <div class="stats-container d-flex">
                                            <div class="stats d-block w-100">
                                                <h4 class="desktop-card-title-comparte text-center border-0">Comparte</h4>
                                                <div class="card-back text-center d-flex align-items-center justify-content-center">
                                                    <?php echo do_shortcode('[desktop_share cerrar="card-evento-'.$i.'" pagina="'.get_permalink($carreras[$i]->ID).'" clase="btn-share" tipo="inline-ul-desktop" seccion="Taller o evento: '.$carreras[$i]->post_title.'"]');?>
                                                </div>    
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="footer text-center w-100">
                                    <a class="regresar rotate-btn d-flex justify-content-center align-items-center" title="Volver" data-card="taller-<?php echo $key_taller; ?>">
                                        <svg class="mr-2" width="20" opacity="0.5" viewBox="0 0 31.999999 26.895333" >
                                            <g transform="matrix(.083119 0 0 .083119 5.5295e-6 -2.5523)">
                                                <path d="m383.37 344.04c0.602-4.042 13.342-101.18-43.778-168.02-35.092-41.071-89.241-63.556-159.16-65.782l-0.373-67.526c0-4.644-2.683-8.866-6.869-10.863-4.199-1.961-9.167-1.396-12.764 1.54l-156 128.03c-2.791 2.286-4.415 5.702-4.427 9.311s1.6 7.026 4.391 9.311l156.01 129.24c3.585 2.971 8.602 3.585 12.764 1.588 4.223-1.985 6.893-6.232 6.893-10.875l0.373-69.042c168.53-0.12 180.52 107.86 180.92 112.33 0.517 6.063 4.355 10.791 10.442 10.984 0.144 0.012 0.277 0.012 0.409 0.012 5.956-1e-3 10.274-4.344 11.165-10.239zm-223.82-133.82c-2.262 2.262-3.537 5.317-3.537 8.517v55.797l-124.98-103.72 124.98-102.76v54.052c0 6.653 5.378 12.03 12.03 12.03 68.452 0 120.75 19.826 153.21 57.457 24.409 28.319 35.634 62.979 39.471 91.743-26.274-35.898-72.278-76.549-192.66-76.621h-0.024c-3.176 0-6.231 1.264-8.481 3.513z"/>
                                            </g>
                                        </svg>
                                        <span>Regresar</span>
                                    </a>
                                </div>
                            </div> <!-- end back panel -->
                        </div> <!-- end card -->
                    </div> <!-- end card-container -->                          
                </div>
            <?php $campusDisponibles = ""; } ?>
            </section>
        </div>
    </div>
</section>
