<div id="card_contacto"></div>
<?php
/**
 * WP Post Template: mob_28_card_contacto
 */
?>

<?php
    $noTabs=NULL;

?>
<main class="col-12 wow fadeInRight mt-3">
    <!-- SECTION-->
    <section id="elegant-card">
        <!-- Live preview-->
        <div class="row">
            <div class="col-md-12">
                <!-- Tab Navegacion -->
                <section  id="tabNav">
                    <?php echo do_shortcode( '[icono nombre="'.sanitize_title("ver-izquierda").'" clase="leftTabNav direccionales" estilo="style=\'display:none\'"][/icono]' ); ?>
                    <div class="tabs-wrapper">
                        <ul class="nav classic-tabs" role="tablist">
                        <?php 
                            if(have_rows('tabs_contacto_carrusel',$theID) ) { 
                                $noTabs=0;
                                while( have_rows('tabs_contacto_carrusel') ) { the_row(); 
                                    if($noTabs == 0){ $active = "active text-black"; } 
                        ?>        
                            <li class="nav-item"><!-- waves-effect waves-light -->
                                <a class=" waves-light <?php echo $active; ?> " data-toggle="tab" href="#tab-panel-mapa-<?php echo $noTabs; ?>" role="tab"><?php echo get_sub_field('titulo_tab_card_contacto'); ?></a>
                            </li>
                            <?php
                                $noTabs++; 
                                $active = NULL; 
                                } 
                            } 
                        ?>
                        </ul>
                    </div>
                    <?php echo do_shortcode( '[icono nombre="'.sanitize_title("ver-derecha").'" clase="rightTabNav direccionales" estilo="style=\'display:none\'"][/icono]' ); ?>
                </section>
                <!-- Tab panels -->
                <div class="tab-content lighten-4">
                  <?php 
                    if(have_rows('tabs_contacto_carrusel',$theID) ) { 
                        $panelContent = 0; 
                        while( have_rows('tabs_contacto_carrusel') ) { the_row(); 
                            $active = NULL; 
                            $itemSlide = 0;  /* Total de Cards en el Carrusel */
                            $slide = 0; /* Contador de indicador */
                            if($panelContent == 0) { $active = "active"; } 
                    ?>
                    <!-- Carousel Tab -->
                    <div class="tab-pane fade in show <?php echo $active; ?>" id="tab-panel-mapa-<?php echo $panelContent; ?>" role="tabpanel">
                    <!-- Carousel Slides -->            
                        <div id="carouselUnitec-mapa-<?php echo $panelContent; ?>" class="carousel slide carousel-fade" data-ride="carouselUnitecMapa<?php echo $panelContent; ?>">
                            <?php 
                                while( have_rows('contenido_card_contacto') ) { the_row(); 
                                    //Obtener opcion del flexible content
                                    $opcion = get_row_layout('contenido_card_contacto');
                                    $getCampusNo = get_sub_field('opciones_campus');
                                        switch ($getCampusNo) {
                                            case 'vacio':
                                                $campusNo = '';
                                                break;
                                            case 'atizapan':
                                                $campusNo = '340';
                                                break;
                                            case 'cuitlahuac':
                                                $campusNo = '342';
                                                break;
                                            case 'ecatepec':
                                                $campusNo = '219';
                                                break;
                                            case 'guadalajara':
                                                $campusNo = '348';
                                                break;
                                            case 'leon':
                                                $campusNo = '350';
                                                break;
                                            case 'marina':
                                                $campusNo = '352';
                                                break;
                                            case 'los-reyes':
                                                $campusNo = '1057';
                                                break;
                                            case 'queretaro':
                                                $campusNo = '354';
                                                break;
                                            case 'sur':
                                                $campusNo = '356';
                                                break;
                                            case 'toluca':
                                                $campusNo = '358';
                                                break;
                                            
                                            default:
                                                $campusNo = '';
                                                break;
                                        }
                                    if($itemSlide == 0) { $active = "active"; 
                            ?>
                            <!-- Carousel Inner -->
                            <div class="carousel-inner <?php echo $class_div; ?>" role="listbox">
                            <?php   }   ?>

                            <?php   
                                switch ($opcion) {
                                    case 'contenido':
                                            $titulo_card = get_sub_field('subtitulo_card_contacto_carrusel'); 
                                            $imagen = get_sub_field('imagen_contacto_carrusel');
                                            $lista = get_sub_field('tipo_lista_contacto_carrusel');
                            ?>
                                <!--First slide-->
                                <div class="carousel-item <?php echo $active; ?>">                                 
                                    <!--Card-->
                                    <div class="card">
                                        <!--Title-->
                                        <h4 class="card-title"><?php echo get_sub_field('subtitulo_card_contacto_carrusel'); ?></h4>
                                        <!-- card image -->
                                        <div class="view overlay hm-white-slight">
                                            <img alt="<?php echo $imagen['alt'] ?>" class="img-fluid lazy" data-src="<?php echo $imagen['url']; ?>">
                                        </div>
                                        <!--Button-->
                                        <!-- <a class="showarrow-<?php echo $panelContent; ?> d-none btn-floating btn-action"><?php echo do_shortcode( '[icono nombre="'.sanitize_title("ver-derecha").'"][/icono]' ); ?></a> -->

                                        <!-- Aqui van los componentes list_icon, list, texto, cascade, footer, image, collapse -->
                                        <div class="card-block pt-0">
                                            <div class="row">
                                                <br>
                                                <div class="col-12 p-0">
                                                    <!-- Comienza implemantacio para agregar descripci�n de contacto by AMC -->
                                                    <?php   
                                                        $contenidoContactoInfo = get_sub_field( 'cardContactoCampus_carrusel' );
                                                        foreach ($contenidoContactoInfo as $k => $datoContacto) {
                                                    ?>
                                                    <div class="simple-list">
                                                        <div type="button" class="row list-item-container border-top-fix m-0">
                                                            <div class="col-2 py-2 flex-item list-icon d-flex align-items-center justify-content-center">
                                                                <span class="d-flex"><?php echo do_shortcode( '[icono nombre="'.sanitize_title( $datoContacto['iconoContactoCampus_carrusel'] ).'" clase="icon-size"][/icono]' ); ?></span>
                                                            </div>
                                                            <div class="col-10 d-flex align-items-center">
                                                                <span><?php echo $datoContacto['nombreContacto_carrusel']; ?></span>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <?php } ?>

                                                    <?php
                                                        $direccion_campus = get_field('direccion_del_campus' , $campusNo);
                                                        if( $direccion_campus != NULL && $direccion_campus !="" && strlen($direccion_campus) > 1 ) { 
                                                    ?>
                                                    <div class="simple-list">
                                                        <div type="button" class="row list-item-container border-top-fix m-0" data-toggle="collapse">
                                                            <div class="col-2 flex-item list-icon d-flex align-items-center justify-content-center"><a href="//maps.google.es/?q=UNITEC, <?php echo get_field('direccion_del_campus' , $campusNo); ?>" title="<?php echo get_field('direccion_del_campus' , $campusNo); ?>" target="_blank" style="color:#757575;"><?php echo do_shortcode( '[icono nombre="'.sanitize_title("ubicacion").'" clase="d-flex"][/icono]' ); ?></a></div>
                                                            <div class="col-10 flex-item d-flex align-items-center item-description"><a href="//maps.google.es/?q=UNITEC, <?php echo get_field('direccion_del_campus' , $campusNo); ?>" title="<?php echo get_field('direccion_del_campus' , $campusNo); ?>" target="_blank"><?php echo get_field('direccion_del_campus' , $campusNo); ?></a></div>
                                                        </div>
                                                    </div>
                                                    <?php } ?>
                                                    <?php
                                                        $telefonos = NULL;
                                                        $contacto_telefonico = get_sub_field('contacto_telefonico_carrusel');
                                                        foreach ($contacto_telefonico['telefonos_carrusel'] as $key => $valuePhone) {
                                                            $telefonos[] = $valuePhone;
                                                        }
                                                    ?>
                                                    <?php if ( count($telefonos) > 0  ) { ?>
                                                    <div class="simple-list">
                                                        <div type="button" class="row list-item-container border-top-fix m-0" data-toggle="collapse" href="#tel-collapse-<?php echo $panelContent; ?>-<?php echo $active; ?>" role="button" aria-expanded="false" aria-controls="tel-collapse-<?php echo $panelContent; ?>-<?php echo $active; ?>">

                                                            <?php if ( count($telefonos) === 1 ) {
                                                                $enlaceInicioIcon = '<a href="tel:'.$telefonos[0]['numero_telefonico_carrusel'].'" class="list-icon d-flex" title="tel&eacute;fono:'.$telefonos[0]['numero_telefonico_carrusel'].'">';
                                                                $enlaceInicioItem = '<a href="tel:'.$telefonos[0]['numero_telefonico_carrusel'].'" class="link-simple-list d-flex" title="tel&eacute;fono:'.$telefonos[0]['numero_telefonico_carrusel'].$telefonos[0]['extension_carrusel'].'">';
                                                                $enlaceFin = '</a>';
                                                            } ?>

                                                            <div class="col-2 flex-item list-icon d-flex align-items-center justify-content-center py-2"><?php echo $enlaceInicioIcon; ?><?php echo do_shortcode( '[icono nombre="'.sanitize_title("llamar-footer").'" clase="arrow-left"][/icono]' ).$enlaceFin; ?></div>
                                                            <div class="col-5 flex-item d-flex align-items-center item-description-arrow"><?php echo $enlaceInicioItem; ?><?php echo $telefonos[0]['numero_telefonico_carrusel'].$enlaceFin; ?></div>                                        
                                                            <div class="col-4 flex-item d-flex align-items-center item-description-arrow"><?php echo $enlaceInicioItem; ?><?php echo $telefonos[0]['extension_carrusel'].$enlaceFin; ?></div>                                        
                                                            <?php if ( count($telefonos) > 1 ) { ?>
                                                            <div class="col-1 flex-item d-flex align-items-center justify-content-center p-0">
                                                                <div class="arrow p-0"><?php echo do_shortcode( '[icono nombre="'.sanitize_title("ver-abajo").'"][/icono]' ); ?></div>
                                                            </div>    
                                                            <?php } ?>
                                                        </div>

                                                        <?php if ( count( $telefonos) > 1 ) { ?>
                                                        <div class="text-colapse card-flotante collapse multi-collapse collapse bg-white" id="tel-collapse-<?php echo $panelContent; ?>-<?php echo $active; ?>">
                                                            <div class="animated flipInX">
                                                                <?php foreach ($telefonos as $key => $value) { ?>
                                                                <a href="tel:<?php echo $value['numero_telefonico_carrusel']; ?>" class="link-simple-list" title="<?php echo $value['numero_telefonico_carrusel']; ?>">
                                                                    <div class="row">
                                                                        <div class="col-2 mt-1"></div>
                                                                        <div class="col-5 mt-1"><?php echo $value['numero_telefonico_carrusel']; ?></div>
                                                                        <div class="col-5 mt-1"><?php echo $value['extension_carrusel']; ?></div>
                                                                    </div>
                                                                </a>
                                                                <?php } ?>
                                                            </div>
                                                        </div>
                                                        <?php } ?>
                                                    </div>
                                                    <?php } //Valida si existen no telefónicos ?>

                                                    <?php
                                                    $horarios = get_sub_field('horarios_carrusel');
                                                    $dias_semana = array("Domingo", "Lunes", "Martes", "Mi&eacute;rcoles", "Jueves", "Viernes", "S&aacute;bado", "Domingo");  
                                                    $horarios_semana = array_values( $horarios );
                                                    array_push( $horarios_semana, $horarios_semana[0] ); 
                                                    date_default_timezone_set("America/Mexico_City");
                                                    $dia = date('w');
                                                    ?>
                                                    
                                                    <?php if( !empty( $horarios['domingo'] ) ) {
                                                    //if ( count($horarios) > 0 ) { ?>
                                                    <div class="simple-list">
                                                        <div type="button" class="row list-item-container border-top-fix m-0">
                                                            <div class="flex-item item-icon list-icon"><?php echo do_shortcode( '[icono nombre="'.sanitize_title("horarios").'"][/icono]' ); ?></div>
                                                            <div class="flex-item item-description pl-3">
                                                                <div class="row fix-horarios-button">
                                                                    <!-- <span class="col-5 horarios-pre d-flex flex-column justify-content-center"><script type="text/javascript">document.write( diasSemana[f.getDay()] );</script><?php //echo ucwords($dias_semana[$dia]); ?></span> -->
                                                                    <span class="col-5 horarios-pre d-flex flex-column justify-content-center"><?php echo ucwords($dias_semana[$dia]); ?></span>
                                                                    <span class="col-7 horarios-pre d-flex flex-column justify-content-center"><?php echo $horarios_semana[$dia]; ?></span>
                                                                </div>
                                                            </div>
                                                            <div class="arrow"><?php echo do_shortcode( '[icono nombre="'.sanitize_title("ver-abajo").'"][/icono]' ); ?></div>
                                                        </div>
                                                    </div>
                                                    <div class="text-colapse collapse animated flipInX">
                                                        <div class="fix-horario-campus horario">
                                                            <?php 
                                                                for ($i = 1; $i < count($dias_semana); $i++) {
                                                            ?>
                                                            <label class="horario-item"><label class="horario-dia"> <?php echo ucwords($dias_semana[$i]); ?></label><label class="horario-hora"><?php echo $horarios_semana[$i]; ?></label></label>
                                                                <?php
                                                                }
                                                            ?>
                                                        </div>
                                                    </div>
                                                    <?php } ?>
                                                    <!-- Comienza implemantacio para agregar contacto redes sociales -->
                                                    <?php   
                                                        $contenidoRedes = get_sub_field( 'cardContactoSociales_carrusel' );
                                                        foreach ($contenidoRedes as $key => $dato) {
                                                            $urlItem = $dato['urlContacto_carrusel'];
                                                            $emailItem = str_split($urlItem);
                                                            $tipoURL = "unitec.mx";
                                                            $mailto = '';
                                                            if (in_array("@", $emailItem)) {
                                                                $tipoURL = "@";
                                                                $mailto = 'mailto:';
                                                            }
                                                            $urlEnlace = ( strpos( strtolower( trim( $dato['urlContacto_carrusel'] ) ), $tipoURL )  );
                                                            $link = $urlEnlace ? 'href=' : 'onclick=';
                                                            $target = ( strpos( strtolower( trim( $dato['urlContacto_carrusel'] ) ), "unitec" ) ) ? "target='_blank'" : "";
                                                            if ( strpos( strtolower( trim( $dato['urlContacto_carrusel'] ) ), "#modal_frm_app") ) {
                                                                $trackeoLocation =  'data-solicitud-location="Middle"';
                                                                $ocultarWhats = '';
                                                            } else if( strpos( strtolower( trim( $dato['urlContacto_carrusel'] ) ), "#modal-whatsapp")) {
                                                                $trackeoLocation =  'data-gtm-tr="evWhats" data-gtm-accion="intenci�n" data-gtm-seccion="contacto" data-gtm-depto=""';
                                                                $ocultarWhats = 'd-none';
                                                            } else if( strpos( strtolower( trim( $dato['urlContacto_carrusel'] ) ), "zopim.livechat.window.show()")) {
                                                                $trackeoLocation =  'data-gtm-tr="chat" data-gtm-pos="intencion" data-gtm-location="Secci&oacute;n Contacto Carrusel" data-gtm-etiqueta="con formulario"';
                                                            } else {
                                                                $trackeoLocation = '';
                                                                $ocultarWhats = '';
                                                            }
                                                    ?>
                                                    <div class="simple-list">
                                                        <div type="button" class="row list-item-container border-top-fix m-0">
                                                            <div class="col-2 py-2 flex-item list-icon d-flex align-items-center justify-content-center">
                                                                <a class="d-flex textItemCard" <?php echo $link.' " '.$mailto.trim($dato['urlContacto_carrusel']).' " '.$target ?> title="<?php echo $dato['descripcionContacto_carrusel']; ?>"><?php echo do_shortcode( '[icono nombre="'.sanitize_title( $dato['iconoContacto_carrusel'] ).'" clase="icon-size"][/icono]' ); ?></a>
                                                            </div>
                                                            <div class="col-10 d-flex align-items-center">
                                                                <a class="textItemCard" <?php echo $link.' " '.$mailto.trim($dato['urlContacto_carrusel']).' " '.$target ?> title="<?php echo $dato['descripcionContacto_carrusel']; ?>"><?php echo $dato['descripcionContacto_carrusel']; ?></a>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <?php } ?>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!--/.First slide-->
                                <?php
                                    $active = NULL; 
                                    $itemSlide++;
                                    }
                                }
                                ?>
                                <?php 
                                    switch ($opcion) {
                                        case 'mapa_curricular':
                                ?>
                            </div><!-- carousel-inner -->
                        </div><!-- carousel-Slides -->
                    </div><!-- carousel-Tab -->
                </div><!-- Tab panels -->
            </div><!-- col-md-12 -->
        </div><!-- row -->
        <?php break; case 'contenido': ?>
        </div><br>
        <!-- Indicators -->
        <?php if ($itemSlide > 1) { ?>
        <ol class="carousel-indicators">
            <?php 
                $slide = 0;
                $active = NULL;
                for ($i=0; $i < $itemSlide; $i++) { 
                    if ($slide == 0) { $active = "active"; } ?>
            <li data-target="#carouselUnitec-mapa-<?php echo $panelContent; ?>" data-slide-to="<?php echo $i; ?>" class="<?php echo $active; ?>">></li>
            <?php 
                $active = NULL;
                $slide++;
                } 
            ?>                                           
        </ol>
        <?php } ?>
        <!-- Indicators -->
        <input type="hidden" name="sliders-<?php echo $panelContent; ?>" id="sliders-<?php echo $panelContent; ?>" value="<?php echo $itemSlide; ?>">
        </div>          
        <?php
            break; 
                default: break;
            }
        ?>                            
        <?php $panelContent++; ?>
        </div>       
            <?php }  ?>
        <?php }   ?>         
        <input type="hidden" name="panelContent" id="panelContent" value="<?php echo $panelContent; ?>">       
    </section><!-- SECTION-->
</main><!-- /.Main <layout-->