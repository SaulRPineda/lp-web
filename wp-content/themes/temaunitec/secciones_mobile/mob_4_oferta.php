<div id="oferta-educativa"></div>
<?php
/**
 * WP Post Template: mob_4_oferta
 */

?>
<div class="margen-top-menu"></div> 
<main class="col-12 mt-0 bg-light"><!-- /.Main <layout--> 
    <section id="elegant-card"><!-- SECTION-->
        <div class="row"><!-- Live preview-->
            <div class="col-md-12"><!-- col-md-12 -->
<!-- ***************************-> Inicia tu código ... -->
                        <section id="tabNav">
                            <!-- Tab Navegacion -->
                            <?php echo do_shortcode( '[icono nombre="'.sanitize_title("tab-izquierda").'" clase="leftTabNav direccionales" estilo="style=\'display:none\'"][/icono]' ); ?>
                            <div class="tabs-wrapper" id="elementoConScroll">
                                <ul class="nav classic-tabs " role="tablist">
                                <?php $noTabs=0; while( have_rows('tabs') ) { the_row(); ?>      
                                <?php if($noTabs == 0) { $active = "active text-black"; } ?>
                                    <li class="nav-item">
                                        <a class=" waves-light <?php echo $active; ?>" data-toggle="tab" href="#tab-panel-<?php echo $noTabs; ?>" role="tab"><?php echo trim(get_sub_field('titulo_del_tab_oferta')); ?></a>
                                    </li>         
                                  <?php $active = NULL; ?>
                                  <?php $noTabs++; } ?>
                                </ul>
                            </div>
                            <?php echo do_shortcode( '[icono nombre="'.sanitize_title("tab-derecha").'" clase="rightTabNav direccionales" estilo="style=\'display:none\'"][/icono]' ); ?>
                        </section>
                        <!-- Tab Secciones -->

                        <div class="tab-content pt-1 ">
                            <?php
                                $active = NULL;
                                $areas_de_estudio = NULL;
                                $panelContent=0;
                                $itemSlide = 0;
                            ?>
                <?php while( have_rows('tabs') ) { the_row(); $currentTab = trim(get_sub_field('titulo_del_tab_oferta'));?>
                  <?php if(have_rows("pestanas",$theID) ){ ?>
                      <?php if($panelContent == 0){ $active = "active"; } ?>
                            <div class="tab-pane fade in show  <?php echo $active; ?>" id="tab-panel-<?php echo $panelContent; ?>" role="tabpanel">
                            <div id="carouselUnitec-<?php echo $panelContent; ?>" class="carousel slide carousel-fade" data-ride="carouselUnitec<?php echo $panelContent; ?>">
                                <!--Slides-->
                                <div class="carousel-inner" role="listbox">
                                <!-- while para contar slides -->
                              <?php while( have_rows('pestanas') ) { the_row(); ?>
                                  <?php if($itemSlide == 0) { $active = "active"; } ?>
                                      <?php $campo_obtenido = get_sub_field('titulo');
                                            $a=get_term_meta($campo_obtenido->term_id); 
                                            //va por los terms asociados al post
                                            $id_post=$a['dhvc_woo_category_page_id'][0];
                                            $pagina = get_post($id_post); ?>

                                    <!--First slide-->
                                    <div class="carousel-item <?php echo $active; ?>">

                                        <!--Card-->
                                        <div class="card">

                                            <!--Title-->
                                            <h4 class="card-title"><?php echo $campo_obtenido->name; ?></h4>

                                            <!-- card image -->
                                            <div class="view overlay hm-white-slight">
                                                <?php 
                                                    switch (get_sub_field('imgen_destacada_o_nueva')) {
                                                        case 'imgnueva':
                                                            $value = get_sub_field('imagen_nueva');
                                                            $urlImg = $value['url'];
                                                            $altImg = $value['alt'];
                                                            
                                                            $term=$campo_obtenido->term_id;
                                                            $taxonomy='product_cat';

                                                            $args = array(
                                                                        'post_type'             => 'product',
                                                                        'post_status'           => 'publish',
                                                                        'ignore_sticky_posts'   => 1,
                                                                        'posts_per_page'        => '-1',
                                                                        'tax_query' => array(
                                                                            'relation' => 'AND',
                                                                            array(
                                                                                'taxonomy' => 'product_cat',
                                                                                'field'    => 'term_id',
                                                                                'terms'    => $campo_obtenido->term_id,
                                                                            ),
                                                                            array(
                                                                                'taxonomy' => 'product_cat',
                                                                                'field'    => 'term_id',
                                                                                'terms'    => $campo_obtenido->term_id,
                                                                            ),
                                                                        ),                                            
                                                                    );
                                                                    $product = query_posts($args);
                                                                    //Revisar el id correcto se tiene que separar en las categorias dos prepas
                                                                    $urlPage = get_permalink($product[0]->ID);
                                                           
                                                        break;
                                                        
                                                        case 'imgdestacada':
                                                            /*Obtenemos la imagen destacada horizontal del Filtro seleccionado
                                                              Recordando que esta imagen se obtiene del page template asignado
                                                              en la categoria de products
                                                            */
                                                              
                                                            $thumbID = get_field('imagen_destacada_horizontal', $pagina->ID);
                                                            $urlImg = $thumbID["url"];
                                                            $altImg = $thumbID["alt"];

                                                            /*$thumbID = get_post_thumbnail_id($pagina->ID);
                                                            $urlImg = wp_get_attachment_url($thumbID);
                                                            $altImg = get_post_meta($thumbID, '_wp_attachment_image_alt', true);*/
                                                            /*End Obtenemos la imagen destacada horizontal del Filtro seleccionado*/
                                                        break;
                                                    }
                                                ?>
                                                <!-- <amp-img
                                                        src="<?php echo $urlImg; ?>"
                                                        width="400"
                                                        height="265"
                                                        srcset="<?php echo $urlImg; ?> 400w, <?php echo $urlImg; ?> 400w"
                                                        sizes="(min-width: 400px) 92vw, 92vw">
                                                    <div fallback>"<?php echo $altImg; ?></div>
                                                </amp-img> -->
                                                <img alt="<?php echo $altImg; ?>" class="img-fluid lazy" data-src="<?php echo $urlImg; ?>">
                                                <!-- class="mask waves-effect waves-light" -->
                                                <!-- <a><div></div></a> -->
                                            </div>
                                            
                                            <!-- Validacion de No de Items en el carrusel -->
                                            <?php //if ( count( get_sub_field('imgen_destacada_o_nueva') ) > 1) { ?>
                                            <!--Button-->
                                            <a class="showarrow-<?php echo $panelContent; ?> d-none btn-floating btn-action">
                                                <?php echo do_shortcode( '[icono nombre="'.sanitize_title("ver-derecha").'"][/icono]' ); ?>
                                            </a>
                                            <?php //} ?>

                                            <!-- Aqui van los componentes list_icon, list, texto, cascade, footer, image, collapse -->

                                            <div class="card-block fix-card-block">            
                                            <!-- Verificar Tipo de Contenido -->
                                          <?php switch (get_sub_field('tipo_de_contenido')) {
                                                    //Colapsable para Pintar Areas De Interes
                                                    case 'filtrado': ?>

                                                    <div class="row">
                                                        <!-- card image -->
                                                        <br>
                                                        <!--/.Accordion wrapper-->
                                                        <div id="simpleListParent" class="col-12 p-0">           
                                                  <?php //echo "Card con filtrado<br>";
                                                        $categoria_padre=get_sub_field('filtro');
                                                        //echo "Categoria Padre:".$categoria_padre->name."<br>";
                                                        $term=$categoria_padre->term_id;
                                                        $taxonomy='product_cat';
                                                        // Id´s adm y ciencias
                                                        $areas_de_estudio= get_term_children( $term, $taxonomy );
                                                        $areas = NULL;

                                                        foreach ($areas_de_estudio as $key => $value) {
                                                            /*Ordenar Alfabeticamente*/
                                                            /*$nombre_areas_interes = get_term_by('id', $value, $taxonomy);  
                                                            $areas[$value] = $nombre_areas_interes->name;*/

                                                            /*Order By Custom Position BackOffice*/
                                                            $orden_areas_de_estudio = get_term_meta($value);
                                                            $areas[$orden_areas_de_estudio['order'][0]] = $value;
                                                        }

                                                        /*Ordenar Alfabeticamente*/
                                                        /*asort($areas);*/

                                                        /*Order By Custom Position BackOffice*/
                                                        ksort($areas);                        
                                                       
                                                        foreach ($areas as $key => $value) {
                                                            if($value!=NULL){    
                                                                //Nombres de adm y ciencias
                                                                /*Ordenar Alfabeticamente Cambiar value por key para ordemaniento alfabetico*/ 

                                                                /*Order By Custom Position BackOffice*/
                                                                $nombre_areas_interes = get_term_by('id', $value, $taxonomy);
                                                        ?>                         
                                                                <?php //Relacion entre $campo_obtenido->term_id y $value
                                                                    $args = array(
                                                                        'post_type'             => 'product',
                                                                        'order'      => 'ASC',               
                                                                        // 'orderby'           => 'meta_value_num',
                                                                        'post_status'           => 'publish',
                                                                        'ignore_sticky_posts'   => 1,
                                                                        'posts_per_page'        => '-1',
                                                                        'tax_query' => array(
                                                                            'relation' => 'AND',
                                                                            array(
                                                                                'taxonomy' => 'product_cat',
                                                                                'field'    => 'term_id',
                                                                                'terms'    => $campo_obtenido->term_id,
                                                                            ),
                                                                            array(
                                                                                'taxonomy' => 'product_cat',
                                                                                'field'    => 'term_id',
                                                                                'terms'    => $value,
                                                                            ),
                                                                        ),                                            
                                                                    );
                                                                    $products = query_posts($args); ?>
            
                                                        <?php if ( count($products) > 0 ) { ?>
                                                                <div class="simple-list table fix-top-space">
                                                                    <div type="button" class="list-item-container border-top-fix">
                                                                        <div class="item-icon list-icon">
                                                                            <?php echo do_shortcode( '[icono nombre="'.sanitize_title($nombre_areas_interes->description).'"][/icono]' ); ?>
                                                                        </div>
                                                                        <div class="item-description"><?php echo trim($nombre_areas_interes->name); ?></div>
                                                                    </div>
                                                                </div>
                                                                <div class="text-colapse collapse animated fadeIn"><!-- flipInX -->
                                                                    <div class="horario fix-horario">
                                                                    <!-- <ul> -->
                                                             <?php } ?> <!-- if condicion muestra categoria -->
                                                                    <!-- <ul> -->
                                                              <?php foreach ($products as $key => $carreraInteres) { 
                                                                    $preparatoria = strtolower( $carreraInteres->post_title );
                                                                    switch ( $preparatoria ) {
                                                                        case "":
                                                                           $label = "CONOCE MÁS";
                                                                        break;
                                                                        
                                                                        default:
                                                                            $label = trim($carreraInteres->post_title);
                                                                        break;
                                                                    }
                                                                ?>
                                                                <a data-gtm-tr="AcademicOffer" data-gtm-Tab="<?php echo $currentTab; ?>" data-gtm-offer-select="<?php echo $campo_obtenido->name; ?>" data-gtm-offer-option="<?php echo trim($nombre_areas_interes->name); ?>" data-gtm-carrera="<?php echo $label;?>" href="<?php echo get_permalink($carreraInteres->ID); ?>" class="link-simple-list"><label class="fix-horario-item horario-item"><label class="horario-hora"><?php echo $label;?>
                                                                </label></label></a><br />

                                                                <!-- <li><a href="<?php echo get_permalink($carreraInteres->ID); ?>" ><?php echo $carreraInteres->post_title; ?></a></li> -->
                                                              <?php } ?>
                                                                    <!-- </ul> --> 
                                                                <?php if ( count($products) > 0 ) { ?>
                                                                    <!-- </ul> -->
                                                                    </div>
                                                                </div>
                                                            <?php } ?>
                                                      <?php } ?><!-- foreach -->
                                                  <?php } ?><!-- if verifica producto -->             
                                                    
                                             <?php  break;

                                                     //Bullets para Prepa
                                                    default: ?>
                                                        <ul class="listop3">
                                                      <?php foreach (get_sub_field('tipo_lista') as $key => $bullet) { ?>
                                                                <li><?php echo do_shortcode( '[icono nombre="'.sanitize_title("bullet").'"][/icono]' ); ?> 
                                                                    <?php echo trim($bullet['listado']); ?>
                                                                </li><br />
                                                      <?php } ?>    
                                                        </ul>

                                                        <a data-gtm-tr="AcademicOffer" data-gtm-Tab="<?php echo $currentTab; ?>" data-gtm-offer-select="<?php echo $campo_obtenido->name; ?>" data-gtm-offer-option="Conoce Más" data-gtm-carrera="<?php echo $campo_obtenido->name; ?>" href="<?php echo $urlPage; ?>" id="btnListop3" class="linkSecundario" ><?php echo trim(get_sub_field('boton')); ?></a><br><br>
                                              <?php break;
                                                } ?>

                                            <?php 
                                                switch (get_sub_field('tipo_de_contenido')) {
                                                    case 'filtrado': ?>
                                                        </div>
                                                        <!--/.Accordion wrapper-->
                                                    </div>
                                                    <!-- row -->
                                              <?php break;
                                                    
                                                    default:                                    
                                                    break;
                                                }
                                            ?>
                                            </div>
                                            <!-- Card Block -->
                                        </div>
                                        <!--/.Card-->
                                    </div>
                                    <!--/.First slide-->                    
                              <?php $itemSlide++; ?>
                              <?php $active = NULL; ?>                    
                               <?php } ?>
                            <!-- End while para contar slides -->
                            </div><br>
                            <!--/.Slides-->

                            <!-- Validacion de No de Items en el carrusel -->
                            <?php if ($itemSlide > 1) { ?>
                            <ol class="carousel-indicators">
                                <?php $slide = 0; ?>
                                <?php if ($slide == 0) { $active = "active"; } ?>
                                    <?php for ($i=0; $i < $itemSlide; $i++) { ?>
                                            <li data-target="#carouselUnitec-<?php echo $slide; ?>" data-slide-to="<?php echo $i; ?>" class="<?php echo $active; ?>">></li>
                                        <?php 
                                            $active = NULL;
                                          } ?>                            
                            </ol>
                            <!--/.Indicators-->
                            <?php } ?>       
                            <input type="hidden" name="sliders-<?php echo $panelContent; ?>" id="sliders-<?php echo $panelContent; ?>" value="<?php echo $itemSlide; ?>">
                        </div>
                        <!--/.Carousel Wrapper-->        
                    </div>
                    <!-- #panel -->
                    <?php $itemSlide=0; ?>
                    <?php $panelContent++; ?>
                    <?php } ?>
                <?php  } ?>
                </div>
                <input type="hidden" name="panelContent" id="panelContent" value="<?php echo $panelContent; ?>">
<!-- ***************************-> Termina tu código ... -->
            </div><!-- col-md-12 -->
        </div><!-- /.Live preview-->
    </section><!-- SECTION-->
</main><!-- /.Main <layout-->