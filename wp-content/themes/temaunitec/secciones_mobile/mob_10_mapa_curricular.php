<div id="perfil"></div>
<?php
/**
 * WP Post Template: mob_10_mapa_curricular
 */
?>

<?php
    /*Se insertan los plugins que necesita la seccion tal y como se llama el archivo*/
    //load_script( $pluginsNecesarios = array('flipcard') );
    /*End Se insertan los plugins que necesita la seccion tal y como se llama el archivo*/
    $noTabs=NULL;
    $descarga = NULL;
    $duracion_por_oferta = NULL;
    $muestraDescarga = '';

global $woocommerce, $product, $post;

//Se obtienen los años Asignados por Carrera 
if(have_rows("tabs",$theID) ) { 
    while( have_rows('tabs') ) { the_row();
        while( have_rows('tipo_de_contenido') ) { the_row();            
            $opcion = get_row_layout('tipo_de_contenido');

            switch ($opcion) {
                case 'mapa_curricular':
                    $field = get_sub_field_object('no_anos');
                    $fieldTipo = get_sub_field_object('desk_plan_de_estudios');
                    $valueTipo = $fieldTipo['value'];
                    $labelTipo = $fieldTipo['choices'][ $valueTipo ];
                    $value = $field['value'];
                    $label = $field['choices'][ $value ]."<br />";
                    $opc_noAnos[$valueTipo][$value] = $label;
                    $opc_tipoPlan[$valueTipo] = $labelTipo;
                    $duracion_por_oferta=get_sub_field('duracion_por_oferta_academica');
                    $plan = "plan-".get_sub_field('desk_plan_de_estudios');
                break;
                default:
                break;
            }
            
        }
    }
}

$totalDuracion = ( count($opc_noAnos) > 1 ) ? array_keys($opc_noAnos)[1]: array_keys($opc_noAnos)[0];

/*Se obtiene la url asignada en los atributos del folleto para compartir en redes Sociales*/
foreach ($product->get_attributes() as $key => $atributo) {    
    $atributosDelProducto = wp_get_post_terms( $product->id, $atributo["name"] );
    foreach ($atributosDelProducto as $key => $value) {
        switch ($value->taxonomy) {
            case 'pa_liga-a-folleto':
                $descarga['plan_presencial'] = $value->name;
            break;

             case 'pa_liga-a-folleto-ejecutiva':
                $descarga['plan_ejecutiva'] = $value->name;
            break;

             case 'pa_liga-a-folleto-enlinea':
                $descarga['plan_enlinea'] = $value->name;
            break;

            case 'pa_liga-a-folleto-pres-linea':
                $descarga['plan_presencial_enlinea'] = $value->name;
            break;
            
            default:
                /*Colocar por defecto presencial para que imprima el total de Cuatrimestres*/
                //$descarga['plan_presencial'] = "Sin_Folleto";
            break;
        }
    }    
}

if ( $descarga == NULL ) {
    $descarga['plan_presencial'] = 'Sin_Folleto';
    $muestraDescarga = 'd-none';
}

$descarga=json_encode($descarga);
/*Se obtiene la url asignada en los atributos del folleto para compartir en redes Sociales*/

?>
<script>
    var descarga = <?php echo $descarga; ?>;
</script> 

<main class="col-12 wow fadeInRight">
<input type="hidden" name="actual" id="actual" value="<?php echo $totalDuracion; ?>">
<input type="hidden" name="h_duracionOferta" id="h_duracionOferta" value="<?php echo $duracion_por_oferta; ?>">
<input type="hidden" name="h_duracionTitle" id="h_duracionTitle" value="Duración: ">

    <!-- SECTION-->
    <section id="elegant-card">
        <!-- Live preview-->
        <div class="row">
            <div class="col-md-12">
<!-- ***************************-> Inicia tu código ... -->

                <!-- Tab Navegacion -->
                <section  id="tabNav">
                    <?php echo do_shortcode( '[icono nombre="'.sanitize_title("ver-izquierda").'" clase="leftTabNav direccionales" estilo="style=\'display:none\'"][/icono]' ); ?>
                    <div class="tabs-wrapper">
                        <ul class="nav classic-tabs" role="tablist">
                        <?php if(have_rows("tabs",$theID) ) { 
                                $noTabs=0;  

                                while( have_rows('tabs') ) { the_row(); 
                                    if($noTabs == 0){ $active = "active text-black"; } 
                        ?>       
                            <li class="nav-item"><!-- waves-effect waves-light -->
                                <a class=" waves-light <?php echo $active; ?> " data-toggle="tab" href="#tab-panel-mapa-<?php echo $noTabs; ?>" role="tab" data-gtm-tr="Tab" data-gtm-tab="<?php echo get_sub_field('titulo_del_tab'); ?>"><?php echo get_sub_field('titulo_del_tab'); ?></a>
                            </li>
                            <?php   $noTabs++; 
                                    $active = NULL; 
                                } 
                            } 
                        ?>
                        </ul>
                    </div>
                    <?php echo do_shortcode( '[icono nombre="'.sanitize_title("ver-derecha").'" clase="rightTabNav direccionales" estilo="style=\'display:none\'"][/icono]' ); ?>
                </section>

                <!-- Tab panels -->
                <div class="tab-content lighten-4">
                  <?php if(have_rows("tabs",$theID) ) { 
                            $panelContent = 0; 

                            while( have_rows('tabs') ) { the_row(); 
                                $active = NULL; 
                                $itemSlide = 0;  /* Total de Cards en el Carrusel */
                                $slide = 0; /* Contador de indicador */
                            
                                if($panelContent == 0) { $active = "active"; } 
                    ?>
            
                    <div class="tab-pane fade in show <?php echo $active; ?>" id="tab-panel-mapa-<?php echo $panelContent; ?>" role="tabpanel">

                          <?php $cnt = 0;
                                while( have_rows('tipo_de_contenido') ) { the_row(); 
                                    //Obtener opcion del flexible content
                                    $opcion = get_row_layout('tipo_de_contenido'); 

                                    if($cnt ==0){ // Comprobar solo una Cantidad

                                        switch ($opcion) {
                                            case 'mapa_curricular': 
                                                $carrouselInner = "mb-1 bsn"; 
                            ?>

                            <div class="card ovf-hidden mb-2 contenido-card">
                 <h4 class="card-title card borde-card"><?php echo get_sub_field('titulo_card_mapa'); ?></h4>

                        <!--Rotating card-->
                        <!-- card-wrapper mt-0 mb-0" ficha-width="600" -->
                        <!-- <div class="card-wrapper mt-0 mb-0" ficha-width="600">
                            <div id="plan-de-estudios" class="card-rotating effect__click"> -->
                                <!--Front Side-->
                                <!-- <div class="face front adelante"> -->  <!-- style="height:auto;" -->

                                <div class="fix-card-block card-block c-b-mapa-curricular card-body">
                                    <div class="row">
                                        <div class="col-12">

                                        
                                <?php                                
                                    /*Se obtienen valores del Select ACF*/
                                    $select = get_sub_field_object( 'no_anos' );

                                    /*foreach ($select['choices'] as $key => $opc_no_anos) {
                                        $opc_noAnos[$key] = $opc_no_anos;
                                    }*/

                                    /*print_r($opc_noAnos);*/
                                    
                                    // $filtrosAsignados[0] = "Todo";
                                    
                                    while( have_rows('cuatrimestre') ) { the_row(); 
                                        while( have_rows('materias') ) { the_row(); 
                                            $materias = get_sub_field_object( 'tipo_de_materia' );
                                            $value = $materias['value'];
                                            if($materias['choices'][$value] !== 'Sin Filtro' ){
                                                $filtrosAsignados[$value] = $materias['choices'][$value];
                                            }
                                            
                                            // echo $materias['choices'][$value];
                                        }
                                    }                                                           
                                ?>

                    <!-- Comprobación si trae Filtro -->
                    <?php $claseDivFiltro = ( !in_array("Sin Filtro", $filtrosAsignados) )? "": "d-flex"; ?>
                    <!-- Comprobación si trae Filtro -->

                                <!-- Seccion de dropdowns -->
                                <div class="">
                                    <section class="prodColapsables mt-1">
                                        <span class="dropdown">
                                            <!-- Trigger -->
                                            <div id="prodTipoPlan" class="drop-view" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" style="cursor: pointer">
                                                <span class="drop-text">
                                                    <label id="nombrePlan"><?php echo array_values($opc_tipoPlan)[0]; ?></label>
                                                    <label id="descModalidad"></label>
                                                    <input type="hidden" id="h_plan" value="<?php echo array_keys($opc_tipoPlan)[0]; ?>">
                                                </span>
                                                <?php echo do_shortcode( '[icono nombre="'.sanitize_title("ver-derecha").'"][/icono]' ); ?>
                                            </div>
                                            <!-- Menu -->
                                            <div class="dropdown-menu dropdown-primary">
                                            <?php
                                                    foreach ($opc_tipoPlan as $key => $opc_tipo_plan) { ?>
                                                <a class="dropdown-item" onclick="filtroTipoPlan( '<?php echo $key ?>', '<?php echo $opc_tipo_plan; ?>' );"><?php echo $opc_tipo_plan; ?></a>
                                            <?php   }
                                            ?>                                                
                                            </div>
                                        </span>
                                        <span class="dropdown d-none">
                                        </span>
                                    </section>
                                </div>
                                <div class="">
                                    <section class="prodColapsables mt-1">
                                        <span class="dropdown">
                                            <!-- Trigger -->
                                            <div id="prodCampus" class="drop-view" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" style="cursor: pointer">
                                                <span class="drop-text">
                                                    <label id="ciclos"><?php echo array_values(array_values($opc_noAnos)[0])[0]; ?></label>
                                                    <label id="duracionPor"></label>
                                                    <input type="hidden" id="h_duracion" value="<?php echo array_keys(array_values($opc_noAnos)[0])[0]; ?>">
                                                </span>
                                                <?php echo do_shortcode( '[icono nombre="'.sanitize_title("ver-derecha").'"][/icono]' ); ?>
                                            </div>
                                            <!-- Menu -->
                                            <div id="prodCampusDropdown" class="dropdown-menu dropdown-primary">
                                            <?php
                                                foreach ($opc_tipoPlan as $keyPlan => $opc_tipo_planes) {
                                                    foreach ($opc_noAnos[$keyPlan] as $key => $opc_no_anos) { ?>
                                                <a class="dropdown-item destacar plan-<?php echo $keyPlan; ?>" onclick="filtroNoAnos( '<?php echo $key ?>', '<?php echo $opc_no_anos; ?>', '<?php echo $keyPlan; ?>' );"><?php echo $opc_no_anos; ?></a>
                                            <?php   } }
                                            ?>                                                
                                            </div>
                                        </span>

                                        <span class="dropdown d-none <?php //echo $claseDivFiltro; ?>">
                                            <!-- Trigger -->
                                            <span id="prodCampus" class="drop-view" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                <span class="drop-text">
                                                    <label>Filtrar</label>
                                                    <label id="filtrarPor"><?php echo $filtrosAsignados[0]; ?></label>
                                                </span>
                                                <?php echo do_shortcode( '[icono nombre="'.sanitize_title("ver-derecha").'"][/icono]' ); ?>
                                            </span>
                                            <!-- Menu -->
                                            <div class="dropdown-menu dropdown-menu-right dropdown-primary">
                                            <?php   
                                                    /*$a = 1;
                                                    foreach ($materias['choices'] as $key => $opc_materias) { 
                                                        $opc_tipo_materias[$key] = $opc_materias;
                                                        $a++;
                                                    }*/

                                                    foreach ($filtrosAsignados as $key => $opc_materias) { ?>
                                                <a class="dropdown-item" onclick="filtroMaterias( '<?php echo $key ?>', '<?php echo $opc_materias; ?>' );"><?php echo $opc_materias; ?></a>
                                            <?php  
                                                    }
                                            ?>                                                
                                            </div>
                                        </span>
                                    </section>
                                </div>
                                <!-- Queda pendiente cerrar div simulador y etiqueta section --> 
                                                                          
                                  <?php     break;
                                        
                                            default: break;
                                        } //End Switch
                                    }//End if $cnt

                                    $cnt++;
                                }//End while contenido para asignar tabla materias?>

                        <!-- Carousel Wrapper -->
                        <div id="carouselUnitec-mapa-<?php echo $panelContent; ?>" class="carousel slide carousel-fade" data-ride="carouselUnitecMapa<?php echo $panelContent; ?>">
            
                            <!-- while para contar slides -->            
                            <?php while( have_rows('tipo_de_contenido') ) { the_row(); 
                                    //Obtener opcion del flexible content
                                    $opcion = get_row_layout('tipo_de_contenido');

                                    switch ($opcion) {
                                        case 'mapa_curricular': $class_div = "mb-1 bsn"; break;
                                        default: $class_div = NULL; break;
                                    }

                                    if($itemSlide == 0) { $active = "active"; ?>
                            <!-- Slides -->
                            <div class="carousel-inner <?php echo $class_div; ?>" role="listbox">
                            <?php   }   ?>

                            <?php   switch ($opcion) {
                                        case 'contenido':
                                                $titulo_card = get_sub_field('titulo_card'); 
                                                $imagen = get_sub_field('imagen');
                                                $lista = get_sub_field('tipo_lista');
                            ?>
                                <!--First slide-->
                                <div class="carousel-item <?php echo $active; ?>">                                 
                                    <!--Card-->
                                    <div class="card">
                                        <!--Title-->
                                        <h4 class="card-title"><?php echo get_sub_field('titulo_card'); ?></h4>
                                        <!-- card image -->
                                        <div class="view overlay hm-white-slight">
                                            <img alt="<?php echo $imagen['alt'] ?>" class="img-fluid lazy" data-src="<?php echo $imagen['url']; ?>">
                                                <a><!-- <div class="mask waves-effect waves-light"></div> --></a>
                                        </div>

                                        <!--Button-->
                                        <a class="showarrow-<?php echo $panelContent; ?> d-none btn-floating btn-action"><?php echo do_shortcode( '[icono nombre="'.sanitize_title("ver-derecha").'"][/icono]' ); ?></a>
                                        <!-- Aqui van los componentes list_icon, list, texto, cascade, footer, image, collapse -->

                                        <div class="card-block">            
                                        <!-- Verificar Tipo de Contenido -->
                                            <ul class="listop3">
                                            <?php foreach ($lista as $key => $bullet) { ?>
                                                <li><?php echo do_shortcode( '[icono nombre="'.sanitize_title("bullet").'"][/icono]' ); ?> 
                                                    <?php echo $bullet['listado']; ?>
                                                </li><br />
                                            <?php } ?>    
                                            </ul>
                                        </div>
                                        <!-- Card Block -->
                                    </div>
                                    <!--/.Card-->
                                </div>
                                <!--/.First slide-->
                                    <?php   $active = NULL; 
                                            $itemSlide++; 
                                        break; 

                                        case 'mapa_curricular':   
                                                /*Diferencias los anos para la carrera*/
                                                /*echo get_sub_field('no_anos');
*/                                                /*$itemSlide = 0;*/                                             
                                                $titulo_card = get_sub_field('titulo_card'); 
                                                $imagen = get_sub_field('imagen');
                                                $lista = get_sub_field('tipo_lista');
                                                $duracion = "duracion-".get_sub_field('no_anos');
                                                $plan = "plan-".get_sub_field('desk_plan_de_estudios');
                                                //$descarga = get_sub_field('link_descarga');

                                                while( have_rows('cuatrimestre') ) { the_row(); 
                                    ?>
                                <!--First slide-->
                                <div class="carousel-item <?php echo $active; ?> <?php echo $duracion; ?> <?php echo $plan; ?> mapa-curricular">
                                    <!-- <label class="ejemplo-<?php echo $itemSlide; ?>"></label> -->

                                    <!-- Se asigna la misma clase al item del carrousel para aplicar filtro -->
                                    <?php $arrDuracion[$itemSlide] = $duracion; 
                                          $arrPlan[$itemSlide] = $plan;  ?>
                                    <!-- Se obtiene el tipo de curso Semestre Cuatrimestre Trimestre -->
                                    <?php $ciclo[] = get_sub_field('nombre_cuatrimestre'); ?>

                                    <div class="col-12 p-0">
                                        <div class="row">
                                            <h3 class="titleMC col-12 m-0"><?php echo get_sub_field('nombre_cuatrimestre'); ?></h3>
                                            <span class="descMC col-12 my-2"><?php echo get_sub_field( 'descripcion_del_cuatrimestre');?></span>
                                        </div>
                                        <input type="hidden" id="ciclo" name="ciclo" value="<?php echo $ciclo[0]; ?>">

                                        <?php 
                                            while( have_rows('materias') ) { the_row(); 
                                                $materias = get_sub_field_object( 'tipo_de_materia' );
                                                $value = $materias['value'];
                                                $label = $materias['choices'][ $value ];
                                                $Color = explode("_", $value);
                                                if($materias['choices'][$value] !== 'Sin Filtro' ){
                                                    $filtrosCiclos[] = array($label,$Color[1]);
                                                    $arrLabel[$Color[1]] = $label;
                                                }                                                
                                            } 
                                        ?>

                                        <?php /*Fix Categorias By SRP 11-07-2019*/ ?>

                                        <div class="esquema d-flex <?php echo $claseDivFiltro; ?>">
                                            <?php 
                                                unset($filtrosAsignados[0]); 
                                                $cntColores = 0;
                                                switch ( count($filtrosAsignados) ) {
                                                    case '3': $claseDiv ="col-4"; break;
                                                    case '4': $claseDiv ="col-3"; break;
                                                    default:  $claseDiv ="col-6"; break;
                                                }
                                                /*End Se obtienen los colores asignados en el backend*/
                                                // print_r($filtrosCiclos);
                                        foreach ($arrLabel as $foo => $arrLabelF) {
                                            echo '<div class="'.$claseDiv.' col-6 indi p-0">
                                                <label class="filtro-indicadores d-flex"><div class="indicador '.$foo.'">.</div>&nbsp;'.$arrLabelF.'</label>';
                                            echo '</div>';
                                           
                                        }

                                            unset($arrLabel);
                                        /*Fix Categorias By SRP 11-07-2019*/
                                        
                                                foreach ($filtrosCiclos as $key => $opc_materias) { 
                                            ?>
                                            <!-- <div class="<?php //echo $claseDiv; ?> col-6 indi p-0">
                                                <label class="filtro-indicadores d-flex"><div class="indicador <?php echo $opc_materias[1]; ?>">.</div>&nbsp;<?php echo $opc_materias[0]; ?></label>
                                            </div> -->
                                            <?php $cntColores++; } unset($filtrosCiclos); ?>

                                            <!-- <div class="<?php //echo $claseDiv; ?> col-6 indi">
                                                <label class="filtro-indicadores"><div class="indicador rojo">.</div>&nbsp;Materias Teóricas</label>
                                            </div>
                                            <div class="<?php //echo $claseDiv; ?> col-6 indi">
                                                <label class="filtro-indicadores"><div class="indicador verde">.</div>&nbsp;Materias Prácticas</label>
                                            </div> -->
                                        </div>

                                        <!-- <div class="row m-0"> -->
                                                <?php         
                                                    $i=0;                                       
                                                    $i=0;                                       
                                                    while( have_rows('materias') ) { the_row();
                                                        $res = $i%2;
                                                        if ($res == 0) {
                                                            echo '<div class="row m-0">';
                                                        }
                                                        /*switch (get_sub_field('tipo_de_materia')) {
                                                            case 'mt': $class = "rojo"; break;
                                                            default: $class = "verde"; break;
                                                        }*/ 

                                                        /*echo get_sub_field('tipo_de_materia');*/

                                                        $class = explode("_", get_sub_field('tipo_de_materia') );
                                                ?>
                                                        <div class="btn-materia <?php echo $class[1]; ?>"><?php echo get_sub_field('nombre_materia'); ?></div>
                                                        <!-- <div class="btn-materia <?php //echo $class; ?>"><a href="#"><?php //echo get_sub_field('nombre_materia'); ?></a></div> -->
                                            <?php   
                                                    if ($res != 0) {
                                                            echo '</div>';
                                                        }
                                                    $i++;
                                                    }
                                                    if ($i%2 != 0) {
                                                        echo '</div>';
                                                    }
                                            ?>

                                        <!-- </div> -->                                      
                                    </div>                                    
                                </div>
                                <!-- Termina primer slide -->
                                    <?php           $active = NULL; 
                                                    $itemSlide++;
                                                    /*echo $itemSlide;*/ 
                                                } /*End while cuatrimestre*/
                                    ?>              
                                                      
                                   <?php break;
                                        default:                       
                                        break;
                                    } ?><!-- switch Contenido -->
                                    <?php //echo get_sub_field('no_anos'); ?>
                          <?php } ?><!-- End while para contar slides -->
                           
                            <?php 
                    switch ($opcion) {
                        case 'mapa_curricular': ?>
                            </div><!-- carousel-inner -->
                            
                            <div class="row">
                                <div class="col-12 icons-mapa">
                                    <a class="rotate-btn" data-card="plan-de-estudios"><?php echo do_shortcode( '[icono nombre="'.sanitize_title("compartir").'" clase="pull-right descarga-plan activator margen-btn-share"][/icono]' ); ?></a>
                                    <a data-gtm-tr="downloadFiles" data-gtm-pos="folleto-de-carrera" href="<?php echo $descarga; ?>" target="_blank" id="descarga_folleto_mapa"><?php echo do_shortcode( '[icono nombre="'.sanitize_title("descargar").'" clase="pull-right descarga-plan '.$muestraDescarga.' "][/icono]' ); ?></a>
                                </div>
                            </div>
                            
                            <!-- Inician los indicadores -->
                            <ol class="carousel-indicators">
                                <?php $slide = 0; ?>
                                <?php $active = NULL; ?>                            
                                    <?php for ($i=0; $i < $itemSlide; $i++) { ?>
                                    <?php if ($slide == 0) { $active = "active"; } ?>
                                <li data-target="#carouselUnitec-mapa-<?php echo $panelContent; ?>" data-slide-to="<?php echo $i; ?>" class="<?php echo $active; ?> <?php echo $arrDuracion[$i]; ?> <?php echo $arrPlan[$i]; ?> mapa-curricular">></li>
                                        <?php $active = NULL; ?>
                                        <?php $slide++; ?>                                 
                                    <?php } ?>                                           
                            </ol>
                            <!-- Finalizan los indicadores -->
                            </div>
                            </div>
                                    </div>
                                </div>
                                <!-- card-block c-b-mapa-curricular -->
                                
                                <!-- </div> -->
                                <!-- face front -->

                                <!--Back Side-->
                                <div class="card-reveal">
                                        <?php echo do_shortcode('[share cerrar="plan-de-estudios" pagina="'.get_permalink().'" seccion="Plan de Estudios: '.$post->post_title.'"][/share]');?>
                                </div>
                                <!--/.Back Side-->

                            <!-- </div>
                                                    </div> -->



                            
                        </div><!-- Carousel Wrapper -->
                  <?php break;

                        case 'contenido': ?>
                            </div><br>
                            <!-- Slides -->
                            <?php if ($itemSlide > 1) { ?>
                            <ol class="carousel-indicators">
                                <?php $slide = 0; ?>
                                <?php $active = NULL; ?>                            
                                    <?php for ($i=0; $i < $itemSlide; $i++) { ?>
                                    <?php if ($slide == 0) { $active = "active"; } ?>
                                <li data-target="#carouselUnitec-mapa-<?php echo $panelContent; ?>" data-slide-to="<?php echo $i; ?>" class="<?php echo $active; ?>">></li>
                                        <?php $active = NULL; ?>
                                        <?php $slide++; ?>                                 
                                    <?php } ?>                                           
                            </ol>
                            <?php } ?>
                            <!-- Indicators -->
                            <input type="hidden" name="sliders-<?php echo $panelContent; ?>" id="sliders-<?php echo $panelContent; ?>" value="<?php echo $itemSlide; ?>">
                        </div><!-- Carousel Wrapper -->                
                  <?php break;
                        
                        default: break;
                    }
                ?>                            
                                        
                <?php $panelContent++; ?>
                </div>
                
                    <!-- Termina Tab -->       
       <?php }  ?>
    <?php }   ?>         
    <input type="hidden" name="panelContent" id="panelContent" value="<?php echo $panelContent; ?>">       
<!-- ***************************-> Termina tu código ... -->
                </div><!-- tab-content lighten-4 -->
            </div><!-- col-md-12 -->
        </div><!-- /.Live preview-->
    </section><!-- SECTION-->
</main><!-- /.Main <layout-->