<div id="contacto"></div>
<?php
/**
 * WP Post Template: mob_16_contacto
 */
?>

<?php
    $icono = NULL;
    $class_div = NULL;
    $contenido = NULL;
    $enlaceInicio = NULL;
    $enlaceFin = NULL;
    $telefonos = NULL;

    $contacto_telefonico = get_field('contacto_telefonico');
    $horarios = get_field('horarios');

    $dias_semana = array("Domingo", "Lunes", "Martes", "Miércoles", "Jueves", "Viernes", "Sábado", "Domingo");
    /*$dias_semana = array_keys( $horarios );*/
    /*array_push( $dias_semana, $dias_semana[0]);*/    

    $horarios_semana = array_values( $horarios );
    array_push( $horarios_semana, $horarios_semana[0] ); 

    date_default_timezone_set("America/Mexico_City");
    $dia = date('w');

    foreach ($contacto_telefonico['telefonos'] as $key => $value) {
        foreach ($value as $key => $numeros_telefonicos) {
            $telefonos[] = $numeros_telefonicos;
        }
    }
?>

<script type="text/javascript">
    var diasSemana = new Array("Domingo","Lunes","Martes","Miércoles","Jueves","Viernes","Sábado");
    var f = new Date();
</script>

<?php if( count($dias_semana > 0) ) { ?>
<main class="col-12 mt-0"><!-- /.Main <layout-->
    <section id="elegant-card"><!-- SECTION-->
        <div class="row"><!-- Live preview-->
            <div class="col-md-12"><!-- col-md-12 -->
<!-- ***************************-> Inicia tu código ... -->
                <div class="card mb-1">
                    <h4 class="card-title"><?php echo get_field('titulo_contacto', $theID); ?></h4>
                    <?php 
                        $imagen = get_field('imagen', $theID);
                    ?>

                    <div class="view overlay hm-white-slight">
                        <img alt="<?php echo $imagen['alt'] ?>" class="img-fluid lazy" data-src="<?php echo $imagen['url'] ?>">
                        <!-- class="mask waves-effect waves-light" -->
                        <a><div></div></a>
                    </div>
                    <div class="card-block pt-0">            
                        <div class="row"><br>
                            <div class="col-12 p-0">

                                <?php
                                    $direccion_campus = get_field('direccion_del_campus' ,$theID);
                                    
                                    if( $direccion_campus != NULL && $direccion_campus !="" && strlen($direccion_campus) > 1 ) { 
                                ?>
                                <div class="simple-list">
                                    <div type="button" class="list-item-container border-top-fix">
                                        <div class="item-icon"><a href="//maps.google.es/?q=UNITEC, <?php echo get_field('direccion_del_campus' ,$theID); ?>" target="_blank" style="color:#757575;"><?php echo do_shortcode( '[icono nombre="'.sanitize_title( get_field('icono_direccion_campus') ).'"][/icono]' ); ?></a></div>
                                        <div class="item-description"><a href="//maps.google.es/?q=UNITEC, <?php echo get_field('direccion_del_campus' ,$theID); ?>" target="_blank"><?php echo get_field('direccion_del_campus' ,$theID); ?></a></div>
                                    </div>
                                </div>
                                <?php } ?>

                                <?php if ( count($telefonos) > 0 ) { ?>
                                <div class="simple-list">
                                    <div type="button" class="list-item-container border-top-fix">
            
                                        <?php if ( count($telefonos) === 1 ) {
                                            $enlaceInicio = '<a data-gtm-tr="call" data-gtm-pos="seccion-contactanos" data-gtm-org="" href="tel:'.$telefonos[0].'" class="link-simple-list">';
                                            $enlaceFin = '</a>';
                                        } ?>

                                        <div class="flex-item item-icon list-icon"><?php echo $enlaceInicio; ?><?php echo do_shortcode( '[icono nombre="'.sanitize_title( get_field('icono_contacto_telefonico') ).'"][/icono]' ).$enlaceFin; ?></div>
                                        <div class="flex-item item-description"><?php echo $enlaceInicio; ?><?php echo $telefonos[0].$enlaceFin; ?></div>

                                        <?php if ( count($telefonos) > 1 ) { ?>
                                            <div class="arrow"><?php echo do_shortcode( '[icono nombre="'.sanitize_title("ver-abajo").'"][/icono]' ); ?></div>
                                        <?php } ?>                                        
                                    </div>
                                </div>
                                
                                <?php if ( count( $telefonos) > 1 ) { ?>
                                <div class="text-colapse collapse animated flipInX">
                                    <div class="fix-horario-campus horario">
                                        <?php 
                                            foreach ($telefonos as $key => $value) {
                                        ?>
                                        <a href="tel:<?php echo $value; ?>" class="link-simple-list" data-gtm-tr="pushCall" data-gtm-accion="Intención"><label class="horario-item"><label> <?php echo $value; ?></label><label class="horario-hora"></label></label></a>
                                            <?php
                                            }
                                        ?>
                                    </div>
                                </div>
                                <?php } ?>

                                <?php } //Valida si existen no telefónicos ?>

                                <?php if( !empty( $horarios['domingo'] ) ) { 
                                //if ( count($horarios) > 0 ) { ?>
                                <div class="simple-list">
                                    <div type="button" class="list-item-container border-top-fix">
                                        <div class="flex-item item-icon list-icon"><?php echo do_shortcode( '[icono nombre="'.sanitize_title( get_field('icono_horarios') ).'"][/icono]' ); ?></div>
                                        <div class="flex-item item-description">
                                            <div class="row fix-horarios-button">
                                                <span class="col-5 horarios-pre d-flex flex-column justify-content-center"><script type="text/javascript">document.write( diasSemana[f.getDay()] );</script><?php //echo ucwords($dias_semana[$dia]); ?></span>
                                                <span class="col-7 horarios-pre d-flex flex-column justify-content-center"><?php echo $horarios_semana[$dia]; ?></span>
                                            </div>
                                            <!-- <pre class="horarios-pre"><?php echo ucwords($dias_semana[$dia])."            ".$horarios_semana[$dia]; ?></pre> -->
                                        </div>
                                        <div class="arrow"><?php echo do_shortcode( '[icono nombre="'.sanitize_title("ver-abajo").'"][/icono]' ); ?></div>
                                    </div>
                                </div>
                                <div class="text-colapse collapse animated flipInX">
                                    <div class="fix-horario-campus horario">
                                        <?php 
                                            for ($i = 1; $i < count($dias_semana); $i++) {
                                        ?>
                                        <label class="horario-item"><label class="horario-dia"> <?php echo ucwords($dias_semana[$i]); ?></label><label class="horario-hora"><?php echo $horarios_semana[$i]; ?></label></label>
                                            <?php
                                            }
                                        ?>
                                    </div>
                                </div>
                                <?php } ?>

                                <?php //if ( !empty($contacto_telefonico['whatsapp']) ) { ?>
                                <!-- <div class="simple-list">
                                    <div type="button" class="list-item-container border-top-fix">
                                        <div class="item-icon">
                                            <a href="https://clxt.ch/unitecg" data-gtm-tr="whats" data-gtm-pos="seccion-contacto | whats" target="_blank"><?php echo do_shortcode( '[icono nombre="'.sanitize_title( get_field('icono_whatsapp') ).'"][/icono]' ); ?></a>
                                        </div> -->

                                        <!-- whatsapp://send?phone= -->
                                        <!-- <div class="item-description"><a href="whatsapp://send?phone=<?php echo $contacto_telefonico['whatsapp']; ?>" target="_blank"><?php echo $contacto_telefonico['whatsapp']; ?></a></div> -->

                                        <!-- <div class="item-description"><a href="https://clxt.ch/unitecg" data-gtm-tr="whats" data-gtm-pos="seccion-contacto | whats" target="_blank">Escríbenos por Whatsapp</a></div>
                                    </div>
                                </div> -->
                                <?php //} ?>

                                <?php ///if ( !empty(get_field('titulo_chat', $theID)) ) { ?>
                                <!-- <div class="simple-list">
                                    <div type="button" class="list-item-container border-top-fix">
                                        <div class="item-icon"><a href="<?php echo get_field('link_chat', $theID); ?>" target="_blank" data-gtm-tr="chat" data-gtm-pos="seccion-contacto | chat" style="color:#757575;"><?php echo do_shortcode( '[icono nombre="'.sanitize_title( get_field('icono_chat') ).'"][/icono]' ); ?></a></div>
                                        <div class="item-description"><a href="<?php echo get_field('link_chat', $theID); ?>" data-gtm-tr="chat" data-gtm-pos="seccion-contacto | chat" target="_blank"><?php echo get_field('titulo_chat', $theID); ?></a></div>
                                    </div>
                                </div> -->
                                <?php ///} ?>

                                <!-- <div class="simple-list">
                                    <div type="button" class="list-item-container border-top-fix">
                                        <div class="item-icon"><a onclick="openModal('#modal_frm_app')" data-gtm-tr="frmTradicional" data-gtm-pos="seccion-contacto | sobre"><?php echo do_shortcode( '[icono nombre="'.sanitize_title( get_field('icono_contacto') ).'"][/icono]' ); ?></a></div>
                                        <div class="item-description"><a onclick="openModal('#modal_frm_app')" data-gtm-tr="frmTradicional" data-gtm-pos="seccion-contacto | sobre"><?php echo get_field('titulo_mail', $theID); ?></a></div>
                                    </div>
                                </div> -->


                                <?php

                                $contenidoRedes = get_field( 'desk_contactoSociales', $theID );
                                /*Es importante que contenga la palabra Unitec para detectar que es un Liga*/
                                foreach ($contenidoRedes as $key => $dato) {
                                    $link = ( strpos( strtolower( trim( $dato['desk_urlContacto'] ) ), "unitec" )  ) ? 'href=' : 'onclick=';
                                    $target = ( strpos( strtolower( trim( $dato['desk_urlContacto'] ) ), "unitec" ) ) ? "target='_blank'" : "";
                                    if ( strpos( strtolower( trim( $dato['desk_urlContacto'] ) ), "#modal_frm_app") ) {
                                        $trackeoLocation =  'data-solicitud-location="Middle"';
                                    } else if( strpos( strtolower( trim( $dato['desk_urlContacto'] ) ), "#modal-whatsapp")) {
                                        $trackeoLocation =  'data-gtm-tr="evWhats" data-gtm-accion="intención" data-gtm-seccion="contacto" data-gtm-depto=""';
                                    } else if( strpos( strtolower( trim( $dato['desk_urlContacto'] ) ), "zopim.livechat.window.show()")) {
                                        $trackeoLocation =  'data-gtm-tr="chat" data-gtm-pos="intencion" data-gtm-location="Secci&oacute;n Contacto" data-gtm-etiqueta="con formulario"';
                                    } else {
                                        $trackeoLocation = '';
                                    }

                                ?>
                                <div class="simple-list">
                                    <div type="button" class="list-item-container border-top-fix">
                                        <div class="flex-item item-icon list-icon"><a class="d-flex" <?php echo $link.' " '.trim( $dato['desk_urlContacto'] ).' " '.$target ?> <?php echo $trackeoLocation ?> ><?php echo do_shortcode( '[icono nombre="'.sanitize_title( trim( $dato['desk_iconoContacto'] ) ).'" clase="list-icon icon-size"][/icono]' ); ?></a></div>
                                        <div class="item-description"><a <?php echo $link.' " '.$dato['desk_urlContacto'].' " '.$target ?> <?php echo $trackeoLocation ?> ><?php echo $dato['desk_descripcionContacto']; ?></a></div>
                                    </div>
                                </div>
                            
                            <?php } ?>






                            </div>
                        </div>
                    </div>
                </div>
<!-- ***************************-> Termina tu código ... -->
            </div><!-- col-md-12 -->
        </div><!-- /.Live preview-->
    </section><!-- SECTION-->
</main><!-- /.Main <layout-->
<?php } ?>