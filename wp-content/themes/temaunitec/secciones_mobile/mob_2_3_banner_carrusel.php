<?php
/**
 * WP Post Template: mob_2_3_banner_carrusel
 */
?>

<?php

    $field = get_field_object('tipo_de_banner');
    $value = $field['value'];
    $label = $field['choices'][ $value ];

    switch ($value) {
        case 'wordpress':
            $banner = "wordpress";
        break;

        case 'dfp':
         $banner = "dfp";
        break;
         
        default:
            $banner = "wordpress";
        break;
    }

    $idPost = get_the_ID();
    $postType = get_post( $idPost );

    if ( $banner == "wordpress" ) {
        /*Se insertan los plugins que necesita la seccion tal y como se llama el archivo*/
        load_script( $pluginsNecesarios = array('mob_2_banner_home') );
        /*End Se insertan los plugins que necesita la seccion tal y como se llama el archivo*/

        
        $activo = NULL; 
        $noSlider = 0;
        $i=0;

        $desk_bannerbtnCarrusel = NULL;

        if(have_rows("contenido_del_slider_carrusel",$theID) ) { 
            while( have_rows('contenido_del_slider_carrusel') ) { the_row();
                $img_vertical = get_sub_field('imagen_vertical');
                $img_horizontal = get_sub_field('imagen_horizontal');

                $bannerimgCarrusel['vertical'][$noSlider] = $img_vertical['url'];
                $bannerimgCarrusel['horizontal'][$noSlider] = $img_horizontal['url'];

                $bannerimgCarruselBtn = get_sub_field('desk_group_banner_carrusel');
                
                $desk_bannerbtnCarrusel[$noSlider] = array( $bannerimgCarruselBtn['desk_call_to_action_banner_carrusel'], $bannerimgCarruselBtn['desk_url_call_to_action_banner_carrusel'], $bannerimgCarruselBtn['desk_color_del_banner_carrusel'] );
                $noSlider++;
                
            }
            $i = $noSlider;
        }
        $noSlider = 0;
        $jsonBannerCarrusel=json_encode($bannerimgCarrusel);
        // print_r ($desk_bannerbtnCarrusel);
?>

        <script>
        var jsonBannerCarrusel = <?php echo $jsonBannerCarrusel; ?>;
        var noSlider = <?php echo $i; ?>;


        function orientacionDispositivo() {

            if (Math.abs(window.orientation) === 90) {
                // Landscape
                //document.getElementById("orientation").innerHTML = "LANDSCAPE";
                var tipo= "horizontal";
            } else {
                // Portrait
                var tipo= "vertical";
                //document.getElementById("orientation").innerHTML = "PORTRAIT";
            }
            var orient=tipo;
            ajusteBannerCarrousel(orient, jsonBannerCarrusel, noSlider);
            bannerHeight();
        }

        jQuery(document).ready(function(){
            var orient = orientacionDispositivo();
            ajusteBannerCarrousel(orient, jsonBannerCarrusel, noSlider);
            bannerHeight();

            window.onorientationchange = orientacionDispositivo; 
            orientacionDispositivo();
        });

        // jQuery(document).ready(function(){
        //     window.onorientationchange = orientacionDispositivo; 
        //     orientacionDispositivo();
        // });
        
        /*Se comento el día 01-11-2018 Descomentar en caso de no funcionar en mobile*/
        //jQuery(document).ready(function(){
            // var orient = orientacionDispositivo();
            // ajusteBannerCarrousel(orient, jsonBannerCarrusel, noSlider);
            // bannerHeight();

        //});


        // Listen for orientation changes
        // window.addEventListener("orientationchange", function() {
        // Announce the new orientation number
           
        // }, false);
        </script>


        <div class="margen-top-menu"> </div> 
        <!--Carousel Wrapper--> <!-- slide carousel-fade -->
        <div id="carousel-banner" class="carousel carousel-slide carousel-banner" data-ride="carousel-banner" >
            <div id="banner-carrusel" class="banner banner-carrusel mb-1">
            <!-- style="margin-top: 45px; position: absolute; z-index: 20; width: 100%;" -->
            <!-- cambiar a margin 0px cuando se ajuste crl-banner -->
                <div class="banner-cinta wow fadeInLeft">
                    <span class="cintillo">
                        <label class="banner-cintillo-titulo">
                            <span class="orange-text roboto-medium">|</span>
                            <?php echo $postType->post_title; ?>
                        </label>
                    </span>        
                </div>
            </div>
            <!--Slides-->
            <div class="carousel-inner content-carousel" role="listbox">
                <?php
                    $i=0; 
                    if(have_rows("contenido_del_slider_carrusel",$theID) ) { 
                        while( have_rows('contenido_del_slider_carrusel') ) { the_row();
                            if($i == 0){ $activo = "active"; }
                ?>
                <!-- First slide -->
                <div class="item-banner carousel-item <?php echo $activo; ?>">              
                    <div class="view hm-black-light">
                        <img id="banner-item-slider-<?php echo $i; ?>" data-src="" class="animated fadeIn lazy" alt="">
                        <div class="full-bg-img"></div>
                    </div>
                    <!-- Caption -->
                    <div class="carousel-caption texto-slider">
                        <div class="animated fadeInDown">
                            <p class="texto-banner-titulo white-text"><?php echo get_sub_field('descripcion_del_slider'); ?></p>
                            <p class="texto-banner-carousel white-text"><?php echo get_sub_field('subtitulo_del_slider'); ?></p>
                            <div class="carousel-banner-boton">
                                <?php if( strlen( trim( $desk_bannerbtnCarrusel[$i][1] ) ) > 3  &&
                                        strlen( trim( $desk_bannerbtnCarrusel[$i][0] ) ) > 3
                                    )
                            { if (strpos($desk_bannerbtnCarrusel[$i][1], 'javascript') !== false) {
                                ?>
                                <a onclick="<?php echo $desk_bannerbtnCarrusel[$i][1]; ?>" <?php if($post->post_name === 'testvocacional' ){echo 'data-solicitud-location="Banner"';} ?> class="btn back-naranja-unitec waves-effect waves-light" id="btn-carousel-<?php echo $i; ?>"><?php echo $desk_bannerbtnCarrusel[$i][0]; ?></a>
                            <?php } else {?>
                                <a href="<?php echo $desk_bannerbtnCarrusel[$i][1]; ?>" class="btn back-naranja-unitec waves-effect waves-light" id="btn-carousel-<?php echo $i; ?>"><?php echo $desk_bannerbtnCarrusel[$i][0]; ?></a>
                            <?php } }?>
                            </div>
                            <!-- <div class="carousel-banner-boton">
                                <button class="btn back-naranja-unitec waves-effect waves-light">Call to action</button>
                            </div> -->
                        </div>
                    </div>
                    <!-- Caption -->
                </div>
                <!-- First slide -->
                <?php
                            $i++;
                            $noSlider++;
                            $activo = NULL;
                        }
                    }
                ?>
            </div>
            <!--/.Slides-->

            <!--Indicators-->
            <ol class="custom-indicadores carousel-indicators indicadores">
                <?php for ($j=0; $j < $noSlider ; $j++) { 
                        if($j == 0){ $activo = "active"; }
                ?>
                    <li data-target="#carousel-banner" data-slide-to="<?php echo $j ?>" class="<?php echo $activo; ?>"></li>
                <?php       $activo = NULL;
                        } ?>
            </ol>
            <!--/.Indicators-->    
            <a href="#start-page" class="scroll-spy btnB btn-banner-estatico"><?php echo do_shortcode( '[icono nombre="'.sanitize_title("ver-abajo").'"][/icono]' ); ?></a>
        </div>
        <!--/.Carousel Wrapper-->
<?php
    }
    /*En if Wordpress*/

    else{
         /*Se insertan los plugins que necesita la seccion tal y como se llama el archivo*/
        //load_script( $pluginsNecesarios = array('mob_2_banner_home') );
        /*End Se insertan los plugins que necesita la seccion tal y como se llama el archivo*/
?>
            <script>
                var jsonBannerCarrusel = 0;
                var noSlider = 0;
            </script>

            
            
            <script type='text/javascript'>
                var Linea = undefined;
                var mySpreadsheet = undefined;
                var urldfp = undefined;
                var TituloPost = "<?php echo $postType->post_title; ?>";

                function impresion_dfp(linea, spreadsheet, url, urldfp) {
                    //console.log(url);
                    var self = this;

                    $.get("<?php echo get_template_directory_uri(); ?>/secciones_desk/desk_2_9_banner_carrusel_DFP.php?titlePost= "+ TituloPost + "&urlDFP=" + urldfp, (data) => {
                            $(".result").html( data );
                        
                        Linea = linea;
                        mySpreadsheet = spreadsheet;
                        $ = jQuery;
                        //console.log("Ejecutando extraer con " + Linea);
                        imprimeBanner(Linea);
                    });
                };

                var googletag = googletag || {};
                googletag.cmd = googletag.cmd || [];
                (function() {
                    var gads = document.createElement('script');
                    gads.async = true;
                    gads.type = 'text/javascript';
                    var useSSL = 'https:' == document.location.protocol;
                    gads.src = (useSSL ? 'https:' : 'http:') +
                        '//www.googletagservices.com/tag/js/gpt.js';
                    var node = document.getElementsByTagName('script')[0];
                    node.parentNode.insertBefore(gads, node);
                })();
            
                googletag.cmd.push(function() {
                    googletag.defineSlot('/7847748/BHUNITEC', [1024, 768], 'div-gpt-ad-1541092674189-0').addService(googletag.pubads());
                    googletag.pubads().enableSingleRequest();
                    googletag.enableServices();
                });

            </script>

            <script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/assets/frontend/js/vendor/min/jqueryLibrariesDFP.min.js"></script>
            <script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/assets/frontend_desktop/js/secciones/min/desk_2_9_banner_carrusel_DFP.min.js"></script>
            

            
            <div class="result iframedfp" id='div-gpt-ad-1541092674189-0'>
                <script type='text/javascript'>
                    googletag.cmd.push(function() { googletag.display('div-gpt-ad-1541092674189-0'); });
                </script>
            </div>

<?php }?>
<span id="start-page"></span>