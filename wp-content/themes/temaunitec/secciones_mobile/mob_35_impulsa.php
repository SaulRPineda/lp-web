<?php
/**
 * WP Post Template: mob_35_impulsa
 */
?>
<?php
/*Se insertan los plugins que necesita la seccion tal y como se llama el archivo*/
/*load_script( $pluginsNecesarios = array('masonry') );*/
/*End Se insertan los plugins que necesita la seccion tal y como se llama el archivo*/

$args = array(
    'post_type'             => 'vacantes-unitec',
    'post_status'           => 'publish',
    'ignore_sticky_posts'   => 1,
    'posts_per_page'        => -1
);
$descuentos = query_posts($args);
$actualDiscount = 0;
$categoryDiscount = [];
$categoryDiscountCampus = [];
$categoryDiscountContactData = [];
$categoryDiscountLinkData = [];
$field_key = NULL;
$categoryDiscounts = [];
$json_descuentos = NULL;
$array_campus = [];

// print_r (get_fields());

$json_descuentos = '{';
//   print_r($descuentos);
foreach ($descuentos as $key => $descuento) {
    $json_descuentos .= '"'.$descuento->post_name.'":';

    $telefono = get_field('datos_de_contacto',$descuento->ID);


    $discount_categoria = get_field('categoria',$descuento->ID);
    $discount_campus = get_field('campus',$descuento->ID);
    $discount_logo = get_field('logotipo_de_la_empresa',$descuento->ID);
    // $discount_ubicacion = get_field('ubicacion_de_la_vacante',$descuento->ID);



    // vars
    $ubicacion_vacante = get_field_object('ubicacion_de_la_vacante',$descuento->ID);
    $value = $ubicacion_vacante['value'];
    $ubicacion = $ubicacion_vacante['choices'][ $value ];


    $discount_campus_object = get_field_object('campus',$descuento->ID);



    $discount_name = get_field('nombre_de_la_empresa',$descuento->ID);
    $discount_contacto = get_field('datos_de_contacto',$descuento->ID);
    $discount_links = get_field('links_de_contacto_descuentos',$descuento->ID);
    // $discount_caracteristica = get_field('caracteristica_del_descuento',$descuento->ID);
    $discount_terminos = get_field('terminos_condiciones',$descuento->ID);
    $discount_qr_link = get_field('qr_code_descuento',$descuento->ID);

    $discount_terminos = str_replace('"', '\\\\"', $discount_terminos); 
    $discount_terminos = str_replace("'", "\\'", $discount_terminos); 
    //$discount_terminos = preg_replace('"', '\"', $discount_terminos);

    $json_descuentos .= '{"title":"'.trim($descuento->post_title).'", "name":"'.trim($descuento->post_name).'", "url":"'.trim($discount_logo).'", "categoria": "'.trim($discount_categoria).'", "terminos_condiciones": "'.trim($discount_terminos).'", "contacto": {';

    $array_campus[$descuento->post_name] =[];
    $array_campus_completo =[];
    
    foreach ($discount_campus as $key => $campus) {
        $array_campus[$descuento->post_name].array_push($array_campus[$descuento->post_name], $campus);
    }

    foreach ($discount_contacto as $key => $datos_array) {
        $json_descuentos .= '"'.$datos_array['medio_de_contacto'].'": ' . '"'.$datos_array['datos'].'",';
    }

    if(count($discount_contacto)> 0){
        if(mb_substr($json_descuentos, -1) != "{") {
            $json_descuentos = substr($json_descuentos, 0, strlen($json_descuentos) - 1);
        }
    }
    $json_descuentos .= '}, "links": {';

    foreach ($discount_links as $key => $datos_array) {
        $json_descuentos .= '"'.$datos_array['link_de_contacto'].'": ' . '["'.$datos_array['texto_link'].'","'.$datos_array['link_contact'].'"],';
    }

    if(count($discount_links)> 0){
        if(mb_substr($json_descuentos, -1) != "{") {
            $json_descuentos = substr($json_descuentos, 0, strlen($json_descuentos) - 1);
        }
    }
    $json_descuentos .= '},';

    $json_descuentos .= '"qr-code-descuento":"'.$discount_qr_link.'"';
    $json_descuentos .= '},';


    if($actualDiscount == 0) {
        $field_key = get_post_meta( $descuento->ID, '_categoria', true);
        $field = get_field_object($field_key);
        $choices = $field['choices'];
        $categoryDiscount = $choices;
        $categoryDiscount = array("all-tipes" => "Ver todas las Vacantes") + $categoryDiscount;

        $field_key = get_post_meta( $descuento->ID, '_campus', true);
        $field = get_field_object($field_key);

        $choices = $field['choices'];
        // print_r($choices);
        $categoryDiscountCampus = $choices;
        $categoryDiscountCampus = array("all-campus" => "Ver todas las Áreas") + $categoryDiscountCampus;

        $field_key = get_post_meta( $descuento->ID, '_datos_de_contacto', true);
        $field = get_field_object($field_key);
        $choices = $field['sub_fields'][0]['choices'];
        // echo "<br><br>";
        // print_r($choices);
        $categoryDiscountContactData = $choices;

        $field_key = get_post_meta( $descuento->ID, '_links_de_contacto_descuentos', true);
        $field = get_field_object($field_key);
        $choices = $field['sub_fields'][0]['choices'];
        // echo "<br><br>";
        // print_r($choices);
        $categoryDiscountLinkData = $choices;
    }

    $categoryDiscounts.array_push($categoryDiscounts, $descuento->post_name);

    $actualDiscount++;
}

$json_descuentos = substr($json_descuentos, 0, strlen($json_descuentos) - 1);
$json_descuentos .= '}';

/* Limpia el JSON de caracteres que no son ASCII */
// echo $json_descuentos . '<br>';
// $json_descuentos = preg_replace('/[\x00-\x1F\x80]/', '', $json_descuentos);
$json_descuentos = preg_replace('/[\n]/', '<br />', $json_descuentos);
$json_descuentos = preg_replace('/[\r]/', '', $json_descuentos);
// $json_descuentos = nl2br($json_descuentos);

?>

<script>
    var discounts_json = '<?php echo $json_descuentos; ?>';
    var category_array_json = '<?php echo json_encode($categoryDiscount) ?>';
    var category_contact_array_json = '<?php echo json_encode($categoryDiscountContactData) ?>';
    var category_link_array_json = '<?php echo json_encode($categoryDiscountLinkData) ?>';
</script>

<?php
    // $json_descuentos = json_decode(preg_replace('/[\x00-\x1F\x80-\xFF]/', '', $json_descuentos), true); // Quita las letras con acentos, no usar si es en español
    /* Se usa striplashes para volver los double quotes en un string a la normalidad */
    $json_descuentos = json_decode(stripslashes($json_descuentos), true);
?>

<input type="hidden" id="galeria-activa" name="galeria-activa" value="<?php echo $galerias[0]->post_name; ?>">
<input type="hidden" id="galeria-name" name="galeria-name" value="<?php echo $galerias[0]->post_title; ?>">

<div id="vacantes"></div>
<section class="container-fluid wow fadeIn d-flex justify-content-center align-items-center mb-2 mt-4 impulsa"><!-- /.Main <layout-->
    <div class="col-lg-11 justify-content-center d-flex align-items-center contenedor-impulsa" id="galeria_catalogo"> <!-- Contenedor de la galería -->
        <div class="w-100 m-0">
            <div class="row"> <!-- Contenedor de encabezado de la sección -->
                <article class="col-12 text-center d-flex justify-content-center p-0" data-wow-offset="50">
                    <h4 class="w-100 title-impulsa-mob">Vacantes Disponibles <?php //echo get_field('desk_tituloimpulsa'); ?></h4>
                </article>
                <article class="col-12 text-center mb-1 mt-1 d-flex flex-column justify-content-center" data-wow-offset="50">
                        <h6 class="w-100 description-impulsa-mob">Culpa adipisicing laborum proident consectetur exercitation mollit ex voluptate minim mollit tempor.
                </article>
            </div> <!-- Fin del contenedor de encabezado de la sección -->

            <div class="row filtros mt-1 mb-1">
                <div class="col-xl-6 col-md-6 col-sm-12">
                    <div class="dropdown">
                        <div class="drop-view filtro-descuentos d-flex align-items-center justify-content-center p-1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            <div class="col-11 p-0 d-flex flex-column"><h6 class="m-0" id="nameCampusDescuentos" data-descuentos-campus="">Ver todas las vacantes</h6></div>
                            <div class="col-1 d-flex align-items-center justify-content-end p-0"><?php echo do_shortcode( '[icono nombre="'.sanitize_title("ver-abajo").'" clase="icon-abajo"][/icono]' ); ?></div>
                        </div>
                        <div class="dropdown-menu dropdown-primary w-100">
                            <?php foreach ($categoryDiscountCampus as $key => $campus_discount) { ?>
                                <a class="dropdown-item destacar" onclick="filtroTipoCampus( '<?php echo $key ?>', '<?php echo $campus_discount; ?>' );"><?php echo $campus_discount; ?></a>
                            <?php
                            } ?>
                            <!-- <a class="dropdown-item destacar" onclick="filtroNoAnos( '<?php //echo $key ?>', '<?php //echo $opc_no_anos; ?>' );"><?php //echo $opc_no_anos; ?>PRIMERA OPCIÓN</a> -->
                            <!-- <a class="dropdown-item destacar" onclick="filtroNoAnos( '<?php //echo $key ?>', '<?php //echo $opc_no_anos; ?>' );"><?php //echo $opc_no_anos; ?>SEGUNDA OPCIÓN</a> -->
                        </div>
                    </div>
                </div>
            </div>
            <!-- <div class="row filtros">

            </div> -->
            <!-- <section id="tabNav" class="mt-1">
                <div class="tabs-wrapper content-iconos"> 
                    <ul class="nav classic-tabs free testimonial" role="tablist" style="justify-content: space-between;">
                        <?php 
                            foreach ($categoryDiscount as $key => $category_discount) { ?>
                        <div class="col-3 nav-item p-0 portada-galeria" id="<?php echo $galeria->post_name; ?>" name="<?php echo $galeria->post_name; ?>" title="<?php echo $galeria->post_title; ?>">
                            <?php
                                $text = $category_discount;
                                $newtext = wordwrap($text, 10, "<br />\n");
                            ?>
                            <a class="dropdown-item destacar d-flex flex-column align-items-center h-100 p-1" onclick="filtroTipoCategorias( '<?php echo $key ?>', '<?php echo $category_discount; ?>' );">
                                <?php echo do_shortcode( '[icono nombre="'.sanitize_title($key).'" clase="icon-descuentos mb-1"][/icono]' ); ?>
                                <?php echo $newtext; ?>
                            </a>
                        </div>
                                    
                        <?php }
                        ?>                                
                    </ul>
                </div>
            </section> -->

            <section class="fila-descuentos">
                <article class="col-12 text-center d-flex justify-content-center" data-wow-offset="50">
                    <h4 class="w-75 titulo-descuentos" id="available-discounts" hidden>No hay descuentos disponibles</h4>
                </article>
                <div class="row">
                <?php
                    $i = 0;
                    foreach ($json_descuentos as $key_descuento => $descuento_array) { ?>

                    <!--Grid column-->
                    <div class="col-12 item-<?php echo $key_descuento; ?> categoria-<?php echo $descuento_array['categoria']; ?> <?php foreach ($array_campus[$key_descuento] as $key_campus => $campus_value) { echo " campus-".$campus_value; $campus = $campus_value; }?> campus-all-campus categoria-all-tipes" id="<?php echo $key_descuento; ?>" name="<?php echo $key_descuento; ?>" title="<?php echo $descuento_array['title']; ?>" style="<?php if ($i++ >= 9) //echo "display:none"; ?>">
                        <!--Collection card-->
                        <div class="row collection-card pb-1 m-0">
                            <div class="col-4 p-0">
                                <!--Card image-->
                                <a onclick="abreGaleriaDesc()" data-gtm-tr="OurFac" data-gtm-facilidad="<?php echo $descuento_array['title']; ?>" data-gtm-interaccion="Descuentos">
                                    <div class="view portada-galeria-descuentos lazy" id="<?php echo $key_descuento; ?>" name="<?php echo $key_descuento; ?>" title="<?php echo $descuento_array['title']; ?>" data-src="<?php echo $descuento_array['url']; ?>" alt="portada galeria <?php echo $descuento_array['title']; ?>">
                                    </div>
                                </a>
                                <!--Card image-->
                            </div>
                            <div class="col-8 p-0 pl-2">
                                <div class="fichaDesign">
                                    <div>
                                        <div class="title"><?php echo $descuento_array['title']; ?></div>
                                        <div class="content">Área: <b ><?php echo $discount_campus_object['choices'][$campus]; ?></b></div>
                                        <div class="content">Empresa: <b><?php echo $discount_name; ?></b></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row collection-card m-0 dir">
                            <div class="col-1 p-1 d-flex align-items-center justify-content-center">
                                <a class="d-flex"><?php echo do_shortcode( '[icono nombre="'.sanitize_title("ubicacion").'"][/icono]' ); ?></a>
                            </div>
                            <div class="col-8 p-1 d-flex align-items-center justify-content-left"><?php echo $ubicacion; ?></div>
                            <div class="col-3 p-1"><a href="" class="linkSecundario pull-right link-fix">VER MÁS</a></div>
                        </div>
                        <!--Collection card-->
                    </div>
                <?php }
                ?>
                </div>
            </section>

            <!-- Modal -->
            <div class="modal fade modal-tarjeta carousel-fade p-0" id="modal-tarjeta-galeria-carrusel-descuentos" role="dialog" aria-hidden="true" data-backdrop="static" data-keyboard="false">
                <div class="modal-dialog modal-fluid" role="document">
                    <div class="modal-content d-flex align-items-top justify-content-center modal-descuentos">
                        <div class="modal-content-tarjeta p-1">
                            <div class="icons">
                                <a href="#galeria-desk" data-dismiss="modal" onclick="cierraGaleriaDesc()" class="d-flex justify-content-center">
                                <?php echo do_shortcode( '[icono nombre="'.sanitize_title("cerrar").'"][/icono]' ); ?>
                                </a>
                            </div>
                            <div class="container contactanos">
                                <!--Card-->
                                <div class="content-gral d-flex justify-content-center">
                                    <div class="col-lg-12 col-xl-5 align-self-center card-white p-0">
                                        <!--Card content-->
                                        <div class="row mt-1">
                                            <div class="col-3 pr-1 d-flex justify-content-center">
                                                <div class="convenio modal-imagen-descuento"></div>
                                                <!-- <img class="convenio modal-imagen-descuento lazy" data-src="" alt=""> -->
                                            </div>
                                            <div class="col-9 pl-1">
                                                <div class="card-desc mt-1">
                                                    <h3 class="tit-desc m-0 modal-titulo-descuentos"></h3>
                                                    <h4 class="sub-desc modal-categoria-descuentos"></h4>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row mt-1">
                                            <div class="col-12">
                                                <div class="card-desc">
                                                    <div class="lista-desc mb-1"></div>
                                                    <span class="legales modal-terminos-descuentos"></span>
                                                </div>
                                                <div class="contacto-desc">
                                                    <h3 class="card-title-contacto mt-2">CONTACTO</h3>
                                                    <ul class="card-sub-contacto" id="modal-contacto-descuentos">
                                                        <!-- <li>Teléfono: <a href="#"><b>2159-0066</b></a></li>
                                                        <li>Correo: <a href="#"><b>info@casadelibro.com.mx</b></a></li>
                                                        <li>Página <a href="#"><b>web: https://casadelibro.com.mx/</b></a></li>
                                                        <li>QR de descuento: <a href="#"><b><span type="button" class="descargar-descuento">Descargalo aquí</span></b></a></li> -->
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>

                                    </div>
                                    <!-- <div class="col-lg-1 d-none d-lg-block phantom"></div> -->
                                </div>
                                <!--/.Card-->
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- Final del Modal -->
        </div>
    </div>
</section>