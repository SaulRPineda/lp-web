<div id="detalle"></div>
<?php
/**
 * WP Post Template: mob_22_card_basica
 */
?>
<main class="col-12 mt-0"><!-- /.Main <layout--> 
    <section id="elegant-card"><!-- SECTION-->
        <div class="row"><!-- Live preview-->
            <div class="col-md-12"><!-- col-md-12 -->
<!-- ***************************-> Inicia tu código ... -->


                    <section id="tabNav">
                            <!-- Tab Navegacion -->
                            <?php echo do_shortcode( '[icono nombre="'.sanitize_title("tab-izquierda").'" clase="leftTabNav direccionales" estilo="style=\'display:none\'"][/icono]' ); ?>

                            <div class="tabs-wrapper" id="elementoConScroll">
                                <ul class="nav classic-tabs " role="tablist">
                                <?php $noTabs=0;
                                 while( have_rows('card_basica_tabs') ) { the_row(); ?> 



                                <?php if($noTabs == 0) { $active = "active text-black"; } ?>
                                    <li class="nav-item">
                                        <a class=" waves-light <?php echo $active; ?>" data-toggle="tab" href="#tab-panel-<?php echo $noTabs; ?>" role="tab" data-gtm-tr="Tab" data-gtm-tab= "<?php echo get_sub_field('titulo_del_tab_oferta_basica'); ?>"><?php echo trim(get_sub_field('titulo_del_tab_oferta_basica')); ?></a>
                                     </li> 
                                         
                                  <?php $active = NULL; ?>
                                  <?php $noTabs++; } ?>
                                </ul>
                            </div>

                            <?php echo do_shortcode( '[icono nombre="'.sanitize_title("tab-derecha").'" clase="rightTabNav direccionales" estilo="style=\'display:none\'"][/icono]' ); ?>


                        </section>


                       <!-- Variables para slider -->
                        <div class="tab-content pt-3">
                            <?php
                                $active = NULL;
                                $areas_de_estudio = NULL;
                                $panelContent=0;
                                $itemSlide = 0;
                            ?>
               <!-- Se asigna el nombre del custom -->             
                <?php while( have_rows('card_basica_tabs') ) { the_row(); ?>
                <!-- Se valida si contienen id`s --> 
                  <?php if(have_rows("pestanas_basic",$theID) ){ ?>
                      <?php if($panelContent == 0){ $active = "active"; } ?>
                            <div class="tab-pane fade in show  <?php echo $active; ?>" id="tab-panel-<?php echo $panelContent; ?>" role="tabpanel">
                            <div id="carouselUnitec_basica-<?php echo $panelContent; ?>" class="carousel slide carousel-fade" data-ride="carouselUnitec_basica<?php echo $panelContent; ?>">
                                <!--Slides-->
                                <div class="carousel-inner" role="listbox">
                                <!-- while para contar slides -->
                              <?php while( have_rows('pestanas_basic') ) { the_row(); ?>
                                  <?php if($itemSlide == 0) { $active = "active"; } ?>
                                      <?php $campo_obtenido = get_sub_field('titulo_card_basica');
                                            $a=get_term_meta($campo_obtenido->term_id); 
                                            //va por los terms asociados al post
                                            $id_post=$a['dhvc_woo_category_page_id'][0];
                                            $pagina = get_post($id_post); ?>

                                    <!--First slide-->
                                    <div class="carousel-item <?php echo $active; ?>">

                                        <!--Card-->
                                        <div class="card">

                                            <!--Title-->
                                    <h4 class="card-title"><?php echo $campo_obtenido; ?></h4>

                                            <!-- card image -->
                                            <div class="view overlay hm-white-slight">
                                                <?php 
                                //Comprueba si es imagen destacada o se agrego
                                switch (get_sub_field('imgen_destacada_o_nueva_basica')) {
                                            //Si la imagen es destacada
                                                        case 'imgnueva':
                                                            $value = get_sub_field('imagen_nueva_basica');
                                                            $urlImg = $value['url'];
                                                            $altImg = $value['alt'];
                                                            
                                                            $term=$campo_obtenido->term_id;
                                                            $taxonomy='product_cat';

                                                            $args = array(
                                                                        'post_type'             => 'product',
                                                                        'post_status'           => 'publish',
                                                                        'ignore_sticky_posts'   => 1,
                                                                        'posts_per_page'        => '12',
                                                                        'tax_query' => array(
                                                                            'relation' => 'AND',
                                                                            array(
                                                                                'taxonomy' => 'product_cat',
                                                                                'field'    => 'term_id',
                                                                                'terms'    => $campo_obtenido->term_id,
                                                                            ),
                                                                            array(
                                                                                'taxonomy' => 'product_cat',
                                                                                'field'    => 'term_id',
                                                                                'terms'    => $campo_obtenido->term_id,
                                                                            ),
                                                                        ),                                            
                                                                    );
                                                                    $product = query_posts($args);
                                                                    //Revisar el id correcto se tiene que separar en las categorias dos prepas
                                                                    $urlPage = get_permalink($product[0]->ID);
                                                           
                                                        break;
                                                        //Si la imagen es agregada
                                                        case 'imgdestacada':
                                                            $thumbID = get_post_thumbnail_id($pagina->ID);
                                                            $urlImg = wp_get_attachment_url($thumbID);
                                                            $altImg = get_post_meta($thumbID, '_wp_attachment_image_alt', true);
                                                        break;
                                                    }
                                                ?>
                                                
                                                <!-- <amp-img alt="<?php echo $altImg; ?>"
                                                  src="<?php echo $urlImg; ?>"
                                                  width="768"
                                                  height="420"
                                                  layout="responsive">
                                                </amp-img> -->
                                                <img alt="<?php echo $altImg; ?>" class="img-fluid lazy" data-src="<?php echo $urlImg; ?>">
                                                <!-- class="mask waves-effect waves-light" -->
                                                <a><div></div></a>
                                            </div>
                                            
                                            <!-- Validacion de No de Items en el carrusel -->
                                            <?php //if ( count( get_sub_field('imgen_destacada_o_nueva_basica') ) > 1) { ?>
                                            <!--Button-->
                                            <a class="showarrow-<?php echo $panelContent; ?> d-none btn-floating btn-action">
                                                <?php echo do_shortcode( '[icono nombre="'.sanitize_title("ver-derecha").'"][/icono]' ); ?>
                                            </a>
                                            <?php //} ?>

                                            <!-- Aqui van los componentes list_icon, list, texto, cascade, footer, image, collapse -->
                                            <div class="card-block fix-card-block"> 
                                                <!-- Inicio de Subtitulo de la Card -->
                                                <h4 class="title-card-basica card-content-title mt-1"><?php echo trim(get_sub_field('subtitulo_card_basica')); ?></h4>
                                                <!-- Fin de Subtitulo de la Card -->
                                                <!-- Inicio de Contenido de la Card -->
                                                <?php echo get_sub_field('tipo_de_contenido_basica');?>
                                                <!-- Fin de de Contenido de la Card -->   
                                                <!-- Inicio de Texto con url de la Card -->
                                                <a href="<?php echo trim(get_sub_field("url_del_boton_card_basica")); ?>" id="btnListop3" class="linkSecundario" target="_blank"><?php echo trim(get_sub_field('boton_basico')); ?></a>
                                                <br>
                                                <br>
                                                <!-- Fin de Texto con url de la Card -->                                   
                                                <!-- Comparacion para poner slider`s -->
                                                <?php 
                                                    switch (get_sub_field('tipo_de_contenido_basica')) {
                                                        case 'tipo_de_contenido_basica': ?>
                                            </div>
                                            <!--/.Accordion wrapper-->
                                        </div>
                                        <!-- row -->
                                                <?php break;
                                                        
                                                        default:                                    
                                                        break;
                                                    }
                                                ?>
                                            </div>
                                            <!-- Card Block -->
                                        </div>
                                        <!--/.Card-->
                                    </div>
                                    <!--/.First slide-->                    
                              <?php $itemSlide++; ?>
                              <?php $active = NULL; ?>                    
                               <?php } ?>
                            <!-- End while para contar slides -->
                            </div>
                            <br>
                            <!--/.Slides-->

                            <!-- Validacion de No de Items en el carrusel -->
                            <?php if ($itemSlide > 1) { ?>
                            <ol class="carousel-indicators">
                                <?php $slide = 0; ?>
                                <?php if ($slide == 0) { $active = "active"; } ?>
                                    <?php for ($i=0; $i < $itemSlide; $i++) { ?>
                                            <li data-target="#carouselUnitec_basica-<?php echo $slide; ?>" data-slide-to="<?php echo $i; ?>" class="<?php echo $active; ?>">></li>
                                        <?php 
                                            $active = NULL;
                                          } ?>                            
                            </ol>
                            <!--/.Indicators-->
                            <?php } ?>       
                            <input type="hidden" name="sliders-<?php echo $panelContent; ?>" id="sliders-<?php echo $panelContent; ?>" value="<?php echo $itemSlide; ?>">
                        </div>
                        <!--/.Carousel Wrapper-->        
                    </div>
                    <!-- #panel -->
                    <?php $itemSlide=0; ?>
                    <?php $panelContent++; ?>
                    <?php } ?>
                <?php  } ?>
                </div>
                <input type="hidden" name="panelContent" id="panelContent" value="<?php echo $panelContent; ?>">
<!-- ***************************-> Termina tu código ... -->
            </div><!-- col-md-12 -->
        </div><!-- /.Live preview-->
    </section><!-- SECTION-->
</main><!-- /.Main <layout-->