<div id="testimoniales"></div>
<?php
/**
 * WP Post Template: mob_12_testimoniales
 */

$json_testimoniales = NULL;
$portadas_testimoniales = NULL;

if(have_rows("contenido_testimoniales",$theID) ) { 
    $json_testimoniales = '{';
    while( have_rows('contenido_testimoniales') ) { the_row();

        $contenido_testimoniales = get_sub_field('contenido_testimonios', $theID);
        $allTabs[] = sanitize_title( get_sub_field('titulo_del_tab', $theID) );

        foreach ($contenido_testimoniales as $key => $value) {
            $testimonial_seleccionado = get_field('fotografias_testimonios', $value);
            $titulos_tab =  sanitize_title( get_sub_field('titulo_del_tab', $theID) );

            /*Update 1.1 se obtiene el nombre del post(galeria)*/
            $post_contenido = get_post( $value );
            $post_name_testimonio = $post_contenido->post_name;
            /*print_r( $post_contenido );*/
            /*Update 1.1 se obtiene el nombre del post(galeria)*/

            if($key == 0) {
                $json_testimoniales .= '"'.strtolower($titulos_tab).'":[{';
            }

            $json_testimoniales .= '"'.$key.'": [';

            foreach ($testimonial_seleccionado as $foo => $imagenes) {
                $json_testimoniales .=  '{"url":"'.$imagenes['url'].'", "alt": "'.$imagenes['alt'].'", "description": "'.$imagenes['description'].'" , "urlthumbnail": "'.$imagenes['sizes']['thumbnail'].'", "nombre" : "'.get_field('nombre_testimonio', $value).'", "perfil": "'.get_field('perfil_testimonio', $value).'", "licenciatura" : "'.get_field('nombre_licenciatura', $value).'", "contenido": "'.get_field('contenido_testimonio', $value).'", "postName": "'.$post_name_testimonio.'", "pagina": "'.get_permalink($post->ID).'"},';

                if($foo == 0) {
                    $portadas_testimoniales []= array( array( $imagenes['sizes']['thumbnail'], get_field('nombre_testimonio', $value) ) );
                }                            
            }

            $json_testimoniales = substr($json_testimoniales, 0, strlen($json_testimoniales) - 1);
            $json_testimoniales .= '],';
        }

        $json_testimoniales = substr($json_testimoniales, 0, strlen($json_testimoniales) - 1);
        $json_testimoniales .= '}],';
    }    

    $json_testimoniales = substr($json_testimoniales, 0, strlen($json_testimoniales) - 1);
    $json_testimoniales .= '}';
}
?>

<?php if( !empty( $testimonial_seleccionado ) || $testimonial_seleccionado != NULL || count( $testimonial_seleccionado ) > 0 ) { ?>
<script>
    var testimoniales_json = '<?php echo $json_testimoniales; ?>';
    testimoniales_json = JSON.parse(testimoniales_json);
    /*console.log(testimoniales_json);
    var tab_activo = Object.keys(testimoniales_json)[0];*/
    var tab_activo = "<?php echo $allTabs[0]; ?>";
</script>

<main class="col-12 mt-0">

<?php //print_r($portadas_testimoniales); ?>          
<?php //print_r($json_testimoniales); ?>          

<input type="hidden" id="tab-testimonio-activo" name="tab-testimonio-activo" value="">
    <!-- SECTION-->
    <section id="elegant-card">
        <!-- Live preview-->
        <div class="row">

            <div class="col-md-12">
                <div class="card mb-2">
                    <h4 class="card-title"><?php echo get_field('titulo_card_testimoniales', $theID); ?></h4>
                    <div class="card-block">
                        <div class="row">
                            <div class="col-12">
                                <p class="card-text roboto-light"><?php echo get_field('contenido_card_testimoniales', $theID); ?></p>
                            </div>
                        </div>        
                    </div>
                   <section id="tabNav" style="display:none">
                        <?php echo do_shortcode( '[icono nombre="'.sanitize_title("ver-izquierda").'" clase="leftTabNav direccionales" estilo="style=\'display:none\'"][/icono]' ); ?>
                        <div class="tabs-wrapper"> 
                            <ul class="testimonios nav classic-tabs " role="tablist">
                        <?php   if(have_rows("contenido_testimoniales",$theID) ) { 
                                    $noTabs=0;  
                                    while( have_rows('contenido_testimoniales') ) { the_row(); 
                                        if($noTabs == 0){ $active = "active text-black"; } 
                        ?>
                                <li class="nav-item nav-item-testimonial">
                                    <a class="waves-light <?php echo $active; ?> waves-effect waves-light" data-toggle="tab" href="#tab-testimonios-<?php echo $noTabs; ?>" role="tab" name="content-testimonio-<?php echo $noTabs; ?>"><?php echo get_sub_field('titulo_del_tab'); ?></a>
                                </li> 
                                    <?php   $noTabs++; 
                                            $active = NULL; 
                                    } 
                                } 
                        ?>          
                            </ul>
                        </div>
                        <?php echo do_shortcode( '[icono nombre="'.sanitize_title("ver-derecha").'" clase="rightTabNav direccionales" estilo="style=\'display:none\'"][/icono]' ); ?>
                    </section><!-- Tab panels -->
                    <div class="tab-content carrusel-testimoniales lighten-4 mb-1 p-0 px-1">
                        <?php   if(have_rows("contenido_testimoniales",$theID) ) { 
                            $panelContent = 0; 
                            while( have_rows('contenido_testimoniales') ) { the_row(); 
                                $active = NULL; 
                                $itemSlide = 0;  /* Total de Cards en el Carrusel */
                                $slide = 0; /* Contador de indicador */
                                if($panelContent == 0) { $active = "active"; } 
                        ?>
                        <div class="tab-pane fade in show <?php echo $active; ?>" id="tab-testimonios-<?php echo $panelContent; ?>" role="tabpanel">
                            <!-- Tab Navegacion -->
                            <section id="tabNav">
                                <?php //echo do_shortcode( '[icono nombre="'.sanitize_title("tab-izquierda").'" clase="leftTabNav direccionales" estilo="style=\'display:none\'"][/icono]' ); ?>
                                <div class="tabs-wrapper m-0 w-100"> <!-- se quita clase content-avatar-testimonios -->
                                    <!-- Icon Testimoniales  -->
                                    <ul id="content-testimonio-<?php echo $panelContent; ?>" class="nav classic-tabs free testimonial" role="tablist" style="justify-content: space-between;">                                        
                                    </ul>
                                </div>
                                <?php //echo do_shortcode( '[icono nombre="'.sanitize_title("tab-derecha").'" clase="rightTabNav direccionales" estilo="style=\'display:none\'"][/icono]' ); ?>
                            </section>    
                        </div> 
                            <?php   $panelContent++;
                                    $active = NULL;
                                }  ?>
                        <?php }   ?>                   
                    </div>
                </div><!-- card mb-2 -->
            </div><!-- col-md-12 -->
        </div><!-- /.Live preview-->
    </section><!-- SECTION-->
    <!-- Modal -->
    <div class="modal fade modal-tarjeta" id="modal-tarjeta-testimoniales" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" data-backdrop="static" data-keyboard="false">
        <div class="modal-dialog modal-fluid card-rotating" role="document">
            <div class="modal-content face front card-body">
                <div class="modal-header">
                    <div class="imgMinTestimoniales">
                        <img id="img-perfil-testimonial" src="">
                    </div>
                    <div class="description">
                        <label id="testimonio-title" class="testimonio-title"></label><br>
                        <label id="perfil-title"></label><br>
                        <label id ="licenciatura-title"></label>
                    </div>
                    <div class="icons">
                        <!-- <?php echo do_shortcode( '[icono nombre="'.sanitize_title("ver-opciones").'"][/icono]' ); ?> -->
                        <!-- <div class="dropdown-menu dropdown-menu-right puntos-tarjeta" aria-labelledby="puntos">
                            <a class="dropdown-item" href="#"><?php echo do_shortcode( '[icono nombre="'.sanitize_title("compartir").'"][/icono]' ); ?> Compartir</a>
                            <a class="dropdown-item" href="#"><?php echo do_shortcode( '[icono nombre="'.sanitize_title("descargar").'"][/icono]' ); ?> Descargar</a>
                        </div> -->
                        <?php echo do_shortcode( '[icono nombre="'.sanitize_title("cerrar").'"][/icono]' ); ?>
                    </div>
                </div>

                <div class="modal-content-tarjeta-testimoniales py-2">
                <!--Carousel Wrapper-->
                    <div id="carousel-tarjeta-testimonial" class="carousel slide carousel-fade" data-ride="carousel-tarjeta-testimonial">
                        <!--Slides-->
                        <div class="carousel-inner bsn" role="listbox" id="content-item-testimonial"></div>
                        <!--/.Slides-->
                        <!--Indicators-->
                        <ol id="indicadores-carusel-testimoniales" class="custom-indicadores carousel-indicators mt-1"></ol>
                        <!--/.Indicators-->
                    </div>
                <!--/.Carousel Wrapper-->
                </div>

                <div class="modal-footer text-center d-flex flex-column px-3">

                    <div class="mt-0 mb-1 card ovf-hidden" style="background-color: rgba(0,0,0,1)">
                        <div id="card-testimonial" class="card-rotating">
                            <div class="face front card-body">
                                <div class="navegacion">
                                    <p id="footer-testimonio"></p>
                                    <a class="rotate-btn" data-card="card-testimonial"><?php echo do_shortcode( '[icono nombre="'.sanitize_title("compartir").'" clase="activator compartir-fix"][/icono]' ); ?></a>
                                    <p class="m-0 py-1">Compartir</p>      
                                </div>
                            </div>

                            <div class="face back card-reveal" id="share-testimoniales">
                                <?php echo do_shortcode('[share cerrar="card-testimonial" pagina="'.get_permalink($post->ID).'"][/share]');?>
                            </div>
                        </div>
                    </div>

                </div>

            </div>
        </div>
    </div>
    <!-- Modal -->
</main><!-- /.Main <layout-->
<?php } ?>