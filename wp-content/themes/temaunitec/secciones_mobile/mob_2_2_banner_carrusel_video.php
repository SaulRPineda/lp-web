<?php
/**
 * WP Post Template: mob_2_2_banner_carrusel_video
 */

$ds = DIRECTORY_SEPARATOR;
$dirVideos = get_template_directory_uri() . '/assets/videos/';

/*Se insertan los plugins que necesita la seccion tal y como se llama el archivo css*/
//load_styles( $pluginsNecesarios = array('mob_2_banner_home') );
/*End Se insertan los plugins que necesita la seccion tal y como se llama el archivo css*/

/*Se insertan los plugins que necesita la seccion tal y como se llama el archivo*/
load_script( $pluginsNecesarios = array('mob_2_banner_home') );
/*End Se insertan los plugins que necesita la seccion tal y como se llama el archivo*/

$idPost = get_the_ID();
$postType = get_post( $idPost ); 

$activo = NULL;
$noSlider = 0;
/*$videoBanner = $dirVideos.get_field('nombre_del_video').".mp4";*/
$videoBanner = "//unitecmx-universidadtecno.netdna-ssl.com/DFP/2017/videos/Mp4/vertical/TEST_VERTICAL.mp4";

/*$bannervideo['vertical'] = $videoBanner;*/
/*$bannervideo['vertical'] = $videoBanner;
$bannervideo['horizontal'] = "//unitecmx-universidadtecno.netdna-ssl.com/DFP/2017/videos/Mp4/Fisioterapia.mp4";*/

$videoVertical = get_field('video_vertical');
$videoHorizontal = get_field('video_horizontal');

$bannervideo['vertical'] = $videoVertical['url'];
$bannervideo['horizontal'] = $videoHorizontal['url'];

$jsonBannerVideo=json_encode($bannervideo);

?>

<script>
    var jsonBannerVideo=<?php echo $jsonBannerVideo; ?>;
    // // Listen for orientation changes
    // window.addEventListener("orientationchange", function() {
    // // Announce the new orientation number
    //     orient = detectaOrientacion();
    //     ajusteBanner(orient, jsonBannerVideo, 'video');
    //     bannerHeight();
    // }, false);
</script>

<script>
// jQuery('document').ready(function(){
        
//     var orient = detectaOrientacion();
//     ajusteBanner(orient, jsonBannerVideo, 'video');
//     bannerHeight();
// });
</script>

<div class="margen-top-menu"> </div> 
<!--Carousel Wrapper--> <!-- slide carousel-fade -->
<div id="carousel-banner-video" class="carousel carousel-slide mb-1 carousel-banner" data-ride="carousel-banner-video">

    <!--Video source-->
    <video id="banner-video" class="video-fluid video-banner" preload="auto" autoplay muted loop webkit-playsinline playsinline></video>    
    <div class="filtro-video">

        <div id="banner-carrusel" class="banner banner-carrusel mb-1">
            <div class="banner-cinta wow fadeInLeft">
                <!-- <div style="background-color: rgba(0,0,0,0.7); position: absolute; width:100%; height:100%;"></div> -->
                <span class="cintillo">
                    <label class="banner-cintillo-titulo">
                        <span class="orange-text roboto-medium">|</span>
                        <?php echo $postType->post_title; ?>
                    </label>
                </span>        
            </div>
        </div>

        <!--Slides-->
        <div class="carousel-inner content-carousel" role="listbox">
            <?php
                $i=0; 
                if(have_rows("contenido_del_slider",$theID) ) { 
                    while( have_rows('contenido_del_slider') ) { the_row();
                        if($i == 0){ $activo = "active"; }
            ?>
            <!--First slide-->
            <div class="item-banner carousel-item <?php echo $activo; ?>">              
                <!--Caption-->
                <div class="carousel-caption texto-slider">
                    <div class="animated fadeInDown">
                        <p class="texto-banner-titulo white-text"><?php echo get_sub_field('descripcion_del_slider'); ?></p>
                        <p class="texto-banner-carousel white-text"><?php echo get_sub_field('subtitulo_del_slider'); ?></p>
                    </div>
                </div>
                <!--Caption-->
            </div>
            <!--/First slide-->
            <?php
                        $i++;
                        $noSlider++;
                        $activo = NULL;
                    }
                }
            ?>
            <!-- First slide-->
        </div>
        <!--/.Slides-->

        <!--Indicators-->
        <ol class="custom-indicadores carousel-indicators indicadores">
            <?php for ($j=0; $j < $noSlider ; $j++) { 
                    if($j == 0){ $activo = "active"; }
            ?>
                <li data-target="#carousel-banner-video" data-slide-to="<?php echo $j ?>" class="<?php echo $activo; ?>"></li>
            <?php       $activo = NULL;
                    } ?>
        </ol>
        <!--/.Indicators-->

        <a href="#start-page" class="scroll-spy btnB btn-banner-estatico"><?php echo do_shortcode( '[icono nombre="'.sanitize_title("ver-abajo").'"][/icono]' ); ?></a>        

    </div><!-- filtro -->
    
</div>
<!--/.Carousel Wrapper-->

<span id="start-page"></span>