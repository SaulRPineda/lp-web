<?php
// $urlcalc = "http://unitecbeta.mx";
// $urlcalc = "https://www.unitec.mx";
// echo "<script>var dataLayer = []</script>";
// $fullpage_min= $urlcalc."/wp-content/themes/temaunitec/assets/frontend/css/vendor/min/jquery.fullpage.min.css";
// $critical_min= $urlcalc."/wp-content/themes/temaunitec/assets/frontend/css/vendor/min/critical.min.css";
// echo "<style>".file_get_contents($fullpage_min).file_get_contents($critical_min)."</style>";
// $scrolloverflow_min=$urlcalc."/wp-content/themes/temaunitec/assets/frontend/js/vendor/min/scrolloverflow.min.js";
// $fullpage_min=$urlcalc."/wp-content/themes/temaunitec/assets/frontend/js/vendor/min/jquery.fullpage.min.js";
// echo "<script>".file_get_contents($jquery_min).file_get_contents($scrolloverflow_min).file_get_contents($fullpage_min)."</script>";

?>

<!-- <script type="text/javascript">var urlwp = "<?php echo $urlcalc ?>/wp-content/themes/temaunitec";</script>
<script async src="<?php echo $urlcalc ?>/wp-content/themes/temaunitec/assets/frontend_desktop/js/secciones/min/desk_0_1_calculadora.min.js"></script>
<script src="<?php echo $urlcalc ?>/wp-content/themes/temaunitec/assets/frontend/js/calculadora_integracion/min/funciones-criticas.min.js"></script> -->


<div id="calculadora-main-mobile" style="display:none">
    <div id="footer">
        <div class="container-full">
            <div class="row items-center justify-center info-footer">
                <div class="col col-4 ctrl btn-footer" data-ir="sec-cursar">Quiero cursar<br><br>
                    <span class="rs-carrera m">
                    </span>
                </div>
                <div class="col col-4 ctrl" >
                    <div>Promedio</div>
                    <div class="clearfix">
                        <div class="col col-4 prom-ctrl right-align ctrlbtn" data-valor="min">-</div>
                        <div class="col col-4 output m"></div>
                        <div class="col col-4 prom-ctrl left-align ctrlbtn" data-valor="mas">+</div>
                    </div>
                </div>
                <div class="col col-4 ctrl" data-ir="sec-materias">
                    <div>Materias</div>
                    <div class="clearfix">
                        <div class="col col-4 mat-ctrl right-align ctrlbtn" data-valor="min">-</div>
                        <div class="col col-4 ctrlrs-materia m rs-materias display-none"></div>
                        <div class="col col-4 mat-ctrl  left-align ctrlbtn" data-valor="mas">+</div>
                    </div>
                </div>
            </div>
        </div>
    </div>
      <div id="fullpage">
         <div class="section fp-auto-height-responsive" data-anchor="calculadora" id="sec0">
            <div class="slide" data-anchor="sec-cursar">
               <div class="row md-hide lg-hide mobcont">
                  <!-- PREPARATORIA -->
                  <div class="max-width-4 mx-auto mt3 xs-mt1 xs-mb1 linea-mobile">
                     <div class="clearfix pb2 relative center line-b">
                        <div class="col col-2 pl2 ">
                           <div data-pos="pr" data-val="PREPARATORIA" class="expandible cerrado clearfix m100 absolute"></div>
                           <div class="imagen-lineas-cuadros cuadro PREPA">
                              <!-- <img data-src="<?php //echo get_template_directory_uri(); ?>/assets/frontend/img/preparatoria_mobile.png" class="circle" alt="Calcula tu Beca Preparatoria"> -->
                           </div>
                        </div>
                        <div class="col col-9 line">PREPARATORIA</div>
                     </div>
                  </div>
                  <!-- PREPARATORIA -->
                  <!-- licenciatura -->
                  <div class="max-width-4 mx-auto linea-mobile">
                     <div class="clearfix pb2 relative center line-b">
                        <div class="col col-2 pl2 ">
                           <div data-pos="li" data-val="LICENCIATURA" class="expandible cerrado clearfix m100 absolute"></div>
                           <div class="imagen-lineas-cuadros cuadro UG">
                              <!-- <img data-src="<?php //echo get_template_directory_uri(); ?>/assets/frontend/img/licenciaturas_mobile.png" class="circle" alt="Calcula tu Beca Licenciaturas"> -->
                           </div>
                        </div>
                        <div class="col col-9 p2  line">LICENCIATURA</div>
                     </div>
                  </div>
                  <!-- licenciatura -->
                  <!-- ingenieria -->
                  <div class="max-width-4 mx-auto linea-mobile">
                     <div class="clearfix pb2 relative center line-b">
                        <div class="col col-2 pl2 ">
                           <div data-pos="in" data-val="INGENIERIA" class="expandible cerrado clearfix m100 absolute"></div>
                           <div class="imagen-lineas-cuadros cuadro ING">
                              <!-- <img data-src="<?php //echo get_template_directory_uri(); ?>/assets/frontend/img/ingenierias_mobile.png" class="circle" alt="Calcula tu Beca Ingenierías"> -->
                           </div>
                        </div>
                        <div class="col col-9 line">INGENIERÍA</div>
                     </div>
                  </div>
                  <!-- ingenieria -->
                  <!-- SALUD -->
                  <div class="max-width-4 mx-auto linea-mobile">
                     <div class="clearfix pb2 relative center line-b">
                        <div class="col col-2 pl2 ">
                           <div data-pos="sa" data-val="SALUD" class="expandible cerrado clearfix m100 absolute"></div>
                           <div class="imagen-lineas-cuadros cuadro CS">
                              <!-- <img data-src="<?php //echo get_template_directory_uri(); ?>/assets/frontend/img/salud_mobile.png" class="circle" alt="Calcula tu Beca Salud"> -->
                           </div>
                        </div>
                        <div class="col col-9 line">SALUD</div>
                     </div>
                  </div>
                  <!-- SALUD -->
                  <!-- POSGRADO -->
                  <div class="max-width-4 mx-auto linea-mobile">
                     <div class="clearfix pb2 relative center line-b">
                        <div class="col col-2 pl2">
                           <div data-pos="po" data-val="DIPLOMADO" class="expandible cerrado clearfix m100 absolute"></div>
                           <div class="imagen-lineas-cuadros cuadro DIP">
                              <!-- <img data-src="<?php //echo get_template_directory_uri(); ?>/assets/frontend/img/posgrados_mobile.png" class="circle" alt="Calcula tu Beca Posgrados"> -->
                           </div>
                        </div>
                        <div class="col col-9 line">POSGRADO</div>
                     </div>
                  </div>
                  <!-- POSGRADO -->
                  <!-- POSGRADO -->
                  <div class="max-width-4 mx-auto linea-mobile">
                     <div class="clearfix pb2 relative center line-b">
                        <div class="col col-2 pl2">
                           <div data-pos="po" data-val="POSGRADO" class="expandible cerrado clearfix m100 absolute"></div>
                           <div class="imagen-lineas-cuadros cuadro POS">
                              <!-- <img data-src="<?php //echo get_template_directory_uri(); ?>/assets/frontend/img/posgrados_mobile.png" class="circle" alt="Calcula tu Beca Posgrados"> -->
                           </div>
                        </div>
                        <div class="col col-9 line">POSGRADO</div>
                     </div>
                  </div>
                  <!-- POSGRADO -->
               </div>
               <!-- TERMINA seccion MOBILEE -->
               <!-- seccion desktop -->
            </div>
            <div class="slide" data-anchor="sec-campus" id="sec-campus"> Los campus  </div>
            <div class="slide" data-anchor="sec-promedio" id="sec-promedio">
               <div class="clearfix center pt1 ">
                  <p class="h2 sub pl1 pr1">Compártenos el promedio que obtuviste en tu <span class="rs-canterior"></span>:</p>
                  <div class="h2">
                     <div class="output">8</div>
                  </div>
               </div>
               <div class="max-width-4 mx-auto px3" id="promedio">
                  <div class="slider">
                     <div style="width: 100%;margin: 3vw 0 1.7vw 0;">
                        <input type="range"  id="range" name="distance" class="form-control " min="6" max="10" step=".1" value="8" list="range_list">
                        <div id="range_list" class="flex justify-between mt1">
                            <div class="flex flex-column center pl1"><span>|</span>6</div>
                            <div class="flex flex-column center"><span>|</span>7</div>
                            <div class="flex flex-column center"><span>|</span>8</div>
                            <div class="flex flex-column center"><span>|</span>9</div>
                            <div class="flex flex-column center"><span>|</span>10</div>
                        </div>
                        <!-- <datalist id="range_list">
                           <option>6</option>
                           <option>7</option>
                           <option>8</option>
                           <option>9</option>
                           <option>10</option>
                        </datalist> -->
                     </div>
                  </div>
               </div>
               <div class="max-width-4 mx-auto px3 center mt3">
                  <button class="button-primaryn " onClick="irA('calculadora','resultados')"> Ver Colegiatura</button>
               </div>
            </div>
            <div class="slide " data-anchor="resultados" id="resultados">
                <div class="envolvente flex items-center justify-center">
                    <div class="bg-bco xs-px0 p3 xs-p0 xs-pb4">
                        <div class="max-width-3 mx-auto">
                            <div class="flex flex-wrap">
                                <div class="col-12 center ">
                                    <div class="tit-concluye flex justify-between">
                                        <p class="h2 sub m0">Concluye tu <span class="rs-carrerac"></span> en:</p>
                                        <span class="aniosduracion"></span>
                                    </div>
                                <p class="subtitulo">Modalidad: <span class="rs-modalidad"></span>&nbsp;&nbsp; | &nbsp;&nbsp; Campus: <span class="rs-campus"></span></p>
                                </div>
                            </div>
                        </div>
                        <div class="clearfix max-width-3 mx-auto">
                            <div class="col col-8 ">
                                <p class="h3 sub ticket">Pago <span class="rs-tipopago">Mensual</span>:</p>
                            </div>
                            <div class="col col-4  right-align">
                                <p class="h2 sub ticket rs-pg-final"></p>
                            </div>
                        </div>
                        <div class="clearfix max-width-3 mx-auto m-r-beca" id="beca-txt">
                            <div class="col col-8 ">
                                <p class="h3 sub ticket">Beca del:</p>
                            </div>
                            <div class="col col-4  right-align">
                                <p class="h2 sub ticket rs-beca"></p>
                            </div>
                        </div>
                        <div class="clearfix max-width-3 mx-auto">
                            <div class="col col-8 ">
                                <p class="h3 sub ticket">Reinscripción/Inscripción:</p>
                            </div>

                            <!--Reincripción Prepa UNAM !-->
                            <div class="col col-4 right-align d-none" id="prepa-unam">
                                <p class="h2 sub ticket ">$3,950</p>
                            </div>
                            <!--Reincripción Prepa UNAM !-->

                            <!--Reincripción Prepa Unitec y Vespertina !-->
                            <div class="col col-4  right-align" id="prepa-sin-costo">
                                <p class="h2 sub ticket ">Sin Costo*</p>
                            </div>
                            <!--Reincripción Prepa Unitec y Vespertina !-->
                        </div>
                        <div class="clearfix max-width-3 mx-auto m-r-ahorro">
                            <div class="col col-8 ">
                                <p class="h3 sub ticket" id="total-ahorro">Total ahorro:</p>
                            </div>
                            <div class="col col-4  right-align">
                                <p class="h2 sub ticket rs-ahorro-mns"></p>
                            </div>
                        </div>
                        <div class="clearfix max-width-3 mx-auto">
                            <div class="col col-8 ">
                                <p class="h3 sub ticket">Materias a cursar:</p>
                            </div>
                            <div class="col col-4  right-align">
                                <p class="h2 sub ticket rs-materias"></p>
                            </div>
                        </div>
                        <!-- Opciones de pago -->
                        <div class="clearfix max-width-3 mx-auto flex items-center">
                            <div class="col col-6  ">
                                <p class="h3 sub ticket">Opciones de pago</p>
                            </div>
                            <div class="col col-6 right-align">
                                <button class="bl ml2 btn-dual-y rs-btn-mensual selected" id="btn-mensual">Mensual</button>
                                <button title="Button" class="bl ml2 btn-dual-y rs-btn-contado" id="btn-contado">Contado</button>
                            </div>
                        </div>
                        <div class="line-b"></div>
                        <div class="terminos flex flex-column">
                            <div class="clearfix right-align mt1 xs-text-center">
                                <a data-gtm-tr="evWhats" data-gtm-accion="intención" data-gtm-seccion="calculadora" data-gtm-depto="" href="https://api.whatsapp.com/send?phone=525529688928&text=Hola%2C%20estoy%20en%20la%20página%20de%20internet%20y%20quiero%20más%20información" target="_blank" class="button button-whatsapp ml2 mr2">
                                    <svg style="width:20px;height:20px;" version="1.1" x="0px" y="0px" viewBox="0 0 512 512" style="enable-background:new 0 0 512 512;" xml:space="preserve" >
                                        <g><path d="M256.064,0h-0.128C114.784,0,0,114.816,0,256c0,56,18.048,107.904,48.736,150.048l-31.904,95.104l98.4-31.456 C155.712,496.512,204,512,256.064,512C397.216,512,512,397.152,512,256S397.216,0,256.064,0z M405.024,361.504 c-6.176,17.44-30.688,31.904-50.24,36.128c-13.376,2.848-30.848,5.12-89.664-19.264C189.888,347.2,141.44,270.752,137.664,265.792 c-3.616-4.96-30.4-40.48-30.4-77.216s18.656-54.624,26.176-62.304c6.176-6.304,16.384-9.184,26.176-9.184 c3.168,0,6.016,0.16,8.576,0.288c7.52,0.32,11.296,0.768,16.256,12.64c6.176,14.88,21.216,51.616,23.008,55.392 c1.824,3.776,3.648,8.896,1.088,13.856c-2.4,5.12-4.512,7.392-8.288,11.744c-3.776,4.352-7.36,7.68-11.136,12.352 c-3.456,4.064-7.36,8.416-3.008,15.936c4.352,7.36,19.392,31.904,41.536,51.616c28.576,25.44,51.744,33.568,60.032,37.024 c6.176,2.56,13.536,1.952,18.048-2.848c5.728-6.176,12.8-16.416,20-26.496c5.12-7.232,11.584-8.128,18.368-5.568 c6.912,2.4,43.488,20.48,51.008,24.224c7.52,3.776,12.48,5.568,14.304,8.736C411.2,329.152,411.2,344.032,405.024,361.504z" fill="#FFFFFF"/></g>
                                    </svg>&nbsp;&nbsp;
                                    Resuelve tus dudas
                                </a>
                                <!-- <button onclick="javascript:window.location.href='/admision-unitec/'" class="button button-whatsapp ml2 mr2">Resuelve tus dudas</button> -->
                            </div>
                            <div class="clearfix max-width-4 mt1 xs-mt1">
                                <div class="sm-col sm-col-12">
                                    <p class="subt pl1 pr1">
                                    *Costos vigentes para el periodo 31 de enero 2019<span class="rs-becamasp"> </span><br></p>
                                    <p class="term-beca">
                                    *El porcentaje de beca se asignará conforme al promedio general del nivel de estudios anterior al que se inscriba
                                    </p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
         </div>
         <div class="section section-image" id="sec1" data-anchor="detalle">
            <div class="container pt3" id="home_principal">
               <div class="row xs-mx1">
                  <div class="sm-col sm-col-12 md-col-7 xs-px0 p2 px3 mt3  ">
                     <div class="mx0">
                        <h2>Ventajas de obtener una Beca Académica</h2>
                        <p>Si te interesa estudiar en Prepa UNITEC, cursar una Licenciatura, Ingeniería, Diplomado o Maestría, ¡Esta información es para ti!</p>
                        <ul class="pl0">
                            <li class="li-sec2 mb1">
                                <i class="flecha-derecha"></i>
                                Podrás mantener tu beca conservando un buen promedio.
                            </li>
                            <li class="li-sec2 mb1">
                                <i class="flecha-derecha"></i>
                                Te ayudará a continuar estudiando, haciendo más accesible el pago de tu colegiatura.
                            </li>
                            <li class="li-sec2 mb1">
                                <i class="flecha-derecha"></i>
                                Será una motivación adicional para mantener un buen nivel académico y concluir tus estudios.
                            </li>
                        </ul>
                        <h2>¿Cómo funciona la Calculadora?</h2>
                        <p>Utilizar la <strong>Calculadora UNITEC</strong> es muy fácil, solo debes seguir estos pasos:</p>
                        <ul class="pl0">
                            <li class="li-sec2 mb1">
                                <i class="flecha-derecha"></i>
                                <span>Ingresa tus datos para obtener un resultado personalizado.</span>
                            </li>
                            <li class="li-sec2 mb1">
                                <i class="flecha-derecha"></i>
                                <span>Elige el programa, campus y la modalidad que deseas estudiar.</span>
                            </li>
                            <li class="li-sec2 mb1">
                                <i class="flecha-derecha"></i>
                                <span>Agrega tu último promedio obtenido en el nivel de estudios anterior al que te deseas inscribir.</span>
                            </li>
                            <li class="li-sec2 mb1">
                                <i class="flecha-derecha"></i>
                                <span><strong>¡Listo!</strong> Conoce el costo de tu colegiatura, materias, porcentaje de beca y el ahorro que tendrás cada cuatrimestre.</span>
                            </li>
                        </ul>
                        <a href="#sec-formulario"  class="sm-col sm-col-12 md-col-6 button button-primaryn calc-desk">Calcula tu Beca Aquí</a>
                        <a href="#form-mobile" class="sm-col sm-col-12 md-col-6 button button-primaryn calc-mob">Calcula tu Beca Aquí</a>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <div class="plantillas" style="display:none">
         <div class="p_expandible">
            <div class="caja px3 pt3 lg-px4 xs-p1 m1">
               <div class="clearfix preg-est">
                  <p class="h2 sub xs-left-align ">¿Qué quieres estudiar?</p>
                  <div class="py2 xs-py1">
                     <input class="input sm-col sm-col-5 project" placeholder="Buscar..." id="project">
                     <input type="hidden" class="project-id" id="project-id">
                     <div id="projectSelected" hidden></div>
                  </div>
               </div>
               <div class="clearfix v_mod display-none">
                  <div class="col col-12 pr3 xs-pr0 ">
                     <p class="h2 sub xs-left-align ">¿Qué modalidad te interesa?</p>
                     <div class="py2 xs-py1">
                        <select class="input sm-col sm-col-5" id="render_modalidad"></select>
                     </div>
                  </div>
               </div>
               <div class="clearfix v_camp display-none">
                  <div class="sm-col sm-col-5 pr3 xs-pr0 ">
                     <p class="h2 sub xs-left-align ">¿En qué campus?</p>
                     <div class="py2 xs-py1">
                        <select class="input sm-col sm-col-5" id="render_campus"></select>
                     </div>
                  </div>
               </div>
               <div class="clearfix v_prepa display-none">
                  <div class="sm-col sm-col-5 pr3 xs-pr0 ">
                     <p class="h2 sub xs-left-align ">¿Eres padre o tutor?</p>
                     <div class="py2 xs-py1">
                        <select class="input sm-col sm-col-5" id="render_prepa"></select>
                     </div>
                  </div>
               </div>
               <div class="clearfix  ">
                   <p class="err_cursar"></p>
               </div>
               <div class="clearfix pb2">
                  <div class="col col-6 text-left ">
                     <button class="cerrar iconb">Regresar</button>
                  </div>
                  <div class="col col-6  sm-col-right ">
                     <button  onClick="javascript:irA('calculadora','sec-promedio')" class=" right button-primaryn">Avanzar</button>
                  </div>
               </div>
            </div>
         </div>
      </div>
</div>

<!-- <link rel="stylesheet" type="text/css" href="<?php echo $urlcalc ?>/wp-content/themes/temaunitec/assets/frontend/css/vendor/min/normalize.min.css" media="none" onload="if(media!='all')media='all'" />
<link rel="stylesheet" type="text/css" href="<?php echo $urlcalc ?>/wp-content/themes/temaunitec/assets/frontend/css/vendor/min/basscss.min.css" media="none" onload="if(media!='all')media='all'" />
<link rel="stylesheet" type="text/css" href="<?php echo $urlcalc ?>/wp-content/themes/temaunitec/assets/frontend/css/vendor/min/no-critical.min.css" media="none" onload="if(media!='all')media='all'" />
<script  src="<?php echo $urlcalc ?>/wp-content/themes/temaunitec/assets/frontend/js/calculadora_integracion/min/funciones-calculadora.min.js"></script> -->
<!-- <script async src="//unitecbeta.mx/wp-content/themes/temaunitec/assets/frontend_desktop/js/calculadora_integracion/jquery-ui.js"></script> -->
<!-- <script async src="<?php echo $urlcalc ?>/wp-content/themes/temaunitec/assets/frontend/js/calculadora_integracion/min/funciones-async.min.js"></script>
<script async src="<?php echo $urlcalc ?>/wp-content/themes/temaunitec/assets/frontend/js/calculadora_integracion/min/funciones-on.min.js"></script> -->
