<div id="biblioteca_botones"></div>
<?php
/**
 * WP Post Template: mob_17_biblioteca_botones
 */

/*Se obtiene el tipo de post Type Page or Product*/
$idPageBiblioteca = get_the_ID();
$urlPageBiblioteca = get_permalink($idPageBiblioteca);

$desk_Botones = get_field('desk_contenidoBiblioteca', $theID);
?>

<main class="col-12"><!-- /.Main <layout-->
    <section id="elegant-card"><!-- SECTION-->
        <div class="row"><!-- Live preview-->
            <div class="col-md-12"><!-- col-md-12 -->

                <section class="botonesUnitec">

                    <?php foreach ($desk_Botones as $key => $bullet) {

                        switch ($key) {
                            case '0':
                                $claseCard = "btn-primary";
                                $claseIcono = "icon-color-1";
                                break;
                            case '1':
                                $claseCard = "back-naranja-unitec";
                                $claseIcono = "icon-color-2";
                                break;
                            case '2':
                                $claseCard = "back-gris-unitec";
                                $claseIcono = "btn-danger";
                                break;
                            
                            default:
                                break;
                        }

                        $link = ( strpos( strtolower( trim( $bullet['desk_linkBiblioteca'] ) ), "unitec" )  ) ? 'href=' : 'onclick=';
                        $target = ( strpos( strtolower( trim( $bullet['desk_linkBiblioteca'] ) ), "unitec" ) ) ? "target='_blank'" : "";
                     ?>


                    <a href="<?php echo $bullet['desk_linkBiblioteca']; ?>" id="biblioteca_libri_<?php echo $key; ?>" class="url-library btn <?php echo $claseCard; ?> text-capitalize w-100" <?php echo $target; ?>><?php echo $bullet['desk_tituloBiblioteca']; ?></a>
                   
                <?php } ?>
                </section>

            </div><!-- col-md-12 -->
        </div><!-- /.Live preview-->
    </section><!-- SECTION-->
</main><!-- /.Main <layout-->