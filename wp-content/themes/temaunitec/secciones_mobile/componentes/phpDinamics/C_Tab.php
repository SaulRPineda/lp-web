<?php

    class C_Tab{

        public $tabs = array();

        private static $instCTab = null;

        public function getTab(){
            if( ! ( self::$instCTab instanceof self ) ){
                self::$instCTab = new self();
            }
            return self::$instCTab;
        }

        private function __construct( ){}

        // Este componente creara el header de los tabs...
        public function makeTabHeaderStart( $tabs ){ //para cerrar este componente utiliza CDIV;
            $this->tabs = $tabs;
            $stringTabsHeader = 
                '<div class="tabs-wrapper">' .
                '    <ul class="nav classic-tabs " role="tablist">';

            foreach( $this->tabs as $tab ){
                $stringTabsHeader .= 
                '       <li class="nav-item">' .
                '            <a class="waves-light text-black' . $tab["class"] . '" data-toggle="tab" href="#' . $tab["id"] . '" role="tab">' . $tab["valor"] . '</a>' .
                '       </li>';
            }

            $stringTabsHeader .= 
                '    </ul>' .
                '</div>' .

                '<!-- Tab panels -->' .
                '<div class="tab-content lighten-4">';

            echo $stringTabsHeader;
        }

        // Aeste componente se le pasara el indexdel elemento que se va a construir empezando de 0...
        public function makeTabSectionStart( $index ){ //para cerrar este componente utiliza CDIV;
            echo '<div class="tab-pane fade in show ' . $this->tabs[$index]["class"] . '" id="' . $this->tabs[$index]["id"] . '" role="tabpanel">';
        }

    }