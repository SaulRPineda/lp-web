<?php

    class C_Cards{

        private static $instCard = null;

        private function __construct(){}

        public static function getCard(){
            if( ! ( self::$instCard instanceof self ) ){
                self::$instCard = new self( );
            }
            return self::$instCard;
        }

        public function makeCardSimpleList( $items ){
            $simpleList =  '<ul class="list-group">';
            foreach( $items as $item ){
                $simpleList .=
                           '   <li class="list-group-item vertical-centered-text">' .
                           '       <div class="col-2">' .
                           '           <span class="' . $item["icon"] . '"></span>' .
                           '       </div>' .
                           '       <div class="col-10" style="margin-bottom: 9px">' .
                           '           <h3>' . $item["path1"] . '</h3>&nbsp&nbsp<h6 class="roboto-regular">' . $item["path2"] . '</h6>' .
                           '       </div>' .
                           '   </li>';
            }
            $simpleList .= '</ul>';
            echo $simpleList;
        }
    }