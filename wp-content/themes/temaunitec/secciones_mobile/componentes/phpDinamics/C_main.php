<?php

    class C_main{
        private $c_main_start = "";
        private $c_main_end = "";
        private static $instCMain = null;

        public static function getMain(){
            if( !( self::$instCMain instanceOf self) ){
                self::$instCMain = new self();
            }
            return self::$instCMain;
        }

        private function __construct(){
            $this->setMainStart();
            $this->setMainEnd();
        }

        public function m_start(){
            echo $this->c_main_start;
        }
        public function m_end(){
            echo $this->c_main_end;
        }

        private function setMainStart(){
            $this->c_main_start = 
'<main>
    <div class="main-wrapper">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-10 col-md-12">
                    <div class="documentation">
                        <section id="elegant-card">
                            <div class="row">
                                <div class="col-md-12">';
        }
        
        private function setMainEnd(){
            $this->c_main_end =
                                "</div>
                            </div>
                        </section>
                    </div>
                </div>
            </div>
        </div>
    </div>
</main>";
        }
    }