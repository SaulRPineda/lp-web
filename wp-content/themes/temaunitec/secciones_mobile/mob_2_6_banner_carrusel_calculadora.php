<?php
/**
 * WP Post Template: mob_2_6_banner_carrusel_calculadora
 */
?>

<?php

    /*Se insertan los plugins que necesita la seccion tal y como se llama el archivo css*/
    //load_styles( $pluginsNecesarios = array('mob_2_banner_home') );
    /*End Se insertan los plugins que necesita la seccion tal y como se llama el archivo css*/

    /*Se insertan los plugins que necesita la seccion tal y como se llama el archivo*/
    load_script( $pluginsNecesarios = array('mob_2_banner_home') );
    /*End Se insertan los plugins que necesita la seccion tal y como se llama el archivo*/

    $idPost = get_the_ID();
    $postType = get_post( $idPost );

    $activo = NULL; 
    $noSlider = 0;

    $i=0;

    if(have_rows("contenido_del_slider_carrusel",$theID) ) { 
        while( have_rows('contenido_del_slider_carrusel') ) { the_row();
            $img_vertical = get_sub_field('imagen_vertical');
            $img_horizontal = get_sub_field('imagen_horizontal');

            $bannerimgCarrusel['vertical'][$noSlider] = $img_vertical['url'];
            $bannerimgCarrusel['horizontal'][$noSlider] = $img_horizontal['url'];
            $noSlider++;
            
        }
        $i = $noSlider;
    }
    $noSlider = 0;
    $jsonBannerCarrusel=json_encode($bannerimgCarrusel);
?>

<script>

 var jsonBannerCarrusel = <?php echo $jsonBannerCarrusel; ?>;
    // console.log( jsonBannerCarrusel );
    var noSlider = <?php echo $i; ?>;


// function orientacionDispositivo() {

//     if (Math.abs(window.orientation) === 90) {
//         // Landscape
//         //document.getElementById("orientation").innerHTML = "LANDSCAPE";
//         var tipo= "horizontal";
//     } else {
//         // Portrait
//         var tipo= "vertical";
//         //document.getElementById("orientation").innerHTML = "PORTRAIT";
//     }
//     var orient=tipo;
//     ajusteBannerCarrousel(orient, jsonBannerCarrusel, noSlider);
//     bannerHeight();
// }



// jQuery(document).ready(function(){
//     window.onorientationchange = orientacionDispositivo; 
//     orientacionDispositivo();
// });
// jQuery(document).ready(function(){
    
   
//     var orient = orientacionDispositivo();
//     ajusteBannerCarrousel(orient, jsonBannerCarrusel, noSlider);
//     bannerHeight();

// });


// Listen for orientation changes
//window.addEventListener("orientationchange", function() {
// Announce the new orientation number
   
//}, false);
</script>


<div class="margen-top-menu"> </div> 
<!--Carousel Wrapper--> <!-- slide carousel-fade -->
<div id="carousel-banner" class="carousel carousel-slide carousel-banner" data-ride="carousel-banner" >
    <div id="banner-carrusel" class="banner banner-carrusel mb-1">
    <!-- style="margin-top: 45px; position: absolute; z-index: 20; width: 100%;" -->
    <!-- cambiar a margin 0px cuando se ajuste crl-banner -->
        <div class="banner-cinta wow fadeInLeft">
            <span class="cintillo">
                <label class="banner-cintillo-titulo">
                    <span class="orange-text roboto-medium">|</span>
                    <?php echo $postType->post_title; ?>
                </label>
            </span>        
        </div>
    </div>
    <!--Slides-->
    <div class="carousel-inner content-carousel" role="listbox">
        <?php
            $i=0; 
            if(have_rows("contenido_del_slider_carrusel",$theID) ) { 
                while( have_rows('contenido_del_slider_carrusel') ) { the_row();
                    if($i == 0){ $activo = "active"; }
        ?>
        <!-- First slide -->
        <div class="item-banner carousel-item <?php echo $activo; ?>">
            <div class="view hm-black-light">
                <img id="banner-item-slider-<?php echo $i; ?>" src="" class="animated fadeIn lazy" alt="">
                <div class="full-bg-img"></div>
            </div>
            <!-- Caption -->
            <div class="carousel-caption carousel-calculadora texto-slider">
                <div class="animated fadeInDown">
                    <p class="texto-banner-titulo-calc white-text"><?php echo get_sub_field('descripcion_del_slider'); ?></p>
                    <p class="texto-banner-carousel-calc white-text"><?php echo get_sub_field('subtitulo_del_slider'); ?></p> 
                    <div class="carousel-calculadora-boton mt-1">
                        <button class="muestracalcu btn back-naranja-unitec waves-effect waves-light">Calcula tu Beca</button>
                    </div>
                </div>
            </div>
            <!-- Caption -->
        </div>
        <!-- First slide -->
        <?php
                    $i++;
                    $noSlider++;
                    $activo = NULL;
                }
            }
        ?>
    </div>
    <!--/.Slides-->
       

    <!--Indicators-->
    <ol class="custom-indicadores carousel-indicators indicadores-calculadora">
        <?php for ($j=0; $j < $noSlider ; $j++) { 
                if($j == 0){ $activo = "active"; }
        ?>
            <li data-target="#carousel-banner" data-slide-to="<?php echo $j ?>" class="<?php echo $activo; ?>"></li>
        <?php       $activo = NULL;
                } ?>
    </ol>
    <!--/.Indicators-->    
    <a href="#start-page" class="scroll-spy btnB btn-banner-estatico-calculadora"><?php echo do_shortcode( '[icono nombre="'.sanitize_title("ver-abajo").'"][/icono]' ); ?></a>

</div>
<!--/.Carousel Wrapper-->
  
<span id="start-page"></span>



