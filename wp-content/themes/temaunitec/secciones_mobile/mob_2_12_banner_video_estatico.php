<?php
/*
 * WP Post Template: mob_2_12_banner_video_estatico
 */
 ?>

<?php
    $idPost = get_the_ID(); 
    $postType = get_post( $idPost );
    $permalink = get_permalink($idPost);
        
    $grupoBanner = get_field('group_banner_video_estatico', $theID);
    $grupoBannerDescripciones = get_field('descripciones_banner_video', $theID);
    $grupoBannerContenido = get_field('desk_group_banner_contenido', $theID);

    $descripcionBannerPrincipal = $grupoBannerDescripciones['descripcion_principal'];
    $descripcionBannerSecundaria = $grupoBannerDescripciones['descripcion_secundaria'];
    $descripcionBAnnerRefuerzo = $grupoBannerDescripciones['descripcion_refuerzo'];

    $desk_callToActionBannerEstatico = $grupoBannerContenido['desk_call_to_action_banner_estatico'];
    $desk_urlCallToActionBannerEstatico = $grupoBannerContenido['desk_url_call_to_action_banner_estatico'];
    $desk_colorTextoBanner = $grupoBannerContenido['desk_color_del_banner'];

    $mobBannerV = $grupoBanner['mob_video_banner_v'];
    $mobBannerH = $grupoBanner['mob_video_banner_h'];

    $bannerimg['vertical']=$mobBannerV;
    $bannerimg['horizontal']=$mobBannerH;
    $jsonBanner=json_encode($bannerimg);
?>
<script>
    var jsonBanner=<?php echo $jsonBanner; ?>;
    function orientacionDispositivo() {
        var tipo;
        if (Math.abs(window.orientation) === 90) {
             tipo= "horizontal";
        } else {
             tipo= "vertical";
        }
        return tipo;
    }
    function ajusteBanner(orient, jsonBanner, tipobanner){
    switch(tipobanner){
        case 'imagen':
            if(orient=="vertical") {
                jQuery('#banner-imagen').css('background-image','url('+jsonBanner.vertical+')').promise().done(function(){
                    jQuery(this).animate({
                        opacity: 1
                    }, 600)
                });

            } else if(orient=="horizontal"){
                jQuery('#banner-imagen').css('background-image','url('+jsonBanner.horizontal+')').promise().done(function(){
                    jQuery(this).animate({
                        opacity: 1
                    }, 600)
                }); 

            } else{
                jQuery('#banner-imagen').css('background-image','url('+jsonBanner.vertical+')').promise().done(function(){
                    jQuery(this).animate({
                        opacity: 1
                    }, 600)
                });
            }
        break;

        case 'video':
            var banner_video = document.getElementById("banner-video");
            if(orient=="vertical") {
                if ( banner_video.canPlayType("video/mp4") ) {
                    jQuery("#banner-video").attr({
                        'src': jsonBanner.vertical,
                        'media':'screen and (max-device-width: 600px)'
                    });
                    banner_video.load();
                } else{
                    console.log("el navegador no soporta el tipo de archivo");
                }
            } else if(orient=="horizontal") {
                if ( banner_video.canPlayType("video/mp4") ) {
                    console.log(jsonBanner.horizontal);

                    jQuery("#banner-video").attr({
                        'src': jsonBanner.horizontal,
                        'media':'screen and (min-device-width: 600px)'
                    });
                    banner_video.load();
                } else{
                    console.log("el navegador no soporta el tipo de archivo");
                }
            } else{
                if ( banner_video.canPlayType("video/mp4") ) {
                    jQuery("#banner-video").attr({
                        'src': jsonBanner.vertical,
                        'media':'screen and (max-device-width: 600px)'
                    });
                    banner_video.load();
                } else{
                    console.log("el navegador no soporta el tipo de archivo");
                }
            }
        break;
        }
        bannerHeight();     
    }
    function bannerHeight(){
        setTimeout(function(){
            var heightNavbar = jQuery(".nvct").height();
            var height = jQuery(window).height() - parseInt(heightNavbar);  //getting windows height
            jQuery('.carousel-banner').css('height', height+'px');
            jQuery(".carousel-banner").find(".carousel-item").css('height', height+'px');   //and setting height of carousel
        }, 500);
    }
    jQuery(document).ready(function(){
        var orient = orientacionDispositivo();
        ajusteBanner(orient,jsonBanner,"video");
        bannerHeight();

        window.addEventListener("orientationchange", function() {
            // Announce the new orientation number
            orient = orientacionDispositivo();
            ajusteBanner(orient, jsonBanner, 'video');
            bannerHeight();
        }, false);
    });
</script>

<div class="margen-top-menu"></div> 
<video id="banner-video" style="position:absolute;bottom:0" class="video-fluid video-banner" preload="auto" autoplay muted loop webkit-playsinline playsinline></video>
<div id="banner-imagen" class="carousel-banner banner banner-imagen carousel-inner bg-video">
    <div class="banner-cinta cinta-end banner-cinta-estatico">
        <div class="banner-cinta wow fadeInLeft">
            <span class="cintillo mt-2">
                <span class="orange-text roboto-medium mx-2">|</span>
                <P class="banner-cintillo-titulo m-0">
                    <?php echo $descripcionBannerPrincipal; ?>
                    <?php //echo $postType->post_title; ?>
                </P>
            </span>   
        </div>
    </div>
    <div class="w-100" style="bottom:2.5rem">
        <div class="carousel-caption mb-1 px-2" style="left: 0!important;right: 0!important;">
            <!-- Caption -->
            <!-- <h4 class="<?php echo $desk_colorTextoBanner; ?> m-0"><?php echo $descripcionBannerPrincipal; ?></h4> -->
            <p class="<?php echo $desk_colorTextoBanner; ?> texto-banner-carousel m-0"><?php echo $descripcionBannerSecundaria; ?></p>
            <p class="<?php echo $desk_colorTextoBanner; ?> texto-banner-carousel m-0"><?php echo $descripcionBAnnerRefuerzo; ?></p>
            <!-- Validación para Imprimr el call to action -->
            <?php if( strlen( trim( $desk_urlCallToActionBannerEstatico ) ) > 3  &&
                    strlen( trim( $desk_callToActionBannerEstatico ) ) > 3
                    ) {
                    if (strpos($desk_urlCallToActionBannerEstatico, 'javascript') !== false) {
                ?>
            <a onclick="<?php echo $desk_urlCallToActionBannerEstatico; ?>" <?php if($post->post_name === 'testvocacional' ){echo 'data-solicitud-location="Banner"';} ?> class="btn back-naranja-unitec btn-warning mx-auto waves-effect waves-light" title="<?php echo $desk_callToActionBannerEstatico; ?>" id="btn-carousel-0"><?php echo $desk_callToActionBannerEstatico; ?></a>
            <?php } else {?>
                        <a href="<?php echo $desk_urlCallToActionBannerEstatico; ?>" class="btn back-naranja-unitec btn-warning mx-auto waves-effect waves-light" title="<?php echo $desk_callToActionBannerEstatico; ?>" id="btn-carousel-0"><?php echo $desk_callToActionBannerEstatico; ?></a>
            <?php } }?>
        </div>
        <!-- Validación para Imprimr el call to action -->  
    </div>
    <a href="#start-page" class="scroll-spy btnB btn-banner"><?php echo do_shortcode( '[icono nombre="'.sanitize_title("ver-abajo").'"][/icono]' ); ?></a>
</div>
<span id="start-page"></span>