<?php
/*
 * WP Post Template: mob_31_menu_landing
 */

/**
* Custom walker class.
*/
class sidenav extends Walker_Nav_Menu {
    public $mini_menu; 

    function start_lvl( &$output, $depth = 0, $args = array()) {
        $output .= "\n" . $indent . '<div class="collapsible-body"><ul>';
    }

    function end_lvl( &$output, $depth = 0, $args = array() ) {
        $output .=  "\n" . $indent . "</ul></div>";
    }

    function start_el( &$output, $item, $depth = 0, $args = array(), $id = 0 ) {
        //print_r($args);

        $indent = ( $depth > 0 ? str_repeat( "\t", $depth ) : '' ); // code indent

        // Passed classes.
        $classes = empty( $item->classes ) ? array() : (array) $item->classes;

        $class_names = esc_attr( implode( ' ', apply_filters( 'nav_menu_css_class', array_filter( $classes ), $item ) ) );
        

        $arrow = ( strrpos( $class_names, "menu-item-has-children") > -1 )? '<i class="fa fa-angle-down rotate-icon"></i>': "";

        if( strrpos( $class_names, "menu-item-has-children") > -1 ){
            $parentHead = '<ul class="collapsible collapsible-accordion"><li>';
            $class_names = "class='collapsible-header waves-effect arrow-r'";
        }else{
            $parentHead = '<li>';
            $class_names = "waves-effect";
            $arrowcollapse = "";
            $attributes = ! empty( $item->url )? ' href="' . esc_attr( $item->url) .'" ' : '';
        }
        
        if( $item->title == "hr" ){
            $output .= "<hr>";
        }else{
            $mm = array( "Preparatoria", "Licenciaturas", "Posgrados" );
            if( in_array( $item->title, $mm ) ){
                $this->mini_menu[$item->title] = $item->url;
            }
            // Build HTML.
            $output .= $indent . $parentHead . 
                '<a ' . $class_names . $attributes . '>' .
                    /*'<i class="icon-u icon-u-' . sanitize_title( $item->title ) . '"></i>' .*/
                    /*do_shortcode( '[icono nombre="'.sanitize_title($item->title).'"][/icono]' ).*/
                    do_shortcode( '[icono nombre="'.sanitize_title($item->classes[0]).'"][/icono]' ).
                    $item->title .
                    $arrow .
                '</a>';
        }
    }

    function end_el( &$output, $item, $depth = 0, $args = array() ) {
        $output .= "</li>";
    }
}

$sideNav = new sidenav();
?>

<!-- elemento para dragable sidenav-->
    <!-- SideNav UnitecSideNav button -->
    <div style="height:1px"></div>
    <div class="nvct">
        <?php
            if( strpos($post->post_name,'impulsa') !== false  ){
                $urlBtn = ( $_COOKIE["reg_impulsa"] ) ? 'href="https://www.unitec.mx/folleto/impulsa.pdf"' : 'onclick="openModal(\'#modal_frm_app\')"';
            } else{
                $urlBtn = 'onclick="openModal(\'#modal_frm_app\')"';
            }
        ?>
        <div class="navContent navegador">
            <!-- <a href="javascript:void(0);" data-activates="UnitecSideNav" class="btnMenuMobile" style="cursor:pointer; z-index: 9999;"><i class="icon-u-menu"></i></a> -->
            <a class="logoNav">
                <img src="<?php echo get_template_directory_uri() . "/assets/frontend/img/footer/svg/unitec.svg"; ?>" alt="UNITEC">
            </a>

        <!-- Icono de entrar biblioteca -->  
            <a data-gtm-tr="frmTradicional" data-gtm-pos="header" id="login-biblioteca" onclick="openModal('#modal_frm_biblioteca')" class="searchNav d-none"><?php echo do_shortcode( '[icono nombre="'.sanitize_title("login").'"][/icono]' ); ?></a>
        <!-- Icono de salir biblioteca -->  
            <a id="logout-biblioteca" onclick="cerrarSesion()" class="searchNav d-none"><?php echo do_shortcode( '[icono nombre="'.sanitize_title("logout").'"][/icono]' ); ?></a>
        <!-- Icono de Whatsapp -->
            <!-- <a data-gtm-tr="Whatsapp" data-gtm-pos="header" href="https://clxt.ch/unitecg" target="_blank" onclick="openModal('#whatsapp')" cla   ss="searchNav"><?php echo do_shortcode( '[icono nombre="'.sanitize_title("whatsapp-footer").'"][/icono]' ); ?></a> -->
        <!-- Icono de sobre para contacto Formulario -->
            <?php if( strpos($post->post_name,'conexion-unitec') === false && strpos($post->post_name,'prueba-performance') === false && strpos($post->post_name,'agendar-cita') === false ){ ?>
            <a data-gtm-tr="frmTradicional" data-gtm-pos="header" id="calculadoraModal" <?php echo $urlBtn; ?> class="searchNav"><?php echo do_shortcode( '[icono nombre="'.sanitize_title("formularios").'"][/icono]' ); ?></a>
            <?php } ?>        
        </div>
    </div>