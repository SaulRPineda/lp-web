<div id="tabs_links_dinamicos"></div>
<?php
/**
 * WP Post Template: mob_14_tabs_links_dinamicos
 */
?>

<?php
    /*Se insertan los plugins que necesita la seccion tal y como se llama el archivo js*/
    load_script( $pluginsNecesarios = array('sec_specialList') );
    /*End Se insertan los plugins que necesita la seccion tal y como se llama el archivo js*/

    $imagen = get_field('imagen_card', $theID);
?>

<main class="col-12 mt-0 wow fadeInLeft"> 
    <!-- SECTION-->
    <section id="elegant-card">
        <!-- Live preview-->
        <div class="row">
            <div class="col-md-12">
<!-- ***************************-> Inicia tu código ... -->
                <div class="card mb-2">
                    <h4 class="card-title"><?php echo get_field('titulo_card', $theID); ?></h4>                    

                    <div class="view overlay hm-white-slight">
                        <img alt="<?php echo $imagen['alt']; ?>" class="img-fluid lazy" data-src="<?php echo $imagen['url']; ?>">
                        <!-- <amp-img 
                            layout="responsive"
                            width="768" 
                            height="420" 
                            src="<?php echo $imagen['url']; ?>" 
                            alt="<?php echo $imagen['alt']; ?>" />
                            <noscript><img src="<?php echo $imagen['url']; ?>" alt="<?php echo $imagen['alt']; ?>"></noscript>
                        </amp-img> -->
                        <a><!-- <div class="mask waves-effect waves-light"></div> --></a>
                    </div>
                    <div class="card-block">
                        <div class="row">
                            <div class="col-12">
                                <p class="card-text roboto-light"><?php echo get_field('contenido_card', $theID); ?></p>
                            </div>
                        </div>        
                    </div>

                    <section id="tabNav">
                        <?php echo do_shortcode( '[icono nombre="'.sanitize_title("tab-izquierda").'" clase="leftTabNav direccionales" estilo="style=\'display:none\'"][/icono]' ); ?>
                        <div class="tabs-wrapper"> 
                            <ul class="nav classic-tabs " role="tablist">
                            <?php 
                                $noTabs=0;
                                if(have_rows("contenido",$theID) ) {
                                    while( have_rows('contenido') ) { the_row();
                                        if($noTabs == 0) { $active = "active text-black"; }
                            ?>
                                <li class="nav-item"><!-- waves-effect waves-light -->
                                    <a class=" waves-light <?php echo $active; ?> " data-toggle="tab" href="#tab-panel-unitec-<?php echo $noTabs; ?>" role="tab" data-gtm-tr="Tab" data-gtm-tab= "<?php echo get_sub_field('titulo_del_tab'); ?>"><?php echo get_sub_field('titulo_del_tab'); ?></a>
                                </li>            
                            <?php 
                                        $active = NULL;
                                        $noTabs++;
                                    }
                                }
                            ?>
                            </ul>
                        </div>
                        <?php echo do_shortcode( '[icono nombre="'.sanitize_title("tab-derecha").'" clase="rightTabNav direccionales" estilo="style=\'display:none\'"][/icono]' ); ?>
                    </section>

                    <?php 
                        $active = NULL;    
                        $panelContent=0;
                        $itemSlide = 0;
                    ?>
                    <!-- Tab panels -->
                    <div class="tab-content lighten-4 ">
                        
                    <?php 
                        if(have_rows("contenido",$theID) ) {        
                            while( have_rows('contenido') ) { the_row();
                                if($panelContent == 0){ $active = "active"; }
                    ?>
                        <!--Inicia Tab -->
                        <div class="tab-pane fade in show <?php echo $active; ?>" id="tab-panel-unitec-<?php echo $panelContent; ?>" role="tabpanel">
                            <div class="col-12 p-0 specialList animated fadeIn">
                            <?php 
                                $nameTab = get_sub_field('titulo_del_tab');
                                    while( have_rows('links') ) { the_row();
                                        $descriptionIcon = get_sub_field('titulo');
                                        $descriptionIconGTM = trim(substr($descriptionIcon, 0, 30)).'...';
                            ?>
                            
                                <div class="simple-list">
                                    <div type="button" class="list-item-container">

                                        <?php
                                            $target = ( strpos( strtolower(get_sub_field('link')), "unitec.mx" ) ) ? "" : "target='_blank'";                                                

                                            if ( get_sub_field('link') != NULL || get_sub_field('link') != "" ) {
                                                $linkInicio = "<a href='".get_sub_field('link')."' ".$target." data-gtm-tr='Prestige'  data-gtm-tab='". $nameTab ."'  data-gtm-option='". $descriptionIconGTM ."' >";
                                                $linkFin = "</a>";                                   
                                            }
                                        ?>

                                        <div class="item-icon">
                                            <?php echo $linkInicio; ?><?php echo do_shortcode( '[icono nombre="'.sanitize_title( get_sub_field("icono") ).'" clase="link-simple-list"][/icono]' ).$linkFin; ?>
                                        </div>
                                        
                                        <div class="item-description">
                                            <?php 
                                                    echo $linkInicio.$descriptionIcon.$linkFin;
                                                    $linkInicio = NULL;
                                                    $linkFin = NULL;
                                            ?>
                                        </div> <!-- icon-u icon-u-alumnos -->
                                    </div>
                                </div>

                            <?php   $itemSlide++;
                                    $active = NULL;  
                                 } 
                                ?>
                            </div>
                        </div>
                        <!-- Termina Tab -->
                    <?php       $itemSlide=0;
                                $panelContent++;  
                            }  
                        }
                    ?>
                    </div>
                    
                </div>
<!-- ***************************-> Termina tu código ... -->
            </div>
        </div><!-- /.Live preview-->
    </section><!-- SECTION-->
</main><!-- /.Main layout-->