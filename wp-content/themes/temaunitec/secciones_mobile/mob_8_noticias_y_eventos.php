<div id="noticias_y_eventos">  </div>
<?php
/**
 * WP Post Template: mob_8_noticias_y_eventos
 */
?>

<?php
    // $imagen = get_field('imagen_card', $theID);
    $tabsEventos = get_field('tabs_noticias_eventos', $theID); 
    $active = NULL;

    /*if( have_rows("tabs_noticias_eventos", $theID) ) {
        while( have_rows('tabs_noticias_eventos') ) { the_row();
            echo get_sub_field('titulo_del_tab')."<br />";

            if( have_rows("tipo_de_contenido", $theID) ) {
                while( have_rows('tipo_de_contenido') ) { the_row();
                    echo get_sub_field('titulo_card_dinamica')."<br />";
                    echo "Tipo de Contenido: ".get_row_layout()."<br>";
                }
            }
        }
    }*/
?>

<?php 
    if(have_rows("tabs_noticias_eventos", $theID) ) {
?>
<main class="col-12 mt-1 mb-2"><!-- /.Main <layout-->
    <section id="elegant-card"><!-- SECTION-->
        <div class="row"><!-- Live preview-->
            <div class="col-md-12"><!-- col-md-12 -->

            <section id="tabNav">
                <!-- Tab Navegacion -->
                <?php echo do_shortcode( '[icono nombre="'.sanitize_title("ver-izquierda").'" clase="leftTabNav direccionales" estilo="style=\'display:none\'"][/icono]' ); ?>
                <div class="tabs-wrapper"> 
                    <ul class="nav classic-tabs " role="tablist">

                        <?php
                            $i=0;                            
                                while( have_rows('tabs_noticias_eventos') ) { the_row();
                                    if ($i == 0) { $active = "active text-black"; }                        
                        ?>
                                        <li class="nav-item">
                                            <a class=" waves-light <?php echo $active ?>" data-toggle="tab" href="#tab-noticia-<?php echo $i; ?>" role="tab"><?php echo get_sub_field('titulo_del_tab'); ?></a>
                                        </li>
                        <?php
                                    $active = NULL;
                                    $i++;
                                }
                        ?>
                    </ul>
                </div>                    
                <?php echo do_shortcode( '[icono nombre="'.sanitize_title("ver-derecha").'" clase="rightTabNav direccionales" estilo="style=\'display:none\'"][/icono]' ); ?>
            </section>

            <!-- Tab panels -->
            <div class="tab-content lighten-4">
            <!--Panel 1-->
            <?php
                $foo = 0;                
                while( have_rows('tabs_noticias_eventos') ) { the_row();
                    
                    if($foo == 0) { $active = "active"; }                           
            ?>
                <!-- tab-panel -->
                <div class="tab-pane fade in show <?php echo $active; ?>" id="tab-noticia-<?php echo $foo; ?>" role="tabpanel">
                                  
                    <?php 
                        if( have_rows("tipo_de_contenido", $theID) ) {
                            $key = 0;
                            while( have_rows('tipo_de_contenido') ) { the_row();
                    ?>
                            <!-- Contenido de la Card -->
                            <div class="card ovf-hidden mt-1">
                                <!--Front Side-->
                                <div class="face front card-body">
                                    <!-- Card -->
                                    <div class="card">
                                        <?php if($key == 0) { ?>
                                            <!-- Title -->
                                            <h4 class="card-title"><?php echo get_sub_field('titulo_card_dinamica'); ?></h4>
                                            <?php 
                                                 $imagenCard = get_sub_field('imagen_card');
                                            //} 
                                        ?>
                                    
                                        <!-- card image -->
                                        <div class="view overlay hm-white-slight">
                                            <img alt="<?php echo $imagenCard['alt']; ?>" class="img-fluid lazy" data-src="<?php echo $imagenCard['url']; ?>">
                                            <!-- class="mask waves-effect waves-light" -->
                                            <a><div></div></a>
                                        </div>

                                        <?php } /*if de la muestra de imagen y boton a la priemra card*/ ?>

                                        <?php if($key == 0) { ?>
                                        <!-- Button -->
                                        <a href="<?php echo get_sub_field('url_contenido'); ?>" class="btn-floating btn-action waves-effect waves-light" target="_blank"><i class="icon-u icon-u-ver-mas "></i></a>
                                        <?php } ?>

                                        <!-- card image -->

                                        <?php 

                                            switch (get_row_layout()) {
                                                case 'noticia':
                                        ?>
                                                    <div class="card-block pb16">
                                                        <div class="row">
                                                            <div class="col-12">
                                                                <?php
                                                                    $contenido_dinamico = get_sub_field('titulo_contenido');
                                                                    $tipoSeccion = "Noticia: ".$contenido_dinamico;
                                                                    if( !empty($contenido_dinamico) || $contenido_dinamico != "" || $contenido_dinamico != NULL ) { 
                                                                ?>
                                                                <h4 class="card-content-title mt-1"><?php echo $contenido_dinamico; ?></h4>
                                                                <?php } ?>
                                                                <p class="card-text"><?php echo get_sub_field('contenido_card'); ?></p>
                                                                <!-- <a href="<?php echo get_field('url_contenido', $theID); ?>" class="linkSecundario" target="_blank"><?php echo get_field('titulo_url', $theID); ?></a> -->

                                                                <a href="<?php echo get_sub_field('url_contenido'); ?>" class="linkSecundario pull-right" target="_blank" data-gtm-tr="evNews" data-gtm-noticia="Noticia" data-gtm-nombre="<?php echo $contenido_dinamico; ?>" ><?php echo get_sub_field('titulo_url'); ?></a>
                                                                <span class="boxiconActions">
                                                                    <a class="rotate-btn"><?php echo do_shortcode( '[icono nombre="'.sanitize_title("compartir").'" clase="activator share"][/icono]' ); ?></a>
                                                                </span>
                                                            </div>
                                                        </div>
                                                    </div>
                                        <?php          
                                                    break;
                                                
                                                    default:
                                                    $titulo_dinamico = get_sub_field('titulo_contenido');
                                                    $tipoSeccion = "Evento: ".$titulo_dinamico;
                                        ?>
                                                        <table class="tableBTN botonTexto mt-1">
                                                            <tr>
                                                                <td class="text-center lh-0">
                                                                    <h1 class="eventCalendarDay"><?php echo get_sub_field('dia_del_evento'); ?></h1>
                                                                    <p class="roboto-medium"><?php echo get_sub_field('mes_del_evento'); ?></p>
                                                                </td>
                                                                <td>
                                                                    <h5 class="roboto-medium"><?php echo $titulo_dinamico; ?></h5>
                                                                    <p class="card-text roboto-light">
                                                                        <?php echo get_sub_field('contenido_card'); ?>
                                                                    </p>
                                                                </td>
                                                            </tr>
                                                            <td  colspan="2">
                                                                <a href="<?php echo get_sub_field('url_contenido'); ?>" class="linkSecundario pull-right" data-gtm-tr="NextEvents" data-gtm-fecha="<?php echo get_sub_field('dia_del_evento'); ?>-<?php echo get_sub_field('mes_del_evento'); ?>" data-gtm-nombre="<?php echo $titulo_dinamico; ?>"><?php echo get_sub_field('titulo_url'); ?></a>
                                                                <span class="boxiconActions">
                                                                    <a class="rotate-btn"><?php echo do_shortcode( '[icono nombre="'.sanitize_title("compartir").'" clase="activator share"][/icono]' ); ?></a>
                                                                </span>
                                                            </td>
                                                        </table>

                                         <?php               
                                                    break;
                                            }
                                        ?>
                                    </div>
                                    <!-- /.Card -->
                                </div>
                                <!--/.Front Side-->
                                <!--Back Side-->
                                <div class="face back card-reveal">

                                   <?php echo do_shortcode('[share cerrar="noticia-1" pagina="'.get_sub_field('url_contenido').'" seccion="'.$tipoSeccion.'"][/share]');?>
                                </div>
                                <!--/.Back Side-->
                            </div>
                            <!-- End Contenido de la Card -->
                    <?php   
                                    $key++;
                                } /*while*/
                            } /*if*/
                        $foo++;
                        $active = NULL;
                    ?>
                    </div>
                    <!-- tab-panel -->
               <?php
                        }                        
                    ?>
                </div>
    <!-- ***************************-> Inicia tu código ... -->
    <!-- ***************************-> Termina tu código ... -->
            </div><!-- col-md-12 -->
        </div><!-- /.Live preview-->
    </section><!-- SECTION-->
</main><!-- /.Main <layout-->
<?php 
    }
?>