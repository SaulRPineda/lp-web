<?php
/**
 * WP Post Template: mob_18_biblioteca_recursos
 */
?>

<div id="biblioteca_recursos"></div>
<?php //if( !empty( $_COOKIE['c_matricula'] ) || $_COOKIE['c_matricula'] != NULL ) { ?> 
<main id="seccion-biblioteca-recursos" class="col-12 mt-0" style="display:none !important;"><!-- /.Main <layout-->    
    <section id="elegant-card"><!-- SECTION-->
        <div class="row"><!-- Live preview-->
            <div class="col-md-12"><!-- col-md-12 -->
<!-- ***************************-> Inicia tu código ... -->

                <div class="card mt-1">
                    <h4 class="card-title"><?php echo get_field('titulo_card_biblioteca_recursos', $theID); ?></h4>

                    <div class="card-block pt-0">
                    	<div>
                            <p class="card-text roboto-light pt-1">
                                <?php echo get_field('contenido_recursos_electronicos', $theID); ?>
                            </p>
                        </div>

                        <div class="row">
                            <div class="col-12 p-0">

                                <?php
                                // check if the repeater field has rows of data
                                if( have_rows('contenido_collapsable_recursos') ){
                                    // loop through the rows of data
                                    while ( have_rows('contenido_collapsable_recursos') ) { the_row();
                                ?>
                                    <div class="simple-list">
                                        <div type="button" class="list-item-container border-top-fix">
                                            <!-- <div class="item-icon list-icon">
                                            <?php echo do_shortcode( '[icono nombre="'.sanitize_title( get_sub_field('icono_collapsable_recursos') ).'"][/icono]' ); ?>
                                            </div> -->
                                            <div class="item-description recurso-item-dscripcion" style="padding-left:16px;"><?php echo get_sub_field('titulo_collapsable_recursos'); ?></div>
                                            <div class="arrow">
                                                <?php echo do_shortcode( '[icono nombre="'.sanitize_title("ver-abajo").'"][/icono]' ); ?>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="text-colapse collapse animated flipInX">
                                        <div class="horario">
                                <?php                                        
                                        if( have_rows('link_del_recurso') ){
                                            // loop through the rows of data
                                            while ( have_rows('link_del_recurso') ) { the_row();
                                ?>                                                
                                            <a class="link-simple-list" href="<?php echo get_sub_field('link'); ?>" target="_blank"><label class="fix-horario-recursos pl-0 horario-item"><label class="horario-hora"> <?php echo get_sub_field('titulo_del_link'); ?></label></label></a> 
                                <?php                                                
                                            }
                                        }
                                ?>
                                        </div>
                                    </div>
                                <?php
                                    }
                                }
                                ?>


                            </div>
                        </div>
                    </div>
                </div>
<!-- ***************************-> Termina tu código ... -->
            </div><!-- col-md-12 -->
        </div><!-- /.Live preview-->
    </section><!-- SECTION-->
</main>
<?php //} ?>