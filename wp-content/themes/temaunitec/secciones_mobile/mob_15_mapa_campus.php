<div id="mapa-campus"></div>
<?php
/**
 * WP Post Template: mob_15_mapa_campus
 */
?>

<?php
    /*Se insertan los plugins que necesita la seccion tal y como se llama el archivo*/
    load_script( $pluginsNecesarios = array('places', 'mapaOperations') );
    /*End Se insertan los plugins que necesita la seccion tal y como se llama el archivo*/

    /*$page = get_page_by_title( 'Home' );
    echo "Home: ".get_field('como_llegar', $page->ID);
    echo '<span id="'.get_field('como_llegar', $page->ID).'"></span>';*/
    /*$comoLlegar = get_field('como_llegar', 13);*/

    $mapa = get_field('mapa', $theID);
    $coordenadasCampus = $mapa['lat'].",".$mapa['lng'];    

    $idPost = get_the_ID();
    $postType = get_post( $idPost ); 

    $imgMapa = get_field('imagen_mapa', $pagina->ID);
    $urlImg = $imgMapa["url"];
    $altImg = $imgMapa["alt"];
?>

<main class="col-12 mt-0"><!-- /.Main <layout-->
    <input type="hidden" name="h_destino" id="h_destino" value="<?php echo $coordenadasCampus; ?>">
    <input type="hidden" id="mi-destino" value="<?php echo get_field('direccion_del_campus'); ?>" disabled>
    <section id="elegant-card"><!-- SECTION-->
        <div class="row"> <!-- <div class="row" id="activarMapaGoogle">Live preview -->
        <!-- <div id="pruebaMapa" style=""> Contenedor mapa -->
            <div class="col-md-12"><!-- col-md-12 -->
<!-- ***************************-> Inicia tu código ... -->

                <div class="card mt-1 mb-1">
                    <h4 class="card-title"><?php echo get_field('titulo', $theID); ?></h4>

                    <section class="geolocalizacion">
                        <!-- Iniciar Mapa Google Maps -->
                        <!-- <div id="map" class=""></div> -->
                        
                        <div class="img-mapa">
                            <!-- <a data-gtm-tr="como-llegar" data-gtm-pos="maps-img" href="javascript:void(0)">
                                <img src="<?php echo $imgMapa["url"]; ?>" alt="<?php echo $imgMapa["alt"]; ?>">
                            </a> -->
                            <iframe class="map-top" width="100%" height="350" frameborder="0" y framespacing="0"
                            src="https://www.google.com/maps/embed/v1/place?key=AIzaSyB8rc5Fp2F0TyXna7Y5ylBhusDesXq_UrI&q=<?php echo $mapa['address']; ?>" 
                            allowfullscreen=""></iframe>
                        </div>
                        <span class="transport-links">
                            <div class="transport-pilot"> 
                                <a href="//maps.google.es/?q=<?php echo $mapa['address']; ?>"  id="icon-maps" target="_blank" data-gtm-tr="NavTool" data-gtm-herramienta="Google Maps">
                                <?php echo do_shortcode( '[icono nombre="'.sanitize_title("maps").'"][/icono]' ); ?>
                                    <label>MAPS</label>
                                </a>
                            </div>
                            <div class="transport-pilot">
                                <a href="//waze.com/ul?ll=<?php echo $coordenadasCampus; ?>&navigate=yes" target="_blank" data-gtm-tr="NavTool" data-gtm-herramienta="Waze">
                                    <?php echo do_shortcode( '[icono nombre="'.sanitize_title("waze").'"][/icono]' ); ?>
                                    <label>WAZE</label>
                                </a>
                            </div>
                            <div class="transport-pilot">
                                <a href="//m.uber.com/ul/?client_id=<CLIENT_ID>&action=setPickup&dropoff[latitude]=<?php echo $mapa['lat']; ?>&dropoff[longitude]=<?php echo $mapa['lng']; ?>" target="_blank" data-gtm-tr="NavTool" data-gtm-herramienta="Uber">
                                    <?php echo do_shortcode( '[icono nombre="'.sanitize_title("uber").'"][/icono]' ); ?>
                                    <label>UBER</label>
                                </a>
                            </div>
                        </span>
                        
                    </section>

                    <!-- Panel donde se muestra com llegar -->
                    <div id="right-panel"></div>

                    <div class="card-block pt-0">            
                        <div class="row">
                            <div class="col-12 p-0">

                                <div class="parametros-geolocalizacion">
                                    <!-- <div class="parametros">
                                        <div class="iconos">
                                            <?php echo do_shortcode( '[icono nombre="'.sanitize_title("localizacion").'"][/icono]' ); ?>
                                            <?php echo do_shortcode( '[icono nombre="'.sanitize_title("distancia").'"][/icono]' ); ?>
                                            <?php echo do_shortcode( '[icono nombre="'.sanitize_title("ubicacion").'"][/icono]' ); ?>
                                        </div>
                                        <form>
                                            <input type="text" id="mi-posicion" placeholder="Ingresa punto de partida">
                                            <input type="text" id="fake_mi-destino" value="<?php echo $postType->post_title; ?>" disabled>
                                        </form>
                                    </div> -->
                                    <!-- <a href="//maps.google.es/?q=<?php echo $mapa['address']; ?>" class="botonSecundario white-text" id="getDirections" target="_blank">CÓMO LLEGAR</a> -->


                                    <section class="botonesUnitec">
                                        <!-- <a class="btn back-naranja-unitec waves-effect waves-light w-100" id="getDirections" target="_blank" data-gtm-tr="Directions" data-gtm-campus="<?php echo $postType->post_title; ?>">CÓMO LLEGAR</a> -->
                                        <a class="btn back-naranja-unitec waves-effect waves-light w-100" href="//maps.google.es/?q=<?php echo $mapa['address']; ?>" target="_blank" data-gtm-tr="Directions" data-gtm-campus="<?php echo $postType->post_title; ?>">CÓMO LLEGAR</a>
                                    </section>
                                </div>
                                <?php
                                // check if the repeater field has rows of data
                                if( have_rows('contenido_collapsable') ){
                                    // loop through the rows of data
                                    while ( have_rows('contenido_collapsable') ) { the_row();
                                ?>
                                    <a class="simple-list" data-gtm-tr="NavOpt" data-gtm-campus="<?php echo $postType->post_title; ?>" data-gtm-ref="" data-gtm-type="<?php echo get_sub_field('titulo_collapsable'); ?>">
                                        <div type="button" class="list-item-container border-top-fix">
                                            <div class="item-icon list-icon">
                                            <?php echo do_shortcode( '[icono nombre="'.sanitize_title( get_sub_field('icono_collapsable') ).'"][/icono]' ); ?>
                                            </div>
                                            <div class="item-description"><?php echo get_sub_field('titulo_collapsable'); ?></div>
                                            <div class="arrow">
                                                <?php echo do_shortcode( '[icono nombre="'.sanitize_title("ver-abajo").'"][/icono]' ); ?>
                                            </div>
                                        </div>
                                    </a>
                                    <div class="text-colapse collapse animated flipInX">
                                        <div class="fix-horario-campus horario">
                                <?php                                        
                                        if( have_rows('item_referencia') ){
                                            // loop through the rows of data
                                            while ( have_rows('item_referencia') ) { the_row();
                                ?>                                                
                                            <a class="link-simple-list"><label class="horario-item"><label class="horario-hora"> <?php echo get_sub_field('referencia'); ?></label></label></a> 
                                <?php                                                
                                            }
                                        }
                                ?>
                                        </div>
                                    </div>
                                <?php
                                    }
                                }
                                ?>                                
                            </div>
                        </div>
                    </div>
                </div>

<!-- ***************************-> Termina tu código ... -->
            </div><!-- col-md-12 -->
        </div><!-- /.Live preview-->
        <!-- </div> Contenedor mapa -->
    </section><!-- SECTION-->
</main><!-- /.Main <layout-->