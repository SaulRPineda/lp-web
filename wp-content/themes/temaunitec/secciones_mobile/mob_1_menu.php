<?php
/*
 * WP Post Template: mob_1_menu
 */

/**
* Custom walker class.
*/
class sidenav extends Walker_Nav_Menu {
    public $mini_menu; 

    function start_lvl( &$output, $depth = 0, $args = array()) {
        $output .= "\n" . $indent . '<div class="collapsible-body"><ul>';
    }

    function end_lvl( &$output, $depth = 0, $args = array() ) {
        $output .=  "\n" . $indent . "</ul></div>";
    }

    function start_el( &$output, $item, $depth = 0, $args = array(), $id = 0 ) {
        //print_r($args);

        $indent = ( $depth > 0 ? str_repeat( "\t", $depth ) : '' ); // code indent

        // Passed classes.
        $classes = empty( $item->classes ) ? array() : (array) $item->classes;

        $class_names = esc_attr( implode( ' ', apply_filters( 'nav_menu_css_class', array_filter( $classes ), $item ) ) );
        

        $arrow = ( strrpos( $class_names, "menu-item-has-children") > -1 )? '<i class="icon-u icon-u-ver-abajo m-0"></i>': "";

        if( strrpos( $class_names, "menu-item-has-children") > -1 ){
            $parentHead = '<ul class="m-0 collapsible collapsible-accordion"><li class="m-0 p-0">';
            $class_names = "class='row p-0 m-0 collapsible-header waves-effect arrow-r'";
        }else{
            $parentHead = '<li class="m-0 p-0">';
            $class_names = "class='row p-0 m-0 waves-effect'";
            $arrowcollapse = "";
            $attributes = ! empty( $item->url )? ' href="' . esc_attr( $item->url) .'" ' : '';
        }
        
        if( $item->title == "hr" ){
            $output .= "<hr>";
        }else{
            $mm = array( "Preparatoria", "Licenciaturas", "Posgrados" );
            if( in_array( $item->title, $mm ) ){
                $this->mini_menu[$item->title] = $item->url;
            }
            // Build HTML.
            $output .= $indent . $parentHead . 
                '<a title="' . $item->title . '" ' . $class_names . $attributes . ' data-gtm-tr="NavOpt" data-gtm-menu="'.$item->title.'">' .
                    /*'<i class="icon-u icon-u-' . sanitize_title( $item->title ) . '"></i>' .*/
                    /*do_shortcode( '[icono nombre="'.sanitize_title($item->title).'"][/icono]' ).*/
                    do_shortcode( '[icono nombre="'.sanitize_title($item->classes[0]).'" clase="col-1 m-0 px-2 text-center"][/icono]' ).
                    '<div class="col-10 p-0 pl-3">'.$item->title .'</div>'.
                    $arrow .
                '</a>';
        }
    }

    function end_el( &$output, $item, $depth = 0, $args = array() ) {
        $output .= "</li>";
    }
}

$sideNav = new sidenav();
?>

<!-- Sidebar navigation -->
<ul id="UnitecSideNav" class="side-nav" style="display:none">
    <div id="side-nav-content" class="side-nav-content">
        <?php
            //Seleccionar la imagen dependiendo si es vertical u horizontal
            //Esta implementacion actualmente inactiva, solo para futuras referencias
            $img_by_position = wp_get_attachment_image_url( get_post_thumbnail_id( get_the_ID(), 'thumbnail') );
            $img_by_position = explode(site_url(), $img_by_position);
            $ruta_imagen = explode(basename($img_by_position[1]), $img_by_position[1]);
            $ruta_imagen = $ruta_imagen[0];
            $url_img_descataca =  site_url().$ruta_imagen;
            $custom_imagen_horizontal = get_field( 'imagen_destacada_horizontal');
        ?>
        <input type="hidden" id="sidenav_url_imagen_destacada" value="<?php echo $url_img_descataca; ?>">
        <input type="hidden" id="sidenav_nombre_imagen_destacada" value="<?php echo basename ( $img_by_position[1]) ?>">
        <div id="searchMenu" class="controls d-flex justify-content-between p-1">
            <!-- <div class="logo"><a href="<?php echo get_home_url(); ?>"><img class="lazy" data-src="<?php echo get_template_directory_uri() . "/assets/frontend/img/header/logo_unitec.svg"; ?>" alt="UNITEC"></a></div> -->
            <?php if( strpos($post->post_name,'calcula-tu-beca')===false ){
                echo '<searchmenu-app
        :bus="bus"
        :formgroup="form"  ref="menuBuscador">
        </searchmenu-app>';
            }
            ?>
            <span onClick="closeSideNav()" style="cursor:pointer; z-index: 9999;" class="icon-u icon-u-cerrar d-flex align-items-center"></span>
        </div>
        <!-- <div class="side-nav-header" style="background: linear-gradient( rgba(0,0,0,.4), rgba(0,0,0,.4) ), url('<?php echo $custom_imagen_horizontal["url"]; ?>'); background-size:cover;"> -->
            
            <!-- <img src="<?php echo wp_get_attachment_url( get_post_thumbnail_id( get_the_ID(), 'thumbnail') ); ?>" class="img-fluid img-head" style:"visibility: hidden"> -->
        <!-- </div> -->
            <?php 
           if( strpos($post->post_name,'calcula-tu-beca')===false ){
                //echo "<search-side-nav></search-side-nav>";
            }
            ?>

        <ul class="collapsible collapsible-accordion">
            <!-- <?php // wp_nav_menu( array( 'menu' => 'sidenav', 'walker' => $sideNav ) ); ?> -->
            <?php wp_nav_menu( array( 'menu' => 'sidenav', 'walker' => $sideNav ) ); ?>
        </ul>

    </div>
</ul>

<!-- elemento para dragable sidenav-->
    <!-- SideNav UnitecSideNav button -->
    <div style="height:1px"></div>
    <div class="nvct">
        <?php
            // if( strpos($post->post_name,'impulsa') !== false  ){
            //     $urlBtn = ( $_COOKIE["reg_impulsa"] ) ? 'href="https://www.unitec.mx/folleto/impulsa.pdf"' : 'onclick="openModal(\'#modal_frm_app\')"';
            // } else{
            //     $urlBtn = 'onclick="openModal(\'#modal_frm_app\')"';
            // }
            $urlBtn = 'onclick="openModal(\'#modal_frm_app\')"';
        ?>
        <div id="searchHead" class="navContent navegador">
            <a href="javascript:void(0);" data-activates="UnitecSideNav" class="btnMenuMobile" style="cursor:pointer; z-index: 9999;"><i class="icon-u-menu"></i></a>
            <a href="<?php echo get_home_url(); ?>" class="logoNav">
                <img src="<?php echo get_template_directory_uri() . "/assets/frontend/img/footer/svg/unitec.svg"; ?>" alt="UNITEC">
            </a>

        <!-- Icono de entrar biblioteca -->  
            <a data-gtm-tr="frmTradicional" data-gtm-pos="header" id="login-biblioteca" onclick="openModal('#modal_frm_biblioteca')" class="searchNav d-none"><?php echo do_shortcode( '[icono nombre="'.sanitize_title("login").'"][/icono]' ); ?></a>
        <!-- Icono de salir biblioteca -->  
            <a id="logout-biblioteca" onclick="cerrarSesion()" class="searchNav d-none"><?php echo do_shortcode( '[icono nombre="'.sanitize_title("logout").'"][/icono]' ); ?></a>
        <!-- Icono de Whatsapp -->
            <!-- <a data-gtm-tr="whats" data-gtm-pos="header" href="https://clxt.ch/unitecg" target="_blank" onclick="openModal('#whatsapp')" cla   ss="searchNav"><?php echo do_shortcode( '[icono nombre="'.sanitize_title("whatsapp-footer").'"][/icono]' ); ?></a> -->
        <!-- Icono de sobre para contacto Formulario -->
        <?php if( strpos($post->post_name,'calcula-tu-beca')===false ){?>  
            <a id="calculadoraModal" <?php echo $urlBtn; ?> class="searchNav" data-solicitud-location="Header"><?php echo do_shortcode( '[icono nombre="'.sanitize_title("formularios").'"][/icono]' ); ?></a>
        <?php }?>
        <?php if(strpos($post->post_name,'calcula-tu-beca')===false) { ?>
        <!-- Icono de Chat -->
            <a class="searchNav" data-gtm-tr="chat" data-gtm-pos="intencion" data-gtm-location="header" data-gtm-etiqueta="con formulario" onclick="openChatLive()"><div id="chatMsj-int" class="d-flex justify-content-end"></div><?php echo do_shortcode( '[icono nombre="'.sanitize_title("chat").'"][/icono]' ); ?></a>
        <?php }?>
            <!-- Icono de Telefono -->  
            <a data-gtm-tr="call" data-gtm-pos="header" data-gtm-org="" href="tel:8007864832" class="searchNav"><?php echo do_shortcode( '[icono nombre="'.sanitize_title("llamar").'"][/icono]' ); ?></a>
            <!-- <a data-gtm-tr="call" data-gtm-pos="intencion ctc header" onclick="openModal('#modal-ctc')" class="searchNav"><?php echo do_shortcode( '[icono nombre="'.sanitize_title("llamar").'"][/icono]' ); ?></a> -->
        <!-- Icono de Buscador -->  
        <?php if( strpos($post->post_name,'calcula-tu-beca')===false ){?>
            <!-- <a onclick="openModal('#buscador')" class="searchNav" data-gtm-tr="IntSearch" data-gtm-seccion="<?php echo $post->post_title ?>" ><?php echo do_shortcode( '[icono nombre="'.sanitize_title("buscar").'"][/icono]' ); ?></a> -->
        <?php }?>
        
        <search-app
        :bus="bus"
        :formgroup="form" ref="menuBuscador2">
        </search-app>
        
        </div>
    </div>
