<?php
/*
 * WP Post Template: mob_2_4_banner_video_360
 */
 ?>

<div class="margen-top-menu"> </div> 
<div id="banner-360" class="banner banner-360 mb-1">
    <div id="yt-container" class="hs-responsive-embed-youtube">
        <div id="M08MEtNaTlM" class="youtube-video"></div>
        <div class="banner-cinta cinta-end">
            <span class="cintillo">
                <label class="banner-cintillo-titulo">
                    <span class="orange-text roboto-medium">|</span>
                    Licenciado en cirujano dentista
                </label>
            </span>
            <a href="#start-page" class="scroll-spy btnB btn-banner-cintillo"><i class="icon-u-ver-abajo"></i></a>
        </div>
    </div>
</div>
<span id="start-page"></span>