<?php
/**
 * WP Post Template: mob_2_15_virtual_tour
 */
?>
<?php
    $postType = get_post( $idPost );
    $tituloTourVirtual = get_field('titulo_banner_tour_virtual', $theID);
    $descriptionOneTour = get_field('descripcion_one_tour_virtual', $theID);
    $descriptionTwoTour = get_field('descripcion_two_tour_virtual', $theID);
    $selectorTipoImg = get_field('selector_tipo_de_imagen_banner', $theID);

    $imgTourMob = get_field('img_banner_tour_mobile', $theID);
        $urlTourMobImgH = $imgTourMob["img_tour_mobile_h"]["url"];
        $altTourMob = $imgTourMob["img_tour_mobile_h"]["alt"];
        $urlTourMobImgV = $imgTourMob["img_tour_mobile_v"]["url"];

    $imgTourMob360 = get_field('img_banner_tour_mobile_360', $theID);
        $urlTourMobImg360 = $imgTourMob360["url"];
        $altTourMob360 = $imgTourMob360["alt"];

    $callToAction = get_field('action_to_tour_virtual', $theID);
    $textCallToActionTour = $callToAction['text_call_to_action'];
    $urlCallToActionTour = $callToAction['url_call_to_action'];

    $bannerimg['vertical']=$urlTourMobImgV;
    $bannerimg['horizontal']=$urlTourMobImgH;

    switch ($selectorTipoImg) {
         case 'imagen_360':
            $imgFondo = "<script defer type='text/javascript' src='https://storage.googleapis.com/vrview/2.0/build/vrview.min.js'></script>";
            $div360 = "<div id='vr-view' class='w-100 h-100'></div>";
         break;
 
         case 'imagen_fija':
            $imgFondo = "<div id='banner-img-tour' class='p-absolute w-100 h-100'></div>";
            $div360 = "";
         break;
         
         default:
            $imgFondo = "<div id='banner-img-tour' class='p-absolute w-100 h-100'></div>";
            $div360 = "";
         break;
     }

    $jsonBanner=json_encode($bannerimg);

?>
<script type="text/javascript">
var jsonBanner = <?php echo $jsonBanner; ?>;
var tipobanner = '<?php echo $selectorTipoImg; ?>';
var urlTourMobImg360 = '<?php echo $urlTourMobImg360; ?>';
jQuery(document).ready(function(){
    console.warn('URL 360: '+urlTourMobImg360);
    var orient = orientacionDispositivo();
    ajusteBanner(orient,jsonBanner,tipobanner);
    bannerHeight();

    window.addEventListener("orientationchange", function() {
        orient = orientacionDispositivo();
        ajusteBanner(orient, jsonBanner, tipobanner);
        bannerHeight();
    }, false);
    jQuery('.btnB').css('margin','0');
});

function orientacionDispositivo() {
    var windowOrientation = Math.abs(window.orientation);
    var tipo;
    if (windowOrientation === 90) {
        tipo= "horizontal";
    } else {
        tipo= "vertical";
    }
    return tipo;
}
function ajusteBanner(orient, jsonBanner, tipobanner){
    switch(tipobanner){
        case 'imagen_fija':
            if(orient=="vertical") {
                jQuery('#banner-img-tour').css('background-image', 'url('+jsonBanner.vertical+')').promise().done(function(){
                    jQuery(this).animate({
                        opacity: 1
                    }, 600)
                });

            } else if(orient=="horizontal"){
                jQuery('#banner-img-tour').css('background-image', 'url('+jsonBanner.horizontal+')').promise().done(function(){
                    jQuery(this).animate({
                        opacity: 1
                    }, 600)
                }); 

            } else{
                jQuery('#banner-img-tour').csscss('background-image', 'url('+jsonBanner.vertical+')').promise().done(function(){
                    jQuery(this).animate({
                        opacity: 1
                    }, 600)
                });
            }
        break;
        case 'imagen_360':
            window.addEventListener('load', onVrViewLoad);
            jQuery('#banner-imagen').css('z-index','0');
        break;
    }
    bannerHeight();     
}
function bannerHeight(){
    setTimeout(function(){
        var heightNavbar = jQuery(".nvct").height();
        var height = jQuery(window).height() - parseInt(heightNavbar);  //getting windows height
        jQuery('#banner-imagen').css({'height': height+'px', 'margin-top': heightNavbar+'px'});
    }, 500);
}

function onVrViewLoad() {
  var vrView = new VRView.Player('#vr-view', {
    image: urlTourMobImg360,
    width: '100%',
    height: '100%',
    is_vr_off: true,
    is_stereo: true
  });
}
function openTourVirtual(campus){
    localStorage.setItem("abrirModalTour", true);
    window.location.href = campus;
}
</script>

<section id="banner-imagen" class="banner banner-imagen d-flex justify-content-center align-items-center">
    <?php echo $imgFondo; ?>
    <?php echo $div360; ?>
    <div class="info-tour row d-flex justify-content-center m-0">
        <div class="col-12 d-flex flex-column align-items-center justify-content-center p-0">
            <span class="title-campus-tour"><b><?php echo $tituloTourVirtual; ?></b></span>
            <svg viewBox="0 0 100 100" style="width:100px;">
                <g style="transform-origin:50px 50px 0px;transform:scale(0.9)">
                    <g style="transform-origin:50px 50px 0px">
                        <g style="transform-origin:50px 50px 0px">
                            <g class="ld ld-breath" style="animation-delay:-2.2s;animation-direction:normal;animation-duration:2.2s;transform-origin:50px 50px 0px">
                                <circle cx="50" cy="50" r="27.5" fill="none" stroke="#fff" stroke-linecap="round" stroke-linejoin="round" stroke-miterlimit="10" stroke-width="3"/>
                            </g>
                            <g class="ld ld-breath" style="animation-delay:-2.112s;animation-direction:normal;animation-duration:2.2s;transform-origin:50px 50px 0px">
                                <ellipse cx="50" cy="50" rx="15" ry="27.5" fill="none" stroke="#fff" stroke-linecap="round" stroke-linejoin="round" stroke-miterlimit="10" stroke-width="3"/>
                            </g><g class="ld ld-breath" style="animation-delay:-2.024s;animation-direction:normal;animation-duration:2.2s;transform-origin:50px 50px 0px">
                                <line x1="50" x2="50" y1="22.5" y2="77.5" fill="none" stroke="#fff" stroke-linecap="round" stroke-linejoin="round" stroke-miterlimit="10" stroke-width="3"/>
                            </g><g class="ld ld-breath" style="animation-delay:-1.936s;animation-direction:normal;animation-duration:2.2s;transform-origin:50px 50px 0px">
                                <line x1="24.365" x2="75.686" y1="40.046" y2="40.046" fill="none" stroke="#fff" stroke-linecap="round" stroke-linejoin="round" stroke-miterlimit="10" stroke-width="3"/>
                            </g>
                            <g class="ld ld-breath" style="animation-delay:-1.848s;animation-direction:normal;animation-duration:2.2s;transform-origin:50px 50px 0px">
                                <line x1="24.365" x2="75.686" y1="59.454" y2="59.454" fill="none" stroke="#fff" stroke-linecap="round" stroke-linejoin="round" stroke-miterlimit="10" stroke-width="3"/>
                            </g>
                        </g>
                        <g style="transform-origin:50px 50px 0px">
                            <g style="transform-origin:50px 50px 0px">
                                <g class="ld ld-breath" style="animation-delay:-1.76s;animation-direction:normal;animation-duration:2.2s;transform-origin:50px 50px 0px">
                                    <path d="M30.002,84.637 C18.049,77.72,10,64.804,10,50c0-20.492,15.409-37.385,35.273-39.724" fill="none" stroke="#fff" stroke-linecap="round" stroke-linejoin="round" stroke-width="3"/>
                                </g>
                                <g style="transform-origin:50px 50px 0px">
                                    <g class="ld ld-breath" style="animation-delay:-1.672s;animation-direction:normal;animation-duration:2.2s;transform-origin:50px 50px 0px">
                                        <polygon points="44.157 4.939 44.94 15.844 50 10" fill="#fff"/>
                                    </g>
                                </g>
                            </g>
                        </g>
                        <g style="transform-origin:50px 50px 0px">
                            <g style="transform-origin:50px 50px 0px">
                                <g class="ld ld-breath" style="animation-delay:-1.584s;animation-direction:normal;animation-duration:2.2s;transform-origin:50px 50px 0px">
                                    <path d="M69.998,15.363 C81.951,22.28,90,35.196,90,50c0,20.492-15.409,37.385-35.273,39.724" fill="none" stroke="#fff" stroke-linecap="round" stroke-linejoin="round" stroke-width="3"/>
                                </g>
                                <g style="transform-origin:50px 50px 0px">
                                    <g class="ld ld-breath" style="animation-delay:-1.496s;animation-direction:normal;animation-duration:2.2s;transform-origin:50px 50px 0px">
                                        <polygon points="55.843 95.061 55.06 84.156 50 90" fill="#fff"/>
                                    </g>
                                </g>
                            </g>
                        </g>
                    </g>
                </g>
            </svg>
            <span class="title-campus-tour text-center"><?php echo $descriptionOneTour; ?></span>
            <span class="subtitle-campus-tour text-center my-2"><?php echo $descriptionTwoTour; ?></span>
            <a onclick="openTourVirtual('<?php echo $urlCallToActionTour; ?>/#tour_virtual')" type="button" class="btn btn-back-tour waves-effect waves-light">
                <span>&#9658; <?php echo $textCallToActionTour; ?></span>
            </a> 
        </div>
    </div>
    <a href="#start-page" class="scroll-spy btnB btn-banner"><?php echo do_shortcode( '[icono nombre="'.sanitize_title("ver-abajo").'"][/icono]' ); ?></a>
</section>
<span id="start-page"></span>