<div id="nuevas_carreras">  </div>

<?php
/**
 * WP Post Template: mob_5_nuevas_carreras
 */
?>

<?php 
    //$nuevaCarrera = get_field('card_nueva_carrera', $theID);
    $nuevaCarrera = get_field('nuevas_carreras', $theID);
    $titulo = get_field('titulo_nuevas_carreras', $theID);
    $boton = get_field('boton', $theID); 
    $active = NULL;   
?>


<main class="col-12 mt-0"><!-- /.Main <layout-->
    <section id="elegant-card"><!-- SECTION-->
        <div class="row"><!-- Live preview-->
            <div class="col-md-12"><!-- col-md-12 -->
                <!--Carousel Wrapper--><div id="prueba"></div>
                <div id="carruselNuevasCarreras" class="carousel slide carousel-fade" data-ride="carruselNuevasCarreras">
                    <!-- Slides -->
                    <div class="carousel-inner" role="listbox">

                    <?php foreach ($nuevaCarrera as $key => $contenido) { ?>
                        <?php if ($key == 0) { $active = "active"; } ?>
                        <!-- First slide -->
                        <div class="carousel-item <?php echo $active; ?>">
                            <!-- Card -->
                            <div class="card">
                                <!-- Title -->
                                <h4 class="card-title"><?php echo $titulo; ?></h4>
                                
                            <?php 
                                $thumbID = get_post_thumbnail_id( $contenido->ID );
                                $urlImg = wp_get_attachment_url( $thumbID );
                                $altImg = get_post_meta($thumbID, '_wp_attachment_image_alt', true);

                                $imgHorizontal = get_field('imagen_destacada_horizontal', $contenido->ID);
                                $urlImgHorizontal = $imgHorizontal['url'];
                            ?>

                            <!-- card image -->
                                <div class="view overlay hm-white-slight">
                                    <a href="<?php echo get_permalink($contenido->ID); ?>" data-gtm-tr="NewCareers" data-gtm-value="<?php echo $contenido->post_title; ?>" data-gtm-interaccion="Imagen">
                                        <img alt="<?php echo $altImg; ?>" class="img-fluid lazy" data-src="<?php echo $urlImgHorizontal; ?>">
                                    </a>
                                    <!-- class="mask waves-effect waves-light" -->
                                </div>
                                <!-- Button -->
                                <a href="<?php echo get_permalink($contenido->ID); ?>" class="btn-floating btn-action"><?php echo do_shortcode( '[icono nombre="'.sanitize_title("ver-mas").'"][/icono]' ); ?></a>
                                <!-- card image -->

                                <!-- Aqui van los componentes list_icon, list, texto, cascade, footer, image, collapse -->
                                <div class="card-block pb16">
                                    <div class="row">
                                        <div class="col-12">
                                            <h4 class="title-nuevas-carreras card-content-title mt-1"><?php echo $contenido->post_title; ?></h4>
                                            <p class="card-text">
                                                <?php echo get_field('descripcion_del_producto', $contenido->ID); ?>
                                            </p>
                                            <a href="<?php echo get_permalink($contenido->ID); ?>" class="linkSecundario" data-gtm-tr="NewCareers" data-gtm-value="<?php echo $contenido->post_title; ?>" data-gtm-interaccion="Ver más"><?php echo $boton; ?></a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- /.Card -->
                        </div>
                        <!-- /.First slide -->
                        <?php $active = NULL; ?>
                  <?php } ?>

                     </div><br>
                   <!--  /.Slides -->
                    
                    <!-- Validacion de No de Items en el carrusel -->
                    <?php if ( count($nuevaCarrera) > 1) { ?>
                   <!--  Indicators -->
                    <ol class="carousel-indicators">
                        <?php 
                            for ($i=0; $i < count($nuevaCarrera); $i++) {
                        ?>
                            <li data-target="#carruselNuevasCarreras" data-slide-to="<?php echo $i; ?>" <?php if($i==0){ ?> class="active" <?php } ?>></li>
                        <?php
                            }
                        ?>        
                    </ol>
                    <!-- /.Indicators -->
                    <?php } ?>
                </div>
                <!--/.Carousel Wrapper-->
<!-- ***************************-> Inicia tu código ... -->
<!-- ***************************-> Termina tu código ... -->
            </div><!-- col-md-12 -->
        </div><!-- /.Live preview-->
    </section><!-- SECTION-->
</main><!-- /.Main <layout-->