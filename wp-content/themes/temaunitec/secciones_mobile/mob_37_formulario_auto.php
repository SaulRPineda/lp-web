<div id="formulario-main-mobile" style="display:none; padding-top: 52px;">
    <div id="footer">
        <div class="container-full">
            <div class="row items-center justify-center info-footer">
                <div class="col col-4 ctrl btn-footer" data-ir="sec-cursar">Quiero cursar<br><br>
                    <span class="rs-carrera m">
                    </span>
                </div>
                <div class="col col-4 ctrl" >
                    <div>Promedio</div>
                    <div class="clearfix">
                        <div class="col col-4 prom-ctrl right-align ctrlbtn" data-valor="min">-</div>
                        <div class="col col-4 output m"></div>
                        <div class="col col-4 prom-ctrl left-align ctrlbtn" data-valor="mas">+</div>
                    </div>
                </div>
                <div class="col col-4 ctrl">
                    <div>Materias</div>
                    <div class="clearfix">
                        <div class="col col-4 mat-ctrl right-align ctrlbtn" data-valor="min">-</div>
                        <div class="col col-4 ctrlrs-materia m rs-materias display-none"></div>
                        <div class="col col-4 mat-ctrl  left-align ctrlbtn" data-valor="mas">+</div>
                    </div>
                </div>
            </div>
        </div>
    </div>
      <div id="fullpage">
         <div class="section fp-auto-height-responsive" data-anchor="calculadora" id="sec0">
            <p class="h2 sub subInteres" id="subInteresTitle">¿Qué área de estudio te interesa?</p>
            <div class="slide" data-anchor="sec-cursar" id="sec-cursar">
               <div id="business-line" class="row md-hide lg-hide mobcont">
                  <!-- Económico-administrativo -->
                  <div class="max-width-4 mx-auto linea-mobile">
                     <div class="clearfix px1 py2 relative center line-b">
                        <div class="col col-2">
                           <div data-pos="pr" data-val="ECOADMIN" class="expandible cerrado m100 clearfix absolute" style="cursor: pointer;"></div>
                           <div style="font-size:4vh;" class="imagen-lineas-cuadros cuadro icon-u icon-u-administrativas">
                           </div>
                        </div>
                        <div class="col col-10 line">LIC. ECONÓMICO-ADMINISTRATIVO</div>
                     </div>
                  </div>
                  <!-- Económico-administrativo -->
                  <!-- Salud -->
                  <div class="max-width-4 mx-auto linea-mobile">
                     <div class="clearfix px1 py2 relative center line-b">
                        <div class="col col-2">
                           <div data-pos="pr" data-val="SALUD" class="expandible cerrado m100 clearfix absolute" style="cursor: pointer;"></div>
                           <div style="font-size:4vh;" class="imagen-lineas-cuadros cuadro icon-u icon-u-salud">
                           </div>
                        </div>
                        <div class="col col-10 line">LIC. CIENCIAS DE LA SALUD</div>
                     </div>
                  </div>
                  <!-- Salud -->
                  <!-- Turismo y Gastronomia -->
                  <div class="max-width-4 mx-auto linea-mobile">
                     <div class="clearfix px1 py2 relative center line-b">
                        <div class="col col-2">
                           <div data-pos="pr" data-val="TURISMO" class="expandible cerrado m100 clearfix absolute" style="cursor: pointer;"></div>
                           <div style="font-size:4vh;" class="imagen-lineas-cuadros cuadro icon-u icon-u-turismo-gastronomia">
                           </div>
                        </div>
                        <div class="col col-10 line">LIC. TURISMO Y GASTRONOMÍA</div>
                     </div>
                  </div>
                  <!-- Turismo y Gastronomia  -->
                  <!-- Ciencias y Artes para el Diseño -->
                  <div class="max-width-4 mx-auto linea-mobile">
                     <div class="clearfix px1 py2 relative center line-b">
                        <div class="col col-2">
                           <div data-pos="in" data-val="DISENO" class="expandible cerrado m100 clearfix absolute" style="cursor: pointer;"></div>
                           <div style="font-size:4vh;" class="imagen-lineas-cuadros cuadro icon-u icon-u-ciencias-artes">
                           </div>
                        </div>
                        <div class="col col-10 line">LIC. ARTES PARA EL DISEÑO</div>
                     </div>
                  </div>
                  <!-- Ciencias y Artes para el Diseño -->
                  <!-- Cmunicación y Mercadotecnia -->
                  <div class="max-width-4 mx-auto linea-mobile">
                     <div class="clearfix px1 py2 relative center line-b">
                        <div class="col col-2">
                           <div data-pos="in" data-val="MERCADOTECNIA" class="expandible cerrado m100 clearfix absolute" style="cursor: pointer;"></div>
                           <div style="font-size:4vh;" class="imagen-lineas-cuadros cuadro icon-u icon-u-comunicacion-merca">
                           </div>
                        </div>
                        <div class="col col-10 line">LIC. COMUNICACIÓN Y MERCADOTECNIA</div>
                     </div>
                  </div>
                  <!-- Comunicación y Mercadotecnia -->
                  <!-- Prepa -->
                  <div class="max-width-4 mx-auto linea-mobile">
                     <div class="clearfix px1 py2 relative center line-b">
                        <div class="col col-2">
                           <div data-pos="pr" data-val="PREPARATORIA" class="expandible cerrado m100 clearfix absolute" style="cursor: pointer;"></div>
                           <div style="font-size:4vh;" class="imagen-lineas-cuadros cuadro icon-u icon-u-preparatoria">
                           </div>
                        </div>
                        <div class="col col-10 line">PREPARATORIA</div>
                     </div>
                  </div>
                  <!-- Prepa -->
                  <!-- ingenieria -->
                  <div class="max-width-4 mx-auto linea-mobile">
                     <div class="clearfix px1 py2 relative center line-b">
                        <div class="col col-2">
                           <div data-pos="in" data-val="INGENIERIA" class="expandible cerrado m100 clearfix absolute" style="cursor: pointer;"></div>
                           <div style="font-size:4vh;" class="imagen-lineas-cuadros cuadro icon-u icon-u-ingenierias">
                           </div>
                        </div>
                        <div class="col col-10 line">INGENIERÍA</div>
                     </div>
                  </div>
                  <!-- ingenieria -->
                  <!-- licenciatura -->
                  <div class="max-width-4 mx-auto linea-mobile">
                     <div class="clearfix px1 py2 relative center line-b">
                        <div class="col col-2">
                           <div data-pos="li" data-val="LICENCIATURA" class="expandible cerrado m100 clearfix absolute" style="cursor: pointer;"></div>
                           <div style="font-size:4vh;" class="imagen-lineas-cuadros cuadro icon-u icon-u-licenciaturas">
                           </div>
                        </div>
                        <div class="col col-10 line">OTRA</div>
                     </div>
                  </div>
                  <!-- licenciatura -->
                  <!-- SALUD -->
                  <!-- <div class="max-width-4 mx-auto linea-mobile">
                     <div class="clearfix px1 py2 relative center line-b">
                        <div class="col col-2">
                           <div data-pos="sa" data-val="ONLINE" class="expandible cerrado m100 clearfix absolute" style="cursor: pointer;"></div>
                           <div class="imagen-lineas-cuadros cuadro ONLINE">
                           </div>
                        </div>
                        <div class="col col-10 line">LIC EN LÍNEA</div>
                     </div>
                  </div> -->
                  <!-- SALUD -->
                  
                  <!-- DIPLOMADOS -->
                  <!-- <div class="max-width-4 mx-auto linea-mobile">
                     <div class="clearfix px1 py2 relative center line-b">
                        <div class="col col-2>
                           <div data-pos="po" data-val="DIPLOMADO" class="expandible cerrado m100 clearfix absolute" style="cursor: pointer;"></div>
                           <div class="imagen-lineas-cuadros cuadro DIP">
                           </div>
                        </div>
                        <div class="col col-10 line">DIPLOMADOS</div>
                     </div>
                  </div> -->
                  <!-- POSGRADO -->
                  <!-- POSGRADO -->
                  <!-- <div class="max-width-4 mx-auto linea-mobile">
                     <div class="clearfix px1 py2 relative center line-b">
                        <div class="col col-2>
                           <div data-pos="po" data-val="POSGRADO" class="expandible cerrado m100 clearfix absolute" style="cursor: pointer;"></div>
                           <div class="imagen-lineas-cuadros cuadro POS">
                           </div>
                        </div>
                        <div class="col col-10 line">POSGRADO</div>
                     </div>
                  </div> -->
                  <!-- POSGRADO -->
               </div>
               <!-- TERMINA seccion MOBILEE -->
               <!-- seccion desktop -->
            </div>
         </div>
          <!-- <div class="caja px3 pt3 lg-px4 xs-p1 m1">
            <div id="ciclosTituloMob" class="row row-menu-app-form hide">
               <div class="col-12 h2" style="text-align:center">¿Cuál es el periodo de tu interés?</div>  
            </div>
            <div id="secCiclos" class="quest w-80 hide">
               <div class="row row-ciclos adapt-desk-lineas col-12" style="justify-content: center;">
               </div>
               <div class="clearfix px1 py2">
                  <div class="col col-12 text-left ">
                     <button onclick="regresaContenedor()" class="iconb btn-regresar-ciclos">Regresar</button>
                  </div>
                  <div class="col col-6  sm-col-right ">
                     <button  onClick="javascript:irA('secCiclos','openThankYouCiclos')" class=" right button-primaryn">Avanzar</button>
                  </div>
               </div>
            </div>
          </div> -->
      </div>
      <div class="plantillas" style="display:none">
         <div class="p_expandible">
            <div class="caja px3 pt3 lg-px4 xs-p1 m1">
               <div class="clearfix preg-est" >
                  <p class="h2 sub xs-left-align" id="subtituloInteres">¿Qué quieres estudiar?</p>
                  <div class="py2 xs-py1">
                     <input class="input sm-col sm-col-5 project" placeholder="Buscar..." id="project">
                     <input type="hidden" class="project-id" id="project-id">
                     <div id="projectSelected" hidden></div>
                  </div>
               </div>
               <div class="clearfix v_mod display-none">
                  <div class="col col-12 pr3 xs-pr0 ">
                     <p class="h2 sub xs-left-align ">¿Qué modalidad te interesa?</p>
                     <div class="py2 xs-py1">
                        <select class="input sm-col sm-col-5" id="render_modalidad"></select>
                     </div>
                  </div>
               </div>
               <div class="clearfix v_camp display-none">
                  <div class="sm-col sm-col-5 pr3 xs-pr0 ">
                     <p class="h2 sub xs-left-align ">¿En qué campus?</p>
                     <div class="py2 xs-py1">
                        <select class="input sm-col sm-col-5" id="render_campus"></select>
                     </div>
                  </div>
               </div>
               <div class="clearfix v_prepa display-none">
                  <div class="sm-col sm-col-5 pr3 xs-pr0 ">
                     <p class="h2 sub xs-left-align ">¿Eres padre o tutor?</p>
                     <div class="py2 xs-py1">
                        <select class="input sm-col sm-col-5" id="render_prepa"></select>
                     </div>
                  </div>
               </div>
               <div class="clearfix  ">
                   <p class="err_cursar"></p>
               </div>
               <div class="clearfix px1 py2">
                  <div class="col col-6 text-left ">
                     <button class="cerrar iconb">Regresar</button>
                  </div>
                  <div class="col col-6  sm-col-right ">
                     <button  onClick="javascript:irA('calculadora','openThankYou')" class=" right button-primaryn">Avanzar</button>
                  </div>
               </div>
            </div>
         </div>
      </div>
</div>