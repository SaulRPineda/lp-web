
<?php
/**
 * WP Post Template: mob_36_calculadora_hb
 */
?>
<!-- <button class="muestracalcu">Prueba</button> -->

<div class="calcuApp" style="display:none">
  <app-root>Loading...</app-root>
  <input type="hidden" value="CALCULADORA" id="banner">
  <input type="hidden" value="" id="url_referrer">
</div>

<?php
//$urlwp = "http://localhost:8888/unitec2017/wp-content/themes/temaunitec";
//$urlwp2 = "http://localhost:8888/unitec2017";

$urlwp = "https://www.unitec.mx/wp-content/themes/temaunitec";
$urlwp2 = "https://www.unitec.mx";

$critical_min = $urlwp."/assets/frontend/css/vendor/min/critical.min.css";
echo "<style>".file_get_contents($fullpage_min).file_get_contents($critical_min)."</style>";
?>

<!-- <link rel="stylesheet" href="<?php echo $urlwp; ?>/assets/frontend_desktop/css/vendor/icon-u.css"> -->

<script type="text/javascript">var urlwp = "<?php echo $urlwp; ?>";</script>
<!-- <script async src="<?php echo $urlwp;?>/assets/frontend_desktop/js/secciones/min/desk_0_1_calculadora.min.js"></script> -->
<script type="text/javascript" src="<?php echo $urlwp2; ?>/kalc/desk_0_1_calculadora.js"></script>
<script async src="<?php echo $urlwp;?>/assets/frontend/js/calculadora_integracion_hb/min/funciones-criticas.min.js"></script>

<div id="calculadora-main-mobile" style="display:none">
    <div id="footer">
        <div class="container-full">
            <div class="row items-center justify-center info-footer">
                <div class="col col-4 ctrl btn-footer" data-ir="sec-cursar" id="footercalc1">Quiero cursar<br><br>
                    <span class="rs-carrera m">
                    </span>
                </div>
                <div class="col col-4 ctrl" id="footercalc2">
                    <div>Promedio</div>
                    <div class="clearfix">
                        <div class="col col-4 prom-ctrl right-align ctrlbtn" data-valor="min">-</div>
                        <div class="col col-4 output m"></div>
                        <div class="col col-4 prom-ctrl left-align ctrlbtn" data-valor="mas">+</div>
                    </div>
                </div>
                <div class="col col-4 ctrl" id="footercalc3">
                    <div>Materias</div>
                    <div class="clearfix">
                        <div class="col col-4 mat-ctrl right-align ctrlbtn" data-valor="min">-</div>
                        <div class="col col-4 ctrlrs-materia m rs-materias display-none"></div>
                        <div class="col col-4 mat-ctrl  left-align ctrlbtn" data-valor="mas">+</div>
                    </div>
                </div>
            </div>
        </div>
    </div>
      <div id="fullpage">
         <div class="section fp-auto-height-responsive" data-anchor="calculadora" id="sec0">
            <div class="slide" data-anchor="sec-cursar" id="sec-cursar">
               <div id="cont-bl" class="row md-hide lg-hide mobcont">
                  <!-- PREPARATORIA -->
                  <div class="max-width-4 mx-auto mt3 xs-mt1 xs-mb1 linea-mobile">
                     <div class="clearfix pb2 relative center line-b">
                        <div class="col col-2 pl2 ">
                           <div data-pos="pr" data-val="PREPARATORIA" class="expandible cerrado clearfix m100 absolute" style="cursor: pointer;"></div>
                           <div class="imagen-lineas-cuadros cuadro PREPA">
                              <!-- <img data-src="<?php echo $urlwp; ?>/assets/frontend/img/preparatoria_mobile.png" class="circle" alt="Calcula tu Beca Preparatoria"> -->
                           </div>
                        </div>
                        <div class="col col-9 line">PREPARATORIA</div>
                     </div>
                  </div>
                  <!-- PREPARATORIA -->
                  <!-- licenciatura -->
                  <div class="max-width-4 mx-auto linea-mobile">
                     <div class="clearfix pb2 relative center line-b">
                        <div class="col col-2 pl2 ">
                           <div data-pos="li" data-val="LICENCIATURA" class="expandible cerrado clearfix m100 absolute" style="cursor: pointer;"></div>
                           <div class="imagen-lineas-cuadros cuadro UG">
                              <!-- <img data-src="<?php echo $urlwp; ?>/assets/frontend/img/licenciaturas_mobile.png" class="circle" alt="Calcula tu Beca Licenciaturas"> -->
                           </div>
                        </div>
                        <div class="col col-9 p2  line">LICENCIATURA</div>
                     </div>
                  </div>
                  <!-- licenciatura -->
                  <!-- ingenieria -->
                  <div class="max-width-4 mx-auto linea-mobile">
                     <div class="clearfix pb2 relative center line-b">
                        <div class="col col-2 pl2 ">
                           <div data-pos="in" data-val="INGENIERIA" class="expandible cerrado clearfix m100 absolute" style="cursor: pointer;"></div>
                           <div class="imagen-lineas-cuadros cuadro ING">
                              <!-- <img data-src="<?php echo $urlwp; ?>/assets/frontend/img/ingenierias_mobile.png" class="circle" alt="Calcula tu Beca Ingenierías"> -->
                           </div>
                        </div>
                        <div class="col col-9 line">INGENIERÍA</div>
                     </div>
                  </div>
                  <!-- ingenieria -->
                  <!-- SALUD -->
                  <div class="max-width-4 mx-auto linea-mobile">
                     <div class="clearfix pb2 relative center line-b">
                        <div class="col col-2 pl2 ">
                           <div data-pos="sa" data-val="SALUD" class="expandible cerrado clearfix m100 absolute" style="cursor: pointer;"></div>
                           <div class="imagen-lineas-cuadros cuadro CS">
                              <!-- <img data-src="<?php echo $urlwp; ?>/assets/frontend/img/salud_mobile.png" class="circle" alt="Calcula tu Beca Salud"> -->
                           </div>
                        </div>
                        <div class="col col-9 line">SALUD</div>
                     </div>
                  </div>
                  <!-- SALUD -->
                  <!-- DIPLOMADOS Educación Continua y de Lista By SRP Descomentar hasta Marzo 2019-->
                  <div class="max-width-4 mx-auto linea-mobile">
                     <div class="clearfix pb2 relative center line-b">
                        <div class="col col-2 pl2">
                           <div data-pos="po" data-val="DIPLOMADO" class="expandible cerrado clearfix m100 absolute" style="cursor: pointer;"></div>
                           <div class="imagen-lineas-cuadros cuadro DIP">
                           
                           </div>
                        </div>
                        <div class="col col-9 line">DIPLOMADO</div>
                     </div>
                  </div>
                  <!-- DIPLOMADOS Educación Continua y de Lista By SRP Descomentar hasta Marzo 2019-->
                  <!-- POSGRADO -->
                  <div class="max-width-4 mx-auto linea-mobile">
                     <div class="clearfix pb2 relative center line-b">
                        <div class="col col-2 pl2">
                           <div data-pos="po" data-val="POSGRADO" class="expandible cerrado clearfix m100 absolute" style="cursor: pointer;"></div>
                           <div class="imagen-lineas-cuadros cuadro POS">
                              <!-- <img data-src="<?php echo $urlwp; ?>/assets/frontend/img/posgrados_mobile.png" class="circle" alt="Calcula tu Beca Posgrados"> -->
                           </div>
                        </div>
                        <div class="col col-9 line">POSGRADO</div>
                     </div>
                  </div>
                  <!-- POSGRADO -->
               </div>
               <!-- TERMINA seccion MOBILEE -->
               <!-- seccion desktop -->
            </div>
            <div class="slide" data-anchor="sec-promedio" id="sec-promedio" style="display: none;">
               <div class="clearfix center pt1 ">
                  <p class="h2 sub pl1 pr1">Compártenos el promedio que obtuviste en tu <span class="rs-canterior"></span>:</p>
                  <div class="h2">
                     <div class="output">8</div>
                  </div>
               </div>
               <div class="max-width-4 mx-auto px3" id="promedio">
                  <div class="slider">
                     <div style="width: 100%;margin: 3vw 0 1.7vw 0;">
                        <input type="range"  id="range" name="distance" class="form-control " min="6" max="10" step=".1" value="8" list="range_list">
                        <div id="range_list" class="flex justify-between mt1">
                            <div class="flex flex-column center pl1"><span>|</span>6</div>
                            <div class="flex flex-column center"><span>|</span>7</div>
                            <div class="flex flex-column center"><span>|</span>8</div>
                            <div class="flex flex-column center"><span>|</span>9</div>
                            <div class="flex flex-column center"><span>|</span>10</div>
                        </div>
                        <!-- <datalist id="range_list">
                           <option>6</option>
                           <option>7</option>
                           <option>8</option>
                           <option>9</option>
                           <option>10</option>
                        </datalist> -->
                     </div>
                  </div>
               </div>
               <div class="max-width-4 mx-auto px3 center mt3">
                  <button class="button-primaryn " onClick="irA('sec-promedio','resultados')"> Ver Colegiatura</button>
               </div>
            </div>
            <div class="slide " data-anchor="resultados" id="resultados" style="display: none;">
                <div id="cont-rs" class="envolvente flex items-center justify-center">
                    <div class="bg-bco xs-px0 p3 xs-p0 xs-pb4">
                        <div class="max-width-3 mx-auto">
                            <div class="flex flex-wrap">
                                <div class="col-12 center ">
                                    <div class="tit-concluye flex justify-between">
                                        <p class="h2 sub m0">Concluye tu <span class="rs-carrerac"></span> en:</p>
                                        <span class="aniosduracion"></span>
                                        <p class="subt pl1 pr1 prepavesp-term" style="display:none">* Dependiendo de las materias a revalidar</p>
                                    </div>
                                <p class="subtitulo">Modalidad: <span class="rs-modalidad"></span>&nbsp;&nbsp; | &nbsp;&nbsp; Campus: <span class="rs-campus"></span></p>
                                </div>
                            </div>
                        </div>
                        <div class="clearfix max-width-3 mx-auto">
                            <div class="col col-8 ">
                                <p class="h3 sub ticket">Pago <span class="rs-tipopago">Mensual</span>:</p>
                            </div>
                            <div class="col col-4  right-align">
                                <p class="h2 sub ticket rs-pg-final"></p>
                            </div>
                        </div>
                        <div class="clearfix max-width-3 mx-auto r-beca">
                            <div class="col col-8 ">
                                <p class="h3 sub ticket">Beca del:</p>
                            </div>
                            <div class="col col-4  right-align">
                                <p class="h2 sub ticket rs-beca"></p>
                            </div>
                        </div>
                        <div class="clearfix max-width-3 mx-auto">
                            <div class="col col-8 ">
                                <p class="h3 sub ticket reinsctext">Reinscripción/Inscripción:</p>
                            </div>
                            <div class="col col-4  right-align">
                                <p class="h2 sub ticket reinsccosto">Sin Costo*</p>
                            </div>
                        </div>
                        <div class="clearfix max-width-3 mx-auto r-ahorro">
                            <div class="col col-8 ">
                                <p class="h3 sub ticket">Total ahorro:</p>
                            </div>
                            <div class="col col-4  right-align">
                                <p class="h2 sub ticket rs-ahorro-mns"></p>
                            </div>
                        </div>
                        <div class="clearfix max-width-3 mx-auto" id="materiasTira">
                            <div class="col col-8 ">
                                <p class="h3 sub ticket">Materias a cursar:</p>
                            </div>
                            <div class="col col-4  right-align">
                                <p class="h2 sub ticket rs-materias"></p>
                            </div>
                        </div>
                        <!-- Opciones de pago -->
                        <div class="clearfix max-width-3 mx-auto flex items-center">
                            <div class="col col-6  ">
                                <p class="h3 sub ticket">Opciones de pago</p>
                            </div>
                            <div class="col col-6 right-align">
                                <button class="bl col-6 left p0 ml2 btn-dual-y rs-btn-mensual selected">Mensual</button>
                                <button title="Button" class="bl col-6 p0 ml2 btn-dual-y rs-btn-contado">Contado</button>
                            </div>
                        </div>
                        <div class="line-b"></div>
                        <div class="terminos flex flex-column">
                            <div class="clearfix right-align mt1 xs-text-center">
                                <a href="https://clxt.ch/unitecg" target="_blank" class="button button-whatsapp ml2 mr2" data-gtm-tr="evWhats" data-gtm-accion="intención" data-gtm-seccion="calculadora" data-gtm-depto="">
                                    <svg style="width:20px;height:20px;" version="1.1" x="0px" y="0px" viewBox="0 0 512 512" style="enable-background:new 0 0 512 512;" xml:space="preserve" >
                                        <g><path d="M256.064,0h-0.128C114.784,0,0,114.816,0,256c0,56,18.048,107.904,48.736,150.048l-31.904,95.104l98.4-31.456 C155.712,496.512,204,512,256.064,512C397.216,512,512,397.152,512,256S397.216,0,256.064,0z M405.024,361.504 c-6.176,17.44-30.688,31.904-50.24,36.128c-13.376,2.848-30.848,5.12-89.664-19.264C189.888,347.2,141.44,270.752,137.664,265.792 c-3.616-4.96-30.4-40.48-30.4-77.216s18.656-54.624,26.176-62.304c6.176-6.304,16.384-9.184,26.176-9.184 c3.168,0,6.016,0.16,8.576,0.288c7.52,0.32,11.296,0.768,16.256,12.64c6.176,14.88,21.216,51.616,23.008,55.392 c1.824,3.776,3.648,8.896,1.088,13.856c-2.4,5.12-4.512,7.392-8.288,11.744c-3.776,4.352-7.36,7.68-11.136,12.352 c-3.456,4.064-7.36,8.416-3.008,15.936c4.352,7.36,19.392,31.904,41.536,51.616c28.576,25.44,51.744,33.568,60.032,37.024 c6.176,2.56,13.536,1.952,18.048-2.848c5.728-6.176,12.8-16.416,20-26.496c5.12-7.232,11.584-8.128,18.368-5.568 c6.912,2.4,43.488,20.48,51.008,24.224c7.52,3.776,12.48,5.568,14.304,8.736C411.2,329.152,411.2,344.032,405.024,361.504z" fill="#FFFFFF"/></g>
                                    </svg>&nbsp;&nbsp;
                                    Resuelve tus dudas
                                </a>
                                <!-- <button onclick="javascript:window.location.href='/admision-unitec/'" class="button button-whatsapp ml2 mr2">Resuelve tus dudas</button> -->
                            </div>
                            <div class="clearfix max-width-4 mt1 xs-mt1">
                                <div class="sm-col sm-col-12">
                                    <p class="subt pl1 pr1">
                                    *Costos vigentes para el periodo 31 de Diciembre 2019.</p><p class="subt pl1 pr1"><span class="rs-becamasp"> </span><br></p>
                                    <p class="subt pl1 pr1 term-beca">*El porcentaje de beca se asignará conforme al promedio general del nivel de estudios anterior al que se inscriba
                                    </p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
         </div>
      </div>
      <div class="plantillas" style="display:none">
         <div class="p_expandible">
            <div class="caja px3 pt3 lg-px4 xs-p1 m1">
               <div class="clearfix preg-est">
                  <p class="h2 sub xs-left-align" id="subtituloInteres">¿Qué quieres estudiar?</p>
                  <div class="py2 xs-py1">
                     <input class="input sm-col sm-col-5 project" placeholder="Buscar..." id="project">
                     <input type="hidden" class="project-id" id="project-id">
                     <div id="projectSelected" hidden></div>
                  </div>
               </div>
               <div class="clearfix v_mod display-none">
                  <div class="col col-12 pr3 xs-pr0 ">
                     <p class="h2 sub xs-left-align ">¿Qué modalidad te interesa?</p>
                     <div class="py2 xs-py1">
                        <select class="input sm-col sm-col-5" id="render_modalidad"><option>Modalidad</option></select>
                     </div>
                  </div>
               </div>
               <div class="clearfix v_camp display-none">
                  <div class="sm-col sm-col-5 pr3 xs-pr0 ">
                     <p class="h2 sub xs-left-align ">¿En qué campus?</p>
                     <div class="py2 xs-py1">
                        <select class="input sm-col sm-col-5" id="render_campus"><option>Campus</option></select>
                     </div>
                  </div>
               </div>
               <div class="clearfix v_prepa display-none">
                  <div class="sm-col sm-col-5 pr3 xs-pr0 ">
                     <p class="h2 sub xs-left-align ">¿Eres padre o tutor?</p>
                     <div class="py2 xs-py1">
                        <select class="input sm-col sm-col-5" id="render_prepa"></select>
                     </div>
                  </div>
               </div>
               <div class="clearfix  ">
                   <p class="err_cursar"></p>
               </div>
               <div class="clearfix pb2">
                  <div class="col col-6 text-left ">
                     <button class="cerrar iconb">Regresar</button>
                  </div>
                  <div class="col col-6  sm-col-right ">
                     <button  onClick="javascript:irA('sec-cursar','sec-promedio')" class=" right button-primaryn">Avanzar</button>
                  </div>
               </div>
            </div>
         </div>
      </div>
</div>

<script>
    var abreCalculadora;
    
  jQuery(document).ready(function(){
    abreCalculadora = function(){
      //urlwp = "http://localhost:8888/unitec2017/wp-content/themes/temaunitec";
        if(getCookie("c_form_data")){
            var verif_cookie = JSON.parse(decodeURIComponent(getCookie("c_form_data")));
        }else{
            var verif_cookie = {};
        }
        var prueba_externo = $(".home-page").attr('data-externo');
        console.log("Verificando el click");
        if (verif_cookie.regcompleto == true){
            $('<link>').attr('rel', 'stylesheet')
                    .attr('type', 'text/css')
                    .attr('id','no-critical-calc-mobile')
                    .attr('href', urlwp + "/assets/frontend/css/vendor/min/no-critical.min.css")
                    .attr('media', 'none')
                    .attr('onload', "if(media!='all')media='all'")
                    .appendTo('body');
            jQuery('main').fadeToggle();
            jQuery('footer').fadeToggle();
            jQuery('#carousel-banner').fadeToggle();
            jQuery('#calculadora-main-mobile').fadeToggle();
            jQuery('#UnitecSideNav').fadeOut();            
            jQuery('#bootstrapcss-css').prop("disabled",true);
            jQuery('#mdb-css').prop("disabled",true);
            jQuery('#footer').fadeIn();
            scrollTo(0,0);

            if(verif_cookie.linea != undefined) {
                $("div").find("[data-val='"+verif_cookie.linea+"']").click();
                var busqueda_grupo = buscar_grupo("IdDynamics",verif_cookie.carreraInteres,JSON.parse(localStorage.jsonCarreras));
                // console.log("LA BUSQUEDA DEL GRUPO");
                // console.log(busqueda_grupo);
                var categoria_grupo = busqueda_grupo[0].Categoria;
                // console.log("LA BUSQUEDA DEL OPTION");
                // console.log(categoria_grupo);
                // $("#project option:contains('"+categoria_grupo+"')").click();
                /* Agregar un timer para la seleccion de carrera */
                setTimeout(() => {
                    var carrera_selected = $("#project option").filter(function() { 
                        return $(this).text() == categoria_grupo;
                    }).prop('selected', true).change();

                    var modalidad_size = $("#render_modalidad").children().length;
                    if(modalidad_size > 1){
                        var txt_modalidad = $("#render_modalidad option[value="+verif_cookie.modalidad+"]").prop("selected",true).change();
                    }
                    var campus_size = $("#render_campus").children().length;
                    if(campus_size > 1){
                        var txt_campus = $("#render_campus option[value="+verif_cookie.campus+"]").prop("selected",true).change();
                    }
                    $(".right.button-primaryn")[0].click();
                }, 400);
                
                //.prop('selected', true).change();

            }
            
        } else if(prueba_externo == "1"){ 
          //urlwp = "http://localhost:8888/unitec2017/wp-content/themes/temaunitec";

            console.log("ABRO TODO NORMAL");
            $('<link>').attr('rel', 'stylesheet')
                    .attr('type', 'text/css')
                    .attr('id','no-critical-calc-mobile')
                    .attr('href', urlwp + "/assets/frontend/css/vendor/min/no-critical.min.css")
                    .attr('media', 'none')
                    .attr('onload', "if(media!='all')media='all'")
                    .appendTo('body');
            jQuery('main').fadeToggle();
            jQuery('footer').fadeToggle();
            jQuery('#carousel-banner').fadeToggle();
            jQuery('#calculadora-main-mobile').fadeToggle();
            jQuery('#UnitecSideNav').fadeOut();
            jQuery('#bootstrapcss-css').prop("disabled",true);
            jQuery('#mdb-css').prop("disabled",true);
            jQuery('#footer').fadeIn();
            scrollTo(0,0);
        } else{
            jQuery('main').fadeToggle();
            jQuery('.calcuApp').fadeToggle();
            jQuery('footer').fadeToggle();
            jQuery('#carousel-banner').fadeToggle();
        }
    }

    if(getCookie("c_form_data")){
            var verif_cookie = JSON.parse(decodeURIComponent(getCookie("c_form_data")));
        }else{
            var verif_cookie = {};
        }
        var prueba_externo = $(".home-page").attr('data-externo');
        console.log("Verificando el click");
        if (verif_cookie.regcompleto == true){
            setTimeout(() => {
                jQuery('.muestracalcu').first().click();
            }, 400);
            
        }
        
    
    jQuery('.muestracalcu').on('click',function(){
        console.log("Entrando en la segunda");
        if(getCookie("c_form_data")){
            var verif_cookie = JSON.parse(decodeURIComponent(getCookie("c_form_data")));
        }else{
            var verif_cookie = {};
        }
        var prueba_externo = $(".home-page").attr('data-externo');
        console.log("Verificando el click");
        if (verif_cookie.regcompleto == true){
          //urlwp = "http://localhost:8888/unitec2017/wp-content/themes/temaunitec";
            $('<link>').attr('rel', 'stylesheet')
                    .attr('type', 'text/css')
                    .attr('id','no-critical-calc-mobile')
                    .attr('href', urlwp + "/assets/frontend/css/vendor/min/no-critical.min.css")
                    .attr('media', 'none')
                    .attr('onload', "if(media!='all')media='all'")
                    .appendTo('body');
            jQuery('main').fadeToggle();
            jQuery('footer').fadeToggle();
            jQuery('#carousel-banner').fadeToggle();
            jQuery('#calculadora-main-mobile').fadeToggle();
            jQuery('#UnitecSideNav').fadeOut();
            jQuery('#bootstrapcss-css').prop("disabled",true);
            jQuery('#mdb-css').prop("disabled",true);
            jQuery('#footer').fadeIn();
            scrollTo(0,0);

            if(verif_cookie.linea != undefined) {
                $("div").find("[data-val='"+verif_cookie.linea+"']").click();
                var busqueda_grupo = buscar_grupo("IdDynamics",verif_cookie.carreraInteres,JSON.parse(localStorage.jsonCarreras));
                // console.log("LA BUSQUEDA DEL GRUPO");
                // console.log(busqueda_grupo);
                var categoria_grupo = busqueda_grupo[0].Categoria;
                // console.log("LA BUSQUEDA DEL OPTION");
                // console.log(categoria_grupo);
                // $("#project option:contains('"+categoria_grupo+"')").click();
                /* Agregar un timer para la seleccion de carrera */
                setTimeout(() => {
                    var carrera_selected = $("#project option").filter(function() { 
                        return $(this).text() == categoria_grupo;
                    }).prop('selected', true).change();

                    var modalidad_size = $("#render_modalidad").children().length;
                    if(modalidad_size > 1){
                        var txt_modalidad = $("#render_modalidad option[value="+verif_cookie.modalidad+"]").prop("selected",true).change();
                    }
                    var campus_size = $("#render_campus").children().length;
                    if(campus_size > 1){
                        var txt_campus = $("#render_campus option[value="+verif_cookie.campus+"]").prop("selected",true).change();
                    }
                    setTimeout(() => {
                        $(".right.button-primaryn")[0].click();
                    }, 400);
                    
                }, 1000);
                
                //.prop('selected', true).change();

            }
            
        } else if(prueba_externo == "1"){ 
          //urlwp = "http://localhost:8888/unitec2017/wp-content/themes/temaunitec";
            console.log("ABRO TODO NORMAL");
            $('<link>').attr('rel', 'stylesheet')
                    .attr('type', 'text/css')
                    .attr('id','no-critical-calc-mobile')
                    .attr('href', urlwp + "/assets/frontend/css/vendor/min/no-critical.min.css")
                    .attr('media', 'none')
                    .attr('onload', "if(media!='all')media='all'")
                    .appendTo('body');
            jQuery('main').fadeToggle();
            jQuery('footer').fadeToggle();
            jQuery('#carousel-banner').fadeToggle();
            jQuery('#calculadora-main-mobile').fadeToggle();
            jQuery('#UnitecSideNav').fadeOut();
            jQuery('#bootstrapcss-css').prop("disabled",true);
            jQuery('#mdb-css').prop("disabled",true);
            jQuery('#footer').fadeIn();
            scrollTo(0,0);
        } else{
            jQuery('main').fadeToggle();
            jQuery('.calcuApp').fadeToggle();
            jQuery('footer').fadeToggle();
            jQuery('#carousel-banner').fadeToggle();
        }
    });
    // console.log("Verificando la cookie del formulario");
  });
document.getElementById("banner").value = getParameterByName("utm_campaign");
document.getElementById("url_referrer").value = window.location.href ;
// console.log(getParameterByName("utm_campaign"));

</script>
<!-- <script type='text/javascript' src='<?php echo $urlwp; ?>/assets/frontend/js/angularcalcu/inline.bundle.js'></script>
<script type='text/javascript' src='<?php echo $urlwp; ?>/assets/frontend/js/angularcalcu/polyfills.bundle.js'></script>
<script type='text/javascript' src='<?php echo $urlwp; ?>/assets/frontend/js/angularcalcu/main.bundle.js'></script>
<script type='text/javascript' src='<?php echo $urlwp; ?>/assets/frontend/js/angularcalcu/scripts.bundle.js'></script> -->

<!-- <link rel="stylesheet" type="text/css" href="<?php echo $urlwp; ?>/assets/frontend/css/vendor/min/normalize.min.css" media="none" onload="if(media!='all')media='all'" /> -->
<link rel="stylesheet" type="text/css" href="<?php echo $urlwp;?>/assets/frontend/css/vendor/min/basscss.min.css" media="none" onload="if(media!='all')media='all'" />
<!-- <link rel="stylesheet" type="text/css" href="<?php echo $urlwp; ?>/assets/frontend/css/vendor/min/no-critical.min.css" media="none" onload="if(media!='all')media='all'" /> -->
<script  src="<?php echo $urlwp;?>/assets/frontend/js/calculadora_integracion_hb/min/funciones-calculadora.min.js"></script>
<!-- <script async src="//unitecwww.mx/wp-content/themes/temaunitec/assets/frontend_desktop/js/calculadora_integracion/jquery-ui.js"></script> -->
<script async src="<?php echo $urlwp;?>/assets/frontend/js/calculadora_integracion_hb/min/funciones-async.min.js"></script>
<script async src="<?php echo $urlwp;?>/assets/frontend/js/calculadora_integracion_hb/min/funciones-on.min.js"></script>

<script type='text/javascript' src='<?php echo $urlwp; ?>/assets/frontend/js/angularcalcu2-v6/runtime.js'></script>
<script async type='text/javascript' src='<?php $urlwp; ?>/assets/frontend/js/angularcalcu2-v6/polyfills.js'></script>
<script type='text/javascript' src='<?php echo $urlwp; ?>/assets/frontend/js/angularcalcu2-v6/main.js'></script>
<script async type='text/javascript' src='<?php echo $urlwp; ?>/assets/frontend/js/angularcalcu2-v6/vendor.js'></script>
<script async type='text/javascript' src='<?php echo $urlwp; ?>/assets/frontend/js/angularcalcu2-v6/scripts.js'></script>
