<div id="carreras_relacionadas"></div>
<?php
/**
 * WP Post Template: mob_13_carreras_relacionadas
 */
?>

<main class="col-12 mt-0 wow fadeInRight"> 
    <!-- SECTION-->
    <section id="elegant-card">
        <!-- Live preview-->
        <div class="row">
            <div class="col-md-12">
<!-- ***************************-> Inicia tu código ... -->
                 <?php 
                $i=0;
                    //if(have_rows("carreras",$theID) ) {                        
                ?>
                <div class="card">
                    <h4 class="card-title"><?php echo get_field('titulo', $theID); ?></h4>
                </div>
                    <?php 
                        if(have_rows("carreras", $theID) ) {
                            while( have_rows('carreras') ) { the_row();
                                // $imagen = get_sub_field( 'imagen' );
                                $carreras = get_field( 'carreras' );
                    ?>
                        <!--Rotating card-->
                        <div class="mt-0 mb-1 card ovf-hidden altura-card"><!--class="card-wrapper" ficha-width="178" -->
                            <div id="card-carreras-de-interes-<?php echo $i;?>" class="card-rotating"><!-- class="card-rotating effect__click" -->
                                <!--Front Side-->
                                <?php 
                                    $thumbID = get_post_thumbnail_id( $carreras[$i]->ID );
                                    $urlImg = wp_get_attachment_url( $thumbID );
                                    $altImg = get_post_meta($thumbID, '_wp_attachment_image_alt', true);
                                ?>
                                <div class="face front card-body">
                                    <!-- <div class="fichaSpecial bsn mb-1"> -->
                                        <div class="fichaImg">
                                            <a href="<?php echo get_permalink($carreras[$i]->ID); ?>" data-gtm-tr="SeeMore" data-gtm-carrera="<?php echo $carreras[$i]->post_title; ?>" data-gtm-interaccion="Imagen">
                                              <img alt="<?php echo $altImg; ?>" class="lazy" data-src="<?php echo $urlImg; ?>">
                                            </a>
                                            <!-- <amp-img 
                                                layout="flex-item"
                                                height="178" 
                                                src="<?php echo $urlImg; ?>" 
                                                alt="<?php echo $altImg; ?>" />
                                                <noscript><img src="<?php echo $imagen['url']; ?>" alt="<?php echo $imagen['alt']; ?>"></noscript>
                                            </amp-img> -->
                                        </div>
                                        <div class="fichaDesign">
                                            <div>
                                                <div class="title"><?php echo $carreras[$i]->post_title; ?></div>
                                                <?php $contenido_producto = get_field('descripcion_del_producto', $carreras[$i]->ID); ?>
                                                <div class="content"><?php echo wp_trim_words( $contenido_producto, 12 ); ?></div>
                                            </div>
                                            <div class="navegacion">

                                                <span class="boxiconActions">
                                                    <!-- <i class="material-icons">turned_in</i> -->
                                                    <a class="rotate-btn" data-card="card-carreras-de-interes-<?php echo $i;?>"><?php echo do_shortcode( '[icono nombre="'.sanitize_title("compartir").'" clase="activator compartir-fix"][/icono]' ); ?></a>
                                                    <!-- <a class="activator"><i class="fa fa-share-alt"></i></a> -->
                                                </span>
                    
                                                <a href="<?php echo get_permalink($carreras[$i]->ID); ?>" class="linkSecundario pull-right link-fix" data-gtm-tr="SeeMore" data-gtm-carrera="<?php echo $carreras[$i]->post_title; ?>" data-gtm-interaccion="Ver más"><?php echo get_field('boton', $theID); ?></a>
                                            </div>
                                        </div>
                                    <!-- </div> -->
                                </div>
                        <!--/.Front Side-->
                        <!--Back Side-->
                        <div class="face back card-reveal">

                           <?php echo do_shortcode('[share cerrar="card-carreras-de-interes-'.$i.'" pagina="'.get_permalink($carreras[$i]->ID).'" seccion="Carreras Relacionadas: '.$carreras[$i]->post_title.'"][/share]');?>
                        </div>
                        <!--/.Back Side-->
                    </div>
                </div>
                <!--/.Rotating card-->
            <?php                            
                                $i++; 
                            } 
                        }
                    //}   
                ?>
<!-- ***************************-> Termina tu código ... -->
            </div>
        </div><!-- /.Live preview-->
    </section><!-- SECTION-->
</main><!-- /.Main layout-->