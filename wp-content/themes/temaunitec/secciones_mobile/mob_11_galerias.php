<?php
/**
 * WP Post Template: mob_11_galerias
 */ 
?>
<?php
/*Se insertan los plugins que necesita la seccion tal y como se llama el archivo*/
load_script( $pluginsNecesarios = array('masonry') );
/*End Se insertan los plugins que necesita la seccion tal y como se llama el archivo*/

$galerias = get_field('categoria_de_galeria');
$json_galery = NULL;
$portadas_galeria = NULL; 

$json_galery = '{';
foreach ($galerias as $key => $galeria) {
    $galeria_seleccionada = get_field('fotografias', $galeria->ID);
    $json_galery .= '"'.$galeria->post_name.'":[';

    foreach ($galeria_seleccionada as $key => $imagenes) {
        $json_galery .= '{"title":"'.$galeria->post_name.'", "url":"'.$imagenes['url'].'", "alt" : "'.$imagenes['alt'].'", "caption": "'.str_replace(array("\r\n", "\r", "\n"), "", $imagenes['caption'] ).'", "description" : "'.str_replace(array("\r\n", "\r", "\n"), "", $imagenes['description'] ).'", "urlthumbnail": "'.$imagenes['sizes']['thumbnail'].'"},';
        if($key == 0) {
            $portadas_galeria []= $imagenes['sizes']['thumbnail'];
        }
        
    }
    $json_galery = substr($json_galery, 0, strlen($json_galery) - 1);
    $json_galery .= '],';
}
$json_galery = substr($json_galery, 0, strlen($json_galery) - 1);
$json_galery .= '}'; 

?>

<?php if( !empty( $galeria_seleccionada ) || $galeria_seleccionada != NULL || count( $galeria_seleccionada ) > 0 ) { ?>

<script>
    var galery_json = '<?php echo $json_galery; ?>';
</script>
<div id="galeria-campus"></div>
<main class="col-12 mt-1 "><!-- /.Main <layout-->
    <input type="hidden" id="galeria-activa" name="galeria-activa" value="<?php echo $galerias[0]->post_name; ?>">
    <input type="hidden" id="galeria-name" name="galeria-name" value="<?php echo $galerias[0]->post_title; ?>">
    <section id="elegant-card"><!-- SECTION-->
        <div class="row"><!-- Live preview-->
            <div class="col-md-12"><!-- col-md-12 -->
<!-- ***************************-> Inicia tu código ... -->
                <div class="card pb-0 mb-1">
                    <h4 class="card-title"><?php echo get_field('titulo_card_galeria'); ?></h4>
                    <!-- Tab Navegacion -->
                    <div class="mt-1">
                        <?php // echo do_shortcode( '[icono nombre="'.sanitize_title("tab-izquierda").'" clase="leftTabNav direccionales" estilo="style=\'display:none\'"][/icono]' ); ?>
                        <section id="tabNav">

                            <div class="tabs-wrapper content-portadas-galeria"> 
                                <ul class="nav classic-tabs free testimonial" role="tablist" style="justify-content: space-between;">
                                    <?php 
                                        foreach ($galerias as $key => $galeria) { ?>
                                    <a class="col-4 nav-item portada-galeria flex-column" id="<?php echo $galeria->post_name; ?>" name="<?php echo $galeria->post_name; ?>" title="<?php echo $galeria->post_title; ?>" data-gtm-tr="OurFac" data-gtm-facilidad="<?php echo $galeria->post_title; ?>" data-gtm-interaccion="Fotogaler&iacute;a">
                                        <img data-src="<?php echo $portadas_galeria[$key]; ?>" alt="portada galeria <?php echo $galeria->post_title; ?>" class="img-fluid centrar flex-custom lazy">
                                        <!-- <?php echo do_shortcode( '[icono nombre="'.sanitize_title("hospital-simulado").'"][/icono]' ); ?> -->
                                        <div class="title-galerias galeria-title-<?php echo $galeria->post_name; ?>"><?php echo $galeria->post_title; ?></div>
                                    </a>
                                                
                                    <?php }
                                    ?>                                
                                </ul>
                            </div>
                    
                        </section>
                        <?php // echo do_shortcode( '[icono nombre="'.sanitize_title("tab-derecha").'" clase="rightTabNav direccionales" estilo="style=\'display:none\'"][/icono]' ); ?>
                    </div>
                    
                    <!-- Grid, masonry con previews de la galeria -->
                    <div class="grid grid-galeria-master mt-1 mb-0" id="load_galery"></div>
                </div><!-- card pb-0 mb-1 -->              
                    
                <!-- Modal -->    
                <div class="modal fade modal-tarjeta" id="modal-tarjeta-galeria-carrusel" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" data-backdrop="static" data-keyboard="false">
                    <div class="modal-dialog modal-fluid" role="document">
                        <div class="modal-content">
                            <div class="modal-header">
                                <!-- Portada de la Galeria -->
                                <div class="imgMin">
                                    <img id="portada-galeria" src="">
                                </div>
                                <!-- End Portada de la Galeria -->
                                <div class="description">
                                    <label>Galería</label><br>
                                    <label id="gallery-title"></label><br>
                                    <!-- <label>texto 2</label> -->
                                </div>
                                <div class="icons">
                                    <!-- <?php echo do_shortcode( '[icono nombre="'.sanitize_title("ver-opciones").'"][/icono]' ); ?> -->
                                    <!-- <div class="dropdown-menu dropdown-menu-right puntos-tarjeta" aria-labelledby="puntos">
                                        <a class="dropdown-item" href="#"><?php echo do_shortcode( '[icono nombre="'.sanitize_title("compartir").'"][/icono]' ); ?> Compartir</a>
                                        <a class="dropdown-item" href="#"><?php echo do_shortcode( '[icono nombre="'.sanitize_title("descargar").'"][/icono]' ); ?> Descargar</a>
                                    </div> -->
                                    <?php echo do_shortcode( '[icono nombre="'.sanitize_title("cerrar").'"][/icono]' ); ?>
                                </div>
                            </div>
                            
                            <div class="modal-content-tarjeta">
                                <!-- carousel-thumbnails -->
                                <!--Carousel Wrapper-->
                                <div id="carousel-tarjeta" class="carousel slide carousel-fade " data-ride="carousel-tarjeta">

                                    <!--Slides-->
                                    <div class="carousel-inner bsn" role="listbox" id="slides-carusel"></div>
                                    <!--/.Slides-->

                                    <!--Indicators-->
                                    <ol id="indicadores-carusel" class="custom-indicadores carousel-indicators mt-1"></ol>
                                    <!--/.Indicators-->

                                </div>
                                <!--/.Carousel Wrapper-->
                            </div>

                            <div class="modal-footer text-center">
                                <p></p>
                            </div>
                        </div>
                    </div>
                </div>                
<!-- ***************************-> Termina tu código ... -->
            </div><!-- col-md-12 -->
        </div><!-- /.Live preview-->
    </section><!-- SECTION-->
</main><!-- /.Main <layout-->
<?php } ?>