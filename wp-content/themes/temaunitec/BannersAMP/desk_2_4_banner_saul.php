<!DOCTYPE HTML> 
<!-- <html amp> -->
<html lang="en">
    <head>
        <meta charset="utf-8"> 
        <title>Widgets Magazine</title> 
        <link rel="canonical" href="$REGULAR_HTML_URL" /> 
        <meta name="viewport" content="width=device-width,minimum-scale=1,initial-scale=1"> 
    </head>

    <script type="text/javascript">

        $(document).ready(function(){
            var availableTags = [
              "ActionScript",
              "AppleScript",
              "Asp",
              "BASIC",
              "C",
              "C++",
              "Clojure",
              "COBOL",
              "ColdFusion",
              "Erlang",
              "Fortran",
              "Groovy",
              "Haskell",
              "Java",
              "JavaScript",
              "Lisp",
              "Perl",
              "PHP",
              "Python",
              "Ruby",
              "Scala",
              "Scheme"
            ];

            $( "#tags" ).autocomplete({
              source: availableTags
            });

            //alert("URL: " + URL + " ID: " + idspreadsheet);
            alert("Hola");

        });

    </script>

    <div class="ui-widget">
        <label for="tags">Tags: </label>
        <input id="tags">
    </div>

</html>