//PARTE 1 SIMULADOR DE COSTOS
jQuery(document).ready(function(e) 
{
    jQuery( '.triggermenu' ).click(function(){     
      menuExpanded = jQuery( '#menu' ).multilevelpushmenu( 'menuexpanded' , jQuery( '#menu' ).multilevelpushmenu( 'findmenusbytitle' , 'Menú' ).first() );
      if( menuExpanded ){
        jQuery( '#menu' ).multilevelpushmenu( 'collapse' );
      }else {
        jQuery( '#menu' ).multilevelpushmenu( 'expand' );
      }
    });
    
    jQuery("#calcApp").data('regcompleto',0)
    var nuevoCampus="";  //variable en caso de cambio de campus
    var nuevaLinea="";  //variable en caso de cambio de campus
    initialization();
    escala();
  
    jQuery(window).bind('beforeunload', function(){
        debugleave(jQuery('#frm_correo').val()+" | Abandona en: "+jQuery('.hz-slide.active').data('anchor')+"|");
    });

    if(getParameterByName("utm_campus")!=null){
        jQuery('#calcApp').data("campus",getParameterByName("utm_campus"));
    }
    getLineasNegocio();
    jQuery.fn.fullpage.silentMoveTo(0, "sec-cursar");
    //****CIERRE DE MENU****//
    jQuery('body').on('click','.btn-close',function(e){
        if(e.target.className.match(/btn-close/)){
            mobileCollapse(jQuery(this).data('btn'));
        }
    });
    //****CLICK EN CLCLINEA (PREPA,LIC,ETC)****//
    jQuery('body').on('click','.clcLinea',function(e){
        //JEAB DISPARA EVENTO Y VALIDA EL CAMPUS
        //Validar que se hayan seleccionado datos necesarios
        //FUNCIONALIDAD PARA EL NUEVO SELECT DE LAS NUEVAS CARRERAS
        jQuery('#calcApp').removeData("crtcampus");
        if(!jQuery(this).hasClass("item-available")){
            jQuery('#error-modal2').modal('show');
            jQuery('.mod-campus').html(jQuery('#calcApp').data("decampus"));
            jQuery('.mod-linea').html(jQuery(this).data("linea"));
            nuevaLinea=jQuery(this).attr("id");
        }
        var campusSelected="";
        if(jQuery(window).width()>768){
            if (e.target !== this){ return;} /*solo afecta si el div es clickeado, no su contenido*/
        }
        jQuery('#secLineas').find(".item-selected").each(function(){jQuery(this).removeClass("item-selected");jQuery("#"+this.id+">h2").remove();
        });  //reinicia los selected itwems
        if(jQuery(this).hasClass("item-available")){
            if(jQuery(this).hasClass("linea-dsk")){
                //debuglog("CLICK animando linea de Negocio: "+this.id);
                animar(jQuery(this));
                jQuery('#'+this.id+'>.linea-elementos').append('<div class="col-md-8">\
                                                          <div class="row que-estudiar dark">\
                                                              ¿Qué quieres estudiar?\
                                                              <br>\
                                                              '+ campusSelected+'\
                                                              <div class="col-md-8"><select class="sel-carrera" onChange="setCarrera();return;">\
                                                              </select></div>\
                                                          </div>\
                                                          <div class="row que-modalidad dark">\
                                                              ¿En qué modalidad?\
                                                              <br>\
                                                          </div>\
                                                          <div class="modstatus"></div>\
                                                          <div class="carreramod dark">\
                                                          </div>\
                                                          <div class="col-md-4 btn-back-sec-cursar"><span class="backMenu back-menu btn btn-primary cerrarExpandible nuevo-boton-regresar" id="new-button-back"><i class="fa fa-chevron-left"></i>&nbsp;Regresar</span></div>\
                                                          <div class="col-md-4 btn-back-sec-cursar"><span class="btn btn-primary nuevo-boton-ir-campus" id="new-button-back" onclick="irCampus();">Seleccionar campus&nbsp;<i class="fa fa-chevron-right"></i></span></div>\
                                                      </div>');

                //AGREGAR FUNCIONALIDAD PARA EL BOTON REGRESAR SI EXISTE COOKIE
                if( checkCookie('frmunitec') == true ) {
                    jQuery('#new-button-back').attr('onclick','regresar();');
                } 
                jQuery('#calcApp').data("selectLinea",jQuery(this).data("linea"));
                getCarreras();
                if(jQuery(this).find('.sel-carrera').size()==1){
                    jQuery(this).find('.sel-carrera').val(56).change();  
                }
            }else{ //hasClass("linea-dsk")

                jQuery(this).addClass('item-selected');
                jQuery(this).append("<h2 class='no-disponible hidden-xs hidden-sm'>Campus Seleccionado</h2>");
                jQuery('#calcApp').data("selectLinea",jQuery(this).data("linea"));    
                var ccontCarrera=getCarreras();
                if(ccontCarrera==1){
                    jQuery('.clcCategoria').click();
                    e.preventDefault();
                    jQuery.fn.fullpage.silentMoveTo(0,'sec-modalidades');
                }else{
                    e.preventDefault();
                    jQuery.fn.fullpage.moveSlideRight();
                }
            }
        }//hasClass("item-available")

    //.on('click','.clcLinea'
    }).children().find(".linea-elementos").on('click', function (event) {event.stopPropagation();});

    //JEAB NUEVA VISTA PARA CAMPUS
    jQuery('body').on('click','.clcCampus',function(e){
    //JEAB DISPARA EVENTO Y VALIDA EL CAMPUS
    //FUNCIONALIDAD PARA EL NUEVO SELECT DE LAS NUEVAS CARRERAS
    //console.log(jQuery('#select-campus option:selected').attr('class'));
    jQuery('#calcApp').removeData("crtcampus");
    if(jQuery('#select-campus option:selected').attr("class") != 'item-available'){
        jQuery('#error-modal2').modal('show');
        jQuery('.mod-campus').html(jQuery('#calcApp').data("decampus"));
        jQuery('.mod-linea').html(jQuery(this).data("linea"));
        nuevaLinea=jQuery(this).attr("id");
    }
        var campusSelected="";
        if(jQuery(window).width()>768){
            if (e.target !== this){ return;} /*solo afecta si el div es clickeado, no su contenido*/
        }
        jQuery('#secLineas').find(".item-selected").each(function(){jQuery(this).removeClass("item-selected");jQuery("#"+this.id+">h2").remove();});  //reinicia los selected itwems
        if(jQuery(this).hasClass("item-available")){
            if(jQuery(this).hasClass("linea-dsk")){ 
                //JEAB REVISAR 29-04-2016
                //JEAB FUNCION DESPUES DE SELECCIONAR LINEA DE NEGOCIO
                animar(jQuery(this));
                if(jQuery('#calcApp').data('decampus')){
                    campusSelected="Campus seleccionado: <span class='camp-sel'>"+jQuery('#calcApp').data('decampus')+"</span>"; 
                }
                //JEAB AGREGAR FUNCIONALIDAD PARA EL BOTON REGRESAR SI EXISTE COOKIE
                if( checkCookie('frmunitec') == true ) {
                    jQuery('#new-button-back').attr('onclick','regresar();');
                } 
                jQuery('#calcApp').data("selectLinea",jQuery(this).data("linea"));
                getCarreras();
                if(jQuery(this).find('.sel-carrera').size()==1){
                    jQuery(this).find('.sel-carrera').val(56).change();  
                }
            //hasClass("linea-dsk")  
            }else{
                jQuery(this).addClass('item-selected');
                jQuery(this).append("<h2 class='no-disponible hidden-xs hidden-sm'>Campus Seleccionado</h2>");
                jQuery('#calcApp').data("selectLinea",jQuery(this).data("linea"));    
                var ccontCarrera=getCarreras();
                if(ccontCarrera==1){
                    jQuery('.clcCategoria').click();
                    e.preventDefault();
                    jQuery.fn.fullpage.silentMoveTo(0,'sec-modalidades');
                }else{
                    e.preventDefault();
                    jQuery.fn.fullpage.moveSlideRight();
                }
            }
        }//hasClass("item-available")


});//jQuery(document).ready(function(e)

//****CAMBIO DE ESQUEMA A DIFERENTES AÑOS ****//
jQuery('body').on("click",".btn-esquema",function(){
    
    var mostrar= jQuery(this).data("esquema");
    getDuracion(mostrar);
    jQuery('.toggle').toggles(false);
    jQuery('div[class*="termino-"]').each(function(){ 

        if(jQuery(this).attr('id') != 'terminos-condiciones') {
          jQuery(this).hide();
        }
    
    }); //limpialos
    
    jQuery('.termino-'+mostrar+':not(".p-contado")').fadeIn();
    jQuery('#calcApp').data('termino',mostrar);
    jQuery('#calcApp').data('materias',jQuery('.mat-calc-'+mostrar).html());
    jQuery('.toggle-off').html('<span style="margin-left:-10px;">No</span>');
    jQuery('.toggle-on').html('<span>Si</span>');
    //CAMBIAR PERIODOS EN QUE TERMINA LA CARRERA EN MOBILE JEAB 09-05-2016
    if( jQuery(window).width() < 768 ){
          jQuery('.anios-carrera').addClass('anios-carrera-mobile');
    }else {
          jQuery('.anios-carrera').removeClass('anios-carrera-mobile');
    }
    jQuery('#terminos-condiciones').show();
    jQuery('#ex14').slider("setValue",jQuery('.mat-calc-'+mostrar).html());
    jQuery('#terminos-condiciones').show();

}); //jQuery('body').on("click",".btn-esquema"

//****CLICK EN LOS BOTONES DE MENOS PROMEDIO ****//
jQuery('body').on("click",".control-menos",function(){
  
  jQuery('.control-mas').fadeIn();
  var sliderp=jQuery(this).data("slider");
  var valor= jQuery('#'+sliderp).slider("getValue");
  var nuevo = valor-0.1;
  if(nuevo<=6 ){
    jQuery('.control-menos').fadeOut();
  }
  if(nuevo>=6 ){
      jQuery('.promedio-val').html(nuevo.toFixed(1));
      jQuery('#'+sliderp).slider("setValue",nuevo);
  }else{
      jQuery('.control-menos').fadeOut();
  }

}); //jQuery('body').on("click",".control-menos"

//****CLICK EN LOS BOTONES DE MAS PROMEDIO****//
jQuery('body').on("click",".control-mas",function(){

    jQuery('.control-menos').fadeIn();
    var sliderp=jQuery(this).data("slider");
    var valor= jQuery('#'+sliderp).slider("getValue");
    var nuevo = valor+0.1;
    if(nuevo==10){
      jQuery('.control-mas').fadeOut();
    }
    if(nuevo<=10){
      jQuery('.promedio-val').html( nuevo.toFixed(1) );
      jQuery('#'+sliderp).slider("setValue",nuevo);
    }

}); //jQuery('body').on("click",".control-mas"

//****CLICK EN LOS BOTONES DE MENOS MATERIAS ****//
jQuery('body').on("click",".control-menos-mat",function(){

    jQuery('.control-mas-mat').fadeIn();
    var sliderp=jQuery(this).data("slider");
    var valor= jQuery('#'+sliderp).slider("getValue");
    var nuevo = valor-1;
    //console.log(jQuery('#calcApp').data('idCosto') +nuevo);
    if(jQuery('#calcApp').data('idCosto')=="DENT" && nuevo<4){
      //console.log("sale");
      return false
    }
    if(nuevo<=1 ){
      jQuery('.control-menos-mat').fadeOut();
    }
    if(nuevo>0 ){
      jQuery('.materias-val').html(nuevo);
      jQuery('#'+sliderp).slider("setValue",nuevo);
    }else{
      jQuery('.control-menos-mat').fadeOut();
    }

}); //jQuery('body').on("click",".control-menos-mat"

//****CLICK EN LOS BOTONES DE MAS MATERIAS ****//
jQuery('body').on("click",".control-mas-mat",function(){
    jQuery('.control-menos-mat').fadeIn();
    jQuery(".btn-esquema").removeClass('btn-primary');
    var sliderp=jQuery(this).data("slider");
    var nuevo= parseInt(jQuery('#'+sliderp).slider("getValue"))+1;
    if(nuevo>=jQuery('#calcApp').data('maxmaterias')){
      jQuery('.control-mas-mat').fadeOut();
    }
    if(nuevo<=jQuery('#calcApp').data('maxmaterias')){
      jQuery('.materias-val').html(nuevo);
      jQuery('.btn-esquema').each(function(){
        if(jQuery('#calcApp').data("mat_"+jQuery(this).data('esquema')+"_"+nuevo)){}
      });
      jQuery('#'+sliderp).slider("setValue",nuevo);
    }
    getDuracion();
});

//****CLICK EN LOS BOTONES DE ACEPTAR PROMEDIO Y MATERIAS  ****//
jQuery('body').on("click","#aceptar", function (e) {
    
    jQuery('#calcApp').data("promedio",jQuery("#ex13").slider('getValue'));
    jQuery('#calcApp').data("materias",jQuery("#ex14").slider('getValue'));
    getBecas();
    getDuracion();
    jQuery.fn.fullpage.moveTo(0,'resultados');
});


//*******FUNCIONALIDAD DE SLIDERS********//
jQuery("#ex13").slider({ticks: [6,7, 8, 9, 10],ticks_labels: ['6','7', '8', '9', '10'],step:0.1});
jQuery("#ex14").slider({ticks: [1,2, 3, 4,5],ticks_labels: ['1','2', '3', '4', '5'],step:1});
jQuery("body").on("change","#ex13", function (e) {  jQuery('.promctrl').fadeIn(); jQuery('.promedio-val').html(jQuery("#ex13").slider('getValue')); });
jQuery("body").on("change","#ex14", function (e) {  jQuery('.matctrl').fadeIn();  jQuery('.materias-val').html(jQuery("#ex14").slider('getValue')); });
jQuery("body").on("slideStop","#ex14", function (e) {
    if(parseInt(jQuery("#ex14").slider('getValue'))<5){jQuery('#calcApp').data('beca',0);}
    jQuery('#calcApp').data("materias",jQuery("#ex14").slider('getValue'));
    getBecas()
});
jQuery('body').on("slideStop","#ex13", function (e) {
    jQuery('#calcApp').data("promedio",jQuery("#ex13").slider('getValue'));
    getBecas();
    getDuracion();
});

//*******CLICK EN clcCategoria********//
jQuery('body').on("click",".clcCategoria",function(e) {
  jQuery('#calcApp').data("selectCategoria",jQuery(this).data("categoria"));
  jQuery('#calcApp').data("url",jQuery(this).find('span').html());
  var ccontMod= getModalidad("",jQuery(this).data("categoria"));
  if(ccontMod==1){ 
      e.preventDefault();
      jQuery.fn.fullpage.silentMoveTo(0, "sec-modalidades")
  }else{
      e.preventDefault();
      jQuery.fn.fullpage.moveTo(0, "sec-modalidades")
  }
});

//**************CLICK EN CLCCARRERAMOD MOBIL*******************//
jQuery(document).on("click",".clcCarrerasMod",function(e) {
    jQuery('#calcApp').data("selectMateria",jQuery(this).data("carreramod"));
    jQuery('#calcApp').data("idCosto",jQuery(this).data("idcosto"));
    var ccontCamp=getCampus(jQuery(this).data("carreramod").toString());
    getAnioslimite();//obtiene las materias minimas
    if(ccontCamp==1){
      e.preventDefault();
      jQuery.fn.fullpage.silentMoveTo(0, "sec-campus")
    }else{
      e.preventDefault();
      jQuery.fn.fullpage.moveTo(0, "sec-campus")
    }
     jQuery('#sec-modalidades').find(".item-selected").each(function(){jQuery(this).removeClass("item-selected");jQuery("#"+this.id+">h2").remove();});
    if(jQuery(this).hasClass("item-available")){jQuery(this).addClass('item-selected');        jQuery(this).append("<h2 class='no-disponible hidden-xs hidden-sm'>Campus Seleccionado</h2>");}
});

//**************CLICK EN CLCCARRERAMOD DESK*******************//
jQuery(document).on("change",".clcCarrerasMod-dsk ",function(e){
    jQuery('#calcApp').data("selectMateria",jQuery(this).data("carreramod"));
    jQuery('#calcApp').data("idCosto",jQuery(this).data("idcosto"));
    //debuglog('Escogio que quiere estudiar');
    //debuglog('Materia'+jQuery(this).data("carreramod"));
    //debuglog('idCosto'+jQuery(this).data("idcosto"));
    getCampus(jQuery(this).data("carreramod").toString());
    getAnioslimite();//obtiene las materias minimas
    e.preventDefault();
    jQuery.fn.fullpage.moveTo(0, "sec-campus");
});
   
/**********FUNCION FULL PAGE************/
function initialization(){

    jQuery('#fullpage').fullpage({
      anchors: ["calculadora","frm_mobile","sec-cursar","sec-categoria","sec-modalidades","sec-campus","sec-promedio","sec-materias","resultados"],
      menu: true,
      //Scrolling
      css3: false,
      scrollingSpeed: 700,
      autoScrolling: true, // navegador CON true  mobile con false 
      fitToSection: false,
      scrollBar: false,
      easing: 'easeInOutCubic',
      easingcss3: 'ease',
      loopBottom: false,
      loopTop: false,
      loopHorizontal: true,
      continuousVertical: false,
      scrollOverflow: false,
      touchSensitivity: 15,
      normalScrollElementTouchThreshold: 5,
      //Accessibility
      keyboardScrolling: true,
      animateAnchor: true,
      recordHistory: true,
      //Design
      controlArrows: false,
      verticalCentered: false,
      resize: false,
      fixedElements: '#header, .footer',
      responsive: 0,
      //Custom selectors
      sectionSelector: '.section',
      slideSelector: '.hz-slide',
      normalScrollElements: '#SliderPRO, .slider',
      //events
      onLeave: function(index, nextIndex, direction) { },
      afterRender: function() {
        jQuery.fn.fullpage.silentMoveTo( jQuery('.hz-slide.active').data('anchor'));
        jQuery.fn.fullpage.setAllowScrolling(false, 'left, right');
      },
      afterResize: function() {
        jQuery.fn.fullpage.silentMoveTo('0', jQuery('.hz-slide.active').data('anchor'));
        jQuery.fn.fullpage.setAllowScrolling(false, 'left, right');
      },
      onSlideLeave:function(anchorLink, index, slideIndex, direction, nextSlideIndex){
        
         //console.log('Saliendo'+anchorLink+index+slideIndex+direction+nextSlideIndex);
      },
      afterSlideLoad: function(anchorLink, index, slideAnchor, slideIndex) {
            
        //JEAB SI SOLO HAY UN CAMPUS MANDAMOS A SEC-PROMEDIO
        if(slideAnchor == 'sec-campus') {
          if(getCookie('campusCounter') == 1) {
            var uncampus = jQuery("#select-campus option[class='item-available']").text();
            var uncampusdata = jQuery("#select-campus option[class='item-available']").data("campus");
            //jQuery("#select-campus option[class='item-available']").addClass('item-selected');
            jQuery("#select-campus option[value='"+jQuery("#select-campus option[class='item-available']").text()+"']").attr('selected','selected');
            //JEAB CURRENT
            setCampus(uncampus);
            jQuery('#calcApp').data("campus", uncampusdata);
            jQuery('#calcApp').data("decampus", uncampusdata);
            jQuery('.camp-sel').html(('decampus'));
            //console.log(jQuery('#calcApp').data);
            irA("sec-promedio");
            //jQuery.fn.fullpage.silentMoveTo(0, "sec-promedio");
          }
        }

        //JEAB Validar si estamos en secciones posteriores para quitar el margen inferior a los bloques
        if( jQuery(window).width() > 768 ){

          if (slideAnchor == 'sec-cursar' || slideAnchor == 'resultados' || slideAnchor == 'sec-campus' || slideAnchor == 'sec-promedio') {

              jQuery("#bloques-sec-cursar").addClass('margin-bottom-ln');
              jQuery("#p-nombre-carrera").addClass('p-bloques');
              jQuery("#p-campus").addClass('p-bloques');

              jQuery("#d-promedio").addClass('d-bloques');
              jQuery("#d-materias").addClass('d-bloques');

              jQuery("#boton-suma").addClass('b-botones');
              jQuery("#boton-resta").addClass('b-botones');

              jQuery("#boton-suma-materias").addClass('b-botones');
              jQuery("#boton-resta-materias").addClass('b-botones');
          
          }
        }

        scrollToTop();
        if(slideAnchor=="sec-cursar"){
          if(jQuery('#calcApp').data('selectLinea')){
            jQuery('#'+jQuery('#calcApp').data('selectLinea')).height(jQuery(window).height());
          }
          if(jQuery('.backMenu').hasClass('cerrarExpandible')){
            //JEAB EVITAR QUE SE QUITE EL LOGO DE NAVBAR
            //jQuery('.backMenu').fadeIn();
            //jQuery('.unitec-logo').fadeOut();
            jQuery('.nuevo-boton-regresar').fadeIn();
            //console.log('test');
            //jQuery('#new-button-back').removeAttr("style");
          }else{
            jQuery('.backMenu').fadeOut();
            jQuery('.unitec-logo').fadeIn();
          }
          setCookie('frmunitec',"calculadora",1);



        }else{
          jQuery('.backMenu').fadeOut();
          jQuery('.unitec-logo').fadeIn();
        }
      
        if(slideAnchor=="sec-campus"){ 
          if(jQuery(window).width()<768){
            //dataLayer.push({'event': 'calcu_mbl_campus'});
          }else{
            //dataLayer.push({'event': 'calcu_dsktp_campus'});
            if(jQuery('#'+jQuery('#calcApp').data('selectLinea')).find('.clcCarrerasMod-dsk:checked').size()==0){ jQuery.fn.fullpage.moveTo(0, "sec-cursar"); jQuery('.modstatus').html('Por favor selecciona una modalidad').css('color','red');}else{ jQuery('.modstatus').empty();}  
          } 

        }

        
        if(slideAnchor=="resultados"){

          /* JEAB Para el boton de pago de colegiatura*/
          jQuery('.toggle-off').html('<span style="margin-left:-10px;">No</span>');
          jQuery('.toggle-on').html('<span>Si</span>');

          /*if(jQuery(window).width()<768){dataLayer.push({'event': 'calcu_mbl_resultado'});}else{dataLayer.push({'event': 'calcu_dsktp_resultado'});}*/
          //JEAB eliminar años para terminar la carrera
          if( jQuery(window).width() < 768 ){

              jQuery('.anios-carrera').addClass('anios-carrera-mobile');

          }else {

            jQuery('.anios-carrera').removeClass('anios-carrera-mobile');

          }

        }
        
        if(slideAnchor=='resultados' && jQuery('#calcApp').data('selectMateria')>0){ 
          //debuglog("Mostrando resultado al usuario");
          if(checkCookie('frmunitec')==false){envioAspirantes();}
        }
        /*Control de modalidades*/
        if(slideAnchor=='sec-modalidades' && jQuery('.clcCarrerasMod').size()==1){jQuery('.clcCarrerasMod').click();  /*jQuery.fn.fullpage.silentMoveTo(0, "sec-campus"); */}
        if(slideAnchor=='sec-campus' && jQuery('.clCampus').size()==1 && jQuery('#calcApp').data('crtcampus')!=1 ){ 
          jQuery('.clCampus').click();  
          jQuery.fn.fullpage.silentMoveTo(0, "sec-promedio"); 
        }           
        if(slideAnchor=='sec-campus' && jQuery('.clCampus.item-available').size()==1){
          var div=jQuery('.clCampus.item-available');
          jQuery(div).addClass('item-selected');
          jQuery(div).append("<h2 class='no-disponible hidden-xs hidden-sm'>Campus Seleccionado</h2>");
          jQuery('#calcApp').data("campus",jQuery(div).data("campus"));
          jQuery('#calcApp').data("decampus",validaCampus(jQuery(div).data("campus")));
        } 
        if(getParameterByName("utm_campus")!=null && slideAnchor=='sec-campus'){
          setTimeout(function(){ jQuery('div[data-campus="'+getParameterByName("utm_campus")+'"]').click(); },500);
        }
        if(getParameterByName("utm_ln")!=null&&slideAnchor=="sec-cursar"){ 
          setTimeout(function(){ jQuery('div[data-linea="'+getParameterByName("utm_ln")+'"]').click(); },100);
          if(getParameterByName("utm_ln")=="PREPARATORIA"){
            setTimeout(function(){jQuery('.sel-carrera option:eq(1)').prop('selected', true).change();},200);
          }
        }
        if(slideAnchor=='sec-promedio'||slideAnchor=='sec-materias'){
          if(jQuery('#calcApp').data('campus')==null ){
            mostrarAlerta("Debes elegir un campus ",1);
          }
        }
        if(slideAnchor=='sec-categoria' && jQuery('.clcCategoria').size()==1){ 
          jQuery('.clcCategoria').click();  
          jQuery.fn.fullpage.silentMoveTo(0, "sec-modalidades"); 
        }
        jQuery.fn.fullpage.setAllowScrolling(false, 'left, right');
        if (slideIndex > 1) {
          jQuery('.color-control').fadeIn();

          if(slideAnchor=='sec-cursar'){
          jQuery('.color-control>div').removeClass("naranja");
          jQuery('.color-control>div').addClass("azul");
          jQuery('.color-control>div').eq(0).addClass("naranja");
          //JEAB Eliminamos clase
          jQuery('#i-cambiar-carrera').removeClass('show-fa');
          jQuery('#i-cambiar-carrera').addClass('hide-fa');
          jQuery('#i-cambiar-campus').removeClass('show-fa');
          jQuery('#i-cambiar-campus').addClass('hide-fa');

          }

          if (slideAnchor == 'sec-campus') {//si la seccion es campus
          jQuery('.color-control>div').removeClass("naranja");
          jQuery('.color-control>div').addClass("azul");
          jQuery('.color-control>div').eq(1).addClass("naranja");
          //JEAB Añadimos indicador de que es clickeable 
          jQuery('#i-cambiar-carrera').removeClass('hide-fa');
          jQuery('#i-cambiar-carrera').addClass('show-fa');
          jQuery('#i-cambiar-campus').removeClass('show-fa');
          jQuery('#i-cambiar-campus').addClass('hide-fa');

          }
          if (slideAnchor == 'sec-promedio') {//si la seccion es promedio
          jQuery('.color-control>div').removeClass("naranja");
          jQuery('.color-control>div').addClass("azul");
          jQuery('.color-control>div').eq(2).addClass("naranja");
          //JEAB Añadimos indicador de que es clickeable
          jQuery('#i-cambiar-campus').removeClass('hide-fa');
          jQuery('#i-cambiar-campus').addClass('show-fa');
          }
          if (slideAnchor =='sec-materias') {//si la seccion es materias
          jQuery('.color-control>div').removeClass("naranja");
          jQuery('.color-control>div').addClass("azul");
          jQuery('.color-control>div').eq(3).addClass("naranja");
          }
        } else {
          jQuery('.color-control').fadeOut();
        }
      }//TERMINA AFTERSLIDELOAD

    }); //jQuery('#fullpage').fullpage

} //TERMINA FUNCION initialization

jQuery('.moveSlideRight').click(function(e) { e.preventDefault(); jQuery.fn.fullpage.moveTo(1, 1); });
  
jQuery('body').on("click",".paso3",function(e){
    init();
    e.preventDefault();
    jQuery.fn.fullpage.moveTo(1, 2);
});

/***********CONTROL DE BOTONES TODO OPTIMIZAR*************/
jQuery('.btn-campus').click( function(e) { 
  
  //JEAB VALIDAR QUE NO SE EJECUTE LA ACCION SI NO TIENE DATOS NECESARIOS
  if( jQuery(window).width() < 768) {

        var linea = jQuery('#calcApp').data().selectLinea;
        var url = jQuery('#calcApp').data().url;
        var idCosto = jQuery('#calcApp').data().idCosto;

        if(typeof linea != 'undefined' &&  typeof url != 'undefined' && typeof idCosto != 'undefined') {

          jQuery('#calcApp').data("crtcampus", 1); 
          e.preventDefault(); 
          jQuery('.color-control>div').removeClass("naranja"); 
          jQuery('.color-control>div').addClass("azul"); 
          jQuery.fn.fullpage.moveTo(0, "sec-campus"); 
          selectColor(this); 

        }

  }else {

      jQuery('#calcApp').data("crtcampus", 1); 
      e.preventDefault(); 
      jQuery('.color-control>div').removeClass("naranja"); 
      jQuery('.color-control>div').addClass("azul"); 
      jQuery.fn.fullpage.moveTo(0, "sec-campus"); 
      selectColor(this); 
  }

});


jQuery('body').on("click",'.btn-cursar',function(e) { 
    
    if(jQuery(window).width()<768){
      reiniciarAppData(); 
      jQuery('#calcApp').removeData('campus')  ; 
    }
    e.preventDefault(); 
    jQuery('.color-control>div').removeClass("naranja"); 
    jQuery('.color-control>div').addClass("azul"); 
    jQuery.fn.fullpage.moveTo(0, "sec-cursar"); 
    selectColor(this); 
    jQuery("#"+jQuery('.cerrarExpandible').data("affected")).css({"height":jQuery(window).height()+"px"});

});


jQuery('.btn-promedio').click(function(e) { 

    if( jQuery(window).width() < 768 ) {

        //VALIDAR QUE NO SE EJECUTE LA ACCION SI NO TIENE DATOS NECESARIOS
        var linea = jQuery('#calcApp').data().selectLinea;
        var url = jQuery('#calcApp').data().url;
        var idCosto = jQuery('#calcApp').data().idCosto;
        var campus = jQuery('#calcApp').data().campus;
        var promedio = jQuery('#calcApp').data().promedio;
        if(typeof linea != 'undefined' &&  typeof url != 'undefined' && typeof idCosto != 'undefined' && typeof campus != 'undefined' && typeof promedio != 'undefined') { 

            mobileExpand('btn-promedio');
        
        }
         
    }

    if(e.target.className.match(/control-menos/)){
      //JEAB BOTONES PROMEDIO MOBILE
      jQuery('.control-mas').css({'visibility':'visible'});
      var sliderp="ex13";
      var valor= jQuery('#'+sliderp).slider("getValue");
      var nuevo = valor-0.1;
      if(nuevo<=6 ){jQuery('.control-menos').css({'visibility':'hidden'});}
      if(nuevo>=6 ){
        jQuery('.promedio-val').html(nuevo.toFixed(1));
        jQuery('#'+sliderp).slider("setValue",nuevo);
      }else{
        jQuery('.control-menos').css({'visibility':'hidden'});
      }
      jQuery('#calcApp').data("promedio",jQuery("#ex13").slider('getValue'));
      jQuery('#calcApp').data("materias",jQuery("#ex14").slider('getValue'));
      getBecas();
      getDuracion();
      //JEAB Agregar texto al tipo de pago
      jQuery('.toggle-off').html('<span style="margin-left:-10px;">No</span>');
      jQuery('.toggle-on').html('<span>Si</span>');
       //JEAB eliminar años para terminar la carrera
      if( jQuery(window).width() < 768 ){

            jQuery('.anios-carrera').addClass('anios-carrera-mobile');

      }else {

            jQuery('.anios-carrera').removeClass('anios-carrera-mobile');

      }
      return false;
    }  //if(e.target.className.match(/control-menos/) 

    if(e.target.className.match(/control-mas/)){
      jQuery('.control-menos').css({'visibility':'visible'});
      var sliderp="ex13";
      var valor= jQuery('#'+sliderp).slider("getValue");
      var nuevo = valor+0.1;
      if(nuevo==10){jQuery('.control-mas').css({'visibility':'hidden'});}
      if(nuevo<=10){
        jQuery('.promedio-val').html( nuevo.toFixed(1) );
        jQuery('#'+sliderp).slider("setValue",nuevo);
      }
      jQuery('#calcApp').data("promedio",jQuery("#ex13").slider('getValue'));
      jQuery('#calcApp').data("materias",jQuery("#ex14").slider('getValue'));
      getBecas();
      getDuracion();
      //JEAB Agregar texto al tipo de pago
      jQuery('.toggle-off').html('<span style="margin-left:-10px;">No</span>');
      jQuery('.toggle-on').html('<span>Si</span>');
      //JEAB eliminar años para terminar la carrera
      if( jQuery(window).width() < 768 ){

            jQuery('.anios-carrera').addClass('anios-carrera-mobile');

      }else {

            jQuery('.anios-carrera').removeClass('anios-carrera-mobile');

      }
      return false;

    }//if(e.target.className.match(/control-mas/))


}); //jQuery('.btn-promedio').click


jQuery('.btn-materias').click(function(e) {

    if( jQuery(window).width() < 768 ) { 

        //VALIDAR QUE NO SE EJECUTE LA ACCION SI NO TIENE DATOS NECESARIOS
        var linea = jQuery('#calcApp').data().selectLinea;
        var url = jQuery('#calcApp').data().url;
        var idCosto = jQuery('#calcApp').data().idCosto;
        var campus = jQuery('#calcApp').data().campus;
        var promedio = jQuery('#calcApp').data().promedio;
        if(typeof linea != 'undefined' &&  typeof url != 'undefined' && typeof idCosto != 'undefined' && typeof campus != 'undefined' && typeof promedio != 'undefined') { 

            mobileExpand('btn-materias');
        }

    }

    if(e.target.className.match(/control-menos-mat/)){

      jQuery('.control-mas-mat').css({'visibility':'visible'});
      var sliderp="ex14";
      var valor= jQuery('#'+sliderp).slider("getValue");
      var nuevo = valor-1;
      if(jQuery('#calcApp').data('idCosto')=="DENT" && nuevo<4){
        jQuery('.control-menos-mat').css({'visibility':'hidden'});
        return false
      }
      if(nuevo<=1 ){jQuery('.control-menos-mat').css({'visibility':'hidden'});}
      if(nuevo>0 ){
       jQuery('.materias-val').html(nuevo);
       jQuery('#'+sliderp).slider("setValue",nuevo);
      }else{
        jQuery('.control-menos-mat').css({'visibility':'hidden'});
      }
      jQuery('#calcApp').data("promedio",jQuery("#ex13").slider('getValue'));
      jQuery('#calcApp').data("materias",jQuery("#ex14").slider('getValue'));
      getBecas();
      getDuracion();
      // Agregar texto al tipo de pago
      jQuery('.toggle-off').html('<span style="margin-left:-10px;">No</span>');
      jQuery('.toggle-on').html('<span>Si</span>');
      // eliminar años para terminar la carrera
      if( jQuery(window).width() < 768 ){
          jQuery('.anios-carrera').addClass('anios-carrera-mobile');
      }else {
          jQuery('.anios-carrera').removeClass('anios-carrera-mobile');
      }
      return false;
    }//if(e.target.className.match(/control-menos-mat/))

    if(e.target.className.match(/control-mas-mat/)){

     jQuery('.control-menos-mat').css({'visibility':'visible'});
     var sliderp="ex14";
     var nuevo= parseInt(jQuery('#'+sliderp).slider("getValue"))+1;
     if(nuevo>=jQuery('#calcApp').data('maxmaterias')){
      jQuery('.control-mas-mat').css({'visibility':'hidden'});}
      if(nuevo<=jQuery('#calcApp').data('maxmaterias')){
        jQuery('.materias-val').html(nuevo);
        jQuery('#'+sliderp).slider("setValue",nuevo);
      }
      jQuery('#calcApp').data("promedio",jQuery("#ex13").slider('getValue'));
      jQuery('#calcApp').data("materias",jQuery("#ex14").slider('getValue'));
      getBecas();
      getDuracion();
      // Agregar texto al tipo de pago
      jQuery('.toggle-off').html('<span style="margin-left:-10px;">No</span>');
      jQuery('.toggle-on').html('<span>Si</span>');
      // eliminar años para terminar la carrera
      if( jQuery(window).width() < 768 ){
          jQuery('.anios-carrera').addClass('anios-carrera-mobile');
      }else {
          jQuery('.anios-carrera').removeClass('anios-carrera-mobile');
      }
      return false;

    } //if(e.target.className.match(/control-mas-mat/))

}); //jQuery('.btn-materias').click(function(e)

/*************FIN DE CONTROL DE BOTONES*********************/

jQuery('#control-input').on("change paste keyup", function() {watchDogs(); });

jQuery('body').on('click','.clCampus',function(e){
    
    jQuery('#secCampus').find(".item-selected").each(function(){
      jQuery(this).removeClass("item-selected");
      jQuery("#"+this.id+">h2").remove();
    });
    
    if(jQuery(this).hasClass("item-available")){
      jQuery(this).addClass('item-selected');
      jQuery(this).append("<h2 class='no-disponible hidden-xs hidden-sm'>Campus Seleccionado</h2>");
      jQuery('#calcApp').data("campus",jQuery(this).data("campus"));
      jQuery('#calcApp').data("decampus",validaCampus(jQuery(this).data("campus")));
      jQuery('.camp-sel').html(jQuery('#calcApp').data('decampus'));
      irA("sec-promedio");
    }else{
      jQuery('#calcApp').data("nuevoCampus",jQuery(this).data("campus"));
      jQuery('#calcApp').data("decampus",jQuery(this).find('.lineas-caption').html());
      jQuery('#error-modal').modal('show');
      jQuery('.mod-campus').html(jQuery('#calcApp').data("decampus"));
      jQuery('.mod-carrera').html(jQuery('#calcApp').data("selectLinea").toLowerCase()+" "+jQuery('#calcApp').data("deMateria"));
      nuevoCampus=jQuery(this).data("campus");
    }

}); //jQuery('body').on('click','.clCampus'

/*Para la nueva pantalla de seleccionar campus*/

jQuery('body').on('change','.clSelectCampus',function(e){
  
  jQuery("#select-campus").find(".item-selected").each(function(){
    jQuery(this).removeClass("item-selected");
  });
  
  if(jQuery("#select-campus :selected").attr("class") == 'item-available'){

      jQuery("#select-campus :selected").addClass('item-selected');
      jQuery('#calcApp').data("campus",jQuery("#select-campus :selected").data("campus"));
      jQuery('#calcApp').data("decampus",validaCampus(jQuery("#select-campus :selected").data("campus")));
      jQuery('.camp-sel').html(jQuery('#calcApp').data('decampus'));
      irA("sec-promedio");

    }else{

      jQuery('#calcApp').data("nuevoCampus",jQuery(this).data("campus"));
      jQuery('#calcApp').data("decampus",jQuery(this).find('.lineas-caption').html());
      jQuery('#error-modal').modal('show');
      jQuery('.mod-campus').html(jQuery('#calcApp').data("decampus"));
      jQuery('.mod-carrera').html(jQuery('#calcApp').data("selectLinea").toLowerCase()+" "+jQuery('#calcApp').data("deMateria"));
      nuevoCampus=jQuery(this).data("campus");

    }

}); //jQuery('body').on('change','.clSelectCampus'


jQuery('body').on('click','.campus-modal-cambiar',function(){

   if(jQuery(this).hasClass("mod-cambia-campus")){
    jQuery('#calcApp').removeData("campus");
    jQuery('#calcApp').data("campus",jQuery('#calcApp').data("nuevoCampus"));
    jQuery('#error-modal2').modal("hide");
    reiniciarAppData();
    getLineasNegocio();
    setTimeout(function(){ jQuery('#'+nuevaLinea).click();},100)
  }else{
    jQuery('#error-modal').modal("hide");
    jQuery('#calcApp').data("campus",nuevoCampus);
    getLineasNegocio({campus: jQuery("#calcApp").data("campus")});
    setTimeout(function(){jQuery.fn.fullpage.moveTo(0, "sec-cursar")},100);}

}); //jQuery('body').on('click','.campus-modal-cambiar'

jQuery('body').on("change",".sel-carrera",function(e) {

    jQuery('#calcApp').data("selectCategoria",jQuery(this).val());
    jQuery('#calcApp').data("deMateria", jQuery(" option:selected" ,this).text());
    //debuglog("Escogio materia: "+jQuery('#calcApp').data("deMateria"));
    jQuery('#calcApp').removeData("campus");
    getModalidad("",jQuery(this).val());
  
}); //jQuery('body').on("change",".sel-carrera"


//Boton para expander las lineas de negocio en la calculadora
jQuery('body').on('click','.cerrarExpandible',function(){

    var elemento=jQuery('.cerrarExpandible').data("affected");
    jQuery('#'+elemento+'>.linea-elementos').children().not('.lineas-caption').hide();
    var arriba=jQuery('#'+elemento).data("ex-top");
    var derecha=jQuery('#'+elemento).data("ex-right");
    var abajo=jQuery('#'+elemento).data("ex-bottom");
    var izquierda=jQuery('#'+elemento).data("ex-left");
    var alto=jQuery('#'+elemento).data("ex-height");
    var ancho=jQuery('#'+elemento).data("ex-width");
    jQuery('#'+elemento).animate({"height": alto+"px","width":ancho,"top":arriba,"left":izquierda}, 500,function(){
      jQuery('#'+elemento).css({"position":"relative","z-index":"1","top":0});
      jQuery('.linea-elementos').css({ "width":"1%", "height":"1%"});
    });
    jQuery('.backMenu').fadeOut();
    jQuery('.unitec-logo').fadeIn();
    jQuery('.backMenu').removeClass('cerrarExpandible');

}); //jQuery('body').on('click','.cerrarExpandible'


jQuery('body').on('click','.clcCategoria',function(){
    jQuery('#secCategorias').find(".item-selected").each(function(){
      jQuery(this).removeClass("item-selected");
      jQuery("#"+this.id+">h2").remove();
    });
    if(jQuery(this).hasClass("item-available")){jQuery(this).addClass('item-selected');jQuery(this).append("<h2 class='no-disponible hidden-xs hidden-sm'>Campus Seleccionado</h2>");}

}); //jQuery('body').on('click','.clcCategoria'


jQuery('body').on('click', '.btn-esquema', function(){

    jQuery(this).removeClass('btn-default').addClass('btn-primary');
    jQuery(this).parent().siblings().find('button').removeClass('btn-primary').addClass('btn-default');

}); //jQuery('body').on('click', '.btn-esquema'


jQuery('body').on('click', '.clcSelector', function(){

    if( jQuery('#calcApp').data('selectLinea')){
      jQuery('.crtl>p>i:eq(0)').addClass('fa-check');
    }
    if( jQuery('#calcApp').data('campus')){
      jQuery('.crtl>p>i:eq(1)').addClass('fa-check');
    }

}); //jQuery('body').on('click', '.clcSelector'


jQuery('body').on('click', '#aceptar', function(){
    if( jQuery('#calcApp').data('promedio')){
      jQuery('.promctrl').fadeIn();
      jQuery('.promedio-val').html(jQuery('.promedio-val').eq(1).html());
      jQuery('.crtl>p>i:eq(2)').addClass('fa-check');
    }
    if( jQuery('#calcApp').data('materias')){
      jQuery('.matctrl').fadeIn();
      jQuery('.materias-val').html(jQuery('.materias-val').eq(1).html());
      jQuery('.crtl>p>i:eq(3)').addClass('fa-check');
    }

});//jQuery('body').on('click', '#aceptar'

});
//<![CDATA[
  jQuery(window).load(function() { // makes sure the whole site is loaded
      jQuery('#status').fadeOut(); // will first fade out the loading animation
      jQuery('#preloader').delay(350).fadeOut('slow'); // will fade out the white DIV that covers the website.
      jQuery('body').delay(350).css({'overflow':'visible'});
  })
//]]>

//TERMINA PARTE 1 DEL SIMULADOR DE COSTOS

//PARTE 2 DEL SIMULADOR DE COSTOS

jQuery(document).ready(function() {

  var patternVocals = /[aeiou]/i;
  var patternConsonants = /[bcdfghjklmnñpqrstvwxyz]/i;
  var patternFourConsonants = /[bcdfghjklmnñpqrstvwxyz]{5}|[bcdfghjklmnñpqrstvwxyz]{2}\s[bcdfghjklmnñpqrstvwxyz]{3}|[bcdfghjklmnñpqrstvwxyz]{3}\s[bcdfghjklmnñpqrstvwxyz]{2}/i;//4 consonantes consecutivas con o sin espacio, ejem: davidd ffranco, daviddffranco
  var patternFourVocals = /[aeiou]{5}|[aeiou]{2}\s[aeiou]{3}|[aeiou]{3}\s[aeiou]{2}/i;//4 vocales consecutivas con o sin espacio, ejem: francoo oomar, francoooomar
  var patternSpacePlusConsonants = /^\s[bcdfghjklmnñpqrstvwxyz]{2,}/i; //No iniciar con espacio seguido de 2 o mas consonantes
  var patternStartWithSpace = /^\s/; //No iniciar con espacio

  jQuery(window).on("resize load",function(){
    if(jQuery(window).width()>768){
      jQuery(".calcu-segunda-seccion").appendTo("#home-calc");
      //dataLayer.push({'event': 'calcu_dsktp_formulario'});
    }else{
      //dataLayer.push({'event': 'calcu_mbl_intro'});
    }
  });

  /* Método nombre basura */
  jQuery.validator.addMethod("basura", function(value, element) {
    var strBasura = '/*/content';
    var qryBasura = queryHandler(jsonBasuraContenido, strBasura);
    var arrBasuraContent = [];
    var array = jQuery.map(qryBasura, function(v, i) {return [v];});
      // Valida texto contenido en arrBasura
      for(i = 0; i < array.length; i++){
        var arrRes = array[i];
        arrBasuraContent.push(arrRes['value']);
      }
      for(i=0; i<arrBasuraContent.length; i++){
        var existe = value.toLowerCase().indexOf(arrBasuraContent[i].toLowerCase());
        if(existe != -1){return false;}
      }
      // Valida texto completo en arrBasura
      strBasura = '/*/completo';
      qryBasura = queryHandler(jsonBasuraContenido, strBasura);
      var arrBasuraCompleto = [];
      array = jQuery.map(qryBasura, function(v, i) {return [v];});
      // Valida exista en arrBasura
      for(i = 0; i < array.length; i++){var arrRes = array[i];arrBasuraCompleto.push(arrRes['value']);}
        if(jQuery.inArray(value,arrBasuraCompleto) != -1){return false;}
      // Valida que sean sólo letras
      var patron = /^[a-zA-Záíúéóñ\s]*$/;
      if(value.search(patron) == -1){return false;}
      return true;
    }, "Mensaje error");

jQuery.validator.addMethod("basura_completo", function(value, element) {
        var texto=jQuery.trim(value.toLowerCase());
    
        //console.log("VALUE/"+texto);

        var strBasura = '/*/completo';
        var qryBasura = queryHandler(jsonBasuraCompleto, strBasura);
        var arrBasuraComplete = [];
        var array = jQuery.map(qryBasura, function(texto, i) {
            return [texto];
        });


        // Valida texto contenido en arrBasura
        for(i = 0; i < array.length; i++){
            var arrRes = array[i];
            arrBasuraComplete.push(arrRes['value']);
            //console.log(arrRes['value']);
        }

        var repetido=arrBasuraComplete.indexOf(texto);
         //console.log(repetido);
        if(repetido!=-1){
           return false;
        }else{ return true;}

      

        // Valida que sean sólo letras
        var patron = /^[a-zA-Záíúéóñ\s]*$/;
        if(value.search(patron) == -1){
            //////console.log('invalido letras, v='+value);
            return false;
        }

        return true;
    });

/* Método valida email */
  jQuery.validator.addMethod("validaEmail", function(value, element) {
        //console.log(value+"-"+element);
        //return this.optional( element ) ;/*|| /[\w-\.]{2,}@([\w-]{2,}\.)*([\w-]{2,}\.)[\w-]{2,4}/.test( value );*/
        var arrEmail = value.split('@');
        var lenUser = arrEmail[0].length;
        if(arrEmail.length <= 1)
            return false;
        var arrDominio = arrEmail[1].split('.');
        var arrLargo = arrDominio.length;
        
        if(arrEmail[0]==="") //Esta vacio el nombre?
            return false;
        
        if(lenUser <= 3 || lenUser > 30) //es muy largo o muy corto el nombre?
            return false;

        if(arrEmail[0].search(/[!"'#$%&\/()=?¿\+¨´¡\\\*\{\}\[\]\|ñ,;:~`^<>°¬@]/)!=-1) //Tiene caracteres especiales?
            return false;

        var strBasuraNombre = '/*/content';
        var qryBasuraNombre = queryHandler(jsonBasuraContenido, strBasuraNombre);
        var arrBasuraContentNombre = [];
        var arrayNombre = jQuery.map(qryBasuraNombre, function(v, i) {
            return [v];
        });
        for(var zz = 0; zz < arrayNombre.length; zz++){
            var arrResNombre = arrayNombre[zz];
            arrBasuraContentNombre.push(arrResNombre['value']);
        }

        for(var z=0; z<arrBasuraContentNombre.length; z++){
            var existeBasura = arrEmail[0].toLowerCase().indexOf(arrBasuraContentNombre[z]);

            //////console.log("existe: "+existe);
            if(existeBasura != -1){
                ////console.log('invalido content, v='+value+', arrB='+arrBasuraContent[i]+', exs='+existe);
                return false;
            }
        } 



        if (arrLargo < 2){ //el numero de palabras en el dominio solo es una? ejem: xxx@yyy //sin punto(.) ya que el split si lo encuentra lo divide en 2
            return false;
        }

        else{

        var strBasura = '/*/content';
        var qryBasura = queryHandler(jsonBasuraContenidoEmail, strBasura);
        var arrBasuraContent = [];
        var array = jQuery.map(qryBasura, function(v, i) {
            return [v];
        });

        // Valida texto contenido en arrBasura
        for(i = 0; i < array.length; i++){
            var arrRes = array[i];
            arrBasuraContent.push(arrRes['value']);
        }



            for (var x = 0; x <= arrLargo-2; x++) { // Verificamos desde el primer elemento al penultimo, ya que el ultimo el es el nombre de dominio superior y requerimos otras validaciones
                if (arrDominio[x]==="") //esta vacio el nombre del dominio o subdominio?
                    return false;
                if(arrDominio[x].length > 15 || arrDominio[x].length < 3) //es muy largo o muy corto el nombre del dominio?
                    return false;
                if(arrDominio[x].search(/[!"'#$%&\/()=?¿\+¨´¡\\\*\{\}\[\]\|ñ,;:~`^<>°¬@]/)!=-1) //tiene caracteres especiales?
                    return false;
                /*if(arrDominio[i].search(/facebook/i)!=-1) //tiene el nombre facebook? //Validacion para nombre de dominio facebook
                    return false;*/

                for(i=0; i<arrBasuraContent.length; i++){
                    var existe = arrDominio[x].indexOf(arrBasuraContent[i]);

                    //////console.log("existe: "+existe);
                    if(existe != -1){
                        ////console.log('invalido content, v='+value+', arrB='+arrBasuraContent[i]+', exs='+existe);
                        return false;
                    }
                }                    
            }

                //comprobamos el nombre del dominio superior

            if(arrDominio[arrLargo-1]==="") // esta vacio?
                return false;
            if(arrDominio[arrLargo-1].length >= 4 || arrDominio[arrLargo-1].length <= 1) // tiene la longitud correcta?
                return false;
            /*if(arrDominio[arrLargo-1].search(/[!"'#$%&\/()=?¿\+¨´¡\\\*\{\}\[\]\|ñ,;:~`^<>°¬@]/)!=-1) // tiene caracteres especiales?
                return false;*/


  
        }



        return true;
    });

    var patternVocals = /[aeiou]/i;
    var patternConsonants = /[bcdfghjklmnñpqrstvwxyz]/i;
    var patternFourConsonants = /[bcdfghjklmnñpqrstvwxyz]{6}/i;//5 consonantes consecutivas con o sin espacio, ejem: davidd ffranco, daviddffranco
    var patternFourVocals = /[aeiou]{6}/i;//5 vocales consecutivas con o sin espacio, ejem: francoo oomar, francoooomar    
    var patternStartWithSpace = /^\s/; //No iniciar con espacio
    var patternRemoveAllSpaces = /\s/g; //quitar espacios en blanco

    jQuery.validator.addMethod("validateName",function(value,element){
        if(value.match(patternVocals)!=null && value.match(patternConsonants)==null){
            //console.log("son puras vocales");
            return false;
        }
        if(value.match(patternVocals)==null && value.match(patternConsonants)!=null){
            //console.log("son puras consonantes");
            return false;
        }
        if(value.match(patternFourConsonants)!=null){
        //console.log("no mas de 4 consonantes juntas");
        return false;
        }

        if(value.match(patternFourVocals)!=null){
            //console.log("no mas de 4 vocales juntas");
            return false;
        }

        if(value.match(patternStartWithSpace)!=null){
            //console.log("no iniciar con esapcio en blanco");
            return false;
        }

        if(value.match(patternSpacePlusConsonants)!=null){
            //console.log("no iniciar con esapcio en blanco seguido de dos consonantes");
            return false;
        }

        return true;    
        
    });

    jQuery.validator.addMethod("validaTel", function(value, element) {
        //////console.log(checkSecuencial(value));

       if( checkRepetido(value)>3 || checkSecuencial(value)>3 || value=="5556581111" || value.charAt(0)==0 ){
        return false;
       }else{
        return true;
       }
       
    }, "Teléfono Incorrecto");


    /*JEAB CORRECCION DE VALIDACIONES DE CALCULADORA JEAB 28-04-2016*/
    
    jQuery("#frm-calculadora").validate({
        
        ignore: "",
            
        rules: {

            frm_nombre:{basura : true, basura_completo:true, validateName:true, required : true, minlength: 3},
            frm_apaterno:{basura : true, basura_completo:true, validateName:true, required : true, minlength: 3},
            frm_amaterno:{basura : true, basura_completo:true, validateName:true, required : true, minlength: 3},
            frm_correo:{required:true, validaEmail:true,email:true},
            frm_celular:{required:true, validaTel:true, minlength:10, number:true},
            frm_informacion:{required : true},
            S_alumno:{required : true},
            politicas: {required:true}

        },//Fin de rules

        messages: {

            frm_nombre: {
                    basura:"Ingresa un nombre válido",
                    basura_completo:"Ingresa un nombre válido",
                    validateName:"Nombre inválido",
                    required:"Por favor ingresa un nombre",
                    minlength:"Nombre inválido"
            },
            
            S_alumno: {
                required:"¿Eres alumno de la UNITEC?"
            },

            frm_apaterno: {
                basura:"Ingresa un apellido Paterno válido",
                basura_completo:"Ingresa un apellido Paterno válido",
                validateName:"Apellido Paterno inválido",
                required:"Por favor ingresa un apellido paterno",
                minlength:"Apellido Paterno inválido"
            },

            frm_amaterno: {
                basura:"Ingresa un apellido Materno válido",
                basura_completo:"Ingresa un apellido Materno válido",
                validateName:"Apellido Materno inválido",
                required:"Por favor ingresa un apellido Materno",
                minlength:"Apellido Materno inválido"
            },

            frm_correo: {
                required:"Por favor ingresa un correo válido", 
                email:"Correo Inválido",
                validaEmail:"Correo Inválido"
            },

            frm_celular: {
                required:"Por favor ingrese un celular valido",
                number:"Revisa tu numero celular",
                minlength:"Al menos debe contener 10 dígitos"
            },
            
            frm_informacion: {
              required : "Selecciona el tipo de información que necesitas."
            },
            
            politicas: { 
                required:"Debes aceptar las políticas de privacidad"
            }
      },//Fin de messages
            
      showErrors: function(errorMap, errorList) {
          
          jQuery("#frm-calculadora").find(".frm-err").each(function() {jQuery(this).remove();});

          if( errorList.length ) {

              jQuery("#frm-calculadora").find(".frm-err").each(function() {
                      
                  jQuery(this).remove();                        
              
              });
                
              if( jQuery(errorList[0]['element']).next().hasClass("frm-err") ){
                
              }else{
                    
                  //JEAB AÑADIMOS CLASE DE BOOTSTRAP ALERT
                  jQuery('<div class="frm-err alert-danger frm-calc-error"></div>').insertAfter(errorList[0]['element']);
              }

              if( errorList.length ) {

                  if( getCookie('lastError') != errorList[0]['element'].id ){
                      
                      switch( errorList[0]['element'].id ) {
                          
                          case "frm_nombre":
                              var msj="nombre";
                              break;

                          case "frm_apaterno":
                              var msj="apaterno";
                              break;
                          
                          case "frm_amaterno":
                              var msj="amaterno";
                              break;

                          case "frm_correo":
                              var msj="correo-electronico";
                              break;
                          
                          case "frm_celular":
                              var msj="celular";
                              break;

                          case "frm_informacion":
                              var msj="tipo-informacion";
                              break;
                          
                          case "S_alumno":
                              var msj="eres-alumno";
                              break;
                          
                          case "politicas": case "frm_politicas":
                              var msj="acepta-politicas";
                              break;
                          
                          default:
                              var msj="(not set)"+errorList[0]['element'].id;

                      }//Termina switch

                      switch( errorList[0]['method'] ) {
                      
                          case "required":  case "frm_informacion": case "politicas": 
                              var metodo="vacio";
                              break;
                          
                          case "validaTel": case "email": case "basura": case"basura_completo": case "validateName": case "validaEmail": case "number":
                              var metodo="invalido";
                              break;

                          case "minlength": case "maxlength":
                              var metodo="longitud";
                              break;
                          
                          default:
                              var metodo="(not set)"+errorList[0]['method'];

                      }//Termina swicth
                      //console.log("Enviando error2 "+"calc:"+msj+' | '+metodo);
                      dataLayer.push({

                          'errorField': 'frm:'+msj+' | '+metodo, //dato dinámico
                          'event': 'errorForm'//dato estático
                      }); 
                      
                      setCookie( 'lastError', errorList[0]['element'].id, 1 );

                  }//Termina if getCookie
                                

                  jQuery("#frm-registro").find(".frm-err").each(function() {jQuery(this).remove();});
                   //////console.log(jQuery(errorList[0]['element']).next().hasClass("frm-err")); 
                                
                  if(jQuery(errorList[0]['element']).next().hasClass("frm-err")){
                                    
                  }else{
                  
                      if(errorList[0]['element']['name']=="frm_politicas"){

                          jQuery('<div class="frm-err">'+errorList[0]['message']+'</div>').insertAfter(".politicas");

                      }else{

                          jQuery('<div class="frm-err"></div>').insertAfter(errorList[0]['element']);

                      }   

                  } //fin else  
                  
                  if(errorList.length && errorList[0]['element']['name']!="frm_politicas") {

                      // console.log(errorList[0]);
                      jQuery(errorList[0]['element']).next().html(errorList[0]['message']); 
                                        
                   }
              }//termina if errorList.length

            }//termina if errorList.length

      },//termina showErrors

      errorPlacement: function(error, element) {

        error.insertBefore(element);
      
      },

      submitHandler: function(form) {

        init();
        jQuery.fn.fullpage.moveTo(1, 2);
        var d = new Date(); 
        var hora=d.getDate() + "/" + (d.getMonth() +1) + "/" + d.getFullYear()+ ', '+d.getHours()+':'+d.getMinutes()+':'+d.getSeconds();
        //debuglog(hora);
        //debuglog("------FORMULARIO COMPLETADO-----------------NUEVO USUARIO--------------");
        var Nombre               =quitaAcentos(jQuery('#frm_nombre').val());
        var Ap_Pat               =quitaAcentos(jQuery('#frm_apaterno').val());
        var Ap_Mat               =quitaAcentos(jQuery('#frm_amaterno').val());
        var email                =formatEmail(jQuery('#frm_correo').val());
        var informacion_2        =1;
        var Estado               =estadoRandom(jQuery('#frm_celular').val());
        var masculino            ='M';
        var TipoTel              ='cel_calc';
        var Telef                =jQuery('#frm_celular').val();
        var periodo_interes_sel  ='16-1';
        var politica             ='on';
        //debuglogname(Nombre+" | "+Ap_Pat+" | "+Ap_Mat+" | "+email+" | "+jQuery('#frm_informacion').val()+" | "+Estado+" | "+Telef);
      }

    });
});

//TERMINA PARTE 2 DEL SIMULADOR DE COSTOS

//PARTE 3 DEL SIMULADOR DE COSTOS
function hasPlaceholderSupport() {
  var input = document.createElement('input');
  return ('placeholder' in input);
}

if(!hasPlaceholderSupport()){
    var inputs = document.getElementsByTagName('input');
    for(var i=0,  count = inputs.length;i<count;i++){
        if(inputs[i].getAttribute('placeholder')){
            inputs[i].style.cssText = "color:#FFF;font-style:italic;"
            inputs[i].value = inputs[i].getAttribute("placeholder");
            inputs[i].onclick = function(){
                if(this.value == this.getAttribute("placeholder")){
                    this.value = '';
                    this.style.cssText = "color:#000;font-style:normal;"
                }
            }
            inputs[i].onblur = function(){
                if(this.value == ''){
                    this.value = this.getAttribute("placeholder");
                    this.style.cssText = "color:#FFF;font-style:italic;"
                }
            }
        }
    }
}

/*****************************MORPHING SEARCH***************************************/
var morphSearch = jQuery( '#morphsearch' ),
input = jQuery( '.morphsearch-input' ),
ctrlClose = jQuery( '.morphsearch-close' ),
isOpen = false,

// show/hide search area
toggleSearch = function(evt) {
  if( evt.type.toLowerCase() === 'focus' && isOpen ) //Si esta en pantalla completa el buscador y el foco lo tiene el input, no hace nada
  return false;

  if( isOpen ) {
    if(jQuery(document.body).hasClass('descuentos-comerciales') || jQuery(document.body).hasClass('politicas-de-privacidad') || jQuery(document.body).hasClass('terminos-y-condiciones')){

    jQuery('html').attr("style","overflow: visible !important");
  }else{
    jQuery.fn.fullpage.setAllowScrolling(true);
    jQuery.fn.fullpage.setKeyboardScrolling(true);
  }

  if(navigator.userAgent.search(/Mobi/i)!=-1){
    jQuery('.morphsearch').css({"backgroundColor":"transparent"});
    input.attr("placeholder","");
  }

  jQuery(morphsearch).removeClass('open');
  if( jQuery(input).val() !== '' ) {
    setTimeout(function() {
        jQuery(morphsearch).addClass('hideInput');
        setTimeout(function() {
          jQuery(morphsearch).removeClass('hideInput');
          jQuery(input).val("");
          jQuery(".morphsearch-content").html('');
        }, 300 );
      }, 500);
  }
  jQuery(input).blur();
}
    else {
         if(jQuery(document.body).hasClass('descuentos-comerciales') || jQuery(document.body).hasClass('politicas-de-privacidad') || jQuery(document.body).hasClass('terminos-y-condiciones')){
            jQuery('html').attr("style","overflow: hidden !important");
         }else{
            jQuery.fn.fullpage.setAllowScrolling(false);
            jQuery.fn.fullpage.setKeyboardScrolling(false);
        }
        jQuery(morphsearch).addClass('open');
        
        if(navigator.userAgent.search(/Mobi/i)!=-1){
        jQuery(".morphsearch").css({"backgroundColor":"#fff"});    
            input.attr("placeholder","Buscar...");
        }
    }
    isOpen = !isOpen;
};

if(navigator.userAgent.search(/Mobi/i)!=-1){
input.attr("placeholder","");
morphSearch.css({"backgroundColor":"transparent"});
}
// events
jQuery(input).focus(toggleSearch);
jQuery(ctrlClose).click(toggleSearch);
jQuery(document).keydown(function(ev){
var keyCode = ev.keyCode || ev.which;
if( keyCode === 27 && isOpen ) {
    toggleSearch(ev);
}
});
input.val("");
jQuery(".morphsearch-content").html('');

var options = {};
var orderBy = {};
orderBy['keys'] = [{label: 'Relevancia', key: ''},{label: 'Fecha', key: 'date'}];
options['enableOrderBy'] = true;
options['orderByOptions'] = orderBy;
var customSearchControl = new google.search.CustomSearchControl('012159505712459906802:yacc1u8-pgq', options);
customSearchControl.setResultSetSize(10); //CONSTANTE DE 10 RESULTADOS, ACEPTA CUALQUIER OTRO ENTERO
customSearchControl.setLinkTarget(google.search.Search.LINK_TARGET_SELF);
jQuery("#searchsubmit").click(function(e){
    if (jQuery("#q").val()!=""){
        var drawOptions = new google.search.DrawOptions();
        drawOptions.enableSearchResultsOnly();
        customSearchControl.draw('cse', drawOptions);
        customSearchControl.setNoResultsString("Lo sentimos, no encontramos nada de lo que buscas en Unitec.");
        customSearchControl.execute(jQuery("#q").val());
    }


});

function openlineafx(obj){
 jQuery(obj).parent().parent().click();
  if( checkCookie('frmunitec') == true ) {

      jQuery('.nuevo-boton-regresar').attr('onclick','regresar();');
        
  } 

}
//JEAB FUNCIONALIDAD PARA EL BOTON DE REGRESAR
function regresar() {

    var elemento=jQuery('.cerrarExpandible').data("affected");
    jQuery('#'+elemento+'>.linea-elementos').children().not('.lineas-caption').hide();
    var arriba=jQuery('#'+elemento).data("ex-top");
    var derecha=jQuery('#'+elemento).data("ex-right");
    var abajo=jQuery('#'+elemento).data("ex-bottom");
    var izquierda=jQuery('#'+elemento).data("ex-left");
    var alto=jQuery('#'+elemento).data("ex-height");
    var ancho=jQuery('#'+elemento).data("ex-width");
    jQuery('#'+elemento).animate({"height": alto+"px","width":ancho,"top":arriba,"left":izquierda}, 500, function() {
      
      jQuery('#'+elemento).css({"position":"relative","z-index":"1","top":0});
      jQuery('.linea-elementos').css({ "width":"1%", "height":"1%"});
    
    });
    jQuery('.backMenu').fadeOut();
    jQuery('.unitec-logo').fadeIn();
    jQuery('.backMenu').removeClass('cerrarExpandible');


}
//JEAB FUNCIONALIDAD PARA EL BOTON DE IR A CAMPUS
function irCampus() {
  irA("sec-campus");
}
function irPromedio() {
  irA("sec-promedio");
}
function irCarrera() {
  irA("sec-cursar");
}
//TERMINA PARTE 3 DEL SIMULADOR DE COSTOS