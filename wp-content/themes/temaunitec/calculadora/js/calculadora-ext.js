/* ---------Cookies

  Metodos de cookies
  setCookie('frmunitec', "calculadora", 1);     //Setea cookie al hacer la peticion ajax a almacen1.php
  getCookie('lastActive')                       //Obtiene valor de cookie
  checkCookie('frmunitec')==true                //Valida si existe

  Valor de cookies
  loginface -> Se crea en login con facebook      val(1,0)
  frmunitec -> Se crea al llegar a resultados      frmunitec   setCookie('frmunitec', "calculadora", 1);


*/

  //Mensaje personalizado para error de validacion de correo
  var customErrorMessage = { emailValid: false ,email: 'Correo Inválido '};
//TERMINA PARTE 3 CALCULADORA
  jQuery(document).ready(function(e)
  {

    jQuery("#calcApp").data('regcompleto',0);   //Limpia valor de registro completado
    var nuevoCampus="";                         //variable en caso de cambio de campus
    var nuevaLinea="";                          //variable en caso de cambio de campus
    var isFormValid;                            //Referencia al resultado de validar el form
    initialization(); 
    console.log("despues de inicializar");                          //INICIALIZA FULLPAGE
    escala();                                   //Establece porcentaje de pixeles en escala con data-resize
    console.log("despues de escala");
    /*Valida si existe la variable code, y hace un login con facebook*/
    var code = getParameterByName('code');
    if(code){askFb();};
    console.log("despues de code");
    //Revisa si el usuario llego a resultados, sino se setea la cookie a 0 de loginface
    if(checkCookie('frmunitec')==false){
      setCookie( 'loginface', "0", 1 );
    }
    console.log("despues de loginface");
    //Revisa si existe la cookie de formulario completo, si existe carga sec-cursar y lo posiciona alli
    //Implementacion para cookie que viene de formulario
    console.log("antes del if cooki");
    if(checkCookie('frmunitec')==true || checkCookie('data_frm')==true){
      console.log("Antes de lineas de negocio");
        getLineasNegocio(); 
        jQuery.fn.fullpage.silentMoveTo(0, "sec-cursar");
    }
    console.log("dspues del if de cookie");

    //Implementacion cookie formulario: revisa si existe cookie de formulario 26-01-2017
    if(checkCookie('data_frm')==true){
        
        //Obtenemos los valores de la cookie
        var data_cookie = getCookie('data_frm');
        var obj_cookie = JSON.parse(data_cookie);
        setCookie('frmunitec',true);
        
        //Desktop
        if (jQuery(window).width() > 768) {
          jQuery.fn.fullpage.silentMoveTo(0, "sec-cursar");
          jQuery('#carga-modal').modal('show');
          getLineasNegocio();
          //Click a la linea de negocio que tiene del formulario
          setTimeout(function(){ 
          jQuery('#' + obj_cookie.linea).click(); 
          //Seleccion de carrera
          jQuery('.sel-carrera option[value="'+obj_cookie.categoria+'"]').attr("selected","selected");
          jQuery('.sel-carrera option[value="'+obj_cookie.categoria+'"]').change();
          //Seleccion de modalidad
          jQuery('.clcCarrerasMod-dsk[value="'+obj_cookie.modalidad+'"]').attr("checked",true);
          //getPagos(lineaNegocio, value, jQuery('#calcApp').data('beca'), jQuery('#calcApp').data('campus'));
          //ID COSTOS
          jQuery('#calcApp').data("idCosto",jQuery('.clcCarrerasMod-dsk[value="'+obj_cookie.modalidad+'"]').data("idcosto"));
          getCampus(obj_cookie.carreraInteres);
          getAnioslimite();
          jQuery('.nuevo-boton-ir-campus').click();


          }, 2000); 
          setTimeout(function(){ 
          //Seleccionar el campus
            jQuery('.clSelectCampus option[id="'+obj_cookie.idcampus+'"]').attr("selected","selected");
            jQuery('.clSelectCampus option[id="'+obj_cookie.idcampus+'"]').change();
            jQuery('#calcApp').data("decampus",obj_cookie.idcampus);
            jQuery('#calcApp').data("campus",obj_cookie.idcampus);
          //Ir a promedio
          jQuery.fn.fullpage.silentMoveTo(0, "sec-promedio");
           }, 2000); 
          
        //Mobile
        }else {
          //Linea
          /*jQuery('#carga-modal').modal('show');
          setTimeout(function(){
            jQuery('div[data-linea='+obj_cookie.linea+']').click(); 
          }, 1000);
          setTimeout(function(){
            jQuery('div[data-categoria='+obj_cookie.categoria+']').click();
          }, 3000);
          setTimeout(function(){
            jQuery('div[data-carreramod="'+obj_cookie.modalidad+'"]').click();
          }, 3000); 
          setTimeout(function(){
            jQuery('div[data-campus='+obj_cookie.idcampus+']').click();
          }, 3000); 
          setTimeout(function(){
            jQuery('div[data-campus="'+obj_cookie.idcampus+'"]').click();
          }, 3000); 
        }*/
      }
    }


    // Click en los botones de login
    jQuery('.fbbtn').on('touchstart click', function(){ askFb(); });
    jQuery('.formbtn').on('touchstart click', function(){ showForm(); });

    /***FUNCIONES MODAL*******************/
    /*
    Aplica para facebook.
    -Cuando cierre el modal del telefono:
      -Cuando el celular sea el deafult 55555555
        -Ejecuta datalayer Registro Calculadora>Registro con Facebook>Abandono en Solicitud de Telefono
    */
    jQuery('#telefono-modal').on('hidden.bs.modal', function () {
        if(jQuery('#frm_celular').val()=="5555555555" ){
          dataLayer.push({'event': 'ABANDONO_CLIC_CERRAR'});
          jQuery('#frm_celular').val('1234567890');// celular para CRM
          jQuery('#frm_banner ').val('CALCULADORA_FB');// celular para CRM
        }
    });

    /*LOG AL SALIR DE LA PAGINA*/
    jQuery(window).bind('beforeunload', function(){
        debugleave(jQuery('#frm_correo').val()+" | Abandona en: "+jQuery('.hz-slide.active').data('anchor')+"|");
    });

    /*URL PARAMETERS (NO FUNCIONANDO)*/
    if(getParameterByName("utm_campus")!=null){
        jQuery('#calcApp').data("campus",getParameterByName("utm_campus"));
    }



    /*****MENU********/
    jQuery( '.triggermenu' ).click(function(){
      menuExpanded = jQuery( '#menu' ).multilevelpushmenu( 'menuexpanded' , jQuery( '#menu' ).multilevelpushmenu( 'findmenusbytitle' , 'Menú' ).first() );
      if( menuExpanded ){
        jQuery( '#menu' ).multilevelpushmenu( 'collapse' );
      }else {
        jQuery( '#menu' ).multilevelpushmenu( 'expand' );
      }
    });

    //****CIERRE DE MENU****//
    jQuery('body').on('click','.btn-close',function(e){
        if(e.target.className.match(/btn-close/)){
            mobileCollapse(jQuery(this).data('btn'));
        }
    });






    //****CLICK EN CLCLINEA (PREPA,LIC,ETC)****//
    jQuery('body').on('click','.clcLinea',function(e){
        //JEAB DISPARA EVENTO Y VALIDA EL CAMPUS
        //Validar que se hayan seleccionado datos necesarios
        //FUNCIONALIDAD PARA EL NUEVO SELECT DE LAS NUEVAS CARRERAS
        jQuery('#calcApp').removeData("crtcampus");
        if(!jQuery(this).hasClass("item-available")){
            jQuery('#error-modal2').modal('show');
            jQuery('.mod-campus').html(jQuery('#calcApp').data("decampus"));
            jQuery('.mod-linea').html(jQuery(this).data("linea"));
            nuevaLinea=jQuery(this).attr("id");
        }
        var campusSelected="";
        if(jQuery(window).width()>768){
            if (e.target !== this){ return;} /*solo afecta si el div es clickeado, no su contenido*/
        }
        jQuery('#secLineas').find(".item-selected").each(function(){jQuery(this).removeClass("item-selected");jQuery("#"+this.id+">h2").remove();
        });  //reinicia los selected itwems
        if(jQuery(this).hasClass("item-available")){
            if(jQuery(this).hasClass("linea-dsk")){
                sendDatalayer('calculadora/sec-categoriaclik','StepCalculator');
                //debuglog("CLICK animando linea de Negocio: "+this.id);
                animar(jQuery(this));
                jQuery('#'+this.id+'>.linea-elementos').append('<div class="col-md-7">\
                                                          <div class="row que-estudiar dark">\
                                                              ¿Qué quieres estudiar?\
                                                              <br>\
                                                              '+ campusSelected+'\
                                                              <div class="col-md-8"><select class="sel-carrera" onChange="setCarrera();return;" style="width: 100%;">\
                                                              </select></div>\
                                                          </div>\
                                                          <div class="row que-modalidad dark">\
                                                              ¿Qué modalidad te interesa?\
                                                              <br>\
                                                          </div>\
                                                          <div class="modstatus"></div>\
                                                          <div class="carreramod dark">\
                                                          </div>\
                                                          <div class="col-md-3 btn-back-sec-cursar"><span class="backMenu back-menu btn btn-primary cerrarExpandible nuevo-boton-regresar" id="new-button-back"><i class="fa fa-chevron-left"></i>&nbsp;Regresar</span></div>\
                                                          <div class="col-md-4 btn-back-sec-cursar" style="text-align:right;margin-left: 45px;"><span class="btn btn-primary nuevo-boton-ir-campus" id="new-button-back" onclick="irCampus();">Seleccionar campus&nbsp;<i class="fa fa-chevron-right"></i></span></div>\
                                                      </div>');

                //AGREGAR FUNCIONALIDAD PARA EL BOTON REGRESAR SI EXISTE COOKIE
                if( checkCookie('frmunitec') == true ) {
                    jQuery('#new-button-back').attr('onclick','regresar();');
                }
                jQuery('#calcApp').data("selectLinea",jQuery(this).data("linea"));
                //Linea en cookie por si refresca la página
                var linea_cookie = { 
                        "selectLinea": jQuery('#calcApp').data("selectLinea"),
                };
                setCookieSession("linea_cookie",JSON.stringify(linea_cookie));
                console.log("COOKIE DE LINEA DE NEGOCIO");
                console.log(getCookie("linea_cookie"));


                getCarreras();
                if(jQuery(this).find('.sel-carrera').size()==1){
                    jQuery(this).find('.sel-carrera').val(56).change();
                } 
            }else{ //hasClass("linea-dsk")
                jQuery('#calcApp').data("selectLinea", jQuery(this).data("linea"));
                //JEABJEABJEAB POR SI ES MOBILE
                    if (jQuery("#calcApp").data("selectLinea") == "LICENCIATURA" || jQuery("#calcApp").data("selectLinea") == "INGENIERIA" || jQuery("#calcApp").data("selectLinea") == "SALUD") {
                        
                             
                                            jQuery('#calcApp').data("selectLinea", jQuery(this).data("linea"));
                                        jQuery.fn.fullpage.moveTo(1, 'sec-preguntas');
                                        jQuery("#secPreguntas>.container>.row").html("");
                                        jQuery("#secPreguntas>.container>.row").append("<div id='pregunta_examen' style='margin: auto;padding: 10px;font-size: 25px;'>¿Realizaste examen para alguna universidad pública?</div>");
                                        jQuery("#secPreguntas>.container>.row").append('<div onclick="clcPreguntas(this);" value="si" id="examen_si" style="cursor:pointer;" class="item-available col-xs-12 col-md-8 clcSelector clcPreguntas"><span class="ancho-carrera">' + 'Si' + '</span><i class="fa fa-chevron-right pull-right"></i></div>');
                                        jQuery("#secPreguntas>.container>.row").append('<div onclick="clcPreguntas(this);" id="examen_no" value="no" style="cursor:pointer;" class="item-available col-xs-12 col-md-8 clcSelector clcPreguntas"><span class="ancho-carrera">' + 'No' + '</span><i class="fa fa-chevron-right pull-right"></i></div>');
                                        //orderElements(jQuery("#secPreguntas>.container>.row"));
                            
                            } else if (jQuery("#calcApp").data("selectLinea") == "PREPARATORIA") {
                                
                                    
                                                    jQuery('#calcApp').data("selectLinea", jQuery(this).data("linea"));
                                                jQuery.fn.fullpage.moveTo(1, 'sec-preguntas');
                                                jQuery("#secPreguntas>.container>.row").html("");
                                                jQuery("#secPreguntas>.container>.row").append("<div id='pregunta_tutor' style='margin: auto;padding: 10px;font-size: 25px;'>¿Eres padre o tutor?</div>");
                                                jQuery("#secPreguntas>.container>.row").append('<div onclick="clcPreguntasTutor(this);" value="si" id="tutor_si" style="cursor:pointer;" class="item-available col-xs-12 col-md-8 clcSelector clcPreguntasTutor"><span class="ancho-carrera">' + 'Si' + '</span><i class="fa fa-chevron-right pull-right"></i></div>');
                                                jQuery("#secPreguntas>.container>.row").append('<div onclick="clcPreguntasTutor(this);" id="tutor_no" value="no" style="cursor:pointer;" class="item-available col-xs-12 col-md-8 clcSelector clcPreguntasTutor"><span class="ancho-carrera">' + 'No' + '</span><i class="fa fa-chevron-right pull-right"></i></div>');
                                                //orderElements(jQuery("#secPreguntas>.container>.row"));
                                    
                                    } else {

                jQuery(this).addClass('item-selected');
                jQuery(this).append("<h2 class='no-disponible hidden-xs hidden-sm'>Campus Seleccionado</h2>");
                jQuery('#calcApp').data("selectLinea",jQuery(this).data("linea"));
                //Linea en cookie por si refresca la página
                //Linea en cookie por si refresca la página
                var linea_cookie = { 
                        "selectLinea": jQuery('#calcApp').data("selectLinea"),
                };
                setCookieSession("linea_cookie",JSON.stringify(linea_cookie));
                console.log("COOKIE DE LINEA DE NEGOCIO");
                console.log(getCookie("linea_cookie"));

                var ccontCarrera=getCarreras();
                if(ccontCarrera==1){
                    jQuery('.clcCategoria').click();
                    e.preventDefault();
                    jQuery.fn.fullpage.silentMoveTo(0,'sec-modalidades');
                }else{
                    e.preventDefault();
                    jQuery.fn.fullpage.moveSlideRight();
                }
            }
        }//hasClass("item-available")
    } //Termina else por si es mobile

    //.on('click','.clcLinea'
    }).children().find(".linea-elementos").on('click', function (event) {event.stopPropagation();});

    //JEAB NUEVA VISTA PARA CAMPUS
    jQuery('body').on('click','.clcCampus',function(e){
    //JEAB DISPARA EVENTO Y VALIDA EL CAMPUS
    //FUNCIONALIDAD PARA EL NUEVO SELECT DE LAS NUEVAS CARRERAS
    //console.log(jQuery('#select-campus option:selected').attr('class'));
    jQuery('#calcApp').removeData("crtcampus");
    if(jQuery('#select-campus option:selected').attr("class") != 'item-available'){
        jQuery('#error-modal2').modal('show');
        jQuery('.mod-campus').html(jQuery('#calcApp').data("decampus"));
        jQuery('.mod-linea').html(jQuery(this).data("linea"));
        nuevaLinea=jQuery(this).attr("id");
    }
        var campusSelected="";
        if(jQuery(window).width()>768){
            if (e.target !== this){ return;} /*solo afecta si el div es clickeado, no su contenido*/
        }
        jQuery('#secLineas').find(".item-selected").each(function(){jQuery(this).removeClass("item-selected");jQuery("#"+this.id+">h2").remove();});  //reinicia los selected itwems
        if(jQuery(this).hasClass("item-available")){
            if(jQuery(this).hasClass("linea-dsk")){

              sendDatalayer('calculadora/sec-categoriaclik','StepCalculator');

                //JEAB REVISAR 29-04-2016
                //JEAB FUNCION DESPUES DE SELECCIONAR LINEA DE NEGOCIO
                animar(jQuery(this));
                if(jQuery('#calcApp').data('decampus')){
                    campusSelected="Campus seleccionado: <span class='camp-sel'>"+jQuery('#calcApp').data('decampus')+"</span>";
                }
                //JEAB AGREGAR FUNCIONALIDAD PARA EL BOTON REGRESAR SI EXISTE COOKIE
                if( checkCookie('frmunitec') == true ) {
                    jQuery('#new-button-back').attr('onclick','regresar();');
                }
                jQuery('#calcApp').data("selectLinea",jQuery(this).data("linea"));
                getCarreras();
                if(jQuery(this).find('.sel-carrera').size()==1){
                    jQuery(this).find('.sel-carrera').val(56).change();
                }
            //hasClass("linea-dsk")
            }else{
                jQuery(this).addClass('item-selected');
                jQuery(this).append("<h2 class='no-disponible hidden-xs hidden-sm'>Campus Seleccionado</h2>");
                jQuery('#calcApp').data("selectLinea",jQuery(this).data("linea"));
                var ccontCarrera=getCarreras();
                if(ccontCarrera==1){
                    jQuery('.clcCategoria').click();
                    e.preventDefault();
                    jQuery.fn.fullpage.silentMoveTo(0,'sec-modalidades');
                }else{
                    e.preventDefault();
                    jQuery.fn.fullpage.moveSlideRight();
                }
            }
        }//hasClass("item-available")


});//jQuery(document).ready(function(e)

//****CAMBIO DE ESQUEMA A DIFERENTES AÑOS ****//
jQuery('body').on("click",".btn-esquema",function(){

    var mostrar= jQuery(this).data("esquema");
    getDuracion(mostrar);
    jQuery('.toggle').toggles(false);
    jQuery('div[class*="termino-"]').each(function(){

        if(jQuery(this).attr('id') != 'terminos-condiciones') {
          jQuery(this).hide();
        }

    }); //limpialos

    jQuery('.termino-'+mostrar+':not(".p-contado")').fadeIn();
    jQuery('#calcApp').data('termino',mostrar);
    jQuery('#calcApp').data('materias',jQuery('.mat-calc-'+mostrar).html());
    jQuery('.toggle-off').html('<span style="margin-left:-10px;">No</span>');
    jQuery('.toggle-on').html('<span>Si</span>');
    //CAMBIAR PERIODOS EN QUE TERMINA LA CARRERA EN MOBILE JEAB 09-05-2016
    if( jQuery(window).width() < 768 ){
          jQuery('.anios-carrera').addClass('anios-carrera-mobile');
    }else {
          jQuery('.anios-carrera').removeClass('anios-carrera-mobile');
    }
    jQuery('#terminos-condiciones').show();
    jQuery('#ex14').slider("setValue",parseInt(jQuery('.mat-calc-'+mostrar).html()));
    jQuery('#terminos-condiciones').show();

}); //jQuery('body').on("click",".btn-esquema"

//****CLICK EN LOS BOTONES DE MENOS PROMEDIO ****//
jQuery('body').on("click",".control-menos",function(){

  jQuery('.control-mas').fadeIn();
  var sliderp=jQuery(this).data("slider");
  var valor= jQuery('#'+sliderp).slider("getValue");
  var nuevo = valor-0.1;
  if(nuevo<=6 ){
    jQuery('.control-menos').fadeOut();
  }
  if(nuevo>=6 ){
      jQuery('.promedio-val').html(nuevo.toFixed(1));
      jQuery('#'+sliderp).slider("setValue",nuevo);
  }else{
      jQuery('.control-menos').fadeOut();
  }

}); //jQuery('body').on("click",".control-menos"

//****CLICK EN LOS BOTONES DE MAS PROMEDIO****//
jQuery('body').on("click",".control-mas",function(){

    jQuery('.control-menos').fadeIn();
    var sliderp=jQuery(this).data("slider");
    var valor= jQuery('#'+sliderp).slider("getValue");
    var nuevo = valor+0.1;
    if(nuevo==10){
      jQuery('.control-mas').fadeOut();
    }
    if(nuevo<=10){
      jQuery('.promedio-val').html( nuevo.toFixed(1) );
      jQuery('#'+sliderp).slider("setValue",nuevo);
    }

}); //jQuery('body').on("click",".control-mas"

//****CLICK EN LOS BOTONES DE MENOS MATERIAS ****//
jQuery('body').on("click",".control-menos-mat",function(){

    jQuery('.control-mas-mat').fadeIn();
    var sliderp=jQuery(this).data("slider");
    var valor= jQuery('#'+sliderp).slider("getValue");
    var nuevo = valor-1;
    //console.log(jQuery('#calcApp').data('idCosto') +nuevo);
    if(jQuery('#calcApp').data('idCosto')=="DENT" && nuevo<4){
      //console.log("sale");
      return false
    }
    if(nuevo<=1 ){
      jQuery('.control-menos-mat').fadeOut();
    }
    if(nuevo>0 ){
      jQuery('.materias-val').html(nuevo);
      jQuery('#'+sliderp).slider("setValue",nuevo);
    }else{
      jQuery('.control-menos-mat').fadeOut();
    }

}); //jQuery('body').on("click",".control-menos-mat"

//****CLICK EN LOS BOTONES DE MAS MATERIAS ****//
jQuery('body').on("click",".control-mas-mat",function(){
    jQuery('.control-menos-mat').fadeIn();
    jQuery(".btn-esquema").removeClass('btn-primary');
    var sliderp=jQuery(this).data("slider");
    var nuevo= parseInt(jQuery('#'+sliderp).slider("getValue"))+1;
    if(nuevo>=jQuery('#calcApp').data('maxmaterias')){
      jQuery('.control-mas-mat').fadeOut();
    }
    if(nuevo<=jQuery('#calcApp').data('maxmaterias')){
      jQuery('.materias-val').html(nuevo);
      jQuery('.btn-esquema').each(function(){
        if(jQuery('#calcApp').data("mat_"+jQuery(this).data('esquema')+"_"+nuevo)){}
      });
      jQuery('#'+sliderp).slider("setValue",nuevo);
    }
    getDuracion();
});

//****CLICK EN LOS BOTONES DE ACEPTAR PROMEDIO Y MATERIAS  ****//
jQuery('body').on("click","#aceptar", function (e) {

    jQuery('#calcApp').data("promedio",jQuery("#ex13").slider('getValue'));
    jQuery('#calcApp').data("materias",jQuery("#ex14").slider('getValue'));
    getBecas();
    getDuracion();
    jQuery.fn.fullpage.moveTo(0,'resultados');
});


//*******FUNCIONALIDAD DE SLIDERS********//
jQuery("#ex13").slider({ticks: [6,7, 8, 9, 10],ticks_labels: ['6','7', '8', '9', '10'],step:0.1});
jQuery("#ex14").slider({ticks: [1,2, 3, 4,5],ticks_labels: ['1','2', '3', '4', '5'],step:1});
jQuery("body").on("change","#ex13", function (e) {  jQuery('.promctrl').fadeIn(); jQuery('.promedio-val').html(jQuery("#ex13").slider('getValue')); });
jQuery("body").on("change","#ex14", function (e) {  jQuery('.matctrl').fadeIn();  jQuery('.materias-val').html(jQuery("#ex14").slider('getValue')); });
jQuery("body").on("slideStop","#ex14", function (e) {
    if(parseInt(jQuery("#ex14").slider('getValue'))<5){jQuery('#calcApp').data('beca',0);}
    jQuery('#calcApp').data("materias",jQuery("#ex14").slider('getValue'));
    getBecas()
});
jQuery('body').on("slideStop","#ex13", function (e) {
    jQuery('#calcApp').data("promedio",jQuery("#ex13").slider('getValue'));
    getBecas();
    getDuracion();
});

//*******CLICK EN clcCategoria********//
jQuery('body').on("click",".clcCategoria",function(e) {
  jQuery('#calcApp').data("selectCategoria",jQuery(this).data("categoria"));
  jQuery('#calcApp').data("url",jQuery(this).find('span').html());
  var ccontMod= getModalidad("",jQuery(this).data("categoria"));
  if(ccontMod==1){
      e.preventDefault();
      jQuery.fn.fullpage.silentMoveTo(0, "sec-modalidades")
  }else{
      e.preventDefault();
      jQuery.fn.fullpage.moveTo(0, "sec-modalidades")
  }
});

//**************CLICK EN CLCCARRERAMOD MOBIL*******************//
jQuery(document).on("click",".clcCarrerasMod",function(e) {
    jQuery('#calcApp').data("selectMateria",jQuery(this).data("carreramod"));
    jQuery('#calcApp').data("idCosto",jQuery(this).data("idcosto"));
    var ccontCamp=getCampus(jQuery(this).data("carreramod").toString());
    getAnioslimite();//obtiene las materias minimas
    if(ccontCamp==1){
      e.preventDefault();
      jQuery.fn.fullpage.silentMoveTo(0, "sec-campus")
    }else{
      e.preventDefault();
      jQuery.fn.fullpage.moveTo(0, "sec-campus")
    }
     jQuery('#sec-modalidades').find(".item-selected").each(function(){jQuery(this).removeClass("item-selected");jQuery("#"+this.id+">h2").remove();});
    if(jQuery(this).hasClass("item-available")){jQuery(this).addClass('item-selected');        jQuery(this).append("<h2 class='no-disponible hidden-xs hidden-sm'>Campus Seleccionado</h2>");}
});

//**************CLICK EN CLCCARRERAMOD DESK*******************//
jQuery(document).on("change",".clcCarrerasMod-dsk ",function(e){
    sendDatalayer('calculadora/sec-modalidad','StepCalculator');
    jQuery('#calcApp').data("selectMateria",jQuery(this).data("carreramod"));
    jQuery('#calcApp').data("idCosto",jQuery(this).data("idcosto"));
    //debuglog('Escogio que quiere estudiar');
    //debuglog('Materia'+jQuery(this).data("carreramod"));
    //debuglog('idCosto'+jQuery(this).data("idcosto"));
    getCampus(jQuery(this).data("carreramod").toString());
    getAnioslimite();//obtiene las materias minimas
    e.preventDefault();
    jQuery.fn.fullpage.moveTo(0, "sec-campus");
});

/**********FUNCION FULL PAGE************/
function initialization(){
    console.log('INICIALIZANDO');

    jQuery('#fullpage').fullpage({
      anchors: ["calculadora","frm_mobile","sec-cursar","sec-categoria","sec-modalidades","sec-campus","sec-promedio","sec-materias","resultados"],
      menu: true,
      //Scrolling
      css3: false,
      scrollingSpeed: 700,
      autoScrolling: true, // navegador CON true  mobile con false
      fitToSection: false,
      scrollBar: false,
      easing: 'easeInOutCubic',
      easingcss3: 'ease',
      loopBottom: false,
      loopTop: false,
      loopHorizontal: true,
      continuousVertical: false,
      scrollOverflow: false,
      touchSensitivity: 15,
      normalScrollElementTouchThreshold: 5,
      //Accessibility
      keyboardScrolling: false,
      animateAnchor: true,
      recordHistory: true,
      //Design
      controlArrows: false,
      verticalCentered: false,
      resize: false,
      fixedElements: '#header, .footer',
      responsive: 0,
      //Custom selectors
      sectionSelector: '.section',
      slideSelector: '.hz-slide',
      normalScrollElements: '#SliderPRO, .slider',
      //events
      onLeave: function(index, nextIndex, direction) { },
      afterRender: function() {

        setCookie( 'lastActive','', 1 );
        setCookie( 'lastError','', 1 );
        setTimeout(function(){
           if(jQuery('.hz-slide.active').data('anchor')=='intro'){
            console.log('Datalayer: Paso0');
            sendDatalayer('calculadora/paso0','StepCalculator');
            setCookie( 'lastActive', jQuery('.hz-slide.active').data('anchor'), 1 );
          }
        },1000); 

        jQuery.fn.fullpage.silentMoveTo( jQuery('.hz-slide.active').data('anchor'));
        jQuery.fn.fullpage.setAllowScrolling(false, 'left, right');
      },
      afterResize: function() {
        jQuery.fn.fullpage.silentMoveTo('0', jQuery('.hz-slide.active').data('anchor'));
        jQuery.fn.fullpage.setAllowScrolling(false, 'left, right');
      },
      onSlideLeave:function(anchorLink, index, slideIndex, direction, nextSlideIndex){

         //console.log('Saliendo'+anchorLink+index+slideIndex+direction+nextSlideIndex);
      },
      afterSlideLoad: function(anchorLink, index, slideAnchor, slideIndex) {

        console.log("Cookie preserve_form_calc_values "+checkCookie('preserve_form_calc_values'));
        console.log(anchorLink+"___"+index+"___"+slideAnchor+"___"+slideIndex);
        if( ( checkCookie('preserve_form_calc_values')==false && checkCookie('data_frm')==false ) && slideAnchor!='intro'){
            
            console.log('entro aca');
            // if(jQuery('#calcApp').data("desk")){
            //     jQuery.fn.fullpage.silentMoveTo('0','');
            // }else{
               // console.log('regresando');
                jQuery.fn.fullpage.silentMoveTo('0','intro');
            // } 
            
        }else{ 
            console.log('entro aqui');
        }

        console.log('cargando..'+slideAnchor);
         //Se ejecuta el set timeout para que no le pegue cuando se hace resize o recarga la pagina y solo se envie la seccion activa
         if(slideAnchor=="sec-promedio"){
            


            

                switch(jQuery('#calcApp').data('selectLinea')) {
                    case "LICENCIATURA":
                        text = "preparatoria";
                        break;
                    case "POSGRADO":
                        text = "licenciatura";
                        break;
                    case "SALUD":
                        text = "preparatoria";
                        break;
                    case "INGENIERIA":
                        text = "preparatoria";
                        break;
                    case "PREPARATORIA":
                        text = "secundaria";
                        break;
                    default:
                        text = "grado de estudios anterior";
                }
                jQuery('.promedio_linea').text(text);
                console.log(text);

         }

         setTimeout(function(){
            if(slideAnchor=="sec-promedio" && checkCookie('data_frm')==true) {
              jQuery('#carga-modal').modal('hide');
            }
           console.log(getCookie('lastActive')+"-----"+jQuery('.hz-slide.active').data('anchor'));
           if(getCookie('lastActive')!=jQuery('.hz-slide.active').data('anchor')){

            if(slideAnchor=="sec-cursar"){
            jQuery('.fp-slidesContainer').scrollTop(0);
            jQuery('.home-fullscreen').removeClass("espacioForm");
            //Implementacion de cookie para que se renderizen las lineas de negocio si ya lleno el formulario en sesion  
            console.log("APLICA TODOS LOS DISPOSITIVOS");
            console.log(getCookie("preserve_form_calc_values"))

            if( getCookie("preserve_form_calc_values")!='' && 
              getCookie("preserve_form_calc_values") != null && 
              typeof getCookie("preserve_form_calc_values") != 'undefined') 

            {
              console.log(getCookie("preserve_form_calc_values"))
              console.log("Entra a inicializacion");
              init();

            }else {

              console.log("no entra a inicializacion");

            }



                //Implementacion banners de pablo
                  if(content_var != ''){ 
                      switch (resolucion_js) {
                          case "xs":
                              break;
                          case "sm":
                              break;
                          case "md":
                              jQuery("#contenedor-lineas-negocio").attr("style","margin-top: 0px!important");
                              var altura_bloques = jQuery( "#bloques-sec-cursar" ).height();
                              jQuery( "#bloques-sec-cursar" ).height(altura_bloques+47);
                              break;
                          case "lg":
                              jQuery("#contenedor-lineas-negocio").attr("style","margin-top: 0px!important");
                              var altura_bloques = jQuery( "#bloques-sec-cursar" ).height();
                              jQuery( "#bloques-sec-cursar" ).height(altura_bloques+50);
                              break;
                          default:
                          break;
                      }
                  }



                                jQuery('#menu-home-unitec').removeClass('transparentenav');

                                //alert(checkCookie('loginface'));
                                if(slideAnchor!="sec-intro" && getCookie('loginface')==1){
                                    console.log('entor732');
                                  if(checkCookie('regcompleto')!=1){
                                    //alert('ejecuta');
                                      jQuery('#telefono-modal').modal('hide')
                                     // jQuery.fn.fullpage.silentMoveTo(0, "intro");
                                  }
                                }
                                sendDatalayer('calculadora/sec-cursar','StepCalculator');
                                //JEABIX
                                if(checkCookie('frmunitec') != true){
                                    dataLayer.push({
                                      'event': 'IX_web_datoscorrectos'
                                    });
                                    dataLayer.push({
                                      'event': 'IX_web_oferta'
                                    });
                                }
                                if((jQuery('#frm_celular').val()=='5555555555' || jQuery('#frm_celular').val()=="") &&  checkCookie('frmunitec')!=true){
                                  if(checkCookie('frmunitec')!=true){jQuery('#telefono-modal').modal('show');}
                                }



}//Termina if sec_cursar




              //JEABIX
              if(slideAnchor=="sec-categoria"){
                if(checkCookie('frmunitec') != true){
                    dataLayer.push({
                      'event': 'IX_web_carrera'
                    });
                }
              }
              //JEABIX
              if(slideAnchor=="sec-modalidades"){
                if(checkCookie('frmunitec') != true){
                    dataLayer.push({
                      'event': 'IX_web_modalidad'
                    });
                }
              }
              if(slideAnchor=="sec-promedio"){
                  //ESBELTAS
                  getAnioslimite();
                  //JEABIX
                  sendDatalayer('calculadora/sec-promedio','StepCalculator');
                  if(checkCookie('frmunitec') != true){
                      dataLayer.push({
                        'event': 'IX_web_promedio'
                      });
                  }

              }
              if(slideAnchor=="sec-campus"){

                  sendDatalayer('calculadora/sec-campus','StepCalculator');
                  //JEABIX
                  if(checkCookie('frmunitec') != true){
                      dataLayer.push({
                        'event': 'IX_web_campus'
                      });
                  }
              }
              if(slideAnchor=="resultados"){

                sendDatalayer('calculadora/sec-Resultados','StepCalculator');
                //JEABIX
                if(checkCookie('frmunitec') != true){
                    dataLayer.push({
                      'event': 'IX_web_resultado'
                    });
                }

              }
              setCookie( 'lastActive', jQuery('.hz-slide.active').data('anchor'), 1 );
           }
         },100)

        //JEAB SI SOLO HAY UN CAMPUS MANDAMOS A SEC-PROMEDIO
        if(slideAnchor == 'sec-campus') {
              //Si es queretaro deshabilitamos la carrera de BM
              /*if(jQuery("#calcApp").data("selectMateria") == "723" || jQuery("#calcApp").data("selectMateria") == "1221"){
                jQuery("#QRO").attr("disabled","disabled");
                jQuery("#QRO").attr("class","");
              }*/
          jQuery.fn.fullpage.setAutoScrolling(false);
          if(getCookie('campusCounter') == 1) {

            if(checkCookie('data_frm')==true){
            jQuery('.clSelectCampus option[class="item-available"]').attr("selected","selected");
            jQuery('#calcApp').data("campus", jQuery('.clSelectCampus option[class="item-available item-selected"]').attr("id"));
            jQuery('#calcApp').data("decampus", jQuery('.clSelectCampus option[class="item-available item-selected"]').attr("id"));
            jQuery('.clSelectCampus option[class="item-available"]').change();
            //PAra una materia

          }else {
            var uncampus = jQuery("#select-campus option[class='item-available']").text();
            var uncampusdata = jQuery("#select-campus option[class='item-available']").data("campus");
            //jQuery("#select-campus option[class='item-available']").addClass('item-selected');
            jQuery("#select-campus option[value='"+jQuery("#select-campus option[class='item-available']").text()+"']").attr('selected','selected');
            //JEAB CURRENT
            setCampus(uncampus);
            jQuery('#calcApp').data("campus", uncampusdata);
            jQuery('#calcApp').data("decampus", uncampusdata);
            jQuery('.camp-sel').html(('decampus'));
            irA("sec-promedio");
          }
            //console.log(jQuery('#calcApp').data);
            
            //jQuery.fn.fullpage.silentMoveTo(0, "sec-promedio");
          }
        }

        //JEAB Validar si estamos en secciones posteriores para quitar el margen inferior a los bloques
        if( jQuery(window).width() > 768 ){

          if (slideAnchor == 'sec-cursar' || slideAnchor == 'resultados' || slideAnchor == 'sec-campus' || slideAnchor == 'sec-promedio') {

              //jQuery("#bloques-sec-cursar").addClass('margin-bottom-ln');
              jQuery("#p-nombre-carrera").addClass('p-bloques');
              jQuery("#p-campus").addClass('p-bloques');

              jQuery("#d-promedio").addClass('d-bloques');
              jQuery("#d-materias").addClass('d-bloques');

              jQuery("#boton-suma").addClass('b-botones');
              jQuery("#boton-resta").addClass('b-botones');

              jQuery("#boton-suma-materias").addClass('b-botones');
              jQuery("#boton-resta-materias").addClass('b-botones');

          }
        }

        scrollToTop();
        if(slideAnchor=="sec-cursar"){
          if(jQuery('#calcApp').data('selectLinea')){
            jQuery('#'+jQuery('#calcApp').data('selectLinea')).height(jQuery(window).height());
          }
          if(jQuery('.backMenu').hasClass('cerrarExpandible')){
            //JEAB EVITAR QUE SE QUITE EL LOGO DE NAVBAR
            //jQuery('.backMenu').fadeIn();
            //jQuery('.unitec-logo').fadeOut();
            jQuery('.nuevo-boton-regresar').fadeIn();
            //console.log('test');
            //jQuery('#new-button-back').removeAttr("style");
          }else{
            jQuery('.backMenu').fadeOut();
            jQuery('.unitec-logo').fadeIn();
          }

          //JEAB validacion para saber si lleno el fomulario
          /*if((jQuery('#frm_nombre').val() == '' || jQuery('#frm_apaterno').val() == '' || jQuery('#frm_amaterno').val() == '' || jQuery('#frm_correo').val() == '' || jQuery('#frm_celular').val() == '') && !getCookie('frmunitec')) {
             jQuery.fn.fullpage.silentMoveTo(0,0);
          }*/


        }else{
          jQuery('.backMenu').fadeOut();
          jQuery('.unitec-logo').fadeIn();
        }




        if(slideAnchor=="sec-campus"){
          if(jQuery(window).width()<768){
              /*if(jQuery("#calcApp").data("selectMateria") == "723" || jQuery("#calcApp").data("selectMateria") == "1221"){
                  jQuery('[data-campus="QRO"]').remove();
              }*/
            //Remover campus queretaro
          }else{


            //Implementación campaña pablo
            if((getParameterByName("utm_campaign")=='FB_HS_BECA' && getParameterByName("utm_campaign")=='GS_HS_BECA') && jQuery('#'+jQuery('#calcApp').data('selectLinea')).find('.clcCarrerasMod-dsk:checked').size()==0 && checkCookie('data_frm') != true){ 
                jQuery.fn.fullpage.moveTo(0, "sec-cursar"); 
                jQuery('.modstatus').html('Por favor selecciona una modalidad').css('color','red');
                }else{ jQuery('.modstatus').empty();}


            //Implementacion cookie formulario
                          if (jQuery('#' + jQuery('#calcApp').data('selectLinea')).find('.clcCarrerasMod-dsk:checked').size() == 0 && checkCookie('data_frm') != true) { jQuery.fn.fullpage.moveTo(0, "sec-cursar"); jQuery('.modstatus').html('Por favor selecciona una modalidad').css('color', 'red'); } else { jQuery('.modstatus').empty(); }
          }

        }





        if(slideAnchor=="resultados"){


          var textresult = "";
          switch(jQuery('#calcApp').data('selectLinea')) {
              case "LICENCIATURA":
                  textresult = " tu Licenciatura en:";
                  break;
              case "POSGRADO":
                  textresult = " tu Posgrado en:";
                  break;
              case "SALUD":
                  textresult = " tu Licenciatura en:";
                  break;
              case "INGENIERIA":
                  textresult = " tu Ingeniería en:";
                  break;
              case "PREPARATORIA":
                  textresult = " tu Preparatoria en:";
                  break;
              default:
                  textresult = " tus estudios en:";
          }
          jQuery('.anios-carrera').append(textresult);
          //console.log(text);

          if(checkCookie("data_frm")!= true) {
            jQuery(".col-recu").hide();
          }
          

          /* JEAB Para el boton de pago de colegiatura*/
          jQuery('.toggle-off').html('<span style="margin-left:-10px;">No</span>');
          jQuery('.toggle-on').html('<span>Si</span>');

          //JEAB eliminar años para terminar la carrera
          if( jQuery(window).width() < 768 ){

              jQuery('.anios-carrera').addClass('anios-carrera-mobile');

          }else {

            jQuery('.anios-carrera').removeClass('anios-carrera-mobile');

          }


            //COMIENZA CALCULA COSTOS
            var ke = jQuery('#calcApp').data('materias');
            console.log("ANTES DE EJECUTAR GETPAGOS EN RESULTADOS");
            if (jQuery('#calcApp').data('selectMateria') == 195 || jQuery('#calcApp').data('selectMateria') == 192) {
                var ke = 11;
            }
            if (jQuery('#calcApp').data('selectMateria') == 193 || jQuery('#calcApp').data('selectMateria') == 197 || jQuery('#calcApp').data('selectMateria') == 196) {
                var ke = 10;
            }
            console.log(jQuery('#calcApp').data('selectLinea'));
            console.log(ke);
            console.log(jQuery('#calcApp').data('beca'));
            console.log(jQuery('#calcApp').data('campus'));
            calculaCostos(jQuery('#calcApp').data('idCosto'), ke, jQuery('#calcApp').data('beca'), jQuery('#calcApp').data('campus'));
            //TERMINA CALCULA COSTOS


        }

        if(slideAnchor=='resultados' && jQuery('#calcApp').data('selectMateria')>0){
          //debuglog("Mostrando resultado al usuario");
          if(checkCookie('frmunitec')==false){envioAspirantes();}    
        }
        //Envio de recurrente
        if(slideAnchor=='resultados')
        {
        //   if( (getCookie('data_frm_sesion')==null || getCookie('data_frm_sesion')=="") && checkCookie('data_frm')==true){
        //     document.cookie = "data_frm_sesion=true; expires=0;";
        //     //Carrera mod
        //     //alert("Se envia al aspirante" + obj_cookie.modalidad);
        //     envioAspirantes();
        //   } 
        
        }
        /*Control de modalidades*/
        if(slideAnchor=='sec-modalidades' && jQuery('.clcCarrerasMod').size()==1){jQuery('.clcCarrerasMod').click();  /*jQuery.fn.fullpage.silentMoveTo(0, "sec-campus"); */}
        if(slideAnchor=='sec-campus' && jQuery('.clCampus').size()==1 && jQuery('#calcApp').data('crtcampus')!=1 ){
          jQuery('.clCampus').click();
          jQuery.fn.fullpage.silentMoveTo(0, "sec-promedio");
        }
        if(slideAnchor=='sec-campus' && jQuery('.clCampus.item-available').size()==1){
          var div=jQuery('.clCampus.item-available');
          jQuery(div).addClass('item-selected');
          jQuery(div).append("<h2 class='no-disponible hidden-xs hidden-sm'>Campus Seleccionado</h2>");
          jQuery('#calcApp').data("campus",jQuery(div).data("campus"));
          jQuery('#calcApp').data("decampus",validaCampus(jQuery(div).data("campus")));
        }
        if(getParameterByName("utm_campus")!=null && slideAnchor=='sec-campus'){
          setTimeout(function(){ jQuery('div[data-campus="'+getParameterByName("utm_campus")+'"]').click(); },500);
        }
        if(getParameterByName("utm_ln")!=null&&slideAnchor=="sec-cursar"){
          setTimeout(function(){ jQuery('div[data-linea="'+getParameterByName("utm_ln")+'"]').click(); },100);
          if(getParameterByName("utm_ln")=="PREPARATORIA"){
            setTimeout(function(){jQuery('.sel-carrera option:eq(1)').prop('selected', true).change();},200);
          }
        }
        if(slideAnchor=='sec-promedio'||slideAnchor=='sec-materias'){

          if(checkCookie('data_frm')==true){
      if( jQuery(window).width() > 768 ){
            jQuery('#calcApp').data("decampus",obj_cookie.idcampus);
            jQuery('#calcApp').data("campus",obj_cookie.idcampus);
            jQuery('#calcApp').data("campus", jQuery('.clSelectCampus option[class="item-available item-selected"]').attr("id"));
            jQuery('#calcApp').data("decampus", jQuery('.clSelectCampus option[class="item-available item-selected"]').attr("id"));
      }
            var campusForm;
               switch (obj_cookie.idcampus) {
        case "ATZ":
            campusForm= "ATIZAPAN";
            break;
        case "MAR":
            campusForm= "MARINA";
            break;
        case "ECA":
            campusForm= "ECATEPEC";
            break;
        case "SUR":
            campusForm= "SUR";
            break;
        case "CUI":
            campusForm= "CUITLAHUAC";
            break;
        case "LEO":
            campusForm= "LEON";
            break;
        case "REY":
            campusForm = "LOS REYES";
            break;
        case "TOL":
            campusForm= "TOLUCA";
            break;
        case "GDL":
            campusForm= "GUADALAJARA";
            break;
        case "QRO":
            campusForm= "QUERETARO";
            break;
        case "ONL":
            campusForm= "ONLINE";
    }
            setCampus(jQuery("#select-campus option[class='item-available item-selected']").text());
          }
          if(jQuery('#calcApp').data('campus')==null ){

              //mostrarAlerta("Debes elegir un campus ",1);
              init();
              irA("sec-cursar");
          }
        }
        if(slideAnchor=='sec-categoria' && jQuery('.clcCategoria').size()==1){
          jQuery('.clcCategoria').click();
          jQuery.fn.fullpage.silentMoveTo(0, "sec-modalidades");
        }
        //Si en categoria no hay linea regresar a seleccionar linea
        if(slideAnchor=='sec-categoria' && (jQuery('#calcApp').data("selectLinea")=="" || typeof jQuery('#calcApp').data("selectLinea") == "undefined")){
          console.log("Entro en validacion de sec-cursar");
          init();
          irA("sec-cursar");
        }
        //
        if(slideAnchor=='sec-modalidades' && (jQuery('#calcApp').data("selectCategoria")=="" || typeof jQuery('#calcApp').data("selectCategoria") == "undefined")){
          console.log("Entro en validacion de sec-modalidades");
          init();
          irA("sec-cursar");
        }
        if ( (slideAnchor=='sec-campus' && (jQuery('#calcApp').data("selectMateria")=="" || typeof jQuery('#calcApp').data("selectMateria") == "undefined") ) && checkCookie("data_frm") != true   ){
          console.log("Entro en validacion de sec-campus");
          init();
          irA("sec-cursar");
        }

        jQuery.fn.fullpage.setAllowScrolling(false, 'left, right');
        if (slideIndex > 1) {
          jQuery('.color-control').fadeIn();

          if(slideAnchor=='sec-cursar'){
          jQuery('.color-control>div').removeClass("naranja");
          jQuery('.color-control>div').addClass("azul");
          jQuery('.color-control>div').eq(0).addClass("naranja");
          //JEAB Eliminamos clase
          jQuery('#i-cambiar-carrera').removeClass('show-fa');
          jQuery('#i-cambiar-carrera').addClass('hide-fa');
          jQuery('#i-cambiar-campus').removeClass('show-fa');
          jQuery('#i-cambiar-campus').addClass('hide-fa');

          }

          if (slideAnchor == 'sec-campus') {//si la seccion es campus
          jQuery('.color-control>div').removeClass("naranja");
          jQuery('.color-control>div').addClass("azul");
          jQuery('.color-control>div').eq(1).addClass("naranja");
          //JEAB Añadimos indicador de que es clickeable
          jQuery('#i-cambiar-carrera').removeClass('hide-fa');
          jQuery('#i-cambiar-carrera').addClass('show-fa');
          jQuery('#i-cambiar-campus').removeClass('show-fa');
          jQuery('#i-cambiar-campus').addClass('hide-fa');

          }
          if (slideAnchor == 'sec-promedio') {//si la seccion es promedio
          jQuery('.color-control>div').removeClass("naranja");
          jQuery('.color-control>div').addClass("azul");
          jQuery('.color-control>div').eq(2).addClass("naranja");
          //JEAB Añadimos indicador de que es clickeable
          jQuery('#i-cambiar-campus').removeClass('hide-fa');
          jQuery('#i-cambiar-campus').addClass('show-fa');
          }
          if (slideAnchor =='sec-materias') {//si la seccion es materias
          jQuery('.color-control>div').removeClass("naranja");
          jQuery('.color-control>div').addClass("azul");
          jQuery('.color-control>div').eq(3).addClass("naranja");
          }
        } else {
          jQuery('.color-control').fadeOut();
        }
      }//TERMINA AFTERSLIDELOAD

    }); //jQuery('#fullpage').fullpage

} //TERMINA FUNCION initialization

jQuery('.moveSlideRight').click(function(e) { e.preventDefault(); jQuery.fn.fullpage.moveTo(1, 1); });

jQuery('body').on("click",".paso3",function(e){
    init();
    e.preventDefault();
    jQuery.fn.fullpage.moveTo(1, 2);
});

/***********CONTROL DE BOTONES TODO OPTIMIZAR*************/
jQuery('.btn-campus').click( function(e) {

  //JEAB VALIDAR QUE NO SE EJECUTE LA ACCION SI NO TIENE DATOS NECESARIOS
  if( jQuery(window).width() < 768) {

        var linea = jQuery('#calcApp').data().selectLinea;
        var url = jQuery('#calcApp').data().url;
        var idCosto = jQuery('#calcApp').data().idCosto;

        if(typeof linea != 'undefined' &&  typeof url != 'undefined' && typeof idCosto != 'undefined') {

          jQuery('#calcApp').data("crtcampus", 1);
          e.preventDefault();
          jQuery('.color-control>div').removeClass("naranja");
          jQuery('.color-control>div').addClass("azul");
          //jQuery.fn.fullpage.moveTo(0, "sec-campus");
            irCampus();
          selectColor(this);

        }

  }else {

      jQuery('#calcApp').data("crtcampus", 1);
      e.preventDefault();
      jQuery('.color-control>div').removeClass("naranja");
      jQuery('.color-control>div').addClass("azul");
      jQuery.fn.fullpage.moveTo(0, "sec-campus");
      selectColor(this);
  }

});


jQuery('body').on("click",'.btn-cursar',function(e) {

    if(jQuery(window).width()<768){
      reiniciarAppData();
      jQuery('#calcApp').removeData('campus')  ;
    }
    e.preventDefault();
    jQuery('.color-control>div').removeClass("naranja");
    jQuery('.color-control>div').addClass("azul");
    jQuery.fn.fullpage.moveTo(0, "sec-cursar");
    selectColor(this);
    jQuery("#"+jQuery('.cerrarExpandible').data("affected")).css({"height":jQuery(window).height()+"px"});

});


jQuery('.btn-promedio').click(function(e) {

    if( jQuery(window).width() < 768 ) {

        //VALIDAR QUE NO SE EJECUTE LA ACCION SI NO TIENE DATOS NECESARIOS
        var linea = jQuery('#calcApp').data().selectLinea;
        var url = jQuery('#calcApp').data().url;
        var idCosto = jQuery('#calcApp').data().idCosto;
        var campus = jQuery('#calcApp').data().campus;
        var promedio = jQuery('#calcApp').data().promedio;
        if(typeof linea != 'undefined' &&  typeof url != 'undefined' && typeof idCosto != 'undefined' && typeof campus != 'undefined' && typeof promedio != 'undefined') {

            mobileExpand('btn-promedio');

        }

    }

    if(e.target.className.match(/control-menos/)){
      //JEAB BOTONES PROMEDIO MOBILE
      jQuery('.control-mas').css({'visibility':'visible'});
      var sliderp="ex13";
      var valor= jQuery('#'+sliderp).slider("getValue");
      var nuevo = valor-0.1;
      if(nuevo<=6 ){jQuery('.control-menos').css({'visibility':'hidden'});}
      if(nuevo>=6 ){
        jQuery('.promedio-val').html(nuevo.toFixed(1));
        jQuery('#'+sliderp).slider("setValue",nuevo);
      }else{
        jQuery('.control-menos').css({'visibility':'hidden'});
      }
      jQuery('#calcApp').data("promedio",jQuery("#ex13").slider('getValue'));
      jQuery('#calcApp').data("materias",jQuery("#ex14").slider('getValue'));
      getBecas();
      getDuracion();
      //JEAB Agregar texto al tipo de pago
      jQuery('.toggle-off').html('<span style="margin-left:-10px;">No</span>');
      jQuery('.toggle-on').html('<span>Si</span>');
       //JEAB eliminar años para terminar la carrera
      if( jQuery(window).width() < 768 ){

            jQuery('.anios-carrera').addClass('anios-carrera-mobile');

      }else {

            jQuery('.anios-carrera').removeClass('anios-carrera-mobile');

      }
      return false;
    }  //if(e.target.className.match(/control-menos/)

    if(e.target.className.match(/control-mas/)){
      jQuery('.control-menos').css({'visibility':'visible'});
      var sliderp="ex13";
      var valor= jQuery('#'+sliderp).slider("getValue");
      var nuevo = valor+0.1;
      if(nuevo==10){jQuery('.control-mas').css({'visibility':'hidden'});}
      if(nuevo<=10){
        jQuery('.promedio-val').html( nuevo.toFixed(1) );
        jQuery('#'+sliderp).slider("setValue",nuevo);
      }
      jQuery('#calcApp').data("promedio",jQuery("#ex13").slider('getValue'));
      jQuery('#calcApp').data("materias",jQuery("#ex14").slider('getValue'));
      getBecas();
      getDuracion();
      //JEAB Agregar texto al tipo de pago
      jQuery('.toggle-off').html('<span style="margin-left:-10px;">No</span>');
      jQuery('.toggle-on').html('<span>Si</span>');
      //JEAB eliminar años para terminar la carrera
      if( jQuery(window).width() < 768 ){

            jQuery('.anios-carrera').addClass('anios-carrera-mobile');

      }else {

            jQuery('.anios-carrera').removeClass('anios-carrera-mobile');

      }
      return false;

    }//if(e.target.className.match(/control-mas/))


}); //jQuery('.btn-promedio').click


jQuery('.btn-materias').click(function(e) {

    if( jQuery(window).width() < 768 ) {

        //VALIDAR QUE NO SE EJECUTE LA ACCION SI NO TIENE DATOS NECESARIOS
        var linea = jQuery('#calcApp').data().selectLinea;
        var url = jQuery('#calcApp').data().url;
        var idCosto = jQuery('#calcApp').data().idCosto;
        var campus = jQuery('#calcApp').data().campus;
        var promedio = jQuery('#calcApp').data().promedio;
        if(typeof linea != 'undefined' &&  typeof url != 'undefined' && typeof idCosto != 'undefined' && typeof campus != 'undefined' && typeof promedio != 'undefined') {

            mobileExpand('btn-materias');
        }

    }

    if(e.target.className.match(/control-menos-mat/)){

      jQuery('.control-mas-mat').css({'visibility':'visible'});
      var sliderp="ex14";
      var valor= jQuery('#'+sliderp).slider("getValue");
      var nuevo = valor-1;
      if(jQuery('#calcApp').data('idCosto')=="DENT" && nuevo<4){
        jQuery('.control-menos-mat').css({'visibility':'hidden'});
        return false
      }
      if(nuevo<=1 ){jQuery('.control-menos-mat').css({'visibility':'hidden'});}
      if(nuevo>0 ){
       jQuery('.materias-val').html(nuevo);
       jQuery('#'+sliderp).slider("setValue",nuevo);
      }else{
        jQuery('.control-menos-mat').css({'visibility':'hidden'});
      }
      jQuery('#calcApp').data("promedio",jQuery("#ex13").slider('getValue'));
      jQuery('#calcApp').data("materias",jQuery("#ex14").slider('getValue'));
      getBecas();
      getDuracion();
      // Agregar texto al tipo de pago
      jQuery('.toggle-off').html('<span style="margin-left:-10px;">No</span>');
      jQuery('.toggle-on').html('<span>Si</span>');
      // eliminar años para terminar la carrera
      if( jQuery(window).width() < 768 ){
          jQuery('.anios-carrera').addClass('anios-carrera-mobile');
      }else {
          jQuery('.anios-carrera').removeClass('anios-carrera-mobile');
      }
      return false;
    }//if(e.target.className.match(/control-menos-mat/))

    if(e.target.className.match(/control-mas-mat/)){

     jQuery('.control-menos-mat').css({'visibility':'visible'});
     var sliderp="ex14";
     var nuevo= parseInt(jQuery('#'+sliderp).slider("getValue"))+1;
     if(nuevo>=jQuery('#calcApp').data('maxmaterias')){
      jQuery('.control-mas-mat').css({'visibility':'hidden'});}
      if(nuevo<=jQuery('#calcApp').data('maxmaterias')){
        jQuery('.materias-val').html(nuevo);
        jQuery('#'+sliderp).slider("setValue",nuevo);
      }
      jQuery('#calcApp').data("promedio",jQuery("#ex13").slider('getValue'));
      jQuery('#calcApp').data("materias",jQuery("#ex14").slider('getValue'));
      getBecas();
      getDuracion();
      // Agregar texto al tipo de pago
      jQuery('.toggle-off').html('<span style="margin-left:-10px;">No</span>');
      jQuery('.toggle-on').html('<span>Si</span>');
      // eliminar años para terminar la carrera
      if( jQuery(window).width() < 768 ){
          jQuery('.anios-carrera').addClass('anios-carrera-mobile');
      }else {
          jQuery('.anios-carrera').removeClass('anios-carrera-mobile');
      }
      return false;

    } //if(e.target.className.match(/control-mas-mat/))

}); //jQuery('.btn-materias').click(function(e)

/*************FIN DE CONTROL DE BOTONES*********************/

jQuery('#control-input').on("change paste keyup", function() {watchDogs(); });

jQuery('body').on('click','.clCampus',function(e){

    jQuery('#secCampus').find(".item-selected").each(function(){
      jQuery(this).removeClass("item-selected");
      jQuery("#"+this.id+">h2").remove();
    });

    if(jQuery(this).hasClass("item-available")){
      jQuery(this).addClass('item-selected');
      jQuery(this).append("<h2 class='no-disponible hidden-xs hidden-sm'>Campus Seleccionado</h2>");
      
      jQuery('#calcApp').data("campus",jQuery(this).data("campus"));

      jQuery('#calcApp').data("decampus",validaCampus(jQuery(this).data("campus")));
      jQuery('.camp-sel').html(jQuery('#calcApp').data('decampus'));
      irA("sec-promedio");
    }else{
      jQuery('#calcApp').data("nuevoCampus",jQuery(this).data("campus"));
      jQuery('#calcApp').data("decampus",jQuery(this).find('.lineas-caption').html());
      jQuery('#error-modal').modal('show');
      jQuery('.mod-campus').html(jQuery('#calcApp').data("decampus"));
      jQuery('.mod-carrera').html(jQuery('#calcApp').data("selectLinea").toLowerCase()+" "+jQuery('#calcApp').data("deMateria"));
      nuevoCampus=jQuery(this).data("campus");
    }

}); //jQuery('body').on('click','.clCampus'

/*Para la nueva pantalla de seleccionar campus*/

jQuery('body').on('change','.clSelectCampus',function(e){

  jQuery("#select-campus").find(".item-selected").each(function(){
    jQuery(this).removeClass("item-selected");
  });

  if(jQuery("#select-campus :selected").attr("class") == 'item-available'){

      jQuery("#select-campus :selected").addClass('item-selected');
      jQuery('#calcApp').data("campus",jQuery("#select-campus :selected").data("campus"));
      jQuery('#calcApp').data("decampus",validaCampus(jQuery("#select-campus :selected").data("campus")));
      jQuery('.camp-sel').html(jQuery('#calcApp').data('decampus'));
      irA("sec-promedio");

    }else{

      jQuery('#calcApp').data("nuevoCampus",jQuery(this).data("campus"));
      jQuery('#calcApp').data("decampus",jQuery(this).find('.lineas-caption').html());
      jQuery('#error-modal').modal('show');
      jQuery('.mod-campus').html(jQuery('#calcApp').data("decampus"));
      jQuery('.mod-carrera').html(jQuery('#calcApp').data("selectLinea").toLowerCase()+" "+jQuery('#calcApp').data("deMateria"));
      nuevoCampus=jQuery(this).data("campus");

    }

}); //jQuery('body').on('change','.clSelectCampus'


jQuery('body').on('click','.campus-modal-cambiar',function(){

   if(jQuery(this).hasClass("mod-cambia-campus")){
    jQuery('#calcApp').removeData("campus");
    jQuery('#calcApp').data("campus",jQuery('#calcApp').data("nuevoCampus"));
    jQuery('#error-modal2').modal("hide");
    reiniciarAppData();
    getLineasNegocio();
    setTimeout(function(){ jQuery('#'+nuevaLinea).click();},100)
  }else{
    jQuery('#error-modal').modal("hide");
    jQuery('#calcApp').data("campus",nuevoCampus);
    getLineasNegocio({campus: jQuery("#calcApp").data("campus")});
    setTimeout(function(){jQuery.fn.fullpage.moveTo(0, "sec-cursar")},100);}

}); //jQuery('body').on('click','.campus-modal-cambiar'

jQuery('body').on("change",".sel-carrera",function(e) {


    if(jQuery(this).val() == "Selecciona") {
      jQuery(".que-modalidad").hide();
    }else {
      jQuery(".que-modalidad").show();
    }

    jQuery('#calcApp').data("selectCategoria",jQuery(this).val());
    jQuery('#calcApp').data("deMateria", jQuery(" option:selected" ,this).text());
    //debuglog("Escogio materia: "+jQuery('#calcApp').data("deMateria"));
    jQuery('#calcApp').removeData("campus");
    getModalidad("",jQuery(this).val());

}); //jQuery('body').on("change",".sel-carrera"


//Boton para expander las lineas de negocio en la calculadora
jQuery('body').on('click','.cerrarExpandible',function(){

    var elemento=jQuery('.cerrarExpandible').data("affected");
    jQuery('#'+elemento+'>.linea-elementos').children().not('.lineas-caption').hide();
    var arriba=jQuery('#'+elemento).data("ex-top");
    var derecha=jQuery('#'+elemento).data("ex-right");
    var abajo=jQuery('#'+elemento).data("ex-bottom");
    var izquierda=jQuery('#'+elemento).data("ex-left");
    var alto=jQuery('#'+elemento).data("ex-height");
    var ancho=jQuery('#'+elemento).data("ex-width");
    jQuery('#'+elemento).animate({"height": alto+"px","width":ancho,"top":arriba,"left":izquierda}, 500,function(){
      jQuery('#'+elemento).css({"position":"relative","z-index":"1","top":0});
      jQuery('.linea-elementos').css({ "width":"1%", "height":"1%"});
    });
    jQuery('.backMenu').fadeOut();
    jQuery('.unitec-logo').fadeIn();
    jQuery('.backMenu').removeClass('cerrarExpandible');

}); //jQuery('body').on('click','.cerrarExpandible'


jQuery('body').on('click','.clcCategoria',function(){
    jQuery('#secCategorias').find(".item-selected").each(function(){
      jQuery(this).removeClass("item-selected");
      jQuery("#"+this.id+">h2").remove();
    });
    if(jQuery(this).hasClass("item-available")){jQuery(this).addClass('item-selected');jQuery(this).append("<h2 class='no-disponible hidden-xs hidden-sm'>Campus Seleccionado</h2>");}

}); //jQuery('body').on('click','.clcCategoria'


jQuery('body').on('click', '.btn-esquema', function(){

    jQuery(this).removeClass('btn-default').addClass('btn-primary');
    jQuery(this).parent().siblings().find('button').removeClass('btn-primary').addClass('btn-default');

}); //jQuery('body').on('click', '.btn-esquema'


jQuery('body').on('click', '.clcSelector', function(){

    if( jQuery('#calcApp').data('selectLinea')){
      jQuery('.crtl>p>i:eq(0)').addClass('fa-check');
    }
    if( jQuery('#calcApp').data('campus')){
      jQuery('.crtl>p>i:eq(1)').addClass('fa-check');
    }

}); //jQuery('body').on('click', '.clcSelector'


jQuery('body').on('click', '#aceptar', function(){
    if( jQuery('#calcApp').data('promedio')){
      jQuery('.promctrl').fadeIn();
      jQuery('.promedio-val').html(jQuery('.promedio-val').eq(1).html());
      jQuery('.crtl>p>i:eq(2)').addClass('fa-check');
    }
    if( jQuery('#calcApp').data('materias')){
      jQuery('.matctrl').fadeIn();
      jQuery('.materias-val').html(jQuery('.materias-val').eq(1).html());
      jQuery('.crtl>p>i:eq(3)').addClass('fa-check');
    }

});//jQuery('body').on('click', '#aceptar'

});
//<![CDATA[
  jQuery(window).load(function() { // makes sure the whole site is loaded
      jQuery('#status').fadeOut(); // will first fade out the loading animation
      jQuery('#preloader').delay(350).fadeOut('slow'); // will fade out the white DIV that covers the website.
      jQuery('body').delay(350).css({'overflow':'visible'});
  })
//]]>

//TERMINA PARTE 1 CALCULADORA

//PARTE 2 CALCULADORA

jQuery(document).ready(function() {

  var patternVocals = /[aeiou]/i;
  var patternConsonants = /[bcdfghjklmnñpqrstvwxyz]/i;
  var patternFourConsonants = /[bcdfghjklmnñpqrstvwxyz]{5}|[bcdfghjklmnñpqrstvwxyz]{2}\s[bcdfghjklmnñpqrstvwxyz]{3}|[bcdfghjklmnñpqrstvwxyz]{3}\s[bcdfghjklmnñpqrstvwxyz]{2}/i;//4 consonantes consecutivas con o sin espacio, ejem: davidd ffranco, daviddffranco
  var patternFourVocals = /[aeiou]{5}|[aeiou]{2}\s[aeiou]{3}|[aeiou]{3}\s[aeiou]{2}/i;//4 vocales consecutivas con o sin espacio, ejem: francoo oomar, francoooomar
  var patternSpacePlusConsonants = /^\s[bcdfghjklmnñpqrstvwxyz]{2,}/i; //No iniciar con espacio seguido de 2 o mas consonantes
  var patternStartWithSpace = /^\s/; //No iniciar con espacio

  jQuery(window).on("resize load",function(){
    if(jQuery(window).width()>768){
      jQuery(".calcu-segunda-seccion").appendTo("#home-calc");
    }else{
    }
  });

  /* Método nombre basura */
  jQuery.validator.addMethod("basura", function(value, element) {
    var strBasura = '/*/content';
    var qryBasura = queryHandler(jsonBasuraContenido, strBasura);
    var arrBasuraContent = [];
    var array = jQuery.map(qryBasura, function(v, i) {return [v];});
      // Valida texto contenido en arrBasura
      for(i = 0; i < array.length; i++){
        var arrRes = array[i];
        arrBasuraContent.push(arrRes['value']);
      }
      for(i=0; i<arrBasuraContent.length; i++){
        var existe = value.toLowerCase().indexOf(arrBasuraContent[i].toLowerCase());
        if(existe != -1){return false;}
      }
      // Valida texto completo en arrBasura
      strBasura = '/*/completo';
      qryBasura = queryHandler(jsonBasuraContenido, strBasura);
      var arrBasuraCompleto = [];
      array = jQuery.map(qryBasura, function(v, i) {return [v];});
      // Valida exista en arrBasura
      for(i = 0; i < array.length; i++){var arrRes = array[i];arrBasuraCompleto.push(arrRes['value']);}
        if(jQuery.inArray(value,arrBasuraCompleto) != -1){return false;}
      // Valida que sean sólo letras
      var patron = /^[a-zA-Záíúéóñ\s]*$/;
      if(value.search(patron) == -1){return false;}
      return true;
    }, "Mensaje error");

jQuery.validator.addMethod("basura_completo", function(value, element) {
        var texto=jQuery.trim(value.toLowerCase());

        //console.log("VALUE/"+texto);

        var strBasura = '/*/completo';
        var qryBasura = queryHandler(jsonBasuraCompleto, strBasura);
        var arrBasuraComplete = [];
        var array = jQuery.map(qryBasura, function(texto, i) {
            return [texto];
        });


        // Valida texto contenido en arrBasura
        for(i = 0; i < array.length; i++){
            var arrRes = array[i];
            arrBasuraComplete.push(arrRes['value']);
            //console.log(arrRes['value']);
        }

        var repetido=arrBasuraComplete.indexOf(texto);
         //console.log(repetido);
        if(repetido!=-1){
           return false;
        }else{ return true;}



        // Valida que sean sólo letras
        var patron = /^[a-zA-Záíúéóñ\s]*$/;
        if(value.search(patron) == -1){
            //////console.log('invalido letras, v='+value);
            return false;
        }

        return true;
    });

   /* Método valida email */
    jQuery.validator.addMethod("validaEmail", function(value, element) {
        //Flag que cambiara cuando sea valido
        customErrorMessage.emailValid = false;
        customErrorMessage.email = 'Correo Inválido';
        //Separamos en 2 partes la direccion
        var arrEmail = value.split('@');

        //Esta vacio el nombre?
        if(arrEmail[0]===""){
            return false;
        }

        //No tiene arroba?
        if(arrEmail.length <= 1){
            return false;
        }
        //Longitud del nombre de usuario
        //es muy largo o muy corto el nombre?
        var lenUser = arrEmail[0].length;
        if(lenUser <= 2 || lenUser > 36){
            return false;
        }

        //Validamos que no existen caracteres raros en el nombre de usuario

        if(arrEmail[0].search(/[^a-zA-Z0-9_.-]/)!=-1){ //Tiene caracteres especiales?
            customErrorMessage.email = "Caracter inválido encontrado.";
            return false;
        }

        /**
        *   El formato de de email de hotmail valida lo siguiente en el nombre de usuario:
        *   PERMITIDO:
        *    - Caracteres permitidos de la A a la Z mayusculas o minusculas
        *    - Numeros del 0 al 9 permitidos al final o dentro del nombre de usuario,
        *    - Guion medio y guion bajo, dentro del usuario la cantidad que sea y consecutivos
        *    - Puntos no consecutivos dentro del usuario.
        *   NO PERMITIDO:
        *    - Números del 0 al 9 al inicio
        *    - Punto, guion medio y guion bajo al inicio
        *    - Punto al final
        *    - Caracteres espciales, incluyendo ñ y acentos
        */
        if(arrEmail[1].search(/(hotmail|outlook|live)/i)!= -1 && (arrEmail[0][lenUser-1].search(/\./)!=-1 || arrEmail[0][0].search(/(-|_|\.|[0-9])/)!=-1 || arrEmail[0].search(/\.{2}/g) != -1) ){
            return false;
        }

        /**
        *   El formato de de email de gmail valida lo siguiente en ll nombre de usuario:
        *   PERMITIDO:
        *   - Caracteres permitidos de la A a la Z mayusculas o minusculas
        *   - Numeros del 0 al 9 permitidos al final o dentro del nombre de usuario,
        *   - Punto dentro del nombre de usuario.
        *   NO PERMITIDO:
        *   - Más de un punto consecutivo.
        *   - Punto al inicio o al final.
        *   - Numeros del 0 al 9 al inicio.
        *   - Caracteres especiales incluyendo guion medio, guion bajo y eñe, acentos, etc., ni al inicio, intermedios o final.
        */
        if(arrEmail[1].search(/gmail/i)!= -1 && (arrEmail[0][lenUser-1].search(/[^a-zA-Z0-9]/)!=-1 || arrEmail[0][0].search(/(-|_|\.|[0-9])/)!=-1 || arrEmail[0].search(/\.{2}/g) != -1 || arrEmail[0].search(/(-|_)/g) != -1)){
            return false;
        }

        /**
            El formato de email de yahoo valida lo siguiente en ll nombre de usuario:
            - El usuario solo empieza con letras,
            - acaba con letra o numero solamente,
            - no acepta guion medio,
            - no acepta guion bajo consecutivo entre letras solo 1 guion bajo, precedido o seguido por letrar o numeros,
            - no mas de un punto consecutivo,
            - no puede llevar combinaciones consecutivas de "_" y ".",
            - ningun caracter especial, incluidas  ñ y acentos.
        */

        if(arrEmail[1].search(/yahoo/i)!= -1 && (arrEmail[0][lenUser-1].search(/[^a-zA-Z0-9]/)!=-1 || arrEmail[0][0].search(/(_|\.|[0-9])/)!=-1 || arrEmail[0].search(/(\.|_){2}/g) != -1 || arrEmail[0].search(/-/g)!=-1 ) ){
            return false;
        }

        //Si no es hotmail, outlook, live, gmail ni yahoo, aplicamos las validaciones que ya teniamos:
        //caracteres permitidos de la A a la Z mayusculas o minusculas,
        //numeros del 0 al 9 al final o dentro del nombre de usuario, no al inicio
        //punto, guion medio y guion bajo, ni al inicio ni al final, solo dentro del usuario y sin más de 1 consecutivo
        if(arrEmail[1].search(/(^hotmail|outlook|gmail|live|yahoo)/i) == -1 && ( arrEmail[0][lenUser-1].search(/(-|_|\.)/)!=-1 || arrEmail[0][0].search(/(-|_|\.|[0-9])/)!=-1 || arrEmail[0].search(/(-|_|\.){2}/g) != -1)){
            return false;
        }

        //Pasamos a validar la parte despues del arroba
        //Separamos el nombre del dominio y del dominio superior
        var arrDominio = arrEmail[1].split('.');
        var arrLargo = arrDominio.length;

        //Al separar el nombre del dominio y del dominio superior, solo existe el dominio?
        if (arrLargo < 2){
            return false;
        }
        else{
        //Extraemos el contenido del json en clave y valor
            var strBasura = '/*/content';
            var qryBasura = queryHandler(jsonBasuraContenidoEmail, strBasura);
            var arrBasuraContent = [];
            var array = jQuery.map(qryBasura, function(v, i) {
                return [v];
            });

            // Extrae el contenido del arreglo de palabras basura, solo el valor
            for(i = 0; i < array.length; i++){
                var arrRes = array[i];
                arrBasuraContent.push(arrRes['value']);
            }


            // Hay emails que tienen dominio  y subdominio, por ejemplo fcastill@mail.unitec.mx,
            // entonces verificamos desde el primer elemento al penultimo, ya que el ultimo el es
            // el nombre de dominio superior y requerimos otras validaciones
            for (var x = 0; x <= arrLargo-2; x++) {
                //esta vacio el nombre del dominio o subdominio?
                if (arrDominio[x]===""){
                    return false;
                }

                //es muy largo o muy corto el nombre del dominio?
                if(arrDominio[x].length > 36 || arrDominio[x].length < 2){
                    return false;
                }

                //tiene caracteres especiales?
                if(arrDominio[x].search(/[!"'#$%&\/()=?¿\+¨´¡\\\*\{\}\[\]\|,;:~`^<>°¬@]/)!=-1){
                    return false;
                }

                //Validamos que la palabra del dominio este en el blacklist del archivo de palabras basura
                for(var i=0; i<arrBasuraContent.length; i++){
                    var existe = arrDominio[x].indexOf(arrBasuraContent[i]);
                    if(existe != -1){
                        return false;
                    }
                }
            }


            // Validamos el nombre del dominio superior

            // esta vacio?
            if(arrDominio[arrLargo-1]===""){
                return false;
            }
            // tiene la longitud correcta?
            if(arrDominio[arrLargo-1].length >= 4 || arrDominio[arrLargo-1].length <= 1){
                return false;
            }
            // tiene caracteres especiales?
            if(arrDominio[arrLargo-1].search(/[!"'#$%&\/()=?¿\+¨´¡\\\*\{\}\[\]\|,;:~`^<>°¬@]/)!=-1){
                return false;
            }



        }
        //Flag para cuando sea correcto el email
        customErrorMessage.emailValid = true;
        //Quitamos la alerta de sugerencia de dominio de correo
        jQuery("#"+element.id).typeahead('close');
        return true;
    });

    jQuery("input").on("focusout",function(e){

        var idElement = e.target.id;
        if(jQuery("#"+idElement).val()===""){
            return e.preventDefault();
        }

        if(jQuery("#"+e.target.id).prop("type") === "text" || jQuery("#"+e.target.id).prop("type") === "tel"){
            //console.log(idElement + " " + jQuery("#"+e.target.id));
            var trimmedStr = "";
            trimmedStr = e.target.id==="frm_correo"?formatEmail(jQuery("#"+e.target.id).val().trim().replace(patternRemoveAllSpaces,"")):jQuery("#"+e.target.id).val().trim();
            switch(idElement){
                case 'frm_nombre':
                case 'frm_apaterno':
                case 'frm_amaterno':
                case 'frm_correo':
                case 'frm_celular':
                case 'frm_periodo_interes_sel':
                case 'revalidacion':
                    jQuery("#"+e.target.id).text(trimmedStr);
                    jQuery("#"+e.target.id).val(trimmedStr);
                break;
            }
        }
    });

    var patternVocals = /[aeiou]/i;
    var patternConsonants = /[bcdfghjklmnñpqrstvwxyz]/i;
    var patternFourConsonants = /[bcdfghjklmnñpqrstvwxyz]{6}/i;//5 consonantes consecutivas con o sin espacio, ejem: davidd ffranco, daviddffranco
    var patternFourVocals = /[aeiou]{6}/i;//5 vocales consecutivas con o sin espacio, ejem: francoo oomar, francoooomar
    var patternStartWithSpace = /^\s/; //No iniciar con espacio
    var patternRemoveAllSpaces = /\s/g; //quitar espacios en blanco

    jQuery.validator.addMethod("validateName",function(value,element){
        if(value.match(patternVocals)!=null && value.match(patternConsonants)==null){
            //console.log("son puras vocales");
            return false;
        }
        if(value.match(patternVocals)==null && value.match(patternConsonants)!=null){
            //console.log("son puras consonantes");
            return false;
        }
        if(value.match(patternFourConsonants)!=null){
        //console.log("no mas de 4 consonantes juntas");
        return false;
        }

        if(value.match(patternFourVocals)!=null){
            //console.log("no mas de 4 vocales juntas");
            return false;
        }

        if(value.match(patternStartWithSpace)!=null){
            //console.log("no iniciar con esapcio en blanco");
            return false;
        }

        if(value.match(patternSpacePlusConsonants)!=null){
            //console.log("no iniciar con esapcio en blanco seguido de dos consonantes");
            return false;
        }

        return true;

    });






    jQuery.validator.addMethod("validaTel", function(value, element) {
        //////console.log(checkSecuencial(value));

       if( checkRepetido(value)>3 || checkSecuencial(value)>3 || value=="5556581111" || value.charAt(0)==0 ){
        return false;
       }else{
        return true;
       }

    }, "Teléfono Incorrecto");


    /*JEAB CORRECCION DE VALIDACIONES DE CALCULADORA JEAB 28-04-2016*/

    isFormValid = jQuery("#frm-calculadora").validate({

        ignore: "",

        rules: {

            frm_nombre:{basura : true, basura_completo:true, validateName:true, required : true, minlength: 3},
            frm_apaterno:{basura : true, basura_completo:true, validateName:true, required : true, minlength: 3},
            frm_amaterno:{basura : true, basura_completo:true, validateName:true, required : true, minlength: 3},
            frm_correo:{required:true, validaEmail:true,email:true},
            frm_celular:{required:true, validaTel:true, minlength:10, number:true},
            S_alumno:{required : true},
            politicas: {required:true},
            frm_periodo_interes_sel: {required:true},
            revalidacion: {required:true}

        },//Fin de rules

        messages: {

            frm_nombre: {
                    basura:"Ingresa un nombre válido",
                    basura_completo:"Ingresa un nombre válido",
                    validateName:"Nombre inválido",
                    required:"Por favor ingresa un nombre",
                    minlength:"Nombre inválido"
            },

            S_alumno: {
                required:"¿Eres alumno de la UNITEC?"
            },

            frm_apaterno: {
                basura:"Ingresa un apellido Paterno válido",
                basura_completo:"Ingresa un apellido Paterno válido",
                validateName:"Apellido Paterno inválido",
                required:"Por favor ingresa un apellido paterno",
                minlength:"Apellido Paterno inválido"
            },

            frm_amaterno: {
                basura:"Ingresa un apellido Materno válido",
                basura_completo:"Ingresa un apellido Materno válido",
                validateName:"Apellido Materno inválido",
                required:"Por favor ingresa un apellido Materno",
                minlength:"Apellido Materno inválido"
            },

            frm_correo: {
                required:"Por favor ingresa un correo válido",
                email:"Correo Inválido",
                validaEmail:function(){return customErrorMessage.email;}
            },

            frm_celular: {
                required:"Por favor ingrese un celular valido",
                number:"Revisa tu numero celular",
                minlength:"Al menos debe contener 10 dígitos"
            },
            politicas: {
                required:"Debes aceptar las políticas de privacidad"
            },
            frm_periodo_interes_sel : {
                required: "Elige cuándo quieres entrar"
            },
            revalidacion : {
                required: "Elige si quieres revalidar estudios"
            }
      },//Fin de messages

      showErrors: function(errorMap, errorList) {

          jQuery("#frm-calculadora").find(".frm-err").each(function() {jQuery(this).remove();});

          if( errorList.length ) {

              jQuery("#frm-calculadora").find(".frm-err").each(function() {

                  jQuery(this).remove();

              });

              if( jQuery(errorList[0]['element']).next().hasClass("frm-err") ){

              }else{

                  //JEAB AÑADIMOS CLASE DE BOOTSTRAP ALERT
                  jQuery('<div class="frm-err alert-danger frm-calc-error"></div>').insertAfter(errorList[0]['element']);
              }

              if( errorList.length ) {

                  if( getCookie('lastError') != errorList[0]['element'].id ){

                      switch( errorList[0]['element'].id ) {

                          case "frm_nombre":
                              var msj="nombre";
                              break;

                          case "frm_apaterno":
                              var msj="apaterno";
                              break;

                          case "frm_amaterno":
                              var msj="amaterno";
                              break;

                          case "frm_correo":
                              var msj="correo-electronico";
                              break;

                          case "frm_celular":
                              var msj="celular";
                              break;

                          case "frm_informacion":
                              var msj="tipo-informacion";
                              break;

                          case "S_alumno":
                              var msj="eres-alumno";
                              break;

                          case "politicas": case "frm_politicas":
                              var msj="acepta-politicas";
                              break;

                          default:
                              var msj="(not set)"+errorList[0]['element'].id;

                      }//Termina switch

                      switch( errorList[0]['method'] ) {

                          case "required":  case "frm_informacion": case "politicas":
                              var metodo="vacio";
                              break;

                          case "validaTel": case "email": case "basura": case"basura_completo": case "validateName": case "validaEmail": case "number":
                              var metodo="invalido";
                              break;

                          case "minlength": case "maxlength":
                              var metodo="longitud";
                              break;

                          default:
                              var metodo="(not set)"+errorList[0]['method'];

                      }//Termina swicth
                      //console.log("Enviando error2 "+"calc:"+msj+' | '+metodo);
                      dataLayer.push({

                          'errorField': 'calc:'+msj+' | '+metodo, //dato dinámico
                          'event': 'errorForm'//dato estático
                      });

                      setCookie( 'lastError', errorList[0]['element'].id, 1 );

                  }//Termina if getCookie


                  jQuery("#frm-registro").find(".frm-err").each(function() {jQuery(this).remove();});
                   //////console.log(jQuery(errorList[0]['element']).next().hasClass("frm-err"));

                  if(jQuery(errorList[0]['element']).next().hasClass("frm-err")){

                  }else{

                      if(errorList[0]['element']['name']=="frm_politicas"){

                          jQuery('<div class="frm-err">'+errorList[0]['message']+'</div>').insertAfter(".politicas");

                      }else{

                          jQuery('<div class="frm-err"></div>').insertAfter(errorList[0]['element']);

                      }

                  } //fin else

                  if(errorList.length && errorList[0]['element']['name']!="frm_politicas") {

                      // console.log(errorList[0]);
                      jQuery(errorList[0]['element']).next().html(errorList[0]['message']);

                   }
              }//termina if errorList.length

            }//termina if errorList.length

      },//termina showErrors

      errorPlacement: function(error, element) {

        error.insertBefore(element);

      },

      submitHandler: function(form) {


        setCookie( 'regcompleto','1', 1 );


        //Implementar Coockie Almacenar datos de formulario en una cookie por si refresca la pagina no voverle a pedir los datos
        
        var Nombre               =quitaAcentos(jQuery('#frm_nombre').val());
        var Ap_Pat               =quitaAcentos(jQuery('#frm_apaterno').val());
        var Ap_Mat               =quitaAcentos(jQuery('#frm_amaterno').val());
        var email                =formatEmail(jQuery('#frm_correo').val());
        var informacion_2        =1;
        var Estado               =estadoRandom(jQuery('#frm_celular').val());
        var masculino            ='M';
        var TipoTel              ='cel_calc';
        var Telef                =jQuery('#frm_celular').val();
        var periodo_interes_sel  ='16-1';
        var politica             ='on';




        //Se crea la coockie para poder utilizar la calculadora
        //25-01-2017
        var preserve_form_calc_values = { 
                        "frm_nombre": jQuery('#frm_nombre').val(), 
                        "frm_apaterno": jQuery('#frm_apaterno').val(), 
                        "frm_amaterno": jQuery('#frm_amaterno').val(), 
                        "frm_correo": jQuery('#frm_correo').val(),
                        "frm_celular": jQuery('#frm_celular').val()
        };
        
        setCookieSession("preserve_form_calc_values",JSON.stringify(preserve_form_calc_values));
        console.log(getCookie("preserve_form_calc_values"));
        //Termina implementacion de cookie
        init();
        //Implementación campaña pablo
        if(getParameterByName("utm_campaign")=='FB_HS_BECA' || getParameterByName("utm_campaign")=='GS_HS_BECA'  ){

                    jQuery('#calcApp').data('idCosto','PREPA');
                    jQuery('#calcApp').data('materias','7');
                    jQuery('#calcApp').data('maxmaterias','7');
                    jQuery('#calcApp').data('maxmateriasanios','2');
                    jQuery('#calcApp').data('minmaterias','5');
                    jQuery('#calcApp').data('minmateriasanios','3');
                    jQuery('#calcApp').data('planSelec','1pago');
                    jQuery('#calcApp').data('regcompleto',0);
                    jQuery('#calcApp').data('selectCategoria',56);
                    jQuery('#calcApp').data('selectLinea','PREPARATORIA');
                    jQuery('#calcApp').data('selectMateria',666);
                    jQuery('#calcApp').data('url','Preparatoria');

                    // jQuery.fn.fullpage.silentMoveTo(0, "sec-campus");
                    // getCampus("666");
                    // setTimeout(function(){ 
                    //   getAnioslimite();//obtiene las materias minimas
                    // jQuery.fn.fullpage.silentMoveTo(0, "sec-campus");
                    // },5000)
                    //irCampus();
                    //jQuery.fn.fullpage.moveTo(1, 2);
                    if(jQuery(window).width()>768){
                                getLineasNegocio();
                                getCampus(666);
                                irCampus();
                                jQuery('#PREPARATORIA').click();
                    }else{
                                getCampus(666);
                                irCampus();
                    }
                    
                    
        }else{
            jQuery.fn.fullpage.moveTo(1, 2);
        }

        var d = new Date();
        var hora=d.getDate() + "/" + (d.getMonth() +1) + "/" + d.getFullYear()+ ', '+d.getHours()+':'+d.getMinutes()+':'+d.getSeconds();
        //debuglog(hora);
        //debuglog("------FORMULARIO COMPLETADO-----------------NUEVO USUARIO--------------");
        var Nombre               =quitaAcentos(jQuery('#frm_nombre').val());
        var Ap_Pat               =quitaAcentos(jQuery('#frm_apaterno').val());
        var Ap_Mat               =quitaAcentos(jQuery('#frm_amaterno').val());
        var email                =formatEmail(jQuery('#frm_correo').val());
        var informacion_2        =1;
        var Estado               =estadoRandom(jQuery('#frm_celular').val());
        var masculino            ='M';
        var TipoTel              ='cel_calc';
        var Telef                =jQuery('#frm_celular').val();
        var periodo_interes_sel  ='16-1';
        var politica             ='on';
        //debuglogname(Nombre+" | "+Ap_Pat+" | "+Ap_Mat+" | "+email+" | "+jQuery('#frm_informacion').val()+" | "+Estado+" | "+Telef);
      },
     invalidHandler: function(event, validator){
        var counter = 0;
        var fieldError = "Campo no especificado";
        var message = "Mensaje no Especificado";
        var data = "dato:No Especificado";

        for(var i = 0; i<validator.errorList.length;i++){
            if(counter >= 1){
                break;
            }
            fieldError = validator.errorList[i].element.id;
            message = validator.errorList[i].message;
            data = "dato:"+ document.getElementById(fieldError).value;
            counter++;
        }

        blinkFieldForm(fieldError);
        debugErrors("PASO_UNICO|FRM_CALCULADORA|"+fieldError+"|"+message+"|"+data);

     }

    });


/**
* Ya que el plugin de jquery.validate no implemmenta la validacion en el evento change, delegamos un manejador del evento click
* (equivalente a un touch de mobile) al dropdown hijo que nos crea al vuelo el plugin typeahead, para que al darle click ,
* se haga la validacion solo del campo de correo y se quite la alerta de "correo invalido" en caso que ya sea valido el correo introducido,
* NOTA: La delegacion de eventos se  hace para elementos creados al vuelo, es decir, para elementos que no existen al momento que el motor
* de Javascript ejecuta el codigo que añade los listeners (los .on() ); el hecho de aññadir un listener a un elemento que no existe aun
* en el DOM, nos lleva en la practica a que ese listener no funcionara por que no esta añadido a ningun elemento.
*/

    jQuery(".form-group").on("click","span.tt-dropdown-menu",function(e){
        if(typeof isFormValid != "undefined" ){
            isFormValid.element("#frm_correo");
        }
    });

});

//TERMINA PARTE 2 CALCULADORA

//PARTE 3 CALCULADORA
function hasPlaceholderSupport() {
  var input = document.createElement('input');
  return ('placeholder' in input);
}

if(!hasPlaceholderSupport()){
    var inputs = document.getElementsByTagName('input');
    for(var i=0,  count = inputs.length;i<count;i++){
        if(inputs[i].getAttribute('placeholder')){
            inputs[i].style.cssText = "color:#FFF;font-style:italic;"
            inputs[i].value = inputs[i].getAttribute("placeholder");
            inputs[i].onclick = function(){
                if(this.value == this.getAttribute("placeholder")){
                    this.value = '';
                    this.style.cssText = "color:#000;font-style:normal;"
                }
            }
            inputs[i].onblur = function(){
                if(this.value == ''){
                    this.value = this.getAttribute("placeholder");
                    this.style.cssText = "color:#FFF;font-style:italic;"
                }
            }
        }
    }
}


function openlineafx(obj){
 jQuery(obj).parent().parent().click();
  if( checkCookie('frmunitec') == true ) {

      jQuery('.nuevo-boton-regresar').attr('onclick','regresar();');

  }

}
//JEAB FUNCIONALIDAD PARA EL BOTON DE REGRESAR
function regresar() {

    var elemento=jQuery('.cerrarExpandible').data("affected");
    jQuery('#'+elemento+'>.linea-elementos').children().not('.lineas-caption').hide();
    var arriba=jQuery('#'+elemento).data("ex-top");
    var derecha=jQuery('#'+elemento).data("ex-right");
    var abajo=jQuery('#'+elemento).data("ex-bottom");
    var izquierda=jQuery('#'+elemento).data("ex-left");
    var alto=jQuery('#'+elemento).data("ex-height");
    var ancho=jQuery('#'+elemento).data("ex-width");
    jQuery('#'+elemento).animate({"height": alto+"px","width":ancho,"top":arriba,"left":izquierda}, 500, function() {

      jQuery('#'+elemento).css({"position":"relative","z-index":"1","top":0});
      jQuery('.linea-elementos').css({ "width":"1%", "height":"1%"});

    });
    jQuery('.backMenu').fadeOut();
    jQuery('.unitec-logo').fadeIn();
    jQuery('.backMenu').removeClass('cerrarExpandible');


}
//JEAB FUNCIONALIDAD PARA EL BOTON DE IR A CAMPUS
function irCampus() {

       // getCampus("666");
           // irCampus();
        
            if (jQuery('#calcApp').data('selectLinea') == 'PREPARATORIA') {
                  if (jQuery('#padretutor').val() != 'si' && jQuery('#padretutor').val() != 'no' && (getParameterByName("utm_campaign") != 'FB_HS_BECA' && getParameterByName("utm_campaign") != 'GS_HS_BECA')) {
                            // do something
                                alert('Por favor selecciona si eres padre o tutor.');
                        } else {
                            // do something else
                                   // alert('ass');
                                  getCampus("666");
                          jQuery('.clcCarrerasMod-dsk').click();
                          jQuery.fn.fullpage.moveTo(0, 'sec-campus');
                           //jQuery('.clcCarrerasMod-dsk').change();
                    
                               jQuery('.clcCarrerasMod-dsk').attr('checked', true);
                           jQuery('.clcCarrerasMod-dsk').change();
                
                              // irCampus(); 
                                //irA("sec-campus");
                            }
                } else {
        
                  irA("sec-campus");
            }
    
        
         


}
function irPromedio() {
  if(jQuery('#calcApp').data('campus')===undefined){
    irA("sec-campus");
  }else{
    irA("sec-promedio");
  }

  
}
function irCarrera() {
  irA("sec-cursar");
}

function sendDatalayer(url_val,event){
  if(checkCookie('frmunitec')!=true){
      dataLayer.push({
        'url_value': url_val, //dato dinámico
        'event': event//dato estático
      });
    }
}


//calcualdora facebook

  jQuery("#frm-fbPhone").validate({

    rules:{
      fb_tel:{ required:true, minlength:10, maxlength:10, validaTel:true,number:true }
    },
    messages:{
      fb_tel:   {
            required:"Por favor ingrese un celular valido",
            number:"Revisa tu numero celular",
            minlength:"Al menos debe contener 10 dígitos"},
    },submitHandler: function(form) {
        jQuery('#frm_celular').val(jQuery('#fb_tel').val());
        jQuery('.modal-footer').fadeOut();
        jQuery('.texto-modal-phone').html('<div class="alert alert-success" role="alert">¡Gracias! En breve un asesor se comunicará contigo.</div>');
        dataLayer.push({'event': 'DEJO_TELEFONO'});
        jQuery.fn.fullpage.silentMoveTo(0, "sec-cursar");
        setTimeout(function(){jQuery('#telefono-modal').modal('hide');},3000);
    }

  }); //jQuery("#frm-fbPhone")
