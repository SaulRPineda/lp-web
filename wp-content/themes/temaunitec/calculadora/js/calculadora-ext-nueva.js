/* ---------Cookies

  Metodos de cookies
  setCookie('frmunitec', "calculadora", 1);     //Setea cookie al hacer la peticion ajax a almacen1.php
  getCookie('lastActive')                       //Obtiene valor de cookie
  checkCookie('frmunitec')==true                //Valida si existe

  Valor de cookies
  loginface -> Se crea en login con facebook      val(1,0)
  frmunitec -> Se crea al llegar a resultados      frmunitec   setCookie('frmunitec', "calculadora", 1);


*/
var $ = jQuery;
var isMobile = /iPhone|iPad|iPod|Android/i.test(navigator.userAgent);
  //Mensaje personalizado para error de validacion de correo
  var customErrorMessage = { emailValid: false ,email: 'Correo Inválido '};
//TERMINA PARTE 3 CALCULADORA
  jQuery(document).ready(function(e)
  {

    jQuery("#calcApp").data('regcompleto',0);   //Limpia valor de registro completado
    var nuevoCampus="";                         //variable en caso de cambio de campus
    var nuevaLinea="";                          //variable en caso de cambio de campus
    var isFormValid;                            //Referencia al resultado de validar el form
    initialization(); 
    console.log("despues de inicializar");                          //INICIALIZA FULLPAGE
    escala();                                   //Establece porcentaje de pixeles en escala con data-resize
    console.log("despues de escala");
    /*Valida si existe la variable code, y hace un login con facebook*/
    var code = getParameterByName('code');
    if(code){askFb();};
    console.log("despues de code");
    //Revisa si el usuario llego a resultados, sino se setea la cookie a 0 de loginface
    if(checkCookie('frmunitec')==false){
      setCookie( 'loginface', "0", 1 );
    }
    console.log("despues de loginface");
    //Revisa si existe la cookie de formulario completo, si existe carga sec-cursar y lo posiciona alli
    //Implementacion para cookie que viene de formulario
    console.log("antes del if cooki");
    if(checkCookie('frmunitec')==true || checkCookie('data_frm')==true){
      console.log("Antes de lineas de negocio");
        getLineasNegocio(); 
        jQuery.fn.fullpage.silentMoveTo(0, "sec-cursar");
    }
    console.log("dspues del if de cookie");

    //Implementacion cookie formulario: revisa si existe cookie de formulario 26-01-2017
    if(checkCookie('data_frm')==true){
        
        //Obtenemos los valores de la cookie
        var data_cookie = getCookie('data_frm');
        var obj_cookie = JSON.parse(data_cookie);
        setCookie('frmunitec',true);
        
        //Desktop
        if (jQuery(window).width() > 768) {
          jQuery.fn.fullpage.silentMoveTo(0, "sec-cursar");
          jQuery('#carga-modal').modal('show');
          getLineasNegocio2();
          //Click a la linea de negocio que tiene del formulario
          setTimeout(function(){ 
          jQuery('#' + obj_cookie.linea).click(); 
          //Seleccion de carrera
          jQuery('.sel-carrera option[value="'+obj_cookie.categoria+'"]').attr("selected","selected");
          jQuery('.sel-carrera option[value="'+obj_cookie.categoria+'"]').change();
          //Seleccion de modalidad
          jQuery('.clcCarrerasMod-dsk[value="'+obj_cookie.modalidad+'"]').attr("checked",true);
          //getPagos(lineaNegocio, value, jQuery('#calcApp').data('beca'), jQuery('#calcApp').data('campus'));
          //ID COSTOS
          jQuery('#calcApp').data("idCosto",jQuery('.clcCarrerasMod-dsk[value="'+obj_cookie.modalidad+'"]').data("idcosto"));
          getCampus(obj_cookie.carreraInteres);
          getAnioslimite();
          jQuery('.nuevo-boton-ir-campus').click();


          }, 2000); 
          setTimeout(function(){ 
          //Seleccionar el campus
            jQuery('.clSelectCampus option[id="'+obj_cookie.idcampus+'"]').attr("selected","selected");
            jQuery('.clSelectCampus option[id="'+obj_cookie.idcampus+'"]').change();
            jQuery('#calcApp').data("decampus",obj_cookie.idcampus);
            jQuery('#calcApp').data("campus",obj_cookie.idcampus);
          //Ir a promedio
          jQuery.fn.fullpage.silentMoveTo(0, "sec-promedio");
           }, 2000); 
          
        //Mobile
        }else {
          //Linea
          /*jQuery('#carga-modal').modal('show');
          setTimeout(function(){
            jQuery('div[data-linea='+obj_cookie.linea+']').click(); 
          }, 1000);
          setTimeout(function(){
            jQuery('div[data-categoria='+obj_cookie.categoria+']').click();
          }, 3000);
          setTimeout(function(){
            jQuery('div[data-carreramod="'+obj_cookie.modalidad+'"]').click();
          }, 3000); 
          setTimeout(function(){
            jQuery('div[data-campus='+obj_cookie.idcampus+']').click();
          }, 3000); 
          setTimeout(function(){
            jQuery('div[data-campus="'+obj_cookie.idcampus+'"]').click();
          }, 3000); 
        }*/
      }
    }


    // Click en los botones de login
    jQuery('.fbbtn').on('touchstart click', function(){ askFb(); });
    jQuery('.formbtn').on('touchstart click', function(){ showForm(); });

    /***FUNCIONES MODAL*******************/
    /*
    Aplica para facebook.
    -Cuando cierre el modal del telefono:
      -Cuando el celular sea el deafult 55555555
        -Ejecuta datalayer Registro Calculadora>Registro con Facebook>Abandono en Solicitud de Telefono
    */
    jQuery('#telefono-modal').on('hidden.bs.modal', function () {
        if(jQuery('#frm_celular').val()=="5555555555" ){
          dataLayer.push({'event': 'ABANDONO_CLIC_CERRAR'});
          jQuery('#frm_celular').val('1234567890');// celular para CRM
          jQuery('#frm_banner ').val('CALCULADORA_FB');// celular para CRM
        }
    });

    /*LOG AL SALIR DE LA PAGINA*/
    jQuery(window).bind('beforeunload', function(){
        debugleave(jQuery('#frm_correo').val()+" | Abandona en: "+jQuery('.hz-slide.active').data('anchor')+"|");
    });

    /*URL PARAMETERS (NO FUNCIONANDO)*/
    if(getParameterByName("utm_campus")!=null){
        jQuery('#calcApp').data("campus",getParameterByName("utm_campus"));
    }



    /*****MENU********/
    jQuery( '.triggermenu' ).click(function(){
      menuExpanded = jQuery( '#menu' ).multilevelpushmenu( 'menuexpanded' , jQuery( '#menu' ).multilevelpushmenu( 'findmenusbytitle' , 'Menú' ).first() );
      if( menuExpanded ){
        jQuery( '#menu' ).multilevelpushmenu( 'collapse' );
      }else {
        jQuery( '#menu' ).multilevelpushmenu( 'expand' );
      }
    });

    //****CIERRE DE MENU****//
    jQuery('body').on('click','.btn-close',function(e){
        if(e.target.className.match(/btn-close/)){
            mobileCollapse(jQuery(this).data('btn'));
        }
    });






    //****CLICK EN CLCLINEA (PREPA,LIC,ETC)****//
    jQuery('body').on('click','.clcLinea',function(e){
        //JEAB DISPARA EVENTO Y VALIDA EL CAMPUS
        //Validar que se hayan seleccionado datos necesarios
        //FUNCIONALIDAD PARA EL NUEVO SELECT DE LAS NUEVAS CARRERAS
        jQuery('#calcApp').removeData("crtcampus");
        if(!jQuery(this).hasClass("item-available")){
            jQuery('#error-modal2').modal('show');
            jQuery('.mod-campus').html(jQuery('#calcApp').data("decampus"));
            jQuery('.mod-linea').html(jQuery(this).data("linea"));
            nuevaLinea=jQuery(this).attr("id");
        }
        var campusSelected="";
        if(jQuery(window).width()>768){
            if (e.target !== this){ return;} /*solo afecta si el div es clickeado, no su contenido*/
        }
        jQuery('#secLineas').find(".item-selected").each(function(){jQuery(this).removeClass("item-selected");jQuery("#"+this.id+">h2").remove();
        });  //reinicia los selected itwems
        if(jQuery(this).hasClass("item-available")){
            if(jQuery(this).hasClass("linea-dsk")){
                sendDatalayer('calculadora/sec-categoriaclik','StepCalculator');
                //debuglog("CLICK animando linea de Negocio: "+this.id);
                animar(jQuery(this));
                jQuery('#'+this.id+'>.linea-elementos').append('<div class="col-md-7">\
                                                          <div class="row que-estudiar dark">\
                                                              ¿Qué quieres estudiar?\
                                                              <br>\
                                                              '+ campusSelected+'\
                                                              <div class="col-md-8"><select class="sel-carrera" onChange="setCarrera();return;" style="width: 100%;">\
                                                              </select></div>\
                                                          </div>\
                                                          <div class="row que-modalidad dark">\
                                                              ¿Qué modalidad te interesa?\
                                                              <br>\
                                                          </div>\
                                                          <div class="modstatus"></div>\
                                                          <div class="carreramod dark">\
                                                          </div>\
                                                          <div class="col-md-3 btn-back-sec-cursar"><span class="backMenu back-menu btn btn-primary cerrarExpandible nuevo-boton-regresar" id="new-button-back"><i class="fa fa-chevron-left"></i>&nbsp;Regresar</span></div>\
                                                          <div class="col-md-4 btn-back-sec-cursar" style="text-align:right;margin-left: 45px;"><span class="btn btn-primary nuevo-boton-ir-campus" id="new-button-back" onclick="irCampus();">Seleccionar campus&nbsp;<i class="fa fa-chevron-right"></i></span></div>\
                                                      </div>');

                //AGREGAR FUNCIONALIDAD PARA EL BOTON REGRESAR SI EXISTE COOKIE
                if( checkCookie('frmunitec') == true ) {
                    jQuery('#new-button-back').attr('onclick','regresar();');
                }
                jQuery('#calcApp').data("selectLinea",jQuery(this).data("linea"));
                //Linea en cookie por si refresca la página
                var linea_cookie = { 
                        "selectLinea": jQuery('#calcApp').data("selectLinea"),
                };
                setCookieSession("linea_cookie",JSON.stringify(linea_cookie));
                console.log("COOKIE DE LINEA DE NEGOCIO");
                console.log(getCookie("linea_cookie"));


                getCarreras2();
                if(jQuery(this).find('.sel-carrera').size()==1){
                    jQuery(this).find('.sel-carrera').val(56).change();
                } 
            }else{ //hasClass("linea-dsk")
                jQuery('#calcApp').data("selectLinea", jQuery(this).data("linea"));
                //JEABJEABJEAB POR SI ES MOBILE
                    if (jQuery("#calcApp").data("selectLinea") == "LICENCIATURA" || jQuery("#calcApp").data("selectLinea") == "INGENIERIA" || jQuery("#calcApp").data("selectLinea") == "SALUD") {
                        
                             
                                            jQuery('#calcApp').data("selectLinea", jQuery(this).data("linea"));
                                        jQuery.fn.fullpage.moveTo(1, 'sec-preguntas');
                                        jQuery("#secPreguntas>.container>.row").html("");
                                        jQuery("#secPreguntas>.container>.row").append("<div id='pregunta_examen' style='margin: auto;padding: 10px;font-size: 25px;'>¿Realizaste examen para alguna universidad pública?</div>");
                                        jQuery("#secPreguntas>.container>.row").append('<div onclick="clcPreguntas(this);" value="si" id="examen_si" style="cursor:pointer;" class="item-available col-xs-12 col-md-8 clcSelector clcPreguntas"><span class="ancho-carrera">' + 'Si' + '</span><i class="fa fa-chevron-right pull-right"></i></div>');
                                        jQuery("#secPreguntas>.container>.row").append('<div onclick="clcPreguntas(this);" id="examen_no" value="no" style="cursor:pointer;" class="item-available col-xs-12 col-md-8 clcSelector clcPreguntas"><span class="ancho-carrera">' + 'No' + '</span><i class="fa fa-chevron-right pull-right"></i></div>');
                                        //orderElements(jQuery("#secPreguntas>.container>.row"));
                            
                            } else if (jQuery("#calcApp").data("selectLinea") == "PREPARATORIA") {
                                
                                    
                                                    jQuery('#calcApp').data("selectLinea", jQuery(this).data("linea"));
                                                jQuery.fn.fullpage.moveTo(1, 'sec-preguntas');
                                                jQuery("#secPreguntas>.container>.row").html("");
                                                jQuery("#secPreguntas>.container>.row").append("<div id='pregunta_tutor' style='margin: auto;padding: 10px;font-size: 25px;'>¿Eres padre o tutor?</div>");
                                                jQuery("#secPreguntas>.container>.row").append('<div onclick="clcPreguntasTutor(this);" value="si" id="tutor_si" style="cursor:pointer;" class="item-available col-xs-12 col-md-8 clcSelector clcPreguntasTutor"><span class="ancho-carrera">' + 'Si' + '</span><i class="fa fa-chevron-right pull-right"></i></div>');
                                                jQuery("#secPreguntas>.container>.row").append('<div onclick="clcPreguntasTutor(this);" id="tutor_no" value="no" style="cursor:pointer;" class="item-available col-xs-12 col-md-8 clcSelector clcPreguntasTutor"><span class="ancho-carrera">' + 'No' + '</span><i class="fa fa-chevron-right pull-right"></i></div>');
                                                //orderElements(jQuery("#secPreguntas>.container>.row"));
                                    
                                    } else {

                jQuery(this).addClass('item-selected');
                jQuery(this).append("<h2 class='no-disponible hidden-xs hidden-sm'>Campus Seleccionado</h2>");
                jQuery('#calcApp').data("selectLinea",jQuery(this).data("linea"));
                //Linea en cookie por si refresca la página
                //Linea en cookie por si refresca la página
                var linea_cookie = { 
                        "selectLinea": jQuery('#calcApp').data("selectLinea"),
                };
                setCookieSession("linea_cookie",JSON.stringify(linea_cookie));
                console.log("COOKIE DE LINEA DE NEGOCIO");
                console.log(getCookie("linea_cookie"));

                var ccontCarrera=getCarreras2();
                if(ccontCarrera==1){
                    jQuery('.clcCategoria').click();
                    e.preventDefault();
                    jQuery.fn.fullpage.silentMoveTo(0,'sec-modalidades');
                }else{
                    e.preventDefault();
                    jQuery.fn.fullpage.moveSlideRight();
                }
            }
        }//hasClass("item-available")
    } //Termina else por si es mobile

    //.on('click','.clcLinea'
    }).children().find(".linea-elementos").on('click', function (event) {event.stopPropagation();});

    //JEAB NUEVA VISTA PARA CAMPUS
    jQuery('body').on('click','.clcCampus',function(e){
    //JEAB DISPARA EVENTO Y VALIDA EL CAMPUS
    //FUNCIONALIDAD PARA EL NUEVO SELECT DE LAS NUEVAS CARRERAS
    //console.log(jQuery('#select-campus option:selected').attr('class'));
    jQuery('#calcApp').removeData("crtcampus");
    if(jQuery('#select-campus option:selected').attr("class") != 'item-available'){
        jQuery('#error-modal2').modal('show');
        jQuery('.mod-campus').html(jQuery('#calcApp').data("decampus"));
        jQuery('.mod-linea').html(jQuery(this).data("linea"));
        nuevaLinea=jQuery(this).attr("id");
    }
        var campusSelected="";
        if(jQuery(window).width()>768){
            if (e.target !== this){ return;} /*solo afecta si el div es clickeado, no su contenido*/
        }
        jQuery('#secLineas').find(".item-selected").each(function(){jQuery(this).removeClass("item-selected");jQuery("#"+this.id+">h2").remove();});  //reinicia los selected itwems
        if(jQuery(this).hasClass("item-available")){
            if(jQuery(this).hasClass("linea-dsk")){

              sendDatalayer('calculadora/sec-categoriaclik','StepCalculator');

                //JEAB REVISAR 29-04-2016
                //JEAB FUNCION DESPUES DE SELECCIONAR LINEA DE NEGOCIO
                animar(jQuery(this));
                if(jQuery('#calcApp').data('decampus')){
                    campusSelected="Campus seleccionado: <span class='camp-sel'>"+jQuery('#calcApp').data('decampus')+"</span>";
                }
                //JEAB AGREGAR FUNCIONALIDAD PARA EL BOTON REGRESAR SI EXISTE COOKIE
                if( checkCookie('frmunitec') == true ) {
                    jQuery('#new-button-back').attr('onclick','regresar();');
                }
                jQuery('#calcApp').data("selectLinea",jQuery(this).data("linea"));
                getCarreras2();
                if(jQuery(this).find('.sel-carrera').size()==1){
                    jQuery(this).find('.sel-carrera').val(56).change();
                }
            //hasClass("linea-dsk")
            }else{
                jQuery(this).addClass('item-selected');
                jQuery(this).append("<h2 class='no-disponible hidden-xs hidden-sm'>Campus Seleccionado</h2>");
                jQuery('#calcApp').data("selectLinea",jQuery(this).data("linea"));
                var ccontCarrera=getCarreras2();
                if(ccontCarrera==1){
                    jQuery('.clcCategoria').click();
                    e.preventDefault();
                    jQuery.fn.fullpage.silentMoveTo(0,'sec-modalidades');
                }else{
                    e.preventDefault();
                    jQuery.fn.fullpage.moveSlideRight();
                }
            }
        }//hasClass("item-available")


});//jQuery(document).ready(function(e)

//****CAMBIO DE ESQUEMA A DIFERENTES AÑOS ****//
jQuery('body').on("click",".btn-esquema",function(){

    var mostrar= jQuery(this).data("esquema");
    getDuracion(mostrar);
    jQuery('.toggle').toggles(false);
    jQuery('div[class*="termino-"]').each(function(){

        if(jQuery(this).attr('id') != 'terminos-condiciones') {
          jQuery(this).hide();
        }

    }); //limpialos

    jQuery('.termino-'+mostrar+':not(".p-contado")').fadeIn();
    jQuery('#calcApp').data('termino',mostrar);
    jQuery('#calcApp').data('materias',jQuery('.mat-calc-'+mostrar).html());
    jQuery('.toggle-off').html('<span style="margin-left:-10px;">No</span>');
    jQuery('.toggle-on').html('<span>Si</span>');
    //CAMBIAR PERIODOS EN QUE TERMINA LA CARRERA EN MOBILE JEAB 09-05-2016
    if( jQuery(window).width() < 768 ){
          jQuery('.anios-carrera').addClass('anios-carrera-mobile');
    }else {
          jQuery('.anios-carrera').removeClass('anios-carrera-mobile');
    }
    jQuery('#terminos-condiciones').show();
    jQuery('#ex14').slider("setValue",parseInt(jQuery('.mat-calc-'+mostrar).html()));
    jQuery('#terminos-condiciones').show();

}); //jQuery('body').on("click",".btn-esquema"

//****CLICK EN LOS BOTONES DE MENOS PROMEDIO ****//
jQuery('body').on("click",".control-menos",function(){

  jQuery('.control-mas').fadeIn();
  var sliderp=jQuery(this).data("slider");
  var valor= jQuery('#'+sliderp).slider("getValue");
  var nuevo = valor-0.1;
  if(nuevo<=6 ){
    jQuery('.control-menos').fadeOut();
  }
  if(nuevo>=6 ){
      jQuery('.promedio-val').html(nuevo.toFixed(1));
      jQuery('#'+sliderp).slider("setValue",nuevo);
  }else{
      jQuery('.control-menos').fadeOut();
  }

}); //jQuery('body').on("click",".control-menos"

//****CLICK EN LOS BOTONES DE MAS PROMEDIO****//
jQuery('body').on("click",".control-mas",function(){

    jQuery('.control-menos').fadeIn();
    var sliderp=jQuery(this).data("slider");
    var valor= jQuery('#'+sliderp).slider("getValue");
    var nuevo = valor+0.1;
    if(nuevo==10){
      jQuery('.control-mas').fadeOut();
    }
    if(nuevo<=10){
      jQuery('.promedio-val').html( nuevo.toFixed(1) );
      jQuery('#'+sliderp).slider("setValue",nuevo);
    }

}); //jQuery('body').on("click",".control-mas"

//****CLICK EN LOS BOTONES DE MENOS MATERIAS ****//
jQuery('body').on("click",".control-menos-mat",function(){

    jQuery('.control-mas-mat').fadeIn();
    var sliderp=jQuery(this).data("slider");
    var valor= jQuery('#'+sliderp).slider("getValue");
    var nuevo = valor-1;
    //console.log(jQuery('#calcApp').data('idCosto') +nuevo);
    if(jQuery('#calcApp').data('idCosto')=="DENT" && nuevo<4){
      //console.log("sale");
      return false
    }
    if(nuevo<=1 ){
      jQuery('.control-menos-mat').fadeOut();
    }
    if(nuevo>0 ){
      jQuery('.materias-val').html(nuevo);
      jQuery('#'+sliderp).slider("setValue",nuevo);
    }else{
      jQuery('.control-menos-mat').fadeOut();
    }

}); //jQuery('body').on("click",".control-menos-mat"

//****CLICK EN LOS BOTONES DE MAS MATERIAS ****//
jQuery('body').on("click",".control-mas-mat",function(){
    jQuery('.control-menos-mat').fadeIn();
    jQuery(".btn-esquema").removeClass('btn-primary');
    var sliderp=jQuery(this).data("slider");
    var nuevo= parseInt(jQuery('#'+sliderp).slider("getValue"))+1;
    if(nuevo>=jQuery('#calcApp').data('maxmaterias')){
      jQuery('.control-mas-mat').fadeOut();
    }
    if(nuevo<=jQuery('#calcApp').data('maxmaterias')){
      jQuery('.materias-val').html(nuevo);
      jQuery('.btn-esquema').each(function(){
        if(jQuery('#calcApp').data("mat_"+jQuery(this).data('esquema')+"_"+nuevo)){}
      });
      jQuery('#'+sliderp).slider("setValue",nuevo);
    }
    getDuracion();
});

//****CLICK EN LOS BOTONES DE ACEPTAR PROMEDIO Y MATERIAS  ****//
jQuery('body').on("click","#aceptar", function (e) {

    jQuery('#calcApp').data("promedio",jQuery("#ex13").slider('getValue'));
    jQuery('#calcApp').data("materias",jQuery("#ex14").slider('getValue'));
    getBecas();
    getDuracion();
    jQuery.fn.fullpage.moveTo(0,'resultados');
});


//*******FUNCIONALIDAD DE SLIDERS********//
jQuery("#ex13").slider({ticks: [6,7, 8, 9, 10],ticks_labels: ['6','7', '8', '9', '10'],step:0.1});
jQuery("#ex14").slider({ticks: [1,2, 3, 4,5],ticks_labels: ['1','2', '3', '4', '5'],step:1});
jQuery("body").on("change","#ex13", function (e) {  jQuery('.promctrl').fadeIn(); jQuery('.promedio-val').html(jQuery("#ex13").slider('getValue')); });
jQuery("body").on("change","#ex14", function (e) {  jQuery('.matctrl').fadeIn();  jQuery('.materias-val').html(jQuery("#ex14").slider('getValue')); });
jQuery("body").on("slideStop","#ex14", function (e) {
    if(parseInt(jQuery("#ex14").slider('getValue'))<5){jQuery('#calcApp').data('beca',0);}
    jQuery('#calcApp').data("materias",jQuery("#ex14").slider('getValue'));
    getBecas()
});
jQuery('body').on("slideStop","#ex13", function (e) {
    jQuery('#calcApp').data("promedio",jQuery("#ex13").slider('getValue'));
    getBecas();
    getDuracion();
});

//*******CLICK EN clcCategoria********//
jQuery('body').on("click",".clcCategoria",function(e) {
  jQuery('#calcApp').data("selectCategoria",jQuery(this).data("categoria"));
  jQuery('#calcApp').data("url",jQuery(this).find('span').html());
  var ccontMod= getModalidad("",jQuery(this).data("categoria"));
  if(ccontMod==1){
      e.preventDefault();
      jQuery.fn.fullpage.silentMoveTo(0, "sec-modalidades")
  }else{
      e.preventDefault();
      jQuery.fn.fullpage.moveTo(0, "sec-modalidades")
  }
});

//**************CLICK EN CLCCARRERAMOD MOBIL*******************//
jQuery(document).on("click",".clcCarrerasMod",function(e) {
    jQuery('#calcApp').data("selectMateria",jQuery(this).data("carreramod"));
    jQuery('#calcApp').data("idCosto",jQuery(this).data("idcosto"));
    var ccontCamp=getCampus(jQuery(this).data("carreramod").toString());
    getAnioslimite();//obtiene las materias minimas
    if(ccontCamp==1){
      e.preventDefault();
      jQuery.fn.fullpage.silentMoveTo(0, "sec-campus")
    }else{
      e.preventDefault();
      jQuery.fn.fullpage.moveTo(0, "sec-campus")
    }
     jQuery('#sec-modalidades').find(".item-selected").each(function(){jQuery(this).removeClass("item-selected");jQuery("#"+this.id+">h2").remove();});
    if(jQuery(this).hasClass("item-available")){jQuery(this).addClass('item-selected');        jQuery(this).append("<h2 class='no-disponible hidden-xs hidden-sm'>Campus Seleccionado</h2>");}
});

//**************CLICK EN CLCCARRERAMOD DESK*******************//
jQuery(document).on("change",".clcCarrerasMod-dsk ",function(e){
    sendDatalayer('calculadora/sec-modalidad','StepCalculator');
    jQuery('#calcApp').data("selectMateria",jQuery(this).data("carreramod"));
    jQuery('#calcApp').data("idCosto",jQuery(this).data("idcosto"));
    //debuglog('Escogio que quiere estudiar');
    //debuglog('Materia'+jQuery(this).data("carreramod"));
    //debuglog('idCosto'+jQuery(this).data("idcosto"));
    getCampus(jQuery(this).data("carreramod").toString());
    getAnioslimite();//obtiene las materias minimas
    e.preventDefault();
    jQuery.fn.fullpage.moveTo(0, "sec-campus");
});

/**********FUNCION FULL PAGE************/
function initialization(){
    console.log('INICIALIZANDO');

    jQuery('#fullpage').fullpage({
      anchors: ["calculadora","frm_mobile","sec-cursar","sec-categoria","sec-modalidades","sec-campus","sec-promedio","sec-materias","resultados"],
      menu: true,
      //Scrolling
      css3: false,
      scrollingSpeed: 700,
      autoScrolling: true, // navegador CON true  mobile con false
      fitToSection: false,
      scrollBar: false,
      easing: 'easeInOutCubic',
      easingcss3: 'ease',
      loopBottom: false,
      loopTop: false,
      loopHorizontal: true,
      continuousVertical: false,
      scrollOverflow: false,
      touchSensitivity: 15,
      normalScrollElementTouchThreshold: 5,
      //Accessibility
      keyboardScrolling: false,
      animateAnchor: true,
      recordHistory: true,
      //Design
      controlArrows: false,
      verticalCentered: false,
      resize: false,
      fixedElements: '#header, .footer',
      responsive: 0,
      //Custom selectors
      sectionSelector: '.section',
      slideSelector: '.hz-slide',
      normalScrollElements: '#SliderPRO, .slider',
      //events
      onLeave: function(index, nextIndex, direction) { },
      afterRender: function() {

        setCookie( 'lastActive','', 1 );
        setCookie( 'lastError','', 1 );
        setTimeout(function(){
           if(jQuery('.hz-slide.active').data('anchor')=='intro'){
            console.log('Datalayer: Paso0');
            sendDatalayer('calculadora/paso0','StepCalculator');
            setCookie( 'lastActive', jQuery('.hz-slide.active').data('anchor'), 1 );
          }
        },1000); 

        jQuery.fn.fullpage.silentMoveTo( jQuery('.hz-slide.active').data('anchor'));
        jQuery.fn.fullpage.setAllowScrolling(false, 'left, right');
      },
      afterResize: function() {
        jQuery.fn.fullpage.silentMoveTo('0', jQuery('.hz-slide.active').data('anchor'));
        jQuery.fn.fullpage.setAllowScrolling(false, 'left, right');
      },
      onSlideLeave:function(anchorLink, index, slideIndex, direction, nextSlideIndex){

         //console.log('Saliendo'+anchorLink+index+slideIndex+direction+nextSlideIndex);
      },
      afterSlideLoad: function(anchorLink, index, slideAnchor, slideIndex) {

        console.log("Cookie preserve_form_calc_values "+checkCookie('preserve_form_calc_values'));
        console.log(anchorLink+"___"+index+"___"+slideAnchor+"___"+slideIndex);
        if( ( checkCookie('preserve_form_calc_values')==false && checkCookie('data_frm')==false ) && slideAnchor!='intro'){
            
            console.log('entro aca');
            // if(jQuery('#calcApp').data("desk")){
            //     jQuery.fn.fullpage.silentMoveTo('0','');
            // }else{
               // console.log('regresando');
                jQuery.fn.fullpage.silentMoveTo('0','intro');
            // } 
            
        }else{ 
            console.log('entro aqui');
        }

        console.log('cargando..'+slideAnchor);
         //Se ejecuta el set timeout para que no le pegue cuando se hace resize o recarga la pagina y solo se envie la seccion activa
         if(slideAnchor=="sec-promedio"){
            


            

                switch(jQuery('#calcApp').data('selectLinea')) {
                    case "LICENCIATURA":
                        text = "preparatoria";
                        break;
                    case "POSGRADO":
                        text = "licenciatura";
                        break;
                    case "SALUD":
                        text = "preparatoria";
                        break;
                    case "INGENIERIA":
                        text = "preparatoria";
                        break;
                    case "PREPARATORIA":
                        text = "secundaria";
                        break;
                    default:
                        text = "grado de estudios anterior";
                }
                jQuery('.promedio_linea').text(text);
                console.log(text);

         }

         setTimeout(function(){
            if(slideAnchor=="sec-promedio" && checkCookie('data_frm')==true) {
              jQuery('#carga-modal').modal('hide');
            }
           console.log(getCookie('lastActive')+"-----"+jQuery('.hz-slide.active').data('anchor'));
           if(getCookie('lastActive')!=jQuery('.hz-slide.active').data('anchor')){

            if(slideAnchor=="sec-cursar"){
            jQuery('.fp-slidesContainer').scrollTop(0);
            jQuery('.home-fullscreen').removeClass("espacioForm");
            //Implementacion de cookie para que se renderizen las lineas de negocio si ya lleno el formulario en sesion  
            console.log("APLICA TODOS LOS DISPOSITIVOS");
            console.log(getCookie("preserve_form_calc_values"))

            if( getCookie("preserve_form_calc_values")!='' && 
              getCookie("preserve_form_calc_values") != null && 
              typeof getCookie("preserve_form_calc_values") != 'undefined') 

            {
              console.log(getCookie("preserve_form_calc_values"))
              console.log("Entra a inicializacion");
              init();

            }else {

              console.log("no entra a inicializacion");

            }



                //Implementacion banners de pablo
                  if(content_var != ''){ 
                      switch (resolucion_js) {
                          case "xs":
                              break;
                          case "sm":
                              break;
                          case "md":
                              jQuery("#contenedor-lineas-negocio").attr("style","margin-top: 0px!important");
                              var altura_bloques = jQuery( "#bloques-sec-cursar" ).height();
                              jQuery( "#bloques-sec-cursar" ).height(altura_bloques+47);
                              break;
                          case "lg":
                              jQuery("#contenedor-lineas-negocio").attr("style","margin-top: 0px!important");
                              var altura_bloques = jQuery( "#bloques-sec-cursar" ).height();
                              jQuery( "#bloques-sec-cursar" ).height(altura_bloques+50);
                              break;
                          default:
                          break;
                      }
                  }



                                jQuery('#menu-home-unitec').removeClass('transparentenav');

                                //alert(checkCookie('loginface'));
                                if(slideAnchor!="sec-intro" && getCookie('loginface')==1){
                                    console.log('entor732');
                                  if(checkCookie('regcompleto')!=1){
                                    //alert('ejecuta');
                                      jQuery('#telefono-modal').modal('hide')
                                     // jQuery.fn.fullpage.silentMoveTo(0, "intro");
                                  }
                                }
                                sendDatalayer('calculadora/sec-cursar','StepCalculator');
                                //JEABIX
                                if(checkCookie('frmunitec') != true){
                                    dataLayer.push({
                                      'event': 'IX_web_datoscorrectos'
                                    });
                                    dataLayer.push({
                                      'event': 'IX_web_oferta'
                                    });
                                }
                                if((jQuery('#frm_celular').val()=='5555555555' || jQuery('#frm_celular').val()=="") &&  checkCookie('frmunitec')!=true){
                                  if(checkCookie('frmunitec')!=true){jQuery('#telefono-modal').modal('show');}
                                }



}//Termina if sec_cursar




              //JEABIX
              if(slideAnchor=="sec-categoria"){
                if(checkCookie('frmunitec') != true){
                    dataLayer.push({
                      'event': 'IX_web_carrera'
                    });
                }
              }
              //JEABIX
              if(slideAnchor=="sec-modalidades"){
                if(checkCookie('frmunitec') != true){
                    dataLayer.push({
                      'event': 'IX_web_modalidad'
                    });
                }
              }
              if(slideAnchor=="sec-promedio"){
                  //ESBELTAS
                  getAnioslimite();
                  //JEABIX
                  sendDatalayer('calculadora/sec-promedio','StepCalculator');
                  if(checkCookie('frmunitec') != true){
                      dataLayer.push({
                        'event': 'IX_web_promedio'
                      });
                  }

              }
              if(slideAnchor=="sec-campus"){

                  sendDatalayer('calculadora/sec-campus','StepCalculator');
                  //JEABIX
                  if(checkCookie('frmunitec') != true){
                      dataLayer.push({
                        'event': 'IX_web_campus'
                      });
                  }
              }
              if(slideAnchor=="resultados"){

                sendDatalayer('calculadora/sec-Resultados','StepCalculator');
                //JEABIX
                if(checkCookie('frmunitec') != true){
                    dataLayer.push({
                      'event': 'IX_web_resultado'
                    });
                }

              }
              setCookie( 'lastActive', jQuery('.hz-slide.active').data('anchor'), 1 );
           }
         },100)

        //JEAB SI SOLO HAY UN CAMPUS MANDAMOS A SEC-PROMEDIO
        if(slideAnchor == 'sec-campus') {
              //Si es queretaro deshabilitamos la carrera de BM
              /*if(jQuery("#calcApp").data("selectMateria") == "723" || jQuery("#calcApp").data("selectMateria") == "1221"){
                jQuery("#QRO").attr("disabled","disabled");
                jQuery("#QRO").attr("class","");
              }*/
          jQuery.fn.fullpage.setAutoScrolling(false);
          if(getCookie('campusCounter') == 1) {

            if(checkCookie('data_frm')==true){
            jQuery('.clSelectCampus option[class="item-available"]').attr("selected","selected");
            jQuery('#calcApp').data("campus", jQuery('.clSelectCampus option[class="item-available item-selected"]').attr("id"));
            jQuery('#calcApp').data("decampus", jQuery('.clSelectCampus option[class="item-available item-selected"]').attr("id"));
            jQuery('.clSelectCampus option[class="item-available"]').change();
            //PAra una materia

          }else {
            var uncampus = jQuery("#select-campus option[class='item-available']").text();
            var uncampusdata = jQuery("#select-campus option[class='item-available']").data("campus");
            //jQuery("#select-campus option[class='item-available']").addClass('item-selected');
            jQuery("#select-campus option[value='"+jQuery("#select-campus option[class='item-available']").text()+"']").attr('selected','selected');
            //JEAB CURRENT
            setCampus(uncampus);
            jQuery('#calcApp').data("campus", uncampusdata);
            jQuery('#calcApp').data("decampus", uncampusdata);
            jQuery('.camp-sel').html(('decampus'));
            irA("sec-promedio");
          }
            //console.log(jQuery('#calcApp').data);
            
            //jQuery.fn.fullpage.silentMoveTo(0, "sec-promedio");
          }
        }

        //JEAB Validar si estamos en secciones posteriores para quitar el margen inferior a los bloques
        if( jQuery(window).width() > 768 ){

          if (slideAnchor == 'sec-cursar' || slideAnchor == 'resultados' || slideAnchor == 'sec-campus' || slideAnchor == 'sec-promedio') {

              //jQuery("#bloques-sec-cursar").addClass('margin-bottom-ln');
              jQuery("#p-nombre-carrera").addClass('p-bloques');
              jQuery("#p-campus").addClass('p-bloques');

              jQuery("#d-promedio").addClass('d-bloques');
              jQuery("#d-materias").addClass('d-bloques');

              jQuery("#boton-suma").addClass('b-botones');
              jQuery("#boton-resta").addClass('b-botones');

              jQuery("#boton-suma-materias").addClass('b-botones');
              jQuery("#boton-resta-materias").addClass('b-botones');

          }
        }

        scrollToTop();
        if(slideAnchor=="sec-cursar"){
          if(jQuery('#calcApp').data('selectLinea')){
            jQuery('#'+jQuery('#calcApp').data('selectLinea')).height(jQuery(window).height());
          }
          if(jQuery('.backMenu').hasClass('cerrarExpandible')){
            //JEAB EVITAR QUE SE QUITE EL LOGO DE NAVBAR
            //jQuery('.backMenu').fadeIn();
            //jQuery('.unitec-logo').fadeOut();
            jQuery('.nuevo-boton-regresar').fadeIn();
            //console.log('test');
            //jQuery('#new-button-back').removeAttr("style");
          }else{
            jQuery('.backMenu').fadeOut();
            jQuery('.unitec-logo').fadeIn();
          }

          //JEAB validacion para saber si lleno el fomulario
          /*if((jQuery('#frm_nombre').val() == '' || jQuery('#frm_apaterno').val() == '' || jQuery('#frm_amaterno').val() == '' || jQuery('#frm_correo').val() == '' || jQuery('#frm_celular').val() == '') && !getCookie('frmunitec')) {
             jQuery.fn.fullpage.silentMoveTo(0,0);
          }*/


        }else{
          jQuery('.backMenu').fadeOut();
          jQuery('.unitec-logo').fadeIn();
        }




        if(slideAnchor=="sec-campus"){
          if(jQuery(window).width()<768){
              /*if(jQuery("#calcApp").data("selectMateria") == "723" || jQuery("#calcApp").data("selectMateria") == "1221"){
                  jQuery('[data-campus="QRO"]').remove();
              }*/
            //Remover campus queretaro
          }else{


            //Implementación campaña pablo
            if((getParameterByName("utm_campaign")=='FB_HS_BECA' && getParameterByName("utm_campaign")=='GS_HS_BECA') && jQuery('#'+jQuery('#calcApp').data('selectLinea')).find('.clcCarrerasMod-dsk:checked').size()==0 && checkCookie('data_frm') != true){ 
                jQuery.fn.fullpage.moveTo(0, "sec-cursar"); 
                jQuery('.modstatus').html('Por favor selecciona una modalidad').css('color','red');
                }else{ jQuery('.modstatus').empty();}


            //Implementacion cookie formulario
                          if (jQuery('#' + jQuery('#calcApp').data('selectLinea')).find('.clcCarrerasMod-dsk:checked').size() == 0 && checkCookie('data_frm') != true) { jQuery.fn.fullpage.moveTo(0, "sec-cursar"); jQuery('.modstatus').html('Por favor selecciona una modalidad').css('color', 'red'); } else { jQuery('.modstatus').empty(); }
          }

        }





        if(slideAnchor=="resultados"){


          var textresult = "";
          switch(jQuery('#calcApp').data('selectLinea')) {
              case "LICENCIATURA":
                  textresult = " tu Licenciatura en:";
                  break;
              case "POSGRADO":
                  textresult = " tu Posgrado en:";
                  break;
              case "SALUD":
                  textresult = " tu Licenciatura en:";
                  break;
              case "INGENIERIA":
                  textresult = " tu Ingeniería en:";
                  break;
              case "PREPARATORIA":
                  textresult = " tu Preparatoria en:";
                  break;
              default:
                  textresult = " tus estudios en:";
          }
          jQuery('.anios-carrera').append(textresult);
          //console.log(text);

          if(checkCookie("data_frm")!= true) {
            jQuery(".col-recu").hide();
          }
          

          /* JEAB Para el boton de pago de colegiatura*/
          jQuery('.toggle-off').html('<span style="margin-left:-10px;">No</span>');
          jQuery('.toggle-on').html('<span>Si</span>');

          //JEAB eliminar años para terminar la carrera
          if( jQuery(window).width() < 768 ){

              jQuery('.anios-carrera').addClass('anios-carrera-mobile');

          }else {

            jQuery('.anios-carrera').removeClass('anios-carrera-mobile');

          }


            //COMIENZA CALCULA COSTOS
            var ke = jQuery('#calcApp').data('materias');
            console.log("ANTES DE EJECUTAR GETPAGOS EN RESULTADOS");
            if (jQuery('#calcApp').data('selectMateria') == 195 || jQuery('#calcApp').data('selectMateria') == 192) {
                var ke = 11;
            }
            if (jQuery('#calcApp').data('selectMateria') == 193 || jQuery('#calcApp').data('selectMateria') == 197 || jQuery('#calcApp').data('selectMateria') == 196) {
                var ke = 10;
            }
            console.log(jQuery('#calcApp').data('selectLinea'));
            console.log(ke);
            console.log(jQuery('#calcApp').data('beca'));
            console.log(jQuery('#calcApp').data('campus'));
            calculaCostos(jQuery('#calcApp').data('idCosto'), ke, jQuery('#calcApp').data('beca'), jQuery('#calcApp').data('campus'));
            //TERMINA CALCULA COSTOS


        }

        if(slideAnchor=='resultados' && jQuery('#calcApp').data('selectMateria')>0){
          //debuglog("Mostrando resultado al usuario");
          if(checkCookie('frmunitec')==false){envioAspirantes();}    
        }
        //Envio de recurrente
        if(slideAnchor=='resultados')
        {
        //   if( (getCookie('data_frm_sesion')==null || getCookie('data_frm_sesion')=="") && checkCookie('data_frm')==true){
        //     document.cookie = "data_frm_sesion=true; expires=0;";
        //     //Carrera mod
        //     //alert("Se envia al aspirante" + obj_cookie.modalidad);
        //     envioAspirantes();
        //   } 
        
        }
        /*Control de modalidades*/
        if(slideAnchor=='sec-modalidades' && jQuery('.clcCarrerasMod').size()==1){jQuery('.clcCarrerasMod').click();  /*jQuery.fn.fullpage.silentMoveTo(0, "sec-campus"); */}
        if(slideAnchor=='sec-campus' && jQuery('.clCampus').size()==1 && jQuery('#calcApp').data('crtcampus')!=1 ){
          jQuery('.clCampus').click();
          jQuery.fn.fullpage.silentMoveTo(0, "sec-promedio");
        }
        if(slideAnchor=='sec-campus' && jQuery('.clCampus.item-available').size()==1){
          var div=jQuery('.clCampus.item-available');
          jQuery(div).addClass('item-selected');
          jQuery(div).append("<h2 class='no-disponible hidden-xs hidden-sm'>Campus Seleccionado</h2>");
          jQuery('#calcApp').data("campus",jQuery(div).data("campus"));
          jQuery('#calcApp').data("decampus",validaCampus(jQuery(div).data("campus")));
        }
        if(getParameterByName("utm_campus")!=null && slideAnchor=='sec-campus'){
          setTimeout(function(){ jQuery('div[data-campus="'+getParameterByName("utm_campus")+'"]').click(); },500);
        }
        if(getParameterByName("utm_ln")!=null&&slideAnchor=="sec-cursar"){
          setTimeout(function(){ jQuery('div[data-linea="'+getParameterByName("utm_ln")+'"]').click(); },100);
          if(getParameterByName("utm_ln")=="PREPARATORIA"){
            setTimeout(function(){jQuery('.sel-carrera option:eq(1)').prop('selected', true).change();},200);
          }
        }
        if(slideAnchor=='sec-promedio'||slideAnchor=='sec-materias'){

          if(checkCookie('data_frm')==true){
      if( jQuery(window).width() > 768 ){
            jQuery('#calcApp').data("decampus",obj_cookie.idcampus);
            jQuery('#calcApp').data("campus",obj_cookie.idcampus);
            jQuery('#calcApp').data("campus", jQuery('.clSelectCampus option[class="item-available item-selected"]').attr("id"));
            jQuery('#calcApp').data("decampus", jQuery('.clSelectCampus option[class="item-available item-selected"]').attr("id"));
      }
            var campusForm;
               switch (obj_cookie.idcampus) {
        case "ATZ":
            campusForm= "ATIZAPAN";
            break;
        case "MAR":
            campusForm= "MARINA";
            break;
        case "ECA":
            campusForm= "ECATEPEC";
            break;
        case "SUR":
            campusForm= "SUR";
            break;
        case "CUI":
            campusForm= "CUITLAHUAC";
            break;
        case "LEO":
            campusForm= "LEON";
            break;
        case "REY":
            campusForm = "LOS REYES";
            break;
        case "TOL":
            campusForm= "TOLUCA";
            break;
        case "GDL":
            campusForm= "GUADALAJARA";
            break;
        case "QRO":
            campusForm= "QUERETARO";
            break;
        case "ONL":
            campusForm= "ONLINE";
    }
            setCampus(jQuery("#select-campus option[class='item-available item-selected']").text());
          }
          if(jQuery('#calcApp').data('campus')==null ){

              //mostrarAlerta("Debes elegir un campus ",1);
              init();
              irA("sec-cursar");
          }
        }
        if(slideAnchor=='sec-categoria' && jQuery('.clcCategoria').size()==1){
          jQuery('.clcCategoria').click();
          jQuery.fn.fullpage.silentMoveTo(0, "sec-modalidades");
        }
        //Si en categoria no hay linea regresar a seleccionar linea
        if(slideAnchor=='sec-categoria' && (jQuery('#calcApp').data("selectLinea")=="" || typeof jQuery('#calcApp').data("selectLinea") == "undefined")){
          console.log("Entro en validacion de sec-cursar");
          init();
          irA("sec-cursar");
        }
        //
        if(slideAnchor=='sec-modalidades' && (jQuery('#calcApp').data("selectCategoria")=="" || typeof jQuery('#calcApp').data("selectCategoria") == "undefined")){
          console.log("Entro en validacion de sec-modalidades");
          init();
          irA("sec-cursar");
        }
        if ( (slideAnchor=='sec-campus' && (jQuery('#calcApp').data("selectMateria")=="" || typeof jQuery('#calcApp').data("selectMateria") == "undefined") ) && checkCookie("data_frm") != true   ){
          console.log("Entro en validacion de sec-campus");
          init();
          irA("sec-cursar");
        }

        jQuery.fn.fullpage.setAllowScrolling(false, 'left, right');
        if (slideIndex > 1) {
          jQuery('.color-control').fadeIn();

          if(slideAnchor=='sec-cursar'){
          jQuery('.color-control>div').removeClass("naranja");
          jQuery('.color-control>div').addClass("azul");
          jQuery('.color-control>div').eq(0).addClass("naranja");
          //JEAB Eliminamos clase
          jQuery('#i-cambiar-carrera').removeClass('show-fa');
          jQuery('#i-cambiar-carrera').addClass('hide-fa');
          jQuery('#i-cambiar-campus').removeClass('show-fa');
          jQuery('#i-cambiar-campus').addClass('hide-fa');

          }

          if (slideAnchor == 'sec-campus') {//si la seccion es campus
          jQuery('.color-control>div').removeClass("naranja");
          jQuery('.color-control>div').addClass("azul");
          jQuery('.color-control>div').eq(1).addClass("naranja");
          //JEAB Añadimos indicador de que es clickeable
          jQuery('#i-cambiar-carrera').removeClass('hide-fa');
          jQuery('#i-cambiar-carrera').addClass('show-fa');
          jQuery('#i-cambiar-campus').removeClass('show-fa');
          jQuery('#i-cambiar-campus').addClass('hide-fa');

          }
          if (slideAnchor == 'sec-promedio') {//si la seccion es promedio
          jQuery('.color-control>div').removeClass("naranja");
          jQuery('.color-control>div').addClass("azul");
          jQuery('.color-control>div').eq(2).addClass("naranja");
          //JEAB Añadimos indicador de que es clickeable
          jQuery('#i-cambiar-campus').removeClass('hide-fa');
          jQuery('#i-cambiar-campus').addClass('show-fa');
          }
          if (slideAnchor =='sec-materias') {//si la seccion es materias
          jQuery('.color-control>div').removeClass("naranja");
          jQuery('.color-control>div').addClass("azul");
          jQuery('.color-control>div').eq(3).addClass("naranja");
          }
        } else {
          jQuery('.color-control').fadeOut();
        }
      }//TERMINA AFTERSLIDELOAD

    }); //jQuery('#fullpage').fullpage

} //TERMINA FUNCION initialization

jQuery('.moveSlideRight').click(function(e) { e.preventDefault(); jQuery.fn.fullpage.moveTo(1, 1); });

jQuery('body').on("click",".paso3",function(e){
    init();
    e.preventDefault();
    jQuery.fn.fullpage.moveTo(1, 2);
});

/***********CONTROL DE BOTONES TODO OPTIMIZAR*************/
jQuery('.btn-campus').click( function(e) {

  //JEAB VALIDAR QUE NO SE EJECUTE LA ACCION SI NO TIENE DATOS NECESARIOS
  if( jQuery(window).width() < 768) {

        var linea = jQuery('#calcApp').data().selectLinea;
        var url = jQuery('#calcApp').data().url;
        var idCosto = jQuery('#calcApp').data().idCosto;

        if(typeof linea != 'undefined' &&  typeof url != 'undefined' && typeof idCosto != 'undefined') {

          jQuery('#calcApp').data("crtcampus", 1);
          e.preventDefault();
          jQuery('.color-control>div').removeClass("naranja");
          jQuery('.color-control>div').addClass("azul");
          //jQuery.fn.fullpage.moveTo(0, "sec-campus");
            irCampus();
          selectColor(this);

        }

  }else {

      jQuery('#calcApp').data("crtcampus", 1);
      e.preventDefault();
      jQuery('.color-control>div').removeClass("naranja");
      jQuery('.color-control>div').addClass("azul");
      jQuery.fn.fullpage.moveTo(0, "sec-campus");
      selectColor(this);
  }

});


jQuery('body').on("click",'.btn-cursar',function(e) {

    if(jQuery(window).width()<768){
      reiniciarAppData();
      jQuery('#calcApp').removeData('campus')  ;
    }
    e.preventDefault();
    jQuery('.color-control>div').removeClass("naranja");
    jQuery('.color-control>div').addClass("azul");
    jQuery.fn.fullpage.moveTo(0, "sec-cursar");
    selectColor(this);
    jQuery("#"+jQuery('.cerrarExpandible').data("affected")).css({"height":jQuery(window).height()+"px"});

});


jQuery('.btn-promedio').click(function(e) {

    if( jQuery(window).width() < 768 ) {

        //VALIDAR QUE NO SE EJECUTE LA ACCION SI NO TIENE DATOS NECESARIOS
        var linea = jQuery('#calcApp').data().selectLinea;
        var url = jQuery('#calcApp').data().url;
        var idCosto = jQuery('#calcApp').data().idCosto;
        var campus = jQuery('#calcApp').data().campus;
        var promedio = jQuery('#calcApp').data().promedio;
        if(typeof linea != 'undefined' &&  typeof url != 'undefined' && typeof idCosto != 'undefined' && typeof campus != 'undefined' && typeof promedio != 'undefined') {

            mobileExpand('btn-promedio');

        }

    }

    if(e.target.className.match(/control-menos/)){
      //JEAB BOTONES PROMEDIO MOBILE
      jQuery('.control-mas').css({'visibility':'visible'});
      var sliderp="ex13";
      var valor= jQuery('#'+sliderp).slider("getValue");
      var nuevo = valor-0.1;
      if(nuevo<=6 ){jQuery('.control-menos').css({'visibility':'hidden'});}
      if(nuevo>=6 ){
        jQuery('.promedio-val').html(nuevo.toFixed(1));
        jQuery('#'+sliderp).slider("setValue",nuevo);
      }else{
        jQuery('.control-menos').css({'visibility':'hidden'});
      }
      jQuery('#calcApp').data("promedio",jQuery("#ex13").slider('getValue'));
      jQuery('#calcApp').data("materias",jQuery("#ex14").slider('getValue'));
      getBecas();
      getDuracion();
      //JEAB Agregar texto al tipo de pago
      jQuery('.toggle-off').html('<span style="margin-left:-10px;">No</span>');
      jQuery('.toggle-on').html('<span>Si</span>');
       //JEAB eliminar años para terminar la carrera
      if( jQuery(window).width() < 768 ){

            jQuery('.anios-carrera').addClass('anios-carrera-mobile');

      }else {

            jQuery('.anios-carrera').removeClass('anios-carrera-mobile');

      }
      return false;
    }  //if(e.target.className.match(/control-menos/)

    if(e.target.className.match(/control-mas/)){
      jQuery('.control-menos').css({'visibility':'visible'});
      var sliderp="ex13";
      var valor= jQuery('#'+sliderp).slider("getValue");
      var nuevo = valor+0.1;
      if(nuevo==10){jQuery('.control-mas').css({'visibility':'hidden'});}
      if(nuevo<=10){
        jQuery('.promedio-val').html( nuevo.toFixed(1) );
        jQuery('#'+sliderp).slider("setValue",nuevo);
      }
      jQuery('#calcApp').data("promedio",jQuery("#ex13").slider('getValue'));
      jQuery('#calcApp').data("materias",jQuery("#ex14").slider('getValue'));
      getBecas();
      getDuracion();
      //JEAB Agregar texto al tipo de pago
      jQuery('.toggle-off').html('<span style="margin-left:-10px;">No</span>');
      jQuery('.toggle-on').html('<span>Si</span>');
      //JEAB eliminar años para terminar la carrera
      if( jQuery(window).width() < 768 ){

            jQuery('.anios-carrera').addClass('anios-carrera-mobile');

      }else {

            jQuery('.anios-carrera').removeClass('anios-carrera-mobile');

      }
      return false;

    }//if(e.target.className.match(/control-mas/))


}); //jQuery('.btn-promedio').click


jQuery('.btn-materias').click(function(e) {

    if( jQuery(window).width() < 768 ) {

        //VALIDAR QUE NO SE EJECUTE LA ACCION SI NO TIENE DATOS NECESARIOS
        var linea = jQuery('#calcApp').data().selectLinea;
        var url = jQuery('#calcApp').data().url;
        var idCosto = jQuery('#calcApp').data().idCosto;
        var campus = jQuery('#calcApp').data().campus;
        var promedio = jQuery('#calcApp').data().promedio;
        if(typeof linea != 'undefined' &&  typeof url != 'undefined' && typeof idCosto != 'undefined' && typeof campus != 'undefined' && typeof promedio != 'undefined') {

            mobileExpand('btn-materias');
        }

    }

    if(e.target.className.match(/control-menos-mat/)){

      jQuery('.control-mas-mat').css({'visibility':'visible'});
      var sliderp="ex14";
      var valor= jQuery('#'+sliderp).slider("getValue");
      var nuevo = valor-1;
      if(jQuery('#calcApp').data('idCosto')=="DENT" && nuevo<4){
        jQuery('.control-menos-mat').css({'visibility':'hidden'});
        return false
      }
      if(nuevo<=1 ){jQuery('.control-menos-mat').css({'visibility':'hidden'});}
      if(nuevo>0 ){
       jQuery('.materias-val').html(nuevo);
       jQuery('#'+sliderp).slider("setValue",nuevo);
      }else{
        jQuery('.control-menos-mat').css({'visibility':'hidden'});
      }
      jQuery('#calcApp').data("promedio",jQuery("#ex13").slider('getValue'));
      jQuery('#calcApp').data("materias",jQuery("#ex14").slider('getValue'));
      getBecas();
      getDuracion();
      // Agregar texto al tipo de pago
      jQuery('.toggle-off').html('<span style="margin-left:-10px;">No</span>');
      jQuery('.toggle-on').html('<span>Si</span>');
      // eliminar años para terminar la carrera
      if( jQuery(window).width() < 768 ){
          jQuery('.anios-carrera').addClass('anios-carrera-mobile');
      }else {
          jQuery('.anios-carrera').removeClass('anios-carrera-mobile');
      }
      return false;
    }//if(e.target.className.match(/control-menos-mat/))

    if(e.target.className.match(/control-mas-mat/)){

     jQuery('.control-menos-mat').css({'visibility':'visible'});
     var sliderp="ex14";
     var nuevo= parseInt(jQuery('#'+sliderp).slider("getValue"))+1;
     if(nuevo>=jQuery('#calcApp').data('maxmaterias')){
      jQuery('.control-mas-mat').css({'visibility':'hidden'});}
      if(nuevo<=jQuery('#calcApp').data('maxmaterias')){
        jQuery('.materias-val').html(nuevo);
        jQuery('#'+sliderp).slider("setValue",nuevo);
      }
      jQuery('#calcApp').data("promedio",jQuery("#ex13").slider('getValue'));
      jQuery('#calcApp').data("materias",jQuery("#ex14").slider('getValue'));
      getBecas();
      getDuracion();
      // Agregar texto al tipo de pago
      jQuery('.toggle-off').html('<span style="margin-left:-10px;">No</span>');
      jQuery('.toggle-on').html('<span>Si</span>');
      // eliminar años para terminar la carrera
      if( jQuery(window).width() < 768 ){
          jQuery('.anios-carrera').addClass('anios-carrera-mobile');
      }else {
          jQuery('.anios-carrera').removeClass('anios-carrera-mobile');
      }
      return false;

    } //if(e.target.className.match(/control-mas-mat/))

}); //jQuery('.btn-materias').click(function(e)

/*************FIN DE CONTROL DE BOTONES*********************/

jQuery('#control-input').on("change paste keyup", function() {watchDogs(); });

jQuery('body').on('click','.clCampus',function(e){

    jQuery('#secCampus').find(".item-selected").each(function(){
      jQuery(this).removeClass("item-selected");
      jQuery("#"+this.id+">h2").remove();
    });

    if(jQuery(this).hasClass("item-available")){
      jQuery(this).addClass('item-selected');
      jQuery(this).append("<h2 class='no-disponible hidden-xs hidden-sm'>Campus Seleccionado</h2>");
      
      jQuery('#calcApp').data("campus",jQuery(this).data("campus"));

      jQuery('#calcApp').data("decampus",validaCampus(jQuery(this).data("campus")));
      jQuery('.camp-sel').html(jQuery('#calcApp').data('decampus'));
      irA("sec-promedio");
    }else{
      jQuery('#calcApp').data("nuevoCampus",jQuery(this).data("campus"));
      jQuery('#calcApp').data("decampus",jQuery(this).find('.lineas-caption').html());
      jQuery('#error-modal').modal('show');
      jQuery('.mod-campus').html(jQuery('#calcApp').data("decampus"));
      jQuery('.mod-carrera').html(jQuery('#calcApp').data("selectLinea").toLowerCase()+" "+jQuery('#calcApp').data("deMateria"));
      nuevoCampus=jQuery(this).data("campus");
    }

}); //jQuery('body').on('click','.clCampus'

/*Para la nueva pantalla de seleccionar campus*/

jQuery('body').on('change','.clSelectCampus',function(e){

  jQuery("#select-campus").find(".item-selected").each(function(){
    jQuery(this).removeClass("item-selected");
  });

  if(jQuery("#select-campus :selected").attr("class") == 'item-available'){

      jQuery("#select-campus :selected").addClass('item-selected');
      jQuery('#calcApp').data("campus",jQuery("#select-campus :selected").data("campus"));
      jQuery('#calcApp').data("decampus",validaCampus(jQuery("#select-campus :selected").data("campus")));
      jQuery('.camp-sel').html(jQuery('#calcApp').data('decampus'));
      irA("sec-promedio");

    }else{

      jQuery('#calcApp').data("nuevoCampus",jQuery(this).data("campus"));
      jQuery('#calcApp').data("decampus",jQuery(this).find('.lineas-caption').html());
      jQuery('#error-modal').modal('show');
      jQuery('.mod-campus').html(jQuery('#calcApp').data("decampus"));
      jQuery('.mod-carrera').html(jQuery('#calcApp').data("selectLinea").toLowerCase()+" "+jQuery('#calcApp').data("deMateria"));
      nuevoCampus=jQuery(this).data("campus");

    }

}); //jQuery('body').on('change','.clSelectCampus'


jQuery('body').on('click','.campus-modal-cambiar',function(){

   if(jQuery(this).hasClass("mod-cambia-campus")){
    jQuery('#calcApp').removeData("campus");
    jQuery('#calcApp').data("campus",jQuery('#calcApp').data("nuevoCampus"));
    jQuery('#error-modal2').modal("hide");
    reiniciarAppData();
    getLineasNegocio2();
    setTimeout(function(){ jQuery('#'+nuevaLinea).click();},100)
  }else{
    jQuery('#error-modal').modal("hide");
    jQuery('#calcApp').data("campus",nuevoCampus);
    getLineasNegocio2({campus: jQuery("#calcApp").data("campus")});
    setTimeout(function(){jQuery.fn.fullpage.moveTo(0, "sec-cursar")},100);}

}); //jQuery('body').on('click','.campus-modal-cambiar'

jQuery('body').on("change",".sel-carrera",function(e) {


    if(jQuery(this).val() == "Selecciona") {
      jQuery(".que-modalidad").hide();
    }else {
      jQuery(".que-modalidad").show();
    }

    jQuery('#calcApp').data("selectCategoria",jQuery(this).val());
    jQuery('#calcApp').data("deMateria", jQuery(" option:selected" ,this).text());
    //debuglog("Escogio materia: "+jQuery('#calcApp').data("deMateria"));
    jQuery('#calcApp').removeData("campus");
    getModalidad("",jQuery(this).val());

}); //jQuery('body').on("change",".sel-carrera"


//Boton para expander las lineas de negocio en la calculadora
jQuery('body').on('click','.cerrarExpandible',function(){

    var elemento=jQuery('.cerrarExpandible').data("affected");
    jQuery('#'+elemento+'>.linea-elementos').children().not('.lineas-caption').hide();
    var arriba=jQuery('#'+elemento).data("ex-top");
    var derecha=jQuery('#'+elemento).data("ex-right");
    var abajo=jQuery('#'+elemento).data("ex-bottom");
    var izquierda=jQuery('#'+elemento).data("ex-left");
    var alto=jQuery('#'+elemento).data("ex-height");
    var ancho=jQuery('#'+elemento).data("ex-width");
    jQuery('#'+elemento).animate({"height": alto+"px","width":ancho,"top":arriba,"left":izquierda}, 500,function(){
      jQuery('#'+elemento).css({"position":"relative","z-index":"1","top":0});
      jQuery('.linea-elementos').css({ "width":"1%", "height":"1%"});
    });
    jQuery('.backMenu').fadeOut();
    jQuery('.unitec-logo').fadeIn();
    jQuery('.backMenu').removeClass('cerrarExpandible');

}); //jQuery('body').on('click','.cerrarExpandible'


jQuery('body').on('click','.clcCategoria',function(){
    jQuery('#secCategorias').find(".item-selected").each(function(){
      jQuery(this).removeClass("item-selected");
      jQuery("#"+this.id+">h2").remove();
    });
    if(jQuery(this).hasClass("item-available")){jQuery(this).addClass('item-selected');jQuery(this).append("<h2 class='no-disponible hidden-xs hidden-sm'>Campus Seleccionado</h2>");}

}); //jQuery('body').on('click','.clcCategoria'


jQuery('body').on('click', '.btn-esquema', function(){

    jQuery(this).removeClass('btn-default').addClass('btn-primary');
    jQuery(this).parent().siblings().find('button').removeClass('btn-primary').addClass('btn-default');

}); //jQuery('body').on('click', '.btn-esquema'


jQuery('body').on('click', '.clcSelector', function(){

    if( jQuery('#calcApp').data('selectLinea')){
      jQuery('.crtl>p>i:eq(0)').addClass('fa-check');
    }
    if( jQuery('#calcApp').data('campus')){
      jQuery('.crtl>p>i:eq(1)').addClass('fa-check');
    }

}); //jQuery('body').on('click', '.clcSelector'


jQuery('body').on('click', '#aceptar', function(){
    if( jQuery('#calcApp').data('promedio')){
      jQuery('.promctrl').fadeIn();
      jQuery('.promedio-val').html(jQuery('.promedio-val').eq(1).html());
      jQuery('.crtl>p>i:eq(2)').addClass('fa-check');
    }
    if( jQuery('#calcApp').data('materias')){
      jQuery('.matctrl').fadeIn();
      jQuery('.materias-val').html(jQuery('.materias-val').eq(1).html());
      jQuery('.crtl>p>i:eq(3)').addClass('fa-check');
    }

});//jQuery('body').on('click', '#aceptar'

});
//<![CDATA[
  jQuery(window).load(function() { // makes sure the whole site is loaded
      jQuery('#status').fadeOut(); // will first fade out the loading animation
      jQuery('#preloader').delay(350).fadeOut('slow'); // will fade out the white DIV that covers the website.
      jQuery('body').delay(350).css({'overflow':'visible'});
  })
//]]>

//TERMINA PARTE 1 CALCULADORA

//PARTE 2 CALCULADORA

jQuery(document).ready(function() {

  var patternVocals = /[aeiou]/i;
  var patternConsonants = /[bcdfghjklmnñpqrstvwxyz]/i;
  var patternFourConsonants = /[bcdfghjklmnñpqrstvwxyz]{5}|[bcdfghjklmnñpqrstvwxyz]{2}\s[bcdfghjklmnñpqrstvwxyz]{3}|[bcdfghjklmnñpqrstvwxyz]{3}\s[bcdfghjklmnñpqrstvwxyz]{2}/i;//4 consonantes consecutivas con o sin espacio, ejem: davidd ffranco, daviddffranco
  var patternFourVocals = /[aeiou]{5}|[aeiou]{2}\s[aeiou]{3}|[aeiou]{3}\s[aeiou]{2}/i;//4 vocales consecutivas con o sin espacio, ejem: francoo oomar, francoooomar
  var patternSpacePlusConsonants = /^\s[bcdfghjklmnñpqrstvwxyz]{2,}/i; //No iniciar con espacio seguido de 2 o mas consonantes
  var patternStartWithSpace = /^\s/; //No iniciar con espacio

  jQuery(window).on("resize load",function(){
    if(jQuery(window).width()>768){
      jQuery(".calcu-segunda-seccion").appendTo("#home-calc");
    }else{
    }
  });

  /* Método nombre basura */
  jQuery.validator.addMethod("basura", function(value, element) {
    var strBasura = '/*/content';
    var qryBasura = queryHandler(jsonBasuraContenido, strBasura);
    var arrBasuraContent = [];
    var array = jQuery.map(qryBasura, function(v, i) {return [v];});
      // Valida texto contenido en arrBasura
      for(i = 0; i < array.length; i++){
        var arrRes = array[i];
        arrBasuraContent.push(arrRes['value']);
      }
      for(i=0; i<arrBasuraContent.length; i++){
        var existe = value.toLowerCase().indexOf(arrBasuraContent[i].toLowerCase());
        if(existe != -1){return false;}
      }
      // Valida texto completo en arrBasura
      strBasura = '/*/completo';
      qryBasura = queryHandler(jsonBasuraContenido, strBasura);
      var arrBasuraCompleto = [];
      array = jQuery.map(qryBasura, function(v, i) {return [v];});
      // Valida exista en arrBasura
      for(i = 0; i < array.length; i++){var arrRes = array[i];arrBasuraCompleto.push(arrRes['value']);}
        if(jQuery.inArray(value,arrBasuraCompleto) != -1){return false;}
      // Valida que sean sólo letras
      var patron = /^[a-zA-Záíúéóñ\s]*$/;
      if(value.search(patron) == -1){return false;}
      return true;
    }, "Mensaje error");

jQuery.validator.addMethod("basura_completo", function(value, element) {
        var texto=jQuery.trim(value.toLowerCase());

        //console.log("VALUE/"+texto);

        var strBasura = '/*/completo';
        var qryBasura = queryHandler(jsonBasuraCompleto, strBasura);
        var arrBasuraComplete = [];
        var array = jQuery.map(qryBasura, function(texto, i) {
            return [texto];
        });


        // Valida texto contenido en arrBasura
        for(i = 0; i < array.length; i++){
            var arrRes = array[i];
            arrBasuraComplete.push(arrRes['value']);
            //console.log(arrRes['value']);
        }

        var repetido=arrBasuraComplete.indexOf(texto);
         //console.log(repetido);
        if(repetido!=-1){
           return false;
        }else{ return true;}



        // Valida que sean sólo letras
        var patron = /^[a-zA-Záíúéóñ\s]*$/;
        if(value.search(patron) == -1){
            //////console.log('invalido letras, v='+value);
            return false;
        }

        return true;
    });

   /* Método valida email */
    jQuery.validator.addMethod("validaEmail", function(value, element) {
        //Flag que cambiara cuando sea valido
        customErrorMessage.emailValid = false;
        customErrorMessage.email = 'Correo Inválido';
        //Separamos en 2 partes la direccion
        var arrEmail = value.split('@');

        //Esta vacio el nombre?
        if(arrEmail[0]===""){
            return false;
        }

        //No tiene arroba?
        if(arrEmail.length <= 1){
            return false;
        }
        //Longitud del nombre de usuario
        //es muy largo o muy corto el nombre?
        var lenUser = arrEmail[0].length;
        if(lenUser <= 2 || lenUser > 36){
            return false;
        }

        //Validamos que no existen caracteres raros en el nombre de usuario

        if(arrEmail[0].search(/[^a-zA-Z0-9_.-]/)!=-1){ //Tiene caracteres especiales?
            customErrorMessage.email = "Caracter inválido encontrado.";
            return false;
        }

        /**
        *   El formato de de email de hotmail valida lo siguiente en el nombre de usuario:
        *   PERMITIDO:
        *    - Caracteres permitidos de la A a la Z mayusculas o minusculas
        *    - Numeros del 0 al 9 permitidos al final o dentro del nombre de usuario,
        *    - Guion medio y guion bajo, dentro del usuario la cantidad que sea y consecutivos
        *    - Puntos no consecutivos dentro del usuario.
        *   NO PERMITIDO:
        *    - Números del 0 al 9 al inicio
        *    - Punto, guion medio y guion bajo al inicio
        *    - Punto al final
        *    - Caracteres espciales, incluyendo ñ y acentos
        */
        if(arrEmail[1].search(/(hotmail|outlook|live)/i)!= -1 && (arrEmail[0][lenUser-1].search(/\./)!=-1 || arrEmail[0][0].search(/(-|_|\.|[0-9])/)!=-1 || arrEmail[0].search(/\.{2}/g) != -1) ){
            return false;
        }

        /**
        *   El formato de de email de gmail valida lo siguiente en ll nombre de usuario:
        *   PERMITIDO:
        *   - Caracteres permitidos de la A a la Z mayusculas o minusculas
        *   - Numeros del 0 al 9 permitidos al final o dentro del nombre de usuario,
        *   - Punto dentro del nombre de usuario.
        *   NO PERMITIDO:
        *   - Más de un punto consecutivo.
        *   - Punto al inicio o al final.
        *   - Numeros del 0 al 9 al inicio.
        *   - Caracteres especiales incluyendo guion medio, guion bajo y eñe, acentos, etc., ni al inicio, intermedios o final.
        */
        if(arrEmail[1].search(/gmail/i)!= -1 && (arrEmail[0][lenUser-1].search(/[^a-zA-Z0-9]/)!=-1 || arrEmail[0][0].search(/(-|_|\.|[0-9])/)!=-1 || arrEmail[0].search(/\.{2}/g) != -1 || arrEmail[0].search(/(-|_)/g) != -1)){
            return false;
        }

        /**
            El formato de email de yahoo valida lo siguiente en ll nombre de usuario:
            - El usuario solo empieza con letras,
            - acaba con letra o numero solamente,
            - no acepta guion medio,
            - no acepta guion bajo consecutivo entre letras solo 1 guion bajo, precedido o seguido por letrar o numeros,
            - no mas de un punto consecutivo,
            - no puede llevar combinaciones consecutivas de "_" y ".",
            - ningun caracter especial, incluidas  ñ y acentos.
        */

        if(arrEmail[1].search(/yahoo/i)!= -1 && (arrEmail[0][lenUser-1].search(/[^a-zA-Z0-9]/)!=-1 || arrEmail[0][0].search(/(_|\.|[0-9])/)!=-1 || arrEmail[0].search(/(\.|_){2}/g) != -1 || arrEmail[0].search(/-/g)!=-1 ) ){
            return false;
        }

        //Si no es hotmail, outlook, live, gmail ni yahoo, aplicamos las validaciones que ya teniamos:
        //caracteres permitidos de la A a la Z mayusculas o minusculas,
        //numeros del 0 al 9 al final o dentro del nombre de usuario, no al inicio
        //punto, guion medio y guion bajo, ni al inicio ni al final, solo dentro del usuario y sin más de 1 consecutivo
        if(arrEmail[1].search(/(^hotmail|outlook|gmail|live|yahoo)/i) == -1 && ( arrEmail[0][lenUser-1].search(/(-|_|\.)/)!=-1 || arrEmail[0][0].search(/(-|_|\.|[0-9])/)!=-1 || arrEmail[0].search(/(-|_|\.){2}/g) != -1)){
            return false;
        }

        //Pasamos a validar la parte despues del arroba
        //Separamos el nombre del dominio y del dominio superior
        var arrDominio = arrEmail[1].split('.');
        var arrLargo = arrDominio.length;

        //Al separar el nombre del dominio y del dominio superior, solo existe el dominio?
        if (arrLargo < 2){
            return false;
        }
        else{
        //Extraemos el contenido del json en clave y valor
            var strBasura = '/*/content';
            var qryBasura = queryHandler(jsonBasuraContenidoEmail, strBasura);
            var arrBasuraContent = [];
            var array = jQuery.map(qryBasura, function(v, i) {
                return [v];
            });

            // Extrae el contenido del arreglo de palabras basura, solo el valor
            for(i = 0; i < array.length; i++){
                var arrRes = array[i];
                arrBasuraContent.push(arrRes['value']);
            }


            // Hay emails que tienen dominio  y subdominio, por ejemplo fcastill@mail.unitec.mx,
            // entonces verificamos desde el primer elemento al penultimo, ya que el ultimo el es
            // el nombre de dominio superior y requerimos otras validaciones
            for (var x = 0; x <= arrLargo-2; x++) {
                //esta vacio el nombre del dominio o subdominio?
                if (arrDominio[x]===""){
                    return false;
                }

                //es muy largo o muy corto el nombre del dominio?
                if(arrDominio[x].length > 36 || arrDominio[x].length < 2){
                    return false;
                }

                //tiene caracteres especiales?
                if(arrDominio[x].search(/[!"'#$%&\/()=?¿\+¨´¡\\\*\{\}\[\]\|,;:~`^<>°¬@]/)!=-1){
                    return false;
                }

                //Validamos que la palabra del dominio este en el blacklist del archivo de palabras basura
                for(var i=0; i<arrBasuraContent.length; i++){
                    var existe = arrDominio[x].indexOf(arrBasuraContent[i]);
                    if(existe != -1){
                        return false;
                    }
                }
            }


            // Validamos el nombre del dominio superior

            // esta vacio?
            if(arrDominio[arrLargo-1]===""){
                return false;
            }
            // tiene la longitud correcta?
            if(arrDominio[arrLargo-1].length >= 4 || arrDominio[arrLargo-1].length <= 1){
                return false;
            }
            // tiene caracteres especiales?
            if(arrDominio[arrLargo-1].search(/[!"'#$%&\/()=?¿\+¨´¡\\\*\{\}\[\]\|,;:~`^<>°¬@]/)!=-1){
                return false;
            }



        }
        //Flag para cuando sea correcto el email
        customErrorMessage.emailValid = true;
        //Quitamos la alerta de sugerencia de dominio de correo
        jQuery("#"+element.id).typeahead('close');
        return true;
    });

    jQuery("input").on("focusout",function(e){

        var idElement = e.target.id;
        if(jQuery("#"+idElement).val()===""){
            return e.preventDefault();
        }

        if(jQuery("#"+e.target.id).prop("type") === "text" || jQuery("#"+e.target.id).prop("type") === "tel"){
            //console.log(idElement + " " + jQuery("#"+e.target.id));
            var trimmedStr = "";
            trimmedStr = e.target.id==="frm_correo"?formatEmail(jQuery("#"+e.target.id).val().trim().replace(patternRemoveAllSpaces,"")):jQuery("#"+e.target.id).val().trim();
            switch(idElement){
                case 'frm_nombre':
                case 'frm_apaterno':
                case 'frm_amaterno':
                case 'frm_correo':
                case 'frm_celular':
                case 'frm_periodo_interes_sel':
                case 'revalidacion':
                    jQuery("#"+e.target.id).text(trimmedStr);
                    jQuery("#"+e.target.id).val(trimmedStr);
                break;
            }
        }
    });

    var patternVocals = /[aeiou]/i;
    var patternConsonants = /[bcdfghjklmnñpqrstvwxyz]/i;
    var patternFourConsonants = /[bcdfghjklmnñpqrstvwxyz]{6}/i;//5 consonantes consecutivas con o sin espacio, ejem: davidd ffranco, daviddffranco
    var patternFourVocals = /[aeiou]{6}/i;//5 vocales consecutivas con o sin espacio, ejem: francoo oomar, francoooomar
    var patternStartWithSpace = /^\s/; //No iniciar con espacio
    var patternRemoveAllSpaces = /\s/g; //quitar espacios en blanco

    jQuery.validator.addMethod("validateName",function(value,element){
        if(value.match(patternVocals)!=null && value.match(patternConsonants)==null){
            //console.log("son puras vocales");
            return false;
        }
        if(value.match(patternVocals)==null && value.match(patternConsonants)!=null){
            //console.log("son puras consonantes");
            return false;
        }
        if(value.match(patternFourConsonants)!=null){
        //console.log("no mas de 4 consonantes juntas");
        return false;
        }

        if(value.match(patternFourVocals)!=null){
            //console.log("no mas de 4 vocales juntas");
            return false;
        }

        if(value.match(patternStartWithSpace)!=null){
            //console.log("no iniciar con esapcio en blanco");
            return false;
        }

        if(value.match(patternSpacePlusConsonants)!=null){
            //console.log("no iniciar con esapcio en blanco seguido de dos consonantes");
            return false;
        }

        return true;

    });






    jQuery.validator.addMethod("validaTel", function(value, element) {
        //////console.log(checkSecuencial(value));

       if( checkRepetido(value)>3 || checkSecuencial(value)>3 || value=="5556581111" || value.charAt(0)==0 ){
        return false;
       }else{
        return true;
       }

    }, "Teléfono Incorrecto");


    /*JEAB CORRECCION DE VALIDACIONES DE CALCULADORA JEAB 28-04-2016*/

    isFormValid = jQuery("#frm-calculadora").validate({

        ignore: "",

        rules: {

            frm_nombre:{basura : true, basura_completo:true, validateName:true, required : true, minlength: 3},
            frm_apaterno:{basura : true, basura_completo:true, validateName:true, required : true, minlength: 3},
            frm_amaterno:{basura : true, basura_completo:true, validateName:true, required : true, minlength: 3},
            frm_correo:{required:true, validaEmail:true,email:true},
            frm_celular:{required:true, validaTel:true, minlength:10, number:true},
            S_alumno:{required : true},
            politicas: {required:true},
            frm_periodo_interes_sel: {required:true},
            revalidacion: {required:true}

        },//Fin de rules

        messages: {

            frm_nombre: {
                    basura:"Ingresa un nombre válido",
                    basura_completo:"Ingresa un nombre válido",
                    validateName:"Nombre inválido",
                    required:"Por favor ingresa un nombre",
                    minlength:"Nombre inválido"
            },

            S_alumno: {
                required:"¿Eres alumno de la UNITEC?"
            },

            frm_apaterno: {
                basura:"Ingresa un apellido Paterno válido",
                basura_completo:"Ingresa un apellido Paterno válido",
                validateName:"Apellido Paterno inválido",
                required:"Por favor ingresa un apellido paterno",
                minlength:"Apellido Paterno inválido"
            },

            frm_amaterno: {
                basura:"Ingresa un apellido Materno válido",
                basura_completo:"Ingresa un apellido Materno válido",
                validateName:"Apellido Materno inválido",
                required:"Por favor ingresa un apellido Materno",
                minlength:"Apellido Materno inválido"
            },

            frm_correo: {
                required:"Por favor ingresa un correo válido",
                email:"Correo Inválido",
                validaEmail:function(){return customErrorMessage.email;}
            },

            frm_celular: {
                required:"Por favor ingrese un celular valido",
                number:"Revisa tu numero celular",
                minlength:"Al menos debe contener 10 dígitos"
            },
            politicas: {
                required:"Debes aceptar las políticas de privacidad"
            },
            frm_periodo_interes_sel : {
                required: "Elige cuándo quieres entrar"
            },
            revalidacion : {
                required: "Elige si quieres revalidar estudios"
            }
      },//Fin de messages

      showErrors: function(errorMap, errorList) {

          jQuery("#frm-calculadora").find(".frm-err").each(function() {jQuery(this).remove();});

          if( errorList.length ) {

              jQuery("#frm-calculadora").find(".frm-err").each(function() {

                  jQuery(this).remove();

              });

              if( jQuery(errorList[0]['element']).next().hasClass("frm-err") ){

              }else{

                  //JEAB AÑADIMOS CLASE DE BOOTSTRAP ALERT
                  jQuery('<div class="frm-err alert-danger frm-calc-error"></div>').insertAfter(errorList[0]['element']);
              }

              if( errorList.length ) {

                  if( getCookie('lastError') != errorList[0]['element'].id ){

                      switch( errorList[0]['element'].id ) {

                          case "frm_nombre":
                              var msj="nombre";
                              break;

                          case "frm_apaterno":
                              var msj="apaterno";
                              break;

                          case "frm_amaterno":
                              var msj="amaterno";
                              break;

                          case "frm_correo":
                              var msj="correo-electronico";
                              break;

                          case "frm_celular":
                              var msj="celular";
                              break;

                          case "frm_informacion":
                              var msj="tipo-informacion";
                              break;

                          case "S_alumno":
                              var msj="eres-alumno";
                              break;

                          case "politicas": case "frm_politicas":
                              var msj="acepta-politicas";
                              break;

                          default:
                              var msj="(not set)"+errorList[0]['element'].id;

                      }//Termina switch

                      switch( errorList[0]['method'] ) {

                          case "required":  case "frm_informacion": case "politicas":
                              var metodo="vacio";
                              break;

                          case "validaTel": case "email": case "basura": case"basura_completo": case "validateName": case "validaEmail": case "number":
                              var metodo="invalido";
                              break;

                          case "minlength": case "maxlength":
                              var metodo="longitud";
                              break;

                          default:
                              var metodo="(not set)"+errorList[0]['method'];

                      }//Termina swicth
                      //console.log("Enviando error2 "+"calc:"+msj+' | '+metodo);
                      dataLayer.push({

                          'errorField': 'calc:'+msj+' | '+metodo, //dato dinámico
                          'event': 'errorForm'//dato estático
                      });

                      setCookie( 'lastError', errorList[0]['element'].id, 1 );

                  }//Termina if getCookie


                  jQuery("#frm-registro").find(".frm-err").each(function() {jQuery(this).remove();});
                   //////console.log(jQuery(errorList[0]['element']).next().hasClass("frm-err"));

                  if(jQuery(errorList[0]['element']).next().hasClass("frm-err")){

                  }else{

                      if(errorList[0]['element']['name']=="frm_politicas"){

                          jQuery('<div class="frm-err">'+errorList[0]['message']+'</div>').insertAfter(".politicas");

                      }else{

                          jQuery('<div class="frm-err"></div>').insertAfter(errorList[0]['element']);

                      }

                  } //fin else

                  if(errorList.length && errorList[0]['element']['name']!="frm_politicas") {

                      // console.log(errorList[0]);
                      jQuery(errorList[0]['element']).next().html(errorList[0]['message']);

                   }
              }//termina if errorList.length

            }//termina if errorList.length

      },//termina showErrors

      errorPlacement: function(error, element) {

        error.insertBefore(element);

      },

      submitHandler: function(form) {


        setCookie( 'regcompleto','1', 1 );


        //Implementar Coockie Almacenar datos de formulario en una cookie por si refresca la pagina no voverle a pedir los datos
        
        var Nombre               =quitaAcentos(jQuery('#frm_nombre').val());
        var Ap_Pat               =quitaAcentos(jQuery('#frm_apaterno').val());
        var Ap_Mat               =quitaAcentos(jQuery('#frm_amaterno').val());
        var email                =formatEmail(jQuery('#frm_correo').val());
        var informacion_2        =1;
        var Estado               =estadoRandom(jQuery('#frm_celular').val());
        var masculino            ='M';
        var TipoTel              ='cel_calc';
        var Telef                =jQuery('#frm_celular').val();
        var periodo_interes_sel  ='16-1';
        var politica             ='on';




        //Se crea la coockie para poder utilizar la calculadora
        //25-01-2017
        var preserve_form_calc_values = { 
                        "frm_nombre": jQuery('#frm_nombre').val(), 
                        "frm_apaterno": jQuery('#frm_apaterno').val(), 
                        "frm_amaterno": jQuery('#frm_amaterno').val(), 
                        "frm_correo": jQuery('#frm_correo').val(),
                        "frm_celular": jQuery('#frm_celular').val()
        };
        
        setCookieSession("preserve_form_calc_values",JSON.stringify(preserve_form_calc_values));
        console.log(getCookie("preserve_form_calc_values"));
        //Termina implementacion de cookie
        init();
        //Implementación campaña pablo
        if(getParameterByName("utm_campaign")=='FB_HS_BECA' || getParameterByName("utm_campaign")=='GS_HS_BECA'  ){

                    jQuery('#calcApp').data('idCosto','PREPA');
                    jQuery('#calcApp').data('materias','7');
                    jQuery('#calcApp').data('maxmaterias','7');
                    jQuery('#calcApp').data('maxmateriasanios','2');
                    jQuery('#calcApp').data('minmaterias','5');
                    jQuery('#calcApp').data('minmateriasanios','3');
                    jQuery('#calcApp').data('planSelec','1pago');
                    jQuery('#calcApp').data('regcompleto',0);
                    jQuery('#calcApp').data('selectCategoria',56);
                    jQuery('#calcApp').data('selectLinea','PREPARATORIA');
                    jQuery('#calcApp').data('selectMateria',666);
                    jQuery('#calcApp').data('url','Preparatoria');

                    // jQuery.fn.fullpage.silentMoveTo(0, "sec-campus");
                    // getCampus("666");
                    // setTimeout(function(){ 
                    //   getAnioslimite();//obtiene las materias minimas
                    // jQuery.fn.fullpage.silentMoveTo(0, "sec-campus");
                    // },5000)
                    //irCampus();
                    //jQuery.fn.fullpage.moveTo(1, 2);
                    if(jQuery(window).width()>768){
                                getLineasNegocio2();
                                getCampus(666);
                                irCampus();
                                jQuery('#PREPARATORIA').click();
                    }else{
                                getCampus(666);
                                irCampus();
                    }
                    
                    
        }else{

            if(isMobile)
            {
                // jQuery.fn.fullpage.moveTo(1, 2);
                jQuery("#aplicativo-calculadora").show();
                jQuery("#todo_calculadora_body").hide();
            } else {
                console.log("Mostrando todo lo nuevo");
                jQuery("#aplicativo-calculadora").show();
                $('#theme-calculadora-nueva-css').prop('disabled', false);
                $('#theme-calculadora-nueva-css').prop('disabled', false);
                $('#bootstrapcss-css').prop('disabled',false);
                // $('#theme-calculadora-css').prop('disabled',true);
                jQuery("#todo_calculadora_body").hide();
            }
            // jQuery.fn.fullpage.moveTo(1, 2);
            
        }

        var d = new Date();
        var hora=d.getDate() + "/" + (d.getMonth() +1) + "/" + d.getFullYear()+ ', '+d.getHours()+':'+d.getMinutes()+':'+d.getSeconds();
        //debuglog(hora);
        //debuglog("------FORMULARIO COMPLETADO-----------------NUEVO USUARIO--------------");
        var Nombre               =quitaAcentos(jQuery('#frm_nombre').val());
        var Ap_Pat               =quitaAcentos(jQuery('#frm_apaterno').val());
        var Ap_Mat               =quitaAcentos(jQuery('#frm_amaterno').val());
        var email                =formatEmail(jQuery('#frm_correo').val());
        var informacion_2        =1;
        var Estado               =estadoRandom(jQuery('#frm_celular').val());
        var masculino            ='M';
        var TipoTel              ='cel_calc';
        var Telef                =jQuery('#frm_celular').val();
        var periodo_interes_sel  ='16-1';
        var politica             ='on';
        //debuglogname(Nombre+" | "+Ap_Pat+" | "+Ap_Mat+" | "+email+" | "+jQuery('#frm_informacion').val()+" | "+Estado+" | "+Telef);
      },
     invalidHandler: function(event, validator){
        var counter = 0;
        var fieldError = "Campo no especificado";
        var message = "Mensaje no Especificado";
        var data = "dato:No Especificado";

        for(var i = 0; i<validator.errorList.length;i++){
            if(counter >= 1){
                break;
            }
            fieldError = validator.errorList[i].element.id;
            message = validator.errorList[i].message;
            data = "dato:"+ document.getElementById(fieldError).value;
            counter++;
        }

        blinkFieldForm(fieldError);
        debugErrors("PASO_UNICO|FRM_CALCULADORA|"+fieldError+"|"+message+"|"+data);

     }

    });


/**
* Ya que el plugin de jquery.validate no implemmenta la validacion en el evento change, delegamos un manejador del evento click
* (equivalente a un touch de mobile) al dropdown hijo que nos crea al vuelo el plugin typeahead, para que al darle click ,
* se haga la validacion solo del campo de correo y se quite la alerta de "correo invalido" en caso que ya sea valido el correo introducido,
* NOTA: La delegacion de eventos se  hace para elementos creados al vuelo, es decir, para elementos que no existen al momento que el motor
* de Javascript ejecuta el codigo que añade los listeners (los .on() ); el hecho de aññadir un listener a un elemento que no existe aun
* en el DOM, nos lleva en la practica a que ese listener no funcionara por que no esta añadido a ningun elemento.
*/

    jQuery(".form-group").on("click","span.tt-dropdown-menu",function(e){
        if(typeof isFormValid != "undefined" ){
            isFormValid.element("#frm_correo");
        }
    });

});

//TERMINA PARTE 2 CALCULADORA

//PARTE 3 CALCULADORA
function hasPlaceholderSupport() {
  var input = document.createElement('input');
  return ('placeholder' in input);
}

if(!hasPlaceholderSupport()){
    var inputs = document.getElementsByTagName('input');
    for(var i=0,  count = inputs.length;i<count;i++){
        if(inputs[i].getAttribute('placeholder')){
            inputs[i].style.cssText = "color:#FFF;font-style:italic;"
            inputs[i].value = inputs[i].getAttribute("placeholder");
            inputs[i].onclick = function(){
                if(this.value == this.getAttribute("placeholder")){
                    this.value = '';
                    this.style.cssText = "color:#000;font-style:normal;"
                }
            }
            inputs[i].onblur = function(){
                if(this.value == ''){
                    this.value = this.getAttribute("placeholder");
                    this.style.cssText = "color:#FFF;font-style:italic;"
                }
            }
        }
    }
}


function openlineafx(obj){
 jQuery(obj).parent().parent().click();
  if( checkCookie('frmunitec') == true ) {

      jQuery('.nuevo-boton-regresar').attr('onclick','regresar();');

  }

}
//JEAB FUNCIONALIDAD PARA EL BOTON DE REGRESAR
function regresar() {

    var elemento=jQuery('.cerrarExpandible').data("affected");
    jQuery('#'+elemento+'>.linea-elementos').children().not('.lineas-caption').hide();
    var arriba=jQuery('#'+elemento).data("ex-top");
    var derecha=jQuery('#'+elemento).data("ex-right");
    var abajo=jQuery('#'+elemento).data("ex-bottom");
    var izquierda=jQuery('#'+elemento).data("ex-left");
    var alto=jQuery('#'+elemento).data("ex-height");
    var ancho=jQuery('#'+elemento).data("ex-width");
    jQuery('#'+elemento).animate({"height": alto+"px","width":ancho,"top":arriba,"left":izquierda}, 500, function() {

      jQuery('#'+elemento).css({"position":"relative","z-index":"1","top":0});
      jQuery('.linea-elementos').css({ "width":"1%", "height":"1%"});

    });
    jQuery('.backMenu').fadeOut();
    jQuery('.unitec-logo').fadeIn();
    jQuery('.backMenu').removeClass('cerrarExpandible');


}
//JEAB FUNCIONALIDAD PARA EL BOTON DE IR A CAMPUS
function irCampus() {

       // getCampus("666");
           // irCampus();
        
            if (jQuery('#calcApp').data('selectLinea') == 'PREPARATORIA') {
                  if (jQuery('#padretutor').val() != 'si' && jQuery('#padretutor').val() != 'no' && (getParameterByName("utm_campaign") != 'FB_HS_BECA' && getParameterByName("utm_campaign") != 'GS_HS_BECA')) {
                            // do something
                                alert('Por favor selecciona si eres padre o tutor.');
                        } else {
                            // do something else
                                   // alert('ass');
                                  getCampus("666");
                          jQuery('.clcCarrerasMod-dsk').click();
                          jQuery.fn.fullpage.moveTo(0, 'sec-campus');
                           //jQuery('.clcCarrerasMod-dsk').change();
                    
                               jQuery('.clcCarrerasMod-dsk').attr('checked', true);
                           jQuery('.clcCarrerasMod-dsk').change();
                
                              // irCampus(); 
                                //irA("sec-campus");
                            }
                } else {
        
                  irA("sec-campus");
            }
    
        
         


}
function irPromedio() {
  if(jQuery('#calcApp').data('campus')===undefined){
    irA("sec-campus");
  }else{
    irA("sec-promedio");
  }

  
}
function irCarrera() {
  irA("sec-cursar");
}

function sendDatalayer(url_val,event){
  if(checkCookie('frmunitec')!=true){
      dataLayer.push({
        'url_value': url_val, //dato dinámico
        'event': event//dato estático
      });
    }
}


//calcualdora facebook

  jQuery("#frm-fbPhone").validate({

    rules:{
      fb_tel:{ required:true, minlength:10, maxlength:10, validaTel:true,number:true }
    },
    messages:{
      fb_tel:   {
            required:"Por favor ingrese un celular valido",
            number:"Revisa tu numero celular",
            minlength:"Al menos debe contener 10 dígitos"},
    },submitHandler: function(form) {
        jQuery('#frm_celular').val(jQuery('#fb_tel').val());
        jQuery('.modal-footer').fadeOut();
        jQuery('.texto-modal-phone').html('<div class="alert alert-success" role="alert">¡Gracias! En breve un asesor se comunicará contigo.</div>');
        dataLayer.push({'event': 'DEJO_TELEFONO'});
        jQuery.fn.fullpage.silentMoveTo(0, "sec-cursar");
        setTimeout(function(){jQuery('#telefono-modal').modal('hide');},3000);
    }

  }); //jQuery("#frm-fbPhone")


/* IMPLEMENTACION OTRA CALCULADORA */

var objIncluye = {
    prepa: ["Acceso a Biblioteca Virtual las 24 horas", "Uso de laboratorios tecnológicos sin costo", "Actividades culturales y deportivas", "Talleres de orientación vocacional", "Centro de Apoyo y Desarrollo Estudiantil"],
    licenciaturas_ingenierias: ["Acceso a Biblioteca Virtual las 24 horas", "Uso de laboratorios tecnológicos sin costo", "Actividades culturales y deportivas", "Bolsa de Trabajo con más de 43 mil vacantes en línea", "Centro de Apoyo y Desarrollo Estudiantil"],
    enfermeria: ["Primer juego de uniformes sin costo", "Uso de Hospital simulado y laboratorios de alta fidelidad", "Actividades culturales y deportivas", "Bolsa de Trabajo con más de 43 mil vacantes en línea", "Centro de Apoyo y Desarrollo Estudiantil"],
    fisioterapia: ["Primer juego de uniformes sin costo", "Uso del Gimnasio terapéutico y laboratorios de alta fidelidad", "Actividades culturales y deportivas", "Bolsa de Trabajo con más de 43 mil vacantes en línea", "Centro de Apoyo y Desarrollo Estudiantil"],
    nutricion: ["Acceso a Biblioteca Virtual las 24 horas", "Uso de Hospital simulado y laboratorios de alta fidelidad", "Actividades culturales y deportivas", "Bolsa de Trabajo con más de 43 mil vacantes en línea", "Centro de Apoyo y Desarrollo Estudiantil"],
    odontologia: ["Uso de laboratorios tecnológicos sin costo", "Prácticas en la Clínica de Odontología con pacientes reales", "Actividades culturales y deportivas", "Bolsa de Trabajo con más de 43 mil vacantes en línea", "Centro de Apoyo y Desarrollo Estudiantil"],
    gastronomia: ["Utensilios e insumos para prácticas sin costo", "Primer juego de uniformes", "Acceso a cocinas de simulación desde el primer cuatrimestre", "Bolsa de Trabajo con más de 43 mil vacantes en línea", "Centro de Apoyo y Desarrollo Estudiantil"],
    turismo: ["Primer juego de uniformes sin costo", "Convenio para prácticas profesionales en los mejores restaurantes y hoteles de México", "Uso de laboratorios tecnológicos sin costo", "Bolsa de Trabajo con más de 43 mil vacantes en línea", "Centro de Apoyo y Desarrollo Estudiantil"],
    posgrado: ["Acceso a Biblioteca Virtual las 24 horas", "Uso de laboratorios tecnológicos sin costo", "Bolsa de Trabajo con más de 43 mil vacantes en línea", "Plataforma educativa Blackboard"]
}

var videosLineas = {
    ingenieria: "/wp-content/themes/temaunitec/assets/videos_calculadora/ingenierias.mp4",
    licenciatura: "/wp-content/themes/temaunitec/assets/videos_calculadora/licenciaturas.mp4",
    online: "/wp-content/themes/temaunitec/assets/videos_calculadora/online.mp4",
    posgrado: "/wp-content/themes/temaunitec/assets/videos_calculadora/posgrado.mp4",
    preparatoria: "/wp-content/themes/temaunitec/assets/videos_calculadora/preparatoria.mp4",
    salud: "/wp-content/themes/temaunitec/assets/videos_calculadora/salud.mp4"
}

var xCalculadora = 0;
var yCalculadora = 0;
var pasoFinalCalculadora = false;
var regresoAnterior = true;

function getRelation(carrera_string) {
    switch (carrera_string) {
        case "preparatoria":
            return "prepa";
            break;
        case "licenciatura":
            return "licenciaturas_ingenierias";
            break;
        case "ingenieria":
            return "licenciaturas_ingenierias";
            break;
        case "enfermeria":
            return "enfermeria";
            break;
        case "enfermería":
            return "enfermeria";
            break;
        case "fisioterapia":
            return "fisioterapia";
            break;
        case "nutrición":
            return "nutricion";
            break;
        case "nutricion":
            return "nutricion";
            break;
        case "odontología":
            return "odontologia";
            break;
        case "odontologia":
            return "odontologia";
            break;
        case "salud":
            return "odontologia";
            break;
        case "gastronomía":
            return "gastronomia";
            break;
        case "gastronomia":
            return "gastronomia";
            break;
        case "turismo":
            return "turismo";
            break;
        case "posgrado":
            return "posgrado";
            break;
    }
}

function changeVideo(tipoVideo) {
    var direccionVideo = videosLineas[tipoVideo];
    $("#video_background source").attr("src", direccionVideo);
    $("#video_background")[0].load();
    // $("#video_background")[0].play();
    // <video controls id="video_background" class="PromotionGlobalLarge-videoBackgroundWrapper" preload="auto" autoplay="false" loop="loop" muted="muted" volume="0"><source src="/wp-content/themes/temaunitec/calculadora/videos-calculadora/ingenierias.mp4" type="video/mp4"> Video not supported </video>
}

function changeAddInCareers(careerSearch) {
    /*Llamada en render_modalidades */
    // console.log("Recibo la modalidad 0 ");
    // console.log(careerSearch);
    var careerLineaWeb = careerSearch.lineaweb.toLowerCase();
    var careerName = careerSearch.nombre.toLowerCase();
    var turismoLead = "LICENCIAURA INTERACIONAL EN TURISMO Y REUNIONES";
    turismoLead = turismoLead.toLowerCase();
    var gastronomiaLead = "LICENCIATURA EN GASTRONOMÍA";
    gastronomiaLead = gastronomiaLead.toLowerCase();
    if (careerLineaWeb == "licenciatura") {
        if (careerName == "gastronomia" || careerName == "gastronomía" || careerName == gastronomiaLead) {
            careerLineaWeb = "gastronomia";
        } else if (careerName == turismoLead) {
            careerLineaWeb = "turismo";
        }
    } else if (careerLineaWeb == "salud") {
        if (careerName == "enfermeria" || careerName == "enfermería" || careerName == "fisioterapia" || careerName == "nutrición" || careerName == "nutricion" || careerName == "odontología" || careerName == "odontologia") {
            careerLineaWeb = careerName;
        }
    }

    var selectCareer = objIncluye[getRelation(careerLineaWeb)];
    var html = "";
    for (var i = 0; i < selectCareer.length; i++) {
        html += "<li>" + selectCareer[i] + "</li>";
    }

    $('.check').empty();
    $('.check').append(html);

}


Number.prototype.format = function (n, x) {
    var re = '\\d(?=(\\d{' + (x || 3) + '})+' + (n > 0 ? '\\.' : '$') + ')';
    return this.toFixed(Math.max(0, ~~n)).replace(new RegExp(re, 'g'), '$&,');
};

// function muestraPaso(seccion){
//     if(jQuery('body').hasClass('inicio')){
//         jQuery('.quest.p-activa').hide();
//         jQuery('#'+seccion).show().addClass('p-activa');
//     }

// }



function muestraPaso(actual, seccion) {



    if (jQuery('body').hasClass('inicio')) {                              //Verifica que este extendido el formulario

        var linea_seleccionada = getUsuario().linea.id_name;
        console.log(linea_seleccionada);

            jQuery('.quest.p-activa').addClass('an-slideUp');               //Agrega SlideUp a la Quest Activa

            setTimeout(function () {
                jQuery('.quest.p-activa').addClass('hide');                 //Esconde la preg Activa 
                jQuery('#' + seccion).removeClass('hide').addClass('p-activa').addClass('an-slideDownToUp');
                jQuery('#' + seccion).removeClass('an-slideUp').removeClass('an-slideDownToUp');

            }, 400);

            setTimeout(function () {
                console.log("Estmoas en:" + actual + "Se va a ir a:" + seccion);
                jQuery('#' + actual).removeClass('an-slideUp');
            }, 400);

            switch (seccion) {
                case "carrera":
                    regresaPaso('carrera', 'business-line');
                    break;
            }

        


    }

}

function checkOffset() {
    if ($('#selector-float').offset().top + $('#selector-float').height() >= $('#footer').offset().top)
        $('#selector-float').css('position', 'absolute');
    if ($(document).scrollTop() + window.innerHeight < $('#footer').offset().top)
        $('#selector-float').css('position', 'fixed'); // restore when you scroll up
    // $('.px').text($(document).scrollTop() + window.innerHeight);
}
jQuery(document).scroll(function () {
    //checkOffset();
});

//Triggers
$('#theme-calculadora-nueva-css').prop('disabled',true);
$('#bootstrapcss-css').prop('disabled',true);
$('#mdb-css').prop('disabled',true);
// $('#theme-calculadora-css').prop('disabled',false);
// $("#aplicativo-calculadora").show();
// $("#todo_calculadora_body").hide();
jQuery(document).ready(function () {

    if(checkCookie('regcompleto')==1){
        jQuery("#aplicativo-calculadora").show();
        jQuery(".calcuApp").show();
        $('#theme-calculadora-nueva-css').prop('disabled', false);
        $('#bootstrapcss-css').prop('disabled',false);
        $('#mdb-css').prop('disabled',false);
        // $('#theme-calculadora-css').prop('disabled',true);
        jQuery("#todo_calculadora_body").hide();
    }

    //INICIALIZACION DE JSONS
    getJsonCarreras();
    getJsonLinksCategorias();
    getJsonBasura();
    getJsonBasuraEmail();
    getJsonCalcCostos();
    getJsonBecasCalc();
    render_lineas_negocio();

    jQuery('#project').focus(function () {
        jQuery(this).val("");
        checkAllInputs();
        on();
    })


    /*******INPUT DE RANGO******/
    jQuery('#range').on("change", function () {
        $('.thumb.active .value').hide();
        var checkRangeSend = $("#range").attr("data-send");
        if (checkRangeSend == "1") {
            $("#range").attr("data-send", "2");
        } else if (checkRangeSend == "2") {
            $("#range").attr("data-send", "2");
        } else {
            $("#range").attr("data-send", "1");
        }
        calcular_uno();
        finaliza();
        console.log('inout de rango 1')
    });

    jQuery(document).on('input change', '#range', function () {
        guardaUsuario('promedio', this.value);
        $('.output').html(this.value);
        console.log('inout de rango 3')

    });


    //   var str = "Hello world, welcome to the universe.";
    // var n = str.indexOf("welcome");

    /********BOTONES SELECT MODALIDAD Y CAMPUS********/
    jQuery('select').not('.form-control').on('change', function () {
        console.log('Guardando modalidad');
        console.log(jQuery('#render_modalidad option:selected').val());



        var dataItem = jQuery('#render_modalidad option:selected').data();
        console.log(dataItem);
        guardaUsuario('modalidad', dataItem);
        jQuery('.rs-mod').text('Modalidad ' + jQuery('#render_modalidad option:selected').text());
        guardaUsuario('campus', jQuery('#render_campus option:selected').val());

        render_selects();
        if (jQuery('#render_campus').val() != 0 && jQuery('#render_modalidad').val() != 0) {
            if(jQuery('#render_campus').val() == "ONL" && jQuery('#render_modalidad').val() != 3){
                on();
            } else {
                off();
            }
            regresoAnterior = false;
            muestraPaso('modalidad', 'promedio');
            $('.steps').text('6');
            // regresaPaso('promedio','carrera');
            if(jQuery('#render_campus').attr('data-preselected')){
                guardaUsuario('campus', jQuery('#render_campus').attr('data-preselected'));
            }else {
                guardaUsuario('campus', jQuery('#render_campus').val());
            }
            jQuery('.rs-campus').text('Campus ' + jQuery('#render_campus option:selected').text());

            console.log("PRUEBA DE ENVÍO");

            var checkOverlaySend = $("#overlay").attr("data-send");
            if(!jQuery('body').hasClass('inicio')){
                if (checkOverlaySend == "1") {
                    $("#overlay").attr("data-send", "2");
                    envioProcWebContinuacion();
                } else if (checkOverlaySend == "2") {
                    $("#overlay").attr("data-send", "2");
                    envioProcWebContinuacion();
                } else {
                    $("#overlay").attr("data-send", "1");
                }
            }
            


            //guardaUsuario('linea', dataItem);  
        }

        var nivelAnterior = getNivelAnterior(getCampo('modalidad').interes);
        jQuery('.rs-canterior').text(nivelAnterior);
        calcular_uno();
    });


    jQuery('.reset').on('click', function () {
        render_modalidades(getModalidades(getCampo('carrera').IdCategoria));
        on();
    });

    jQuery('#render_campus').on('change', function () {
        var a = getModalidades(getCampo("carrera").IdCategoria); // Obtengo de nuevo todas las carreras para filtrar por campus 
        var html = "";
        var campus_select = jQuery('#render_campus').val();
        var modalidad_select = jQuery('#render_modalidad').val();

        var modalidadesSize = a.length;

        if (jQuery('#render_campus').val() != 0) {   //Si Cambia el campus
            html += '<option value="0">Modalidad</option>';
            console.log('cambio campus');
            console.log('Campus Elegido:' + campus_select);
            jQuery.each(a, function (index, value) {
                console.log(a);
                console.log('My array has at position ' + index + ', this value: ' + value.campus);
                console.log(value);
                var c = value.campus.toString();

                if (c.search(campus_select) != -1) {
                    console.log('My array has at position ' + index + ', this value: ' + value.modalidad + modalidadtxt(value.modalidad));
                    html += '<option data-Grupo_carreras="' + value.Grupo_carreras + '" data-IdDynamics="' + value.IdDynamics + '" data-campus="' + value.campus + '"  data-catCosto="' + value.catCosto + '" data-interes="' + value.interes + '" data-lineaweb="' + value.lineaweb + '" data-modalidad="' + value.modalidad + '"  data-nombre="' + value.nombre + '" value="' + value.modalidad + '">' + modalidadtxt(value.modalidad) + '</option>';
                };
            });
            jQuery('#render_modalidad').html(html).delay(500).queue(function (next) {
                if (modalidadesSize == 1) {
                    jQuery('#render_modalidad').next().addClass('inactive');
                } else if (modalidadesSize > 1) {
                    jQuery('#render_modalidad').next().removeClass('inactive');
                }
                next();
            });
            jQuery('#render_modalidad option[value="' + modalidad_select + '"]').attr('selected', 'selected');


        }
    });



    jQuery('#render_modalidad').on('change', function () {
        var a = getModalidades(getCampo("carrera").IdCategoria); // Obtengo de nuevo todas las carreras para filtrar por campus 
        var html = "";
        var campus_select = jQuery('#render_campus').val();
        var modalidad_select = jQuery('#render_modalidad').val();
        var sizeCampus = 0;
        var campusValue = "";

        if (jQuery('#render_modalidad').val() != 0) { //si cambia modalidad pinta campus 
            var campus = [];
            var html2 = "";
            jQuery.each(a, function (index, value) {
                console.log(value.modalidad + " Busca modalidad:" + modalidad_select);
                if (value.modalidad == modalidad_select) {
                    console.log('Encontro');
                    console.log(value);
                    console.log("campus encontrados");
                    var ArrCamp = value.campus.toString().split(",");
                    sizeCampus = ArrCamp.length;
                    if(ArrCamp.length == 1){
                        jQuery('#render_campus').attr('data-preselected',ArrCamp[0]);
                        html2 += '<option selected value="' + ArrCamp[0] + '">' + campustxt(ArrCamp[0]) + '</option>';
                    } else {
                        html2 += '<option value="0">Campus</option>';
                        jQuery('#render_campus').removeAttr('data-preselected');
                        jQuery.each(ArrCamp, function (index1, value1) {
                            console.log(value1);
                            html2 += '<option value="' + value1 + '">' + campustxt(value1) + '</option>';
                        });
                    }
                }
            }); //Guarda los arreglos de campus

            jQuery('#render_campus').html(html2).delay(200).queue(function (next) {
                // if(campus_select != "" || campus_select != undefined){
                //     jQuery('#render_campus option[value="' + campus_select + '"]').attr('selected', 'selected');
                // } else {
                //     jQuery('#render_campus option:first').next().attr('selected', 'selected');
                // }
                console.log("El valor de los campus es de: " + sizeCampus);
                if(sizeCampus == 1){
                    jQuery('#render_campus').next().addClass('inactive');
                    muestraPaso('modalidad', 'promedio');
                    regresoAnterior = false;
                    off();
                    calcular();
                    guardaUsuario('campus', jQuery('#render_campus').attr('data-preselected'));
                    var checkOverlaySend = $("#overlay").attr("data-send");
                    if(!jQuery('inicio').hasClass('inicio')){
                        if (checkOverlaySend == "1") {
                            $("#overlay").attr("data-send", "2");
                            envioProcWebContinuacion();
                        } else if (checkOverlaySend == "2") {
                            $("#overlay").attr("data-send", "2");
                            envioProcWebContinuacion();
                        } else {
                            $("#overlay").attr("data-send", "1");
                        }
                    }
                    
                } else {
                    jQuery('#render_campus').next().removeClass('inactive');
                }
                next();
            });

            jQuery('#render_campus option[value="' + campus_select + '"]').attr('selected', 'selected');
            

        }


        // if(jQuery('select.ipt-b').eq(0).val() != '0' && jQuery('select.ipt-b').eq(1).val()!='0'){

        //     muestraPaso('modalidad','promedio');
        // } 

    });

    /* Trigger (Linea de Negocio)
    * 
    */
    jQuery(".card.card-calculadora").on("mouseup", function () {
        var dataItem = jQuery(this).find("div").data();       //Obtiene todo el data del primer div 
        select(".card", this);                               //this=card-body 
        guardaUsuario('linea', dataItem);                   //guarda en LocalStorage
        var a = getCarrerasBien(getCarreras(dataItem.id_name));     //Solicita las carreras y las pinta en el select  
        changeVideo(dataItem.id_name.toLowerCase());
        render_carreras(a);
        
        if(dataItem.id_name != "PREPARATORIA"){
            muestraPaso('business-line', 'carrera');             //Se va al siguiente paso
            $('.steps').text('4');
            regresaPaso('carrera','business-line');
        }
        if (jQuery('body').hasClass('inicio') == false) {
            on();
            jQuery('.reset').click();
            jQuery('#project').val('');
            jQuery('#project').focus();
            if(dataItem.id_name == "PREPARATORIA"){
                $('.ui-menu-item div').first().click();
            }
            checkAllInputs();
        };
    });

    /********CLICK EN LA ULTIMA ETAPA***********/
    // jQuery("#resultado").on("click", function(){
    //     finaliza();
    // });

    /*******ESQUEMA DE PAGOS CONTADO O MENSUAL********/
    jQuery("body").on("click", ".btn-dual-y", function () {
        select(".btn-dual-y", this);
        jQuery('.rs-tipopago').text(jQuery(this).text());
        if (jQuery(this).text().toLowerCase() == 'mensual') {
            jQuery('.rs-pg-final').text("$" + getCampo('mensual').format());
            jQuery('.rs-ahorro-mns').text("$" + getCampo('mensualAhorro').format());
        } else {
            jQuery('.rs-pg-final').text("$" + getCampo('contado').format());
            jQuery('.rs-ahorro-mns').text("$" + getCampo('contadoAhorro').format());

        }
    });

    /*******PLAN A 3 Y 4 AÑOS********/
    jQuery("body").on("click", ".btn-dual", function () {
        select(".btn-dual", this);
        var duracion = getCampo('costos').duracion;
        guardaUsuario('materias', duracion[jQuery(this).data('termino')].materias);
        jQuery('.btn-circ').removeClass("selected");
        var mat = getCampo('materias');
        console.log(mat);
        jQuery('.btn-circ').eq(mat - 1).addClass('selected');
        calcular();

    });

    /*******NUMERO DE MATERIAS********/
    jQuery("body").on("click", ".btn-circ", function () {
        select(".btn-circ", this);
        jQuery('.btn-dual.selected').removeClass("selected");
        var mat_sel = jQuery(this).text();
        var final;
        jQuery.each(getCampo('costos').duracion, function (key, value) {
            if (value.materias == mat_sel) { final = key; }
        });


        jQuery('[data-termino="' + final + '"]').addClass('selected');
        guardaUsuario('materias', jQuery(this).text());
        calcular();
    });

    /*******INPUT DE BUSQUEDA DE CARRERAS********/
    jQuery('#project').on("focus", function () {
        $('#project').autocomplete('search', $('#project').val())
    });



});

function regresaPaso(actual,movingTo) {
    if(jQuery('body').hasClass('inicio')){
        if(actual == 'promedio' && regresoAnterior == false){
            console.log('Omito todo');
    
            /* Se implementa el regreso a carrera */
            jQuery('.controls').removeClass('d-none');
            jQuery('.controls a').on('click',function(){
                muestraPaso('promedio','carrera');
            });
            $('.steps').text('6');
        } else {
            console.log("Moviendo a "+ movingTo);
            switch(movingTo) {
                case "business-line":
                    jQuery('.controls').removeClass('d-none');
                    $('.steps').text('4');
                    jQuery('.controls a').on('click',function(){
                        muestraPaso(actual,movingTo);
                        jQuery('.controls').addClass('d-none');
                    });
                break;
                case "carrera":
                    jQuery('.controls').removeClass('d-none');
                    jQuery('.controls a').on('click',function(){
                        muestraPaso(actual,movingTo);
                    });
                break;
                default:
                    jQuery('.controls').removeClass('d-none');
                    jQuery('.controls a').on('click',function(){
                        muestraPaso(actual,movingTo);
                    });
                break;
            }
        }
    }
    
    
}


function select(clase, el) {
    $(clase).each(function () { $(this).removeClass('selected') });
    if(!pasoFinalCalculadora){
        setTimeout(function(){$(el).addClass('selected');},500);
    } else {
        $(el).addClass('selected');
    }
    
    
}

function on() {
    //document.getElementById("overlay").style.display = "block";
    jQuery('#results').addClass('blur');
    jQuery('#overlay').fadeIn();
}

function off() {
    //document.getElementById("overlay").style.display = "none";
    jQuery('#results').removeClass('blur');
    jQuery('#overlay').fadeOut();
}

function checkAllInputs() {
    /* Controla todos los inputs que faltan */
    if(jQuery('#project').val() != ""){
        jQuery('#calc-apply-career').hide();
        jQuery('#project').removeClass('missing-select');
    } else{
        jQuery('#calc-apply-career').show();
        jQuery('#project').addClass('missing-select');
    }

    if(jQuery('#render_campus').val() != 0){
        jQuery('#calc-apply-campus').hide();
        jQuery('#render_campus').next().removeClass('missing-select');
        jQuery('.rs-campus').text('Campus ' + jQuery('#render_campus option:selected').text());
    } else{
        jQuery('#calc-apply-campus').show();
        jQuery('#render_campus').next().addClass('missing-select');
    }

    if(jQuery('#render_modalidad').val() != 0){
        jQuery('#calc-apply-type').hide();
        jQuery('#render_modalidad').next().removeClass('missing-select');
        jQuery('.rs-mod').text('Modalidad ' + jQuery('#render_modalidad option:selected').text());
    } else{
        jQuery('#calc-apply-type').show();
        jQuery('#render_modalidad').next().addClass('missing-select');
    }
}

function cerrarModalCalculadora() {
    $('#calculadora-css').attr('disabled', 'disabled');
    $('#calculadora-css').prop("disabled",true);
    $('#bootstrapcss-css').removeAttr('disabled');
    $('#mdb-css').removeAttr('disabled');
    $('#bootstrapcss-css').prop('disabled',false);
    $('#mdb-css').prop('disabled',false);

    if(yCalculadora != 0){
        console.log("Cambiando scroll en y " + yCalculadora);
        setTimeout(function(xCalculadora,yCalculadora){window.scrollTo(xCalculadora,yCalculadora);},700,xCalculadora,yCalculadora);
    }
}



/* SEGUNDA PARTE JS */

var urlJsonBasura = "/wp-content/themes/temaunitec/assets/frontend/json/min/json_basura.min.json";
var urlJsonBasuraEmail = "/wp-content/themes/temaunitec/assets/frontend/json/min/json_basuraEmail.min.json";
var urlJsonCarreras = "/wp-content/themes/temaunitec/assets/frontend/json/min/json_calc_carreras.min.json";
//Se agrega nueva url para obtener los links y nombre de las categorias de las carreras
var urlJsonLinksCategorias = "/wp-content/themes/temaunitec/assets/frontend/json/min/json_calc_linkPaginas.min.json";
//Se agregan variables para calculadora
//Costos
var urlJsonCalcCostos = "/wp-content/themes/temaunitec/assets/frontend/json/min/json_calc_costos.min.json";
//Becas
var urlJsonCalcBecas = "/wp-content/themes/temaunitec/assets/frontend/json/min/json_calc_becas.min.json";
var urlwp = "";

function getJsonCarreras() {
    jQuery
        .ajax({
            type: "GET",
            //url: this.urlJsonCarreras,
            url: urlwp + urlJsonCarreras,
            "Content-Type": "application/json",
            dataType: "json",
            success: function (resultado) { }
        })
        .done(function (resultado) {
            localStorage.setItem("jsonCarreras", JSON.stringify(resultado));
        });
}
//Funcion para obtener el json de links y categorias
function getJsonLinksCategorias() {
    jQuery
        .ajax({
            type: "GET",
            //url: this.urlJsonLinksCategorias,
            url: urlwp + urlJsonLinksCategorias,
            "Content-Type": "application/json",
            dataType: "json",
            success: function (resultado) { }
        })
        .done(function (resultado) {
            localStorage.setItem("jsonLinksCategorias", JSON.stringify(resultado));
        });
}
//Funcion para traer el Json de basura con jquery ajax
function getJsonBasura() {
    jQuery
        .ajax({
            type: "GET",
            //url: this.urlJsonBasura,
            url: urlwp + urlJsonBasura,
            "Content-Type": "application/json",
            dataType: "json",
            success: function (resultado) { }
        })
        .done(function (resultado) {
            console.log(resultado);
            localStorage.setItem("jsonBasura", JSON.stringify(resultado));

        });
}
//Funcion para traer el Json de email basura con jquery ajax
function getJsonBasuraEmail() {
    jQuery
        .ajax({
            type: "GET",
            //url: this.urlJsonBasuraEmail,
            url: urlwp + urlJsonBasuraEmail,
            "Content-Type": "application/json",
            dataType: "json",
            success: function (resultado) { }
        })
        .done(function (resultado) {
            localStorage.setItem("jsonBasuraEmail", JSON.stringify(resultado));
        });
}
function getJsonCalcCostos() {
    jQuery
        .ajax({
            type: "GET",
            //url: this.urlJsonCarreras,
            url: urlwp + urlJsonCalcCostos,
            "Content-Type": "application/json",
            dataType: "json",
            success: function (resultado) { }
        })
        .done(function (resultado) {
            localStorage.setItem("jsonCostos", JSON.stringify(resultado));
        });
}
//Obtener las becas
function getJsonBecasCalc() {
    jQuery
        .ajax({
            type: "GET",
            //url: this.urlJsonCarreras,
            url: urlwp + urlJsonCalcBecas,
            "Content-Type": "application/json",
            dataType: "json",
            success: function (resultado) { }
        })
        .done(function (resultado) {
            localStorage.setItem("jsonBecas", JSON.stringify(resultado));
        });
}



function finaliza() {
    console.log("_______FUNCION FINALIZA_______");

    getDuracion();

    jQuery('.an-slideUp').each(function () { jQuery(this).removeClass('an-slideUp') }); // QUITA LA ANIMACION PARA QUE CUANDO HAGA RESIZE NO EJECUTE LA ANIMACION DE NUEVO

    jQuery('body').removeClass('inicio');// EJECUTO EL ULTIMO PASO, SE QUITA LA BANDERA PARA QUE NO HAGA ANIMACIONES
    jQuery('.cont-quest').addClass('containerSecond');
    jQuery('.cont-quest').removeClass('cont-quest');
    jQuery('.imagen-lineas-negocio').addClass('d-none');
    jQuery('.boton-lineas-negocio').addClass('d-none');
    jQuery("#selector-float").animate({ width: '45%' }, function () {
        //jQuery(".output").css(");
        pasoFinalCalculadora = true;
        jQuery('.titlecont').addClass('d-none');
        jQuery('.titlecont').removeClass('d-flex');
        jQuery('.card-calculadora').addClass('w-20');
        jQuery('document').removeClass('an-slideUp');
        jQuery('.containerSecond').removeClass('justify-content-center');
        jQuery('.controls').addClass('d-none');
        jQuery(".hide").fadeIn();
        jQuery(".hide-on-res").hide();
        jQuery(".quest.p-activa").removeClass('p-activa');
        jQuery(".quest").show();
        jQuery('.quest').removeClass('w-80');
        jQuery('.quest').addClass('w-100');
        jQuery(".questB").addClass("d-flex");
        jQuery(".pasos").hide();
        jQuery("#coming-back").removeClass('d-none');

        //SOLO EN UNITEC.MX
        jQuery("#cierra-tu-calculadora-2").addClass('d-none');

        jQuery(".more").click(function(){
            $(".switch-arrow").toggleClass("down-window top-window");

        });
        jQuery('.btn-parpadeante').click(function(e) {
            $(this).attr('href',$(this).attr('href') == '#resumen' ? '#first' : '#resumen');
        });
    });
    //Envio a Proc web
    var checkRangeSend = $("#range").attr("data-send");
    if (checkRangeSend == "1") {
        envioProcWeb();
    } else if (checkRangeSend == "2") {
        envioProcWebContinuacion();
    }

}

function readCookie(name) {
    var nameEQ = name + "=";
    var ca = document.cookie.split(';');
    for (var i = 0; i < ca.length; i++) {
        var c = ca[i];
        while (c.charAt(0) == ' ') c = c.substring(1, c.length);
        if (c.indexOf(nameEQ) == 0) return c.substring(nameEQ.length, c.length);
    }
    return null;
}

function obtenerTextoCampus(campus) {
    switch (campus) {
        case 'ATZ': return "Atizapán"; break;
        case 'MAR': return "Marina"; break;
        case 'ECA': return "Ecatepec"; break;
        case 'GDL': return "Guadalajara"; break;
        case 'LEO': return "León"; break;
        case 'SUR': return "Sur"; break;
        case 'CUI': return "Cuitláhuac"; break;
        case 'TOL': return "Toluca"; break;
        case 'QRO': return "Querétaro"; break;
        case 'REY': return "Los Reyes"; break;
        case 'ONL': return "En Línea"; break;

    }
}

function envioProcWeb() {
    var seleccionJson = JSON.parse(localStorage.getItem("jsonUsr"));
    var seleccionCookie = JSON.parse(decodeURIComponent(readCookie('c_form_data')));
    var bannerCalculadora = jQuery("#frm_banner").val();

    var seleccionNivelOferta = "";
    var seleccionHerramienta = "";
    var seleccionCampus = obtenerTextoCampus(seleccionJson.campus);

    if (seleccionJson.linea.id_name == "PREPARATORIA") {
        seleccionNivelOferta = "P";
        seleccionHerramienta = "ASPIRANTES PREPA";
    } else {
        seleccionNivelOferta = "L";
        seleccionHerramienta = "ASPIRANTES LIC";
        if (seleccionJson.modalidad.interes == 4 || seleccionJson.modalidad.interes == 6) {
            seleccionHerramienta = "ONLINEP";
        }
    }

    var objectoSeleccion = {
        nivelOferta: seleccionNivelOferta,
        Herramienta: seleccionHerramienta,
        Campana: 1,
        Origen: seleccionHerramienta,
        Banner: jQuery("#frm_banner").val(),
        Calidad: 3,
        L_Negocio: seleccionJson.linea.abrv,
        F_Nacim: "01/01/2000",
        Ciclo: "19-1",
        Alumno: 0,
        Estad: estadoRandom(jQuery('#frm_celular').val()),
        C_Carrera: seleccionJson.modalidad.iddynamics,
        Carrera: seleccionJson.modalidad.nombre,
        banner_id: bannerCalculadora,
        CP: jQuery("#revalidacion").val(),
        Pobla: seleccionJson.modalidad.interes,
        name_form: "",
        Sexo: "M",
        nom_carrera: "",
        nom_campus: seleccionCampus,
        promedioF: seleccionJson.promedio,
        Pdescuento: seleccionJson.beca,
        nivelF: "L",
        CostoUnico: 0,
        num_carrera: 0,
        costoMensual: 0,
        descuentoF: 0,
        planSelec: "4pagos",
        CostoSelec: seleccionJson.mensual,
        planRecom: "4pagos",
        CostoRecom: seleccionJson.mensual,
        envioB: 1,
        C_Carrera_WP: "",
        Direcc: "WEBCALL",
        Colon: "",
        Nombre2: "",
        email2: "",
        URLreferer: location.href,
        debugs: 1,
        gclid_field: "",
        userId_field: "",
        CID: jQuery('#CID').val(),
        email_unitec: "",
        otraConsulta: 0,
        SERVER_URL: "",
        frm_LadaLenght: "",
        frm_LadaAllow: "",
        alum_2_2: "",
        Nombre: quitaAcentos(jQuery('#frm_nombre').val()),
        Ap_Pat: quitaAcentos(jQuery('#frm_apaterno').val()),
        Ap_Mat:quitaAcentos(jQuery('#frm_amaterno').val()),
        email: formatEmail(jQuery('#frm_correo').val().trim().replace(/ /g, "")),
        informacion_2: 1,
        Estado: estadoRandom(jQuery('#frm_celular').val()),
        masculino: "M",
        TipoTel: "",
        cel_calc: "",
        Telef: jQuery('#frm_celular').val(),
        periodo_interes_sel: "19-1",
        politica: "on",
        promedio: seleccionJson.promedio,
        programa: seleccionJson.modalidad.iddynamics,
        materias: seleccionJson.materias,
        hubspotutk: "",
        padretutor: "No",
        termino: 3,
        pago_mensual_beca: seleccionJson.mensualAhorro,
        formulario_completado: "paso_2",
        form_origen: "Calculadora"
    };

    console.log(objectoSeleccion);
    // console.log("LO QUE ENVIO");
    // console.log(JSON.stringify(objectoSeleccion));
    $
        .ajax({
            type: "POST",
            url: "//www.unitec.mx/wp-content/themes/temaunitec/calculadora/v2_calc/almacen1.php",
            // url: "/wp-content/themes/temaunitec/calculadora/v2_calc/almacen1.php",
            data: objectoSeleccion,
            // dataType: "JSON",
            success: function (res) {
                console.log("Datos Enviados");
                console.log(res);
            },
            error: function (xhr, textStatus, error) {
                console.log(xhr.statusText);
                console.log(textStatus);
                console.log(error);
            }
        });
}

function envioProcWebContinuacion() {
    var seleccionJson = JSON.parse(localStorage.getItem("jsonUsr"));
    var seleccionCookie = JSON.parse(decodeURIComponent(readCookie('c_form_data')));
    var bannerCalculadora = jQuery("#frm_banner").val();

    var seleccionNivelOferta = "";
    var seleccionHerramienta = "";
    var seleccionCampus = obtenerTextoCampus(seleccionJson.campus);

    if (seleccionJson.linea.id_name == "PREPARATORIA") {
        seleccionNivelOferta = "P";
        seleccionHerramienta = "ASPIRANTES PREPA";
    } else {
        seleccionNivelOferta = "L";
        seleccionHerramienta = "ASPIRANTES LIC";
        if (seleccionJson.modalidad.interes == 4 || seleccionJson.modalidad.interes == 6) {
            seleccionHerramienta = "ONLINEP";
        }
    }

    var objectoSeleccion = {
        nivelOferta: seleccionNivelOferta,
        Herramienta: seleccionHerramienta,
        Campana: 1,
        Origen: seleccionHerramienta,
        Banner: bannerCalculadora,
        Calidad: 3,
        L_Negocio: seleccionJson.linea.abrv,
        F_Nacim: "01/01/2000",
        Ciclo: "19-1",
        Alumno: 0,
        Estad: estadoRandom(jQuery('#frm_celular').val()),
        C_Carrera: seleccionJson.modalidad.iddynamics,
        Carrera: seleccionJson.modalidad.nombre,
        banner_id: bannerCalculadora,
        CP: jQuery("#revalidacion").val(),
        Pobla: seleccionJson.modalidad.interes,
        name_form: "",
        Sexo: "M",
        nom_carrera: "",
        nom_campus: seleccionCampus,
        promedioF: seleccionJson.promedio,
        Pdescuento: seleccionJson.beca,
        nivelF: "L",
        CostoUnico: 0,
        num_carrera: 0,
        costoMensual: 0,
        descuentoF: 0,
        planSelec: "4pagos",
        CostoSelec: seleccionJson.mensual,
        planRecom: "4pagos",
        CostoRecom: seleccionJson.mensual,
        envioB: 1,
        C_Carrera_WP: "",
        Direcc: "WEBCALL",
        Colon: "",
        Nombre2: "",
        email2: "",
        URLreferer: location.href,
        debugs: 1,
        gclid_field: "",
        userId_field: "",
        CID: jQuery('#CID').val(),
        email_unitec: "",
        otraConsulta: 1,
        SERVER_URL: "",
        frm_LadaLenght: "",
        frm_LadaAllow: "",
        alum_2_2: "",
        Nombre: quitaAcentos(jQuery('#frm_nombre').val()),
        Ap_Pat: quitaAcentos(jQuery('#frm_apaterno').val()),
        Ap_Mat:quitaAcentos(jQuery('#frm_amaterno').val()),
        email: formatEmail(jQuery('#frm_correo').val().trim().replace(/ /g, "")),
        informacion_2: 1,
        Estado: estadoRandom(jQuery('#frm_celular').val()),
        masculino: "M",
        TipoTel: "",
        cel_calc: "",
        Telef: jQuery('#frm_celular').val(),
        periodo_interes_sel: "19-1",
        politica: "on",
        promedio: seleccionJson.promedio,
        programa: seleccionJson.modalidad.iddynamics,
        materias: seleccionJson.materias,
        hubspotutk: "",
        padretutor: "No",
        termino: 3,
        pago_mensual_beca: seleccionJson.mensualAhorro,
        formulario_completado: "paso_2",
        form_origen: "Calculadora"
    };

    console.log(objectoSeleccion);

    $
        .ajax({
            type: "POST",
            url: "//www.unitec.mx/wp-content/themes/temaunitec/calculadora/v2_calc/otra_consulta.php",
            // url: "/wp-content/themes/temaunitec/calculadora/v2_calc/otra_consulta.php",
            data: objectoSeleccion,
            // dataType: "JSON",
            success: function (res) {
                console.log("Datos Enviados");
                console.log(res);
            },
            error: function (xhr, textStatus, error) {
                console.log(xhr.statusText);
                console.log(textStatus);
                console.log(error);
            }
        });
}



/* FUNCIONES DE LINEA DE NEGOCIO
 * getLineasNegocio         -> Consulta JSON
 * render_lineas_negocio    -> Pinta todos los divs 
 * set_linea_negocio        -> Cuando el usuario selecciona una linea de negocio
*/
function getLineasNegocio() {
    var lineasNegocio =
        [{ "name": "Preparatoria", "id_name": "PREPARATORIA", "order": "", "abrv": "PREPA" },
        { "name": "Licenciatura", "id_name": "LICENCIATURA", "order": "", "abrv": "UG" },
        { "name": "Ingeniería", "id_name": "INGENIERIA", "order": "", "abrv": "ING" },
        { "name": "Lic. en Salud", "id_name": "SALUD", "order": "", "abrv": "CS" },
        { "name": "Posgrado", "id_name": "POSGRADO", "order": "1", "abrv": "POS" }];
    return lineasNegocio;
}
function render_lineas_negocio() {
    var lineas = getLineasNegocio();
    jQuery.each(lineas, function (index, value) {
        console.log('My array has at position ' + index + ', this value: ' + value.name);
        var html = '<div class="card card-calculadora"><div data-name="' + value.name + '" data-id_name="' + value.id_name + '" data-abrv="' + value.abrv + '" class="card-body tc w-100"><h5 class="mt-0" style="letter-spacing: 0px !important;">' + value.name + '</h5><div class="imagen-lineas-negocio imagen-lineas-cuadros cuadro ' + value.abrv + '"></div><button class="btn btn-dtc boton-lineas-negocio waves-effect waves-light"  role="button">Selecciona</button></div></div>';
        jQuery('#render_lineas_negocio').append(html);
    });
}

function getCarreras(linea, campus) {
    var keySearch = "lineaweb";
    var arrayCarrerasPorLinea = [];
    var objJsonCarreras = JSON.parse(localStorage.getItem("jsonCarreras"));
    arrayCarrerasPorLinea = this.buscar(
        keySearch,
        linea,
        objJsonCarreras
    );

    return arrayCarrerasPorLinea
}

function getCarrerasBien(categorias) {
    var objJsonCarreras = JSON.parse(localStorage.getItem("jsonLinksCategorias"));
    var categoriasno = new Array();
    Object.entries(categorias).forEach(function (key, value) {
        categoriasno.push(key[1]['Grupo_carreras']);
    });
    //console.log(categoriasno);
    var nombres = new Array();
    Object.entries(objJsonCarreras).forEach(function (key, value) {
        if (categoriasno.includes(key[1]['IdCategoria'])) {
            nombres.push(key[1]);
        }
    });
    return nombres;
}


/* FUNCIONES DE CARRERA
 * render_carreras         -> Pinta las carreras en el autocomplete 
*/
function render_carreras(carreras) {
    // console.log('las carreras');
    // console.log(carreras);
    var carrerasjson = [];
    jQuery.each(carreras, function (index, value) {
        //console.log(value);
        console.log('My array has at position ' + index + ', this value: ' + value.IdCategoria);
        item = {}
        item["value"] = value.IdCategoria;
        item["label"] = value.Categoria;
        //item["desc"] = value.Categoria;
        item["icon"] = value.IdDynamics;
        item["Categoria"] = value.Categoria;
        item["IdCategoria"] = value.IdCategoria;
        item["Link"] = value.Link;
        carrerasjson.push(item);
    });

    //Manejo de las carreras cuando es prepa
    var linea_seleccionada = getUsuario().linea.id_name;
    if(linea_seleccionada == "PREPARATORIA"){
        guardaUsuario('carrera', carrerasjson[0]);
        jQuery('.rs-carrera').text(getCampo('carrera').Link.replace(/-/g, ' ').replace(/\//g, ''));
        render_modalidades(getModalidades(carrerasjson[0].IdCategoria));
        muestraPaso('business-line', 'modalidad');
        regresaPaso('modalidad','business-line');
        $('.steps').text('5');
    }


    $("#project").autocomplete({
        minLength: 0,
        source: carrerasjson,
        focus: function (event, ui) {
            $("#project").val(ui.item.label);
            return false;
        },
        select: function (event, ui) {
            console.log(ui);
            $(this).blur();
            $("#project").val(ui.item.label);
            $("#project-id").val(ui.item.value);
            //$("#project-description").html(ui.item.desc);
            $("#project-icon").attr("src", "images/" + ui.item.icon);
            //let oferta =[{ "Categoria": ui.item.Categoria, "IdCategoria": ui.item.IdCategoria, "Link": ui.item.Link }];
            guardaUsuario('carrera', ui.item);
            // jQuery('.rs-carrera').text(getCampo('carrera').Link);
            jQuery('.rs-carrera').text(getCampo('carrera').Link.replace(/-/g, ' ').replace(/\//g, ''));
            render_modalidades(getModalidades(ui.item.IdCategoria));

            //render_campus(getModalidades(ui.item.IdCategoria));
            // console.log(getCarreras(getCampo("carrera").IdCategoria));

            // var campus=alert()("modalidad").campus.toString().split(",").sort();
            // console.log(campus);

            muestraPaso('carrera', 'modalidad');
            regresaPaso('modalidad','carrera');
            $('.steps').text('5');
            return false;
        }
    })
        .autocomplete("instance")._renderItem = function (ul, item) {
            return $("<li>")
                .append("<div>" + item.label + "</div>")
                //.append("<div>" + item.label + "<br>" + item.desc + "</div>")
                .appendTo(ul);
        };


}

/* FUNCIONES DE MODALIDADES
* getModalidades         -> OBTIENE MODALIDADES Y RECIBE EL ID DE CATEGORIA
* render_MODALIDADES  -> Obtienes las modalidades y las pinta
*/

function getModalidades(categoria) {
    var keySearch = "Grupo_carreras";
    var arrayCarrerasPorLinea = [];
    var objJsonCarreras = JSON.parse(localStorage.getItem("jsonCarreras"));
    arrayCarrerasPorLinea = buscar(
        keySearch,
        categoria,
        objJsonCarreras
    );

    return arrayCarrerasPorLinea
}

function render_modalidades(modalidades) {
    console.log('render_modalidades');
    console.log(modalidades);
    console.log('Existen:' + modalidades.length);
    changeAddInCareers(modalidades[0]);

    var modalidadesSize = modalidades.length;
    /*******MODALIDADES******/
    if (modalidades.length == 1) {
        var html = '<option selected data-Grupo_carreras="' + modalidades[0].Grupo_carreras + '" data-IdDynamics="' + modalidades[0].IdDynamics + '" data-campus="' + modalidades[0].campus + '"  data-catCosto="' + modalidades[0].catCosto + '" data-interes="' + modalidades[0].interes + '" data-lineaweb="' + modalidades[0].lineaweb + '" data-modalidad="' + modalidades[0].modalidad + '"  data-nombre="' + modalidades[0].nombre + '" value="' + modalidades[0].modalidad + '">' + modalidadtxt(modalidades[0].modalidad) + '</option>';
    } else {
        console.log('mas de 3');
        var html = '<option value="0">Modalidad</option>';
        jQuery.each(modalidades, function (index, value) {
            console.log('My array has at position ' + index + ', this value: ' + value.modalidad + modalidadtxt(value.modalidad));
            html += '<option data-Grupo_carreras="' + value.Grupo_carreras + '" data-IdDynamics="' + value.IdDynamics + '" data-campus="' + value.campus + '"  data-catCosto="' + value.catCosto + '" data-interes="' + value.interes + '" data-lineaweb="' + value.lineaweb + '" data-modalidad="' + value.modalidad + '"  data-nombre="' + value.nombre + '" value="' + value.modalidad + '">' + modalidadtxt(value.modalidad) + '</option>';
        });
    }
    jQuery('#render_modalidad').html(html).delay(200).queue(function (next) {
        if (modalidadesSize == 1) {
            jQuery('#render_modalidad').next().addClass('inactive');
        } else if (modalidadesSize > 1) {
            jQuery('#render_modalidad').next().removeClass('inactive');
        }
        next();
    });

    /*******CAMPUS******/
    console.log('todos los campus que tienen esta carrera');
    var campus = [];
    jQuery.each(modalidades, function (index, value) { campus.push(value.campus); }); //Guarda los arreglos de campus
    var resultArray = Array.prototype.concat.apply([], campus);
    allcampus = resultArray.join(',');
    var b = unique(allcampus.split(",")); // Array de campus unicos 

    var html = '<option value="0"> Campus</option>';
    if(b.length == 1){
        html += '<option selected value="' + b[0] + '">' + campustxt(b[0]) + '</option>';
    } else {
        jQuery.each(b, function (index, value) {
            html += '<option value="' + value + '">' + campustxt(value) + '</option>';
        });
    }
    
    jQuery('#render_campus').html(html).delay(300).queue(function(next){
        if(b.length == 1){
            jQuery('#render_campus').next().addClass('inactive');
            if(jQuery('#render_modalidad').next().hasClass('inactive')){
                regresoAnterior = false;
                setTimeout(function(){muestraPaso('modalidad', 'promedio'); regresoAnterior = false;},500);
                // muestraPaso('modalidad', 'promedio');
                guardaUsuario('campus', jQuery('#render_campus').val());
                guardaUsuario('modalidad', jQuery('#render_modalidad option:selected').data());
                off();
                var checkOverlaySend = $("#overlay").attr("data-send");
                if (checkOverlaySend == "1") {
                    calcular_uno();
                    if(!jQuery('inicio').hasClass('inicio')){
                        $("#overlay").attr("data-send", "2");
                        // $('#render_campus').next().next().children().first().click();
                        envioProcWebContinuacion();
                    }
                } else if (checkOverlaySend == "2") {
                    calcular_uno();
                    if(!jQuery('inicio').hasClass('inicio')){
                        $("#overlay").attr("data-send", "2");
                        // $('#render_campus').next().next().children().first().click();
                        envioProcWebContinuacion();
                    }
                } else {
                    if(!jQuery('inicio').hasClass('inicio')){
                        $("#overlay").attr("data-send", "1");
                    }
                }
            }
            // $('#render_campus').next().next().children().first().click();
        } else {
            jQuery('#render_campus').removeAttr('data-preselected');
            jQuery('#render_campus').next().removeClass('inactive');
        }
        next();
    });
    render_selects();
}


function calcular_uno() {
    console.log("_______FUNCION CALCULAR UNO_____");
    /***Funciones por defecto en resutlado.component.html primera carga ***/
    var campus = getCampo("campus");
    console.log('El campus escogido');
    console.log(campus);
    var catcosto = getCampo("modalidad").catcosto;
    console.log(catcosto);
    //se tienen que habilitar en este orden las funciones
    var costos = getCampoCosto(catcosto); // 1.-
    guardaUsuario('costos', costos);
    console.log("costos:");
    console.log(costos);
    var materias = parseInt(costos['duracion'][Object.keys(costos['duracion'])[0]]["materias"]);
    guardaUsuario('materias', materias);
    var limitMaterias = parseInt(costos['duracion'][Object.keys(costos['duracion'])[0]]["materias"]);
    if (campus == 'QRO' && catcosto == 'LIC') {
        materias = parseInt(costos['duracion'][Object.keys(costos['duracion'])[0]]["materias"]) * 1 + 1;
        limitMaterias = parseInt(costos['duracion'][Object.keys(costos['duracion'])[0]]["materias"]) * 1 + 1;

    }
    console.log(limitMaterias);
    console.log('Colocando materias:' + materias);
    console.log('El limite de materias es:' + limitMaterias);
    //terminan funciones de primera carga 


    var btnmaterias = "";
    for (i = 1; i <= limitMaterias; i++) {
        if (i == limitMaterias) { var sel = "selected"; }
        btnmaterias += '<div class="btn-circ ' + sel + '">' + i + '</div>';
    }

    if (limitMaterias > 7) {
        btnmaterias = '<div class=" alert alert-primary ' + sel + '"> ' + i + ' materias</div>';
    }

    jQuery('.mat-circ').html(btnmaterias);

    // if(getCampo('materias')){

    // }else{
    //     guardaUsuario('materias',materias);
    // }
    getDuracion();
}






function getDuracion() {
    console.log('______FUNCION GETDURACION_____');
    var materias = getCampo('materias');
    var campus = getCampo('campus');
    var catcosto = getCampo('modalidad').catCosto;
    var terminando = void 0;
    var valoranios = void 0;
    var materiasanios = void 0;
    //let aniosRango:any;
    var aniosRango = new Array();
    var materiasMaximo = void 0;
    var lasMaterias = materias;
    var costos = getCampo('costos');
    console.log("costos['duracion']");
    console.log(costos['duracion']);

    if (campus == 'QRO' && catcosto == 'LIC') {
        Object.entries(costos['duracion']).forEach(function (key, value) {
            console.log("Con " + key[1]['materias'] * 1 + 1 + " materias terminas en " + key[0] + " años");
            console.log(key[0]);
            console.log(key[1]['materias'] * 1 + 1);

            valoranios = key[0];
            materiasanios = parseInt(key[1]['materias']) + 1;
            console.log("lasmat: " + lasMaterias);
            if (materiasanios == lasMaterias) {
                console.log('Estaras terminando en ' + valoranios);
                terminando = valoranios;
            }

            aniosRango.push(valoranios);
        });
    } else {
        Object.entries(costos['duracion']).forEach(function (key, value) {
            console.log("Con " + key[0] + " materias terminas en " + key[1]['materias'] + " años");
            console.log(key[0]);
            console.log(key[1]['materias']);

            valoranios = key[0];
            materiasanios = key[1]['materias'];
            console.log("lasmat: " + lasMaterias);
            if (materiasanios == lasMaterias) {
                console.log('Estaras terminando en ' + valoranios + " Mateiras anios" + materiasanios);
                terminando = valoranios;
            }

            aniosRango.push(valoranios);

        });
    }

    aniosRango = aniosRango;
    console.log("temrinando" + terminando);
    console.log(aniosRango);
    var durahtml = "";
    var i = "";
    jQuery.each(aniosRango, function (index, value) {
        if (durahtml == "") { var sel = "selected" }
        durahtml += '<button class="bl btn-dual ' + sel + '" data-termino="' + value + '">' + value + ' años</button>';
        i = value;
    });

    if (aniosRango.length < 2) {
        durahtml = '<div class=" alert alert-primary "> ' + i + ' Años</div>';
    }
    //guardaUsuario('materias',jQuery('.btn-dual.selected'));
    jQuery('.aniosduracion').html(durahtml);



    duracionAnios = terminando;
    console.log("duracionAnios:");
    console.log(duracionAnios);
    if (!duracionAnios) {
        console.log('No hay duracion para las materias escogidas');
    }
    calcular();

}


function getBecaJSON() {
    return JSON.parse(localStorage.getItem("jsonBecas"));
}

function getCampoBeca(campo) {
    var beca = getBecaJSON();
    return beca[0][campo];
}


function getBeca(promedio, catcosto) {
    console.log("obteniendo la beca");
    var setbeca = getCampoBeca(catcosto);
    var campus = getCampo('campus');
    console.log('buscando beca de campus' + campus);
    var becacampus = setbeca;
    console.log(becacampus);
    var RangoBecas = "";
    //console.log(becacampus['LEO']);
    if (becacampus[campus]) {
        console.log('Si eocntro un nodo con' + campus);
        RangoBecas = becacampus[campus];


    } else {
        console.log('NO encontro un nodo' + campus + ' y usara el de TODOS');
        RangoBecas = becacampus['TODOS'];
    }


    console.log(RangoBecas);
    var becaFinal;
    // for (let entry of RangoBecas) {
    //   console.log("aa"+entry); // 1, "string", false
    // }
    console.log('buscando este promedio' + promedio);
    Object.entries(RangoBecas).forEach(function (key, value) {
        console.log(key[1]['promedio'][0] + "<=" + promedio + "&&" + key[1]['promedio'][1] + ">=" + promedio);
        if (key[1]['promedio'][0] <= promedio && key[1]['promedio'][1] >= promedio) {
            console.log("SI_ENTRA_BECA_entro en este porcentaje de beaca");
            console.log(key[1]['promedio'][0] + " " + key[1]['promedio'][1] + "con beca de " + key[1]['beca']);
            becaFinal = key[1]['beca'];

        } else {
            console.log('no encuentra beca');
            //console.log('No es de este rango porque ' + key[1]['promedio'][0] +"no es menor o igual que"+this.promedio +"y " );
        }
    });

    if (becaFinal) { becaFinal = becaFinal; } else { becaFinal = 0; }

    console.log('La beca fue de' + becaFinal);
    //return becacampus[''];

    guardaUsuario('beca', becaFinal);
    guardaUsuario('promedio', promedio);

    if (campus == 'GDL') {
        if (
            getCampo('modalidad').IdDynamics == "787" ||
            getCampo('modalidad').IdDynamics == "883" ||
            getCampo('modalidad').IdDynamics == "884" ||
            getCampo('modalidad').IdDynamics == "588"
        ) {
            console.log('No aplica beca GDL Fundador');
        } else {
            console.log('venia ' + becaFinal);
            becaFinal = parseInt(becaFinal) + 20;
            console.log('Temrino con beca de' + becaFinal);
        }

    }

    return becaFinal;

}

function getMinimoMatBeca(catCosto) {
    var beca;
    switch (catCosto) {
        case "DIP": case "DIPON": case "POS": case "POSCUAT": case "POSCUATS": case "POSON": case "POSONCUAT":
            beca = 3;
            break;
        default:
            beca = 5;
    }
    return beca;
}



function calcular() {
    var campus = getCampo('campus');
    var costos = getCampo('costos');
    var catcosto = getCampo('modalidad').catcosto;
    var materias = getCampo('materias');
    var promedio = Math.round(jQuery('#range').val() * 10) / 10;

    if(campus == undefined || campus == null || campus == "" || campus == "0"){
        campus = jQuery('#render_campus').attr('data-preselected');
    }

    //Obtiene el costo por materia 
    // Costo de la materia X Costo de materia en campus
    console.log("el campus" + campus);
    console.log(costos);
    console.log('Duraciones disponibles');

    // console.log(this.materias);

    // if (this.materias != materiasanios){

    // }else{
    //   this.duracionAnios = valoranios;
    //   this.materias = materiasanios;
    // }

    var costoXmateria = costos["costoMateria"]["1pago"][campus]; //COSTO POR MATERIA DEL CAMPUS SELECCIONADO
    var costoLista = materias * costoXmateria;

    //Detecta a partir de cuanto se crea la beca 
    var minimoMateriasParaBeca = getMinimoMatBeca(catcosto);
    console.log('El minimo para beca es:' + minimoMateriasParaBeca + " y tengo:" + materias);

    if (materias >= minimoMateriasParaBeca) {
        console.log('si aplica beca');
        beca = getBeca(promedio, catcosto);

    } else {
        beca = 0;
        console.log('no aplica beca');

    }
    console.log('tomando el costo por materia');

    /*PAGOS MENSUALES 4 APGOS */
    var costoXmateriaMens = costos['costoMateria']['4pagos'][campus]; //TODO: revisar si hay nodo de campus, sino usar todos
    var PagoSinBecaMensual = costoXmateriaMens * materias;
    console.log('Pago sin descuento' + PagoSinBecaMensual);
    var PagoConBeca = Math.round(PagoSinBecaMensual - ((beca * PagoSinBecaMensual) / 100));
    var pagoConSR = PagoSinBecaMensual - ((beca * PagoSinBecaMensual) / 100);




    jQuery('.rs-beca').text(beca + "%*");
    console.log('con el ' + beca + " de " + PagoSinBecaMensual + " Queda: " + pagoConSR);
    var AhorroMensual = Math.round(PagoSinBecaMensual - pagoConSR);
    console.log('Te ahorras:' + AhorroMensual);

    // if(jQuery('.btn-dual-y.selected').text().toLowerCase()=='mensual'){
    //         jQuery('.rs-pg-final').text("$"+getCampo('mensual').format());
    //         jQuery('.rs-ahorro-mns').text("$"+getCampo('mensualAhorro').format());
    //     }else{
    //         jQuery('.rs-pg-final').text("$"+getCampo('contado').format());
    //         jQuery('.rs-ahorro-mns').text("$"+getCampo('contadoAhorro').format());

    //     }


    /* PAGOS DE CONTADO*/
    var costoXmateriaContado = costos['costoMateria']['1pago'][campus]; //TODO: revisar si hay nodo de campus, sino usar todos
    var PagoSinBecaContado = costoXmateriaContado * materias;
    console.log('Pago sin descuento' + PagoSinBecaContado);
    var PagoConBecaContado = Math.round(PagoSinBecaContado - ((beca * PagoSinBecaContado) / 100));
    var PagoConBecaContadoSR = PagoSinBecaContado - ((beca * PagoSinBecaContado) / 100);
    console.log('con el ' + beca + " de " + PagoSinBecaContado + " Queda: " + PagoConBecaContadoSR);
    var AhorroContado = Math.round(PagoSinBecaContado - PagoConBecaContadoSR);
    console.log('Te ahorras:' + AhorroContado);

    guardaUsuario('costos', costos);
    guardaUsuario('materias', materias);
    guardaUsuario('mensual', pagoConSR);
    guardaUsuario('mensualAhorro', AhorroMensual);

    guardaUsuario('contado', PagoConBecaContadoSR);
    guardaUsuario('contadoAhorro', AhorroContado)

    if (jQuery('.btn-dual-y.selected').text().toLowerCase() == "mensual") {
        jQuery('.rs-pg-final').text("$" + (Math.round(pagoConSR * 100) / 100).format());
        jQuery('.rs-ahorro-mns').text("$" + (Math.round(AhorroMensual * 100) / 100).format());
    } else {
        jQuery('.rs-pg-final').text("$" + (Math.round(PagoConBecaContadoSR * 100) / 100).format());
        jQuery('.rs-ahorro-mns').text("$" + (Math.round(AhorroContado * 100) / 100).format());
    }

    var nivelAnterior = getNivelAnterior(getCampo('modalidad').interes.toString());


}

function getNivelAnterior(nivel) {
    var nivelAnt = "";
    switch (nivel) {
        case 1: case 2: case 3: case 4:
            nivelAnt = 'Preparatoria'
            break;
        case 5: case 6:
            nivelAnt = 'Licenciatura'
            break;
        case 7:
            nivelAnt = 'Secundaria'
            break;
        default:
            nivelAnt = 'Licenciatura'
            break;
    }
    return nivelAnt;
}


/* FUNCIONES GENERALES
 * guardaUsuario   -> Guarda en localstorage todo el objeto en formato JSON
*/

function modalidadtxt(num) {
    switch (parseInt(num)) {
        case 1: return "Presencial"; break;
        case 2: return "Ejecutiva"; break;
        case 3: return "En línea"; break;
    }
}

function campustxt(campus) {
    switch (campus) {
        case 'ATZ': return "Atizapán"; break;
        case 'MAR': return "Marina"; break;
        case 'ECA': return "Ecatepec"; break;
        case 'GDL': return "Guadalajara"; break;
        case 'LEO': return "León"; break;
        case 'SUR': return "Sur"; break;
        case 'CUI': return "Cuitláhuac"; break;
        case 'TOL': return "Toluca"; break;
        case 'QRO': return "Querétaro"; break;
        case 'REY': return "Los Reyes"; break;
        case 'ONL': return "En Línea"; break;

    }
}

function unique(array) {
    return array.filter(function (el, index, arr) {
        return index == arr.indexOf(el);
    });
}
function getUsuario() {
    return JSON.parse(localStorage.getItem("jsonUsr"));
}

function getCampo(campo) {
    var usuario = getUsuario();
    return usuario[campo];
}
function getCosto() {
    return JSON.parse(localStorage.getItem("jsonCostos"));
}

function getCampoCosto(campo) {
    var costo = getCosto();
    //console.log("el campo"+campo);
    //console.log(costo[0]);
    return costo[0][campo];
}

function buscar(key, value, jsonCarreras) {
    var resultadoBusqueda = [];
    for (var i in jsonCarreras) {
        if (getObjects(jsonCarreras[i], key, value).length != 0) {
            resultadoBusqueda.push(jsonCarreras[i])
        }
    }
    return resultadoBusqueda;
}

function getObjects(obj, key, val) {
    var objects = [];
    for (var i in obj) {
        if (!obj.hasOwnProperty(i)) continue;

        if (typeof obj[i] == 'object') {
            objects = objects.concat(this.getObjects(obj[i], key, val));
        } else
            //if key matches and value matches or if key matches and value is not passed (eliminating the case where key matches but passed value does not)
            if (i == key && obj[i] == val || i == key && val == '') { //
                objects.push(obj);
            } else if (obj[i] == val && key == '') {
                //only add if the object is not already in the array
                if (objects.indexOf(obj) == -1) {
                    objects.push(obj);
                }
            }
    }
    return objects;
}

function guardaUsuario(valor, data) {
    if (localStorage.getItem("jsonUsr") === null) {
        var jsonnew = {};
        jsonnew[valor] = data;
        localStorage.setItem("jsonUsr", JSON.stringify(jsonnew));
        console.log('no existe el Json de usuario');
    } else {
        var viejo = JSON.parse(localStorage.getItem("jsonUsr"));
        viejo[valor] = data;
        localStorage.setItem("jsonUsr", JSON.stringify(viejo));
    }

}





function render_selects() {
    jQuery('.select-selected,.select-items').remove();
    var x, i, j, selElmnt, a, b, c;
    /*look for any elements with the class "custom-select":*/
    x = document.getElementsByClassName("custom-select");
    for (i = 0; i < x.length; i++) {
        selElmnt = x[i].getElementsByTagName("select")[0];
        /*for each element, create a new DIV that will act as the selected item:*/
        a = document.createElement("DIV");
        a.setAttribute("class", "select-selected w-100");
        console.log("pasando el primero");
        a.innerHTML = selElmnt.options[selElmnt.selectedIndex].innerHTML;
        x[i].appendChild(a);
        /*for each element, create a new DIV that will contain the option list:*/
        b = document.createElement("DIV");
        b.setAttribute("class", "select-items select-hide");
        for (j = 1; j < selElmnt.length; j++) {
            /*for each option in the original select element,
            create a new DIV that will act as an option item:*/
            c = document.createElement("DIV");
            console.log("pasando el segundo");
            c.innerHTML = selElmnt.options[j].innerHTML;
            c.addEventListener("click", function (e) {
                /*when an item is clicked, update the original select box,
                and the selected item:*/
                var y, i, k, s, h;
                s = this.parentNode.parentNode.getElementsByTagName("select")[0];
                h = this.parentNode.previousSibling;
                for (i = 0; i < s.length; i++) {
                    if (s.options[i].innerHTML == this.innerHTML) {
                        s.selectedIndex = i;
                        h.innerHTML = this.innerHTML;
                        y = this.parentNode.getElementsByClassName("same-as-selected");
                        for (k = 0; k < y.length; k++) {
                            y[k].removeAttribute("class");
                        }
                        this.setAttribute("class", "same-as-selected");
                        break;
                    }
                }
                h.click();
            });
            b.appendChild(c);
        }
        x[i].appendChild(b);



        b.addEventListener("click", function (e) {
            var idSelect = jQuery(this).parent().find('select').prop('id');
            updateSelect(idSelect);

        });

        a.addEventListener("click", function (e) {
            /*when the select box is clicked, close any other select boxes,
            and open/close the current select box:*/
            e.stopPropagation();
            closeAllSelect(this);
            this.nextSibling.classList.toggle("select-hide");
            this.classList.toggle("select-arrow-active");
            console.log('elegiste:')
            console.log(e);



        });
    }
    checkAllInputs();
}
function updateSelect(id) {
    setTimeout(function () { jQuery("#" + id).change(); render_selects(); }, 200);

}

function closeAllSelect(elmnt) {
    /*a function that will close all select boxes in the document,
    except the current select box:*/
    var x, y, i, arrNo = [];
    x = document.getElementsByClassName("select-items");
    y = document.getElementsByClassName("select-selected");
    for (i = 0; i < y.length; i++) {
        if (elmnt == y[i]) {
            arrNo.push(i)
        } else {
            y[i].classList.remove("select-arrow-active");
        }
    }
    for (i = 0; i < x.length; i++) {
        if (arrNo.indexOf(i)) {
            x[i].classList.add("select-hide");
        }
    }
}
function abreCalculadora() {
    var prueba_terminado = $("#modal_frm_app_calculadora").attr('data-terminado');
    if (prueba_terminado == "1") {
        xCalculadora = window.scrollX;
        yCalculadora = window.scrollY;
        console.log("Guardando y: " + yCalculadora);
        //Se cierra todo para que se muestre la calculadora
        console.log("Cerrando todo");
        window.scrollTo(0, 0);
        $('#bootstrapcss-css').attr('disabled', 'disabled');
        $('#mdb-css').attr('disabled', 'disabled');
        $('#bootstrapcss-css').prop('disabled', true);
        $('#mdb-css').prop('disabled', true);
        $('#calculadora-css').removeAttr("disabled");
        $('#calculadora-css').prop("disabled",false);
        $(".todoElBody").hide();
        $('.main-calculadora-todo').show();
        $('#aplicativo-calculadora').show();


    } else {
        jQuery("#modal_frm_app_calculadora").modal();
    }
}
/*if the user clicks anywhere outside the select box,
then close all select boxes:*/
document.addEventListener("click", closeAllSelect);
/* SEGUNDA PARTE JS */
/* IMPLEMENTACION OTRA CALCULADORA */