﻿var espacio = false;


function showForm() {
    dataLayer.push({'event': 'REGISTRO_TRADICIONAL'});
    jQuery('.calcimg').fadeOut(function() {
        jQuery('.frm-calculadoradiv').fadeIn();
        jQuery(".opcLogin").addClass("ocultomobile");
    });

    jQuery('.backb').fadeIn();

}

function hideForm() {
    dataLayer.push({'event': 'REGISTRO_REGRESAR' });
jQuery('.frm-calculadoradiv').fadeOut("fast");
    jQuery('.calcimg').fadeIn(function() {

        jQuery(".opcLogin").removeClass("ocultomobile");
    });

}

function askFb() {

    jQuery.ajaxSetup({
        cache: false
    });
    jQuery.getScript('//connect.facebook.net/en_US/sdk.js', function() {
        FB.init({
            appId: '844270899007087',
            xfbml: true,
            version: 'v2.7'
        });
        jQuery('#loginbutton,#feedbutton').removeAttr('disabled');
        FB.getLoginStatus(function(response) {
            console.log(response);
            if (response.status === 'connected') {
                // the user is logged in and has authenticated your
                // app, and reaspsponse.authResponse supplies
                // the user's ID, a valid access token, a signed
                // request, and the time the access token
                // and signed request each expire
                var uid = response.authResponse.userID;
                var accessToken = response.authResponse.accessToken;


                if (response.authResponse) {
                    console.log('Welcome!  Fetching your information.... ');
                    FB.api('/me', {
                        locale: 'en_US',
                        fields: 'name, email'
                    }, function(response) {
                        console.log(response);
                        console.log('Good to see you, ' + response.name + '.');
                        console.log("emilio:"+response.email);
                        jQuery('#frm_correo').val(response.email);

                        FB.api(
                            "/" + response.id + '?fields=first_name,last_name',
                            function(response) {
                             if (response && !response.error) {
                                    var apellidos = response.last_name.split(" ")
                                    var numap = apellidos.length;
                                    console.log(response);
                                    dataLayer.push({'event': 'REGISTRO_FACEBOOK' });
                                    jQuery('#frm_nombre').val(response.first_name);
                                    setCookie( 'loginface', 1, 1 );
                                    console.log(apellidos);
                                    console.log(numap);
                                    console.log(apellidos[0]);
                                    console.log(apellidos[1]);
                                    console.log(response);
                                    if (numap > 1) {
                                        jQuery('#frm_apaterno').val(apellidos[0]);
                                        jQuery('#frm_amaterno').val(apellidos[1]);


                                    } else {

                                        jQuery('#frm_apaterno').val(response.last_name);
                                        jQuery('#frm_amaterno').val(response.last_name);
                                    }

                                }
                            }
                        );
                          jQuery('#frm_celular').val('5555555555');


                    });

                    getLineasNegocio();
                    //jQuery.fn.fullpage.silentMoveTo(0, "sec-cursar");


                } else {
                    console.log('User cancelled login or did not fully authorize.');
                }




                jQuery.fn.fullpage.silentMoveTo(0, "sec-cursar");

                jQuery('#status2').fadeOut(); // will first fade out the loading animation
                jQuery('#preloader2').delay(350).fadeOut('slow'); // will fade out the white DIV that covers the website.
                jQuery('body').delay(350).css({
                    'overflow': 'visible'
                });

            } else if (response.status === 'not_authorized') {
                // the user is logged in to Facebook,
                // but has not authenticated your app

                var url = window.location.href;
                window.location = encodeURI("https://www.facebook.com/dialog/oauth?client_id=844270899007087&scope=email&redirect_uri=" + url + "#calculadora/sec-cursar&response_type=token");



            } else {
                var url = window.location.href;
                window.location = encodeURI("https://www.facebook.com/dialog/oauth?client_id=844270899007087&scope=email&redirect_uri=" + url + "#calculadora/sec-cursar&response_type=token");

            }
        });

    });

}


function checkSecuencial(numero) {
    consecutivo = 0;
    for (i = 0; i < 9; i++) {
        res = numero.substring(i, i + 1) - numero.substring(i + 1, i + 2);
        if (res == 1) {
            consecutivo++
        } else {
            consecutivo = 0;
        }
        //////console.log(Math.abs(res)+" Consecutivo: "+consecutivo);
        if (consecutivo > 3) {
            return consecutivo;
        }
    }

    for (i = 0; i < 9; i++) {
        res = numero.substring(i, i + 1) - numero.substring(i + 1, i + 2);
        if (res == -1) {
            consecutivo++
        } else {
            consecutivo = 0;
        }
        //////console.log(Math.abs(res)+" Consecutivo: "+consecutivo);
        if (consecutivo > 3) {
            return consecutivo;
        }
    }



}

function numVecestexto(codigo, campoid) {
    //console.log("id===="+campoid);

    if (jQuery('#' + campoid).val().length == 0) {
        keyAnt = codigo;
        vecesText = 0;
        //console.log("1keyAnt="+keyAnt+"-vecesText="+vecesText);

        return true;
    } else if (keyAnt == codigo) {
        if (vecesText >= 1) {
            //console.log("2keyAnt="+keyAnt+"-vecesText="+vecesText);
            return false;
        } else if (vecesText != 1) {
            keyAnt = codigo;
            vecesText = vecesText + 1;
            //console.log("3keyAnt="+keyAnt+"-vecesText="+vecesText);
            return true;
        }
    } else {

        if (keyAnt != codigo) {
            keyAnt = codigo;
            vecesText = 0;
            return true;
        }
    }
}

function checkRepetido(numero) {
    repetido = 0;
    for (i = 0; i < 9; i++) {
        res = numero.substring(i, i + 1) - numero.substring(i + 1, i + 2);
        if (Math.abs(res) == 0) {
            repetido++
        } else {
            repetido = 0;
        }
        //////console.log("Repetido:"+repetido);
        if (repetido > 3) {
            break;
        }
    }
    return repetido;
}

function frm_sel_inf(valor) {

    if (valor == "inf1") {
        jQuery(".inf1").addClass("frm_choosed_blue");
        valor = 10040;
    }
    if (valor == "infrev") {
        jQuery(".infrev").addClass("frm_choosed_blue");
        valor = 10183;
    }
    jQuery("#frm_informacion").val(valor);
}

function mobileExpand(boton1) {
    var boton = jQuery('.' + boton1);
    boton.css({
        "position": "absolute",
        "z-index": 99
    });
    //JEAB SE CAMBIO ANIMATE POR CSS
    boton.stop().css({
        width: "100%"
    });
    if (boton1 == 'btn-materias') {
        boton.html('<div class="row espacio-prom-mater">Materias a cursar:</div><div class="col-xs-4"><div ><i data-slider="ex14" class="fa fa-minus fa-2x  control-menos-mat"></i></div></div><div class="col-xs-4" style="font-size: 60px; margin-top: -17px; margin-left:0%;" ><div  class="materias-val  montserrat">' + jQuery('#calcApp').data('materias') + '</div></div> <div class="col-xs-4"  style="margin-left="15px"><div ><i data-slider="ex14" class="fa fa-plus fa-2x control-mas-mat"></i></div></div>');
    }
    if (boton1 == 'btn-promedio') {
        boton.html('<div class="row espacio-prom-mater">Promedio:</div><div class="col-xs-4"><div ><i data-slider="ex13" class="fa fa-2x fa-minus  control-menos"></i></div></div><div class="col-xs-4" style="font-size: 60px; margin-top: -17px; margin-left:0%;" ><div  class="promedio-val  montserrat">' + jQuery('#calcApp').data('promedio') + '</div></div> <div class="col-xs-4"  style="margin-left="15px"><div ><i data-slider="ex13" class="fa fa-2x fa-plus control-mas"></i></div></div>');
    }
    jQuery('.color-control').prepend('<div class="btn-close">X</div>');
    jQuery('.btn-close').data('btn', boton1);
}

function mobileCollapse(boton1) {

    var boton = jQuery('.' + boton1);
    if (boton1 == "btn-materias") {
        boton.html('<p>MATERIAS</p><i class="fa"></i><div class="row pos-relative small-text hidden-xs matctrl" style="display: block;"><div class="col-xs-6" style="    text-align: right;    font-size: 83px;    margin-top: -45px;"><div class="materias-val  montserrat">' + jQuery('#calcApp').data('materias') + '</div></div><div class="col-xs-6" style="text-align: left;font-size: 23px;    top: -38px;"><div><i data-slider="ex14" class="fa fa-plus control-mas-mat"></i></div><div><i data-slider="ex14" class="fa fa-minus  control-menos-mat"></i></div></div></div>')
    }
    if (boton1 == "btn-promedio") {
        boton.html('<p>PROMEDIO</p><i class="fa"></i><div class="row pos-relative small-text hidden-xs promctrl" style="display: block;"><div class="col-xs-6"><div style="    text-align: right;    font-size: 83px;    margin-top: -45px;" class="promedio-val montserrat">' + jQuery('#calcApp').data('promedio') + '</div></div><div class="col-xs-6" style="text-align: left;font-size: 23px;top: -38px;"><div><i data-slider="ex13" class="control-mas fa fa-plus "></i></div><div><i data-slider="ex13" class="control-menos fa fa-minus"></i></div></div></div>')
    }
    jQuery('.btn-close').remove();
    boton.css({
        "position": "relative",
        "z-index": ""
    });
    boton.css({
        "width": ''
    });

}

function formatEmail(email) {

    if (email.search(/@/) == -1) {
        return email;
        //console.log("entra a format email");
    }
    try {
        email = email.toLowerCase();
        //console.log("email/"+email);
        var arrEmailCorrecto = ['hotmail.com', 'hotmail.es', 'gmail.com', 'yahoo.com', 'yahoo.com.mx', 'yahoo.mx'];
        var arrEmail = email.split("@");

        var arrDominio = arrEmail[1].split(".");
        var dominio_ok;
        var email_valido;

        //console.log(arrEmail[1]);

        var i = 0;
        var dominio = Array();
        //console.log("----"+arrDominio[0]);
        switch (arrDominio[0].substring(0, 3)) {

            case "icl":
                dominio_ok = "icloud.com";
                break;

            case "hot":
            case "hor":
            case "hoi":
            case "hol":
            case "hom":
            case "hoy":
            case "hitmail":
            case "hatmail":
            case "htomail":
                if (arrDominio[1] && arrDominio[1].substring(0, 1) == "c") {
                    dominio_ok = "hotmail.com";
                } else
                if (arrDominio[1] && arrDominio[1].substring(0, 1) == "e") {
                    dominio_ok = "hotmail.es";
                } else {
                    dominio_ok = "hotmail.com";
                }
                break;

            case "gma":
            case "gam":
            case "gmi":
                dominio_ok = "gmail.com";
                break;

            case "out":
            case "oul":
            case "otl":
                if (arrDominio[1] && arrDominio[1].substring(0, 1) == "c") {
                    dominio_ok = "outlook.com";
                }
                if (arrDominio[1] && arrDominio[1].substring(0, 1) == "e") {
                    dominio_ok = "outlook.es";
                }
                break;

            case "yah":
                if (arrDominio[2]) {
                    dominio_ok = "yahoo.com.mx";
                } else {
                    if (arrDominio[1] && arrDominio[1].substring(0, 1) == "c") {
                        dominio_ok = "yahoo.com";
                    } else
                    if (arrDominio[1] && arrDominio[1].substring(0, 1) == "m") {
                        dominio_ok = "yahoo.mx";
                    } else {
                        dominio_ok = arrEmail[1];
                    }
                }

                break;

            default:
                email_valido = email;
                break;
        } //fin switch

        if (!email_valido) {
            email_valido = arrEmail[0] + "@" + dominio_ok;
        }

    } catch (err) {
        console.log(err);
    }
    //console.log("correo-valid:"+email_valido);

    return email_valido;
}


function checkRepetido(numero) {
    var repetido = 0;
    for (i = 0; i < 9; i++) {
        res = numero.substring(i, i + 1) - numero.substring(i + 1, i + 2);
        if (Math.abs(res) == 0) {
            repetido++
        } else {
            repetido = 0;
        }
        //console.log("Repetido:"+repetido);
        if (repetido > 3) {
            break;
        }
    }
    return repetido;
}

function ValidaSoloNumeros() {
    if ((event.keyCode < 48) || (event.keyCode > 57))
        event.returnValue = false;
}

function ValidaSoloTexto() {
    if ((event.keyCode != 32) && (event.keyCode < 65) || (event.keyCode > 90) && (event.keyCode < 97) || (event.keyCode > 122))
        event.returnValue = false;
}

function EvaluateTextP(obj, e, id) {

    var validoT = false;
    opc = true;
    tecla = (document.all) ? e.keyCode : e.which;
    if (id == "micro")
        validoT = numVecestextoMicro(tecla);
    else
        validoT = numVecestexto(tecla, id);
    if (tecla == 8) {
        return true;
    } else if (tecla == 13 && obj.value.length > 0) {
        espacio = true;
        return;
    } else if (validoT == true) {
        if (tecla == 32 && espacio == true) {
            espacio = false;
            return true;
        } else if (espacio = false && tecla == 32) {
            return false;
        }
        if ((tecla != 32) && !((tecla > 8 && tecla < 33) || (tecla < 65 && tecla > 31) || (tecla > 90 && tecla < 97) || tecla > 122)) {
            espacio = true;
        }
        if ((tecla > 8 && tecla < 33) || (tecla < 65 && tecla > 31) || (tecla > 90 && tecla < 97) || tecla > 122) {
            return false;
        }
        return true;
    } else return false;
}

function frm_SinEspacios(e) {
    tecla = (document.all) ? e.keyCode : e.which;
    if (tecla == 32) {
        return false;
    }
    if (tecla != 32) {
        return true;
    }
}



function selectColor(obj) {
    jQuery(obj).addClass("naranja");
}

function identificaModalidad(valorModalidad) {
    switch (valorModalidad) {
        case 1:
            return 'Presencial';
            break;
        case 2:
            return 'Ejecutiva';
            break;
        case 3:
            return 'En Linea';
            break;
    }
}

function queryHandler(json, queryString) {
    var db = SpahQL.db(json);
    var result = db.select(queryString);
    return result.clone();
}

function unique(array) {
    var uniqueValues = [];
    jQuery.each(array, function(i, el) {
        if (jQuery.inArray(el, uniqueValues) === -1) uniqueValues.push(el);
    });
    return uniqueValues;
}

function irA(anchor, cerrar) {
    jQuery.fn.fullpage.moveTo(0, anchor);
    if (cerrar == 0) {
        jQuery('#msj-modal').modal('hide');
    }
}

function validaForm(val) {
    return true;
}

function getParameterByName(name) {
    var match = RegExp('[?&]' + name + '=([^&]*)').exec(window.location.search);
    return match && decodeURIComponent(match[1].replace(/\+/g, ' '));
}

function init() {

    /*******FUNCIONES DE INICIALIZACION********/
    jQuery("#secLineas").html("");

    //if(jQuery("#calcApp").data("formulario")==null){jQuery.fn.fullpage.moveTo(0, "sec-form");} //Si no ah llenado el formulario se obliga a llenarlo
    if (getParameterByName("utm_campus") || getParameterByName("utm_linea")) {
        jQuery("#calcApp").data("selectLinea", getParameterByName("utm_ln"));
        jQuery("#calcApp").data("campus", getParameterByName("utm_campus"));
        jQuery("#calcApp").data("banner", getParameterByName("utm_campaign"));

        if (jQuery("#calcApp").data("linea") != null) { /*getCategoriaNegocio({linea: jQuery("#calcApp").data("campus")});*/ }
        if (jQuery("#calcApp").data("campus") != null) {
            getLineasNegocio({
                campus: jQuery("#calcApp").data("campus")
            });
        }
        if (jQuery("#calcApp").data("carrera") != null) { /*getCategoriaNegocio({linea: jQuery("#calcApp").data("campus")});*/ }
        //NO SIGUE FLUJO NORMAL
    } else {
        //SIGUE FLUJO NATURAL
        getLineasNegocio({
            campus: ''
        });
        //validaForm();
        //validaLineaNegocio();
        //validaCarrera();
        //validaCampus();
        //validaPromedio();
        //validaMaterias();
    }
}


function getURLvars() {}

function paso1() {
    return true
}

function watchDog() {
    if (jQuery("#calcApp").data('utm_campaign')) {}
}

function getLineasNegocio(options) {
    jQuery("#secLineas>.container>.row").empty();
    jQuery("#secLineas>.container-fluid").empty();
    //console.log("Ejecutando Funcion Lineas de Negocio");
    var options = typeof options == 'object' && options
        //Creamos un Array para insertar las lineas del query (Estan Duplicados los valores en este punto)
    var duplicatedLineas = new Array();
    //Variable de Control
    var i;
    if (options.campus) {
        //console.log("Se encontro la variable en URL");
        var qry = queryHandler(jsonCarreras, '/*[/campus/*}>{ {"' + options.campus + '"}]');

    } else {
        //console.log("No se encontro la variable en URL, carga default");
        var qry = queryHandler(jsonCarreras, '/*');
        //console.log(qry);
    }
    for (i = 0; i < qry.length; i++) {
        duplicatedLineas[i] = qry[i].value.lineaWeb;
    }
    var lineasUnique = unique(duplicatedLineas);

    /*Reconstruir el full page By SRP 16-04-2018*/
    jQuery(".seccionesCalculadora").remove();
    jQuery("body").css({'overflow': 'hidden'});
    //setTimeout(function(){ jQuery.fn.fullpage.reBuild(); }, 3000);
   /*Reconstruir el full page By SRP 16-04-2018*/

    //console.log("veces"); 
    //console.log(lineasUnique);
    var ejecuta = 0;
    if (jQuery(window).width() > 768) {
        jQuery('#calcApp').data("desk", 1);
        jQuery("#secLineas").append('\
              <div id="contenedor-lineas-negocio" class="container-fluid">\
                <div class="row no-gutter">\
                  <div class="col-sm-4">\
                  \
                    <div data-animacion="1" style="cursor:pointer;" data-linea="PREPARATORIA"  id="PREPARATORIA" class="linea-dsk clcLinea" data-resize="39">\
                      <div class="linea-elementos">\
                        <div class="lineas-caption" onclick="openlineafx(this)">\
                          <div class="col-md-6"> PREPARATORIA</div>\
                          <div class="col-md-6 "> \
                            <a class="btn btn-primary pull-right">\
                              <span class="hidden-xs">Selecciona <i class="fa fa-arrow-right" aria-hidden="true"></i></span>\
                            </a>\
                          </div>\
                        </div>\
                      </div>\
                    </div>\
                    \
                    <div data-animacion="2" style="cursor:pointer;" data-linea="POSGRADO" id="POSGRADO" class="linea-dsk clcLinea" data-resize="39">\
                      <div class="linea-elementos">\
                        <div class="lineas-caption" onclick="openlineafx(this)">\
                          <div class="col-md-6"> POSGRADO</div>\
                          <div class="col-md-6 "> \
                            <a class="btn btn-primary pull-right">\
                              <span class="hidden-xs">Selecciona <i class="fa fa-arrow-right" aria-hidden="true"></i></span>\
                            </a>\
                          </div>\
                        </div>\
                      </div>\
                    </div>\
                        \
                  </div>\
                  \
                  <div class="col-sm-4">\
                    <div data-animacion="3" style="cursor:pointer;" data-linea="LICENCIATURA" id="LICENCIATURA" class="linea-dsk clcLinea" data-resize="78">\
                      <div class="linea-elementos">\
                         <div class="lineas-caption" onclick="openlineafx(this)">\
                          <div class="col-md-6"> LICENCIATURA</div>\
                          <div class="col-md-6 "> \
                            <a class="btn btn-primary pull-right">\
                              <span class="hidden-xs">Selecciona <i class="fa fa-arrow-right" aria-hidden="true"></i></span>\
                            </a>\
                          </div>\
                        </div>\
                      </div>\
                    </div>\
                  </div>\
                  \
                  <div class="col-sm-4">\
                    \
                    <div data-animacion="4" style="cursor:pointer;" data-linea="INGENIERIA" id="INGENIERIA" class="linea-dsk clcLinea" data-resize="39">\
                      <div class="linea-elementos">\
                         <div class="lineas-caption" onclick="openlineafx(this)">\
                          <div class="col-md-6"> INGENIERíA</div>\
                          <div class="col-md-6 "> \
                            <a class="btn btn-primary pull-right">\
                              <span class="hidden-xs">Selecciona <i class="fa fa-arrow-right" aria-hidden="true"></i></span>\
                            </a>\
                          </div>\
                        </div>\
                      </div>\
                    </div>\
                    \
                    <div data-animacion="5" style="cursor:pointer;" data-linea="SALUD" id="SALUD" class=" linea-dsk clcLinea" data-resize="39">\
                      <div class="linea-elementos">\
                         <div class="lineas-caption" onclick="openlineafx(this)">\
                          <div class="col-md-6"> SALUD</div>\
                          <div class="col-md-6 "> \
                            <a class="btn btn-primary pull-right">\
                              <span class="hidden-xs">Selecciona <i class="fa fa-arrow-right" aria-hidden="true"></i></span>\
                            </a>\
                          </div>\
                        </div>\
                      </div>\
                    </div>\
                  \
                  </div>\
                   </div>\
              </div>');

        //Jeab si es version de escritorio añadimos la clase para el margen del contenedor de lineas de negocio
        if (jQuery(window).width() > 768) {

            jQuery("#contenedor-lineas-negocio").addClass('margin-top-ln');
            /*jQuery("#bloques-sec-cursar").addClass('margin-bottom-ln');*/
            jQuery("#p-nombre-carrera").addClass('p-bloques');
            jQuery("#p-campus").addClass('p-bloques');

            jQuery("#d-promedio").addClass('d-bloques');
            jQuery("#d-materias").addClass('d-bloques');

            jQuery("#boton-suma").addClass('b-botones');
            jQuery("#boton-resta").addClass('b-botones');

            jQuery("#boton-suma-materias").addClass('b-botones');
            jQuery("#boton-resta-materias").addClass('b-botones');

        }


        jQuery.each(lineasUnique, function(index, val) {
            //console.log(val);
            jQuery('div[data-linea="' + val + '"]').addClass("item-available");
        });
        escala();
    } else {

        //console.log("ejecutando");
        jQuery("#secLineas").append('<div class="container"><div class="row padding-top-calcu"></div></div>');

        jQuery.each(lineasUnique, function(index, val) {
            // console.log(ejecuta);
            ejecuta++;

            jQuery("#secLineas>.container>.row").append('<div style="cursor:pointer;" id="mob_' + val + '" data-linea="' + val + '"  class="item-available col-xs-12 col-md-8 clcLinea clcSelector"> <img src="/wp-content/themes/temaunitec/' + myLibraryObject + 'img/' + val + '.jpg" class="img-circle img-calcu" alt="img_circle" /><span>' + val + '</span><i class="fa fa-chevron-right pull-right"></i></div>');
            orderElements(jQuery("#secLineas>.container>.row"));
        });
    }




}



function getCampus() {

    var idDynamics = jQuery('#calcApp').data("selectMateria");
    jQuery("#secCampus").html('');
    jQuery("#secCampus>.container-fluid>.row").html('');
    debuglog("Clave Dynamics obtenido:" + idDynamics);

    if (!idDynamics) {
        var losCampus = ["ATZ", "MAR", "GDL", "ECA", "SUR", "CUI", "LEO", "REY", "TOL", "ONL","QRO"];
        //console.log("----------------------------------------------------------------------");
        //console.log('Ejecutando Funcion getCampus');
        //console.log('Query: /*/[/IdDynamics == "'+idDynamics+'"]');
        //console.log('Campus encontrados con el id: '+idDynamics);
        //console.log(losCampus);
    } else {
        var qryDynamics = queryHandler(jsonCarreras, '/*/[/IdDynamics == "' + idDynamics + '"]');
        var losCampus = qryDynamics[0].value.campus;
        //console.log("----------------------------------------------------------------------");
        //console.log('Ejecutando Funcion getCampus');
        //console.log('Query: /*/[/IdDynamics == "'+idDynamics+'"]');
        //console.log('Campus encontrados con el id: '+idDynamics);
        //console.log(losCampus);
    }



    if (jQuery(window).width() > 768) {


        /*JEAB CAMBIAR LA EXPERIENCIA DEL CAMPUS*/
        jQuery("#secCampus").append('\
                    <div data-animacion="3" style="cursor:pointer;" data-linea="CAMPUS" id="CAMPUS" class="linea-dsk" data-resize="100">\
                      <div class="linea-campus">\
                         <div class="lineas-caption" onclick="openlineafx(this)">\
                          <div class="col-md-6"> LICENCIATURA</div>\
                          <div class="col-md-6 "> \
                            <a class="btn btn-primary pull-right">\
                              <span class="hidden-xs">Selecciona <i class="fa fa-arrow-right" aria-hidden="true"></i></span>\
                            </a>\
                          </div>\
                        </div>\
                      </div>\
                    </div>\
                  </div>');

        jQuery('#' + 'CAMPUS' + '>.linea-campus').append('<div class="col-md-8">\
                                                          <div class="row que-campus">\
                                                              Campus de interés:\
                                                              <br>\
                                                              ' + '' + '\
                                                              <div class="col-md-8"><select id="select-campus" size="11" class="sel-campus clSelectCampus" onChange="setCampus(this.value);return;">\
                                                              </select></div><div class="col-md-4"></div>\
                                                          </div>\
                                                          <div class="modstatus"></div>\
                                                          <div class="carreramods dark">\
                                                          </div>\
                                                      </div>\
                                                          <div class="col-md-4 btn-back-sec-campus"><span class="btn btn-primary nuevo-boton-ir-carrera" id="new-button-back" onclick="irCarrera();"><i class="fa fa-chevron-left"></i>&nbsp;Cambiar carrera</span></div>\
                                                          <div class="col-md-4 btn-go-sec-promedio"><span class="btn btn-primary nuevo-boton-ir-campus" id="new-button-back" onclick="irPromedio();"><i class="fa fa-chevron-right"></i>&nbsp;Ir a promedio</span></div>\
                                                      ');
        /*AÑADIR LOS OPTIONS DEL SELECT*/
        if (jQuery('#calcApp').data("desk") == 1) {
 
            jQuery(".sel-campus").append('<option data-campus="ATZ" disabled="disabled" id="ATZ" value="Atizapán, Edo. Mex.">' + 'Atizapán, Edo. Mex.' + '</option>');
            jQuery(".sel-campus").append('<option data-campus="CUI" disabled="disabled" id="CUI" value="Cuitláhuac, CDMX">' + 'Cuitláhuac, CDMX' + '</option>');
            jQuery(".sel-campus").append('<option data-campus="ECA" disabled="disabled" id="ECA" value="Ecatepec, Edo. Mex.">' + 'Ecatepec, Edo. Mex.' + '</option>');
            jQuery(".sel-campus").append('<option data-campus="ONL" disabled="disabled" id="ONL" value="En Línea">' + 'En Línea' + '</option>');
            jQuery(".sel-campus").append('<option data-campus="GDL" disabled="disabled" id="GDL" value="Guadalajara">' + 'Guadalajara' + '</option>');
            jQuery(".sel-campus").append('<option data-campus="LEO" disabled="disabled" id="LEO" value="León, Gto">' + 'León, Gto' + '</option>');
            jQuery(".sel-campus").append('<option data-campus="REY" disabled="disabled" id="REY" value="Los Reyes, CDMX">' + 'Los Reyes, CDMX' + '</option>');
            jQuery(".sel-campus").append('<option data-campus="MAR" disabled="disabled" id="MAR" value="Marina, CDMX">' + 'Marina, CDMX' + '</option>');
            jQuery(".sel-campus").append('<option data-campus="QRO" disabled="disabled" id="QRO" value="Querétaro, QRO">' + 'Querétaro, QRO' + '</option>');
            jQuery(".sel-campus").append('<option data-campus="SUR" disabled="disabled" id="SUR" value="Sur Iztapalapa, CDMX">' + 'Sur Iztapalapa, CDMX' + '</option>');
            jQuery(".sel-campus").append('<option data-campus="TOL" disabled="disabled" id="TOL" value="Toluca, Edo. Mex.">' + 'Toluca, Edo. Mex.' + '</option>');
            //jQuery(".sel-campus").append('<option value="'+value[0].idcarrera+'" >' + value[0].carrera + '</option>');
            /*Cacha si el hover esta en un campus*/
            /*jQuery('.sel-campus').on('mouseenter', 'option', function(e) { 

              /jQuery('#CAMPUS').animate({
                    background:"url('https://unitecmx-universidadtecno.netdna-ssl.com/wp-content/themes/temaunitec/calculadora/css/img/CAMPUS_"+jQuery(this).attr('id')+".jpg') no-repeat center center fixed"
                });
                 jQuery('#CAMPUS').css('background',"url(http://local.unitec.mx/wp-content/themes/temaunitec/calculadora/css/img/CAMPUS_"+jQuery(this).attr('id')+".jpg) 0 0/cover"); 

            });*/
        }


        jQuery("#secCampus>.container-fluid").append('\
              <div class="col-sm-4 no-padding" style="margin-top: 46px;">\
                <div data-campus="GDL" id="GDL" class="col-sm-12 campus-dsk clCampus cursor-pointer" onclick="setCampus(\'Guadalajara\')" data-resize="78"><h2 class="no-disponible hidden-xs hidden-sm">No Disponible</h2><div class="lineas-caption calc-steps-title" onclick="setCampus(\'Guadalajara\')">Guadalajara</div></div>\
              </div>');
        escala();


        var campusCounter = 0;
        jQuery.each(losCampus, function(key, value) {
                var campusAbv = value;
                campusCounter = campusCounter + 1;
                setCookie('campusCounter', campusCounter, 1);
                switch (value) {
                    case 'ATZ': //1
                        key = value;
                        value = "Atizapán";
                        break;
                    case 'CUI': //2
                        key = value;
                        value = "Cuitláhuac";
                        break;
                    case 'ECA': //3
                        key = value;
                        value = "Ecatepec";
                        break;
                    case 'MAR': //5
                        key = value;
                        value = "Marina";
                        break;
                    case 'SUR': //7
                        key = value;
                        value = "Sur";
                        break;
                    case 'LEO': //4
                        key = value;
                        value = "León";
                        break;
                    case 'REY': //4
                        key = value;
                        value = "Los Reyes";
                        break;
                    case 'TOL': //8
                        key = value;
                        value = "Toluca";
                        break;
                    case 'ONL': //6
                        key = value;
                        value = "En Línea";
                        break;
                    case 'GDL': //9
                        key = value;
                        value = "Guadalajara";
                        break;
                     case 'QRO': //9
                        key = value;
                        value = "Querétaro";
                        break;
                }

                jQuery('option[data-campus="' + campusAbv + '"]>h2').remove();
                jQuery('option[data-campus="' + campusAbv + '"]').addClass("item-available");
                jQuery('option[data-campus="' + campusAbv + '"]').removeAttr('disabled');

                jQuery('option[data-campus="' + campusAbv + '"]').find(".lineas-caption").html(value);


            })
            //AGREGAR TEXTO A LOS INPUTS QUE NO ESTAN DISPONIBLES
        jQuery('option[disabled="disabled"]').append(' (No disponible)');


      var elem = jQuery('#select-campus').find('option').sort(sortMe);
            function sortMe(a, b) {
                return a.className < b.className;
            }
            jQuery('#select-campus').append(elem);



    } else {

        jQuery("#secCampus").append('<div class="container"><div class="row padding-top-calcu"></div></div>');
        var i = 0;
        jQuery.each(losCampus, function(key, value) {
            var img = value;
            switch (value) {
                case 'ATZ': //1
                    key = value;
                    value = "Atizapán";
                    break;
                case 'CUI': //2
                    key = value;
                    value = "Cuitláhuac";
                    break;
                case 'ECA': //3
                    key = value;
                    value = "Ecatepec";
                    break;
                case 'MAR': //5
                    key = value;
                    value = "Marina";
                    break;
                case 'SUR': //7
                    key = value;
                    value = "Sur";
                    break;
                case 'LEO': //4
                    key = value;
                    value = "León";
                    break;
                case 'REY': //4
                    key = value;
                    value = "Los Reyes";
                    break;
                case 'TOL': //8
                    key = value;
                    value = "Toluca";
                    break;
                case 'ONL': //6
                    key = value;
                    value = "En Línea";
                    break;
                case 'GDL': //w
                    key = value;
                    value = "Guadalajara";
                    break;
                case 'QRO': //w
                    key = value;
                    value = "Querétaro";
                    break;
            }

            jQuery("#secCampus>.container>.row").append('<div data-campus="' + key + '" style="cursor:pointer;" class="col-xs-12 col-md-8 clCampus clcSelector item-available"><img src="/wp-content/themes/temaunitec/calculadora/img/' + img + '.jpg" class="img-circle img-calcu" alt="img_circle2" /> <span>' + value + '</span><i class="fa fa-chevron-right pull-right"></i></div>');

            i++;
        });

        orderElements(jQuery("#secCampus>.container>.row"));

    }

    if (i == 1) {
        jQuery("#secCampus>.container>.row").append('<div class="col-xs-12 col-md-8 clcSelector "> <span>Esta carrera solo esta disponible en este campus </span><span class="btn-cursar">Elegir otra carrera</span></div>');
    }
    return i;
}




function escala() {
    var pantallaCompleta = jQuery(window).height();
    jQuery('[data-resize]').each(
        function(index) {
            medida = (jQuery(this).attr("data-resize") * pantallaCompleta) / 100;
            jQuery(this).css("height", medida);
        });
}

function clcPreguntas(pregunta) {
    
          //var preg = pregunta.id.split("examen_")
          var resp = pregunta.id.split("examen_");
      jQuery("#calcApp").data("examen", resp[1]);
      //jQuery("#calcApp").data("padreTutor");
                        var ccontCarrera = getCarreras();
                    if (ccontCarrera == 1) {
                            jQuery('.clcCategoria').click();
                            //e.preventDefault();
                                jQuery.fn.fullpage.moveTo(0, 'sec-modalidades');
                        } else {
                            //e.preventDefault();
                                jQuery.fn.fullpage.moveTo(0, 'sec-categoria');
                        }
    }
    
    
    function clcPreguntasTutor(pregunta) {
          //var preg = pregunta.id.split("examen_")
              var resp = pregunta.id.split("tutor_");
          jQuery("#calcApp").data("padreTutor", resp[1]);
          //jQuery("#calcApp").data("padreTutor");
                            var ccontCarrera = getCarreras();
                        if (ccontCarrera == 1) {
                                jQuery('.clcCategoria').click();
                                //e.preventDefault();
                                    jQuery.fn.fullpage.moveTo(0, 'sec-campus');
                            } else {
                                //e.preventDefault();
                                    jQuery.fn.fullpage.moveTo(0, 'sec-categoria');
                            }
        }
    
    function muestraPregunta()
 {

        //JEABJEABJEAB
        if (jQuery("#calcApp").data("selectLinea") == "LICENCIATURA" || jQuery("#calcApp").data("selectLinea") == "INGENIERIA" || jQuery("#calcApp").data("selectLinea") == "SALUD") {
        
                    alert("Entra a implementacion");
        
                    jQuery("#secCategorias>.container>.row").append("<div id='pregunta_examen' style='margin: auto;padding: 10px;font-size: 25px;'>¿Realizaste examen para alguna universidad pública?</div>");
        
                    jQuery("#secCategorias>.container>.row").append('<div onclick="clcPreguntas(this);" value="si" id="examen_si" style="cursor:pointer;" class="item-available col-xs-12 col-md-8 clcSelector clcPreguntas"><span class="ancho-carrera">' + 'Si' + '</span><i class="fa fa-chevron-right pull-right"></i></div>');
        
                    jQuery("#secCategorias>.container>.row").append('<div onclick="clcPreguntas(this);" id="examen_no" value="no" style="cursor:pointer;" class="item-available col-xs-12 col-md-8 clcSelector clcPreguntas"><span class="ancho-carrera">' + 'No' + '</span><i class="fa fa-chevron-right pull-right"></i></div>');
        
                        orderElements(jQuery("#secCategorias>.container>.row"))
            
                }
}




function getCarreras() {

    jQuery("#secCategorias>.container>.row").html('');
    jQuery(".sel-carrera").empty();
    var lineaNegocio = jQuery('#calcApp').data("selectLinea");
    /*console.log("----------------------------------------------------------------------");
    console.log("Ejecutando getCarreras");
    console.log("Linea de Negocio :"+lineaNegocio);
    console.log("Campus :"+jQuery('#calcApp').data("cmpus"));*/

    var nombreCarrera = [];
    var i = 0;
    switch (lineaNegocio) {
        case 'LICENCIATURA':
            var qry = queryHandler(jsonCarreras, '/*/[/lineaWeb=="LICENCIATURA"][/ocultar!= "cal"]/Grupo_carreras');
            break;
        case 'INGENIERIA':
            var qry = queryHandler(jsonCarreras, '/*/[/lineaWeb=="INGENIERIA"][/ocultar!= "cal"]/Grupo_carreras');
            break;
        case 'POSGRADO':
            var qry = queryHandler(jsonCarreras, '/*/[/lineaWeb=="POSGRADO"][/ocultar!= "cal"]/Grupo_carreras');

            break;
        case 'SALUD':
            var qry = queryHandler(jsonCarreras, '/*/[/lineaWeb=="SALUD"][/ocultar!= "cal"]/Grupo_carreras');
            break;
        case 'PREPARATORIA':
            var qry = queryHandler(jsonCarreras, '/*/[/lineaWeb=="PREPARATORIA"][/ocultar!= "cal"]/Grupo_carreras');
            break;
        default:
            var qry = queryHandler(jsonCarreras, '/*/Grupo_carreras[/ocultar!= "cal"]');
    }

    if (jQuery('#calcApp').data("campus")) {
        var qry = queryHandler(jsonCarreras, '/*/[/lineaWeb=="' + lineaNegocio + '"][/ocultar!= "cal"][/campus/*}>{ {"' + jQuery('#calcApp').data("campus") + '"}]/Grupo_carreras');

        if (qry.length == 0) {
            jQuery('#calcApp').removeData("campus");
            getCarreras();
        }
    }



    var categoria = unique(qry.values());
    var arrayCarreras = new Array;

    for (var i = 0; i < categoria.length; i++) {

        var results = queryHandler(jsonCategorias, "/0/" + categoria[i] + "");
        var nombreArray = results.values();

        arrayCarreras.push([{
            "idcarrera": categoria[i],
            "carrera": nombreArray[0]
        }]);



    };
    //JEAB ORDENAMIENTO DE DATOS 27-04-2016
    arrayCarreras.sort(function(a, b) {
        //console.log(a[1]);
        if (a[0].carrera.toLowerCase() < b[0].carrera.toLowerCase()) return -1;
        if (a[0].carrera.toLowerCase() > b[0].carrera.toLowerCase()) return 1;
        return 0;

    });
    //console.log(arrayCarreras);

    jQuery(".sel-carrera").append("<option>Selecciona</option>");
    var contCarrera = 0;
    jQuery.each(arrayCarreras, function(key, value) {
        contCarrera++;

        //if(value[0].idcarrera!=55 && value[0].idcarrera!=53 && value[0].idcarrera!=52 && value[0].idcarrera!=51 && value[0].idcarrera!=36 ){}

        if (jQuery('#calcApp').data("desk") == 1) {
            jQuery(".sel-carrera").append('<option value="' + value[0].idcarrera + '" >' + value[0].carrera + '</option>');
        } else {
            jQuery("#secCategorias>.container>.row").append('<div style="cursor:pointer;" data-categoria="' + value[0].idcarrera + '" class="item-available col-xs-12 col-md-8 clcCategoria clcSelector"><span class="ancho-carrera">' + value[0].carrera + '</span><i class="fa fa-chevron-right pull-right"></i></div>');
            orderElements(jQuery("#secCategorias>.container>.row"));
        }


    });

    return contCarrera;

}

function getModalidad(idcarrera, categoria) {
    /*console.log("----------------------------------------------------------------------");
    console.log('Ejecutando Funcion getModalidad');
    console.log('Parametros recibidos: idcarrera:'+idcarrera +"| categoria: "+categoria);*/
    jQuery(".carreramod").empty();
    jQuery("#secModalidades>.container>.row").html('');
    //console.log(idcarrera+"|"+categoria);
    if (idcarrera != '') {
        var qry = queryHandler(jsonCarreras, '/*[/IdDynamics=="' + idcarrera + '"]/');
        // console.log("Query ejecutada: "+'/*[/IdDynamics=="'+idcarrera+'"]/'+"debido al idcarrera: "+idcarrera);
    } else if (categoria != '' || categoria == 0) {
        var qry = queryHandler(jsonCarreras, '/*[/Grupo_carreras=="' + categoria + '"]/');
        //console.log("Query ejecutada: "+'/*[/Grupo_carreras=="'+categoria+'"]/'+"debido a la categoria: "+categoria);
    }
    /* console.log(qry);
     console.log("idcarrera: "+idcarrera+"   | Categoria:"+categoria);*/
    var contMod = 0;
    jQuery.each(qry, function(key, value) {
        contMod++;
        var idCosto = value.value.catCosto;
        //alert(value.value.catCosto);
        switch (parseInt(value.value.modalidad)) {
            case 1:
                key = value.value.IdDynamics;
                value = 'Presencial';
                break;
            case 2:
                key = value.value.IdDynamics;
                value = 'Ejecutiva';
                break;
            case 3:
                key = value.value.IdDynamics;
                value = 'En Linea';
                break;
            case 4:
                key = value.value.IdDynamics;
                value = 'Flexible';
                break;
            case 5:
                key = value.value.IdDynamics;
                value = 'Hibrida';
                break;
            default:
                key = value.value.IdDynamics;
                value = 'Presencial';


        }

        if (jQuery('#calcApp').data('desk') == 1) {
            dataLayer.push({
                'event': 'calcu_dsktp_modalidad'
            });
            jQuery('.carreramod').append('\
                      <div class="radio">\
                        <label>\
                          <input type="radio" class="clcCarrerasMod-dsk" data-carreramod="' + key + '"  data-idcosto="' + idCosto + '" name="modalidadRadios"  value="' + key + '" >\
                          ' + value + '\
                        </label>\
                      </div>');


        } else {

            jQuery("#secModalidades>.container>.row").append('<div data-carreramod="' + key + '" data-idcosto="' + idCosto + '" style="cursor:pointer;" class="item-available col-xs-12 col-md-8 clcCarrerasMod clcSelector"><span>' + value + '</span><i class="fa fa-chevron-right pull-right"></i></div>');
            orderElements(jQuery("#secModalidades>.container>.row"));

        };


    });

    if (jQuery('#calcApp').data('desk') == 1) {
        if (getParameterByName("utm_campus") == "ONL") {
            jQuery('.clcCarrerasMod-dsk[data-idcosto="POSON"]').click()
            jQuery('.clcCarrerasMod-dsk[data-idcosto="DIPON"]').click()
            jQuery('.clcCarrerasMod-dsk[data-idcosto="LICON"]').click()
        }
        if (qry.length == 1) {
                       jQuery('.clcCarrerasMod-dsk').attr('checked', 'checked');
            
                           if (jQuery('#calcApp').data('selectLinea') == "PREPARATORIA") {
                                //alert('repa');
                        //Add a comment to this line
                             jQuery('#PREPARATORIA>.linea-elementos>.col-md-7>.carreramod').append('<div class="row">\
                                                                <div class="col-md-8">\
                                                               <div class="row que-padre dark">¿Eres padre o tutor?<br></div>\
                                                                 <br> <select class="sel-carrera1" id="padretutor">\
                                                                 <option >Selecciona</option>\
                                                                 <option value="si">Si</option>\
                                                                 <option value="no">No</option>\
                                                                 </select>\
                                                                </div>\
                                                                </div>');
                               } else {
                            jQuery('.clcCarrerasMod-dsk').click().change();
                
                               }
                       //getCampus("666");
                           //irCampus();
                           //jQuery('.clcCarrerasMod-dsk').click();
                           //TODO CLick cambia a campus 
                
                           //alert('fin');
                //            jQuery('.clcCarrerasMod-dsk').click()
        }
    }

    return contMod;
}

function campusDynamics(campus) {
    switch (campus) {
        case "ATZ":
            return "ATIZAPAN";
            break;
        case "REY":
            return "LOS REYES";
            break;
        case "MAR":
            return "MARINA";
            break;
        case "ECA":
            return "ECATEPEC";
            break;
        case "SUR":
            return "SUR";
            break;
        case "CUI":
            return "CUITLAHUAC";
            break;
        case "LEO":
            return "LEON";
            break;
        case "REY":
            return "Los Reyes";
            break;
        case "TOL":
            return "TOLUCA";
            break;
        case "GDL":
            return "GUADALAJARA";
            break;
        case "ONL":
            return "EN LINEA";
            break
        case "QRO":
            return "QUERETARO";
            break
        default:
            return "MARINA";
    }
}
   
function getBecas() {
    console.log('--------------------------------------------------------------------');
    console.log('Ejecutando funcion getBecas ');
    var idcosto = jQuery('#calcApp').data('idCosto');
    var campus = jQuery('#calcApp').data('campus');
 
    if(campus=="REY" && idcosto=="LICPEDAG"){
        var idcosto='LIC';
    }     
 
    if (campus != "LEO") {   
        
              if(campus=="QRO" && idcosto=="FISIO"){
                campus = "QRO";
              }else{
                campus = "TODOS"; 
              }
                
        }

    /*Beca Toluca 11-06-2018 By SRP*/ 
    if( jQuery('#calcApp').data('campus')=="TOL" && jQuery('#calcApp').data('idCosto')=='NUTRI' ){ 
        campus="TOL";
    }
     /*Beca Toluca 11-06-2018 By SRP*/

    console.log('Latoluca'+campus+idcosto);
    if(jQuery('#calcApp').data('campus')=="TOL" && jQuery('#calcApp').data('idCosto')=='LIC' ){ 
        campus="TOL";
    }
 
        console.log(campus+" "+idcosto);
   
    //console.log('Campus:  '+campus+"  |idCosto:"+idcosto);

    var qryBecas = queryHandler(jsonBecas, '/*/' + idcosto + '/' + campus + '/*/');
    console.log("Query Becas" + '/*/' + idcosto + '/' + campus + '/*/');
    console.log(qryBecas);
    console.log("Campus: " + campus);
    var promedio = jQuery('#calcApp').data('promedio');
    console.log("Variables de entrada beca IdCosto:" + idcosto + " Campus:" + campus + " promedio:" + promedio);

    var rango = new Array();
    console.log(qryBecas.values());
    var contador = 0;
    jQuery.each(qryBecas.values(), function(key, value) {
        //console.log(value['promedio']);

        jQuery.each(value['promedio'], function(key1, value1) {

            rango[contador] = value;
            rango[contador]['beca'] = value['beca'];
        });
        contador++;
    });

    console.log(rango);
    console.log("rango");

    for (i = 0; i < rango.length; i++) {
        //console.log(rango[i]['promedio']);
 
        //if (x >= rango[i][0] && x <= rango[i][1]) {  }
        //console.log(promedio+" "+rango[i][0]+" "+rango[i][1]);
        if (parseFloat(promedio) >= parseFloat(rango[i]['promedio'][0]) && promedio <= parseFloat(rango[i]['promedio'][1])) {
            console.log("Entro al Rango: " + rango[i]['promedio'][0] + "| " + rango[i]['promedio'][1] + " Tu beca es " + rango[i]['beca'])
                //var beca=rango[i]['beca']+becafundador;
                console.log('PreparabecaGDL');
                var flagbeca="";  
        if (jQuery('#calcApp').data('campus') == "GDL") {
            var becafundador = 20;
            console.log('32d2d3wd223e');
            if (jQuery('#calcApp').data('selectMateria') == "787" ||
             /*Remove Ing Civil entra a beca + posibilidades 11-06-2018 By SRP*/    
             /*jQuery('#calcApp').data('selectMateria') == "883" ||*/ 
             jQuery('#calcApp').data('selectMateria') == "884" || 
             jQuery('#calcApp').data('selectMateria') == "588" ||
             jQuery('#calcApp').data('selectMateria') == 3954 || //Esbeltas GDL
             jQuery('#calcApp').data('selectMateria') == 3953 ||
             jQuery('#calcApp').data('selectMateria') == 3959 ||
             jQuery('#calcApp').data('selectMateria') == 3956 || 
             jQuery('#calcApp').data('selectMateria') == 3972 ||
             jQuery('#calcApp').data('selectMateria') == 3976 ||
             /*Remove Ing Software y Redes e Industriial y Sistemas LX Entra a beca + posibilidades 15-06-2018 By SRP Solicitado por Steff*/
             /*jQuery('#calcApp').data('selectMateria') == 3974 ||
             jQuery('#calcApp').data('selectMateria') == 3875 ||*/
             jQuery('#calcApp').data('selectMateria') == 3961 
            
             /*||   
             jQuery('#calcApp').data('selectMateria') == "1326"*/
            ) {
                var becafundador = 0;
                console.log('no aplico la beca fun');
                 flagbeca=0;
            } else {
                var becafundador = 20;
                console.log('SIII aplico la beca fun');
            }
        } else {

            var becafundador = 0;
        }
            //var becafundador=0;
            var beca = parseInt(parseInt(rango[i]['beca']) + parseInt(becafundador));
        }else{
            console.log('FLAGBCA'+flagbeca);
            if (jQuery('#calcApp').data('campus') == "GDL" && flagbeca!=0) {
                console.log("Guada");
                 if (promedio >= 6 && promedio <= 6.9) {

                /*Caso especial para Industrial y Administración 3974 ya que en la consicion de la linea 912 plancha la beca*/
                if (jQuery("#calcApp").data('selectLinea') != "INGENIERIA" || jQuery('#calcApp').data('selectMateria') == 3974) {
                    var beca = parseInt(20);
                    console.log('ewd3d32');

                    if (jQuery('#calcApp').data('selectMateria') == "787" ||
                    /*Remove Ing Civil entra a beca + posibilidades 11-06-2018 By SRP*/    
                    /*jQuery('#calcApp').data('selectMateria') == "883" ||*/ 
                    jQuery('#calcApp').data('selectMateria') == "884" || 
                    jQuery('#calcApp').data('selectMateria') == "588" ||
                    jQuery('#calcApp').data('selectMateria') == 3954 || //Esbeltas GDL
                    jQuery('#calcApp').data('selectMateria') == 3953 ||
                    jQuery('#calcApp').data('selectMateria') == 3959 ||
                    jQuery('#calcApp').data('selectMateria') == 3956 || 
                    jQuery('#calcApp').data('selectMateria') == 3972 ||
                    jQuery('#calcApp').data('selectMateria') == 3976 ||
                    /*Remove Ing Software y Redes e Industriial y Sistemas LX Entra a beca + posibilidades 15-06-2018 By SRP Solicitado por Steff*/
                    /*jQuery('#calcApp').data('selectMateria') == 3974 ||
                    jQuery('#calcApp').data('selectMateria') == 3875 ||*/
                    jQuery('#calcApp').data('selectMateria') == 3961 
                   
                    /*||   
                    jQuery('#calcApp').data('selectMateria') == "1326"*/
                   ){
                    var beca=0; 
                   }



                } else {
                    var beca = parseInt(0);
                }

                console.log('Fundadorhhh' + beca);
                 }
            }

        }
 
    }



    
console.log("________labeca"+beca);

    if (!beca) {
        var beca = 0; 
    }


     /*Liberación de Esbeltas FORANEO y ZM ApliCando mismos Costos y Becas 18-06-2018 By SRP*/ 
    if ( jQuery('#calcApp').data('selectMateria') == "3954" ||
         jQuery('#calcApp').data('selectMateria') == "3929" ||
         jQuery('#calcApp').data('selectMateria') == "3932" ||
         jQuery('#calcApp').data('selectMateria') == "3953" ||
         jQuery('#calcApp').data('selectMateria') == "3959" ||
         jQuery('#calcApp').data('selectMateria') == "3956" ||
         jQuery('#calcApp').data('selectMateria') == "3973" ||
         jQuery('#calcApp').data('selectMateria') == "3972" ||
         jQuery('#calcApp').data('selectMateria') == "3914" ||
         jQuery('#calcApp').data('selectMateria') == "3976" ||
         jQuery('#calcApp').data('selectMateria') == "3971" ||
         jQuery('#calcApp').data('selectMateria') == "3961" ||
         jQuery('#calcApp').data('selectMateria') == "3974" ||
         jQuery('#calcApp').data('selectMateria') == "3875" ||
         jQuery('#calcApp').data('selectMateria') == "1104" ||
         jQuery('#calcApp').data('selectMateria') == "1101" ||
         jQuery('#calcApp').data('selectMateria') == "1103" ||
         jQuery('#calcApp').data('selectMateria') == "1109" ||
         jQuery('#calcApp').data('selectMateria') == "1129" ||
         jQuery('#calcApp').data('selectMateria') == "1106" ||
         jQuery('#calcApp').data('selectMateria') == "1213" ||
         jQuery('#calcApp').data('selectMateria') == "1112" ||
         jQuery('#calcApp').data('selectMateria') == "1111" ||
         jQuery('#calcApp').data('selectMateria') == "1114" ||
         jQuery('#calcApp').data('selectMateria') == "1144" ||
         jQuery('#calcApp').data('selectMateria') == "1176" ||
         jQuery('#calcApp').data('selectMateria') == "1146" ||
         jQuery('#calcApp').data('selectMateria') == "1149" ||
         jQuery('#calcApp').data('selectMateria') == "879"
       ) {
 
            if ( jQuery("#ex14").slider('getValue') > 3 ){           
                jQuery('#calcApp').data('beca', beca);            
            } else{
                jQuery('#calcApp').data('beca', 0);
            }
    
    }
    /* End Liberación de Esbeltas FORANEO y ZM ApliCando mismos Costos y Becas 18-06-2018 By SRP*/

     /*Beca + posibilidades 11-06-2018 By SRP*/ 
     /*  
        Sistemas computacionales    889
        Industrial y de Sistemas    886
        Civil                       883
        
        LX GDL 15-06-2018
        Software y Redes            3875
        Industrial y Administración 3974
     */


    if ( jQuery('#calcApp').data('selectMateria') == "889" ||
         jQuery('#calcApp').data('selectMateria') == "886" ||
         jQuery('#calcApp').data('selectMateria') == "883" ||
         jQuery('#calcApp').data('selectMateria') == "3875" ||
         jQuery('#calcApp').data('selectMateria') == "3974"
       ) {

         if (jQuery('#calcApp').data('campus') == "GDL"){
            if (promedio >= 6 && promedio <= 6.9) {
                beca = parseInt(20);
            }
        }     
    }

     /*End Beca + posibilidades 11-06-2018 By SRP*/ 


    /*Liberación de Nueva Licenciatura Presencial en Cuitlahuac 20-06-2018 By SRP*/ 
    if ( jQuery('#calcApp').data('selectMateria') == "1346"
       ) {
 
            if ( jQuery("#ex14").slider('getValue') > 4 ){           
                jQuery('#calcApp').data('beca', beca);            
            } else{
                jQuery('#calcApp').data('beca', 0);
            }
    
    }
   /*End Liberación de Nueva Licenciatura Presencial en Cuitlahuac 20-06-2018 By SRP*/

   if ( jQuery('#calcApp').data('selectMateria') == "268"
       ) {
 
            if ( jQuery("#ex14").slider('getValue') > 2 ){           
                jQuery('#calcApp').data('beca', beca);            
            } else{
                jQuery('#calcApp').data('beca', 0);
            }
    
    }


   

    if (jQuery("#ex14").slider('getValue') < 5 && jQuery('#calcApp').data('idCosto') != 'INGON' && jQuery('#calcApp').data('idCosto') != 'BLUEM' && jQuery('#calcApp').data('idCosto') != 'DIP' && jQuery('#calcApp').data('idCosto') != 'DIPON' && jQuery('#calcApp').data('idCosto') != 'POSON' && jQuery('#calcApp').data('idCosto') != 'POS'&& jQuery('#calcApp').data('idCosto') != 'POSONCUAT') {
        
 
        
    //var esbeltas = ['3954','3932','3953','3959','3956','3972','3976','3974','3961','3973','3914','3971']; //ok
    //var a = esbeltas.indexOf(jQuery('#calcApp').data('selectMateria').toString());



    /*if(jQuery('#calcApp').data('selectMateria') == 3954) {
        var a = 0;
    }else if(jQuery('#calcApp').data('selectMateria') == 3932) {
        var a = 0;
    }else if(jQuery('#calcApp').data('selectMateria') == 3953) {
        var a = 0;
    }else if(jQuery('#calcApp').data('selectMateria') == 3959) {
        var a = 0;
    }else if(jQuery('#calcApp').data('selectMateria') == 3956) {
        var a = 0;
    }else if(jQuery('#calcApp').data('selectMateria') == 3972) {
        var a = 0;
    }else if(jQuery('#calcApp').data('selectMateria') == 3976) {
        var a = 0;
    }else if(jQuery('#calcApp').data('selectMateria') == 3974) {
        var a = 0;
    }else if(jQuery('#calcApp').data('selectMateria') == 3961) {
        var a = 0;
    }else if(jQuery('#calcApp').data('selectMateria') == 3973) {
        var a = 0;
    }else if(jQuery('#calcApp').data('selectMateria') == 3914) {
        var a = 0;
    }else if(jQuery('#calcApp').data('selectMateria') == 3971) {
        
    }else {
        var a = -1;
    }
    var esbforaneos = ["GDL", "LEO", "TOL", "QRO"];
    console.log(jQuery('#calcApp').data('campus'));
    var b = esbforaneos.indexOf(jQuery('#calcApp').data('campus'));
    
    if (a >= 0 && b >= 0 ) {
        jQuery('#calcApp').data('beca', beca);
    }else{
        console.log("MAterias  es menor a 5 y es idCosto dip o dipon o poson o pos y se le quito la beca");
        jQuery('#calcApp').data('beca', 0);
    }*/
    






    } else { 
        console.log('Entrodoqui_S_S_');          
 
        if (jQuery("#ex14").slider('getValue') >= 4) { 
            console.log("es mayor o igual a 4 se aplica becaa:" + beca);
            jQuery('#calcApp').data('beca', beca);
        }

        else if( jQuery('#calcApp').data('campus')=="LEO" ){
            if (  jQuery("#ex14").slider('getValue') > 4 && jQuery('#calcApp').data('selectMateria')==1305 ) { 
                jQuery('#calcApp').data('beca', beca);
            } else{
                //AQUI VA
                console.log('beca_:_:_'+beca)
                jQuery('#calcApp').data('beca', 0);
            }
        }

        /*Se correge issue para d mostrar beca a diplomados By SRP 02-04-2018*/
        else if ( jQuery("#ex14").slider('getValue') >= 3 && jQuery("#ex14").slider('getValue') < 4 ) { 
            /*alert(  "es igual a 3 se aplica beca DIPLOMADOS:" + beca );*/
            jQuery('#calcApp').data('beca', beca);    
        } 
        /*End Se correge issue para d mostrar beca a diplomados By SRP 02-04-2018*/

        

        else {
            jQuery('#calcApp').data('beca', 0);console.log("aassdad");
        }

    }

    if (jQuery('#calcApp').data('idCosto') == 'POSCUAT' || jQuery('#calcApp').data('idCosto') == 'POSONCUAT'|| jQuery('#calcApp').data('idCosto') == 'POSONCUATS' ||jQuery('#calcApp').data('idCosto') == 'POSCUATS'||jQuery('#calcApp').data('idCosto') == 'POSCUATS' ||  jQuery('#calcApp').data('idCosto') == 'DIP') {
        if (jQuery("#ex14").slider('getValue') >= 3) {

            jQuery('#calcApp').data('beca', beca);
            jQuery('.control-menos-mat').fadeIn();
            jQuery('.materiasCrtl').fadeIn();
console.log("asui7777"); 
        }
        console.log("asui89");
    }


    if(jQuery('#calcApp').data('selectMateria')==1149   || 
    jQuery('#calcApp').data('selectMateria')==1146      ||
    jQuery('#calcApp').data('selectMateria')==3875      ||
    jQuery('#calcApp').data('selectMateria')==3874      ||
    jQuery('#calcApp').data('selectMateria')==3876     
    ) {
        console.log('Becas especiales');
        if(jQuery("#ex14").slider('getValue') >= 4 ){

            jQuery('#calcApp').data('beca', beca);
        }else{
            jQuery('#calcApp').data('beca', 0)
        }
    }

    jQuery('.app-desc').html(beca + "%");
}



/*function format2(n, currency) {
    return currency + " " + n.toFixed(0).replace(/(\d)(?=(\d{3})+\.)/g, "$1,");
}*/

function format2(n, currency) {
    return currency + " " + n.toFixed(0).replace(/./g, function(c, i, a) {
        return i > 0 && c !== "." && (a.length - i) % 3 === 0 ? "," + c : c;
    });
}


function getDuracion(anio) {

    /***********LIMPIA LAS MATERIAS DE LA SELECCION**********/
        jQuery('#calcApp').each(function(){
            for(data in jQuery(this).data())
        if(!data.search('mat_')){
         jQuery('#calcApp').removeData(data,'');
        console.log(data); // returns confirmID so element as an attribute `data-confirmID`
        jQuery(this);
        }

        });

    var anioArr = {};
    var tipoPago = {};
    var qryStrCostos = '/*/' + jQuery('#calcApp').data('idCosto') + '/costoMateria';
    var qryResults = queryHandler(jsonCostos, qryStrCostos);
    var losPagos = qryResults[0].value;
    console.log("LOS PAGOS");
    console.log(losPagos);
    var theKey = Object.keys(losPagos);
    var pago = {};
    var costos = [];
    var ahorro = {};
    var classi, cont = 0;
    var lineaNegocio = jQuery('#calcApp').data('idCosto');
    var qryCostos="";
    
    var qryCostos = queryHandler(jsonCostos, '/*/' + lineaNegocio + '/duracion');
    var beca = jQuery('#calcApp').data('beca');

    console.log('Los añosss'+anio);
    console.log(qryCostos.value());

    //ESBELTAS
    //var esbeltas = ['3954','3932','3953','3959','3956','3972','3976','3974','3961','3973','3914','3971']; //ok
    //var a = esbeltas.indexOf(jQuery('#calcApp').data('selectMateria').toString());



    /*if(jQuery('#calcApp').data('selectMateria') == 3954) {
        var a = 0;
    }else if(jQuery('#calcApp').data('selectMateria') == 3932) {
        var a = 0;
    }else if(jQuery('#calcApp').data('selectMateria') == 3953) {
        var a = 0;
    }else if(jQuery('#calcApp').data('selectMateria') == 3959) {
        var a = 0;
    }else if(jQuery('#calcApp').data('selectMateria') == 3956) {
        var a = 0;
    }else if(jQuery('#calcApp').data('selectMateria') == 3972) {
        var a = 0;
    }else if(jQuery('#calcApp').data('selectMateria') == 3976) {
        var a = 0;
    }else if(jQuery('#calcApp').data('selectMateria') == 3974) {
        var a = 0;
    }else if(jQuery('#calcApp').data('selectMateria') == 3961) {
        var a = 0;
    }else if(jQuery('#calcApp').data('selectMateria') == 3973) {
        var a = 0;
    }else if(jQuery('#calcApp').data('selectMateria') == 3914) {
        var a = 0;
    }else if(jQuery('#calcApp').data('selectMateria') == 3971) {
        
    }else {
        var a = -1;
    }*/
    



    /*var esbforaneos = ["GDL", "LEO", "TOL", "QRO"];
    console.log(jQuery('#calcApp').data('campus'));
    var b = esbforaneos.indexOf(jQuery('#calcApp').data('campus'));
    console.log("LAfinal");
    console.log(qryCostos[0]);
    console.log(qryCostos);
    console.log(qryCostos[0].value[3]); 

    if (a >= 0 && b >= 0 && qryCostos[0].value[3]) {
        qryCostos[0].value[3].Materias = 5;
        console.log('ENTREE_ESBELTAS_1_anios-a-');
    }
    if (a >= 0 && b >= 0 && qryCostos[0].value[4]) {
        qryCostos[0].value[4].Materias = 4;
        console.log('ENTREE_ESBELTAS_4_anios-b-');
    }*/
    //esbelltas

    if (anio) {
        jQuery.map(qryCostos.value(), function(val, i) {
            jQuery('#calcApp').data('mat_' + i + "_" + val['Materias'], 'mat_' + i + "_" + val['Materias']);
            //var materiasaCotizar=(jQuery('#calcApp').data('materias'))?jQuery('#calcApp').data('materias'):val['Materias'];
            //console.log("valor:"+val+" i="+i);
            var materiasaCotizar = val['Materias'];
            console.log("materias a cotizar" + materiasaCotizar);
            anioArr[i] = {
                'materias': materiasaCotizar
            };
            jQuery('.materias-val').html(materiasaCotizar);
            jQuery.map(losPagos, function(value, a) {
                anioArr[i][a] = value[jQuery('#calcApp').data('campus')] * materiasaCotizar;
                console.log("EACH LOS PAGOS");
                console.log(value[jQuery('#calcApp').data('campus')]);
                anioArr[i][a + "-beca"] = ((value[jQuery('#calcApp').data('campus')] * materiasaCotizar) * ((100 - beca) / 100)); //pagos con Becas
            });
        });

    } else {
        jQuery.map(qryCostos.value(), function(val, i) {
            jQuery('#calcApp').data('mat_' + i + "_" + val['Materias'], 'mat_' + i + "_" + val['Materias']);
            var materiasaCotizar = (jQuery('#calcApp').data('materias')) ? jQuery('#calcApp').data('materias') : val['Materias'];
            //var materiasaCotizar=val['Materias'];
            console.log("2materias a cotizar" + materiasaCotizar);
            anioArr[i] = {
                'materias': materiasaCotizar
            };
            jQuery('.materias-val').html(materiasaCotizar);
            jQuery.map(losPagos, function(value, a) {
                if (jQuery('#calcApp').data('idCosto') == "ESP") {
                    materiasaCotizar = 1;
                    anioArr[i][a] = value[jQuery('#calcApp').data('campus')] * materiasaCotizar;
                    console.log("aaaaaaaaaaniooooo");
                    console.log(anio);
                } else {
                    anioArr[i][a] = value[jQuery('#calcApp').data('campus')] * materiasaCotizar;
                }


                anioArr[i][a + "-beca"] = ((value[jQuery('#calcApp').data('campus')] * materiasaCotizar) * ((100 - beca) / 100)); //pagos con Becas
            });
        });
    }   
 


    jQuery('#app-esquemapagos>.row>.botones_anios').empty();
    jQuery('#app-resumen').empty();
    //JEAB Añadir conclusion de años
    jQuery('.botones_anios').append('<span class="anios-carrera">Concluye</span>');
    console.log("ARREGLOTI:");


    //var esbeltas = ['3954','3932','3953','3959','3956','3972','3976','3974','3961','3973','3914','3971']; //ok
    //var a = esbeltas.indexOf(jQuery('#calcApp').data('selectMateria').toString());


    /*if(jQuery('#calcApp').data('selectMateria') == 3954) {
        var a = 0;
    }else if(jQuery('#calcApp').data('selectMateria') == 3932) {
        var a = 0;
    }else if(jQuery('#calcApp').data('selectMateria') == 3953) {
        var a = 0;
    }else if(jQuery('#calcApp').data('selectMateria') == 3959) {
        var a = 0;
    }else if(jQuery('#calcApp').data('selectMateria') == 3956) {
        var a = 0;
    }else if(jQuery('#calcApp').data('selectMateria') == 3972) {
        var a = 0;
    }else if(jQuery('#calcApp').data('selectMateria') == 3976) {
        var a = 0;
    }else if(jQuery('#calcApp').data('selectMateria') == 3974) {
        var a = 0;
    }else if(jQuery('#calcApp').data('selectMateria') == 3961) {
        var a = 0;
    }else if(jQuery('#calcApp').data('selectMateria') == 3973) {
        var a = 0;
    }else if(jQuery('#calcApp').data('selectMateria') == 3914) {
        var a = 0;
    }else if(jQuery('#calcApp').data('selectMateria') == 3971) {
        
    }else {
        var a = -1;
    }*/


    /*var esbforaneos = ["GDL", "LEO", "TOL", "QRO"];
    console.log(jQuery('#calcApp').data('campus'));
    var b = esbforaneos.indexOf(jQuery('#calcApp').data('campus'));
    
    if (a >= 0 && b >= 0 && anioArr[3]) {
        anioArr["3"].materias = 5;
        console.log('ENTREE_ESBELTAS_1_anios-q');
    }
    if (a >= 0 && b >= 0 && anioArr[4]) {
        anioArr["4"].materias = 4;
        console.log('ENTREE_ESBELTAS_4_anios-w');
    }*/
    
    // console.log(anioArr);
    // anioArr[3].materias = 9999;
    console.log(anioArr);
    console.log("__-___-___-___-");
    jQuery.map(anioArr, function(val, i) {
        console.log("valuesanioArr:" + val + " i:" + i);
        console.log(val);
        if (val['4pagos'] === undefined || val['4pagos'] === null) {
            pagoDisponible = '6pagos';
            mensualidades = 6;
        } else {
            pagoDisponible = '4pagos';
            mensualidades = 4;
        }
        console.log("PAGODISPO" + pagoDisponible);
        console.log("MENSUALIDADES" + mensualidades);


        if (cont == 0 && pagoDisponible != '6pagos' && Object.keys(anioArr).length != 1) {
            var inicio = 'style="display:none;"'
            classi = "btn-default";
            /* jQuery('#calcApp').data('planSelec',key);
             jQuery('#calcApp').data('CostoSelec',value);*/
        } else {
            var inicio = "";
            console.log("GUARDANDO EN DATA TERMINO"+i);

            
            jQuery('#calcApp').data("termino", i);
            classi = "btn-primary";
            /*jQuery('#calcApp').data('planRecom',key);
            jQuery('#calcApp').data('CostoRecom',value);*/

        }
        cont++;
        console.log("ARR COMPLETO: ");
        console.log(val);
        console.log("PAGO DISPONIBLE ANTES DE ARREGLO" + pagoDisponible);

        var tipoDuracion = (jQuery('#calcApp').data('idCosto') == 'DIPON' || jQuery('#calcApp').data('idCosto') == 'DIP') ? '4 meses' : i + " años";
        console.log("________TipoDuracion:"+tipoDuracion);
        var pagoMensualconBeca = (jQuery('#calcApp').data('beca')) ? val[pagoDisponible + '-beca'] : val[pagoDisponible];




        console.log( "RESET BECA"); 
        /*Inicia Reset Beca By SRP 03-07-2018*/

        if ( jQuery('#calcApp').data('idCosto') == 'POSCUAT' || 
             jQuery('#calcApp').data('idCosto') == 'POSONCUAT'|| 
             jQuery('#calcApp').data('idCosto') == 'POSONCUATS' ||
             jQuery('#calcApp').data('idCosto') == 'DIP' ||
             jQuery('#calcApp').data('idCosto') == 'DIPON' ||
             jQuery('#calcApp').data('idCosto') == 'POS' ||
             jQuery('#calcApp').data('idCosto') == 'POSON' ||
             jQuery('#calcApp').data('idCosto') == 'ESP' ||
             jQuery('#calcApp').data('idCosto') == 'BLUEM'

        ) {
            if (jQuery("#ex14").slider('getValue') >= 3) {

                jQuery('#calcApp').data('beca', beca);
                jQuery('.control-menos-mat').fadeIn();
                jQuery('.materiasCrtl').fadeIn();
                console.log("asui7777");
            } else{
              jQuery('#calcApp').data('beca', 0);
            }
          console.log("Para Posgrado");

        }

        else if(  jQuery('#calcApp').data('idCosto') == "LICEJEC" ||
                    jQuery('#calcApp').data('idCosto') == "INGEJEC" ||
                    jQuery('#calcApp').data('idCosto') == "LICON" ||
                    jQuery('#calcApp').data('idCosto') == "INGON"

                ){
                if ( jQuery("#ex14").slider('getValue') > 3 ) {
                      jQuery('#calcApp').data('beca');
                    } else{
                      jQuery('#calcApp').data('beca', 0); 
                    }
        }

        else if ( jQuery('#calcApp').data('idCosto') == "PREPA" ||
                  jQuery('#calcApp').data('idCosto') == "LIC" || 
                  jQuery('#calcApp').data('idCosto') == "ING" ||
                  jQuery('#calcApp').data('idCosto') == "NUTRI" ||
                  jQuery('#calcApp').data('idCosto') == "ENFER" ||
                  jQuery('#calcApp').data('idCosto') == "FISIO" ||
                  jQuery('#calcApp').data('idCosto') == "TURI" ||
                  jQuery('#calcApp').data('idCosto') == "LICPSICOLOG" ||
                  jQuery('#calcApp').data('idCosto') == 'GASTRO' ||
                  jQuery('#calcApp').data('idCosto') == "LICPEDAG"
                ){

                    if ( jQuery("#ex14").slider('getValue') > 4 ) {
                      jQuery('#calcApp').data('beca');
                    } else{
                      jQuery('#calcApp').data('beca', 0); 
                    }
            console.log("Para Licenciaturas");
        } 

        /*Caso Para Cirujano Dentista By SRP 20-07_2018*/
        else if( jQuery('#calcApp').data('idCosto') == "DENT" 
                ){

            if ( jQuery("#ex14").slider('getValue') > 4 ) {
                      jQuery('#calcApp').data('beca');
            } else{
              jQuery('#calcApp').data('beca', 0);

            }
        }
        /*Caso Para Cirujano Dentista By SRP 20-07_2018*/

        else{          
          jQuery('#calcApp').data('beca', 0); 
        }
        /*End Inicia Reset Beca By SRP 03-07-2018*/





        console.log("BECA:" + jQuery('#calcApp').data('beca'));
        console.log("BECA -beca:" + val[pagoDisponible + '-beca']);
        console.log("BECA -valdispo:" + val[pagoDisponible]);
        console.log("PAGOOOOOO MENSUAL QUE SE MUESTRA EN EL FRONT: ");
        console.log(pagoMensualconBeca);
        jQuery('#calcApp').data('pago_mensual_front', pagoMensualconBeca)
        var pagoContadoconBeca = (jQuery('#calcApp').data('beca')) ? val['1pago-beca'] : val['1pago'];
        var ahorroConBeca = (val[pagoDisponible + '-beca'] > 0) ? val[pagoDisponible] - val[pagoDisponible + '-beca'] : 0;
        var ahorrodeContadoconBeca = (val[pagoDisponible + '-beca'] > 0) ? ahorroConBeca + (val[pagoDisponible + '-beca'] * mensualidades) - val['1pago-beca'] : ahorroConBeca + val['1pago'] - (val[pagoDisponible] * mensualidades);
        var ahorrodeContadoconBeca = val['1pago'] - val['1pago-beca'];

        if (jQuery('#calcApp').data('idCosto') == "ESP" ) {
            // if (jQuery('#calcApp').data('idCosto') == "POSCUAT"){
            //     jQuery('#app-esquemapagos>.row>.botones_anios').append('<button  type="submit" class=" btn  btn-sm espacio btn-primary">1 año 8 meses</button>');

            //     }else{
                jQuery('#app-esquemapagos>.row>.botones_anios').append('<button  type="submit" class=" btn  btn-sm espacio btn-primary">2 años</button>');

                // }
            jQuery('.materiasCrtl').fadeOut();
        } 

        else if (jQuery('#calcApp').data('idCosto') == "POSCUAT") {
                jQuery('#app-esquemapagos>.row>.botones_anios').append('<button data-esquema="' + i + '" type="submit" class=" btn-esquema-unselected btn-esquema btn  btn-sm espacio ' + classi + '"> 1 año 8 meses</button>');
 
            }
        else if ((jQuery('#calcApp').data('idCosto') == "DIP") || (jQuery('#calcApp').data('idCosto') == "DIPON")){
                jQuery('#app-esquemapagos>.row>.botones_anios').append('<button data-esquema="' + i + '" type="submit" class=" btn-esquema-unselected btn-esquema btn  btn-sm espacio ' + classi + '"> 4 meses</button>');

            }

        else{
                jQuery('#app-esquemapagos>.row>.botones_anios').append('<button data-esquema="' + i + '" type="submit" class=" btn-esquema-unselected btn-esquema btn  btn-sm espacio ' + classi + '">' + tipoDuracion + '</button>');

            }

        /*else {

            if (jQuery('#calcApp').data('idCosto') == "POSCUAT") {
                jQuery('#app-esquemapagos>.row>.botones_anios').append('<button data-esquema="' + i + '" type="submit" class="btn-esquema-unselected btn-esquema btn  btn-sm espacio ' + classi + '"> 1 año 8 meses</button>');

            }
	    if ((jQuery('#calcApp').data('idCosto') == "DIP") || (jQuery('#calcApp').data('idCosto') == "DIPON")){
                jQuery('#app-esquemapagos>.row>.botones_anios').append('<button data-esquema="' + i + '" type="submit" class="btn-esquema-unselected btn-esquema btn  btn-sm espacio ' + classi + '"> 4 meses</button>');

            }
	    else{
                jQuery('#app-esquemapagos>.row>.botones_anios').append('<button data-esquema="' + i + '" type="submit" class="btn-esquema-unselected btn-esquema btn  btn-sm espacio ' + classi + '">' + tipoDuracion + '</button>');

            }

            jQuery('.materiasCrtl').fadeIn();
        }*/

        if (jQuery('#calcApp').data('idCosto') == 'POSCUAT') {
 
            jQuery('#calcApp').data('beca', beca);

            jQuery('.control-menos-mat').fadeIn();
            jQuery('.materiasCrtl').fadeIn();

        }
        if (jQuery('#calcApp').data('campus') == "GDL") {
            var txtgdl = '         *Incluye Beca +posibilidades';
        } else {
            var txtgdl = ""; 
        }
 
        if(jQuery('#calcApp').data('idCosto') == 'POS' ||jQuery('#calcApp').data('idCosto') == 'ESP' || jQuery('#calcApp').data('idCosto') == 'DIP' || jQuery('#calcApp').data('idCosto') == 'DIPON' || jQuery('#calcApp').data('idCosto') == 'POSON' || jQuery('#calcApp').data('idCosto') == 'DIPON'){
            var pago_tipo="Semestral";
        }else{
            var pago_tipo="Cuatrimestral";
        }

        jQuery('#app-resumen').append('<div ' + inicio + ' class="row termino-' + i + '  p-mensual">              <div class="col-xs-6 col-sm-8 renglones texto-minimo renglones-calculadora">Pago Mensual</div> <div class="col-xs-6 col-sm-4 montserrat numero-calcu-resultado renglones-sl">' + format2(pagoMensualconBeca, "$") + '*</div></div>');
        jQuery('#app-resumen').append('<div style="display:none;" class="row termino-' + i + '  p-contado">    <div class="col-xs-6 col-sm-8 renglones texto-minimo renglones-calculadora">Pago '+pago_tipo+'</div> <div class="col-xs-6 col-sm-4 montserrat numero-calcu-resultado renglones-sl">' + format2(pagoContadoconBeca, "$") + '*</div></div>');
        jQuery('#app-resumen').append('<div ' + inicio + ' class="row margen-superior termino-' + i + ' ">                         <div class="col-xs-6 col-sm-8 renglones texto-minimo renglones-calculadora">Beca del:</div><div class="col-xs-6 col-sm-4 montserrat numero-calcu-resultado renglones-sl">' + jQuery('#calcApp').data('beca') + '%*</div></div>');
        if (i == 3 || i == 4 || i == 2 || i == '6m') {
            jQuery('#app-resumen').append('<div ' + inicio + '  class="insc-reinsc row margen-superior termino-' + i + '" >                         <div class="col-xs-6 col-sm-8 renglones texto-minimo renglones-calculadora texto-naranja top-renglon margin-top-inscripcion">Reinscripción / Inscripción :</div><div class="col-xs-6 col-sm-4 montserrat numero-calcu-resultado renglones-sl texto-naranja margen-arriba mb-inscripcion" style="font-size: 20px;margin-top: 20px;">Sin Costo*</div></div>');
        }
        jQuery('#app-resumen').append('<div ' + inicio + ' class="row termino-' + i + '  p-mensual ">               <div class="col-xs-6 col-sm-8 renglones-texto texto-minimo renglones-calculadora c-total-ahorro">Total Ahorro</div><div class="col-xs-6 col-sm-4 montserrat numero-calcu-parrafo renglones-sl c-total-ahorro-val">' + format2(ahorroConBeca, "$") + '</div></div>');
        jQuery('#app-resumen').append('<div style="display:none;"  class="row termino-' + i + ' p-contado ">    <div class="col-xs-6 col-sm-8 renglones-texto texto-minimo renglones-calculadora">Total Ahorro</div><div class="col-xs-6 col-sm-4 montserrat numero-calcu-parrafo renglones-sl">' + format2(ahorrodeContadoconBeca, '$') + '</div></div>');
        jQuery('#app-resumen').append('<div ' + inicio + ' class="row termino-' + i + ' ">                         <div class="col-xs-6 col-sm-8 renglones-texto texto-minimo renglones-calculadora"># Materias a cursar</div><div class="c-mat-cl col-xs-6 col-sm-4 montserrat renglones-sl numero-calcu-parrafo mat-calc-' + i + '">' + Number(val['materias']) + '</div></div>');

        //jQuery('#app-resumen').append('<div '+inicio+' class="row termino-'+i+' ">                         <div class="col-xs-12 "><p class="condiciones" style="font-size:12px">*Precios vigentes hasta el 23 de enero del 2016</p></div></div>');

    });

    jQuery('.btn-esquema').removeClass('btn-primary');
    jQuery('.btn-esquema').addClass('btn-esquema-cirujano');




    if (anio) {
console.log("anio: "+anio);
        //jQuery('#calcApp').data('materias',materiasaCotizar);
        jQuery('.materias-val').html(jQuery('.mat-calc-' + anio).html());

        jQuery("[data-esquema='" + anio + "']").removeClass('btn-esquema-cirujano');
        jQuery("[data-esquema='" + anio + "']").addClass('btn-esquema-selected');
        //JEAB jQuery("[data-esquema='"+anio+"']").addClass('btn-primary');


    } else {
        console.log('hereeeeeeee!!!!');


        jQuery("[data-esquema]").each(function() {
            if (jQuery('#calcApp').data("mat_" + jQuery(this).data('esquema') + "_" + jQuery('#calcApp').data("materias"))) {
                jQuery("[data-esquema='" + jQuery(this).data('esquema') + "']").removeClass('btn-esquema-cirujano');
                jQuery("[data-esquema='" + jQuery(this).data('esquema') + "']").addClass('btn-esquema-selected');
                //JEAB jQuery("[data-esquema='"+jQuery(this).data('esquema')+"']").addClass('btn-primary');
            }
        });


        if (jQuery('#calcApp').data('selectMateria') == 3875 ||
            jQuery('#calcApp').data('selectMateria') == 1149 ||
            jQuery('#calcApp').data('selectMateria') == 1146 ||
            jQuery('#calcApp').data('selectMateria') == 3874 ||
            jQuery('#calcApp').data('selectMateria') == 1122 ||
            jQuery('#calcApp').data('selectMateria') == 1176 ||
            jQuery('#calcApp').data('selectMateria') == 3876 ) {
                // if ( jQuery("#ex14").slider('getValue') < 7 ) {
                //     alert('Agregando Clase');
                //           jQuery('btn-esquema').addClass('btn-esquema-cirujano');
                // } else{
                //     alert('Quitando Clase');
                //     jQuery('btn-esquema').removeClass('btn-esquema-cirujano');
    
                // }
                if (jQuery("#ex14").slider('getValue') == 4) {
                    jQuery("[data-esquema='3']").removeClass('btn-esquema-selected');
                    jQuery("[data-esquema='3']").addClass('btn-esquema-cirujano');

                    jQuery("[data-esquema='4']").removeClass('btn-esquema-cirujano');
                    jQuery("[data-esquema='4']").addClass('btn-esquema-selected');
                    console.log('entro en el 4');
                }else if(jQuery("#ex14").slider('getValue') == 5) {
                    jQuery("[data-esquema='4']").removeClass('btn-esquema-selected');
                    jQuery("[data-esquema='4']").addClass('btn-esquema-cirujano');

                    jQuery("[data-esquema='3']").removeClass('btn-esquema-cirujano');
                    jQuery("[data-esquema='3']").addClass('btn-esquema-selected');
                    console.log('entro en el 4');
                }else{
                    jQuery("[data-esquema='3']").removeClass('btn-esquema-selected');
                    jQuery("[data-esquema='3']").addClass('btn-esquema-cirujano');
                    jQuery("[data-esquema='4']").removeClass('btn-esquema-selected');
                    jQuery("[data-esquema='4']").addClass('btn-esquema-cirujano');

                }
        }
        



    }

    /*Leyenda Para precios en Campus Los Reyes By SRP 05-07-2018*/
    /*if( jQuery('#calcApp').data('campus') == "REY" ){
        var txtCostos = "*Precios de colegiaturas vigentes hasta el 15 de julio del 2018<br>";
    } else{
        var txtCostos = "";
    }*/
    /*Leyenda Para precios en Campus Los Reyes By SRP 05-07-2018*/

    if (jQuery('#calcApp').data('campus') == "GDL") {
        var txtgdl = '         *Incluye Beca +posibilidades';
    } else {
        var txtgdl = "";
    }
    var inicio = '';
    jQuery('#app-resumen').append('\
            <div class="row margin-top-mobile">\
                <div class="col-xs-12 col-sm-6  col-xs-offset-0">\
                    <div class="col-xs-6 col-sm-6">\
                        <a  data-gtm-tr="admision_calcu" data-gtm-pos="calculadora | resultados" href="//www.unitec.mx/admision-unitec/" class=" btn btn-primary btn-sm">INICIA TU PROCESO DE ADMISIÓN\
                        </a>\
                    </div>\
                <div class="col-xs-6 col-md-6 div-tipo-pago">\
                <div class="col-xs-12 col-md-12" style="font-size:16px; margin-left:10px; margin-bottom: 4px;">¿Pago de contado?\
                </div>\
                <div class="col-xs-12 col-md-4 no-margin" style="margin-left: 7%!important;"><div class="toggle toggle-light">\
                </div>\
                </div>\
                </div>\
                </div>\
                \
                <div class="col-xs-12 col-sm-6  col-xs-offset-0 nvo-div-terminos" id="nvo_div_terminos" style="margin-top: -60px;">\
                    <div class="col-xs-6 col-sm-6 col-recu hidden-xs hidden-sm hidden-md hidden-lg">\
                    <a  data-gtm-tr="admision_calcu" data-gtm-pos="calculadora | resultados" href="//www.unitec.mx/admision-unitec/" class="  btn btn-primary btn-sm">INICIA TU PROCESO DE ADMISIÓN\
                    </a>\
                    </div>\
                ');



    jQuery('#nvo_div_terminos').append('<div ' + inicio + ' class="row termino-' + i + ' " id="terminos-condiciones">                         <div class="col-xs-12 "><p class="condiciones"></p><div class="row text-condiciones" style="color: #a9a6a6;font-size: 12px;margin-left: 12px!important; margin: 30px auto 20px auto">*Costos vigentes para el per&iacute;odo junio-diciembre 2018' + txtgdl + '<br>*El porcentaje de beca se asignará conforme al promedio general del nivel de estudios anterior al que se inscriba</div></div></div>');


    jQuery('.toggle').toggles({
        drag: true, // allow dragging the toggle between positions
        click: true, // allow clicking on the toggle
        text: {
            on: '', // text for the ON position
            off: '' // and off
        },
        on: false, // is the toggle ON on init
        animate: 250, // animation time
        transition: 'swing', // animation transition,
        checkbox: null, // the checkbox to toggle (for use in forms)
        clicker: null, // element that can be clicked on to toggle. removes binding from the toggle itself (use nesting)
        /*width: 10, // width used if not set in css
        height: 20, // height if not set in css*/
        type: 'compact' // if this is set to 'select' then the select style toggle will be used
    });
    // Getting notified of changes, and the new state:
    jQuery('.toggle').on('toggle', function(e, active) {
        if (!active) {
            jQuery('.p-contado').hide();
            jQuery('.p-mensual.termino-' + jQuery('#calcApp').data("termino")).show();
        } else {
            jQuery('.p-mensual').hide();
            jQuery('.p-contado.termino-' + jQuery('#calcApp').data("termino")).show();
        }
    });
    // console.log(anioArr);
}

function gotoPlan() {
  var categoria=jQuery('#calcApp').data('selectCategoria');
  var qry = queryHandler(linkCategorias, '/*/'+categoria);

  if(qry.value().indexOf("prepa") != -1) {
    window.open("//www.unitec.mx/prepa/#perfil");
  }else {
    window.open("http://www.unitec.mx"+qry.value()+"#perfil");
  }
}
function showAdmision()
{
    jQuery("#recu-modal").modal("show");
    return;
}
function closeRecuModal()
{
    window.open("//www.unitec.mx/admision-unitec/", '_blank');
    jQuery("#recu-modal").modal("hide");
    return;
}
function getCosto() {
    /*
        //console.log("----------------------------------------------------------------------");
        //console.log("Ejecutando Funcion getCosto");

        var lineaNegocio=jQuery('#calcApp').data('idCosto');
        var noMaterias=jQuery('#calcApp').data('materias');
        var anios;
        var materias;
        console.log("Variebles de entrada: linea de Negocio: "+lineaNegocio+" |noMaterias: "+noMaterias);

        //debuglog("\nObteniendo Costos");
        //debuglog("Linea de negocio: "+lineaNegocio+" Materias:"+noMaterias);

        if(!noMaterias){ //si no trae materias, tendra que mostrar las materias de el mayor años de duracion
          //debuglog('Aun no interactua con materias');
         // var qryCostos = queryHandler(jsonCostos, '/ '+lineaNegocio+'/duracion');
          //console.log("El usuario no eligio materiasl y el query es "+'/'+lineaNegocio+'/duracion');
          if(noMaterias.length==0){
            console.log("No se encontraron conicidencias");
            errorCalculadora();}
          var contador=0;
          console.log(qryCostos[0].value);
            jQuery.each( qryCostos[0].value, function( key, value ) {
              if(contador==0){debuglog("Buscando en primer resultado en query: "+key);
                              jQuery('.resAnios1').html(key)};
              if(contador==1){
                debuglog("Buscando en segundo resultado en query: "+key);
                jQuery('.resAnios2').html(key)};
                  jQuery.each( value, function( key, value ) {
                   if(contador==0){jQuery('.materias1').html(value);
                                  jQuery('#calcApp').data('materias',value);
                                  getPagos(lineaNegocio, value, jQuery('#calcApp').data('beca'), jQuery('#calcApp').data('campus'));
                                }
                   if(contador==1){jQuery('.materias2').html(value);}
                  });
                  contador++;
            });
        }else{
          debuglog("El usuario eligio materias: "+noMaterias);
          console.log('GETPAGOS');


          getPagos(lineaNegocio, jQuery('#calcApp').data('materias'), jQuery('#calcApp').data('beca'), jQuery('#calcApp').data('campus'));
        }*/
}


function debuglog(towrite) {
    jQuery.ajax({
        url: "../wp-content/themes/temaunitec/calculadora/ajaxdebug.php",
        type: 'POST',
        cache: false,
        data: "accion=" + towrite,
        progress: function() {},
        success: function(res) {}
    });

}

function debugErrors(data){
      jQuery.ajax({
        url: "../wp-content/themes/temaunitec/calculadora/ajaxdebugerrors.php",
        type: 'POST',
        cache: false,
        data: "accion="+data,
        progress: function(){},
        success: function(res){}
      });
}

function debuglogname(towrite) {
    jQuery.ajax({
        url: "../wp-content/themes/temaunitec/calculadora/ajaxdebugname.php",
        type: 'POST',
        cache: false,
        data: "accion=" + towrite,
        progress: function() {},
        success: function(res) {}
    });

}

function debugleave(towrite) {
    var clientId = "";
    ga(function(tracker) {
        clientId = tracker.get('clientId');
    });

    jQuery.ajax({
        url: "../wp-content/themes/temaunitec/calculadora/ajaxdebugleave.php",
        type: 'POST',
        cache: false,
        data: "seccion=" + towrite + " | " + navigator.platform + " | " + navigator.userAgent + " | " + clientId,
        progress: function() {},
        success: function(res) {}
    });

}

/**
*Da efecto de enfasis a los elementos del form que tienen error o falta completarse
*@param {string} id - El id del elemento DOM.
*/
function blinkFieldForm(id){
      jQuery("#"+id).addClass("blink-animation");
      setTimeout(function(){jQuery("#"+id).removeClass("blink-animation");},2500);
}

function calculaCostos(catCosto, noMaterias, descuento, campus) 
{
    debuglog("\nEjecutando Funcion calculaCostos")
    console.log("%cEjecutando Funcion calculaCostos", "color: red");
    console.log("--------------------Ejecutando Funcion calculaCostos----------------------------------");
    console.log("Variables de entrada:");
    console.log("catCosto:" + catCosto);
    console.log("noMaterias:" + noMaterias);
    console.log("Descuento:" + descuento)
    console.log("Campus:" + campus);

    //VARIABLES GENERALES
    if (!catCosto) {
        alert("Selecciona la carrera y modalidad deseada");

        throw ("No existe el catCosto");
    }
    var qryStrCostos = '/*/' + catCosto + '/costoMateria';
    //console.log("La"+qryStrCostos);
    var qryResults = queryHandler(jsonCostos, qryStrCostos);
    var losPagos = qryResults[0].value;
    var theKey = Object.keys(losPagos);
    var pago = {};
    var costos = [];
    var ahorro = {};
    var classi;
    for (var i = 0; i < Object.keys(losPagos).length; i++) {

        if (Array.isArray(losPagos[theKey[i]][campus])) {

            var variosPagos = losPagos[theKey[i]][campus];

            jQuery.each(variosPagos, function(key, value) {

                var costoPorMateria = value;
                jQuery('#calcApp').data('costoPorMateria', value);

                if (catCosto == "ESP") {
                    var pagoNormal = costoPorMateria;
                } else {
                    var pagoNormal = costoPorMateria * noMaterias;
                }

                var descuentoBeca = pagoNormal * (descuento / 100);
                var totalPagar = pagoNormal - descuentoBeca;

                if (catCosto == "DIPON" || catCosto == "DIP") {
                    var elAhorro = descuentoBeca * 6;
                    debuglog('cat Costo:' + catCosto + " el ahorro: " + elAhorro);

                } else {

                    var elAhorro = descuentoBeca * 4
                    debuglog('cat Costo:' + catCosto + " el ahorro: " + elAhorro);
                }

                costos.push(totalPagar);
                pago[theKey[i]] = pagoNormal;
                ahorro[theKey[i]] = elAhorro;

            });


        } else {

            var costoPorMateria = losPagos[theKey[i]][campus];
            jQuery('#calcApp').data('costoPorMateria', costoPorMateria);

            if (catCosto == "ESP") {
                var pagoNormal = costoPorMateria;
                console.log("Entro aqui= " + costoPorMateria);
            } else {
                var pagoNormal = costoPorMateria * noMaterias;
            }
            var descuentoBeca = pagoNormal * (descuento / 100);
            var totalPagar = pagoNormal - descuentoBeca;

            if (catCosto == "DIPON" || catCosto == "DIP") {
                var elAhorro = descuentoBeca * 6;
            } else {

                var elAhorro = descuentoBeca * 4
            }

            pago[theKey[i]] = pagoNormal;
            ahorro[theKey[i]] = elAhorro;

        }

    }
    i = 0;
    console.log("DENTRO DE CALCULA COSTOS PAGO:");
    console.log(pago);
    jQuery.each(pago, function(key, value) {
        if (i == 0) {
            classi = "btn-primary";
            jQuery('#app-esquemapagos>.row>.botones_anios').append('<div class="col-sm-8"></div>');
            jQuery('#calcApp').data('planSelec', key);
            jQuery('#calcApp').data('CostoSelec', value);
        } else {
            classi = "btn-default";
            jQuery('#calcApp').data('planRecom', key);
            jQuery('#calcApp').data('CostoRecom', value);

        }

    });
    console.log("DENTRO DE CALCULA COSTOS: ");
    console.log("__________________________");
    console.log("planSelec: ");
    console.log(jQuery('#calcApp').data('planSelec'));
    console.log("CostoSelec: ");
    console.log(jQuery('#calcApp').data('CostoSelec'));
    console.log("planRecom: ");
    console.log(jQuery('#calcApp').data('planRecom'));
    console.log("CostoRecom: ");
    console.log(jQuery('#calcApp').data('CostoRecom'));
    /*****************IMPRIME PAGOS EN HTML**********************/
    var tresPagos;
    var cuatroPagos;
    var seisPagos;
    var diezPagos;
    var contado;
    var totalCuatri;
    var totalCuatriconBeca;
    if (jQuery('#calcApp').data("idCosto") == "DIPON" || jQuery('#calcApp').data("idCosto") == "DIP" || jQuery('#calcApp').data("idCosto") == "ESP") {
        var tipoDuracion = "semestre";
    } else {    
        
        var tipoDuracion = "cuatrimestre";
    }
    var duracionCarrera = "";
    debuglog(tipoDuracion);
    if (jQuery('#calcApp').data('materias') == jQuery('#calcApp').data('maxmaterias')) {
        duracionCarrera = "para terminar en " + jQuery('#calcApp').data('maxmateriasanios') + " años";
        if (jQuery('#calcApp').data('maxmateriasanios') == "6m") {
            duracionCarrera = "para terminar en 6 meses"; 
        }
    }
    if (jQuery('#calcApp').data('materias') == jQuery('#calcApp').data('minmaterias')) {
        duracionCarrera = "para terminar en " + jQuery('#calcApp').data('minmateriasanios') + " años";
        if (jQuery('#calcApp').data('maxmateriasanios') == "6m") {
            duracionCarrera = "para terminar en 6 meses";
        }
    }

    jQuery.each(pago, function(key, value) {

        if (key == "1pago") {
            var simulacion = '<div class="row">Es como si pagaras: ' + format2(value - (value * (jQuery('#calcApp').data('beca') / 100)), "$") + ' al mes</div>';
            var ahorroContado = '<div class="row"> te estarias ahorrando <span class="ahorroContado"></span> si lo pagas de contado</div>';
            contado = value;
            /*************IMPRIME BECAS***************/
            if (jQuery("#ex14").slider('getValue') < 5 && jQuery('#calcApp').data('idCosto') != 'INGON' && jQuery('#calcApp').data('idCosto') != 'DIP' && jQuery('#calcApp').data('idCosto') != 'DIPON' && jQuery('#calcApp').data('idCosto') != 'POSON' && jQuery('#calcApp').data('idCosto') != 'POS') {

            } else {
                jQuery('#res-' + key).append('<div id="ahorro-' + key + '">Tu ahorro es de ' + format2((jQuery('#calcApp').data('beca') / 100) * value, "$") + ' y en tu ' + tipoDuracion + '</div>');
                jQuery('#res-' + key).append(ahorroContado);
                debuglog('<div id="ahorro-' + key + '">Tu ahorro es de ' + format2((jQuery('#calcApp').data('beca') / 100) * value, "$") + ' y en tu ' + tipoDuracion + '</div>' + ahorroContado);
            }
            contado = value - (value * (jQuery('#calcApp').data('beca') / 100));
            debuglog(contado);
        }

        if (key == "4pagos") {
            cuatroPagos = value;
            var CostoporMaterias = value;
            totalCuatri = CostoporMaterias * 4;
            totalCuatriconBeca = (totalCuatri - (totalCuatri * (jQuery('#calcApp').data('beca') / 100)));
            /*************IMPRIME BECAS***************/
            if (jQuery("#ex14").slider('getValue') < 5 && jQuery('#calcApp').data('idCosto') != 'INGON' && jQuery('#calcApp').data('idCosto') != 'DIP' && jQuery('#calcApp').data('idCosto') != 'DIPON' && jQuery('#calcApp').data('idCosto') != 'POSON' && jQuery('#calcApp').data('idCosto') != 'POS') {
            } else {
            }

        }
        if (key == "6pagos") {
            debuglog('6pagos')
            seisPagos = value;
            var CostoporMaterias = value;

            totalCuatri = CostoporMaterias * 6;
            totalCuatriconBeca = (totalCuatri - (totalCuatri * (jQuery('#calcApp').data('beca') / 100)));

            /*************IMPRIME BECAS***************/
            jQuery('#res-' + key).append('<hr class="division-calcu">');
            jQuery('#res-' + key).append('<div class="col-xs-12 l1"><div class="row">Tu beca es de</div> <div class="numero-calcu-resultado"> ' + jQuery('#calcApp').data('beca') + '%</div></div>');

            if (jQuery("#ex14").slider('getValue') < 5 && jQuery('#calcApp').data('idCosto') != 'INGON' && jQuery('#calcApp').data('idCosto') != 'DIP' && jQuery('#calcApp').data('idCosto') != 'DIPON' && jQuery('#calcApp').data('idCosto') != 'POSON' && jQuery('#calcApp').data('idCosto') != 'POS') {
            } else {
            }
        }
    });
    if (isNaN(contado) != true) {
        debuglog("mi ahorro:" + contado);
        debuglog("ahorroContado:" + contado);
    }

}//TERMINA CALCULA COSTOS


function getPagos(catCosto, noMaterias, descuento, campus) {

    debuglog("\nEjecutando Funcion GetPagos")
        //debuglog("Variables de entrada:");
        //debuglog("catCosto:" + catCosto );
        //debuglog("noMaterias:" + noMaterias);
        //debuglog("Descuento:" + descuento)
        //debuglog("Campus:" + campus);

    console.log("%cEjecutando Funcion GetPagos", "color: red");
    console.log("--------------------Ejecutando Funcion getPagos----------------------------------");
    console.log("Variables de entrada:");
    console.log("catCosto:" + catCosto);
    console.log("noMaterias:" + noMaterias);
    console.log("Descuento:" + descuento)
    console.log("Campus:" + campus);


    jQuery('#app-esquemapagos>.row>.botones_anios').empty();
    jQuery('#app-resumen').html('');
    jQuery('#app-becas').html('');
    jQuery('#app-ahorro').html('');

    //console.log("Funcion:getPagos  Variables de Entrada: CarCosto: "+catCosto+" |noMaterias: "+noMaterias+" |Descruento: "+descuento+" |Campus: "+campus);
    //VARIABLES GENERALES
    if (!catCosto) {
        alert("Selecciona la carrera y modalidad deseada");

        throw ("No existe el catCosto");
    }
    var qryStrCostos = '/*/' + catCosto + '/costoMateria';
    //console.log("La"+qryStrCostos);
    var qryResults = queryHandler(jsonCostos, qryStrCostos);
    var losPagos = qryResults[0].value;
    var theKey = Object.keys(losPagos);
    var pago = {};
    var costos = [];
    var ahorro = {};
    var classi;
    //console.log(qryStrCostos);
    //console.log(qryResults);
    //console.log("los pagos abajo");
    //console.log(losPagos);
    //console.log(Object.keys(losPagos).length);
    for (var i = 0; i < Object.keys(losPagos).length; i++) {
        //console.log("un array");
        //console.log(losPagos[theKey[i]][campus]);
        if (Array.isArray(losPagos[theKey[i]][campus])) {

            //console.log("--a");
            var variosPagos = losPagos[theKey[i]][campus];

            jQuery.each(variosPagos, function(key, value) {

                var costoPorMateria = value;
                jQuery('#calcApp').data('costoPorMateria', value);

                if (catCosto == "ESP") {
                    var pagoNormal = costoPorMateria;
                    //console.log("Entro aqui");
                } else {
                    var pagoNormal = costoPorMateria * noMaterias;
                }



                var descuentoBeca = pagoNormal * (descuento / 100);
                var totalPagar = pagoNormal - descuentoBeca;

                if (catCosto == "DIPON" || catCosto == "DIP") {

                    var elAhorro = descuentoBeca * 6;
                    debuglog('cat Costo:' + catCosto + " el ahorro: " + elAhorro);

                } else {

                    var elAhorro = descuentoBeca * 4
                    debuglog('cat Costo:' + catCosto + " el ahorro: " + elAhorro);
                }

                costos.push(totalPagar);
                pago[theKey[i]] = pagoNormal;
                ahorro[theKey[i]] = elAhorro;

            });


        } else {
            //console.log("--b");
            var costoPorMateria = losPagos[theKey[i]][campus];
            jQuery('#calcApp').data('costoPorMateria', costoPorMateria);
            //console.log(costoPorMateria);

            if (catCosto == "ESP") {
                var pagoNormal = costoPorMateria;
                console.log("Entro aqui= " + costoPorMateria);
            } else {
                var pagoNormal = costoPorMateria * noMaterias;
            }
            var descuentoBeca = pagoNormal * (descuento / 100);
            var totalPagar = pagoNormal - descuentoBeca;

            if (catCosto == "DIPON" || catCosto == "DIP") {
                var elAhorro = descuentoBeca * 6;
                //console.log("el ahorro:"+pagoNormal )
                //debuglog('cat Costo:'+catCosto+" el ahorro: "+elAhorro);
            } else {
                //console.log("B el ahorro:"+pagoNormal )
                var elAhorro = descuentoBeca * 4
                    //debuglog('cat Costo:'+catCosto+" el ahorro: "+elAhorro);
            }

            pago[theKey[i]] = pagoNormal;
            ahorro[theKey[i]] = elAhorro;

        }

    }

    i = 0;
    //console.log("*******************");
    //console.log(pago);


    jQuery.each(pago, function(key, value) {
        //console.log(value+"  "+key);
        if (i == 0) {
            classi = "btn-primary";
            jQuery('#app-esquemapagos>.row>.botones_anios').append('<div class="col-sm-8"></div>');
            jQuery('#calcApp').data('planSelec', key);
            jQuery('#calcApp').data('CostoSelec', value);
        } else {
            classi = "btn-default";
            jQuery('#calcApp').data('planRecom', key);
            jQuery('#calcApp').data('CostoRecom', value);

        }
        /*if(key=="1pago"){keyn="Pago Contado";}
        if(key=="4pagos"){keyn="Pago Mensual";}
        if(key=="6pagos"){keyn="Pago Mensual";}
        if(key=="1pago"||key=="4pagos"||key=="6pagos"){
        jQuery('#app-esquemapagos>.row').append('<div class="col-xs-6 col-sm-2"><button data-esquema="' + key + '" type="submit" class="btn-esquema btn  btn-sm ' + classi + '">' + keyn + '</button></div>');
         debuglog('<div class="col-xs-6 col-sm-2"><button data-esquema="' + key + '" type="submit" class="btn-esquema btn  btn-sm ' + classi + '">' + keyn + '</button></div>');
        i++;
        }*/

    });

    /*****************IMPRIME PAGOS EN HTML**********************/
    var tresPagos;
    var cuatroPagos;
    var seisPagos;
    var diezPagos;
    var contado;
    var totalCuatri;
    var totalCuatriconBeca;
    //console.warn(pago);
    if (jQuery('#calcApp').data("idCosto") == "DIPON" || jQuery('#calcApp').data("idCosto") == "DIP" || jQuery('#calcApp').data("idCosto") == "ESP") {
        var tipoDuracion = "semestre";
    } else {    
        
        var tipoDuracion = "cuatrimestre";
    }
    var duracionCarrera = "";
    debuglog(tipoDuracion);
    if (jQuery('#calcApp').data('materias') == jQuery('#calcApp').data('maxmaterias')) {
        duracionCarrera = "para terminar en " + jQuery('#calcApp').data('maxmateriasanios') + " años";
        if (jQuery('#calcApp').data('maxmateriasanios') == "6m") {
            duracionCarrera = "para terminar en 6 meses"; 
        }
    }
    if (jQuery('#calcApp').data('materias') == jQuery('#calcApp').data('minmaterias')) {
        duracionCarrera = "para terminar en " + jQuery('#calcApp').data('minmateriasanios') + " años";
        if (jQuery('#calcApp').data('maxmateriasanios') == "6m") {
            duracionCarrera = "para terminar en 6 meses";
        }
    }

    jQuery.each(pago, function(key, value) {
        //console.log(key+"  "+value);

        if (key == "1pago") {
            //console.log("Tipo duracion:"+tipoDuracion);
            //debuglog('1pago');
            var simulacion = '<div class="row">Es como si pagaras: ' + format2(value - (value * (jQuery('#calcApp').data('beca') / 100)), "$") + ' al mes</div>';
            var ahorroContado = '<div class="row"> te estarias ahorrando <span class="ahorroContado"></span> si lo pagas de contado</div>';
            contado = value;




            jQuery('#app-resumen').append('<div class="col-xs-12 oculto" id="res-' + key + '">\
                                           <div class="row l1">Tu ' + tipoDuracion + ' costaría: </div>\
                                           <div class="row numero-calcu-resultado montserrat"> ' + format2(value - (value * (jQuery('#calcApp').data('beca') / 100)), "$") + '</div>\
                                           <div class="row l1"><!--Pagando mensualmente: ' + format2((value - (value * (jQuery('#calcApp').data('beca') / 100))) / 4, "$") + '--></div>\
                                           <div class="row">con ' + jQuery('#calcApp').data('materias') + ' materias </div>\
                                           </div>');
            debuglog('<div class="col-xs-12 oculto" id="res-' + key + '">\
                                           <div class="row l1">Tu ' + tipoDuracion + ' costaría: </div>\
                                           <div class="row numero-calcu-resultado montserrat"> ' + format2(value - (value * (jQuery('#calcApp').data('beca') / 100)), "$") + '</div>\
                                           <div class="row l1"><!--Pagando mensualmente: ' + format2((value - (value * (jQuery('#calcApp').data('beca') / 100))) / 4, "$") + '--></div>\
                                           <div class="row">con ' + jQuery('#calcApp').data('materias') + ' materias </div>\
                                           </div>');

            /*************IMPRIME BECAS***************/
            jQuery('#res-' + key).append('<hr class="division-calcu">');
            jQuery('#res-' + key).append('<div class="col-xs-12 l1"><div class="row">Tu beca es de</div> <div class="numero-calcu-resultado"> ' + jQuery('#calcApp').data('beca') + '%</div></div>');

            debuglog('<div class="col-xs-12 l1"><div class="row">Tu beca es de</div> <div class="numero-calcu-resultado"> ' + jQuery('#calcApp').data('beca') + '%</div></div>');

            //  if (  (parseInt(jQuery('#calcApp').data("materias")) < parseInt(jQuery('#calcApp').data("minmaterias"))) || (parseInt(jQuery('#calcApp').data("materias")) != parseInt(jQuery('#calcApp').data("maxmaterias")))   ) {
            if (jQuery("#ex14").slider('getValue') < 5 && jQuery('#calcApp').data('idCosto') != 'INGON' && jQuery('#calcApp').data('idCosto') != 'DIP' && jQuery('#calcApp').data('idCosto') != 'DIPON' && jQuery('#calcApp').data('idCosto') != 'POSON' && jQuery('#calcApp').data('idCosto') != 'POS') {

                debuglog('<div>Debes tener 5 o mas materias para obtener beca </div>');
                jQuery('#res-' + key).append('<div>Debes tener 5 o mas materias para obtener beca </div>');
            } else {
                jQuery('#res-' + key).append('<div id="ahorro-' + key + '">Tu ahorro es de ' + format2((jQuery('#calcApp').data('beca') / 100) * value, "$") + ' y en tu ' + tipoDuracion + '</div>');
                jQuery('#res-' + key).append(ahorroContado);
                debuglog('<div id="ahorro-' + key + '">Tu ahorro es de ' + format2((jQuery('#calcApp').data('beca') / 100) * value, "$") + ' y en tu ' + tipoDuracion + '</div>' + ahorroContado);
            }
            contado = value - (value * (jQuery('#calcApp').data('beca') / 100));
            debuglog(contado);


        }
        /*if (key == "3pagos") {
            tresPagos=value;
            var html = '<div  class="oculto" id="res-' + key + '">Tu cuatrimestre costaria';
            for (i = 0; i < value.length; i++) {
                if (i == 0) {
                    html += " un pago de: ";
                }
                if (i == 1) {
                    html += " y dos pagos iguales de ";
                }
                html += " " + format2(value[i], "$") + " ";
            };
            html += '</div>';
            jQuery('#app-resumen').append(html);
        }*/
        if (key == "4pagos") {
            cuatroPagos = value;
            var CostoporMaterias = value;
            totalCuatri = CostoporMaterias * 4;
            totalCuatriconBeca = (totalCuatri - (totalCuatri * (jQuery('#calcApp').data('beca') / 100)));
            jQuery('#app-resumen').append('<div id="res-' + key + '">\
                                        <div class="row l1">Tu pago mensual sería de: </div>\
                                        <div class="row numero-calcu-resultado montserrat"> ' + format2((totalCuatri - (totalCuatri * (jQuery('#calcApp').data('beca') / 100))) / 4, "$") + '</div>\
                                        <div class="row">Con ' + jQuery('#calcApp').data('materias') + ' materias</div>\
                                        <div class="row">y tu ' + tipoDuracion + ' completo costaría: ' + format2((totalCuatri - (totalCuatri * (jQuery('#calcApp').data('beca') / 100))), "$") + '</div>\
                                      </div>');
            debuglog('4 pagos');
            debuglog('<div id="res-' + key + '">\
                                        <div class="row l1">Tu pago mensual sería de: </div>\
                                        <div class="row numero-calcu-resultado montserrat"> ' + format2((totalCuatri - (totalCuatri * (jQuery('#calcApp').data('beca') / 100))) / 4, "$") + '</div>\
                                        <div class="row">Con ' + jQuery('#calcApp').data('materias') + ' materias</div>\
                                        <div class="row">y tu ' + tipoDuracion + ' completo costaría: ' + format2((totalCuatri - (totalCuatri * (jQuery('#calcApp').data('beca') / 100))), "$") + '</div>\
                                      </div>');
            /*************IMPRIME BECAS***************/
            jQuery('#res-' + key).append('<hr class="division-calcu">');
            jQuery('#res-' + key).append('<div class="col-xs-12 l1"><div class="row">Tu beca es de</div> <div class="numero-calcu-resultado"> ' + jQuery('#calcApp').data('beca') + '%</div></div>');
            //            if (  (parseInt(jQuery('#calcApp').data("materias")) < parseInt(jQuery('#calcApp').data("minmaterias"))) || (parseInt(jQuery('#calcApp').data("materias")) != parseInt(jQuery('#calcApp').data("maxmaterias")))   ) {
            if (jQuery("#ex14").slider('getValue') < 5 && jQuery('#calcApp').data('idCosto') != 'INGON' && jQuery('#calcApp').data('idCosto') != 'DIP' && jQuery('#calcApp').data('idCosto') != 'DIPON' && jQuery('#calcApp').data('idCosto') != 'POSON' && jQuery('#calcApp').data('idCosto') != 'POS') {
                //
                jQuery('#res-' + key).append('<div>Debes tener 5 o mas materias para obtener beca </div>');
                debuglog('<div>Debes tener 5 o mas materias para obtener beca </div>');
            } else {

                jQuery('#res-' + key).append('<div id="ahorro-' + key + '">Tu ahorro es de ' + format2(totalCuatri - (totalCuatri - (totalCuatri * (jQuery('#calcApp').data('beca') / 100))), "$") + ' en tu ' + tipoDuracion + '</div>');
                debuglog('<div id="ahorro-' + key + '">Tu ahorro es de ' + format2(totalCuatri - (totalCuatri - (totalCuatri * (jQuery('#calcApp').data('beca') / 100))), "$") + ' en tu ' + tipoDuracion + '</div>');
            }
            //jQuery('.simulacion').html(format2(contado/4, "$"));


        }
        if (key == "6pagos") {
            debuglog('6pagos')
            seisPagos = value;
            var CostoporMaterias = value;

            totalCuatri = CostoporMaterias * 6;
            totalCuatriconBeca = (totalCuatri - (totalCuatri * (jQuery('#calcApp').data('beca') / 100)));
            /* jQuery('#app-resumen').append('<div class="col-xs-12 " id="res-' + key + '">\
                                            <div class="row l1">Tu '+ tipoDuracion +' costaria: </div>\
                                            <div class="row numero-calcu-resultado montserrat"> ' + format2( value - (value* (jQuery('#calcApp').data('beca')/100))  , "$")  + '</div>\
                                            <div class="row l1">Pagando mensualmente: '+format2((value - (value* (jQuery('#calcApp').data('beca')/100))) /4, "$")+'</div>\
                                            <div class="row">con ' + jQuery('#calcApp').data('materias') + ' materias '+ duracionCarrera +'</div>\
                                            </div>');*/

            jQuery('#app-resumen').append('<div id="res-' + key + '">\
                                        <div class="row l1">Tu pago mensual sería de: </div>\
                                        <div class="row numero-calcu-resultado montserrat"> ' + format2((totalCuatri - (totalCuatri * (jQuery('#calcApp').data('beca') / 100))) / 6, "$") + '</div>\
                                        <div class="row">Con ' + jQuery('#calcApp').data('materias') + ' materias</div>\
                                        <div class="row">y tu ' + tipoDuracion + ' completo costaría: ' + format2((totalCuatri - (totalCuatri * (jQuery('#calcApp').data('beca') / 100))), "$") + '</div>\
                                      </div>');
            debuglog('<div id="res-' + key + '">\
                                        <div class="row l1">Tu pago mensual sería de: </div>\
                                        <div class="row numero-calcu-resultado montserrat"> ' + format2((totalCuatri - (totalCuatri * (jQuery('#calcApp').data('beca') / 100))) / 6, "$") + '</div>\
                                        <div class="row">Con ' + jQuery('#calcApp').data('materias') + ' materias</div>\
                                        <div class="row">y tu ' + tipoDuracion + ' completo costaría: ' + format2((totalCuatri - (totalCuatri * (jQuery('#calcApp').data('beca') / 100))), "$") + '</div>\
                                      </div>');

            /*************IMPRIME BECAS***************/
            jQuery('#res-' + key).append('<hr class="division-calcu">');
            jQuery('#res-' + key).append('<div class="col-xs-12 l1"><div class="row">Tu beca es de</div> <div class="numero-calcu-resultado"> ' + jQuery('#calcApp').data('beca') + '%</div></div>');

            //if (  (parseInt(jQuery('#calcApp').data("materias")) < parseInt(jQuery('#calcApp').data("minmaterias"))) || (parseInt(jQuery('#calcApp').data("materias")) != parseInt(jQuery('#calcApp').data("maxmaterias")))   ) {
            if (jQuery("#ex14").slider('getValue') < 5 && jQuery('#calcApp').data('idCosto') != 'INGON' && jQuery('#calcApp').data('idCosto') != 'DIP' && jQuery('#calcApp').data('idCosto') != 'DIPON' && jQuery('#calcApp').data('idCosto') != 'POSON' && jQuery('#calcApp').data('idCosto') != 'POS') {

                jQuery('#res-' + key).append('<div>Debes tener 5 o más materias para obtener beca </div>');
                debuglog('<div>Debes tener 5 o más materias para obtener beca </div>');
            } else {

                jQuery('#res-' + key).append('<div id="ahorro-' + key + '">Tu ahorro es de ' + format2(totalCuatri - (totalCuatri - (totalCuatri * (jQuery('#calcApp').data('beca') / 100))), "$") + ' en tu ' + tipoDuracion + '</div>');
                debuglog('<div id="ahorro-' + key + '">Tu ahorro es de ' + format2(totalCuatri - (totalCuatri - (totalCuatri * (jQuery('#calcApp').data('beca') / 100))), "$") + ' en tu ' + tipoDuracion + '</div>');
            }
        }
        /*if (key == "10pagos") {
             diezPagos=value;//
            jQuery('#app-resumen').append('<div  id="res-' + key + '">Tu pago mensual es de ' + format2(value, "$") + '</div>');
        }*/
    });
    if (isNaN(contado) != true) {
        debuglog("mi ahorro:" + contado);
        debuglog("ahorroContado:" + contado);
        jQuery('.ahorroContado').html(format2(totalCuatriconBeca - contado, "$"));

    }

    //jQuery('.ahorroContado').html( (value-contado/4)*4 + ")");

    /*console.log(ahorro);
    console.log(pago);*/

}//TERMINA GET pagos




function getAnioslimite() {
    var lineaNegocio = jQuery('#calcApp').data('idCosto');
    var anios;
    var materias;
    var noMaterias = jQuery('#calcApp').data('materias');
    //console.log("----------------------------------------------------------------------");
    //console.log("Ejecutando Funcion getAnioslimite");
    //console.log("Ejecuta Query en costos.json");
    //console.log("Variables: Linea de negocio: "+lineaNegocio+"  | Numero de materias: "+noMaterias);
    //console.log("noMaterias: "+noMaterias)

    //debuglog("----------------------------------------------------------------------");
    //debuglog("Ejecutando Funcion getAnioslimite");
    //debuglog("Ejecuta Query en costos.json");
    //debuglog("Variables: Linea de negocio: "+lineaNegocio+"  | Numero de materias: "+noMaterias);
    //debuglog("noMaterias: "+noMaterias)

    //limpieza de variables
    jQuery('#calcApp').removeData('maxmaterias');
    jQuery('#calcApp').removeData('maxmateriasanios');
    jQuery('#calcApp').removeData('minmaterias');
    jQuery('#calcApp').removeData('minmateriasanios');


    var qryCostos = queryHandler(jsonCostos, '/*/' + lineaNegocio + '/duracion');
    var contador = 0;
    if (qryCostos.length != 1) {
        //console.log("[FATAL ERROR] No se encontro coincidencia en el query: "+'/*/'+lineaNegocio+'/duracion');
        errorCalculadora();
    }
    console.log('YAAAAAAAA');
    console.log(qryCostos);
    console.log(qryCostos[0].value);
   //console.log(qryCostos[0].value["3"].materias=9999);
    


   //var esbeltas = ['3954','3932','3953','3959','3956','3972','3976','3974','3961','3973','3914','3971']; //ok
   //var a = esbeltas.indexOf(jQuery('#calcApp').data('selectMateria').toString());



    /*if(jQuery('#calcApp').data('selectMateria') == 3954) {
        var a = 0;
    }else if(jQuery('#calcApp').data('selectMateria') == 3932) {
        var a = 0;
    }else if(jQuery('#calcApp').data('selectMateria') == 3953) {
        var a = 0;
    }else if(jQuery('#calcApp').data('selectMateria') == 3959) {
        var a = 0;
    }else if(jQuery('#calcApp').data('selectMateria') == 3956) {
        var a = 0;
    }else if(jQuery('#calcApp').data('selectMateria') == 3972) {
        var a = 0;
    }else if(jQuery('#calcApp').data('selectMateria') == 3976) {
        var a = 0;
    }else if(jQuery('#calcApp').data('selectMateria') == 3974) {
        var a = 0;
    }else if(jQuery('#calcApp').data('selectMateria') == 3961) {
        var a = 0;
    }else if(jQuery('#calcApp').data('selectMateria') == 3973) {
        var a = 0;
    }else if(jQuery('#calcApp').data('selectMateria') == 3914) {
        var a = 0;
    }else if(jQuery('#calcApp').data('selectMateria') == 3971) {
        
    }else {
        var a = -1;
    }*/


    /*var esbforaneos=["GDL","LEO","TOL","QRO"];
    console.log(jQuery('#calcApp').data('campus'));
    var b = esbforaneos.indexOf(jQuery('#calcApp').data('campus'));
    console.log('LOS ESBELTOS');
    console.log(a);
    console.log(b);
    console.log(qryCostos[0].value["3"]);

    if (a >= 0 && b >= 0 && qryCostos[0].value["3"]) {
        qryCostos[0].value["3"].materias = 5;
     console.log('ENTREE_ESBELTAS--dsda');
     }
    if (a >= 0 && b >= 0 && qryCostos[0].value["4"]) {
        qryCostos[0].value["4"].materias = 4;
        console.log('ENTREE_ESBELTAS--ds32323da');
    }*/


    //qryCostos[0][3].materias=9999;
   
    jQuery.each(qryCostos[0].value, function(anios, value) {
        //console.log(anios);
        if (contador == 0) {
            jQuery('.resAnios1').html(anios)
        };
        if (contador == 1) {
            jQuery('.resAnios2').html(anios)
        };
        jQuery.each(value, function(key, value) {

            console.log("key: " + key + " valor:" + value);
            //ASIGNACION DE NUMERO DE MATERIAS


            /*//www.unitec.mx/especialidad-en-endodoncia/  11   36
            especialidad-en-ortodoncia/ 11  53
            especialidad-en-odontologia-restauradora/ 10 52
            especialidad-en-odontologia-pediatrica/ 10   51
            especialidad-en-periodoncia/ 10  55*/

            if (jQuery('#calcApp').data('selectMateria') == 195 || jQuery('#calcApp').data('selectMateria') == 192) {
                value = 11;
            }
            if (jQuery('#calcApp').data('selectMateria') == 193 || jQuery('#calcApp').data('selectMateria') == 197 || jQuery('#calcApp').data('selectMateria') == 196) {
                value = 10;
            }

            if (contador == 0) {
                jQuery('#calcApp').data("maxmaterias", value);
                jQuery('#calcApp').data("maxmateriasanios", anios);
                jQuery('.materias1').html(value);
                jQuery('#calcApp').data('materias', value);
                getPagos(lineaNegocio, value, jQuery('#calcApp').data('beca'), jQuery('#calcApp').data('campus'));
                //console.log('esto paso'+lineaNegocio+ "|" +value+ "|" + jQuery('#calcApp').data('beca')+ "|" + jQuery('#calcApp').data('campus')     )
            }
            if (contador == 1) {
                jQuery('#calcApp').data("minmaterias", value);
                jQuery('#calcApp').data("minmateriasanios", anios);
                jQuery('.materias2').html(value);
            }
        });

        //comentado porque deberia ser dinamico
        /*if(anios=="6m"){ //Es una carrera semestral que solo mostrara 4 materias
                alert("semestral");
                jQuery('#ex14').slider('destroy');
                jQuery("#ex14").slider({
                ticks: [1,2, 3, 4],
                ticks_labels: ['1','2', '3', '4'],
                step:1,
                });
                jQuery('#materias-val').html("4");

          }else if(lineaNegocio=='ESP'){

             jQuery('#ex14').slider('destroy');
                jQuery("#ex14").slider({
                ticks: [1,2, 3, 4,5,6,7,8,9,10],
                ticks_labels: ['1','2', '3', '4','5','6','7','8','9','10'],
                step:1,
                });
                jQuery('#materias-val').html("5");

          }else{

                jQuery('#ex14').slider('destroy');
                jQuery("#ex14").slider({
                ticks: [1,2, 3, 4,5,6,7],
                ticks_labels: ['1','2', '3', '4','5','6','7'],
                step:1,
                });
                jQuery('#materias-val').html("5");

          }*/




        contador++;
    });

    //slider constructor
    var materias_label = [];
    var materia = [];
    for (i = 1; i < parseInt(jQuery('#calcApp').data('maxmaterias')) + 1; i++) {
        materias_label.push(i.toString());
        materia.push(i);
    }
    jQuery('#ex14').slider('destroy');
    jQuery("#ex14").slider({ 
        ticks: materia,
        ticks_labels: materias_label,
        step: 1,
    });
    if (jQuery('#calcApp').data('minmaterias')) {
        jQuery('#ex14').slider('setValue', parseInt(jQuery('#calcApp').data('minmaterias')));
        jQuery('.materias-val').html(parseInt(jQuery('#calcApp').data('minmaterias')));
        jQuery('#materias-val').html(jQuery('#calcApp').data('minmaterias'));
        //jQuery('.control-mas-mat').fadeOut();
    } else {
        jQuery('#ex14').slider('setValue', parseInt(jQuery('#calcApp').data('maxmaterias')));
        jQuery('#materias-val').html(jQuery('#calcApp').data('maxmaterias'));
    }
 
    if (jQuery('#calcApp').data('selectMateria') == 3875 ||
        jQuery('#calcApp').data('selectMateria') == 1149 ||
        jQuery('#calcApp').data('selectMateria') == 1146 ||
        jQuery('#calcApp').data('selectMateria') == 3874 ||
        jQuery('#calcApp').data('selectMateria') == 1122 ||
        jQuery('#calcApp').data('selectMateria') == 1176 ||
        jQuery('#calcApp').data('selectMateria') == 3876) {
        console.log('Becas especiales');
        jQuery('#ex14').slider('setValue', parseInt('4'));
        jQuery('.materias-val').html(parseInt('4'));
        jQuery('#materias-val').html('4');
    }

}


function orderElements(coleccion) {
    var mylist = coleccion;
    var listitems = mylist.children('div').get();
    listitems.sort(function(a, b) {
        return jQuery(a).text().toUpperCase().localeCompare(jQuery(b).text().toUpperCase());
    });
    jQuery.each(listitems, function(index, item) {
        mylist.append(item);
    });
}

function scrollToTop() {
    //jQuery("html, body").animate({ scrollTop: 0 }, "fast");
    window.scrollTo(0, 0);
}

function errorCalculadora() {
    //console.log("Ejecutando funcion de error");
    alert("Revisa consola de errores");
}
//JEAB FUNCION ANIMAR
function animar(div) {
    //JEAB EVITAMOS QUE EL LOGO SE OCULTE 29-04-2016
    //jQuery('.unitec-logo').fadeOut();
    //jQuery('.backMenu').fadeIn();

    jQuery('.backMenu').addClass('btn btn-primary');
    jQuery('.backMenu').addClass('cerrarExpandible');
    var pantallaH = jQuery(window).height();
    var pantallaW = jQuery(window).width();
    jQuery(div).data("ex-top", jQuery(div).position().top + "px");
    jQuery(div).data("ex-right", jQuery(div).position().right + "px");
    jQuery(div).data("ex-bottom", jQuery(div).position().bottom + "px");
    jQuery(div).data("ex-left", jQuery(div).position().left + "px");
    jQuery(div).data("ex-width", jQuery(div).width());
    jQuery(div).data("ex-height", parseFloat(jQuery(div).outerHeight()).toFixed(2));
    jQuery('.cerrarExpandible').data("affected", jQuery(div).attr("id"));

    if (jQuery(div).data("animacion") == 1) {
        jQuery(div).animate({
            "height": pantallaH + "px",
            "width": pantallaW
        }, 500).css({
            "z-index": "99",
            "position": "absolute",
            "width": jQuery(div).width() + "px"
        });
    }

    if (jQuery(div).data("animacion") == 2) {
        jQuery(div).css({
            "z-index": "99",
            "top": jQuery(div).data("ex-top"),
            "position": "absolute",
            "width": jQuery(div).data("ex-width")
        }).animate({
            "top": 0,
            "height": pantallaH,
            "width": pantallaW
        });
    }
    if (jQuery(div).data("animacion") == 3) {
        jQuery(div).css({
            "z-index": "99",
            "top": jQuery(div).data("ex-top"),
            "position": "absolute",
            "width": jQuery(div).data("ex-width")
        }).animate({
            "top": 0,
            "left": "-" + jQuery(div).offset().left,
            "height": pantallaH,
            "width": pantallaW
        });
    }
    if (jQuery(div).data("animacion") == 4) {
        jQuery(div).css({
            "z-index": "99",
            "top": jQuery(div).data("ex-top"),
            "position": "absolute",
            "width": jQuery(div).data("ex-width")
        }).animate({
            "top": 0,
            "left": "-" + jQuery(div).offset().left,
            "height": pantallaH,
            "width": pantallaW
        });
    }
    if (jQuery(div).data("animacion") == 5) {
        jQuery(div).css({
            "z-index": "99",
            "top": jQuery(div).data("ex-top"),
            "position": "absolute",
            "width": jQuery(div).data("ex-width")
        }).animate({
            "top": 0,
            "left": "-" + jQuery(div).offset().left,
            "height": pantallaH,
            "width": pantallaW
        });
    }

    jQuery('.linea-elementos').css({
        "width": "100%",
        "height": "100%"
    });

    //console.log(jQuery(div).html());


}


function reiniciarAppData() {
    jQuery('#calcApp').removeData('decampus');
    jQuery('#calcApp').removeData('idCosto');
    jQuery('#calcApp').removeData('selectCategoria');
    jQuery('#calcApp').removeData('selectLinea');
    jQuery('#calcApp').removeData('selectMateria');
}

function mostrarAlerta(msj, tipo) {
    if (tipo == 1) {

        jQuery('.msj-modal-title').html("Faltan campos");
        jQuery('.msj-modal-body').html(msj);
        jQuery('.msj-modal-btn').html('<button type="button" class="btn btn-default" data-dismiss="modal" onclick="irA(\'sec-campus\',0)">Close</button>\
          <button type="button" class="btn btn-primary " onclick="irA(\'sec-campus\',0)">Aceptar</button>');
        jQuery("#cerrarMsjModal").html('<span aria-hidden="true" onclick="irA(\'sec-campus\',0)" >&times;</span>');
    }
    if (tipo == 2) {

        jQuery('.msj-modal-title').html("Faltan campos");
        jQuery('.msj-modal-body').html(msj);
        jQuery('.msj-modal-btn').html('<button type="button" class="btn btn-default" onclick="irA(\'sec-cursar\',0)" data-dismiss="modal">Close</button>\
          <button type="button" class="btn btn-primary " onclick="irA(\'sec-cursar\',0)">Aceptar</button>');

    }

    jQuery('#msj-modal').modal('show');
}


function validaCampus(campus) {
    switch (campus) {
        case "ATZ":
            return "ATIZAPAN";
            break;
        case "MAR":
            return "MARINA";
            break;
        case "ECA":
            return "ECATEPEC";
            break;
        case "SUR":
            return "SUR";
            break;
        case "CUI":
            return "CUITLAHUAC";
            break;
        case "LEO":
            return "LEON";
            break;
        case "TOL":
            return "TOLUCA";
            break;
        case "GDL":
            return "GUADALAJARA";
            break;
        case "QRO":
            return "QUERETARO";
            break;
        case "ONL":
            return "ONLINE";
    }
}


function estadoRandom(telefono) {
    var ladas_edomex = ['42', '58', '59', '71', '72', '74', '76'];
    var top_5_estados = ['GUANAJUATO', 'JALISCO', 'VERACRUZ', 'PUEBLA', 'HIDALGO'];

    // TRAIGO EL VALOR DEL ESTADO
    var prefijo_telefono = telefono.substring(0, 2);
    if (prefijo_telefono === '55') {
        return 'CIUDAD DE MEXICO';
    } else if (jQuery.inArray(prefijo_telefono, ladas_edomex) !== -1) {
        return 'ESTADO DE MEXICO';
    } else {
        return top_5_estados[Math.floor(Math.random() * top_5_estados.length)];
        // estado='ZACATECAS';
    }
}

function esNumero(e) {
    tecla = (document.all) ? e.keyCode : e.which;
    if (tecla > 31 && (tecla < 48 || tecla > 57)) {
        //console.log("true");
        return false;
    }
}

function getParametersDynamics(idDynamics) {
    var arrayDynamics = [];
    var carrera = "";
    var nivelInteres = "";
    var subnivelinteres = "";

    if (idDynamics) {

        // var qry=queryHandler(jsonCarreras,'/*/[/IdDynamics == "'+idDynamics+'"]');
        //var interes = qry[0].value.campus;
        var qry = queryHandler(jsonDynamics, '/*/[/IdDynamics == "' + idDynamics + '"]');
        jQuery.each(qry, function(key, value) {

            carrera = value.value.carrera;
            nivelInteres = value.value.nivelInteres;
            subnivelinteres = value.value.subnivelinteres;

            //var modalidad=query.value.modalidad;
        });

        return arrayDynamics = [{
            'carrera': carrera,
            'nivelInteres': nivelInteres,
            'subnivelInteres': subnivelinteres
        }];
    } else {

        return "No hay Id Dynamics";
    }

}

function setCookie(cname, cvalue, exdays) {
    var d = new Date();
    d.setTime(d.getTime() + (exdays * 24 * 60 * 60 * 1000));
    var expires = "expires=" + d.toUTCString();
    document.cookie = cname + "=" + cvalue + "; " + expires;
}

function setCookieSession(cname, cvalue) {
    document.cookie = cname + "=" + cvalue + ";path=/";
}

function getCookie(cname) {
    var name = cname + "=";
    var ca = document.cookie.split(';');
    for (var i = 0; i < ca.length; i++) {
        var c = ca[i];
        while (c.charAt(0) == ' ') c = c.substring(1);
        if (c.indexOf(name) == 0) return c.substring(name.length, c.length);
    }
    return "";
}

function checkCookie(valor) {
    var user = getCookie(valor);
    if (user != "") {
        return true;
    } else {
        return false;
    }

}

function quitaAcentos(str) {
    for (var i = 0; i < str.length; i++) {
        //Sustituye "á é í ó ú"
        if (str.charAt(i) == "á") str = str.replace(/á/, "a");
        if (str.charAt(i) == "é") str = str.replace(/é/, "e");
        if (str.charAt(i) == "í") str = str.replace(/í/, "i");
        if (str.charAt(i) == "ó") str = str.replace(/ó/, "o");
        if (str.charAt(i) == "ú") str = str.replace(/ú/, "u");
    }
    return str;
}


function isStudent(sValue, frm_domain) {
    /*   jQuery('.boton_frm').removeClass('frm_choosed_blue');
       if(sValue==1){jQuery(".alumno_si").addClass('frm_choosed_blue');}
       if(sValue==0){jQuery(".alumno_no").addClass('frm_choosed_blue');}*/
    switch (frm_domain) {

        case "impulsa":
            jQuery('#S_alumno').val(sValue);
            break;

        default:
            if (sValue == 0) {
                jQuery('#S_alumno').val(sValue);
            } else {
                window.open('//www.unitec.mx/colegiaturas', '_blank');
                jQuery('#S_alumno').val("");
            }
            break;
    }
}


function codigoAnterior(codigo){
    switch(codigo) {
        case 3954:
            return 3754
            break;
        case 3929:
            return 779
            break;
        case 3932:
            return 3932
            break;
        case 3953:
            return 3753
            break;
        case 3959:
            return 3759
            break;
        case 3956:
            return 3756
            break;
        case 3973:
            return 3763
            break;
        case 3972:
            return 3762
            break;
        case 3914:
            return 3914
            break;
        case 3976:
            return 3876
            break;
        case 3971:
            return 3751
            break;
        case 3974:
            return 3874
            break;
        case 3961:
            return 3761
            break;
        
       
    }
}


function envioAspirantes() {

    var idDynamics = jQuery('#calcApp').data('selectMateria');
    if(checkCookie('data_frm')==true) {
        if(idDynamics=='' || idDynamics==null || typeof idDynamics == "undefined"){
            var data_cookie = getCookie('data_frm');
            var obj_cookie = JSON.parse(data_cookie);
            idDynamics = obj_cookie.carreraInteres;
        }
    }
    var qry = queryHandler(jsonDynamics, '/* /[/IdDynamics == "' + idDynamics + '"]');
    var Carrera;
    var L_Negocio;
    var Pobla;
    var carreraInteres;
    //console.log(qry);


    var dynamics_val = getParametersDynamics(idDynamics);
    jQuery.each(dynamics_val, function(key, value) {
        Carrera = value['carrera'];
        L_Negocio = value['nivelInteres'];
        carreraInteres = value['subnivelInteres'];
        Pobla = carreraInteres;
    });




    var aspirantes = 'ASPIRANTES LIC';
    var nivelOferta = 'L';

    if (idDynamics == 666) {
        aspirantes = "ASPIRANTES PREPA";
        nivelOferta = "P"
    }
    if (Pobla == 4 || Pobla == 6) {
        aspirantes = "ONLINEP";
    }
    var Banner = jQuery("#frm_banner").val();

    var Herramienta = aspirantes;
    var Campana = 1;
    var Origen = aspirantes;
    var Calidad = 3;
    var F_Nacim = '01/01/2000';


    var Ciclo = '';
    var Ciclo = jQuery("#frm_periodo_interes_sel").val();
   
   
    var Ciclo="18-2";
    var Alumno = 0;
    var Estad = estadoRandom(jQuery('#frm_celular').val());
    var C_Carrera = idDynamics;
    var banner_id = jQuery('#frm_banner').val();
    var CP = jQuery("#revalidacion").val();

    if ( jQuery('#calcApp').data('campus') == 'QRO' || jQuery('#calcApp').data('selectLinea') == 'POSGRADO') {
        CP = 10040;
    }
    var name_form = ''; 
    var Sexo = 'M';
    var nom_carrera = " ";
    var nom_campus = quitaAcentos(campusDynamics(jQuery('#calcApp').data('campus')));
    var promedioF = jQuery('#calcApp').data('promedio');
    var Pdescuento = jQuery('#calcApp').data('beca');
    var nivelF = 'L';
    var CostoUnico = 0;
    var num_carrera = 0;
    var costoMensual = 0;
    var descuentoF = 0;
    var planSelec = jQuery('#calcApp').data('planSelec');
    var CostoSelec = jQuery('#calcApp').data('CostoSelec');
    var planRecom = jQuery('#calcApp').data('planRecom');
    var CostoRecom = jQuery('#calcApp').data('CostoRecom');
    var envioB = 1;
    var C_Carrera_WP = "";
    var Direcc = 'WEBCALL';
    var Colon = '';
    var Nombre2 = '';
    var email2 = '';
    var URLreferer = encodeURIComponent(window.location.href);
    if(getCookie('loginface')==1){
        URLreferer=URLreferer+"&loginface=1";
    }
    var debugs = '1';
    var gclid_field = '';
    var userId_field = '';
    var CID = jQuery('#CID').val();
    var costoMateria = jQuery('#calcApp').data('costoPorMateria');
    var email_unitec = '';
    var otraConsulta = 0;
    var SERVER_URL = '';
    var frm_LadaLenght = '';
    var frm_LadaAllow = '';
    var alum_2_2 = '';
    var Nombre = quitaAcentos(jQuery('#frm_nombre').val());
    var Ap_Pat = quitaAcentos(jQuery('#frm_apaterno').val());
    var Ap_Mat = quitaAcentos(jQuery('#frm_amaterno').val());
    var email = formatEmail(jQuery('#frm_correo').val().trim().replace(/ /g, ""));
    var informacion_2 = 1;
    var Estado = estadoRandom(jQuery('#frm_celular').val());
    var masculino = 'M';
    var TipoTel = 'cel_calc';
    var Telef = jQuery('#frm_celular').val();
    var periodo_interes_sel = '17-1';
    //Adecuacion para campus Queretaro
    if(jQuery('#calcApp').data('campus') == 'QRO') {
        CP = 10040;
        periodo_interes_sel = '18-1';
    }
    var politica = 'on';
    var promedio = jQuery('#calcApp').data('promedio');
    var programa = idDynamics;
    var materias = jQuery('#calcApp').data('materias');
    var cookieHubspot = getCookie('hubspotutk') || "";
    //Validacion de cookie por si no trae valores enviarlos
    var anos_termino = jQuery('#calcApp').data('termino');
    console.log("ANOS DE TERMINO SELECCIONADOS______________________:");
    console.log(anos_termino);
    //comentar para quitar pregunta 
    if (jQuery('#padretutor').val() != 'si' || jQuery('#padretutor').val() != 'no') {
                if (jQuery('#padretutor').val() == 'si') {
                        var padretutor = 'TUTOR';
                    } else {
                        var padretutor = 'No';
                    }
        
                } else {
        
                    var padretutor = 'No';
            }


    if(checkCookie('data_frm')==true) {
        var data_cookie = getCookie('data_frm');
        var obj_cookie = JSON.parse(data_cookie);
        if(Nombre == "" || Nombre == null ) {
            Nombre = obj_cookie.frm_nombre;
        }
        if(Ap_Pat == "" || Ap_Pat == null ) {
            Ap_Pat = obj_cookie.frm_apaterno;
        }
        if(Ap_Mat == "" || Ap_Mat == null ) {
            Ap_Mat = obj_cookie.frm_amaterno;
        }
        if(email == "" || email == null ) {
            email = obj_cookie.frm_email;
        }
        if(Telef == "" || Telef == null) {
            Telef = obj_cookie.frm_celular;
        }
    }
    //Validacion por sin los datos son de la cookie de sesion que se genero para solucionar el problema del refrescado

    if(checkCookie('preserve_form_calc_values')==true) {
        var data_cookie = getCookie('preserve_form_calc_values');
        var obj_cookie_p = JSON.parse(data_cookie);
        if(Nombre == "" || Nombre == null ) {
            Nombre = obj_cookie_p.frm_nombre;
        }
        if(Ap_Pat == "" || Ap_Pat == null ) {
            Ap_Pat = obj_cookie_p.frm_apaterno;
        }
        if(Ap_Mat == "" || Ap_Mat == null ) { 
            Ap_Mat = obj_cookie_p.frm_amaterno;
        }
        if(email == "" || email == null ) {
            email = obj_cookie_p.frm_correo;
        }
        if(Telef == "" || Telef == null) {
            Telef = obj_cookie_p.frm_celular;
        }
    }
    console.log("IMPRIMIR COOKIE ANTES DE ENVIO");
    console.log(obj_cookie_p);
    
    //var esbeltas = ['3954','3932','3953','3959','3956','3972','3976','3974','3961','3973','3914','3971']; //ok
    //var a = esbeltas.indexOf(jQuery('#calcApp').data('selectMateria').toString());




    /*if(jQuery('#calcApp').data('selectMateria') == 3954) {
        var a = 0;
    }else if(jQuery('#calcApp').data('selectMateria') == 3932) {
        var a = 0;
    }else if(jQuery('#calcApp').data('selectMateria') == 3953) {
        var a = 0;
    }else if(jQuery('#calcApp').data('selectMateria') == 3959) {
        var a = 0;
    }else if(jQuery('#calcApp').data('selectMateria') == 3956) {
        var a = 0;
    }else if(jQuery('#calcApp').data('selectMateria') == 3972) {
        var a = 0;
    }else if(jQuery('#calcApp').data('selectMateria') == 3976) {
        var a = 0;
    }else if(jQuery('#calcApp').data('selectMateria') == 3974) {
        var a = 0;
    }else if(jQuery('#calcApp').data('selectMateria') == 3961) {
        var a = 0;
    }else if(jQuery('#calcApp').data('selectMateria') == 3973) {
        var a = 0;
    }else if(jQuery('#calcApp').data('selectMateria') == 3914) {
        var a = 0;
    }else if(jQuery('#calcApp').data('selectMateria') == 3971) {
        
    }else {
        var a = -1;
    }*/


    /*Para el caso de ZM*/

    /*var esbforaneos = ["GDL", "LEO", "TOL", "QRO"];
    var b = esbforaneos.indexOf(jQuery('#calcApp').data('campus'));
    
    if (a >= 0 && b==-1 ) {
        C_Carrera=codigoAnterior(C_Carrera);
        programa=codigoAnterior(C_Carrera);
    }*/
   
    


    var pago_mens_beca = jQuery('#calcApp').data('pago_mensual_front');
    var fdata = "nivelOferta=" + nivelOferta + "&Herramienta=" + Herramienta + "&Campana=" + Campana + "&Origen=" + Origen + "&Banner=" + Banner + "&Calidad=" + Calidad + "&L_Negocio=" + L_Negocio + "&F_Nacim=" + F_Nacim + "&Ciclo=" + Ciclo + "&Alumno=" + Alumno + "&Estad=" + Estad + "&C_Carrera=" + C_Carrera + "&Carrera=" + Carrera + "&banner_id=" + banner_id + "&CP=" + CP + "&Pobla=" + Pobla + "&name_form=" + name_form + "&Sexo=" + Sexo + "&nom_carrera=" + nom_carrera + "&nom_campus=" + nom_campus + "&promedioF=" + promedioF + "&Pdescuento=" + Pdescuento + "&nivelF=" + nivelF + "&CostoUnico=" + CostoUnico + "&num_carrera=" + num_carrera + "&costoMensual=" + costoMensual + "&descuentoF=" + descuentoF + "&planSelec=" + planSelec + "&CostoSelec=" + CostoSelec + "&planRecom=" + planRecom + "&CostoRecom=" + CostoRecom + "&envioB=" + envioB + "&C_Carrera_WP=" + C_Carrera_WP + "&Direcc=" + Direcc + "&Colon=" + Colon + "&Nombre2=" + Nombre2 + "&email2=" + email2 + "&URLreferer=" + URLreferer + "&debugs=" + debugs + "&gclid_field=" + gclid_field + "&userId_field=" + userId_field + "&CID=" + CID + "&costoMateria=" + costoMateria + "&email_unitec=" + email_unitec + "&otraConsulta=" + otraConsulta + "&SERVER_URL=" + SERVER_URL + "&frm_LadaLenght=" + frm_LadaLenght + "&frm_LadaAllow=" + frm_LadaAllow + "&alum_2_2=" + alum_2_2 + "&Nombre=" + Nombre + "&Ap_Pat=" + Ap_Pat + "&Ap_Mat=" + Ap_Mat + "&email=" + email + "&informacion_2=" + informacion_2 + "&Estado=" + Estado + "&masculino=" + masculino + "&TipoTel=" + TipoTel + "&Telef=" + Telef + "&periodo_interes_sel=" + periodo_interes_sel + "&politica=" + politica + "&promedio=" + promedio + "&programa=" + programa + "&materias=" + materias + "&hubspotutk=" + cookieHubspot + "&padretutor=" + padretutor + "&termino=" + anos_termino + "&pago_mensual_beca=" + pago_mens_beca;
    console.log("ANTES DE ENVIAR:");
    console.log(fdata);
    var checkURL = jQuery("#otraConsulta").val();
    var urlCalc = (jQuery("#calcApp").data('regcompleto') == 0) ? "//www.unitec.mx/wp-content/themes/temaunitec/calculadora/v2_calc/almacen1.php" : "//www.unitec.mx/wp-content/themes/temaunitec/calculadora/v2_calc/otra_consulta.php";

        jQuery.ajax({
            url: urlCalc,
            data: fdata,
            dataType: "JSON",
            cache: false,
            type: "POST",
            success: function(msg) {
                //console.log("Ejecutado"+msg);
                //Cuando retorne el valor debe considerarse como otra consulta ya que seguira enviando sus datos.


                jQuery("#otraConsulta").val(1);
                jQuery("#calcApp").data('regcompleto', 1);
                setCookie('frmunitec', "calculadora", 1);
                if (msg.tipo == "nuevo") {
                    dataLayer.push({
                        'event': 'CALCULADORA_REGISTRO'
                    });
                    //JEABIX
                    dataLayer.push({
                          'event': 'IX_web_registro'
                    });
                    console.log("Se esta ejecutando ahora");
                    dataLayer.push({
                        'event': '"' + Banner + '"'
                    });

                    //ga(function(tracker) { clientId = tracker.get('clientId');});
                    dataLayer.push({
                        //'leadId': clientId, //dato dinámico
                        //  'CDClientID':clientId,
                        'origenLead': 'frmCalcu', //dato dinámico
                        'isAlumn': Alumno, //dato dinámico
                        'ingress': CP, //dato dinámico
                        'state': Estado, //dato dinámico
                        'levelStudies': L_Negocio, //dato dinámico
                        'Period': Ciclo, //dato dinámico
                        'carrera': Carrera, //dato dinámico
                        'campus': nom_campus, //dato dinámico
                        'date': new Date(), //dato dinámico
                        'event': 'leadGenerationCalcu' //dato estático
                    });

                } else {
                    dataLayer.push({
                        'event': 'CALCULADORA_DUPLICADO'
                    });
                    dataLayer.push({
                        'url_value': 'formularioCalculadora/Duplicado', //dato dinÃ¡mico
                        'event': 'StepForm' //dato estÃ¡tico
                    });
                    dataLayer.push({
                          'event': 'IX_web_duplicado'
                    });

                }

                document.cookie = "utm_campaign=; expires=Thu, 01 Jan 1970 00:00:00 UTC;path=/"; //elimina la cookie utm_campaign una vez enviado

            }
        });


}
/*JEAB FUNCION PARA ASIGNAR CARRERA A EL BLOQUE INFERIOR DE SEC-CURSAR 28-04-2016*/
function setCarrera() {
    var carrera = '';
    carrera = jQuery(".sel-carrera option:selected").text();
    var re = /Selecciona/g;
    var result = carrera.replace(re, "");
    //JEAB ASIGNAMOS VALOR AL SPAN PARA LA CARRERA 28-04-2016
    if (result != 'undefined' && result != 'Selecciona') {

        jQuery('#nombre-quiero-cursar').html(result);
        jQuery('#titulo-seccion-carrera').html('<span class="faa-parent animated-hover">CAMBIAR CARRERA  <i id="i-cambiar-carrera" class="faa-parent fa fa-play faa-horizontal animated-hover faa-slow hide-fa"></i></span>');
        jQuery('#nombre-campus').html('');

    }
    //Validar que se hayan seleccionado datos necesarios
    if (jQuery('#calcApp').data().deMateria != 'undefined' && jQuery('#calcApp').dataselectLinea != 'undefined') {

        jQuery('.nuevo-boton-ir-campus').removeAttr('display', 'none');
        jQuery('.nuevo-boton-ir-campus').attr('display', 'true');

    } else {

        jQuery('.nuevo-boton-ir-campus').removeAttr('display', 'true');
        jQuery('.nuevo-boton-ir-campus').attr('display', 'none');

    }
    //jQuery('nombre-quiero-cursar').html(jQuery('#calcApp').data());
}

/*JEAB FUNCION PARA ASIGNAR CARRERA A EL BLOQUE INFERIOR DE SEC-CURSAR 28-04-2016*/

function setCampus(campus) {
    //var nombreCampus = jQuery('#calcApp').data().decampus;
    jQuery('#nombre-campus').html(campus);
    if (jQuery(window).width() < 768) {

        jQuery('#titulo-seccion-campus').html('<span>CAMBIAR CAMPUS  </span>');

    } else {

        jQuery('#titulo-seccion-campus').html('<span class="faa-parent animated-hover">CAMBIAR CAMPUS  <i id="i-cambiar-campus" class="faa-parent fa fa-play faa-horizontal animated-hover faa-slow hide-fa"></i></span>');

    }

}
