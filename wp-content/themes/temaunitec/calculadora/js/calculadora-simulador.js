function frm_sel_inf(valor){
    jQuery('.frm_select-af').removeClass("frm_choosed_blue");
    if(valor=="inf1"){jQuery(".inf1").addClass("frm_choosed_blue");valor=10040;}
    if(valor=="infrev"){jQuery(".infrev").addClass("frm_choosed_blue");valor=10183;}
    jQuery("#frm_informacion").val(valor);
}   

  function mobileExpand(boton1){
    var boton=jQuery('.'+boton1);
    boton.css({"position":"absolute","z-index":99});
    boton.stop().animate({width:"100%"});
    if(boton1=='btn-materias'){boton.html('<div class="row espacio-prom-mater">Materias a cursar:</div><div class="col-xs-4"><div ><i data-slider="ex14" class="fa fa-minus fa-2x  control-menos-mat"></i></div></div><div class="col-xs-4" style="font-size: 60px; margin-top: -17px; margin-left:0%;" ><div  class="materias-val  montserrat">'+jQuery('#calcApp').data('materias')+'</div></div> <div class="col-xs-4"  style="margin-left="15px"><div ><i data-slider="ex14" class="fa fa-plus fa-2x control-mas-mat"></i></div></div>');}
    if(boton1=='btn-promedio'){boton.html('<div class="row espacio-prom-mater">Promedio:</div><div class="col-xs-4"><div ><i data-slider="ex13" class="fa fa-2x fa-minus  control-menos"></i></div></div><div class="col-xs-4" style="font-size: 60px; margin-top: -17px; margin-left:0%;" ><div  class="promedio-val  montserrat">'+jQuery('#calcApp').data('promedio')+'</div></div> <div class="col-xs-4"  style="margin-left="15px"><div ><i data-slider="ex13" class="fa fa-2x fa-plus control-mas"></i></div></div>');}
    jQuery('.color-control').prepend('<div class="btn-close">X</div>');
    jQuery('.btn-close').data('btn',boton1);
  }

  function  mobileCollapse(boton1){

     var boton=jQuery('.'+boton1);
    if(boton1=="btn-materias"){boton.html('<p>MATERIAS</p><i class="fa"></i><div class="row pos-relative small-text hidden-xs matctrl" style="display: block;"><div class="col-xs-6" style="    text-align: right;    font-size: 83px;    margin-top: -45px;"><div class="materias-val  montserrat">'+jQuery('#calcApp').data('materias')+'</div></div><div class="col-xs-6" style="text-align: left;font-size: 23px;    top: -38px;"><div><i data-slider="ex14" class="fa fa-plus control-mas-mat"></i></div><div><i data-slider="ex14" class="fa fa-minus  control-menos-mat"></i></div></div></div>')}
    if(boton1=="btn-promedio"){boton.html('<p>PROMEDIO</p><i class="fa"></i><div class="row pos-relative small-text hidden-xs promctrl" style="display: block;"><div class="col-xs-6"><div style="    text-align: right;    font-size: 83px;    margin-top: -45px;" class="promedio-val montserrat">'+jQuery('#calcApp').data('promedio')+'</div></div><div class="col-xs-6" style="text-align: left;font-size: 23px;top: -38px;"><div><i data-slider="ex13" class="control-mas fa fa-plus "></i></div><div><i data-slider="ex13" class="control-menos fa fa-minus"></i></div></div></div>')}
      jQuery('.btn-close').remove();
      boton.css({"position":"relative","z-index":""});
      boton.css({"width":''});

  }

  function formatEmail(email){

    email=email.toLowerCase();
    //console.log("email/"+email); 
    var arrEmailCorrecto=['hotmail.com','hotmail.es','gmail.com','yahoo.com','yahoo.com.mx','yahoo.mx']; 
    var arrEmail=email.split("@");
    var arrDominio=arrEmail[1].split(".");
    var dominio_ok;
    var email_valido;

    //console.log(arrEmail[1]);

        var i=0;
        var dominio=Array();
        //console.log("----"+arrDominio[0]);
        switch(arrDominio[0].substring(0,3)){

            case "hot" :
            if(arrDominio[1] && arrDominio[1].substring(0,1)=="c"){dominio_ok = "hotmail.com";}else
            if(arrDominio[1] && arrDominio[1].substring(0,1)=="e"){dominio_ok = "hotmail.es";}else{dominio_ok="hotmail.com";}    
            break;

            case "gma" :
            dominio_ok = "gmail.com";
            break;

            case "yah":
            if(arrDominio[2]){dominio_ok = "yahoo.com.mx";}else{
            if(arrDominio[1] && arrDominio[1].substring(0,1)=="c"){ dominio_ok = "yahoo.com"; }else 
            if(arrDominio[1] && arrDominio[1].substring(0,1)=="m"){ dominio_ok = "yahoo.mx"; }else{dominio_ok=arrEmail[1];} 
            }
            
            break;
            default:
            email_valido=email;
            break;
        }//fin switch
    
    if(!email_valido){
        email_valido=arrEmail[0]+"@"+dominio_ok;
    }
    
    //console.log("correo-valid:"+email_valido);
    
    return email_valido;
}


  function checkRepetido(numero){
     var repetido=0;
    for(i=0;i<9;i++){
        res=numero.substring(i,i+1)-numero.substring(i+1,i+2);
        if(Math.abs(res)==0){repetido++}else{repetido=0;}
        //console.log("Repetido:"+repetido);
        if(repetido>3){break;}
        }
    return repetido;
    }

function ValidaSoloNumeros() {
 if ((event.keyCode < 48) || (event.keyCode > 57)) 
  event.returnValue = false;
}

function ValidaSoloTexto() {
 if ((event.keyCode != 32) && (event.keyCode < 65) || (event.keyCode > 90) && (event.keyCode < 97) || (event.keyCode > 122))
  event.returnValue = false;
}

function EvaluateTextP(obj, e, id){
    var validoT= false; 
    tecla = (document.all) ? e.keyCode : e.which;
    /*if(id == "micro")
        validoT = numVecestextoMicro(tecla);
    else
        validoT = numVecestexto(tecla);*/
    if (tecla == 8 ){
        return true;
        }
    else if (tecla == 13 && obj.value.length > 0){
        espacio = true;
        return true;
        }

    //else if(validoT ==true){    
    if(tecla == 32 && espacio== true){
        espacio = false;
        return true;
    }else if (espacio= false && tecla ==32 ){
        return false;
    }       
    if((tecla!=32) && !((tecla > 8 && tecla < 33) || (tecla < 65 && tecla > 31) || (tecla > 90 && tecla < 97) || tecla > 122)){
        espacio= true;
    }   
    if ((tecla > 8 && tecla < 33) || (tecla < 65 && tecla > 31) || (tecla > 90 && tecla < 97) || tecla > 122 ){
        return  false;
    }   
    return true;
   // }
//else return false;
}   

function frm_SinEspacios(e){
    tecla = (document.all) ? e.keyCode : e.which;
    if (tecla ==32 ){return false;} 
    if (tecla !=32 ){return true;}
}

    

    function selectColor(obj) {jQuery(obj).addClass("naranja");}
    function identificaModalidad(valorModalidad) { switch (valorModalidad) { case 1: return 'Presencial'; break; case 2: return 'Ejecutiva'; break; case 3: return 'En Linea'; break; } }
    function queryHandler(json, queryString) { var db = SpahQL.db(json); var result = db.select(queryString); return result; }
    function unique(array) { var uniqueValues = []; jQuery.each(array, function(i, el) { if (jQuery.inArray(el, uniqueValues) === -1) uniqueValues.push(el); }); return uniqueValues; }
    function irA(anchor,cerrar){jQuery.fn.fullpage.moveTo(0, anchor); if(cerrar==0){ jQuery('#msj-modal').modal('hide');}}
    function validaForm(val) { return true; }
    function getParameterByName(name) { var match = RegExp('[?&]' + name + '=([^&]*)').exec(window.location.search); return match && decodeURIComponent(match[1].replace(/\+/g, ' ')); }
    function init() {

      /*******FUNCIONES DE INICIALIZACION********/


        //if(jQuery("#calcApp").data("formulario")==null){jQuery.fn.fullpage.moveTo(0, "sec-form");} //Si no ah llenado el formulario se obliga a llenarlo
        if (getParameterByName("utm_campus") || getParameterByName("utm_linea")) {
            jQuery("#calcApp").data("selectLinea", getParameterByName("utm_ln"));
            jQuery("#calcApp").data("campus", getParameterByName("utm_campus"));
            jQuery("#calcApp").data("banner", getParameterByName("utm_campaign"));

            if (jQuery("#calcApp").data("linea") != null) {/*getCategoriaNegocio({linea: jQuery("#calcApp").data("campus")});*/}
            if (jQuery("#calcApp").data("campus") != null) {getLineasNegocio({campus: jQuery("#calcApp").data("campus")});}
            if (jQuery("#calcApp").data("carrera") != null) {/*getCategoriaNegocio({linea: jQuery("#calcApp").data("campus")});*/}
            //NO SIGUE FLUJO NORMAL
        } else {
            //SIGUE FLUJO NATURAL
            getLineasNegocio({campus: ''});
            //validaForm();
            //validaLineaNegocio();
            //validaCarrera();
            //validaCampus();
            //validaPromedio();
            //validaMaterias();
        }
    }


    function getURLvars() {}
    function paso1() {return true}
    function watchDog() {if (jQuery("#calcApp").data('utm_campaign')) {}}
    function getLineasNegocio(options) {
      jQuery("#secLineas>.container>.row").empty();
      jQuery("#secLineas>.container-fluid").empty();
       //console.log("Ejecutando Funcion Lineas de Negocio");
        var options = typeof options == 'object' && options
            //Creamos un Array para insertar las lineas del query (Estan Duplicados los valores en este punto)
        var duplicatedLineas = new Array();
        //Variable de Control
        var i;
        if (options.campus) {
            //console.log("Se encontro la variable en URL");
            var qry = queryHandler(jsonCarreras, '/*[/campus/*}>{ {"' + options.campus + '"}]');

        } else {
            //console.log("No se encontro la variable en URL, carga default");
            var qry = queryHandler(jsonCarreras, '/*');
            //console.log(qry);
        }
            for (i = 0; i < qry.length; i++) {  duplicatedLineas[i] = qry[i].value.lineaWeb;}
            var lineasUnique = unique(duplicatedLineas);
            
//console.log("veces");
//console.log(lineasUnique);
var ejecuta=0;
        if(jQuery(window).width()>768){
          jQuery('#calcApp').data("desk",1);
            jQuery("#secLineas").append('\
              <div class="container-fluid">\
                <div class="row no-gutter">\
                  <div class="col-sm-4">\
                    <div data-animacion="1" data-linea="PREPARATORIA"  id="PREPARATORIA" class="linea-dsk clcLinea" data-resize="39"><div class="linea-elementos"> <div class="lineas-caption">PREPARATORIA</div></div> </div>\
                    <div data-animacion="2" data-linea="POSGRADO" id="POSGRADO" class="linea-dsk clcLinea" data-resize="39"><div class="linea-elementos"> <div class="lineas-caption">POSGRADO</div> </div></div>\
                  </div>\
                  <div class="col-sm-4">\
                  <div data-animacion="3" data-linea="LICENCIATURA" id="LICENCIATURA" class="linea-dsk clcLinea" data-resize="78"><div class="linea-elementos"> <div class="lineas-caption">LICENCIATURA</div></div> </div>\
                  </div>\
                  <div class="col-sm-4">\
                    <div data-animacion="4" data-linea="INGENIERIA" id="INGENIERIA" class="linea-dsk clcLinea" data-resize="39"><div class="linea-elementos"> <div class="lineas-caption">INGENIERIA</div></div> </div>\
                    <div data-animacion="5" data-linea="SALUD" id="SALUD" class=" linea-dsk clcLinea" data-resize="39"><div class="linea-elementos"> <div class="lineas-caption">SALUD</div></div> </div>\
                  </div>\
                   </div>\
              </div>');
              jQuery.each(lineasUnique, function(index, val) {
                //console.log(val);
                jQuery('div[data-linea="'+val+'"]').addClass("item-available");
              });
            escala();
          }else{

//console.log("ejecutando");
            jQuery("#secLineas").append('<div class="container"><div class="row padding-top-calcu"></div></div>');

            jQuery.each(lineasUnique, function(index, val) {
             // console.log(ejecuta);
              ejecuta++;
              
            jQuery("#secLineas>.container>.row").append('<div data-linea="'+val+'"  class="item-available col-xs-12 col-md-8 clcLinea clcSelector"> <img src="/wp-content/themes/temaunitec/'+myLibraryObject+'img/'+val+'.jpg" class="img-circle img-calcu" alt="img_circle" /><span>' + val + '</span><i class="fa fa-chevron-right pull-right"></i></div>');
            orderElements(jQuery("#secLineas>.container>.row"));
            });
            }

            


}



function getCampus(){

      var idDynamics=jQuery('#calcApp').data("selectMateria");
      jQuery("#secCampus").html('');
      jQuery("#secCampus>.container-fluid>.row").html('');
      debuglog("Clave Dynamics obtenido:"+idDynamics);

      if(!idDynamics){
       var losCampus   = ["ATZ", "MAR", "GDL","ECA", "SUR", "CUI","LEO","TOL","ONL"];
      //console.log("----------------------------------------------------------------------");
      //console.log('Ejecutando Funcion getCampus');
      //console.log('Query: /*/[/IdDynamics == "'+idDynamics+'"]');
      //console.log('Campus encontrados con el id: '+idDynamics);
      //console.log(losCampus);
        }else{
      var qryDynamics = queryHandler(jsonCarreras, '/*/[/IdDynamics == "'+idDynamics+'"]');
      var losCampus   = qryDynamics[0].value.campus;
      //console.log("----------------------------------------------------------------------");
      //console.log('Ejecutando Funcion getCampus');
      //console.log('Query: /*/[/IdDynamics == "'+idDynamics+'"]');
      //console.log('Campus encontrados con el id: '+idDynamics);
      //console.log(losCampus);
      }



if(jQuery(window).width()>768){
            jQuery("#secCampus").append('\
              <div class="container-fluid no-padding">\
                <div class="col-sm-8 no-padding">\
                  <div data-campus="ATZ" id="ATZ" class="col-sm-3 campus-dsk clCampus" data-resize="39"><div class="lineas-caption">Atizapán</div></div>\
                  <div data-campus="CUI" id="CUI" class="col-sm-3 campus-dsk clCampus" data-resize="39"><div class="lineas-caption">Cuitláhuac</div></div>\
                  <div data-campus="ECA" id="ECA" class="col-sm-3 campus-dsk clCampus" data-resize="39"><div class="lineas-caption">Ecatepec</div></div>\
                  <div data-campus="LEO" id="LEO" class="col-sm-3 campus-dsk clCampus" data-resize="39"><div class="lineas-caption">León</div></div>\
                  <div data-campus="MAR" id="MAR" class="col-sm-3 campus-dsk clCampus" data-resize="39"><div class="lineas-caption">Marina</div></div>\
                  <div data-campus="ONL" id="ONL" class="col-sm-3 campus-dsk clCampus" data-resize="39"><div class="lineas-caption">En Línea</div></div>\
                  <div data-campus="SUR" id="SUR" class="col-sm-3 campus-dsk clCampus" data-resize="39"><div class="lineas-caption">SUR</div></div>\
                  <div data-campus="TOL" id="TOL" class="col-sm-3 campus-dsk clCampus" data-resize="39"><div class="lineas-caption">Toluca</div></div>\
                </div>\
              </div>');

               
           jQuery("#secCampus>.container-fluid").append('\
              <div class="col-sm-4 no-padding">\
                <div data-campus="GDL" id="GDL" class="col-sm-12 campus-dsk clCampus" data-resize="78"><div class="lineas-caption">Guadalajara</div></div>\
              </div>');
            escala();


        jQuery.each(losCampus,function(key, value){
           var campusAbv=value;
        switch(value){
          case 'ATZ': //1
            key   = value;
            value = "Atizapán";
            break;
          case 'CUI': //2
            key   = value;
            value = "Cuitláhuac";
            break;
          case 'ECA'://3
            key   = value;
            value = "Ecatepec";
            break;
          case 'MAR'://5
            key   = value;
            value = "Marina";
            break;
          case 'SUR'://7
            key   = value;
            value = "Sur";
            break;
          case 'LEO'://4
            key   = value;
            value = "León";
            break;
          case 'TOL'://8
            key   = value;
            value = "Toluca";
            break;
          case 'ONL'://6
            key   = value;
            value = "En Línea";
            break;
          case 'GDL'://9
            key   = value;
            value = "Guadalajara";
            break;
        }

          jQuery('div[data-campus="'+campusAbv+'"]').addClass("item-available");
          jQuery('div[data-campus="'+campusAbv+'"]').find(".lineas-caption").html(value);
        })



       
        }else{

jQuery("#secCampus").append('<div class="container"><div class="row padding-top-calcu"></div></div>');
      var i=0;
      jQuery.each(losCampus,function(key, value){
        var img=value;
        switch(value){
          
          case 'ATZ': //1
            key   = value;
            value = "Atizapán";
            break;
          case 'CUI': //2
            key   = value;
            value = "Cuitláhuac";
            break;
          case 'ECA'://3
            key   = value;
            value = "Ecatepec";
            break;
          case 'MAR'://5
            key   = value;
            value = "Marina";
            break;
          case 'SUR'://7
            key   = value;
            value = "Sur";
            break;
          case 'LEO'://4
            key   = value;
            value = "León";
            break;
          case 'TOL'://8
            key   = value;
            value = "Toluca";
            break;
          case 'ONL'://6
            key   = value;
            value = "En Línea";
            break;
          case 'GDL'://9
            key   = value;
            value = "Guadalajara";
            break;
        }

            jQuery("#secCampus>.container>.row").append('<div data-campus="'+key+'" class="col-xs-12 col-md-8 clCampus clcSelector item-available"><img src="/wp-content/themes/temaunitec/calculadora/img/'+img+'.jpg" class="img-circle img-calcu" alt="img_circle2" /> <span>' + value + '</span><i class="fa fa-chevron-right pull-right"></i></div>');
             
              i++;
      });

     orderElements(jQuery("#secCampus>.container>.row"));

    }

    if(i==1){
      jQuery("#secCampus>.container>.row").append('<div class="col-xs-12 col-md-8 clcSelector "> <span>Esta carrera solo esta disponible en este campus </span><span class="btn-cursar">Elegir otra carrera</span></div>');
    }

return i;
}








    function escala() { 
        var pantallaCompleta = jQuery(window).height();  
        //jQuery('[data-resize]').not("#"+jQuery('.cerrarExpandible').data('affected')).each(
        jQuery('[data-resize]').each(
              function(index) { 
                medida = (jQuery(this).attr("data-resize") * pantallaCompleta) / 100;
               jQuery(this).css("height", medida); 
                }); 
         }
    function getCarreras() {

        jQuery("#secCategorias>.container>.row").html('');
         jQuery(".sel-carrera").empty();
        var lineaNegocio=jQuery('#calcApp').data("selectLinea");
        /*console.log("----------------------------------------------------------------------");
        console.log("Ejecutando getCarreras");
        console.log("Linea de Negocio :"+lineaNegocio);
        console.log("Campus :"+jQuery('#calcApp').data("campus"));*/

        var nombreCarrera   = [];
        var i = 0;
      switch (lineaNegocio) {
        case 'LICENCIATURA':
        var qry = queryHandler(jsonCarreras, '/*/[/lineaWeb=="LICENCIATURA"]/Grupo_carreras');
        break;
        case 'INGENIERIA':
        var qry = queryHandler(jsonCarreras, '/*/[/lineaWeb=="INGENIERIA"]/Grupo_carreras');
        break;
        case 'POSGRADO':
        var qry = queryHandler(jsonCarreras, '/*/[/lineaWeb=="POSGRADO"]/Grupo_carreras');
        break;
        case 'SALUD':
        var qry = queryHandler(jsonCarreras, '/*/[/lineaWeb=="SALUD"]/Grupo_carreras');
        break;
        case 'PREPARATORIA':
        var qry = queryHandler(jsonCarreras, '/*/[/lineaWeb=="PREPARATORIA"]/Grupo_carreras');
        break;
        default:
        var qry = queryHandler(jsonCarreras, '/*/Grupo_carreras');
      }

      if(jQuery('#calcApp').data("campus")){
        var qry = queryHandler(jsonCarreras, '/*/[/lineaWeb=="'+lineaNegocio+'"][/campus/*}>{ {"'+jQuery('#calcApp').data("campus")+'"}]/Grupo_carreras');
        
        if(qry.length==0){ jQuery('#calcApp').removeData("campus") ; getCarreras();}
      }

  
      

      var categoria = unique( qry.values() );
      var arrayCarreras = new Array;

        for (var i = 0; i < categoria.length; i++) {

          var results = queryHandler(jsonCategorias,"/0/"+categoria[i]+"");
          var nombreArray = results.values();

          arrayCarreras.push([{ "idcarrera" : categoria[i],  "carrera" : nombreArray[0] }]);

        };

      jQuery(".sel-carrera").append("<option>Selecciona</option>");
      var contCarrera=0;
        jQuery.each(arrayCarreras,function(key, value){
        contCarrera++;
        if(jQuery('#calcApp').data("desk")==1){
             jQuery(".sel-carrera").append('<option value="'+value[0].idcarrera+'" >' + value[0].carrera + '</option>');
        }else{
          jQuery("#secCategorias>.container>.row").append('<div data-categoria="'+value[0].idcarrera+'" class="item-available col-xs-12 col-md-8 clcCategoria clcSelector"><span class="ancho-carrera">' + value[0].carrera + '</span><i class="fa fa-chevron-right pull-right"></i></div>');
          orderElements(jQuery("#secCategorias>.container>.row"));
        }   
          
       
        });
        return contCarrera;

    }
     function getModalidad(idcarrera, categoria){
      /*console.log("----------------------------------------------------------------------");
      console.log('Ejecutando Funcion getModalidad');
      console.log('Parametros recibidos: idcarrera:'+idcarrera +"| categoria: "+categoria);*/
      jQuery(".carreramod").empty();
      jQuery("#secModalidades>.container>.row").html('');
      //console.log(idcarrera+"|"+categoria);
      if(idcarrera != ''){
        var qry = queryHandler(jsonCarreras, '/*[/IdDynamics=="'+idcarrera+'"]/');
        // console.log("Query ejecutada: "+'/*[/IdDynamics=="'+idcarrera+'"]/'+"debido al idcarrera: "+idcarrera);
      }else if(categoria != ''||categoria==0){
        var qry = queryHandler(jsonCarreras, '/*[/Grupo_carreras=="'+categoria+'"]/');
         //console.log("Query ejecutada: "+'/*[/Grupo_carreras=="'+categoria+'"]/'+"debido a la categoria: "+categoria);
      }

     /* console.log(qry);
      console.log("idcarrera: "+idcarrera+"   | Categoria:"+categoria);*/
var contMod=0;
      jQuery.each(qry,function(key, value){
        contMod++;
        var idCosto=value.value.catCosto;
        //alert(value.value.catCosto);
         switch( parseInt(value.value.modalidad) ) {
           case 1:
             key   = value.value.IdDynamics;
             value = 'Presencial';
             break;
           case 2:
             key   = value.value.IdDynamics;
             value = 'Ejecutiva';
             break;
           case 3:
             key   = value.value.IdDynamics;
             value = 'En Linea';
             break;
           case 4:
             key   = value.value.IdDynamics;
             value = 'Flexible';
             break;
           case 5:
             key   = value.value.IdDynamics;
             value = 'Hibrida';
             break;
           default:
             key   = value.value.IdDynamics;
             value = 'Presencial';


         }

              if(jQuery('#calcApp').data('desk')==1){
                  jQuery('.carreramod').append('\
                      <div class="radio">\
                        <label>\
                          <input type="radio" class="clcCarrerasMod-dsk" data-carreramod="'+key+'"  data-idcosto="'+idCosto+'" name="modalidadRadios"  value="'+key+'" >\
                          ' + value + '\
                        </label>\
                      </div>');

                  
              }else{

                jQuery("#secModalidades>.container>.row").append('<div data-carreramod="'+key+'" data-idcosto="'+idCosto+'" class="item-available col-xs-12 col-md-8 clcCarrerasMod clcSelector"><span>' + value + '</span><i class="fa fa-chevron-right pull-right"></i></div>');
                orderElements(jQuery("#secModalidades>.container>.row"));

              };

     
        });

        if(jQuery('#calcApp').data('desk')==1){
           if(getParameterByName("utm_campus")=="ONL"){
                      jQuery('.clcCarrerasMod-dsk[data-idcosto="POSON"]').click()
                      jQuery('.clcCarrerasMod-dsk[data-idcosto="DIPON"]').click()
                      jQuery('.clcCarrerasMod-dsk[data-idcosto="LICON"]').click()
                    }
          if(qry.length==1){ 
            jQuery('.clcCarrerasMod-dsk').click()
          }
        }
          
        return contMod;
    }

    function campusDynamics(campus){ 
      switch(campus){
        case "ATZ" :return "ATIZAPAN";
        break;
        case "MAR" :return "MARINA";
        break;
        case "ECA" :return "ECATEPEC";
        break;
        case "SUR" :return "SUR";
        break;
        case "CUI" :return "CUITLAHUAC";
        break;
        case "LEO" :return "LEON";
        break;
        case "TOL" :return "TOLUCA";
        break;
        case "GDL" :return "GUADALAJARA";
        break;
        case "ONL" :return "MARINA"; 
    break
    default: 
    return "MARINA";
    }
}

function getBecas(){
  //console.log('--------------------------------------------------------------------');
  //console.log('Ejecutando funcion getBecas ');
  var idcosto=jQuery('#calcApp').data('idCosto');
  var campus=jQuery('#calcApp').data('campus');
  if(campus!="LEO"){campus="TODOS";}
  //console.log('Campus:  '+campus+"  |idCosto:"+idcosto);

  var qryBecas = queryHandler(jsonBecas, '/*/'+idcosto+'/'+campus+'/*/');
  //console.log("Query Becas"+'/*/'+idcosto+'/'+campus+'/*/');
  //console.log(qryBecas);
  //console.log("Campus: "+campus);
  var promedio=jQuery('#calcApp').data('promedio');
  //console.log("Variables de entrada beca IdCosto:"+idcosto+" Campus:"+campus+" promedio:"+promedio);

  var rango=new Array();
 //console.log(qryBecas.values());
var contador=0;
    jQuery.each( qryBecas.values(), function( key, value ) {
            //console.log(value['promedio']);

           jQuery.each( value['promedio'], function( key1, value1 ) {

                rango[contador]=value;
                 rango[contador]['beca']=value['beca'];
            });
            contador++;
    });

//console.log(rango);

for(i=0; i<rango.length;i++){
//console.log(rango[i]['promedio']);

  //if (x >= rango[i][0] && x <= rango[i][1]) {  }
  //console.log(promedio+" "+rango[i][0]+" "+rango[i][1]);
  if ( parseFloat(promedio) >= parseFloat(rango[i]['promedio'][0]) && promedio <= parseFloat(rango[i]['promedio'][1])){
      //console.log("Entro al Rango: "+rango[i]['promedio'][0]+"| "+rango[i]['promedio'][1]+" Tu beca es "+rango[i]['beca'])
      if(jQuery('#calcApp').data('campus')=="GDL"){var becafundador=20;}else{var becafundador=0;}
      //var becafundador=0;
      var beca=parseInt(parseInt(rango[i]['beca'])+parseInt(becafundador));
  }

}

if(jQuery('#calcApp').data('campus')=="GDL"){
  console.log("Guada");
      if(promedio>=6 && promedio<=6.9){
      var beca=  parseInt(20);
      console.log('Fundador'+beca);
      }
    } 

if(!beca){var beca=0;}
  if( jQuery("#ex14").slider('getValue')<5 && jQuery('#calcApp').data('idCosto')!='INGON' && jQuery('#calcApp').data('idCosto')!='DIP' && jQuery('#calcApp').data('idCosto')!='DIPON' && jQuery('#calcApp').data('idCosto')!='POSON' && jQuery('#calcApp').data('idCosto')!='POS' ){
     debuglog("MAterias  es menor a 5 y es idCosto dip o dipon o poson o pos y se le quito la beca");
      jQuery('#calcApp').data('beca',0); 
    }else{
       if(jQuery("#ex14").slider('getValue')>=4){
        debuglog("es mayor o igual a 4 se aplica beca:"+beca);
        jQuery('#calcApp').data('beca',beca);
       }else{
        jQuery('#calcApp').data('data',0);
       }
      
    }

  jQuery('.app-desc').html(beca+"%");
}



/*function format2(n, currency) {
    return currency + " " + n.toFixed(0).replace(/(\d)(?=(\d{3})+\.)/g, "$1,");
}*/

function format2(n, currency) {
    return currency + " " + n.toFixed(0).replace(/./g, function(c, i, a) {
        return i > 0 && c !== "." && (a.length - i) % 3 === 0 ? "," + c : c;
    });
}


function getDuracion(anio){

  var anioArr={};
  var tipoPago={};
  var qryStrCostos = '/*/' + jQuery('#calcApp').data('idCosto') + '/costoMateria';
  var qryResults = queryHandler(jsonCostos, qryStrCostos);
  var losPagos = qryResults[0].value;
  var theKey = Object.keys(losPagos);
  var pago = {};
  var costos = [];
  var ahorro = {};
  var classi,cont=0;
  var lineaNegocio=jQuery('#calcApp').data('idCosto');
  var qryCostos = queryHandler(jsonCostos, '/*/'+lineaNegocio+'/duracion');
  var beca=jQuery('#calcApp').data('beca');

   if(anio){
      jQuery.map( qryCostos.value(), function( val, i ) { 
          jQuery('#calcApp').data('mat_'+i+"_"+val['Materias'],'mat_'+i+"_"+val['Materias']);
          //var materiasaCotizar=(jQuery('#calcApp').data('materias'))?jQuery('#calcApp').data('materias'):val['Materias'];
          var materiasaCotizar=val['Materias'];
          anioArr[i]={'materias':materiasaCotizar};
          jQuery('.materias-val').html(materiasaCotizar);
          jQuery.map( losPagos, function( value, a ) {  
           anioArr[i][a]=value[jQuery('#calcApp').data('campus')]*materiasaCotizar;
           anioArr[i][a+"-beca"]=((value[jQuery('#calcApp').data('campus')]*materiasaCotizar)*((100-beca)/100));//pagos con Becas
          });
        });

   }else{
        jQuery.map( qryCostos.value(), function( val, i ) { 
          jQuery('#calcApp').data('mat_'+i+"_"+val['Materias'],'mat_'+i+"_"+val['Materias']);
          var materiasaCotizar=(jQuery('#calcApp').data('materias'))?jQuery('#calcApp').data('materias'):val['Materias'];
          //var materiasaCotizar=val['Materias'];
          anioArr[i]={'materias':materiasaCotizar};
          jQuery('.materias-val').html(materiasaCotizar);
          jQuery.map( losPagos, function( value, a ) {  
           anioArr[i][a]=value[jQuery('#calcApp').data('campus')]*materiasaCotizar;
           anioArr[i][a+"-beca"]=((value[jQuery('#calcApp').data('campus')]*materiasaCotizar)*((100-beca)/100));//pagos con Becas
          });
        });
  }



  jQuery('#app-esquemapagos>.row>.botones_anios').empty();
  jQuery('#app-resumen').empty();
  
  jQuery.map(anioArr,function(val, i){
    console.log("val:"+val +" i:"+i);
    if (val['4pagos'] === undefined || val['4pagos'] === null) { pagoDisponible='6pagos'; mensualidades=6;}else{ pagoDisponible='4pagos';mensualidades=4;}


     if (cont == 0 && pagoDisponible!='6pagos' && Object.keys(anioArr).length!=1) {
              var inicio='style="display:none;"'
             classi = "btn-default";
           /* jQuery('#calcApp').data('planSelec',key);
            jQuery('#calcApp').data('CostoSelec',value);*/
        } else {
          var inicio="";
             jQuery('#calcApp').data("termino",i);
             classi = "btn-primary";
             /*jQuery('#calcApp').data('planRecom',key);
            jQuery('#calcApp').data('CostoRecom',value);*/
        }
        cont++;

        
        var tipoDuracion=(jQuery('#calcApp').data('idCosto')=='DIPON'||jQuery('#calcApp').data('idCosto')=='DIP')? '6 meses'  : i+" años" ;
        var pagoMensualconBeca=(jQuery('#calcApp').data('beca'))? val[pagoDisponible+'-beca']  :  val[pagoDisponible] ;
        var pagoContadoconBeca=(jQuery('#calcApp').data('beca'))? val['1pago-beca']  :  val['1pago'] ;
        var ahorroConBeca=(val[pagoDisponible+'-beca']>0)? val[pagoDisponible]-val[pagoDisponible+'-beca']  :  0 ;
        var ahorrodeContadoconBeca=(val[pagoDisponible+'-beca']>0)? ahorroConBeca+ (val[pagoDisponible+'-beca']*mensualidades)-val['1pago-beca']: ahorroConBeca+val['1pago']-(val[pagoDisponible]*mensualidades) ;  
        jQuery('#app-esquemapagos>.row>.botones_anios').append('<button data-esquema="' + i + '" type="submit" class="btn-esquema btn  btn-sm espacio ' + classi + '">' + tipoDuracion + '</button>');
        jQuery('#app-resumen').append('<div '+inicio+' class="row termino-'+i+'  p-mensual">              <div class="col-xs-6 col-sm-8 renglones texto-minimo">Pago Mensual</div> <div class="col-xs-6 col-sm-4 montserrat numero-calcu-resultado renglones-sl">'+format2(pagoMensualconBeca,"$")+'</div></div>');
        jQuery('#app-resumen').append('<div style="display:none;" class="row termino-'+i+'  p-contado">    <div class="col-xs-6 col-sm-8 renglones texto-minimo">Pago Cuatrimestral</div> <div class="col-xs-6 col-sm-4 montserrat numero-calcu-resultado renglones-sl">'+format2(pagoContadoconBeca,"$")+'</div></div>');
        jQuery('#app-resumen').append('<div '+inicio+' class="row margen-superior termino-'+i+' ">                         <div class="col-xs-6 col-sm-8 renglones texto-minimo">Beca del:</div><div class="col-xs-6 col-sm-4 montserrat numero-calcu-resultado renglones-sl">'+jQuery('#calcApp').data('beca')+'%</div></div>');
        if(i==3||i==4){jQuery('#app-resumen').append('<div '+inicio+' style="display:none;" class="row margen-superior termino-'+i+' p-contado" >                         <div class="col-xs-6 col-sm-8 renglones texto-minimo texto-naranja" style="top:0px">Pago de inscripción/reinscripción:</div><div class="col-xs-6 col-sm-4 montserrat numero-calcu-resultado renglones-sl texto-naranja" style=" font-size: 25px;">Sin Costo</div></div>');}
        jQuery('#app-resumen').append('<div '+inicio+' class="row termino-'+i+'  p-mensual">               <div class="col-xs-6 col-sm-8 renglones-texto texto-minimo">Total Ahorro</div><div class="col-xs-6 col-sm-4 montserrat numero-calcu-parrafo renglones-sl">'+ format2(ahorroConBeca,"$") +'</div></div>');
        jQuery('#app-resumen').append('<div style="display:none;"  class="row termino-'+i+' p-contado ">    <div class="col-xs-6 col-sm-8 renglones-texto texto-minimo">Total Ahorro</div><div class="col-xs-6 col-sm-4 montserrat numero-calcu-parrafo renglones-sl">'+ format2(ahorrodeContadoconBeca,'$') +'</div></div>');
        jQuery('#app-resumen').append('<div '+inicio+' class="row termino-'+i+' ">                         <div class="col-xs-6 col-sm-8 renglones-texto texto-minimo"># Materias a cursar</div><div class="col-xs-6 col-sm-4 montserrat renglones-sl numero-calcu-parrafo mat-calc-'+i+'">'+ Number(val['materias']) +'</div></div>');
        jQuery('#app-resumen').append('<div '+inicio+' class="row termino-'+i+' ">                         <div class="col-xs-12 "><p class="condiciones">*Aplican cambios y condiciones sin previo aviso</p></div></div>');
        jQuery('#app-resumen').append('<div '+inicio+' class="col-xs-12"><div class="text-center margen-inferior"> <a href="//www.unitec.mx/respaldo-economico-unitec/" class="texto-naranja">Conoce Respaldo Económico UNITEC</a> </div> </div>');
  });
 
 jQuery('.btn-esquema').removeClass('btn-primary');
 jQuery('.btn-esquema').addClass('btn-default');


if(anio){

   
      //jQuery('#calcApp').data('materias',materiasaCotizar);
      jQuery('.materias-val').html(jQuery('.mat-calc-'+anio).html());

                  jQuery("[data-esquema='"+anio+"']").removeClass('btn-default');
                 jQuery("[data-esquema='"+anio+"']").addClass('btn-primary');
}else{
  jQuery("[data-esquema]").each(function(){
          if(jQuery('#calcApp').data("mat_"+jQuery(this).data('esquema')+"_"+jQuery('#calcApp').data("materias") )){
                 jQuery("[data-esquema='"+jQuery(this).data('esquema')+"']").removeClass('btn-default');
                 jQuery("[data-esquema='"+jQuery(this).data('esquema')+"']").addClass('btn-primary');
              }
  });
}
            


        jQuery('#app-resumen').append('<div class="row"><div class="col-xs-12 col-sm-6 col-sm-offset-3 col-xs-offset-0"><div class="col-xs-5 col-sm-6"><button type="submit" onclick="gotoPlan();" class=" btn btn-default btn-sm ">PLAN DE ESTUDIOS</button></div><div class="col-xs-7 col-md-6"><div class="col-xs-7 col-md-8" style="font-size:12px; margin-left:10px;">Pago de contado:</div><div class="col-xs-5 col-md-4" style="margin-left:-10px;"><div class="toggle toggle-light"></div></div></div></div>');
     

   jQuery('.toggle').toggles({
        drag: true, // allow dragging the toggle between positions
        click: true, // allow clicking on the toggle
        text: {
          on: '', // text for the ON position
          off: '' // and off
        },
        on: false, // is the toggle ON on init
        animate: 250, // animation time
        transition: 'swing', // animation transition,
        checkbox: null, // the checkbox to toggle (for use in forms)
        clicker: null, // element that can be clicked on to toggle. removes binding from the toggle itself (use nesting)
        /*width: 10, // width used if not set in css
        height: 20, // height if not set in css*/
        type: 'compact' // if this is set to 'select' then the select style toggle will be used
    });
   // Getting notified of changes, and the new state:
jQuery('.toggle').on('toggle', function (e, active) {
  if (!active) {
    jQuery('.p-contado').hide();
    jQuery('.p-mensual.termino-'+jQuery('#calcApp').data("termino")).show();
  } else {
    jQuery('.p-mensual').hide();
    jQuery('.p-contado.termino-'+jQuery('#calcApp').data("termino")).show();
  }
});
  console.log(anioArr);
}

function gotoPlan(){
  var categoria=jQuery('#calcApp').data('selectCategoria');
  var qry = queryHandler(linkCategorias, '/*/'+categoria);
  
  if(qry.value().indexOf("prepa") != -1) {
    window.open("//www.unitec.mx/prepa/#perfil");
  }else {
    window.open("http://www.unitec.mx"+qry.value()+"#perfil");
  }
}

function getCosto() {/*
    //console.log("----------------------------------------------------------------------");
    //console.log("Ejecutando Funcion getCosto");
    
    var lineaNegocio=jQuery('#calcApp').data('idCosto');
    var noMaterias=jQuery('#calcApp').data('materias');
    var anios;
    var materias;
    console.log("Variebles de entrada: linea de Negocio: "+lineaNegocio+" |noMaterias: "+noMaterias);

    //debuglog("\nObteniendo Costos");
    //debuglog("Linea de negocio: "+lineaNegocio+" Materias:"+noMaterias);

    if(!noMaterias){ //si no trae materias, tendra que mostrar las materias de el mayor años de duracion
      //debuglog('Aun no interactua con materias');
     // var qryCostos = queryHandler(jsonCostos, '/ '+lineaNegocio+'/duracion');
      //console.log("El usuario no eligio materiasl y el query es "+'/'+lineaNegocio+'/duracion');
      if(noMaterias.length==0){
        console.log("No se encontraron conicidencias");
        errorCalculadora();}
      var contador=0;
      console.log(qryCostos[0].value);
        jQuery.each( qryCostos[0].value, function( key, value ) {
          if(contador==0){debuglog("Buscando en primer resultado en query: "+key);
                          jQuery('.resAnios1').html(key)};
          if(contador==1){
            debuglog("Buscando en segundo resultado en query: "+key);
            jQuery('.resAnios2').html(key)};
              jQuery.each( value, function( key, value ) {
               if(contador==0){jQuery('.materias1').html(value);
                              jQuery('#calcApp').data('materias',value);
                              getPagos(lineaNegocio, value, jQuery('#calcApp').data('beca'), jQuery('#calcApp').data('campus'));
                            }
               if(contador==1){jQuery('.materias2').html(value);}
              });
              contador++;
        });
    }else{
      debuglog("El usuario eligio materias: "+noMaterias);
      console.log('GETPAGOS');


      getPagos(lineaNegocio, jQuery('#calcApp').data('materias'), jQuery('#calcApp').data('beca'), jQuery('#calcApp').data('campus'));
    }*/
  }


function debuglog(towrite){
      jQuery.ajax({
        url: "../wp-content/themes/temaunitec/calculadora/ajaxdebug.php",  
        type: 'POST',
        cache: false,
        data: "accion="+towrite,
        progress: function(){},
        success: function(res){}
      }); 

}

function debuglogname(towrite){
      jQuery.ajax({
        url: "../wp-content/themes/temaunitec/calculadora/ajaxdebugname.php",  
        type: 'POST',
        cache: false,
        data: "accion="+towrite,
        progress: function(){},
        success: function(res){}
      }); 

}

function debugleave(towrite){
      jQuery.ajax({
        url: "../wp-content/themes/temaunitec/calculadora/ajaxdebugleave.php",  
        type: 'POST',
        cache: false,
        data: "seccion="+towrite+" | "+navigator.platform+" | "+navigator.userAgent,
        progress: function(){},
        success: function(res){}
      }); 

}


function getPagos(catCosto, noMaterias, descuento, campus) {

debuglog("\nEjecutando Funcion GetPagos")
   //debuglog("Variables de entrada:");
   //debuglog("catCosto:" + catCosto );
   //debuglog("noMaterias:" + noMaterias);
   //debuglog("Descuento:" + descuento)
   //debuglog("Campus:" + campus);

    console.log("%cEjecutando Funcion GetPagos", "color: red");
    console.log("--------------------Ejecutando Funcion getPagos----------------------------------");
    console.log("Variables de entrada:");
    console.log("catCosto:" + catCosto );
    console.log("noMaterias:" + noMaterias);
    console.log("Descuento:" + descuento)
    console.log("Campus:" + campus);


    jQuery('#app-esquemapagos>.row>.botones_anios').empty();
    jQuery('#app-resumen').html('');
    jQuery('#app-becas').html('');
    jQuery('#app-ahorro').html('');

    //console.log("Funcion:getPagos  Variables de Entrada: CarCosto: "+catCosto+" |noMaterias: "+noMaterias+" |Descruento: "+descuento+" |Campus: "+campus);
    //VARIABLES GENERALES
    if(!catCosto){
      alert("Selecciona la carrera y modalidad deseada");

      throw( "No existe el catCosto" );
    }
    var qryStrCostos = '/*/' + catCosto + '/costoMateria';
    //console.log("La"+qryStrCostos);
    var qryResults = queryHandler(jsonCostos, qryStrCostos);
    var losPagos = qryResults[0].value;
    var theKey = Object.keys(losPagos);
    var pago = {};
    var costos = [];
    var ahorro = {};
    var classi;
    //console.log(qryStrCostos);
    //console.log(qryResults);
    //console.log("los pagos abajo");
    //console.log(losPagos);
    //console.log(Object.keys(losPagos).length);
    for (var i = 0; i < Object.keys(losPagos).length; i++) {
        //console.log("un array");
        //console.log(losPagos[theKey[i]][campus]);
        if (Array.isArray(losPagos[theKey[i]][campus])) {

        console.log("--a");
            var variosPagos = losPagos[theKey[i]][campus];

            jQuery.each(variosPagos, function(key, value) {

                var costoPorMateria = value;
                jQuery('#calcApp').data('costoPorMateria',value);
                var pagoNormal = costoPorMateria * noMaterias;
                var descuentoBeca = pagoNormal * (descuento / 100);
                var totalPagar = pagoNormal - descuentoBeca;

                if (catCosto == "DIPON" || catCosto == "DIP") {

                    var elAhorro = descuentoBeca * 6;
                    debuglog('cat Costo:'+catCosto+" el ahorro: "+elAhorro);

                } else {

                    var elAhorro = descuentoBeca * 4
                     debuglog('cat Costo:'+catCosto+" el ahorro: "+elAhorro);
                }

                costos.push(totalPagar);
                pago[theKey[i]] = pagoNormal;
                ahorro[theKey[i]] = elAhorro;

            });


        } else {
//console.log("--b");
            var costoPorMateria = losPagos[theKey[i]][campus];
            jQuery('#calcApp').data('costoPorMateria',costoPorMateria);
            //console.log(costoPorMateria);
            var pagoNormal = costoPorMateria * noMaterias;
            var descuentoBeca = pagoNormal * (descuento / 100);
            var totalPagar = pagoNormal - descuentoBeca;

            if (catCosto == "DIPON" || catCosto == "DIP") {
                var elAhorro = descuentoBeca * 6;
                //console.log("el ahorro:"+pagoNormal )
                 //debuglog('cat Costo:'+catCosto+" el ahorro: "+elAhorro);
            } else {
  //console.log("B el ahorro:"+pagoNormal )
                var elAhorro = descuentoBeca * 4
                 //debuglog('cat Costo:'+catCosto+" el ahorro: "+elAhorro);
            }

            pago[theKey[i]] = pagoNormal;
            ahorro[theKey[i]] = elAhorro;

        }

    }

    i = 0;
//console.log("*******************");
//console.log(pago);


    jQuery.each(pago, function(key, value) {
      //console.log(value+"  "+key);
        if (i == 0) {
             classi = "btn-primary";
            jQuery('#app-esquemapagos>.row>.botones_anios').append('<div class="col-sm-8"></div>');
            jQuery('#calcApp').data('planSelec',key);
            jQuery('#calcApp').data('CostoSelec',value);
        } else {
             classi = "btn-default";
             jQuery('#calcApp').data('planRecom',key);
            jQuery('#calcApp').data('CostoRecom',value);

        }
        /*if(key=="1pago"){keyn="Pago Contado";}
        if(key=="4pagos"){keyn="Pago Mensual";}
        if(key=="6pagos"){keyn="Pago Mensual";}
        if(key=="1pago"||key=="4pagos"||key=="6pagos"){
        jQuery('#app-esquemapagos>.row').append('<div class="col-xs-6 col-sm-2"><button data-esquema="' + key + '" type="submit" class="btn-esquema btn  btn-sm ' + classi + '">' + keyn + '</button></div>');
         debuglog('<div class="col-xs-6 col-sm-2"><button data-esquema="' + key + '" type="submit" class="btn-esquema btn  btn-sm ' + classi + '">' + keyn + '</button></div>');
        i++;
        }*/

    });

    /*****************IMPRIME PAGOS EN HTML**********************/
var tresPagos;
var cuatroPagos;
var seisPagos;
var diezPagos;
var contado;
var totalCuatri;
var totalCuatriconBeca;
//console.warn(pago);
if(jQuery('#calcApp').data("idCosto")=="DIPON"||jQuery('#calcApp').data("idCosto")=="DIP"||jQuery('#calcApp').data("idCosto")=="ESP"){
              var tipoDuracion="semestre";
             }else{
              var tipoDuracion="cuatrimestre";
             }
var duracionCarrera="";
debuglog(tipoDuracion);
             if(jQuery('#calcApp').data('materias')==jQuery('#calcApp').data('maxmaterias')){
               duracionCarrera="para terminar en "+jQuery('#calcApp').data('maxmateriasanios')+" años";
               if(jQuery('#calcApp').data('maxmateriasanios')=="6m"){duracionCarrera="para terminar en 6 meses";}
             }
             if(jQuery('#calcApp').data('materias')==jQuery('#calcApp').data('minmaterias')){
               duracionCarrera="para terminar en "+jQuery('#calcApp').data('minmateriasanios')+" años";
                if(jQuery('#calcApp').data('maxmateriasanios')=="6m"){duracionCarrera="para terminar en 6 meses";}
             }


    jQuery.each(pago, function(key, value) {
      //console.log(key+"  "+value);

        if (key == "1pago") {
//console.log("Tipo duracion:"+tipoDuracion);
 //debuglog('1pago');
            var simulacion='<div class="row">Es como si pagaras: '+  format2( value - (value* (jQuery('#calcApp').data('beca')/100))  , "$") +' al mes</div>';
            var ahorroContado='<div class="row"> te estarias ahorrando <span class="ahorroContado"></span> si lo pagas de contado</div>';
             contado=value;

             
             

             
              jQuery('#app-resumen').append('<div class="col-xs-12 oculto" id="res-' + key + '">\
                                           <div class="row l1">Tu '+ tipoDuracion +' costaría: </div>\
                                           <div class="row numero-calcu-resultado montserrat"> ' + format2( value - (value* (jQuery('#calcApp').data('beca')/100))  , "$")  + '</div>\
                                           <div class="row l1"><!--Pagando mensualmente: '+format2((value - (value* (jQuery('#calcApp').data('beca')/100))) /4, "$")+'--></div>\
                                           <div class="row">con ' + jQuery('#calcApp').data('materias') + ' materias </div>\
                                           </div>');
              debuglog('<div class="col-xs-12 oculto" id="res-' + key + '">\
                                           <div class="row l1">Tu '+ tipoDuracion +' costaría: </div>\
                                           <div class="row numero-calcu-resultado montserrat"> ' + format2( value - (value* (jQuery('#calcApp').data('beca')/100))  , "$")  + '</div>\
                                           <div class="row l1"><!--Pagando mensualmente: '+format2((value - (value* (jQuery('#calcApp').data('beca')/100))) /4, "$")+'--></div>\
                                           <div class="row">con ' + jQuery('#calcApp').data('materias') + ' materias </div>\
                                           </div>');

              /*************IMPRIME BECAS***************/
              jQuery('#res-' + key ).append('<hr class="division-calcu">');
              jQuery('#res-' + key ).append('<div class="col-xs-12 l1"><div class="row">Tu beca es de</div> <div class="numero-calcu-resultado"> ' + jQuery('#calcApp').data('beca') + '%</div></div>');
             
              debuglog('<div class="col-xs-12 l1"><div class="row">Tu beca es de</div> <div class="numero-calcu-resultado"> ' + jQuery('#calcApp').data('beca') + '%</div></div>');

            //  if (  (parseInt(jQuery('#calcApp').data("materias")) < parseInt(jQuery('#calcApp').data("minmaterias"))) || (parseInt(jQuery('#calcApp').data("materias")) != parseInt(jQuery('#calcApp').data("maxmaterias")))   ) {
             if( jQuery("#ex14").slider('getValue')<5 && jQuery('#calcApp').data('idCosto')!='INGON' && jQuery('#calcApp').data('idCosto')!='DIP' && jQuery('#calcApp').data('idCosto')!='DIPON' && jQuery('#calcApp').data('idCosto')!='POSON' && jQuery('#calcApp').data('idCosto')!='POS' ){

              debuglog('<div>Debes tener 5 o mas materias para obtener beca </div>');
               jQuery('#res-' + key ).append('<div>Debes tener 5 o mas materias para obtener beca </div>');
              } else {
                jQuery('#res-' + key).append('<div id="ahorro-' + key + '">Tu ahorro es de ' + format2((jQuery('#calcApp').data('beca')/100)*value, "$") + ' y en tu '+tipoDuracion+'</div>');
                jQuery('#res-' + key).append(ahorroContado);
                debuglog('<div id="ahorro-' + key + '">Tu ahorro es de ' + format2((jQuery('#calcApp').data('beca')/100)*value, "$") + ' y en tu '+tipoDuracion+'</div>'+ahorroContado);
              }
              contado=value - (value* (jQuery('#calcApp').data('beca')/100));
              debuglog(contado);


        }
        /*if (key == "3pagos") {
            tresPagos=value;
            var html = '<div  class="oculto" id="res-' + key + '">Tu cuatrimestre costaria';
            for (i = 0; i < value.length; i++) {
                if (i == 0) {
                    html += " un pago de: ";
                }
                if (i == 1) {
                    html += " y dos pagos iguales de ";
                }
                html += " " + format2(value[i], "$") + " ";
            };
            html += '</div>';
            jQuery('#app-resumen').append(html);
        }*/
        if (key == "4pagos") {
            cuatroPagos=value;
            var CostoporMaterias=value;
             totalCuatri=CostoporMaterias*4;
            totalCuatriconBeca=(totalCuatri - (totalCuatri* (jQuery('#calcApp').data('beca')/100)));
            jQuery('#app-resumen').append('<div id="res-' + key + '">\
                                        <div class="row l1">Tu pago mensual sería de: </div>\
                                        <div class="row numero-calcu-resultado montserrat"> ' + format2( (totalCuatri - (totalCuatri* (jQuery('#calcApp').data('beca')/100)))/4  , "$")  + '</div>\
                                        <div class="row">Con ' + jQuery('#calcApp').data('materias') + ' materias</div>\
                                        <div class="row">y tu '+tipoDuracion+' completo costaría: ' + format2( (totalCuatri - (totalCuatri* (jQuery('#calcApp').data('beca')/100)))  , "$") +'</div>\
                                      </div>');
            debuglog('4 pagos');
            debuglog('<div id="res-' + key + '">\
                                        <div class="row l1">Tu pago mensual sería de: </div>\
                                        <div class="row numero-calcu-resultado montserrat"> ' + format2( (totalCuatri - (totalCuatri* (jQuery('#calcApp').data('beca')/100)))/4  , "$")  + '</div>\
                                        <div class="row">Con ' + jQuery('#calcApp').data('materias') + ' materias</div>\
                                        <div class="row">y tu '+tipoDuracion+' completo costaría: ' + format2( (totalCuatri - (totalCuatri* (jQuery('#calcApp').data('beca')/100)))  , "$") +'</div>\
                                      </div>');
              /*************IMPRIME BECAS***************/
              jQuery('#res-' + key ).append('<hr class="division-calcu">');
              jQuery('#res-' + key ).append('<div class="col-xs-12 l1"><div class="row">Tu beca es de</div> <div class="numero-calcu-resultado"> ' + jQuery('#calcApp').data('beca') + '%</div></div>');
  //            if (  (parseInt(jQuery('#calcApp').data("materias")) < parseInt(jQuery('#calcApp').data("minmaterias"))) || (parseInt(jQuery('#calcApp').data("materias")) != parseInt(jQuery('#calcApp').data("maxmaterias")))   ) {
if( jQuery("#ex14").slider('getValue')<5 && jQuery('#calcApp').data('idCosto')!='INGON' && jQuery('#calcApp').data('idCosto')!='DIP' && jQuery('#calcApp').data('idCosto')!='DIPON' && jQuery('#calcApp').data('idCosto')!='POSON' && jQuery('#calcApp').data('idCosto')!='POS' ){
//
                jQuery('#res-' + key ).append('<div>Debes tener 5 o mas materias para obtener beca </div>');
                debuglog('<div>Debes tener 5 o mas materias para obtener beca </div>');
              } else {

                jQuery('#res-' + key).append('<div id="ahorro-' + key + '">Tu ahorro es de ' + format2(totalCuatri-(totalCuatri - (totalCuatri* (jQuery('#calcApp').data('beca')/100))), "$") + ' en tu '+tipoDuracion+'</div>');
                debuglog('<div id="ahorro-' + key + '">Tu ahorro es de ' + format2(totalCuatri-(totalCuatri - (totalCuatri* (jQuery('#calcApp').data('beca')/100))), "$") + ' en tu '+tipoDuracion+'</div>');
              }
             //jQuery('.simulacion').html(format2(contado/4, "$"));   


        }
        if (key == "6pagos") {
          debuglog('6pagos')
            seisPagos=value;
            var CostoporMaterias=value;

             totalCuatri=CostoporMaterias*6;
             totalCuatriconBeca=(totalCuatri - (totalCuatri* (jQuery('#calcApp').data('beca')/100)));
           /* jQuery('#app-resumen').append('<div class="col-xs-12 " id="res-' + key + '">\
                                           <div class="row l1">Tu '+ tipoDuracion +' costaria: </div>\
                                           <div class="row numero-calcu-resultado montserrat"> ' + format2( value - (value* (jQuery('#calcApp').data('beca')/100))  , "$")  + '</div>\
                                           <div class="row l1">Pagando mensualmente: '+format2((value - (value* (jQuery('#calcApp').data('beca')/100))) /4, "$")+'</div>\
                                           <div class="row">con ' + jQuery('#calcApp').data('materias') + ' materias '+ duracionCarrera +'</div>\
                                           </div>');*/

           jQuery('#app-resumen').append('<div id="res-' + key + '">\
                                        <div class="row l1">Tu pago mensual sería de: </div>\
                                        <div class="row numero-calcu-resultado montserrat"> ' + format2( (totalCuatri - (totalCuatri* (jQuery('#calcApp').data('beca')/100)))/6  , "$")  + '</div>\
                                        <div class="row">Con ' + jQuery('#calcApp').data('materias') + ' materias</div>\
                                        <div class="row">y tu '+tipoDuracion+' completo costaría: ' + format2( (totalCuatri - (totalCuatri* (jQuery('#calcApp').data('beca')/100)))  , "$") +'</div>\
                                      </div>');
           debuglog('<div id="res-' + key + '">\
                                        <div class="row l1">Tu pago mensual sería de: </div>\
                                        <div class="row numero-calcu-resultado montserrat"> ' + format2( (totalCuatri - (totalCuatri* (jQuery('#calcApp').data('beca')/100)))/6  , "$")  + '</div>\
                                        <div class="row">Con ' + jQuery('#calcApp').data('materias') + ' materias</div>\
                                        <div class="row">y tu '+tipoDuracion+' completo costaría: ' + format2( (totalCuatri - (totalCuatri* (jQuery('#calcApp').data('beca')/100)))  , "$") +'</div>\
                                      </div>');

            /*************IMPRIME BECAS***************/
              jQuery('#res-' + key ).append('<hr class="division-calcu">');
              jQuery('#res-' + key ).append('<div class="col-xs-12 l1"><div class="row">Tu beca es de</div> <div class="numero-calcu-resultado"> ' + jQuery('#calcApp').data('beca') + '%</div></div>');
              
              //if (  (parseInt(jQuery('#calcApp').data("materias")) < parseInt(jQuery('#calcApp').data("minmaterias"))) || (parseInt(jQuery('#calcApp').data("materias")) != parseInt(jQuery('#calcApp').data("maxmaterias")))   ) {
              if( jQuery("#ex14").slider('getValue')<5 && jQuery('#calcApp').data('idCosto')!='INGON' && jQuery('#calcApp').data('idCosto')!='DIP' && jQuery('#calcApp').data('idCosto')!='DIPON' && jQuery('#calcApp').data('idCosto')!='POSON' && jQuery('#calcApp').data('idCosto')!='POS' ){

                jQuery('#res-' + key ).append('<div>Debes tener 5 o más materias para obtener beca </div>');
                debuglog('<div>Debes tener 5 o más materias para obtener beca </div>');
              } else {

                jQuery('#res-' + key).append('<div id="ahorro-' + key + '">Tu ahorro es de ' + format2(totalCuatri-(totalCuatri - (totalCuatri* (jQuery('#calcApp').data('beca')/100))), "$") + ' en tu '+tipoDuracion+'</div>');
                debuglog('<div id="ahorro-' + key + '">Tu ahorro es de ' + format2(totalCuatri-(totalCuatri - (totalCuatri* (jQuery('#calcApp').data('beca')/100))), "$") + ' en tu '+tipoDuracion+'</div>');
              }
        }
        /*if (key == "10pagos") {
             diezPagos=value;//
            jQuery('#app-resumen').append('<div  id="res-' + key + '">Tu pago mensual es de ' + format2(value, "$") + '</div>');
        }*/
    });
      if(isNaN(contado)!=true){
            debuglog("mi ahorro:"+contado);
            debuglog("ahorroContado:"+contado);
            jQuery('.ahorroContado').html( format2(totalCuatriconBeca-contado,"$"));

      }

            //jQuery('.ahorroContado').html( (value-contado/4)*4 + ")");
            
    /*console.log(ahorro);
    console.log(pago);*/

}




  function getAnioslimite(){
    var lineaNegocio=jQuery('#calcApp').data('idCosto');
    var anios;
    var materias;
    var noMaterias=jQuery('#calcApp').data('materias');
    //console.log("----------------------------------------------------------------------");
    //console.log("Ejecutando Funcion getAnioslimite");
    //console.log("Ejecuta Query en costos.json");
    //console.log("Variables: Linea de negocio: "+lineaNegocio+"  | Numero de materias: "+noMaterias);
    //console.log("noMaterias: "+noMaterias)

    //debuglog("----------------------------------------------------------------------");
    //debuglog("Ejecutando Funcion getAnioslimite");
    //debuglog("Ejecuta Query en costos.json");
    //debuglog("Variables: Linea de negocio: "+lineaNegocio+"  | Numero de materias: "+noMaterias);
    //debuglog("noMaterias: "+noMaterias)
    
    //limpieza de variables 
    jQuery('#calcApp').removeData('maxmaterias');  
    jQuery('#calcApp').removeData('maxmateriasanios');
    jQuery('#calcApp').removeData('minmaterias');
    jQuery('#calcApp').removeData('minmateriasanios');


      var qryCostos = queryHandler(jsonCostos, '/*/'+lineaNegocio+'/duracion');
      var contador=0;
      if(qryCostos.length!=1){
        //console.log("[FATAL ERROR] No se encontro coincidencia en el query: "+'/*/'+lineaNegocio+'/duracion');
        errorCalculadora();
      }
        jQuery.each( qryCostos[0].value, function( anios, value ) {
          //console.log(anios);
          if(contador==0){jQuery('.resAnios1').html(anios)};
          if(contador==1){jQuery('.resAnios2').html(anios)};
              jQuery.each( value, function( key, value ) {
                
               // console.log("key: "+key+" valor:"+value);
               if(contador==0){
                              jQuery('#calcApp').data("maxmaterias",value);
                              jQuery('#calcApp').data("maxmateriasanios",anios);
                              jQuery('.materias1').html(value);
                                jQuery('#calcApp').data('materias',value);
                                getPagos(lineaNegocio, value, jQuery('#calcApp').data('beca'), jQuery('#calcApp').data('campus'));
                                //console.log('esto paso'+lineaNegocio+ "|" +value+ "|" + jQuery('#calcApp').data('beca')+ "|" + jQuery('#calcApp').data('campus')     )
                              }
               if(contador==1){
                jQuery('#calcApp').data("minmaterias",value);
                jQuery('#calcApp').data("minmateriasanios",anios);
                  jQuery('.materias2').html(value);}
              });

                //comentado porque deberia ser dinamico
               /*if(anios=="6m"){ //Es una carrera semestral que solo mostrara 4 materias
                alert("semestral");
                jQuery('#ex14').slider('destroy');
                jQuery("#ex14").slider({
                ticks: [1,2, 3, 4],
                ticks_labels: ['1','2', '3', '4'],
                step:1,
                });
                jQuery('#materias-val').html("4");

          }else if(lineaNegocio=='ESP'){

             jQuery('#ex14').slider('destroy');
                jQuery("#ex14").slider({
                ticks: [1,2, 3, 4,5,6,7,8,9,10],
                ticks_labels: ['1','2', '3', '4','5','6','7','8','9','10'],
                step:1,
                });
                jQuery('#materias-val').html("5");

          }else{

                jQuery('#ex14').slider('destroy');
                jQuery("#ex14").slider({
                ticks: [1,2, 3, 4,5,6,7],
                ticks_labels: ['1','2', '3', '4','5','6','7'],
                step:1,
                });
                jQuery('#materias-val').html("5");

          }*/




              contador++;
        });

                //slider constructor
                var materias_label=[];
                var materia=[];
                for(i=1;i<parseInt(jQuery('#calcApp').data('maxmaterias'))+1;i++){
                  materias_label.push(i.toString());
                  materia.push(i);
                }
                jQuery('#ex14').slider('destroy');
                jQuery("#ex14").slider({
                ticks: materia,
                ticks_labels: materias_label,
                step:1,
                });
                if(jQuery('#calcApp').data('minmaterias')){
                  jQuery('#ex14').slider('setValue',parseInt(jQuery('#calcApp').data('minmaterias')));
                  jQuery('.materias-val').html(parseInt(jQuery('#calcApp').data('minmaterias')));
                  jQuery('#materias-val').html(jQuery('#calcApp').data('minmaterias'));
                  //jQuery('.control-mas-mat').fadeOut();
                }else{
                  jQuery('#ex14').slider('setValue',parseInt(jQuery('#calcApp').data('maxmaterias')));
                  jQuery('#materias-val').html(jQuery('#calcApp').data('maxmaterias'));
                }
  
  }


function orderElements(coleccion){
  var mylist = coleccion;
  var listitems = mylist.children('div').get();
  listitems.sort(function(a, b) {
     return jQuery(a).text().toUpperCase().localeCompare(jQuery(b).text().toUpperCase());
  });
  jQuery.each(listitems, function(index, item) {
     mylist.append(item);
  });
}

function scrollToTop(){
  //jQuery("html, body").animate({ scrollTop: 0 }, "fast");
  window.scrollTo(0, 0);
}

function errorCalculadora(){
  //console.log("Ejecutando funcion de error");
  alert("Revisa consola de errores");
}

function animar(div){ 
  jQuery('.unitec-logo').fadeOut();
  jQuery('.backMenu').fadeIn();
  jQuery('.backMenu').addClass('btn btn-primary');
  jQuery('.backMenu').addClass('cerrarExpandible');
  var pantallaH=jQuery(window).height(); 
  var pantallaW=jQuery(window).width();
  jQuery(div).data("ex-top",jQuery(div).position().top+"px" );
  jQuery(div).data("ex-right",jQuery(div).position().right+"px");
  jQuery(div).data("ex-bottom",jQuery(div).position().bottom+"px" );
  jQuery(div).data("ex-left",jQuery(div).position().left+"px");
  jQuery(div).data("ex-width",jQuery(div).width() );
  jQuery(div).data("ex-height",parseFloat(jQuery(div).outerHeight()).toFixed(2) );  
jQuery('.cerrarExpandible').data("affected",jQuery(div).attr("id"));

  if(jQuery(div).data("animacion")==1){
    jQuery(div).animate({"height": pantallaH+"px","width":pantallaW}, 500).css({"z-index":"99","position":"absolute","width":jQuery(div).width()+"px"});
  }

  if(jQuery(div).data("animacion")==2){
    jQuery(div).css({"z-index":"99","top":jQuery(div).data("ex-top"),"position":"absolute","width":jQuery(div).data("ex-width")}).animate({"top":0,"height":pantallaH,"width":pantallaW});
  }
  if(jQuery(div).data("animacion")==3){
    jQuery(div).css({"z-index":"99","top":jQuery(div).data("ex-top"),"position":"absolute","width":jQuery(div).data("ex-width")}).animate({"top":0,"left":"-"+jQuery(div).offset().left,"height":pantallaH,"width":pantallaW});
  }
  if(jQuery(div).data("animacion")==4){
    jQuery(div).css({"z-index":"99","top":jQuery(div).data("ex-top"),"position":"absolute","width":jQuery(div).data("ex-width")}).animate({"top":0,"left":"-"+jQuery(div).offset().left,"height":pantallaH,"width":pantallaW});
  }
  if(jQuery(div).data("animacion")==5){
    jQuery(div).css({"z-index":"99","top":jQuery(div).data("ex-top"),"position":"absolute","width":jQuery(div).data("ex-width")}).animate({"top":0,"left":"-"+jQuery(div).offset().left,"height":pantallaH,"width":pantallaW});
  }

jQuery('.linea-elementos').css({ "width":"100%", "height":"100%"});

//console.log(jQuery(div).html());

   
}


function reiniciarAppData(){
  jQuery('#calcApp').removeData('decampus');
  jQuery('#calcApp').removeData('idCosto');
  jQuery('#calcApp').removeData('selectCategoria');
  jQuery('#calcApp').removeData('selectLinea');
  jQuery('#calcApp').removeData('selectMateria');
}

function mostrarAlerta(msj,tipo){
        if(tipo==1){

          jQuery('.msj-modal-title').html("Faltan campos");
          jQuery('.msj-modal-body').html(msj);
          jQuery('.msj-modal-btn').html('<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>\
          <button type="button" class="btn btn-primary " onclick="irA(\'sec-campus\',0)">Aceptar</button>');
          
        }
        if(tipo==2){

          jQuery('.msj-modal-title').html("Faltan campos");
          jQuery('.msj-modal-body').html(msj);
          jQuery('.msj-modal-btn').html('<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>\
          <button type="button" class="btn btn-primary " onclick="irA(\'sec-cursar\',0)">Aceptar</button>');
          
        }


    jQuery('#msj-modal').modal('show');
}


function validaCampus(campus){
    switch(campus){
           case "ATZ" :return"ATIZAPAN";
        break;
        case "MAR" :return "MARINA";
        break;
        case "ECA" :return "ECATEPEC";
        break;
        case "SUR" :return "SUR";
        break;
        case "CUI" :return "CUITLAHUAC";
        break;
        case "LEO" :return "LEON";
        break;
        case "TOL" :return "TOLUCA";
        break;
        case "GDL" :return "GUADALAJARA";
        break;
        case "ONL" :return "ONLINE";
    }
}


function estadoRandom(telefono){
var ladas_edomex = ['42', '58', '59', '71', '72', '74', '76'];
    var top_5_estados =['GUANAJUATO', 'JALISCO', 'VERACRUZ', 'PUEBLA', 'HIDALGO'];
    
    // TRAIGO EL VALOR DEL ESTADO
    var prefijo_telefono = telefono.substring(0, 2);
    if(prefijo_telefono==='55'){
      return 'DISTRITO FEDERAL';
    }else if(jQuery.inArray( prefijo_telefono, ladas_edomex )!==-1){
      return 'ESTADO DE MEXICO';
    }else{
      return top_5_estados[Math.floor(Math.random()*top_5_estados.length)];
      // estado='ZACATECAS';
    }
}
function esNumero(e){
   tecla = (document.all) ? e.keyCode : e.which;
    if(tecla > 31 && (tecla < 48 || tecla > 57 )){
    //console.log("true");
        return  false;
    }
}
function getParametersDynamics(idDynamics){
  var arrayDynamics=[];
  var carrera="";
  var nivelInteres="";
  var subnivelinteres="";
  
    if(idDynamics){

       // var qry=queryHandler(jsonCarreras,'/*/[/IdDynamics == "'+idDynamics+'"]');
        //var interes = qry[0].value.campus;
        var qry=queryHandler(jsonDynamics,'/*/[/IdDynamics == "'+idDynamics+'"]');
         jQuery.each(qry,function(key, value){

          carrera=value.value.carrera;
          nivelInteres=value.value.nivelInteres;
          subnivelinteres=value.value.subnivelinteres;

          //var modalidad=query.value.modalidad;
          });

         return  arrayDynamics=[{'carrera':carrera,'nivelInteres':nivelInteres,'subnivelInteres':subnivelinteres}];
    }else{

         return "No hay Id Dynamics";
    }

}

function setCookie(cname, cvalue, exdays) {
    var d = new Date();
    d.setTime(d.getTime() + (exdays*24*60*60*1000));
    var expires = "expires="+d.toUTCString();
    document.cookie = cname + "=" + cvalue + "; " + expires;
}

function getCookie(cname) {
    var name = cname + "=";
    var ca = document.cookie.split(';');
    for(var i=0; i<ca.length; i++) {
        var c = ca[i];
        while (c.charAt(0)==' ') c = c.substring(1);
        if (c.indexOf(name) == 0) return c.substring(name.length, c.length);
    }
    return "";
}

function checkCookie(valor) {
    var user = getCookie(valor);
    if (user != "") {
       return true;
    } else {
       return false;
    }

}
function quitaAcentos(str){ 
for (var i=0;i<str.length;i++){ 
//Sustituye "á é í ó ú" 
if (str.charAt(i)=="á") str = str.replace(/á/,"a"); 
if (str.charAt(i)=="é") str = str.replace(/é/,"e"); 
if (str.charAt(i)=="í") str = str.replace(/í/,"i"); 
if (str.charAt(i)=="ó") str = str.replace(/ó/,"o"); 
if (str.charAt(i)=="ú") str = str.replace(/ú/,"u"); 
} 
return str; 
} 
function envioAspirantes(){
  
    var idDynamics      = jQuery('#calcApp').data('selectMateria');
    var qry=queryHandler(jsonDynamics,'/* /[/IdDynamics == "'+idDynamics+'"]');
    var Carrera;
    var L_Negocio;
    var Pobla;
    var carreraInteres;
    //console.log(qry);

          
    var dynamics_val=getParametersDynamics(idDynamics);
    jQuery.each(dynamics_val, function(key, value){
        Carrera=value['carrera'];
        L_Negocio=value['nivelInteres'];
        carreraInteres=value['subnivelInteres'];   
        Pobla=carreraInteres;  
    });





    var aspirantes='ASPIRANTES LIC';
    var nivelOferta          ='L';
     
    if(idDynamics==666){
      aspirantes="ASPIRANTES PREPA";
      nivelOferta="P"
    }
    if(Pobla==4 || Pobla==6){aspirantes="ONLINEP";}

var Banner="CALCULADORA";

if(getCookie('utm_campaign')){jQuery("#frm_banner").val(getCookie('utm_campaign'));}
if(getParameterByName('utm_campaign') != null){Banner = getParameterByName('utm_campaign');}else{Banner = "CALCULADORA";}

  var Herramienta          =aspirantes;
  var Campana              =1;
  var Origen               =aspirantes;
  
  var Calidad              =3;
  var F_Nacim              ='01/01/2000';
  if(jQuery('#calcApp').data('selectLinea')=='POSGRADO'){var Ciclo='17-1';}else{var Ciclo='16-3';}

  if(jQuery('#calcApp').data('campus')=='GDL'){var Ciclo='17-1';} 
  if(idDynamics==35){ Ciclo='17-1';}
  if(idDynamics==258){Ciclo='16-3';}
 if(Pobla==4 || Pobla==6){Ciclo="16-2";}
  var Alumno               =0;
  var Estad                =estadoRandom(jQuery('#frm_celular').val());
  var C_Carrera            =idDynamics;
  var banner_id            =jQuery('#frm_banner').val();
  var CP                   =(jQuery('#frm_informacion').val())?jQuery('#frm_informacion').val() : 10040;

  var name_form            ='';
  var Sexo                 ='M';
  var nom_carrera          ="PREPARATORIA";
  var nom_campus           =quitaAcentos(campusDynamics(jQuery('#calcApp').data('campus')));
  var promedioF            =jQuery('#calcApp').data('promedio');
  var Pdescuento           =jQuery('#calcApp').data('beca');
  var nivelF               ='L';
  var CostoUnico           =0;
  var num_carrera          =0;
  var costoMensual         =0;
  var descuentoF           =0;
  var planSelec            =jQuery('#calcApp').data('planSelec');
  var CostoSelec           =jQuery('#calcApp').data('CostoSelec');
  var planRecom            =jQuery('#calcApp').data('planRecom');
  var CostoRecom           =jQuery('#calcApp').data('CostoRecom');
  var envioB               =1;
  var C_Carrera_WP         ="";
  var Direcc               ='WEBCALL';
  var Colon                ='';
  var Nombre2              ='';
  var email2               ='';
  var URLreferer           =encodeURIComponent(window.location.href);
  var debugs               ='1';
  var gclid_field          ='';
  var userId_field         ='';
  var CID                  =jQuery('#CID').val();;
  var costoMateria         =jQuery('#calcApp').data('costoPorMateria');
  var email_unitec         ='';
  var otraConsulta         =0;
  var SERVER_URL           ='';
  var frm_LadaLenght       ='';
  var frm_LadaAllow        ='';
  var alum_2_2             ='';
  var Nombre               =quitaAcentos(jQuery('#frm_nombre').val());
  var Ap_Pat               =quitaAcentos(jQuery('#frm_apaterno').val());
  var Ap_Mat               =quitaAcentos(jQuery('#frm_amaterno').val());
  var email                =formatEmail(jQuery('#frm_correo').val());
  var informacion_2        =1;
  var Estado               =estadoRandom(jQuery('#frm_celular').val());
  var masculino            ='M';
  var TipoTel              ='cel_calc';
  var Telef                =jQuery('#frm_celular').val();
  var periodo_interes_sel  ='16-1';
  var politica             ='on';
  var promedio             =jQuery('#calcApp').data('promedio');
  var programa             =idDynamics;
  var materias             =jQuery('#calcApp').data('materias');
  var fdata="nivelOferta="+nivelOferta+"&Herramienta="+Herramienta+"&Campana="+Campana+"&Origen="+Origen+"&Banner="+Banner+"&Calidad="+Calidad+"&L_Negocio="+L_Negocio+"&F_Nacim="+F_Nacim+"&Ciclo="+Ciclo+"&Alumno="+Alumno+"&Estad="+Estad+"&C_Carrera="+C_Carrera+"&Carrera="+Carrera+"&banner_id="+banner_id+"&CP="+CP+"&Pobla="+Pobla+"&name_form="+name_form+"&Sexo="+Sexo+"&nom_carrera="+nom_carrera+"&nom_campus="+nom_campus+"&promedioF="+promedioF+"&Pdescuento="+Pdescuento+"&nivelF="+nivelF+"&CostoUnico="+CostoUnico+"&num_carrera="+num_carrera+"&costoMensual="+costoMensual+"&descuentoF="+descuentoF+"&planSelec="+planSelec+"&CostoSelec="+CostoSelec+"&planRecom="+planRecom+"&CostoRecom="+CostoRecom+"&envioB="+envioB+"&C_Carrera_WP="+C_Carrera_WP+"&Direcc="+Direcc+"&Colon="+Colon+"&Nombre2="+Nombre2+"&email2="+email2+"&URLreferer="+URLreferer+"&debugs="+debugs+"&gclid_field="+gclid_field+"&userId_field="+userId_field+"&CID="+CID+"&costoMateria="+costoMateria+"&email_unitec="+email_unitec+"&otraConsulta="+otraConsulta+"&SERVER_URL="+SERVER_URL+"&frm_LadaLenght="+frm_LadaLenght+"&frm_LadaAllow="+frm_LadaAllow+"&alum_2_2="+alum_2_2+"&Nombre="+Nombre+"&Ap_Pat="+Ap_Pat+"&Ap_Mat="+Ap_Mat+"&email="+email+"&informacion_2="+informacion_2+"&Estado="+Estado+"&masculino="+masculino+"&TipoTel="+TipoTel+"&Telef="+Telef+"&periodo_interes_sel="+periodo_interes_sel+"&politica="+politica+"&promedio="+promedio+"&programa="+programa+"&materias="+materias;
  
  var checkURL = jQuery("#otraConsulta").val();
  var urlCalc = (jQuery("#calcApp").data('regcompleto')==0) ? "//www.unitec.mx/wp-content/themes/temaunitec/calculadora/v2_calc/almacen1.php" : "//www.unitec.mx/wp-content/themes/temaunitec/calculadora/v2_calc/otra_consulta.php";
  
  jQuery.ajax({
  url:urlCalc,
  data:fdata,
  cache:false,
  type:"POST",
  success :function(msg){
    //console.log("Ejecutado"+msg);   
    //Cuando retorne el valor debe considerarse como otra consulta ya que seguira enviando sus datos.
    jQuery("#otraConsulta").val(1);
    jQuery("#calcApp").data('regcompleto',1);
    setCookie('frmunitec',"calculadora",1);
  }
}); 

}