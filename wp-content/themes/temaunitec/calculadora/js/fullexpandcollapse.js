// JavaScript Document
$(document).ready(function(){
	//alto del screen del dispositivo
	var alto_screen = screen.height;
	es_mobile();
	// HTML markup implementation, overlap mode
	$( '#menu' ).multilevelpushmenu({
		direction: 'rtl',
		fullCollapse: true,
		collapsed: true,
		menuWidth: '90%',
        menuHeight: alto_screen,

		// Just for fun also changing the look of the menu
		wrapperClass: 'mlpm_w',
		menuInactiveClass: 'mlpm_inactive',
		onItemClick: function() {
		// Anchor href
        var itemHref = $item.find( 'a:first' ).attr( 'href' );
        // Redirecting the page
        location.href = itemHref;
		}
	});

	// Base expand
	$( '.baseexpand' ).click(function(){
		$( '#menu' ).multilevelpushmenu( 'expand' );
	});


});

 function es_mobile(){
        if( /Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent) ) {
            //console.log("Estoy en mobile");
            //si es mobile se elimina a partir del tercer nivel del menu.
            $("#menu > nav > ul > li > ul > li >ul ").remove();
            return true;
         }
    }
