var jsonBecas=[
  {
    "PREPA": {
      "TODOS": {
        "r1": {
          "promedio": [
            "7",
            "7.9"
          ],
          "beca": "25"
        },
        "r2": {
          "promedio": [
            "8",
            "8.5"
          ],
          "beca": "30"
        },
        "r3": {
          "promedio": [
            "8.6",
            "9"
          ],
          "beca": "35"
        },
        "r4": {
          "promedio": [
            "9.1",
            "9.5"
          ],
          "beca": "40"
        },
        "r5": {
          "promedio": [
            "9.6",
            "10"
          ],
          "beca": "45"
        }
      },
      "LEO": {
        "r1": {
          "promedio": [
            "7",
            "7.9"
          ],
          "beca": "5"
        },
        "r2": {
          "promedio": [
            "8",
            "8.5"
          ],
          "beca": "10"
        },
        "r3": {
          "promedio": [
            "8.6",
            "9"
          ],
          "beca": "15"
        },
        "r4": {
          "promedio": [
            "9.1",
            "9.5"
          ],
          "beca": "20"
        },
        "r5": {
          "promedio": [
            "9.6",
            "10"
          ],
          "beca": "25"
        }
      }
    }
  },
  {
    "LIC": {
      "TODOS": {
        "r1": {
          "promedio": [
            "7",
            "7.9"
          ],
          "beca": "15"
        },
        "r2": {
          "promedio": [
            "8",
            "8.5"
          ],
          "beca": "20"
        },
        "r3": {
          "promedio": [
            "8.6",
            "9"
          ],
          "beca": "25"
        },
        "r4": {
          "promedio": [
            "9.1",
            "9.5"
          ],
          "beca": "30"
        },
        "r5": {
          "promedio": [
            "9.6",
            "10"
          ],
          "beca": "35"
        }
      },
      "LEO": {
        "r1": {
          "promedio": [
            "7",
            "7.9"
          ],
          "beca": "5"
        },
        "r2": {
          "promedio": [
            "8",
            "8.5"
          ],
          "beca": "10"
        },
        "r3": {
          "promedio": [
            "8.6",
            "9"
          ],
          "beca": "15"
        },
        "r4": {
          "promedio": [
            "9.1",
            "9.5"
          ],
          "beca": "20"
        },
        "r5": {
          "promedio": [
            "9.6",
            "10"
          ],
          "beca": "25"
        }
      },"TOL": {
        "r1": {
          "promedio": [ 
            "7",
            "7.9"
          ],
          "beca": "25"
        },
        "r2": {
          "promedio": [
            "8",
            "9"
          ],
          "beca": "30"
        },
        "r3": {
          "promedio": [
            "9.1",
            "10"
          ],
          "beca": "35"
        }
      }
    }
  },
  {
    "ENFER": {
      "TODOS": {
        "r1": {
          "promedio": [
            "7",
            "7.9"
          ],
          "beca": "15"
        },
        "r2": {
          "promedio": [
            "8",
            "8.5"
          ],
          "beca": "20"
        },
        "r3": {
          "promedio": [
            "8.6",
            "9"
          ],
          "beca": "25"
        },
        "r4": {
          "promedio": [
            "9.1",
            "9.5"
          ],
          "beca": "30"
        },
        "r5": {
          "promedio": [
            "9.6",
            "10"
          ],
          "beca": "35"
        }
      },
      "LEO": {
        "r1": {
          "promedio": [
            "7",
            "7.9"
          ],
          "beca": "5"
        },
        "r2": {
          "promedio": [
            "8",
            "8.5"
          ],
          "beca": "10"
        },
        "r3": {
          "promedio": [
            "8.6",
            "9"
          ],
          "beca": "15"
        },
        "r4": {
          "promedio": [
            "9.1",
            "9.5"
          ],
          "beca": "20"
        },
        "r5": {
          "promedio": [
            "9.6",
            "10"
          ],
          "beca": "25"
        }
      }
    }
  },
  {
    "NUTRI": {
      "TODOS": {
        "r1": {
          "promedio": [
            "7",
            "7.9"
          ],
          "beca": "15"
        },
        "r2": {
          "promedio": [
            "8",
            "8.5"
          ],
          "beca": "20"
        },
        "r3": {
          "promedio": [
            "8.6",
            "9"
          ],
          "beca": "25"
        },
        "r4": {
          "promedio": [
            "9.1",
            "9.5"
          ],
          "beca": "30"
        },
        "r5": {
          "promedio": [
            "9.6",
            "10"
          ],
          "beca": "35"
        }
      },
      "LEO": {
        "r1": {
          "promedio": [
            "7",
            "7.9"
          ],
          "beca": "5"
        },
        "r2": {
          "promedio": [
            "8",
            "8.5"
          ],
          "beca": "10"
        },
        "r3": {
          "promedio": [
            "8.6",
            "9"
          ],
          "beca": "15"
        },
        "r4": {
          "promedio": [
            "9.1",
            "9.5"
          ],
          "beca": "20"
        },
        "r5": {
          "promedio": [
            "9.6",
            "10"
          ],
          "beca": "25"
        }
      },
      "TOL": {
        "r1": {
          "promedio": [
            "7",
            "7.9"
          ],
          "beca": "25" 
        },
        "r2": {
          "promedio": [
            "8",
            "9"
          ],
          "beca": "30"
        },
        "r3": {
          "promedio": [
            "9.1",
            "10"
          ],
          "beca": "35"
        }
      }
    }
  },
  {
    "FISIO": {
      "TODOS": {
        "r1": {
          "promedio": [
            "7",
            "7.9"
          ],
          "beca": "25"
        },
        "r2": {
          "promedio": [
            "8",
            "8.5"
          ],
          "beca": "30"
        },
        "r3": {
          "promedio": [
            "8.6",
            "9"
          ],
          "beca": "35"
        },
        "r4": {
          "promedio": [
            "9.1",
            "9.5"
          ],
          "beca": "40"
        },
        "r5": {
          "promedio": [
            "9.6",
            "10"
          ],
          "beca": "45"
        }
      },
      "QRO": {
        "r1": {
          "promedio": [
            "7",
            "7.9"
          ],
          "beca": "15"
        },
        "r2": {
          "promedio": [
            "8",
            "8.5"
          ],
          "beca": "20"
        },
        "r3": {
          "promedio": [
            "8.6",
            "9"
          ],
          "beca": "25"
        },
        "r4": {
          "promedio": [
            "9.1",
            "9.5"
          ],
          "beca": "30"
        },
        "r5": {
          "promedio": [
            "9.6",
            "10"
          ],
          "beca": "35"
        }
      },
      "LEO": {
        "r1": {
          "promedio": [
            "7",
            "7.9"
          ],
          "beca": "15"
        },
        "r2": {
          "promedio": [
            "8",
            "8.5"
          ],
          "beca": "20"
        }, 
        "r3": {
          "promedio": [
            "8.6",
            "9" 
          ],
          "beca": "25"
        },
        "r4": {
          "promedio": [
            "9.1",
            "9.5"
          ],
          "beca": "30"
        },
        "r5": {
          "promedio": [
            "9.6",
            "10"
          ],
          "beca": "35"
        }
      }
    }
  },
  {
    "ING": {
      "TODOS": {
        "r1": {
          "promedio": [
            "7",
            "7.9"
          ],
          "beca": "15"
        },
        "r2": {
          "promedio": [
            "8",
            "8.5"
          ],
          "beca": "20"
        },
        "r3": {
          "promedio": [
            "8.6",
            "9"
          ],
          "beca": "25"
        },
        "r4": {
          "promedio": [
            "9.1",
            "9.5"
          ],
          "beca": "30"
        },
        "r5": {
          "promedio": [
            "9.6",
            "10"
          ],
          "beca": "35"
        }
      },
      "LEO": {
        "r1": {
          "promedio": [
            "7",
            "7.9"
          ],
          "beca": "5"
        },
        "r2": {
          "promedio": [
            "8",
            "8.5"
          ],
          "beca": "10"
        },
        "r3": {
          "promedio": [
            "8.6",
            "9"
          ],
          "beca": "15"
        },
        "r4": {
          "promedio": [
            "9.1",
            "9.5"
          ],
          "beca": "20"
        },
        "r5": {
          "promedio": [
            "9.6",
            "10"
          ],
          "beca": "25"
        }
      }
    }
  },
  {
    "TURI": {
      "TODOS": {
        "r1": {
          "promedio": [
            "7",
            "7.9"
          ],
          "beca": "15"
        },
        "r2": {
          "promedio": [
            "8",
            "8.5"
          ],
          "beca": "20"
        },
        "r3": {
          "promedio": [
            "8.6",
            "9"
          ],
          "beca": "25"
        },
        "r4": {
          "promedio": [
            "9.1",
            "9.5"
          ],
          "beca": "30"
        },
        "r5": {
          "promedio": [
            "9.6",
            "10"
          ],
          "beca": "35"
        }
      },
      "LEO": {
        "r1": {
          "promedio": [
            "7",
            "7.9"
          ],
          "beca": "5"
        },
        "r2": {
          "promedio": [
            "8",
            "8.5"
          ],
          "beca": "10"
        },
        "r3": {
          "promedio": [
            "8.6",
            "9"
          ],
          "beca": "15"
        },
        "r4": {
          "promedio": [
            "9.1",
            "9.5"
          ],
          "beca": "20"
        },
        "r5": {
          "promedio": [
            "9.6",
            "10"
          ],
          "beca": "25"
        }
      }
    }
  },
  {
    "DENT": {
      "TODOS": {
        "r1": {
          "promedio": [
            "7",
            "7.9"
          ],
          "beca": "20"
        },
        "r2": {
          "promedio": [
            "8",
            "8.5"
          ],
          "beca": "25"
        },
        "r3": {
          "promedio": [
            "8.6",
            "9"
          ],
          "beca": "30"
        },
        "r4": {
          "promedio": [
            "9.1",
            "9.5"
          ],
          "beca": "35"
        },
        "r5": {
          "promedio": [
            "9.6",
            "10"
          ],
          "beca": "40"
        }
      }
    }
  },
  {
    "POS": {
      "TODOS": {
        "r1": {
          "promedio": [
            "7",
            "7.9"
          ],
          "beca": "20"
        },
        "r2": {
          "promedio": [
            "8",
            "8.9"
          ],
          "beca": "30"
        },
        "r3": {
          "promedio": [
            "9",
            "10"
          ],
          "beca": "40"
        }
      },
      "LEO": {
        "r1": {
          "promedio": [
            "7",
            "7.9"
          ],
          "beca": "10"
        },
        "r2": {
          "promedio": [
            "8",
            "8.9"
          ],
          "beca": "20"
        },
        "r3": {
          "promedio": [
            "9",
            "10"
          ],
          "beca": "30"
        }
      }
    }
  },
  {
    "DIP": {
      "TODOS": {
        "r1": {
          "promedio": [
            "7",
            "7.9"
          ],
          "beca": "20"
        },
        "r2": {
          "promedio": [
            "8",
            "8.9"
          ],
          "beca": "30"
        },
        "r3": {
          "promedio": [
            "9",
            "10"
          ],
          "beca": "40"
        }
      },
      "LEO": {
        "r1": {
          "promedio": [
            "7",
            "7.9"
          ],
          "beca": "20"
        },
        "r2": {
          "promedio": [
            "8",
            "8.9"
          ],
          "beca": "30"
        },
        "r3": {
          "promedio": [
            "9",
            "10"
          ],
          "beca": "40"
        }
      }
    }
  },
  {
    "DIPON": {
      "TODOS": {
        "r1": {
          "promedio": [
            "7",
            "7.9"
          ],
          "beca": "20"
        },
        "r2": {
          "promedio": [
            "8",
            "8.9"
          ],
          "beca": "30"
        },
        "r3": {
          "promedio": [
            "9",
            "10"
          ],
          "beca": "40"
        }
      }
    }
  },
  {
    "POSON": {
      "TODOS": {
        "r1": {
          "promedio": [
            "7",
            "7.9"
          ],
          "beca": "20"
        },
        "r2": {
          "promedio": [
            "8",
            "8.9"
          ],
          "beca": "30"
        },
        "r3": {
          "promedio": [
            "9",
            "10"
          ],
          "beca": "40"
        }
      }
    }
  },
  {
    "POSONCUAT": {
      "TODOS": {
        "r1": {
          "promedio": [
            "7",
            "7.9"
          ],
          "beca": "20"
        },
        "r2": {
          "promedio": [
            "8",
            "8.9"
          ],
          "beca": "30"
        },
        "r3": {
          "promedio": [
            "9",
            "10"
          ],
          "beca": "40"
        }
      },"ONL": {
        "r1": {
          "promedio": [
            "7", 
            "7.9"
          ],
          "beca": "20"
        },
        "r2": {
          "promedio": [
            "8",
            "8.9"
          ],
          "beca": "30"
        },
        "r3": {
          "promedio": [ 
            "9",
            "10"
          ],
          "beca": "40"
        }
      }
    }
  },
  {
    "ESP": {
      "TODOS": {
        "r1": {
          "promedio": [
            "7",
            "7.9"
          ],
          "beca": "20"
        },
        "r2": {
          "promedio": [
            "8",
            "8.9"
          ],
          "beca": "30"
        },
        "r3": {
          "promedio": [
            "9",
            "10"
          ],
          "beca": "40"
        }
      }
    }
  },
  {
    "BLUEM": {
      "TODOS": {
        "r1": {
          "promedio": [
            "7",
            "7.9"
          ],
          "beca": "30"
        },
        "r2": {
          "promedio": [
            "8.0",
            "8.5"
          ],
          "beca": "35"
        },
        "r3": {
          "promedio": [
            "8.6",
            "9"
          ],
          "beca": "40"
        },
        "r4": {
          "promedio": [
            "9.1",
            "9.5"
          ],
          "beca": "45"
        },
        "r5": {
          "promedio": [
            "9.6",
            "10"
          ],
          "beca": "50"
        }
      },"LEO": {
        "r1": {
          "promedio": [
            "7",
            "7.9"
          ],
          "beca": "20"
        },
        "r2": {
          "promedio": [
            "8.0",
            "8.5"
          ],
          "beca": "25"
        },
        "r3": {
          "promedio": [
            "8.6",
            "9"
          ],
          "beca": "30"
        },
        "r4": {
          "promedio": [
            "9.1",
            "9.5"
          ],
          "beca": "35"
        },
        "r5": {
          "promedio": [
            "9.6",
            "10"
          ],
          "beca": "40"
        }
      }
    }
  },
  {
    "GASTRO": {
      "TODOS": {
        "r1": {
          "promedio": [
            "7",
            "7.9"
          ],
          "beca": "30"
        },
        "r2": {
          "promedio": [
            "8",
            "8.5"
          ],
          "beca": "35"
        },
        "r3": {
          "promedio": [
            "8.6",
            "9"
          ],
          "beca": "40"
        },
        "r4": {
          "promedio": [
            "9.1",
            "9.5"
          ],
          "beca": "45"
        },
        "r5": {
          "promedio": [
            "9.6",
            "10"
          ],
          "beca": "50"
        }
      },"LEO": {
        "r1": {
          "promedio": [
            "7",
            "7.9"
          ],
          "beca": "20"
        },
        "r2": {
          "promedio": [
            "8.0",
            "8.5"
          ],
          "beca": "25"
        },
        "r3": {
          "promedio": [
            "8.6",
            "9"
          ],
          "beca": "30"
        },
        "r4": {
          "promedio": [
            "9.1",
            "9.5"
          ],
          "beca": "35"
        },
        "r5": {
          "promedio": [
            "9.6",
            "10"
          ],
          "beca": "40"
        }
      }
    }
  },
  {
    "LICEJEC": {
      "TODOS": {
        "r1": {
          "promedio": [
            "7",
            "7.5"
          ],
          "beca": "5"
        },
        "r2": {
          "promedio": [
            "7.6",
            "8.5"
          ],
          "beca": "10"
        },
        "r3": {
          "promedio": [
            "8.6",
            "9"
          ],
          "beca": "15"
        },
        "r4": {
          "promedio": [
            "9.1",
            "9.5"
          ],
          "beca": "20"
        },
        "r5": {
          "promedio": [
            "9.6",
            "10"
          ],
          "beca": "25"
        }
      },
      "LEO": {
        "r1": {
          "promedio": [
            "7",
            "7.5"
          ],
          "beca": "5"
        },
        "r2": {
          "promedio": [
            "7.6",
            "8.5"
          ],
          "beca": "10"
        },
        "r3": {
          "promedio": [
            "8.6",
            "9"
          ],
          "beca": "15"
        },
        "r4": {
          "promedio": [
            "9.1",
            "9.5"
          ],
          "beca": "20"
        },
        "r5": {
          "promedio": [
            "9.6",
            "10"
          ],
          "beca": "25"
        }
      }
    }
  },
  {
    "INGEJEC": {
      "TODOS": {
        "r1": {
          "promedio": [
            "7",
            "7.5"
          ],
          "beca": "5"
        },
        "r2": {
          "promedio": [
            "7.6",
            "8.5"
          ],
          "beca": "10"
        },
        "r3": {
          "promedio": [
            "8.6",
            "9"
          ],
          "beca": "15"
        },
        "r4": {
          "promedio": [
            "9.1",
            "9.5"
          ],
          "beca": "20"
        },
        "r5": {
          "promedio": [
            "9.6",
            "10"
          ],
          "beca": "25"
        }
      },
      "LEO": {
        "r1": {
          "promedio": [
            "7",
            "7.5"
          ],
          "beca": "5"
        },
        "r2": {
          "promedio": [
            "7.6",
            "8.5"
          ],
          "beca": "10"
        },
        "r3": {
          "promedio": [
            "8.6",
            "9"
          ],
          "beca": "15"
        },
        "r4": {
          "promedio": [
            "9.1",
            "9.5"
          ],
          "beca": "20"
        },
        "r5": {
          "promedio": [
            "9.6",
            "10"
          ],
          "beca": "25"
        }
      }
    }
  },
  {
    "LICON": {
      "TODOS": {
        "r1": {
          "promedio": [
            "6",
            "6.9"
          ],
          "beca": "15"
        },
        "r2": {
          "promedio": [
            "7",
            "7.5"
          ],
          "beca": "20"
        },
        "r3": {
          "promedio": [
            "7.6",
            "8.5"
          ],
          "beca": "25"
        },
        "r4": {
          "promedio": [
            "8.6",
            "9"
          ],
          "beca": "30"
        },
        "r5": {
          "promedio": [
            "9.1",
            "9.5"
          ],
          "beca": "35"
        },
        "r6": {
          "promedio": [
            "9.6",
            "10"
          ],
          "beca": "40"
        }
      }
    }
  },
  {
    "INGON": {
      "TODOS": {
        "r1": {
          "promedio": [
            "6",
            "6.9"
          ],
          "beca": "15"
        },
        "r2": {
          "promedio": [
            "7",
            "7.5"
          ],
          "beca": "20"
        },
        "r3": {
          "promedio": [
            "7.6",
            "8.5"
          ],
          "beca": "25"
        },
        "r4": {
          "promedio": [
            "8.6",
            "9"
          ],
          "beca": "30"
        },
        "r5": {
          "promedio": [
            "9.1",
            "9.5"
          ],
          "beca": "35"
        },
        "r6": {
          "promedio": [
            "9.6",
            "10"
          ],
          "beca": "40"
        }
      }
    }
  },
  {
    "POSCUAT": {
      "TODOS": {
        "r1": {
          "promedio": [
            "7",
            "7.9"
          ],
          "beca": "20"
        },
        "r2": {
          "promedio": [
            "8",
            "8.9"
          ],
          "beca": "30"
        },
        "r3": {
          "promedio": [
            "9",
            "10"
          ],
          "beca": "40"
        }
      },
      "LEO": {
        "r1": {
          "promedio": [
            "7",
            "7.9"
          ],
          "beca": "20"
        },
        "r2": {
          "promedio": [
            "8",
            "8.9"
          ],
          "beca": "30"
        },
        "r3": {
          "promedio": [
            "9",
            "10"
          ],
          "beca": "40"
        }
      }
    }
  },  {
    "POSCUATS": {
      "TODOS": {
        "r1": {
          "promedio": [
            "7",
            "7.9"
          ],
          "beca": "20"
        },
        "r2": {
          "promedio": [
            "8",
            "8.9"
          ],
          "beca": "30"
        },
        "r3": {
          "promedio": [
            "9",
            "10"
          ],
          "beca": "40"
        }
      },
      "LEO": {
        "r1": {
          "promedio": [
            "7",
            "7.9"
          ],
          "beca": "10"
        },
        "r2": {
          "promedio": [
            "8",
            "8.9"
          ],
          "beca": "20"
        },
        "r3": {
          "promedio": [
            "9",
            "10"
          ],
          "beca": "30"
        }
      }
    }
  },
  {
    "LICPEDAG": {
      "TODOS": {
        "r1": {
          "promedio": [
            "7",
            "7.9"
          ],
          "beca": "30"
        },
        "r2": {
          "promedio": [
            "8",
            "8.5"
          ],
          "beca": "35"
        },
        "r3": {
          "promedio": [
            "8.6",
            "9"
          ],
          "beca": "40"
        },
        "r4": {
          "promedio": [
            "9.1",
            "9.5"
          ],
          "beca": "45"
        },
        "r5": {
          "promedio": [
            "9.6",
            "10"
          ],
          "beca": "50"
        }
      },
      "LEO": {
        "r1": {
          "promedio": [
            "7",
            "7.9"
          ],
          "beca": "30"
        },
        "r2": {
          "promedio": [
            "8",
            "8.5"
          ],
          "beca": "35"
        },
        "r3": {
          "promedio": [
            "8.6",
            "9"
          ],
          "beca": "40"
        },
        "r4": {
          "promedio": [
            "9.1",
            "9.5"
          ],
          "beca": "45"
        },
        "r5": {
          "promedio": [
            "9.6",
            "10"
          ],
          "beca": "50"
        }
      }
    }
  },
  {
    "LICPSICOLOG": {
      "TODOS": {
        "r1": {
          "promedio": [
            "7",
            "7.9"
          ],
          "beca": "15"
        },
        "r2": {
          "promedio": [
            "8",
            "8.5"
          ],
          "beca": "20"
        },
        "r3": {
          "promedio": [
            "8.6",
            "9"
          ],
          "beca": "25"
        },
        "r4": {
          "promedio": [
            "9.1",
            "9.5"
          ],
          "beca": "30"
        },
        "r5": {
          "promedio": [
            "9.6",
            "10"
          ],
          "beca": "35"
        }
      },
      "LEO": {
        "r1": {
          "promedio": [
            "7",
            "7.9"
          ],
          "beca": "5"
        },
        "r2": {
          "promedio": [
            "8",
            "8.5"
          ],
          "beca": "10"
        },
        "r3": {
          "promedio": [
            "8.6",
            "9"
          ],
          "beca": "15"
        },
        "r4": {
          "promedio": [
            "9.1",
            "9.5"
          ],
          "beca": "20"
        },
        "r5": {
          "promedio": [
            "9.6",
            "10"
          ],
          "beca": "25"
        }
      },"TOL": {
        "r1": {
          "promedio": [ 
            "7",
            "7.9"
          ],
          "beca": "25"
        },
        "r2": {
          "promedio": [
            "8",
            "9"
          ],
          "beca": "30"
        },
        "r3": {
          "promedio": [
            "9.1",
            "10"
          ],
          "beca": "35"
        }
      }
    }
  }
];