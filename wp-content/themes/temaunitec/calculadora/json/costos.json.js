var jsonCostos =[
  { 
    "PREPA": {
      "title": "Preparatoria",
      "duracion": {
        "2": {
          "Materias": "7"
        },
        "3": {
          "Materias": "5"
        }
      },
      "costoMateria": {
        "4pagos": {
          "ATZ": "790",
          "CUI": "790",
          "ECA": "790",
          "GDL": "690",
          "LEO": "490",
          "MAR": "790",
          "SUR": "815",
          "TOL": "775",
          "QRO": "735",
          "REY": "790"
        },
        "1pago": {
          "ATZ": "3000",
          "CUI": "3000",
          "ECA": "3000",
          "GDL": "2625",
          "LEO": "1865",
          "MAR": "3000",
          "SUR": "3095",
          "TOL": "2900",
          "QRO": "2810",
          "REY": "3000"
        }
      }
    }
  },
  {
    "ING": {
      "title": "Ingenieria",
      "duracion": {
        "3": {
          "Materias": "6"
        },
        "4": {
          "Materias": "5"
        }
      },
      "costoMateria": {
        "4pagos": {
          "ATZ": "1000",
          "CUI": "990",
          "ECA": "990",
          "GDL": "880",
          "LEO": "850",
          "MAR": "990",
          "SUR": "1080",
          "TOL": "990",
          "QRO": "990",
          "REY": "925"
        },
        "1pago": {
          "ATZ": "3835",
          "CUI": "3765",
          "ECA": "3765",
          "GDL": "3345",
          "LEO": "3230",
          "MAR": "3765",
          "SUR": "4105",
          "TOL": "3765",
          "QRO": "3785",
          "REY": "3645"
        }
      }
    }
  },
  {
    "LIC": {
      "title": "Licenciatura",
      "duracion": {
        "3": {
          "Materias": "6"
        },
        "4": {
          "Materias": "5"
        }
      },
      "costoMateria": {
        "4pagos": {
          "ATZ": "990",
          "CUI": "970",
          "ECA": "960",
          "GDL": "830",
          "LEO": "790",
          "MAR": "970",
          "SUR": "990",
          "TOL": "970",
          "QRO": "950",
          "REY": "925"
        },
        "1pago": {
          "ATZ": "3765",
          "CUI": "3690",
          "ECA": "3650",
          "GDL": "3155",
          "LEO": "3005",
          "MAR": "3690",
          "SUR": "3765",
          "TOL": "3690",
          "QRO": "3630",
          "REY": "3645"
        }
      }
    }
  },
  {
    "TURI": {
      "title": "EmpreTuristicas",
      "duracion": {
        "3": {
          "Materias": "7"
        },
        "4": {
          "Materias": "6"
        }
      },
      "costoMateria": {
        "4pagos": {
          "LEO": "760"
        },
        "1pago": {
          "LEO": "2875"
        }
      }
    }
  },
  {
    "ENFER": {
      "title": "Enfermeria",
      "duracion": {
        "4": {
          "Materias": "6"
        }
      },
      "costoMateria": {
        "4pagos": {
          "ATZ": "725",
          "CUI": "725",
          "ECA": "725",
          "GDL": "655",
          "LEO": "640",
          "MAR": "725",
          "SUR": "725",
          "TOL": "725",
          "QRO": "665",
          "REY": "725"
        },
        "1pago": {
          "ATZ": "2755",
          "CUI": "2755",
          "ECA": "2755",
          "GDL": "2490",
          "LEO": "2435",
          "MAR": "2755",
          "SUR": "2755",
          "TOL": "2755",
          "QRO": "2545",
          "REY": "2755"
        }
      }
    }
  },
  {
    "NUTRI": {
      "title": "Nutricion",
      "duracion": {
        "3": {
          "Materias": "6"
        },
        "4": {
          "Materias": "5"
        }
      },
      "costoMateria": {
        "4pagos": {
          "ATZ": "1000",
          "CUI": "990",
          "ECA": "990",
          "GDL": "880",
          "LEO": "850",
          "MAR": "990",
          "SUR": "1080",
          "TOL": "990",
          "QRO": "990",
          "REY": "990"
        },
        "1pago": {
          "ATZ": "3835",
          "CUI": "3765",
          "ECA": "3765",
          "GDL": "3345",
          "LEO": "3230",
          "MAR": "3765",
          "SUR": "4105",
          "TOL": "3765",
          "QRO": "3785",
          "REY": "3765"
        }
      }
    }
  },
  {
    "BLUEM": {
      "title": "Bluemontains",
      "duracion": {
        "3": {
          "Materias": "6"
        }
      },
      "costoMateria": {
        "4pagos": {
          "ATZ": "1190",
          "TOL": "1190",
          "CUI": "1190",
          "ECA": "1190",
          "MAR": "1190",
          "QRO": "1200",
          "SUR": "1190",
          "LEO": "860",
          "TOL": "1190",
          "GDL": "1000",
          "REY": "1190"
        },
        "1pago": {
          "ATZ": "4525",
          "TOL": "4525",
          "CUI": "4525",
          "ECA": "4525",
          "MAR": "4525",
          "QRO": "4575",
          "SUR": "4525",
          "LEO": "3270",
          "TOL": "4525",
          "GDL": "3800",
          "REY": "4525"
        }
      }
    }
  },
  {
    "GASTRO": {
      "title": "GASTRONOMIA",
      "duracion": {
        "4": {
          "Materias": "5"
        }
      },
      "costoMateria": {
        "4pagos": {
          "ATZ": "1190",
          "ECA": "1190",
          "QRO": "1200",
          "TOL": "1190",
          "GDL": "1000",
          "LEO": "860",
          "CUI": "1190",
          "REY": "1190"
        },
        "1pago": {
          "ATZ": "4525",
          "ECA": "4525",
          "QRO": "4575",
          "TOL": "4525",
          "GDL": "3800",
          "LEO": "3270",
          "CUI": "4525",
          "REY": "4525"
        }
      }
    }
  },
  {
    "FISIO": {
      "title": "Fisioterapia",
      "duracion": {
        "4": {
          "Materias": "5"
        }
      },
      "costoMateria": {
        "4pagos": {
          "ATZ": "1070",
          "CUI": "990",
          "ECA": "990",
          "GDL": "880",
          "LEO": "795",
          "MAR": "990",
          "SUR": "1090",
          "TOL": "990",
          "QRO": "990",
          "REY": "990"
        },
        "1pago": {
          "ATZ": "4070",
          "CUI": "3765",
          "ECA": "3765",
          "GDL": "3345",
          "LEO": "3020",
          "MAR": "3765",
          "SUR": "4145",
          "TOL": "3765",
          "QRO": "3785",
          "REY": "3765"
        }
      }
    }
  },
  {
    "LICEJEC": {
      "title": "Licejecutiva",
      "duracion": {
        "3": {
          "Materias": "5"
        },
        "4": {
          "Materias": "4"
        }
      },
      "costoMateria": {
        "4pagos": {
          "ATZ": "550",
          "CUI": "550",
          "ECA": "550",
          "GDL": "550",
          "LEO": "550",
          "MAR": "550",
          "SUR": "550",
          "TOL": "550",
          "QRO": "550"
        },
        "1pago": {
          "ATZ": "1875",
          "CUI": "1875",
          "ECA": "1875",
          "GDL": "1875",
          "LEO": "1875",
          "MAR": "1875",
          "SUR": "1875",
          "TOL": "1875",
          "QRO": "1875"
        }
      }
    }
  },
  {
    "INGEJEC": {
      "title": "Ingejecutiva",
      "duracion": {
        "3": {
          "Materias": "5"
        },
        "4": {
          "Materias": "4"
        }
      },
      "costoMateria": {
        "4pagos": {
          "ATZ": "555",
          "CUI": "555",
          "ECA": "555",
          "LEO": "555",
          "MAR": "555",
          "SUR": "555",
          "GDL": "555",
          "TOL": "555",
          "QRO": "555"
        },
        "1pago": {
          "ATZ": "1890",
          "CUI": "1890",
          "ECA": "1890",
          "LEO": "1890",
          "GDL": "1890",
          "MAR": "1890",
          "SUR": "1890",
          "TOL": "1890",
          "QRO": "1890"
        }
      }
    }
  },
  {
    "DENT": {
      "title": "Cdentista",
      "duracion": {
        "4": {
          "Materias": "7"
        }
      },
      "costoMateria": {
        "6pagos": {
          "MAR": "1440"
        },
        "1pago": {
          "MAR": "10080"
        }
      }
    }
  },
  {
    "POS": {
      "title": "Maestrias",
      "duracion": {
        "2": {
          "Materias": "4"
        }
      },
      "costoMateria": {
        "10pagos": {
          "ATZ": "1200",
          "CUI": "1200",
          "ECA": "1200",
          "MAR": "1200",
          "SUR": "1200",
          "TOL": "795"
        },
        "6pagos": {
          "ATZ": "1850",
          "CUI": "1850",
          "ECA": "1850",
          "LEO": "895",
          "MAR": "1850",
          "SUR": "1850",
          "TOL": "1290"
        },
        "1pago": {
          "ATZ": "9915",
          "CUI": "9915",
          "ECA": "9915",
          "LEO": "4785",
          "MAR": "9915",
          "SUR": "9915",
          "TOL": "6935"
        }
      }
    }
  },
  {
    "POSCUAT": {
      "title": "Maestrias cuatrimestrales",
      "duracion": {
        "4m": {
          "Materias": "3"
        }
      },
      "costoMateria": {
        "8pagos": {
          "ATZ": "7560",
          "CUI": "7560",
          "ECA": "7560",
          "MAR": "7560",
          "SUR": "7560",
          "TOL": "7560"
        },
        "4pagos": {
          "ATZ": "1890",
          "CUI": "1890",
          "ECA": "1890",
          "LEO": "1425",
          "MAR": "1890",
          "SUR": "1890",
          "TOL": "1890"
        },
        "1pago": {
          "ATZ": "7185",
          "CUI": "7185",
          "ECA": "7185",
          "LEO": "5615",
          "MAR": "7185",
          "SUR": "7185",
          "TOL": "7185"
        }
      }
    }
  },
  {
    "POSONCUAT": {
      "title": "Maestrias cuatrimestrales online",
      "duracion": {
        "2": {
          "Materias": "3"
        }
      },
      "costoMateria": {
        "8pagos": {
          "ONL": "6080",

        },
        "4pagos": {
          "ONL": "1520"
        },
        "1pago": {
          "ONL": "5780"
        }
      }
    }
  },
  {
    "ESP": {
      "title": "Especialidades",
      "duracion": {
        "2": {
          "Materias": "4"
        }
      },
      "costoMateria": {
        "4pagos": {
          "MAR": "19500"
        },
        "1pago": {
          "MAR": "111150"
        }
      }
    }
  },
  {
    "DIP": {
      "title": "Diplomados",
      "duracion": {
        "4m": {
          "Materias": "3"
        }
      },
      "costoMateria": {
        "8pagos": {
          "ATZ": "7560",
          "CUI": "7560",
          "ECA": "7560",
          "LEO": "7560",
          "MAR": "7560",
          "SUR": "7560"
        },
        "4pagos": {
          "ATZ": "1890",
          "CUI": "1890",
          "ECA": "1890",
          "MAR": "1890",
          "SUR": "1890",
          "LEO": "1425"
        },
        "1pago": {
          "ATZ": "7185",
          "CUI": "7185",
          "ECA": "7185",
          "MAR": "7185",
          "SUR": "7185",
          "LEO": "5615"
        }
      }
    }
  },
  {
    "DIPON": {
      "title": "Diplomados en linea",
      "duracion": {
        "4m": {
          "Materias": "3"
        }
      },
      "costoMateria": {
        "10pagos": {
          "ONL": "6080"
        },
        "4pagos": {
          "ONL": "1520"
        },
        "1pago": {
          "ONL": "5780"
        }
      }
    }
  },
  {
    "POSON": {
      "title": "Maestriaenlinea",
      "duracion": {
        "2": {
          "Materias": "4"
        }
      },
      "costoMateria": {
        "8pagos": {
          "ONL": "6080"
        },
        "4pagos": {
          "ONL": "1520"
        },
        "1pago": {
          "ONL": "5780"
        }
      }
    }
  },
  {
    "LICON": {
      "title": "Licenlinea",
      "duracion": {
        "3": {
          "Materias": "5"
        },
        "4": {
          "Materias": "4"
        }
      },
      "costoMateria": {
        "4pagos": {
          "ONL": "565"
        },
        "1pago": {
          "ONL": "2150"
        }
      }
    }
  },
  {
    "INGON": {
      "title": "INGenlinea",
      "duracion": {
        "3": {
          "Materias": "5"
        },
        "4": {
          "Materias": "4"
        }
      },
      "costoMateria": {
        "4pagos": {
          "ONL": "595"
        },
        "1pago": {
          "ONL": "2265"
        }
      }
    }
  },
  {
    "POSCUATS": {
      "title": "Pos cuatrimestral",
      "duracion": {
        "2": {
          "Materias": "3"
        }
      },
      "costoMateria": {
        "4pagos": {
          "ATZ": "1890",
          "SUR": "1890",
          "ECA": "1890",
          "TOL": "1890",
          "MAR": "1890"
        },
        "1pago": {
          "ATZ": "7185",
          "SUR": "7185",
          "ECA": "7185",
          "TOL": "7185",
          "MAR": "7185"
        }
      }
    }
  },
  {
    "LICPEDAG": {
      "title": "LICPEDAGOGIA",
      "duracion": {
        "3": {
          "Materias": "6"
        },
        "4": {
          "Materias": "5"
        }
      },
      "costoMateria": {
        "4pagos": {
          "ATZ": "990",
          "CUI": "970",
          "ECA": "960",
          "GDL": "830",
          "LEO": "790",
          "MAR": "970",
          "SUR": "990",
          "TOL": "970",
          "QRO": "950",
          "REY": "740"
        },
        "1pago": {
          "ATZ": "3765",
          "CUI": "3690",
          "ECA": "3650",
          "GDL": "3155",
          "LEO": "3005",
          "MAR": "3690",
          "SUR": "3765",
          "TOL": "3690",
          "QRO": "3630",
          "REY": "2815"
        }
      }
    }
  },
  {
    "LICPSICOLOG": {
      "title": "LICPSICOLOG",
      "duracion": {
        "3": {
          "Materias": "6"
        },
        "4": {
          "Materias": "5"
        }
      },
      "costoMateria": {
        "4pagos": {
          "ATZ": "990",
          "CUI": "970",
          "ECA": "960",
          "GDL": "830",
          "LEO": "790",
          "MAR": "970",
          "SUR": "990",
          "TOL": "970",
          "QRO": "950",
          "REY": "740"
        },
        "1pago": {
          "ATZ": "3765",
          "CUI": "3690",
          "ECA": "3650",
          "GDL": "3155",
          "LEO": "3005",
          "MAR": "3690",
          "SUR": "3765",
          "TOL": "3690",
          "QRO": "3630",
          "REY": "2815"
        }
      }
    }
  }
]; 