var jsonDynamics =[ 
{"IdDynamics":"722","carrera":"LIC. INTERNACIONAL EN ADMON. DE CENTROS DE CONVENCION Y EVENTOS","nivelInteres":"U","subnivelinteres":"2"},
{"IdDynamics":"359","carrera":"MAESTRIA EN DIRECCION DE LA COMUNICACION (TRADICIONAL)","nivelInteres":"G","subnivelinteres":"5"},
{"IdDynamics":"1637","carrera":"MAESTRIA EN DIRECCION DE LA COMUNICACION","nivelInteres":"G","subnivelinteres":"6"}, 
{"IdDynamics":"704","carrera":"LIC. EN ADMINISTRACION DE EMPRESAS","nivelInteres":"U","subnivelinteres":"2"},
{"IdDynamics":"3954","carrera":"LIC. EN ADMON DE EMP. HIBRIDO","nivelInteres":"U","subnivelinteres":"3"},
{"IdDynamics":"1104","carrera":"LIC. EN ADMINISTRACION DE EMPRESAS (ONLINE)","nivelInteres":"U","subnivelinteres":"4"},
{"IdDynamics":"130","carrera":"MAESTRIA EN ADMINISTRACION DE NEGOCIOS (TRADICIONAL)","nivelInteres":"G","subnivelinteres":"5"},
{"IdDynamics":"705","carrera":"LICENCIATURA EN ADMINISTRACION DE EMPRESAS TURISTICAS","nivelInteres":"U","subnivelinteres":"2"},
{"IdDynamics":"723","carrera":"LIC. INTERNACIONAL EN ADMON. DE HOTELES Y COMPLEJOS TURISTICOS","nivelInteres":"U","subnivelinteres":"2"},
{"IdDynamics":"1305","carrera":"LICENCIAURA INTERACIONAL EN TURISMO Y REUNIONES","nivelInteres":"U","subnivelinteres":"2"},
{"IdDynamics":"717","carrera":"LICENCIATURA EN ADMINISTRACION INDUSTRIAL","nivelInteres":"U","subnivelinteres":"2"},
{"IdDynamics":"1630","carrera":"MAESTRIA SEMESTRAL EN ADMINISTRACION DE NEGOCIOS ONLINE","nivelInteres":"G","subnivelinteres":"6"},
{"IdDynamics":"2001","carrera":"MTRIA EN ADMON DE NEGOCIOS D.CH (DIPL. EN CAPITAL HUMANO)","nivelInteres":"G","subnivelinteres":"5"},
{"IdDynamics":"2071","carrera":"MTRIA EN ADMON DE NEGOCIOS D.CH ON - LINE (DIPL. EN CAPITAL HUMANO)","nivelInteres":"G","subnivelinteres":"6"},
{"IdDynamics":"701","carrera":"ADM. DE TECNOLOGIAS DE LA INF.","nivelInteres":"U","subnivelinteres":"2"},
{ "IdDynamics":"3971","carrera":"LIC.ADMON DE TEC DE INF HIBRIDO","nivelInteres":"U","subnivelinteres":"3"},
{"IdDynamics":"1101","carrera":"LIC.EN ADMON.DE TECNOLOGIAS DE LA INF. ON LINE","nivelInteres":"U","subnivelinteres":"4"},
{"IdDynamics":"718","carrera":"LICENCIATURA EN INGENIERIA AMBIENTAL Y SUSTENTABILIDAD","nivelInteres":"U","subnivelinteres":"2"},
{"IdDynamics":"682","carrera":"ARQUITECTURA","nivelInteres":"U","subnivelinteres":"2"},
{"IdDynamics":"2010","carrera":"MTRIA EN ADMON DE NEGOCIOS D.ES (DIPL. EN EMPRESAS SUSTENTABLES)","nivelInteres":"G","subnivelinteres":"5"},
{"IdDynamics":"2080","carrera":"MTRIA EN ADMON DE NEGOCIOS D.ES ON-LINE (DIPL. EN EMPRESAS SUSTENTABLES)","nivelInteres":"G","subnivelinteres":"6"},
{"IdDynamics":"510","carrera":"CIENCIAS DE LA COMUNICACION","nivelInteres":"U","subnivelinteres":"2"},
{"IdDynamics":"35","carrera":"CIRUJANO DENTISTA","nivelInteres":"U","subnivelinteres":"1"},
{"IdDynamics":"883","carrera":"INGENIERIA CIVIL","nivelInteres":"U","subnivelinteres":"2"},
{"IdDynamics":"890","carrera":"INGENIERIA GESTION DE NEGOCIOS","nivelInteres":"U","subnivelinteres":"2"},
{"IdDynamics":"728","carrera":"LIC. EN COMUNICACION Y ADMINISTRACION DE EMPRESAS DE ENTRETENIMIENTO","nivelInteres":"U","subnivelinteres":"2"},
{"IdDynamics":"603","carrera":"CONTADURIA PUBLICA","nivelInteres":"U","subnivelinteres":"2"},
{ "IdDynamics":"3953","carrera":"LIC. EN CONT PUBLICA HIBRIDO","nivelInteres":"U","subnivelinteres":"3"},
{"IdDynamics":"1103","carrera":"LIC. EN CONTADURIA PUBLICA ON LINE","nivelInteres":"U","subnivelinteres":"4"},
{"IdDynamics":"739","carrera":"CONTADURIA PUBLICA CON ENFASIS EN DERECHO FISCAL","nivelInteres":"U","subnivelinteres":"2"},
{"IdDynamics":"733","carrera":"LIC.EN CONTADURIA PUBLICA Y FINANZAS","nivelInteres":"U","subnivelinteres":"2"},
{"IdDynamics":"609","carrera":"DERECHO","nivelInteres":"U","subnivelinteres":"2"},
{ "IdDynamics":"3959","carrera":"LIC. EN DERECHO HIBRIDO","nivelInteres":"U","subnivelinteres":"3"},
{"IdDynamics":"1109","carrera":"LIC. EN DERECHO ON LINE","nivelInteres":"U","subnivelinteres":"4"},
{"IdDynamics":"131","carrera":"MAESTRIA EN DERECHO (TRADICIONAL)","nivelInteres":"G","subnivelinteres":"5"},
{"IdDynamics":"1631","carrera":"MAESTRIA EN DERECHO (ON LINE NORMAL)","nivelInteres":"G","subnivelinteres":"6"},
{"IdDynamics":"2005","carrera":"MTRIA EN DERECHO D.DC (DIPL. EN DERECHO CONPORATIVO)","nivelInteres":"G","subnivelinteres":"5"},
{"IdDynamics":"2075","carrera":"MTRIA EN DERECHO D.DC ON-LINE (DIPL. EN DERECHO CONPORATIVO)","nivelInteres":"G","subnivelinteres":"6"},
{"IdDynamics":"2006","carrera":"MTRIA EN DERECHO D.DF (DIPL. EN DERECHO FISCAL)","nivelInteres":"G","subnivelinteres":"5"},
{"IdDynamics":"2076","carrera":"MTRIA EN DERECHO D.DF ON-LINE (DIPL. EN DERECHO FISCAL)","nivelInteres":"G","subnivelinteres":"6"},
{"IdDynamics":"2004","carrera":"MTRIA EN DERECHO D.DP (DIPL. EN DERECHO PENAL)","nivelInteres":"G","subnivelinteres":"5"},
{"IdDynamics":"2074","carrera":"MTRIA EN DERECHO D.DP ON-LINE (DIPL. EN DERECHO PENAL)","nivelInteres":"G","subnivelinteres":"6"},
{"IdDynamics":"132","carrera":"MAESTRIA EN DIRECCION DE PROYECTOS (TRADICIONAL)","nivelInteres":"G","subnivelinteres":"5"},
{"IdDynamics":"1632","carrera":"MAESTRIA EN DIRECCION DE PROYECTOS (ON LINE MODULAR)","nivelInteres":"G","subnivelinteres":"6"},
{"IdDynamics":"681","carrera":"DISEÑO GRAFICO","nivelInteres":"U","subnivelinteres":"2"},
{"IdDynamics":"897","carrera":"LIC. EN DISEÑO INDUSTRIAL","nivelInteres":"U","subnivelinteres":"2"},
{"IdDynamics":"358","carrera":"MAESTRIA EN DISEÑO MULTIMEDIA (TRADICIONAL)","nivelInteres":"G","subnivelinteres":"5"},
{"IdDynamics":"1638","carrera":"MAESTRÍA EN DISEÑO MULTIMEDIA","nivelInteres":"G","subnivelinteres":"6"},
{"IdDynamics":"902","carrera":"LIC. EN DISEÑO ANIMACION Y ARTE DIGITAL","nivelInteres":"U","subnivelinteres":"2"},
{"IdDynamics":"608","carrera":"ECONOMIA","nivelInteres":"U","subnivelinteres":"2"},
{"IdDynamics":"133","carrera":"MAESTRIA EN EDUCACION (TRADICIONAL)","nivelInteres":"G","subnivelinteres":"5"},
{"IdDynamics":"1633","carrera":"MAESTRIA EN EDUCACION (ON-LINE)","nivelInteres":"G","subnivelinteres":"6"},
{"IdDynamics":"2011","carrera":"MTRIA EN ADMON DE NEGOCIOS D.ET (DIPL. EN EMPRESAS TURISTICAS)","nivelInteres":"G","subnivelinteres":"5"},
{"IdDynamics":"2081","carrera":"MTRIA EN ADMON DE NEGOCIOS D.ET ON-LINE (DIPL. EN EMPRESAS TURISTICAS)","nivelInteres":"G","subnivelinteres":"6"},
{"IdDynamics":"2002","carrera":"MTRIA EN ADMON DE NEGOCIOS D.FZ (DIPL. EN FINANZAS)","nivelInteres":"G","subnivelinteres":"5"},
{"IdDynamics":"195","carrera":"ESPECIALIDAD EN ENDODONCIA","nivelInteres":"G","subnivelinteres":"5"},
{"IdDynamics":"1150","carrera":"ENFERMERÍA","nivelInteres":"U","subnivelinteres":"1"},
{"IdDynamics":"2082","carrera":"MTRIA EN EDUCACION D.EE ON-LINE (DIPL. EN EVALUACION EDUCATIVA)","nivelInteres":"G","subnivelinteres":"6"},
{"IdDynamics":"2072","carrera":"MTRIA EN ADMON DE NEGOCIOS D.FZ ON-LINE (DIPL. EN FINANZAS)","nivelInteres":"G","subnivelinteres":"6"},
{"IdDynamics":"2003","carrera":"MTRIA EN ADMON DE NEGOCIOS D.MK (DIPL. EN MARKETING)","nivelInteres":"G","subnivelinteres":"5"},
{"IdDynamics":"607","carrera":"FINANZAS","nivelInteres":"U","subnivelinteres":"2"},
{"IdDynamics":"1060","carrera":"LICENCIATURA EN FISIOTERAPIA","nivelInteres":"U","subnivelinteres":"1"},
{"IdDynamics":"886","carrera":"INGENIERIA INDUSTRIAL Y DE SISTEMAS","nivelInteres":"U","subnivelinteres":"2"},
{"IdDynamics":"363","carrera":"MAESTRIA EN JUICIOS ORALES (TRADICIONAL)","nivelInteres":"G","subnivelinteres":"5"},
{"IdDynamics":"1636","carrera":"MAESTRIA EN JUICIOS ORALES","nivelInteres":"G","subnivelinteres":"6"},
{"IdDynamics":"2073","carrera":"MTRIA EN ADMON DE NEGOCIOS D.MK ON-LINE (DIPL. EN MARKETING)","nivelInteres":"G","subnivelinteres":"6"},
{"IdDynamics":"884","carrera":"INGENIERIA MECANICA","nivelInteres":"U","subnivelinteres":"2"},
{"IdDynamics":"588","carrera":"MECATRONICA","nivelInteres":"U","subnivelinteres":"2"},
{"IdDynamics":"1213","carrera":"LIC. EN NEGOCIOS INTERNACIONALES ON LINE (INGL)","nivelInteres":"U","subnivelinteres":"4"},
{ "IdDynamics":"3973","carrera":"LIC. NEGOCIOS INT. HIBRIDO","nivelInteres":"U","subnivelinteres":"3"},
{"IdDynamics":"706","carrera":"MERCADOTECNIA","nivelInteres":"U","subnivelinteres":"2"},
{ "IdDynamics":"3956","carrera":"LIC. EN MERCADOTECNIA HIBRIDO","nivelInteres":"U","subnivelinteres":"3"},
{"IdDynamics":"1106","carrera":"LIC. EN MERCADOTECNIA ON LINE","nivelInteres":"U","subnivelinteres":"4"},
{"IdDynamics":"361","carrera":"MAESTRIA EN MIGRACION DE SISTEMAS (TRADICIONAL)","nivelInteres":"G","subnivelinteres":"5"},
{"IdDynamics":"716","carrera":"LICENCIATURA EN ADMINISTRACION DE NEGOCIOS","nivelInteres":"U","subnivelinteres":"2"},
{"IdDynamics":"676","carrera":"LICENCIATURA EN ADMINISTRACION DE NEGOCIOS (CORPORATIVA)","nivelInteres":"U","subnivelinteres":"3"},
{"IdDynamics":"713","carrera":"NEGOCIOS INTERNACIONALES","nivelInteres":"U","subnivelinteres":"2"},
{"IdDynamics":"1040","carrera":"NUTRICIÓN","nivelInteres":"U","subnivelinteres":"1"},
{"IdDynamics":"193","carrera":"ESPECIALIDAD EN ODONTOLOGIA PEDIATRICA","nivelInteres":"G","subnivelinteres":"5"},
{"IdDynamics":"197","carrera":"ESPECIALIDAD EN ODONTOLOGIA RESTAURADORA","nivelInteres":"G","subnivelinteres":"5"},
{"IdDynamics":"192","carrera":"ESPECIALIDAD EN ORTODONCIA","nivelInteres":"G","subnivelinteres":"5"},
{"IdDynamics":"612","carrera":"PEDAGOGÍA","nivelInteres":"U","subnivelinteres":"2"},
{ "IdDynamics":"3972","carrera":"LIC. EN PEDAGOGIA HIBRIDO","nivelInteres":"U","subnivelinteres":"3"},
{"IdDynamics":"1112","carrera":"LICENCIATURA EN PEDAGOGIA ONLINE","nivelInteres":"U","subnivelinteres":"4"},
{"IdDynamics":"196","carrera":"ESPECIALIDAD EN PERIODONCIA","nivelInteres":"G","subnivelinteres":"5"},
{"IdDynamics":"666","carrera":"PREPARATORIA","nivelInteres":"P","subnivelinteres":"7"},
{"IdDynamics":"611","carrera":"PSICOLOGIA","nivelInteres":"U","subnivelinteres":"2"},
{ "IdDynamics":"3961","carrera":"LIC. EN PSICOLOGIA HIBRIDO","nivelInteres":"U","subnivelinteres":"3"},
{"IdDynamics":"761","carrera":"PSICOLOGIA CORPORATIVA","nivelInteres":"U","subnivelinteres":"3"},
{"IdDynamics":"1111","carrera":"LICENCIATURA EN PSICOLOGIA ONLINE","nivelInteres":"U","subnivelinteres":"4"},
{"IdDynamics":"134","carrera":"MAESTRIA EN PSICOLOGIA (PRESENCIAL)","nivelInteres":"G","subnivelinteres":"5"},
{"IdDynamics":"1634","carrera":"MAESTRIA EN PSICOLOGIA (ON LINE)","nivelInteres":"G","subnivelinteres":"6"},
{"IdDynamics":"2014","carrera":"MTRIA EN PSICOLOGIA D.PE (DIPL. EN PSIC. EDUCATIVA)","nivelInteres":"G","subnivelinteres":"5"},
{"IdDynamics":"2084","carrera":"MTRIA EN PSICOLOGIA D.PE ON-LINE (DIPL. EN PSIC. EDUCATIVA)","nivelInteres":"G","subnivelinteres":"6"},
{"IdDynamics":"2015","carrera":"MTRIA EN PSICOLOGIA D.PO (DIPL.- EN PSIC ORGANIZACIONAL)","nivelInteres":"G","subnivelinteres":"5"},
{"IdDynamics":"2085","carrera":"MTRIA EN PSICOLOGIA D.PO ON-LINE (DIPL.- EN PSIC ORGANIZACIONAL)","nivelInteres":"G","subnivelinteres":"6"},
{"IdDynamics":"715","carrera":"LICENCIATURA EN PUBLICIDAD Y MEDIOS","nivelInteres":"U","subnivelinteres":"2"},
{"IdDynamics":"727","carrera":"LIC. EN PUBLICIDAD Y MERCADOTECNIA DIGITAL","nivelInteres":"U","subnivelinteres":"2"},
{"IdDynamics":"787","carrera":"INGENIERIA QUIMICA","nivelInteres":"U","subnivelinteres":"2"},
{"IdDynamics":"135","carrera":"MAESTRIA EN SEGURIDAD DE TECNOLOGIAS DE LA INFORMACION (SEM TRADICIONAL)","nivelInteres":"G","subnivelinteres":"5"},
{"IdDynamics":"889","carrera":"INGENIERIA SISTEMAS COMPUTACIONALES","nivelInteres":"U","subnivelinteres":"2"},
{"IdDynamics":"892","carrera":"LIC. EN ING. SISTEMAS DIGITALES Y ROBOTICA","nivelInteres":"U","subnivelinteres":"2"},
{"IdDynamics":"2013","carrera":"MTRIA EN EDUCACION D.TE (DIPL. EN TECNOLOGIA EDUCATIVA)","nivelInteres":"G","subnivelinteres":"5"},
{"IdDynamics":"2083","carrera":"MTRIA EN EDUCACION D.TE ON-LINE (DIPL. EN TECNOLOGIA EDUCATIVA)","nivelInteres":"G","subnivelinteres":"6"},
{"IdDynamics":"891","carrera":"LIC. EN ING. EN TEC. DE INF. Y COMUNICACIONES","nivelInteres":"U","subnivelinteres":"2"},
{"IdDynamics":"885","carrera":"INGENIERIA ELECTRONICA Y DE COMUNICACIONES","nivelInteres":"U","subnivelinteres":"2"},
{"IdDynamics":"729","carrera":"LIC. EN COMERCIO INTERNACIONAL","nivelInteres":"U","subnivelinteres":"2"},
{ "IdDynamics":"3929","carrera":"LIC.EN COM.INTERNACIONAL HIB.","nivelInteres":"U","subnivelinteres":"3"},
{"IdDynamics":"1129","carrera":"LIC.EN COM.INTERNAC.ON LINE","nivelInteres":"U","subnivelinteres":"4"},
{"IdDynamics":"1205","carrera":"LIC. EN ADMINISTRACIÓN DE EMPRESAS TURISTICAS ON LINE (INGL)","nivelInteres":"U","subnivelinteres":"4"},
{"IdDynamics":"1120","carrera":"LIC. ADMON EMP C/ENF EN OP COM (ONL)","nivelInteres":"U","subnivelinteres":"4"},
{"IdDynamics":"1221","carrera":"LICENCIATURA EN GASTRONOMÍA","nivelInteres":"U","subnivelinteres":"2"},
{"IdDynamics":"1146","carrera":"LIC. EN INGENIERÍA INDUSTRIAL Y ADMINISTRACIÓN ON LINE","nivelInteres":"U","subnivelinteres":"4"},
{"IdDynamics":"1149","carrera":"LIC. EN INGENIERÍA EN SOFTWARE Y REDES ON LINE","nivelInteres":"U","subnivelinteres":"4"},
{"IdDynamics":"1548","carrera":"MAESTRIA EN ANALITICA E INTELIGENCIA DE MERCADOS ONLINE","nivelInteres":"G","subnivelinteres":"6"},
{ "IdDynamics":"3914","carrera":"LICENCIATURA EN RELACIONES INTERNACIONALES","nivelInteres":"U","subnivelinteres":"3"},
{ "IdDynamics": "1314", "carrera": "LICENCIATURA EN RELACIONES INTERNACIONALES", "nivelInteres": "U", "subnivelinteres": "2" },
{"IdDynamics":"1326","carrera":"LICENCIATURA EN MERCADOTECNIA DIGITAL Y PUBLICIDAD","nivelInteres":"U","subnivelinteres":"2"},
{ "IdDynamics":"3976","carrera":"DISEÑO DIGITAL Y ANIMACIÓN","nivelInteres":"U","subnivelinteres":"3"},
{"IdDynamics":"3875","carrera":"INGENIERÍA EN SOFTWARE Y REDES","nivelInteres":"U","subnivelinteres":"3"},
{ "IdDynamics":"3974","carrera":"INGENIERÍA INDUSTRIAL Y ADMINISTRACIÓN","nivelInteres":"U","subnivelinteres":"3"},
{"IdDynamics":"1144","carrera":"LICENCIATURA EN RELACIONES INTERNACIONALES","nivelInteres":"U","subnivelinteres":"4"}, 
{"IdDynamics":"3932","carrera":"LICENCIATURA EN COMUNICACIÓN Y MEDIOS","nivelInteres":"U","subnivelinteres":"4"},
{"IdDynamics":"1176","carrera":"DISEÑO DIGITAL Y ANIMACIÓN","nivelInteres":"U","subnivelinteres":"4"},
{"IdDynamics":"268","carrera":"MAESTRIA EN DIRECCIÓN DE ORGANIZACIONES DE SALUD","nivelInteres":"G","subnivelinteres":"5"},
{"IdDynamics":"1668","carrera":"MAESTRIA EN DIRECCIÓN DE ORGANIZACIONES DE SALUD","nivelInteres":"G","subnivelinteres":"6"},
{"IdDynamics":"1346","carrera":"LICENCIATURA EN EMPRENDIMIENTO Y DESARROLLO DE NEGOCIOS","nivelInteres":"U","subnivelinteres":"2"},
{"IdDynamics":"139","carrera":"MAESTRIA EN DESARROLLO EMPRESARIAL SOSTENIBLE","nivelInteres":"G","subnivelinteres":"6"}
];