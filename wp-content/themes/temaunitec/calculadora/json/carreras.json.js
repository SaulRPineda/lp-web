var jsonCarreras = [
    { 
        "IdDynamics": "704",
        "nombre": "Administración Empresas",
        "campus": [
            "ATZ",
            "MAR",
            "ECA",
            "GDL",
            "LEO",
            "SUR",
            "CUI",
            "TOL",
            "QRO",
            "REY"
        ],
        "interes": "2",
        "modalidad": "1",
        "Grupo_carreras": "3",
        "catCosto": "LIC",
        "lineaWeb": "LICENCIATURA"
    },
    {
        "IdDynamics": "3954",
        "nombre": "Administración Empresas",
        "campus": [
            "ATZ", 
            "MAR",
            "ECA",
            "GDL",
            "LEO",
            "SUR",
            "CUI",
            "TOL", 
            "QRO"
        ],
        "interes": "3",
        "modalidad": "2",
        "Grupo_carreras": "3",
        "catCosto": "LICEJEC",
        "lineaWeb": "LICENCIATURA"
    },
    {
        "IdDynamics": "1104",
        "nombre": "Administración Empresas",
        "campus": [
            "ONL"
        ],
        "interes": "4",
        "modalidad": "3",
        "Grupo_carreras": "3",
        "catCosto": "LICON",
        "lineaWeb": "LICENCIATURA"
    },
    {
        "IdDynamics": "1305",
        "nombre": "Internacional en Turismo y Reuniones",
        "campus": [
            "ATZ",
            "MAR",
            "ECA",
            "SUR",
            "CUI",
            "QRO",
            "TOL",
            "LEO",
            "GDL",
            "REY"
        ],
        "interes": "2",
        "modalidad": "1",
        "Grupo_carreras": "1",
        "catCosto": "BLUEM",
        "lineaWeb": "LICENCIATURA"
    },
    {
        "IdDynamics": "701",
        "nombre": "Administración Tecnologías la Información",
        "campus": [
            "ATZ",
            "MAR",
            "ECA",
            "LEO",
            "SUR",
            "CUI",
            "TOL"
        ],
        "interes": "2",
        "modalidad": "1",
        "Grupo_carreras": "10",
        "catCosto": "LIC",
        "lineaWeb": "LICENCIATURA"
    },
    {
        "IdDynamics": "1101",
        "nombre": "Administración Tecnologías la Información",
        "campus": [
            "ONL"
        ],
        "interes": "4",
        "modalidad": "3",
        "Grupo_carreras": "10",
        "catCosto": "LICON",
        "lineaWeb": "LICENCIATURA"
    },
    {
        "IdDynamics": "3971",
        "nombre": "Administración Tecnologías la Información",
        "campus": [
            "ATZ",
            "MAR",
            "ECA",
            "SUR",
            "CUI",
            "LEO",
            "GDL",
            "QRO"
        ],
        "interes": "3",
        "modalidad": "2",
        "Grupo_carreras": "10",
        "catCosto": "LICEJEC",
        "lineaWeb": "LICENCIATURA"
    },
    {
        "IdDynamics": "718",
        "nombre": "Ambiental y Sustentabilidad",
        "campus": [
            "ATZ",
            "MAR",
            "ECA",
            "SUR",
            "CUI"
        ],
        "interes": "5",
        "modalidad": "1",
        "Grupo_carreras": "11",
        "catCosto": "ING",
        "lineaWeb": "INGENIERIA"
    },
    {
        "IdDynamics": "682",
        "nombre": "Arquitectura",
        "campus": [
            "ATZ",
            "MAR",
            "ECA",
            "GDL",
            "LEO",
            "SUR",
            "CUI",
            "TOL",
            "QRO",
            "REY"
        ],
        "interes": "2",
        "modalidad": "1",
        "Grupo_carreras": "12",
        "catCosto": "ING",
        "lineaWeb": "LICENCIATURA"
    },
    {
        "IdDynamics": "510",
        "nombre": "Ciencias de la Comunicación",
        "campus": [
            "ATZ",
            "MAR",
            "ECA",
            "SUR",
            "CUI",
            "TOL"
        ],
        "interes": "2",
        "modalidad": "1",
        "Grupo_carreras": "14",
        "catCosto": "LIC",
        "lineaWeb": "LICENCIATURA"
    },
    {
        "IdDynamics": "35",
        "nombre": "Cirujano Dentista",
        "campus": [
            "MAR"
        ],
        "interes": "2",
        "modalidad": "1",
        "Grupo_carreras": "15",
        "catCosto": "DENT",
        "lineaWeb": "SALUD"
    },
    {
        "IdDynamics": "883",
        "nombre": "Civil",
        "campus": [
            "ATZ",
            "MAR",
            "ECA",
            "SUR",
            "CUI",
            "QRO",
            "TOL",
            "LEO",
            "GDL"
        ],
        "interes": "5",
        "modalidad": "1",
        "Grupo_carreras": "16",
        "catCosto": "ING",
        "lineaWeb": "INGENIERIA"
    },
    {
        "IdDynamics": "890",
        "nombre": "en Gestión de Negocios",
        "campus": [
            "ATZ",
            "MAR",
            "ECA",
            "SUR",
            "CUI"
        ],
        "interes": "5",
        "modalidad": "1",
        "Grupo_carreras": "17",
        "catCosto": "ING",
        "lineaWeb": "INGENIERIA"
    },
    {
        "IdDynamics": "728",
        "nombre": "en Administración de Empresas de Entretenimiento y Comunicación",
        "campus": [
            "ATZ",
            "MAR",
            "ECA",
            "SUR",
            "CUI"
        ],
        "interes": "2",
        "modalidad": "1",
        "Grupo_carreras": "18",
        "catCosto": "LIC",
        "lineaWeb": "LICENCIATURA"
    },
    {
        "IdDynamics": "603",
        "nombre": "Contaduría Pública",
        "campus": [
            "ATZ",
            "MAR",
            "ECA",
            "GDL",
            "LEO",
            "SUR",
            "CUI",
            "TOL",
            "QRO",
            "REY"
        ],
        "interes": "2",
        "modalidad": "1",
        "Grupo_carreras": "19",
        "catCosto": "LIC",
        "lineaWeb": "LICENCIATURA"
    },
    {
        "IdDynamics": "3953",
        "nombre": "Contaduría Pública",
        "campus": [
            "ATZ",
            "MAR",
            "ECA",
            "GDL",
            "LEO",
            "SUR",
            "CUI",
            "TOL",
            "QRO"
        ],
        "interes": "3",
        "modalidad": "2",
        "Grupo_carreras": "19",
        "catCosto": "LICEJEC",
        "lineaWeb": "LICENCIATURA"
    },
    {
        "IdDynamics": "1103",
        "nombre": "Contaduría Pública",
        "campus": [
            "ONL"
        ],
        "interes": "4",
        "modalidad": "3",
        "Grupo_carreras": "19",
        "catCosto": "LICON",
        "lineaWeb": "LICENCIATURA"
    },
    {
        "IdDynamics": "733",
        "nombre": "Contaduría Pública y Finanzas",
        "campus": [
            "ATZ",
            "MAR",
            "ECA",
            "LEO",
            "SUR",
            "CUI"
        ],
        "interes": "2",
        "modalidad": "1",
        "Grupo_carreras": "21",
        "catCosto": "LIC",
        "lineaWeb": "LICENCIATURA"
    },
    {
        "IdDynamics": "609",
        "nombre": "Derecho",
        "campus": [
            "ATZ",
            "MAR",
            "ECA",
            "GDL",
            "LEO",
            "SUR",
            "CUI",
            "TOL",
            "QRO",
            "REY"
        ],
        "interes": "2",
        "modalidad": "1",
        "Grupo_carreras": "22",
        "catCosto": "LIC",
        "lineaWeb": "LICENCIATURA"
    },
    {
        "IdDynamics": "3959",
        "nombre": "Derecho",
        "campus": [
            "ATZ",
            "MAR",
            "ECA",
            "GDL",
            "LEO",
            "SUR",
            "CUI",
            "TOL",
            "QRO"
        ],
        "interes": "3",
        "modalidad": "2",
        "Grupo_carreras": "22",
        "catCosto": "LICEJEC",
        "lineaWeb": "LICENCIATURA"
    },
    {
        "IdDynamics": "1109",
        "nombre": "Derecho",
        "campus": [
            "ONL"
        ],
        "interes": "4",
        "modalidad": "3",
        "Grupo_carreras": "22",
        "catCosto": "LICON",
        "lineaWeb": "LICENCIATURA"
    },
    {
        "IdDynamics": "2081",
        "nombre": "Diplomado Administración Empresas Turísticas",
        "campus": [
            "ONL"
        ],
        "interes": "10",
        "modalidad": "3",
        "Grupo_carreras": "4",
        "catCosto": "DIPON",
        "lineaWeb": "POSGRADO"
    },
    {
        "IdDynamics": "2001",
        "nombre": "Diplomado Capital Humano",
        "campus": [
            "ATZ",
            "MAR",
            "ECA",
            "SUR",
            "CUI"
        ],
        "interes": "9",
        "modalidad": "1",
        "Grupo_carreras": "13",
        "catCosto": "DIP",
        "lineaWeb": "POSGRADO"
    },
    {
        "IdDynamics": "2071",
        "nombre": "Diplomado Capital Humano",
        "campus": [
            "ONL"
        ],
        "interes": "10",
        "modalidad": "3",
        "Grupo_carreras": "13",
        "catCosto": "DIPON",
        "lineaWeb": "POSGRADO"
    },
    {
        "IdDynamics": "2005",
        "nombre": "Diplomado Derecho Corporativo",
        "campus": [
            "ATZ",
            "MAR",
            "ECA",
            "SUR",
            "CUI"
        ],
        "interes": "9",
        "modalidad": "1",
        "Grupo_carreras": "24",
        "catCosto": "DIP",
        "lineaWeb": "POSGRADO"
    },
    {
        "IdDynamics": "2075",
        "nombre": "Diplomado Derecho Corporativo",
        "campus": [
            "ONL"
        ],
        "interes": "10",
        "modalidad": "3",
        "Grupo_carreras": "24",
        "catCosto": "DIPON",
        "lineaWeb": "POSGRADO"
    },
    {
        "IdDynamics": "2006",
        "nombre": "Diplomado Derecho Fiscal",
        "campus": [
            "ATZ",
            "MAR",
            "ECA",
            "SUR",
            "CUI"
        ],
        "interes": "9",
        "modalidad": "1",
        "Grupo_carreras": "25",
        "catCosto": "DIP",
        "lineaWeb": "POSGRADO"
    },
    {
        "IdDynamics": "2076",
        "nombre": "Diplomado Derecho Fiscal",
        "campus": [
            "ONL"
        ],
        "interes": "10",
        "modalidad": "3",
        "Grupo_carreras": "25",
        "catCosto": "DIPON",
        "lineaWeb": "POSGRADO"
    },
    {
        "IdDynamics": "2004",
        "nombre": "Diplomado Derecho Penal",
        "campus": [
            "ATZ",
            "MAR",
            "ECA",
            "SUR",
            "CUI"
        ],
        "interes": "9",
        "modalidad": "1",
        "Grupo_carreras": "26",
        "catCosto": "DIP",
        "lineaWeb": "POSGRADO"
    },
    {
        "IdDynamics": "2074",
        "nombre": "Diplomado Derecho Penal",
        "campus": [
            "ONL"
        ],
        "interes": "10",
        "modalidad": "3",
        "Grupo_carreras": "26",
        "catCosto": "DIPON",
        "lineaWeb": "POSGRADO"
    },
    {
        "IdDynamics": "2010",
        "nombre": "Diplomado Empresas Sustentables",
        "campus": [
            "ATZ",
            "MAR",
            "ECA",
            "SUR",
            "CUI"
        ],
        "interes": "9",
        "modalidad": "1",
        "Grupo_carreras": "34",
        "catCosto": "DIP",
        "lineaWeb": "POSGRADO"
    },
    {
        "IdDynamics": "2080",
        "nombre": "Diplomado Empresas Sustentables",
        "campus": [
            "ONL"
        ],
        "interes": "10",
        "modalidad": "3",
        "Grupo_carreras": "34",
        "catCosto": "DIPON",
        "lineaWeb": "POSGRADO"
    },
    {
        "IdDynamics": "2011",
        "nombre": "Diplomado Empresas Turísticas",
        "campus": [
            "ATZ",
            "MAR",
            "ECA",
            "SUR",
            "CUI"
        ],
        "interes": "9",
        "modalidad": "1",
        "Grupo_carreras": "35",
        "catCosto": "DIP",
        "lineaWeb": "POSGRADO"
    },
    {
        "IdDynamics": "2082",
        "nombre": "Diplomado Evaluación Educativa",
        "campus": [
            "ONL"
        ],
        "interes": "10",
        "modalidad": "3",
        "Grupo_carreras": "38",
        "catCosto": "DIPON",
        "lineaWeb": "POSGRADO"
    },
    {
        "IdDynamics": "2002",
        "nombre": "Diplomado Finanzas",
        "campus": [
            "ATZ",
            "MAR",
            "ECA",
            "SUR",
            "CUI"
        ],
        "interes": "9",
        "modalidad": "1",
        "Grupo_carreras": "39",
        "catCosto": "DIP",
        "lineaWeb": "POSGRADO"
    },
    {
        "IdDynamics": "2072",
        "nombre": "Diplomado Finanzas",
        "campus": [
            "ONL"
        ],
        "interes": "10",
        "modalidad": "3",
        "Grupo_carreras": "39",
        "catCosto": "DIPON",
        "lineaWeb": "POSGRADO"
    },
    {
        "IdDynamics": "2003",
        "nombre": "Diplomado Mercadotecnia",
        "campus": [
            "ATZ",
            "MAR",
            "ECA",
            "SUR",
            "CUI"
        ],
        "interes": "9",
        "modalidad": "1",
        "Grupo_carreras": "46",
        "catCosto": "DIP",
        "lineaWeb": "POSGRADO"
    },
    {
        "IdDynamics": "2073",
        "nombre": "Diplomado Mercadotecnia",
        "campus": [
            "ONL"
        ],
        "interes": "10",
        "modalidad": "3",
        "Grupo_carreras": "46",
        "catCosto": "DIPON",
        "lineaWeb": "POSGRADO"
    },
    {
        "IdDynamics": "2014",
        "nombre": "Diplomado Psicología Educativa",
        "campus": [
            "ATZ",
            "MAR",
            "ECA",
            "SUR",
            "CUI"
        ],
        "interes": "9",
        "modalidad": "1",
        "Grupo_carreras": "58",
        "catCosto": "DIP",
        "lineaWeb": "POSGRADO"
    },
    {
        "IdDynamics": "2084",
        "nombre": "Diplomado Psicología Educativa",
        "campus": [
            "ONL"
        ],
        "interes": "10",
        "modalidad": "3",
        "Grupo_carreras": "58",
        "catCosto": "DIPON",
        "lineaWeb": "POSGRADO"
    },
    {
        "IdDynamics": "2015",
        "nombre": "Diplomado Psicología Organizacional",
        "campus": [
            "ATZ",
            "MAR",
            "ECA",
            "SUR",
            "CUI"
        ],
        "interes": "9",
        "modalidad": "1",
        "Grupo_carreras": "59",
        "catCosto": "DIP",
        "lineaWeb": "POSGRADO"
    },
    {
        "IdDynamics": "2085",
        "nombre": "Diplomado Psicología Organizacional",
        "campus": [
            "ONL"
        ],
        "interes": "10",
        "modalidad": "3",
        "Grupo_carreras": "59",
        "catCosto": "DIPON",
        "lineaWeb": "POSGRADO"
    },
    {
        "IdDynamics": "2013",
        "nombre": "Diplomado Tecnología Educativa",
        "campus": [
            "ATZ",
            "MAR",
            "ECA",
            "SUR",
            "CUI"
        ],
        "interes": "9",
        "modalidad": "1",
        "Grupo_carreras": "66",
        "catCosto": "DIP",
        "lineaWeb": "POSGRADO"
    },
    {
        "IdDynamics": "2083",
        "nombre": "Diplomado Tecnología Educativa",
        "campus": [
            "ONL"
        ],
        "interes": "10",
        "modalidad": "3",
        "Grupo_carreras": "66",
        "catCosto": "DIPON",
        "lineaWeb": "POSGRADO"
    },
    {
        "IdDynamics": "681",
        "nombre": "Diseño Gráfico",
        "campus": [
            "ATZ",
            "MAR",
            "ECA",
            "GDL",
            "LEO",
            "SUR",
            "CUI",
            "TOL",
            "QRO",
            "REY"
        ],
        "interes": "2",
        "modalidad": "1",
        "Grupo_carreras": "28",
        "catCosto": "ING",
        "lineaWeb": "LICENCIATURA"
    },
    {
        "IdDynamics": "897",
        "nombre": "Diseño Industrial",
        "campus": [
            "ATZ",
            "MAR",
            "ECA",
            "SUR",
            "CUI",
            "LEO"
        ],
        "interes": "2",
        "modalidad": "1",
        "Grupo_carreras": "29",
        "catCosto": "ING",
        "lineaWeb": "LICENCIATURA"
    },
    {
        "IdDynamics": "902",
        "nombre": "Diseño, Animación y Arte Digital",
        "campus": [
            "ATZ",
            "MAR",
            "ECA",
            "GDL",
            "SUR",
            "CUI",
            "TOL",
            "QRO",
            "LEO",
            "REY"
        ],
        "interes": "2",
        "modalidad": "1",
        "Grupo_carreras": "31",
        "catCosto": "ING",
        "lineaWeb": "LICENCIATURA"
    },
    {
        "IdDynamics": "608",
        "nombre": "Economía",
        "campus": [
            "ATZ",
            "MAR",
            "ECA",
            "SUR",
            "CUI"
        ],
        "interes": "2",
        "modalidad": "1",
        "Grupo_carreras": "32",
        "catCosto": "LIC",
        "lineaWeb": "LICENCIATURA"
    },
    {
        "IdDynamics": "1150",
        "nombre": "Enfermería",
        "campus": [
            "ATZ",
            "MAR",
            "ECA",
            "GDL",
            "LEO",
            "SUR",
            "CUI",
            "TOL",
            "QRO",
            "REY"
        ],
        "interes": "2",
        "modalidad": "1",
        "Grupo_carreras": "37",
        "catCosto": "ENFER",
        "lineaWeb": "SALUD"
    },
    {
        "IdDynamics": "195",
        "nombre": "Especialidad Endodoncia",
        "campus": [
            "MAR"
        ],
        "interes": "8",
        "modalidad": "1",
        "Grupo_carreras": "36",
        "catCosto": "ESP",
        "lineaWeb": "POSGRADO"
    },
    {
        "IdDynamics": "193",
        "nombre": "Especialidad Odontología Pediátrica",
        "campus": [
            "MAR"
        ],
        "interes": "8",
        "modalidad": "1",
        "Grupo_carreras": "51",
        "catCosto": "ESP",
        "lineaWeb": "POSGRADO"
    },
    {
        "IdDynamics": "197",
        "nombre": "Especialidad Odontología Restauradora",
        "campus": [
            "MAR"
        ],
        "interes": "82",
        "modalidad": "1",
        "Grupo_carreras": "52",
        "catCosto": "ESP",
        "lineaWeb": "POSGRADO"
    },
    {
        "IdDynamics": "192",
        "nombre": "Especialidad Ortodoncia",
        "campus": [
            "MAR"
        ],
        "interes": "8",
        "modalidad": "1",
        "Grupo_carreras": "53",
        "catCosto": "ESP",
        "lineaWeb": "POSGRADO"
    },
    {
        "IdDynamics": "196",
        "nombre": "Especialidad Periodoncia",
        "campus": [
            "MAR"
        ],
        "interes": "8",
        "modalidad": "1",
        "Grupo_carreras": "55",
        "catCosto": "ESP",
        "lineaWeb": "POSGRADO"
    },
    {
        "IdDynamics": "607",
        "nombre": "Finanzas",
        "campus": [
            "ATZ",
            "MAR",
            "ECA",
            "LEO",
            "SUR",
            "CUI"
        ],
        "interes": "2",
        "modalidad": "1",
        "Grupo_carreras": "40",
        "catCosto": "LIC",
        "lineaWeb": "LICENCIATURA"
    },
    {
        "IdDynamics": "1060",
        "nombre": "Fisioterapia",
        "campus": [
            "ATZ",
            "MAR",
            "ECA",
            "GDL",
            "LEO",
            "SUR",
            "CUI",
            "TOL",
            "QRO",
            "REY"
        ],
        "interes": "2",
        "modalidad": "1",
        "Grupo_carreras": "41",
        "catCosto": "FISIO",
        "lineaWeb": "SALUD"
    },
    {
        "IdDynamics": "886",
        "nombre": "Industrial y Sistemas",
        "campus": [
            "ATZ",
            "MAR",
            "ECA",
            "GDL",
            "LEO",
            "SUR",
            "CUI",
            "TOL",
            "QRO",
            "REY"
        ],
        "interes": "5",
        "modalidad": "1",
        "Grupo_carreras": "42",
        "catCosto": "ING",
        "lineaWeb": "INGENIERIA"
    },
    {
        "IdDynamics": "729",
        "nombre": "Licenciatura en Comercio Internacional",
        "campus": [
            "ATZ",
            "MAR",
            "ECA",
            "SUR",
            "CUI",
            "TOL",
            "QRO",
            "GDL",
            "LEO"
        ],
        "interes": "2",
        "modalidad": "1",
        "Grupo_carreras": "70",
        "catCosto": "LIC",
        "lineaWeb": "LICENCIATURA"
    },
    {
        "IdDynamics": "3929",
        "nombre": "Licenciatura en Comercio Internacional",
        "campus": [
            "ATZ",
            "MAR",
            "ECA",
            "SUR",
            "CUI"
        ], 
        "interes": "3",
        "modalidad": "2",
        "Grupo_carreras": "70",
        "catCosto": "LICEJEC",
        "lineaWeb": "LICENCIATURA"
    },
    {
        "IdDynamics": "1129",
        "nombre": "Licenciatura en Comercio Internacional",
        "campus": [
            "ONL"
        ],
        "interes": "4",
        "modalidad": "3",
        "Grupo_carreras": "70",
        "catCosto": "LICON",
        "lineaWeb": "LICENCIATURA"
    },
    {
        "IdDynamics": "359",
        "nombre": "Maestría Administración de la Comunicación",
        "campus": [
            "MAR",
            "CUI"
        ],
        "interes": "6",
        "modalidad": "1",
        "Grupo_carreras": "2",
        "catCosto": "POSCUAT",
        "lineaWeb": "POSGRADO"
    },
    {
        "IdDynamics": "1637",
        "nombre": "Maestría Administración de la Comunicación",
        "campus": [
            "ONL"
        ],
        "interes": "7",
        "modalidad": "3",
        "Grupo_carreras": "2",
        "catCosto": "POSONCUAT",
        "lineaWeb": "POSGRADO"
    },
    {
        "IdDynamics": "130",
        "nombre": "Maestría Administración Negocios",
        "campus": [
            "ATZ",
            "MAR",
            "ECA",
            "SUR",
            "CUI",
            "TOL",
            "LEO"
        ],
        "interes": "6",
        "modalidad": "1",
        "Grupo_carreras": "9",
        "catCosto": "POSCUAT",
        "lineaWeb": "POSGRADO"
    },
    {
        "IdDynamics": "1630",
        "nombre": "Maestría Administración Negocios",
        "campus": [
            "ONL"
        ],
        "interes": "7",
        "modalidad": "3",
        "Grupo_carreras": "9",
        "catCosto": "POSONCUAT",
        "lineaWeb": "POSGRADO"
    },
    {
        "IdDynamics": "131",
        "nombre": "Maestría Derecho",
        "campus": [
            "ATZ",
            "MAR",
            "ECA",
            "SUR",
            "CUI",
            "TOL",
            "LEO"
        ],
        "interes": "6",
        "modalidad": "1",
        "Grupo_carreras": "23",
        "catCosto": "POSCUAT",
        "lineaWeb": "POSGRADO"
    },
    {
        "IdDynamics": "1631",
        "nombre": "Maestría Derecho",
        "campus": [
            "ONL"
        ],
        "interes": "7",
        "modalidad": "3",
        "Grupo_carreras": "23",
        "catCosto": "POSONCUAT",
        "lineaWeb": "POSGRADO"
    },
    {
        "IdDynamics": "132",
        "nombre": "Maestría Dirección Proyectos",
        "campus": [
            "ATZ",
            "MAR",
            "ECA",
            "SUR",
            "CUI",
            "TOL"
        ],
        "interes": "6",
        "modalidad": "1",
        "Grupo_carreras": "27",
        "catCosto": "POSCUAT",
        "lineaWeb": "POSGRADO"
    },
    {
        "IdDynamics": "1632",
        "nombre": "Maestría Dirección Proyectos",
        "campus": [
            "ONL"
        ],
        "interes": "7",
        "modalidad": "3",
        "Grupo_carreras": "27",
        "catCosto": "POSONCUAT",
        "lineaWeb": "POSGRADO"
    },
    {
        "IdDynamics": "358",
        "nombre": "Maestría Diseño Multimedia",
        "campus": [
            "MAR",
            "CUI"
        ],
        "interes": "6",
        "modalidad": "1",
        "Grupo_carreras": "30",
        "catCosto": "POSCUAT",
        "lineaWeb": "POSGRADO"
    },
    {
        "IdDynamics": "1638",
        "nombre": "Maestría Diseño Multimedia",
        "campus": [
            "ONL"
        ],
        "interes": "7",
        "modalidad": "3",
        "Grupo_carreras": "30",
        "catCosto": "POSONCUAT",
        "lineaWeb": "POSGRADO"
    },
    {
        "IdDynamics": "1633",
        "nombre": "Maestría Educación",
        "campus": [
            "ONL"
        ],
        "interes": "7",
        "modalidad": "3",
        "Grupo_carreras": "33",
        "catCosto": "POSONCUAT",
        "lineaWeb": "POSGRADO"
    },
    {
        "IdDynamics": "133",
        "nombre": "Maestría Educación",
        "campus": [
            "MAR",
            "CUI",
            "SUR",
            "ATZ",
            "ECA"
        ],
        "interes": "6",
        "modalidad": "1",
        "Grupo_carreras": "33",
        "catCosto": "POSCUAT",
        "lineaWeb": "POSGRADO"
    },
    {
        "IdDynamics": "268",
        "nombre": "Maestría en Dirección de Organizaciones de Salud",
        "campus": [
            "ATZ",
            "SUR",
            "ECA",
            "TOL",
            "MAR"
        ],
        "interes": "6",
        "modalidad": "1",
        "Grupo_carreras": "81",
        "catCosto": "POSCUAT",
        "lineaWeb": "POSGRADO"
    },
    {
        "IdDynamics": "1668",
        "nombre": "Maestría en Dirección de Organizaciones de Salud",
        "campus": [
            "ONL"
        ],
        "interes": "7",
        "modalidad": "3",
        "Grupo_carreras": "81",
        "catCosto": "POSONCUAT",
        "lineaWeb": "POSGRADO"
    },
    {
        "IdDynamics": "363",
        "nombre": "Maestría Juicios Orales",
        "campus": [
            "MAR"
        ],
        "interes": "6",
        "modalidad": "1",
        "Grupo_carreras": "43",
        "catCosto": "POSCUAT",
        "lineaWeb": "POSGRADO"
    },
    {
        "IdDynamics": "1636",
        "nombre": "Maestría Juicios Orales",
        "campus": [
            "ONL"
        ],
        "interes": "7",
        "modalidad": "3",
        "Grupo_carreras": "43",
        "catCosto": "POSONCUAT",
        "lineaWeb": "POSGRADO"
    },
    {
        "IdDynamics": "361",
        "nombre": "Maestría Migración Sistemas",
        "campus": [
            "MAR",
            "CUI"
        ],
        "interes": "6",
        "modalidad": "1",
        "Grupo_carreras": "48",
        "catCosto": "POSCUAT",
        "lineaWeb": "POSGRADO"
    },
    {
        "IdDynamics": "134",
        "nombre": "Maestría Psicología",
        "campus": [
            "ATZ",
            "MAR",
            "ECA",
            "SUR",
            "CUI"
        ],
        "interes": "6",
        "modalidad": "1",
        "Grupo_carreras": "69",
        "catCosto": "POSCUAT",
        "lineaWeb": "POSGRADO"
    },
    {
        "IdDynamics": "1634",
        "nombre": "Maestría Psicología",
        "campus": [
            "ONL"
        ],
        "interes": "7",
        "modalidad": "3",
        "Grupo_carreras": "69",
        "catCosto": "POSONCUAT",
        "lineaWeb": "POSGRADO"
    },
    {
        "IdDynamics": "135",
        "nombre": "Maestría Seguridad en Tecnología de la Información",
        "campus": [
            "MAR",
            "CUI"
        ],
        "interes": "6",
        "modalidad": "1",
        "Grupo_carreras": "63",
        "catCosto": "POSCUAT",
        "lineaWeb": "POSGRADO"
    },
    {
        "IdDynamics": "884",
        "nombre": "Mecánica",
        "campus": [
            "ATZ",
            "MAR",
            "ECA",
            "SUR",
            "CUI",
            "TOL",
            "LEO",
            "GDL"
        ],
        "interes": "5",
        "modalidad": "1",
        "Grupo_carreras": "44",
        "catCosto": "ING",
        "lineaWeb": "INGENIERIA"
    },
    {
        "IdDynamics": "588",
        "nombre": "Mecatrónica",
        "campus": [
            "ATZ",
            "MAR",
            "ECA",
            "SUR",
            "CUI",
            "QRO",
            "TOL",
            "LEO",
            "GDL"
        ],
        "interes": "5",
        "modalidad": "1",
        "Grupo_carreras": "45",
        "catCosto": "ING",
        "lineaWeb": "INGENIERIA"
    },
    {
        "IdDynamics": "706",
        "nombre": "Mercadotecnia",
        "campus": [
            "ATZ",
            "MAR",
            "ECA",
            "GDL",
            "LEO",
            "SUR",
            "CUI",
            "TOL",
            "QRO",
            "REY"
        ],
        "interes": "2",
        "modalidad": "1",
        "Grupo_carreras": "47",
        "catCosto": "LIC",
        "lineaWeb": "LICENCIATURA"
    },
    {
        "IdDynamics": "3956",
        "nombre": "Mercadotecnia",
        "campus": [
            "ATZ",
            "MAR",
            "ECA",
            "GDL",
            "LEO",
            "SUR",
            "CUI",
            "TOL",
            "QRO"
        ],
        "interes": "3",
        "modalidad": "2",
        "Grupo_carreras": "47",
        "catCosto": "LICEJEC",
        "lineaWeb": "LICENCIATURA"
    },
    {
        "IdDynamics": "1106",
        "nombre": "Mercadotecnia",
        "campus": [
            "ONL"
        ],
        "interes": "4",
        "modalidad": "3",
        "Grupo_carreras": "47",
        "catCosto": "LICON",
        "lineaWeb": "LICENCIATURA"
    },
    {
        "IdDynamics": "713",
        "nombre": "Negocios Internacionales",
        "campus": [
            "ATZ",
            "MAR",
            "ECA",
            "GDL",
            "LEO",
            "SUR",
            "CUI",
            "TOL",
            "QRO",
            "REY"
        ],
        "interes": "2",
        "modalidad": "1",
        "Grupo_carreras": "49",
        "catCosto": "LIC",
        "lineaWeb": "LICENCIATURA"
    },
    {
        "IdDynamics": "1213",
        "nombre": "Negocios Internacionales",
        "campus": [
            "ONL"
        ],
        "interes": "4",
        "modalidad": "3",
        "Grupo_carreras": "49",
        "catCosto": "LICON",
        "lineaWeb": "LICENCIATURA"
    },
    {
        "IdDynamics": "3973",
        "nombre": "Negocios Internacionales",
        "campus": [
            "ATZ",
            "MAR",
            "ECA",
            "SUR",
            "CUI",
            "LEO",
            "GDL",
            "QRO"
        ],
        "interes": "3",
        "modalidad": "2",
        "Grupo_carreras": "49",
        "catCosto": "LICEJEC",
        "lineaWeb": "LICENCIATURA"
    },
    {
        "IdDynamics": "1040",
        "nombre": "Nutrición",
        "campus": [
            "ATZ",
            "MAR",
            "ECA",
            "GDL",
            "LEO",
            "SUR",
            "CUI",
            "TOL",
            "QRO",
            "REY"
        ],
        "interes": "2",
        "modalidad": "1",
        "Grupo_carreras": "50",
        "catCosto": "NUTRI",
        "lineaWeb": "SALUD"
    },
    {
        "IdDynamics": "1112",
        "nombre": "Pedagogía",
        "campus": [
            "ONL"
        ],
        "interes": "4",
        "modalidad": "3",
        "Grupo_carreras": "54",
        "catCosto": "LICON",
        "lineaWeb": "LICENCIATURA"
    },
    {
        "IdDynamics": "612",
        "nombre": "Pedagogía",
        "campus": [
            "ATZ",
            "MAR",
            "ECA",
            "GDL",
            "LEO",
            "SUR",
            "CUI",
            "TOL",
            "QRO",
            "REY"
        ],
        "interes": "2",
        "modalidad": "1",
        "Grupo_carreras": "54",
        "catCosto": "LICPEDAG",
        "lineaWeb": "LICENCIATURA"
    },
    {
        "IdDynamics": "3972",
        "nombre": "Pedagogía",
        "campus": [
            "ATZ",
            "MAR",
            "ECA",
            "SUR",
            "CUI",
            "LEO",
            "QRO",
            "GDL"
        ],
        "interes": "3",
        "modalidad": "2",
        "Grupo_carreras": "54",
        "catCosto": "LICEJEC",
        "lineaWeb": "LICENCIATURA"
    },
    {
        "IdDynamics": "666",
        "nombre": "Preparatoria",
        "campus": [
            "ATZ",
            "MAR",
            "ECA",
            "GDL",
            "LEO",
            "SUR",
            "CUI",
            "TOL",
            "QRO",
            "REY"
        ],
        "interes": "1",
        "modalidad": "1",
        "Grupo_carreras": "56",
        "catCosto": "PREPA",
        "lineaWeb": "PREPARATORIA"
    },
    {
        "IdDynamics": "1111",
        "nombre": "Psicología",
        "campus": [
            "ONL"
        ],
        "interes": "4",
        "modalidad": "3",
        "Grupo_carreras": "57",
        "catCosto": "LICON",
        "lineaWeb": "LICENCIATURA"
    },
    {
        "IdDynamics": "611",
        "nombre": "Psicología",
        "campus": [
            "ATZ",
            "ECA",
            "GDL",
            "LEO",
            "SUR",
            "CUI",
            "TOL",
            "QRO",
            "REY"
        ],
        "interes": "2",
        "modalidad": "1",
        "Grupo_carreras": "57",
        "catCosto": "LICPSICOLOG",
        "lineaWeb": "LICENCIATURA"
    },
    {
        "IdDynamics": "3961",
        "nombre": "Psicología",
        "campus": [
            "ATZ",
            "MAR",
            "ECA",
            "GDL",
            "LEO",
            "SUR",
            "CUI",
            "TOL",
            "QRO"
        ],
        "interes": "3",
        "modalidad": "2",
        "Grupo_carreras": "57",
        "catCosto": "LICEJEC",
        "lineaWeb": "LICENCIATURA"
    },
    {
        "IdDynamics": "715",
        "nombre": "Publicidad y Medios",
        "campus": [
            "ATZ",
            "MAR",
            "ECA",
            "SUR",
            "CUI"
        ],
        "interes": "2",
        "modalidad": "1",
        "Grupo_carreras": "60",
        "catCosto": "LIC",
        "lineaWeb": "LICENCIATURA"
    },
    {
        "IdDynamics": "787",
        "nombre": "Química",
        "campus": [
            "ATZ",
            "MAR",
            "ECA",
            "SUR",
            "CUI",
            "TOL",
            "LEO",
            "GDL"
        ],
        "interes": "5",
        "modalidad": "1",
        "Grupo_carreras": "62",
        "catCosto": "ING",
        "lineaWeb": "INGENIERIA"
    },
    {
        "IdDynamics": "889",
        "nombre": "Sistemas Computacionales",
        "campus": [
            "ATZ",
            "MAR",
            "ECA",
            "GDL",
            "LEO",
            "SUR",
            "CUI",
            "TOL",
            "QRO",
            "REY"
        ],
        "interes": "5",
        "modalidad": "1",
        "Grupo_carreras": "64",
        "catCosto": "ING",
        "lineaWeb": "INGENIERIA"
    },
    {
        "IdDynamics": "892",
        "nombre": "Sistemas Digitales y Robótica",
        "campus": [
            "ATZ",
            "MAR",
            "ECA",
            "SUR",
            "CUI"
        ],
        "interes": "5",
        "modalidad": "1",
        "Grupo_carreras": "65",
        "catCosto": "ING",
        "lineaWeb": "INGENIERIA"
    },
    {
        "IdDynamics": "1221",
        "nombre": "Gastronomía",
        "campus": [
            "ATZ",
            "ECA",
            "QRO",
            "TOL",
            "GDL",
            "LEO",
            "CUI",
            "REY"
        ],
        "interes": "2",
        "modalidad": "1",
        "Grupo_carreras": "73",
        "catCosto": "GASTRO",
        "lineaWeb": "LICENCIATURA"
    },
    {
        "IdDynamics": "885",
        "nombre": "Telecomunicaciones y Electrónica",
        "campus": [
            "ATZ",
            "MAR",
            "ECA",
            "SUR",
            "CUI"
        ],
        "interes": "5",
        "modalidad": "1",
        "Grupo_carreras": "68",
        "catCosto": "ING",
        "lineaWeb": "INGENIERIA"
    },
    {
        "IdDynamics": "1149",
        "nombre": "Software y redes",
        "campus": [
            "ONL"
        ],
        "interes": "4",
        "modalidad": "3",
        "Grupo_carreras": "74",
        "catCosto": "INGON",
        "lineaWeb": "INGENIERIA"
    },
    {
        "IdDynamics": "1146",
        "nombre": "Industrial y Administración",
        "campus": [
            "ONL"
        ],
        "interes": "4",
        "modalidad": "3",
        "Grupo_carreras": "75",
        "catCosto": "INGON",
        "lineaWeb": "INGENIERIA"
    },
    {
        "IdDynamics": "1548",
        "nombre": "Maestria en Analítica e Inteligencia de Negocios",
        "campus": [
            "ONL"
        ],
        "interes": "7",
        "modalidad": "3",
        "Grupo_carreras": "76",
        "catCosto": "POSONCUAT",
        "lineaWeb": "POSGRADO"
    },
    {
        "IdDynamics": "1314",
        "nombre": "Relaciones Internacionales",
        "campus": [
            "CUI",
            "SUR",
            "ATZ",
            "ECA",
            "QRO"
        ],
        "interes": "2",
        "modalidad": "1",
        "Grupo_carreras": "77",
        "catCosto": "LIC",
        "lineaWeb": "LICENCIATURA"
    },
    {
        "IdDynamics": "1326",
        "nombre": "Mercadotecnia Digital y Publicidad",
        "campus": [
            "LEO",
            "GDL"
        ],
        "interes": "2",
        "modalidad": "1",
        "Grupo_carreras": "78",
        "catCosto": "LIC",
        "lineaWeb": "LICENCIATURA"
    },
    {
        "IdDynamics": "3976",
        "nombre": "Diseño Digital y Animación",
        "campus": [
            "CUI",
            "SUR",
            "ATZ",
            "ECA",
            "LEO",
            "GDL",
            "QRO"
        ],
        "interes": "3",
        "modalidad": "2",
        "Grupo_carreras": "79",
        "catCosto": "INGEJEC",
        "lineaWeb": "LICENCIATURA"
    },
    {
        "IdDynamics": "3875",
        "nombre": "Software y redes",
        "campus": [
            "CUI",
            "SUR",
            "ATZ",
            "ECA",
            "GDL",
            "LEO"
        ],
        "interes": "3",
        "modalidad": "2",
        "Grupo_carreras": "74",
        "catCosto": "INGEJEC",
        "lineaWeb": "INGENIERIA"
    },
    {
        "IdDynamics": "3974",
        "nombre": "Industrial y Administración",
        "campus": [
            "CUI",
            "SUR",
            "ATZ",
            "ECA",
            "GDL",
            "LEO",
            "QRO"
        ],
        "interes": "3",
        "modalidad": "2",
        "Grupo_carreras": "75",
        "catCosto": "INGEJEC",
        "lineaWeb": "INGENIERIA"
    },
    {
        "IdDynamics": "1144",
        "nombre": "Relaciones Internacionales",
        "campus": [
            "ONL"
        ],
        "interes": "4",
        "modalidad": "3",
        "Grupo_carreras": "77",
        "catCosto": "LICON",
        "lineaWeb": "LICENCIATURA"
    },
    {
        "IdDynamics": "3914",
        "nombre": "Relaciones Internacionales",
        "campus": [
            "LEO",
            "GDL",
            "QRO"
        ],
        "interes": "3",
        "modalidad": "2",
        "Grupo_carreras": "77",
        "catCosto": "LICEJEC",
        "lineaWeb": "LICENCIATURA"
    },
    {
        "IdDynamics": "3932",
        "nombre": "Comunicación y Medios",
        "campus": [
            "ONL"
        ],
        "interes": "4",
        "modalidad": "3",
        "Grupo_carreras": "80",
        "catCosto": "LICON",
        "lineaWeb": "LICENCIATURA"
    },
    
    {
        "IdDynamics": "1176",
        "nombre": "Diseño Digital y Animación",
        "campus": [
            "ONL"
        ],
        "interes": "4",
        "modalidad": "3",
        "Grupo_carreras": "79",
        "catCosto": "INGON",
        "lineaWeb": "LICENCIATURA"
    },
    {
        "IdDynamics": "1346",
        "nombre": "Emprendimiento y Desarrollo de Negocios",
        "campus": [           
            "CUI"           
        ],
        "interes": "2",
        "modalidad": "1",
        "Grupo_carreras": "82",
        "catCosto": "LIC",
        "lineaWeb": "LICENCIATURA"
    },
    {
        "IdDynamics": "139",
        "nombre": "Mtría. en Desarrollo Empresarial Sostenible",
        "campus": [
            "ONL"
        ],
        "interes": "7",
        "modalidad": "3",
        "Grupo_carreras": "83",
        "catCosto": "POSONCUAT",
        "lineaWeb": "POSGRADO"
    }
];