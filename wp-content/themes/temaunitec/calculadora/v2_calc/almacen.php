<?php
ini_set("display_errors","off");
include "configuracion.php";
include "funciones.php";

$search = explode(",","ç,æ,œ,á,é,í,ó,ú,à,è,ì,ò,ù,ä,ë,ï,ö,ü,ÿ,â,ê,î,ô,û,å,e,i,ø,u,ñ");
$replace = explode(",","c,ae,oe,a,e,i,o,u,a,e,i,o,u,a,e,i,o,u,y,a,e,i,o,u,a,e,i,o,u,ñ");

if(isset($_REQUEST["nom_carrera"] ) ) {
	$nom_carrera = mysql_real_escape_string(utf8_decode($_REQUEST["nom_carrera"]));
	$nom_campus = htmlspecialchars(mysql_real_escape_string( $_REQUEST["nom_campus"]) , ENT_QUOTES);
	$planSelec =  htmlspecialchars(mysql_real_escape_string( $_REQUEST["planSelec"]) , ENT_QUOTES);
	$promedioF =  htmlspecialchars(mysql_real_escape_string( $_REQUEST["promedioF"]) , ENT_QUOTES);
	$Pdescuento =  htmlspecialchars(mysql_real_escape_string( $_REQUEST["Pdescuento"]) , ENT_QUOTES);
	$nivelF =  htmlspecialchars(mysql_real_escape_string( $_REQUEST["nivelF"]) , ENT_QUOTES);
	$planSelec = htmlspecialchars(mysql_real_escape_string( $_REQUEST["planSelec"]) , ENT_QUOTES);
	$CostoSelec = htmlspecialchars(mysql_real_escape_string( $_REQUEST["CostoSelec"]) , ENT_QUOTES);
	$planRecom = htmlspecialchars(mysql_real_escape_string( $_REQUEST["planRecom"]) , ENT_QUOTES);
	$CostoRecom = htmlspecialchars(mysql_real_escape_string( $_REQUEST["CostoRecom"]) , ENT_QUOTES);
	
	$F_Regis =  date('Y-m-d');
	$Herramienta = htmlspecialchars(mysql_real_escape_string( $_REQUEST["Herramienta"]) , ENT_QUOTES);
	$Campana = htmlspecialchars(mysql_real_escape_string(  $_REQUEST["Campana"]) , ENT_QUOTES);
	$Origen = htmlspecialchars(mysql_real_escape_string(  $_REQUEST["Origen"]) , ENT_QUOTES);
	$Calidad = htmlspecialchars(mysql_real_escape_string(  $_REQUEST["Calidad"]) , ENT_QUOTES);
	$L_Negocio = htmlspecialchars(mysql_real_escape_string(  $_REQUEST["L_Negocio"]) , ENT_QUOTES);
	$F_Nacim = htmlspecialchars(mysql_real_escape_string(  $_REQUEST["F_Nacim"]) , ENT_QUOTES);
	$cimay = @(date('y')+1).'-3';
	if(($_REQUEST['Pobla'] == 5 || $_REQUEST['Pobla'] == 6) && $_REQUEST['periodo_interes_sel'] == $cimay)
		$Ciclo = @(date('y')+2).'-1';
	else
		$Ciclo =  htmlspecialchars(mysql_real_escape_string(  $_REQUEST["periodo_interes_sel"]) , ENT_QUOTES);
	if($Ciclo == 0 || $Ciclo == '')
		$Ciclo = '15-3';
	$Alumno = htmlspecialchars(mysql_real_escape_string(  $_REQUEST["alum"]) , ENT_QUOTES);
	$Estad = htmlspecialchars(mysql_real_escape_string( $_REQUEST["Estad"]) , ENT_QUOTES);
	$Carrera = mysql_real_escape_string( strtoupper( utf8_decode($_REQUEST["Carrera"])));
	$C_Carrera = htmlspecialchars(mysql_real_escape_string(  $_REQUEST["C_Carrera"]) , ENT_QUOTES);
	$banner_id = htmlspecialchars(mysql_real_escape_string(  $_REQUEST["banner_id"]) , ENT_QUOTES);
	$Banner = htmlspecialchars(mysql_real_escape_string( $_REQUEST["Banner"]) , ENT_QUOTES);
	$Nombre = htmlspecialchars(mysql_real_escape_string( strtoupper( str_replace($search, $replace, $_REQUEST["Nombre"]) )), ENT_QUOTES);
	$Ap_Pat = htmlspecialchars(mysql_real_escape_string( strtoupper( str_replace($search, $replace, $_REQUEST["Ap_Pat"]) )), ENT_QUOTES);
	$Ap_Mat = htmlspecialchars(mysql_real_escape_string( strtoupper( str_replace($search, $replace, $_REQUEST["Ap_Mat"]) )), ENT_QUOTES);
	$Sexo = htmlspecialchars(mysql_real_escape_string( $_REQUEST["Sexo"]) , ENT_QUOTES);
	$email = htmlspecialchars(mysql_real_escape_string(  $_REQUEST["email"]) , ENT_QUOTES);
	$Campus = htmlspecialchars(mysql_real_escape_string( $_REQUEST["Campus"]) , ENT_QUOTES);
	$tipoTel = htmlspecialchars(mysql_real_escape_string( strtoupper( $_REQUEST["tipoTel"] )), ENT_QUOTES);
	if($tipoTel == 'CELULAR'){
		$Celul = htmlspecialchars(mysql_real_escape_string( $_REQUEST["Telef"]) , ENT_QUOTES);
	}
	else{
		$Telef = htmlspecialchars(mysql_real_escape_string(  $_REQUEST["Telef"]) , ENT_QUOTES);
	}
	$CP = htmlspecialchars(mysql_real_escape_string(  $_REQUEST["CP"]) , ENT_QUOTES);
	$Pobla = htmlspecialchars(mysql_real_escape_string(  $_REQUEST["Pobla"]) , ENT_QUOTES);
	$nivelF = htmlspecialchars(mysql_real_escape_string(  $_REQUEST["nivelF"]) , ENT_QUOTES);
	$isDIP = "";
	$Alumno = "no";
	$nombreCompleto = $Nombre;
	if ($L_Negocio == 'P'){
		$Origen = "ASPIRANTES PREPA";
		$Herramienta =  "ASPIRANTES PREPA";
	}

	$CampusF =  $nom_campus;
	$Campus = $nom_campus;	
	$arrEsp = array(195,193,197,192,196); // Contiene los codigos de las carreras de Salud
	if(in_array($C_Carrera, $arrEsp)){
		$Campus = "MARINA";
	}		
	//CIRIJANO DENTISTA
	if($C_Carrera == "35"){
		$Campus = "MARINA";
		$CampusF = "MARINA";
	}
	if($nivelF=="DIP" || $nivelF=="DIPON"){
		$isDIP = " DIP";
	}

	/*===================================================*/
	/*===================================================*/
	/*Obtenemos el nombre del campus corto, atizapan.
	===================================================*/
	/*===================================================*/ 
	if ($Pdescuento != 0){
		$precioSelec = $CostoSelec - (($CostoSelec * $Pdescuento)/100); //4 años
		$precioRecom = $CostoRecom - (($CostoRecom * $Pdescuento)/100); //3 años
		$precioSelec = round ( $precioSelec );
		$precioRecom =  round (  $precioRecom );  
		$ahorroBecaR = round(($CostoRecom * 12) - ($precioRecom * 12));
		$ahorroBecaS = round(($CostoSelec * 12) - ($precioSelec * 12));
	}else{
		$precioSelec = $CostoSelec;
		$precioRecom = $CostoRecom;
	}
	if($planRecom != 0){
		$alternativa = "o en ".$planRecom." a&ntilde;os $".$precioRecom."*";
	}else{
		$alternativa = "";
	}
	if ($Pdescuento != 0){
		$por_descuento = $Pdescuento."%* Beca acad&eacute;mica <br> de Primer Ingreso";
	}else{
		$por_descuento = "";
	}
	if($promedioF==5.9){
		$promedioF = 6;
	}	
	
	//-->
	if($Pobla == 7 && $Campus == "CUITLAHUAC"){
		$Campus = "MARINA";
		$CampusF = "MARINA";
	}
	$arrsalud = array(1040, 1050, 1060,35); // Contiene los codigos de las carreras de Salud
	if($Campus == "CUITLAHUAC" && in_array($C_Carrera, $arrsalud)){
		$Campus = "MARINA";
		$CampusF = "MARINA";
	}
	
	
	/*===================================================*/
	/*===================================================*/
	/*Fin calculo de costos
	===================================================*/	
	/*===================================================*/
	/*Comienza Envio a base de datos de hosting
	===================================================*/
	/*===================================================*/   
	$busca_correo_SQL = "SELECT * FROM crm_calculadora WHERE email='".$email."'";
	$id = mysql_query($busca_correo_SQL) or die ('Fallo la consulta revisando correo');
	$res =  mysql_num_rows($id);
	if ($res == 0){
		$comandSQL = "INSERT INTO crm_calculadora VALUES ('','{$F_Regis}', '{$Herramienta}', '{$Campana}', '{$Origen}', '{$Calidad}', 
					'{$L_Negocio}', '{$F_Nacim}', '{$Ciclo}', '{$Alumno}', '{$Estad}','{$Carrera}', '{$C_Carrera}', '{$banner_id}', 
					'{$Banner}', '{$Nombre}', '{$Ap_Pat}', '{$Ap_Mat}', '{$Sexo}', '{$email}', '{$Telef}', '{$Celul}','{$CampusF}', '{$tipoTel}', 
					'{$CP}', '{$Pobla}', '{$planSelec}','{$promedioF}','{$Pdescuento}','{$CostoRecom}','{$CostoSelec}','{$planRecom}',
					'{$nivelF}',0,0,0,'','','','','','','','{$precioRecom}','{$precioSelec}','{$ahorroBecaR}','{$ahorroBecaS}')";		
		mysql_query($comandSQL)  or die ('Fallo insertar nuevo registro');		
		/*===================================================*/
		//Enviamos a crm_dynamics
		
			/*===================================================*/
			$costo1 =  $CostoSelec - (($CostoSelec * $Pdescuento)/100);
			$costo2 =  ($CostoSelec * 12) - ($costo1 * 12);
			$porcentaje = $Pdescuento;
			/*===================================================*/
			$costo1 = number_format(round($costo1),0,'.',',');
			$costo2 = number_format(round($costo2),0,'.',',');
			/*===================================================*/
			$hubspotCookie = isset($_COOKIE['hubspotutk']) ? $_COOKIE['hubspotutk'] : "";			
				$url = "https://www.unitec.mx/medios/4mkt/index.php";			
				//ACTUALIZAMOS CP					
				switch($CP){
					case "1":
						$CP = 10040;
					break;
					case "2":
						$CP = 10183;
					break;
					default:
						$CP = 10040;
					break;
				}		
				$date = "";
				/*===================================================*/
				$url_completa = $url."?micrositio=".urlencode($Herramienta)."&fuenteW=CALCU&UserToken=".$hubspotCookie.
									 "&nombre=".urlencode($Nombre)."&apaterno=".urlencode($Ap_Pat)."&amaterno=".urlencode($Ap_Mat).
									 "&email=".urlencode($email)."&celular=".urlencode($Celul)."&telefono=".urlencode($Telef).
									 "&estado=".urlencode($Estad)."&fechaNacimiento=".urlencode($F_Nacim)."&sexo=".urlencode($Sexo).
									 "&ciclo=".urlencode($Ciclo)."&campus=".urlencode($Campus)."&carrera=".urlencode(utf8_encode($Carrera)). 
									 "&carreraInteres=".urlencode($C_Carrera).
									 "&banner=".urlencode($Banner)."&esAlumno=".urlencode($Alumno).
									 "&nivelInteres=".urlencode($L_Negocio)."&calidadRegistro=C".urlencode($Calidad)."&subNivelInteres=".urlencode($Pobla).
									 "&tipoRegistro=".urlencode($CP)."&fechaRellamada=".urlencode($date).
									"&promedio=".urlencode($promedioF)."&porcentaje=".urlencode($porcentaje)."&costo1=".urlencode($costo1).
									"&costo2=".urlencode($costo2)."&planselec=".urlencode($planselec)."&tipocalculadora=";
			//echo $url_completa;
		
			$comandSQL = "UPDATE crm_calculadora SET estatus=3, email2 = '".$date."', estatusOrigen = NULL WHERE email='".$email."'";
			mysql_query($comandSQL);
			/*===================================================*/
			$ch = curl_init();
			curl_setopt($ch, CURLOPT_URL,$url_completa);
			curl_setopt($ch, CURLOPT_HEADER, 0);
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
			$result = curl_exec($ch);						#PARA ACTIVAR EL ENVIO DE DATOS AL CRM SE DEBE DESCOMENTAR ESTA LINEA
			$error = curl_error($ch);
			curl_close($ch);
			/*===================================================*/
	}else{
		$result = "-1";
		//Insertamos nueva consulta, con fecha y hora. 
		$comandSQL = "INSERT INTO crm_calculadora_consulta (email,campus,Carrera,C_Carrera,promedio,descuento,Banner,costo3,costo4,fecha) 
					VALUES ('".$email."','".$Campus."','".$Carrera."','".$C_Carrera."','".$promedioF."','".$Pdescuento."',
					'".$banner_id."','".$CostoSelec."','".$CostoRecom."',now())";		
		mysql_query($comandSQL)  or die ('Fallo la consulta, insert en calcu consulta');				
	}	
	
	$arrResult = explode("|",$result);	
	if ($arrResult[0] == 1){
		echo "0";
	}else{
		echo "1";
	}
	mysql_close();
}else{
	echo "1";
}
?>
