<?php
header('Access-Control-Allow-Origin: *'); 
ini_set("display_errors","off"); 
include "configuracion.php";
include "funciones.php";


$search = explode(",","ç,æ,œ,á,é,í,ó,ú,à,è,ì,ò,ù,ä,ë,ï,ö,ü,ÿ,â,ê,î,ô,û,å,e,i,ø,u,ñ");
$replace = explode(",","c,ae,oe,a,e,i,o,u,a,e,i,o,u,a,e,i,o,u,y,a,e,i,o,u,a,e,i,o,u,ñ");

#-#############################################
# Desc: Se asigna ciclo de auerdo a LN y mes en el qie desea entrar
# Param: str-> linea de NEgocio
#		 $mes-> Mes seleccionado para entrar
# returns: ciclo asignado (varchar)
function getCiclo($data){
	
	$ciclo = $data['Ciclo'];
	
	return $ciclo;
}

// echo "val".$_REQUEST["nom_carrera"];
// var_dump($_REQUEST);

if(isset($_REQUEST["nom_carrera"] ) ) {
	$nom_carrera = utf8_decode($_REQUEST["nom_carrera"]);
	$nom_campus = htmlspecialchars( $_REQUEST["nom_campus"]);
	$planSelec =  htmlspecialchars( $_REQUEST["planSelec"]);
	$promedioF =  htmlspecialchars( $_REQUEST["promedioF"]);
	$Pdescuento =  htmlspecialchars( $_REQUEST["Pdescuento"]);
	$nivelF =  htmlspecialchars( $_REQUEST["nivelF"]);
	$planSelec = htmlspecialchars( $_REQUEST["planSelec"]);
	$CostoSelec = htmlspecialchars( $_REQUEST["CostoSelec"]);
	$planRecom = htmlspecialchars( $_REQUEST["planRecom"]);
	$CostoRecom = htmlspecialchars( $_REQUEST["CostoRecom"]);

	/*Actualización de Envio de Datos a hubspot By SRP 09-07-2019*/
	//$costoMensual = htmlspecialchars( $_REQUEST["costoMensual"] );
	//$descuentoF = htmlspecialchars( $_REQUEST["descuentoF"] );
	/*End Actualización de Envio de Datos a hubspot By SRP 09-07-2019*/
	
	$F_Regis =  date('Y-m-d');
	$Herramienta = htmlspecialchars( $_REQUEST["Herramienta"]);
	$Campana = htmlspecialchars(  $_REQUEST["Campana"]);
	$Origen = htmlspecialchars(  $_REQUEST["Origen"]);
	$Calidad = htmlspecialchars(  $_REQUEST["Calidad"]);
	$L_Negocio = htmlspecialchars(  $_REQUEST["L_Negocio"]);
	$F_Nacim = htmlspecialchars(  $_REQUEST["F_Nacim"]);
	$cimay = @(date('y')+1).'-3';
	$Ciclo =  htmlspecialchars(  $_REQUEST["Ciclo"]);
	$Alumno = htmlspecialchars(  $_REQUEST["Alumno"]);
	$Estad = htmlspecialchars( $_REQUEST["Estad"]);
	$Carrera =  strtoupper( utf8_decode($_REQUEST["Carrera"]));
	$C_Carrera = htmlspecialchars(  $_REQUEST["C_Carrera"]);
	$banner_id = htmlspecialchars(  $_REQUEST["banner_id"]);
	$Banner = htmlspecialchars( $_REQUEST["Banner"]);
	$Nombre = htmlspecialchars( strtoupper( str_replace($search, $replace, $_REQUEST["Nombre"]) ));
	$Ap_Pat = htmlspecialchars( strtoupper( str_replace($search, $replace, $_REQUEST["Ap_Pat"]) ));
	$Ap_Mat = htmlspecialchars( strtoupper( str_replace($search, $replace, $_REQUEST["Ap_Mat"]) ));
	$Sexo = htmlspecialchars( $_REQUEST["Sexo"]);
	$email = htmlspecialchars(  $_REQUEST["email"]);
	$Campus = htmlspecialchars( $_REQUEST["Campus"]);
	$tipoTel = htmlspecialchars( strtoupper( $_REQUEST["TipoTel"] ));
	$urlRef = htmlspecialchars( strtoupper( $_REQUEST["URLreferer"] ));
	$anos_termino = $_REQUEST["termino"];
	if($_REQUEST["termino"] == "4m" || $_REQUEST["termino"] == "6m") {
		$anos_termino = "2";
	}else{
		$anos_termino="4";
	}

	$pago_mensual_beca = htmlspecialchars( strtoupper( $_REQUEST["pago_mensual_beca"] ));
	$CID= $_REQUEST["CID"];
	if($tipoTel == 'CEL_CALC'){
		$Celul = htmlspecialchars( $_REQUEST["Telef"]);
	}
	else{ 
		$Telef = htmlspecialchars(  $_REQUEST["Telef"]);
	}
	$CP = htmlspecialchars(  $_REQUEST["CP"]);
	$Pobla = htmlspecialchars(  $_REQUEST["Pobla"]);
	$nivelF = htmlspecialchars(  $_REQUEST["nivelF"]);
	$isDIP = "";
	$Alumno = "0";
	//$Ciclo = '16-1';
	$nombreCompleto = $Nombre;
	if ($L_Negocio == 'P'){
		$Origen = "ASPIRANTES PREPA";
		$Herramienta =  "ASPIRANTES PREPA";
		$aPaternoTutor = (strtoupper($_REQUEST['aPaternoTutor']) == "SI" )? "TUTOR": strtoupper($_REQUEST['aPaternoTutor']);
	}
	$CampusF =  $nom_campus;
	$Campus = $nom_campus;	
	$arrEsp = array(195,193,197,192,196); // Contiene los codigos de las carreras de Salud
	/*if(in_array($C_Carrera, $arrEsp)){
		$Campus = "MARINA";
		$Ciclo = '16-1';
	}		
	//CIRIJANO DENTISTA
	if($C_Carrera == "35"){
		$Campus = "MARINA";
		$CampusF = "MARINA";
		$Ciclo = '16-1';
	}*/
	if($nivelF=="DIP" || $nivelF=="DIPON"){
		$isDIP = " DIP";
	}
	/*===================================================*/
	/*===================================================*/
	/*Obtenemos el nombre del campus corto, atizapan.
	===================================================*/
	/*===================================================*/ 
	if ($Pdescuento != 0){
		$precioSelec = $CostoSelec - (($CostoSelec * $Pdescuento)/100); //4 años
		$precioRecom = $CostoRecom - (($CostoRecom * $Pdescuento)/100); //3 años
		$precioSelec = round ( $precioSelec );
		$precioRecom =  round (  $precioRecom );  
		$ahorroBecaR = round(($CostoRecom * 12) - ($precioRecom * 12));
		$ahorroBecaS = round(($CostoSelec * 12) - ($precioSelec * 12));
	}else{
		$precioSelec = $CostoSelec;
		$precioRecom = $CostoRecom;
	}
	if($planRecom != 0){
		$alternativa = "o en ".$planRecom." a&ntilde;os $".$precioRecom."*";
	}else{
		$alternativa = "";
	}
	if ($Pdescuento != 0){
		$por_descuento = $Pdescuento."%* Beca acad&eacute;mica <br> de Primer Ingreso";
	}else{
		$por_descuento = "";
	}
	if($promedioF==5.9){
		$promedioF = 6;
	}	
	
	//-->
	// if($Pobla == 7 && $Campus == "CUITLAHUAC"){
	// 	$Campus = "MARINA";
	// 	$CampusF = "MARINA";
	// }
	$arrsalud = array(1040, 1050, 1060,35); // Contiene los codigos de las carreras de Salud
	if($Campus == "CUITLAHUAC" && in_array($C_Carrera, $arrsalud)){
		$Campus = "MARINA";
		$CampusF = "MARINA";
	}


	/*Actualización de Envio de Datos a hubspot By SRP 09-07-2019*/
	$materias = htmlspecialchars( $_REQUEST["num_carrera"] );
	$costoMateria = htmlspecialchars( $_REQUEST["costoMensual"] );

	$ahorro = ( ($Pdescuento) / 100 );
	$precioPorcentaje = ( $materias * $costoMateria * $ahorro );
	

	$precioTotal = ( $materias * $costoMateria ); //4 años
	$ahorroBecaS = $precioPorcentaje;
	$precioSelec = ( $precioTotal - $ahorroBecaS );
	/*End Actualización de Envio de Datos a hubspot By SRP 09-07-2019*/
	
	
	/*===================================================*/
	/*===================================================*/
	/*Fin calculo de costos
	===================================================*/	
	/*===================================================*/
	/*Comienza Envio a base de datos de hosting
	===================================================*/
	/*===================================================*/   
	 $busca_correo_SQL = "SELECT * FROM crm_calculadora WHERE email='".$email."'";
	//echo "<br>";
	$id = mysqli_query($conection,$busca_correo_SQL) or die ('Fallo la consulta revisando correo');
	//mysqli_query($con,"SELECT * FROM Persons");
	$res =  mysqli_num_rows($id);
	if ($res == 0){
		$comandSQL = "INSERT INTO crm_calculadora VALUES ('','{$F_Regis}', '{$Herramienta}', '{$Campana}', '{$Origen}', '{$Calidad}', 
					'{$L_Negocio}', '{$F_Nacim}', '{$Ciclo}', '{$Alumno}', '{$Estad}','{$Carrera}', '{$C_Carrera}', '{$banner_id}', 
					'{$Banner}', '{$Nombre}', '{$Ap_Pat}', '{$Ap_Mat}', '{$Sexo}', '{$email}', '{$Telef}', '{$Celul}','{$CampusF}', '{$tipoTel}', 
					'{$CP}', '{$Pobla}', '{$anos_termino}','{$promedioF}','{$Pdescuento}','{$CostoRecom}','{$CostoSelec}','{$planRecom}',
					'{$nivelF}',0,0,0,'','','','','','','','{$precioRecom}','{$precioSelec}','{$ahorroBecaR}','{$ahorroBecaS}')";		
		mysqli_query($conection,$comandSQL)  or die ('Fallo insertar nuevo registro');		
		/*===================================================*/
		//Enviamos a crm_dynamics
		
			/*===================================================*/
			$costo1 =  $CostoSelec - (($CostoSelec * $Pdescuento)/100);
			$costo2 =  ($CostoSelec * 12) - ($costo1 * 12);
			$porcentaje = $Pdescuento;
			/*===================================================*/
			//$costo1 = number_format(round($costo1),0,'.',',');
			//$costo2 = number_format(round($costo2),0,'.',',');
			//MV77
			$costo1 = number_format(round($costo1),0,'.',',');
			$costo2 = number_format(round($pago_mensual_beca),0,'.',',');


			/*Actualización de Envio de Datos a hubspot By SRP 09-07-2019*/
			$costo1 = $precioSelec;
			$costo2 = $precioPorcentaje;
			/*End Actualización de Envio de Datos a hubspot By SRP 09-07-2019*/

			/*===================================================*/
			$hubspotCookie = isset($_COOKIE['hubspotutk']) ? $_COOKIE['hubspotutk'] : $_REQUEST['hubspotutk'];
			$ipAddress = $_SERVER['REMOTE_ADDR'];			
				$url = "https://www.unitec.mx/procWebLeads/index.php";			
				//ACTUALIZAMOS CP					
				switch($CP){
					case "1":
						$CP = 10040;
					break;
					case "2":
						$CP = 10183;
					break;
					default:
						$CP = $CP;
					break;
				}		
				$date = "";
				/*===================================================*/
				$url_completa = $url."?urlreferrer=".urlencode($urlRef)."&micrositio=".urlencode($Herramienta)."&fuenteW=CALCU&UserToken=".$hubspotCookie."&ipCalcu=".$ipAddress.
									 "&nombre=".urlencode($Nombre)."&CID=".urlencode($CID)."&apaterno=".urlencode($Ap_Pat)."&amaterno=".urlencode($Ap_Mat).
									 "&email=".urlencode($email)."&celular=".urlencode($Celul)."&telefono=".urlencode($Telef).
									 "&estado=".urlencode($Estad)."&fechaNacimiento=".urlencode($F_Nacim)."&sexo=".urlencode($Sexo).
									 "&ciclo=".urlencode($Ciclo)."&campus=".urlencode($Campus)."&carrera=".urlencode(utf8_encode($Carrera)). 
									 "&carreraInteres=".urlencode($C_Carrera).
									 "&banner=".urlencode($Banner)."&esAlumno=".urlencode($Alumno).
									 "&nivelInteres=".urlencode($L_Negocio)."&calidadRegistro=C".urlencode($Calidad)."&subNivelInteres=".urlencode($Pobla).
									 "&tipoRegistro=".urlencode($CP)."&fechaRellamada=".urlencode($date).
									 "&aPaternoTutor=".$aPaternoTutor.
									"&promedio=".urlencode($promedioF)."&porcentaje=".urlencode($porcentaje)."&costo1=".urlencode($costo1).
									"&costo2=".urlencode($costo2)."&planselec=".urlencode($anos_termino)."&tipocalculadora=";
			//echo $url_completa;
		
			$comandSQL = "UPDATE crm_calculadora SET estatus=3, email2 = '".$date."', estatusOrigen = NULL WHERE email='".$email."'";
			mysqli_query($conection,$comandSQL);
			
			/*===================================================*/
			$ch = curl_init();
			curl_setopt($ch, CURLOPT_URL,$url_completa);
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
            curl_setopt($ch, CURLOPT_HEADER, 0);
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
            curl_setopt($ch, CURLOPT_HEADER, 0);
			curl_setopt($ch, CURLOPT_SSL_VERIFYPEER,FALSE);
			curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 2); 
         //   curl_setopt($ch, CURLOPT_CAINFO, "C:\unitecssl.pem");
			
			
			
			$result = curl_exec($ch);						#PARA ACTIVAR EL ENVIO DE DATOS AL CRM SE DEBE DESCOMENTAR ESTA LINEA
			$error = curl_error($ch);
			curl_close($ch);
			/*===================================================*/
	}else{
		//print_r($_REQUEST);die();
		$result = '{"tipo":"dupli"}';
		//Insertamos nueva consulta, con fecha y hora. 
		$comandSQL = "INSERT INTO crm_calculadora_consulta (email,campus,Carrera,C_Carrera,promedio,descuento,Banner,costo3,costo4,fecha) 
					VALUES ('".$email."','".$Campus."','".$Carrera."','".$C_Carrera."','".$promedioF."','".$Pdescuento."',
					'".$banner_id."','".$CostoSelec."','".$CostoRecom."',now())";		
		mysqli_query($conection,$comandSQL)  or die ('Fallo la consulta, insert en calcu consulta');

		if(isset($_COOKIE['data_frm'])){
			$costo1 =  $CostoSelec - (($CostoSelec * $Pdescuento)/100);
			$costo2 =  ($CostoSelec * 12) - ($costo1 * 12);
			$porcentaje = $Pdescuento;
			/*===================================================*/
			//$costo1 = number_format(round($costo1),0,'.',',');
			//$costo2 = number_format(round($costo2),0,'.',',');
			//MV77
			$costo1 = number_format(round($costo1),0,'.',',');
			$costo2 = number_format(round($pago_mensual_beca),0,'.',',');
			/*===================================================*/
			$hubspotCookie = isset($_COOKIE['hubspotutk']) ? $_COOKIE['hubspotutk'] : $_REQUEST['hubspotutk'];
			$ipAddress = $_SERVER['REMOTE_ADDR'];			
				$url = "https://www.unitec.mx/procWebLeads/index.php";		

				/*Actualización de Envio de Datos a hubspot By SRP 09-07-2019*/
				$costo1 = $precioSelec;
				$costo2 = $precioPorcentaje;
				/*End Actualización de Envio de Datos a hubspot By SRP 09-07-2019*/

					
				//ACTUALIZAMOS CP					
				switch($CP){
					case "1":
						$CP = 10040;
					break;
					case "2":
						$CP = 10183;
					break;
					default:
						$CP = $CP;
					break;
				}		
				$date = "";
				/*===================================================*/
				$cookie_frm = json_decode($_COOKIE['data_frm'], true);
				$url_completa = $url."?urlreferrer=".urlencode($urlRef)."&micrositio=".urlencode($Herramienta)."&fuenteW=CALCU&UserToken=".$hubspotCookie."&ipCalcu=".$ipAddress.
									 "&nombre=".urlencode($Nombre)."&CID=".urlencode($CID)."&apaterno=".urlencode($Ap_Pat)."&amaterno=".urlencode($Ap_Mat).
									 "&email=".urlencode($email)."&celular=".urlencode($Celul)."&telefono=".urlencode($Telef).
									 "&estado=".urlencode($Estad)."&fechaNacimiento=".urlencode($F_Nacim)."&sexo=".urlencode($Sexo).
									 "&ciclo=".urlencode($Ciclo)."&campus=".urlencode($Campus)."&carrera=".urlencode(utf8_encode($Carrera)). 
									 "&carreraInteres=".urlencode($C_Carrera).
									 "&banner=".urlencode($Banner)."&esAlumno=".urlencode($Alumno).
									 "&nivelInteres=".urlencode($L_Negocio)."&calidadRegistro=C".urlencode($Calidad)."&subNivelInteres=".urlencode($Pobla).
									 "&tipoRegistro=".urlencode($CP)."&fechaRellamada=".urlencode($date).
									 "&aPaternoTutor=".$aPaternoTutor.
									"&promedio=".urlencode($promedioF)."&porcentaje=".urlencode($porcentaje)."&costo1=".urlencode($costo1).
									"&costo2=".urlencode($costo2)."&planselec=".urlencode($anos_termino)."&tipocalculadora=";
			echo $url_completa."<br>";
			/*===================================================*/
			$ch = curl_init();
			curl_setopt($ch, CURLOPT_URL,$url_completa);
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
            curl_setopt($ch, CURLOPT_HEADER, 0);
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
            curl_setopt($ch, CURLOPT_HEADER, 0);
			curl_setopt($ch, CURLOPT_SSL_VERIFYPEER,FALSE);
			curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 2); 
           // curl_setopt($ch, CURLOPT_CAINFO, "C:\unitecssl.pem");
			
			$result = curl_exec($ch);
			#PARA ACTIVAR EL ENVIO DE DATOS AL CRM SE DEBE DESCOMENTAR ESTA LINEA
			$error = curl_error($ch);
			curl_close($ch);
		}			
	}	
	
	$arrResult = explode("|",$result);	


	
	echo $result;


	mysqli_close($conection);
}else{
	
	echo "Sin Carrera";
}
?>
