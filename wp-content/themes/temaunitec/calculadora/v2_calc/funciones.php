<?php
//header('Access-Control-Allow-Origin: *');
/*===================================================*/
/*===================================================*/
/*Obtenemos el nombre del campus completo
===================================================*/
/*===================================================*/   
function obtenerNomCampusLargo($nom_campus){
	$CampusF = "";
	switch($nom_campus){
		case  'Atizapan (Estado de Mexico)':
			$CampusF = 'Campus Atizapan';
		break;		
		case 'Cuitlahuac (Distito Federal)':
			$CampusF = 'Campus Cuitlahuac';
		break;	
		case 'Ecatepec (Estado de Mexico)':
			$CampusF = 'Campus Ecatepec';
		break;		
		case 'Marina (Distito Federal)':
			$CampusF = 'Campus Marina';
		break;		
		case 'Sur-Iztapalapa (Distito Federal)':
			$CampusF = 'Campus Sur';
		break;
	} 
	return $CampusF;
}
/*===================================================*/
/*===================================================*/
/*Obtenemos el nombre del campus corto, atizapan.
===================================================*/
/*===================================================*/ 
function obtenerNomCampusCorto($nom_campus){
	$Campus = "";
	switch($nom_campus){
		case  'Atizapan (Estado de Mexico)':
			$Campus = 'ATIZAP&Aacute;N';
		break;		
		case 'Cuitlahuac (Distito Federal)':
			$Campus = 'CUITL&Aacute;HUAC';		
		break;		
		case 'Ecatepec (Estado de Mexico)':
			$Campus = 'ECATEPEC';
		break;		
		case 'Marina (Distito Federal)':
			$Campus = 'MARINA';
		break;		
		case 'Sur-Iztapalapa (Distito Federal)':
			$Campus = 'SUR';
		break;
	}
	return $Campus;
}
/*===================================================*/
/*===================================================*/
/*Envio de Mailing x Mandrill
===================================================*/
/*===================================================*/   
function envioMailProspectoMandrill($email,$Nombre,$Pdescuento,$CostoSelec,$nom_carrera,$Campus,$promedioF,$planSelec,$isDIP,$tagMandrill){
	
	$costo1 =  $CostoSelec - (($CostoSelec * $Pdescuento)/100);
	$costo2 =  ($CostoSelec * 12) - ($costo1 * 12);

	$apiKEY = "03fc2cae-cb7b-440f-bba4-32ef677a82d9";	
	$dataMail = array(
		'key' => $apiKEY, 
		'template_name' => 'REGISTRO CALCULADORA UNITEC V2'.$isDIP,
		'template_content' => array(
				array(
				'name' => $Nombre.' una combinación a tu medida, para pagar tu colegiatura.'
				)
			),
		'message' => array(
				'subject' => $Nombre.' una combinación a tu medida, para pagar tu colegiatura.',
				'from_email' => 'atencion@mail.unitec.mx',
				'from_name' => 'UNITEC',
				'to' => array(
					array(
						'email' => $email,
						'name' => $Nombre						
					)
				),
				'track_opens' => true,
				'track_clicks' => true,
				'auto_text' => true,
				'url_strip_qs' => true,
				'merge_vars' => array(
					array(
						'rcpt' => $email, 
						'vars' => array(
							array(
								'name' => 'FNAME',
								'content' => $Nombre
							),
							array(
								'name' => 'CARRERA',
								'content' => $nom_carrera
							),
							array(
								'name' => 'CAMPUS',
								'content' => $Campus
							),
							array(
								'name' => 'PROMEDIO',
								'content' => $promedioF
							),
							array(
								'name' => 'PORCENTAJE',
								'content' => $Pdescuento
							),
							array(
								'name' => 'COSTO1',
								'content' => number_format(round($costo1),0,'.',',')
							),
							array(
								'name' => 'COSTO2',
								'content' => number_format(round($costo2),0,'.',',')
							),
							array(
								'name' => 'PLANS',
								'content' => $planSelec
							)
							
						)
					)
				),
				'tags' => array(
						$tagMandrill
					),
				'google_analytics_domains' => array(
					'.unitec.mx'
				),	
				'google_analytics_campaign' => array(
					'mailing'
				)
			)
		
	);                                                                    
	$data_string = json_encode($dataMail);                                                                                   
	$ch = curl_init();
	curl_setopt($ch, CURLOPT_URL, "https://mandrillapp.com/api/1.0/messages/send-template.json");
	curl_setopt($ch, CURLOPT_POST, true);
	curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string);
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
	curl_setopt($ch, CURLOPT_HEADER, 0);
	curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
	curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json','Content-Length:'.strlen($data_string)));
				
					
	$result = curl_exec($ch);
	$obj = json_decode($result);
	if(curl_errno($ch)){
		//echo 'Curl error: ' . curl_error($ch);
		//Correo no enviado
	}else{
		if($obj[0]->status=="error"){			
			$sql = "INSERT INTO tblemailmandrill (email,estatus,mensaje,fecha) VALUES ('".$email."','".$obj[0]->status."','".$obj[0]->message."',now())";
			@mysql_query($sql);
		}
		elseif($obj[0]->status=="rejected"){
			$sql = "INSERT INTO tblemailmandrill (email,estatus,mensaje,fecha) VALUES ('".$email."','".$obj[0]->status."','".$obj[0]->message."',now())";
			@mysql_query($sql);
		}
		else{
			$sql = "INSERT INTO tblemailmandrill (email,estatus,mensaje,fecha) VALUES ('".$email."','".$obj[0]->status."','".(isset($obj[0]->message) ? $obj[0]->message : "")."',now())";		
			@mysql_query($sql);
		}	
		return $result;	
	}
	curl_close($ch);
	
}
?>