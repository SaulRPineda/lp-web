<?php
/**
 * Storefront engine room
 *
 * @package storefront
 */

/**
 * Trim zeros in price decimals
 **/
global $pluginsNecesarios;
global $assetsTipoBanner;


/*Redirect a 404 en caso de no match en url*/
/**
 * @author    Unitec
 * @example   http://wpsites.net/wordpress-tips/redirect-404-page-not-found-to-any-url/
 * @copyright 2014 WP Sites
 */
function redirector($post_id) {
    if (is_single()) {
        $redirect_to = get_permalink($post_id);
        if (empty($_SERVER['HTTPS']))
            $ht_prot = 'http://';
        else
            $ht_prot = 'https://';

        $cur_link = $ht_prot . $_SERVER['SERVER_NAME'] . $_SERVER['REQUEST_URI'];
        $pattern = '/'.$post_id.'\/+$/';
        preg_match($pattern, $cur_link, $matches);

        if(sizeof($matches) < 1) :
            wp_redirect($redirect_to, 301);
            exit;
        endif;
    }
}
/*Redirect a 404 en caso de no match en url*/

add_filter('woocommerce_price_trim_zeros', '__return_true'); 					 //Quita los ceros de los precios
add_filter('woocommerce_show_variation_price',      function() { return TRUE;}); // Sirve para mostrar todos los precios incluso para una sola variacion IMPORTANTE NO QUITAR



/* remove add to cart class */
add_filter('add_to_cart_class', 'woo_custom_cart_button_class');
function woo_custom_cart_button_class()
{
	return __('more_info_button','woocommerce');
}

add_filter('woocommerce_get_price_html', 'unitec_producto_sales_price', 1, 2);
add_filter('woocommerce_cart_item_price', 'unitec_producto_sales_price', 1, 2);
function unitec_producto_sales_price($price, $product)
{


    // Variables initialisation
	$regular_price = $product->regular_price;
	$percentage = $product->sale_price; //Contiene el porcentaje
	$claseTachado = "";

	//$save_text = __('Beca del', 'woocommerce') . ' ';
	/*echo "Descuento: ".$woocommerce->cart->discount_total;*/
	
	if (!empty($percentage)) {
        // Percentage calculation
		/*$percentage = '<span class="save-percent"> ' . $save_text . round( ( ($regular_price - $sale_price) / $regular_price) * 100) . '%</span>';

		$price = '<del class="strike">' . woocommerce_price($regular_price) . '</del>
        <ins class="highlight">' . woocommerce_price($sale_price) . $percentage . '</ins>';*/

        $sale_price = round( ( ($regular_price * ( $percentage /100 ) ) - $regular_price ) * ( -1) );
        $claseTachado = "tachado";
	}
	else {
		//$sale_price = '<ins class="highlight">' . woocommerce_price($regular_price) . '</ins>';
		$percentage = 0;		
		$sale_price = round( $regular_price );
	}

	
	return "<section class='mt-1 miniSimulador'>
				<div class='row'>
					<div class='parametros col-6'>
                            <div class='row'>
                                <div class='margen-precio'>Precio actual:</div>
                                <div class='".$claseTachado." margen-cantidad'> ".woocommerce_price($regular_price)."</div>
                            </div>
                            <div class='row'>
                                <div class='margen-precio'>Beca hasta del:</div>
								<div class='descuento margen-cantidad'>". $percentage ." %</div>
							</div>
					</div>
					<div class='resultadoMS col-6 text-center'>
                            <span class='costoDesc'>".woocommerce_price($sale_price)."*</span>
                            <span class='descrip text-right'>Mensualmente</span>
                    </div>
				</div>
			</section>";

}

add_filter('woocommerce_variable_sale_price_html', 'adventure_tours_sales_min_max_prices', 20, 2);
function adventure_tours_sales_min_max_prices($price, $product)
{

    // Variables initialisation
	$variation_min_reg_price = $product->get_variation_regular_price('min', true);
	$variation_max_reg_price = $product->get_variation_regular_price('max', true);
	$variation_min_sale_price = $product->get_variation_sale_price('min', true);
	$variation_max_sale_price = $product->get_variation_sale_price('max', true);
	$percentage_min = '';
	$percentage_max = '';
	$save_text = __('Save', 'woocommerce') . ' ';

    // Percentage calculations
	if ($variation_min_reg_price != $variation_min_sale_price)
		$percentage_min = '<span class="save-percent-min"> (' . $save_text . round( ( ($variation_min_reg_price - $variation_min_sale_price) / $variation_min_reg_price) * 100) . '%)</span>';
	if ($variation_max_reg_price != $variation_max_sale_price)
		$percentage_max = '<span class="save-percent-max"> (' . $save_text . round( ( ($variation_max_reg_price - $variation_max_sale_price) / $variation_max_reg_price) * 100) . '%)</span>';

	if ( ($variation_min_reg_price != $variation_min_sale_price) || ($variation_max_reg_price != $variation_max_sale_price)) {
		$price = '<del class="strike">' . woocommerce_price($variation_min_reg_price) . '-' . woocommerce_price($variation_max_reg_price) . '</del>
        <ins class="highlight">' . woocommerce_price($variation_min_sale_price) . $percentage_min . ' - ' . woocommerce_price($variation_max_sale_price) . $percentage_max . '</ins>';
	}


	return "<section class='miniSimulador'>".$price."</section>"; 
}

/**
 * Output a list of variation attributes for use in the cart forms.
 *
 * @param array $args
 * @since 2.4.0
 */
function wc_dropdown_variation_attribute_options($args = array())
{
	$args = wp_parse_args(apply_filters('woocommerce_dropdown_variation_attribute_options_args', $args), array(
		'options' => false,
		'attribute' => false,
		'product' => false,
		'selected' => false,
		'name' => '',
		'id' => '',
		'class' => '',
		'show_option_none' => __('Choose an option', 'woocommerce'),
	));

	$options = $args['options'];
	$product = $args['product'];
	$attribute = $args['attribute'];
	$name = $args['name'] ? $args['name'] : 'attribute_' . sanitize_title($attribute);
	$id = $args['id'] ? $args['id'] : sanitize_title($attribute);
	$class = $args['class'];
	$show_option_none = $args['show_option_none'] ? true : false;
	$show_option_none_text = $args['show_option_none'] ? $args['show_option_none'] : __('Choose an option', 'woocommerce'); // We'll do our best to hide the placeholder, but we'll need to show something when resetting options.

	if (empty($options) && !empty($product) && !empty($attribute)) {
		$attributes = $product->get_variation_attributes();
		$options = $attributes[$attribute];
	}
	$html = '<div id="select_' . esc_attr($id) . '" class="select-custom"><span class="icon-u icon-u-seleciona"></span>';
	$html .= '<select id="' . esc_attr($id) . '" class="' . esc_attr($class) . '" name="' . esc_attr($name) . '" data-attribute_name="attribute_' . esc_attr(sanitize_title($attribute)) . '" data-show_option_none="' . ($show_option_none ? 'yes' : 'no') . '">';
	$html .= '<option value="">' . esc_html($show_option_none_text) . '</option>';

	if (!empty($options)) {
		if ($product && taxonomy_exists($attribute)) {
				// Get terms if this is a taxonomy - ordered. We need the names too.
			$terms = wc_get_product_terms($product->get_id(), $attribute, array('fields' => 'all'));

			foreach ($terms as $term) {
				if (in_array($term->slug, $options)) {
					$html .= '<option value="' . esc_attr($term->slug) . '" ' . selected(sanitize_title($args['selected']), $term->slug, false) . '>' . esc_html(apply_filters('woocommerce_variation_option_name', $term->name)) . '</option>';
				}
			}
		}
		else {
			foreach ($options as $option) {
					// This handles < 2.4.0 bw compatibility where text attributes were not sanitized.
				$selected = sanitize_title($args['selected']) === $args['selected'] ? selected($args['selected'], sanitize_title($option), false) : selected($args['selected'], $option, false);
				$html .= '<option value="' . esc_attr($option) . '" ' . $selected . '>' . esc_html(apply_filters('woocommerce_variation_option_name', $option)) . '</option>';
			}
		}
	}
	
	$labels='<label  for ="' . sanitize_title($args['label_sel']) . '">' . wc_attribute_label($args['label_sel']) . '</label>';
	$html .=  '</select></div>' . $labels;

	echo apply_filters('woocommerce_dropdown_variation_attribute_options_html', $html, $args);
}


function cw_change_product_price_display($price)
{
	//$price .= ' At Each Item Product';

	$html= '<section class="miniSimulador">
	testeando
                    <div class="row">
                        <div class="parametros col-6">
                            <div class="row">
                                <div class="col-8">Precio actual:</div>
                                <div class="tachado col-4 text-right"> $4,500</div>
                            </div>
                            <div class="row">
                                <div class="col-8">Beca hasta del:</div>
                                <div class="descuento col-4 text-right"> 25%</div>
                            </div>
                        </div>
                        <div class="resultadoMS col-6 text-center">
                            <span class="costoDesc">aqui el pirce'.$price.'*</span>
                            <span class="descrip text-right">Mensualmente</span>
                        </div>
                    </div>
                </section>';
	return $html;
}
// add_filter('woocommerce_get_price_html', 'cw_change_product_price_display');
//add_filter('woocommerce_cart_item_price', 'cw_change_product_price_display');


/**
 * Registra la taxonomia para plantilla mobile y desk, aqui se guardaran
 * las secciones que contiene cada post id
 * Afecta solo para que en la pagina solo cargen los custom fields de las secciones agregadas
 */

function woocommerce_variable_add_to_cart()
{
	global $product;

		// Enqueue variation scripts
	wp_enqueue_script('wc-add-to-cart-variation');

		// Get Available variations?
	$get_variations = sizeof($product->get_children()) <= apply_filters('woocommerce_ajax_variation_threshold', 30, $product);

		// Load the template
		//var_dump($product->get_available_variations());
	wc_get_template('single-product/add-to-cart/variable.php', array(
		'available_variations' => $get_variations ? $product->get_available_variations() : false,
		'attributes' => $product->get_variation_attributes(),
		'selected_attributes' => $product->get_default_attributes(),
	));
}
 
 class DHVCWooAdmin { //Habilidad para meter templates a categorias

	 public function __construct(){
		// add_action('admin_init', array($this, 'init'));
		 
		 //product category form
		 add_action( 'product_cat_add_form_fields', array( $this, 'add_category_fields' ) );
		 add_action( 'product_cat_edit_form_fields', array( $this, 'edit_category_fields' ) ,10,2);
		 add_action( 'created_term', array( $this, 'save_category_fields' ), 10,3 );
		 add_action( 'edit_term', array( $this, 'save_category_fields' ), 10,3);
		 
 // 		//product tag form
 // 		add_action( 'product_tag_add_form_fields', array( $this, 'add_tag_fields' ) );
 // 		add_action( 'product_tag_edit_form_fields', array( $this, 'edit_tag_fields' ) ,10,2);
 // 		add_action( 'created_term', array( $this, 'save_tag_fields' ) , 10,3);
 // 		add_action( 'edit_term', array( $this, 'save_tag_fields' ), 10,3);
		 
		 //product brand form
		 add_action( 'product_brand_add_form_fields', array( $this, 'add_brand_fields' ) );
		 add_action( 'product_brand_edit_form_fields', array( $this, 'edit_brand_fields' ) ,10,2);
		 add_action( 'created_term', array( $this, 'save_brand_fields' ), 10,3);
		 add_action( 'edit_term', array( $this, 'save_brand_fields' ), 10,3);
		 
 
		 add_action( 'woocommerce_settings_catalog_options_after',array(&$this,'settings_catalog_options_after'));
		 add_action('woocommerce_update_options_products',array(&$this,'update_options_products'));
		 
		 
	 } 

 
	 /**--------------woocommerce_admin_settings--------------*/
	 protected function _get_admin_settings(){
		 return  apply_filters( 'dhvc_woo_settings_fields', array(
			 array(
				 'name' => __( 'DHVC Woo Options', DHVC_WOO ),
				 'type' => 'title',
				 'id' => 'dhvc_woo' ),
			 array(
				 'name' 		=> __( 'Product Archive / Shop Page', DHVC_WOO ),
				 'desc' 		=> __( 'Use DHVC WOO Override Woocommerce Product Archive / Shop Page Options.', DHVC_WOO ),
				 'id' 		=> 'dhvc_woo_product_archives',
				 'type' 		=> 'checkbox',
			 ),
			 array(
				 'type' => 'sectionend',
				 'id' => 'dhvc_woo_override_product_archives'
			 ),
		 ) );
	 }
	 
	 
	 public function settings_catalog_options_after(){
		 woocommerce_admin_fields($this->_get_admin_settings());
	 }
	 
	 public function update_options_products(){
		 woocommerce_update_options($this->_get_admin_settings());
	 }
	 
	
	 
	 public function add_category_fields(){
	 ?>
	 <div class="form-field">
		 <label for="dhvc_woo_category_page_id"><?php _e( 'Page Template', DHVC_WOO ); ?></label>
		 <?php 
		 $args = array(
				 'name'=>'dhvc_woo_category_page_id',
				 'show_option_none'=>' ',
				 'echo'=>false,
		 );
		 echo str_replace(' id=', " data-placeholder='" . __( 'Select a page&hellip;',DHVC_WOO) .  "' class='enhanced chosen_select_nostd' id=", wp_dropdown_pages( $args ) );
		 
		 ?>
	 </div>
	 
	 <?php
	 }
	 
	 public function edit_category_fields( $term, $taxonomy ) {
		 $dhvc_woo_category_page_id = get_woocommerce_term_meta( $term->term_id, 'dhvc_woo_category_page_id', true );
	 ?>
	 <tr class="form-field">
		 <th scope="row" valign="top"><label><?php _e( 'Page Template', DHVC_WOO ); ?></label></th>
		 <td>
			 <?php 
			 $args = array(
					 'name'=>'dhvc_woo_category_page_id',
					 'show_option_none'=>' ',
					 'echo'=>false,
					 'selected'=>absint($dhvc_woo_category_page_id)
			 );
			 echo str_replace(' id=', " data-placeholder='" . __( 'Select a page&hellip;',DHVC_WOO) .  "' class='enhanced chosen_select_nostd' id=", wp_dropdown_pages( $args ) );
			 
			 ?>
		 </td>
	 </tr>
	 <?php
	 }
	 
	 public function save_category_fields( $term_id, $tt_id, $taxonomy ) {
		 if(!empty($_POST['dhvc_woo_category_page_id'])){
			 update_woocommerce_term_meta($term_id, 'dhvc_woo_category_page_id', absint( $_POST['dhvc_woo_category_page_id'] ) );
		 }else{
			 delete_woocommerce_term_meta($term_id,  'dhvc_woo_category_page_id');
		 }
	 }
	 
	 public function add_tag_fields(){
		 ?>
	 <div class="form-field">
		 <label for="dhvc_woo_tag_page_id"><?php _e( 'Page Template', DHVC_WOO ); ?></label>
		 <?php 
		 $args = array(
				 'name'=>'dhvc_woo_tag_page_id',
				 'show_option_none'=>' ',
				 'echo'=>false,
		 );
		 echo str_replace(' id=', " data-placeholder='" . __( 'Select a page&hellip;',DHVC_WOO) .  "' class='enhanced chosen_select_nostd' id=", wp_dropdown_pages( $args ) );
		 
		 ?>
	 </div>
	 
	 <?php
	 }
	 
	 public function edit_tag_fields( $term, $taxonomy ) {
		 $dhvc_woo_tag_page_id = get_woocommerce_term_meta( $term->term_id, 'dhvc_woo_tag_page_id', true );
	 ?>
	 <tr class="form-field">
		 <th scope="row" valign="top"><label><?php _e( 'Page Template', DHVC_WOO ); ?></label></th>
		 <td>
			 <?php 
			 $args = array(
					 'name'=>'dhvc_woo_tag_page_id',
					 'show_option_none'=>' ',
					 'echo'=>false,
					 'selected'=>absint($dhvc_woo_tag_page_id)
			 );
			 echo str_replace(' id=', " data-placeholder='" . __( 'Select a page&hellip;',DHVC_WOO) .  "' class='enhanced chosen_select_nostd' id=", wp_dropdown_pages( $args ) );
			 
			 ?>
		 </td>
	 </tr>
	 <?php
	 }
	 
	 public function save_tag_fields( $term_id, $tt_id, $taxonomy ) {
		 if(!empty($_POST['dhvc_woo_tag_page_id'])){
			 update_woocommerce_term_meta($term_id, 'dhvc_woo_tag_page_id', absint( $_POST['dhvc_woo_tag_page_id'] ) );
		 }else{
			 delete_woocommerce_term_meta($term_id,  'dhvc_woo_tag_page_id');
		 }
	 }
	 
 
	 public function add_brand_fields(){
		 ?>
	 <div class="form-field">
		 <label for="dhvc_woo_brand_page_id"><?php _e( 'Page Template', DHVC_WOO ); ?></label>
		 <?php 
		 $args = array(
				 'name'=>'dhvc_woo_brand_page_id',
				 'show_option_none'=>' ',
				 'echo'=>false,
		 );
		 echo str_replace(' id=', " data-placeholder='" . __( 'Select a page&hellip;',DHVC_WOO) .  "' class='enhanced chosen_select_nostd' id=", wp_dropdown_pages( $args ) );
		 
		 ?>
	 </div>
	 
	 <?php
	 }
	 
	 public function edit_brand_fields( $term, $taxonomy ) {
		 $dhvc_woo_brand_page_id = get_woocommerce_term_meta( $term->term_id, 'dhvc_woo_brand_page_id', true );
	 ?>
	 <tr class="form-field">
		 <th scope="row" valign="top"><label><?php _e( 'Page Template', DHVC_WOO ); ?></label></th>
		 <td>
			 <?php 
			 $args = array(
					 'name'=>'dhvc_woo_brand_page_id',
					 'show_option_none'=>' ',
					 'echo'=>false,
					 'selected'=>absint($dhvc_woo_brand_page_id)
			 );
			 echo str_replace(' id=', " data-placeholder='" . __( 'Select a page&hellip;',DHVC_WOO) .  "' class='enhanced chosen_select_nostd' id=", wp_dropdown_pages( $args ) );
			 
			 ?>
		 </td>
	 </tr>
	 <?php
	 }
	 
	 public function save_brand_fields( $term_id, $tt_id, $taxonomy ) {
		 if(!empty($_POST['dhvc_woo_brand_page_id'])){
			 update_woocommerce_term_meta($term_id, 'dhvc_woo_brand_page_id', absint( $_POST['dhvc_woo_brand_page_id'] ) );
		 }else{
			 delete_woocommerce_term_meta($term_id,  'dhvc_woo_brand_page_id');
		 }
	 }
	 
	 
	 
 }
 
 new DHVCWooAdmin();

//Termina implementacion de category 


add_theme_support( 'post-thumbnails' );
                       
function wpdocs_create_plantilla_tax() {
    register_taxonomy( 'plantillaMobile', 'book', array(
        'label'        => __( 'plantilla_mobile', 'textdomain' ),
        'rewrite'      => array( 'slug' => 'plantillaCustom' ),
        'hierarchical' => true,
    ) );

      register_taxonomy( 'plantillaDesk', 'book', array(
        'label'        => __( 'plantilla_desk', 'textdomain' ),
        'rewrite'      => array( 'slug' => 'plantillaCustom' ),
        'hierarchical' => true,
    ) );
}
add_action( 'init', 'wpdocs_create_plantilla_tax', 0 );


/**
 * Assign the Storefront version to a var
 */
$theme              = wp_get_theme( 'temaunitec' );
$storefront_version = $theme['Version'];


add_action( 'add_meta_boxes', 'product_details_add' );                                                      

add_action( 'save_post', 'product_details_save' );

function product_details_add() {
    add_meta_box( 'product_details', 'Product Details', 'product_details_call', 'product', 'normal', 'high' );
}

function product_details_call( $post ) {
    // Use nonce for verification
    wp_nonce_field( plugin_basename( __FILE__ ), 'product_details_noncename' ); 
    $field_value = get_post_meta( $post->ID, 'product_details_meta', false );
    wp_editor( $field_value[0], 'product_details_meta' );
}

/**
 * Agrega la funcionalidad de administrador de secciones
 * 
 */   
add_action('add_meta_boxes', 'add_page_meta_boxes');

function add_page_meta_boxes() {

	add_meta_box(
		'page_section_box',
		__('ADMNISTRADOR DE SECCIONES', 'unitec_theme'),
		'render_page_section_metabox',
		array("page", "product" ),
		'normal',
		'high');
}

function render_page_section_metabox($post) {

	require_once 'views/listado_de_secciones.php';

}

function load_custom_wp_admin_style() {

      	#wp_enqueue_script('jquery-validate', UNITEC_SCRIPTS_LIB_URL . '/jquery.validate.min.js', array('jquery'), '1.9.0', true);
      	#wp_enqueue_script('jquery-ytpplayer', UNITEC_SCRIPTS_LIB_URL . '/jquery.mb.YTPlayer.js', array('jquery'), '1.0', true);

      wp_register_style('unitec-admin-css', get_template_directory_uri().'/assets/admin/css/admin.min.css');
      wp_enqueue_style('unitec-admin-css');
      wp_enqueue_script('unitec-admin-js', get_template_directory_uri() . '/assets/admin/admin.js', array( 'jquery-ui-sortable', 'jquery-ui-draggable', 'jquery-ui-droppable' ), '1.0', true);
  

}

add_action( 'admin_enqueue_scripts', 'load_custom_wp_admin_style' );


/**
 * Funcion para guardar las secciones incluyendo el orden
 */   

add_action('wp_ajax_update-page-section-order',  'save_page_section_order');
function save_page_section_order() {

	$post_id = $_POST['page_id'];
	$nonce = $_POST['security'];
	$order = $_POST['order'];
	$page_sections = "";
	
	//agregar=1
	//borrar=0;
	if($_POST['add_or_del']=="add"){$add_or_del=="1";echo "Agredando<br>";}else{$add_or_del=="0";echo "Elmininando<br>";}


	/* Update even if empty to indicate that no page sections were selected to empty the page section bin */
	if($_POST['tipo']=='mob'){
		if (!wp_verify_nonce($nonce, 'page_section_' . $post_id))
			die('-1');

		if ($order!="") {
			$order = wp_parse_args($order);
			$order = $order['sectionm'];
			$page_sections = implode(',', array_values($order));
			echo "hola";
			foreach (array_values($order) as $key => $value) {
				$flightCategory[] = $value;
				//var_dump( $flightCategory );
				//var_dump($ji);
			}
			$ji=wp_set_object_terms( $post_id, $flightCategory, 'plantillaMobile',$add_or_del );
			unset($flightCategory);
			update_post_meta($post_id, '_page_section_order_fieldmobile', $page_sections);

			//buscar el id del termino en la tabla terminos con el nombre de la plantilla y despues borrarlo 


		}else{
			//borrar campo de post meta 
			delete_post_meta($post_id, '_page_section_order_fieldmobile');
			wp_set_object_terms( $post_id, null, 'plantillaMobile',$add_or_del );
		}

		
	}else{



		if (!wp_verify_nonce($nonce, 'page_section_' . $post_id))
		die('-1');

		if ($order!="") {
			echo "con orden <br>";
			$order = wp_parse_args($order);
			$order = $order['section'];
			$page_sections = implode(',', array_values($order));
			//var_dump(array_values($order));
			
			foreach (array_values($order) as $key => $value) {
				$flightCategory[] = $value;
				//var_dump( $flightCategory );
				
			}

			$ji=wp_set_object_terms( $post_id, $flightCategory, 'plantillaDesk',$add_or_del );
			unset($flightCategory);
			echo "------";
			var_dump($ji);


			update_post_meta($post_id, '_page_section_order_field', $page_sections);
		}else{
			echo "sin orden <br>";
			//borrar campo de post meta 
			delete_post_meta($post_id, '_page_section_order_field')."qqq";
			wp_set_object_terms( $post_id,null, 'plantillaDesk',$add_or_del );
		}

		
	}
	

}


/**
 * Set the content width based on the theme's design and stylesheet.
 */
if ( ! isset( $content_width ) ) {
	$content_width = 980; /* pixels */
}

$storefront = (object) array(
	'version' => $storefront_version,
	/**
	 * Initialize all the things.
	 */
	'main'       => require 'inc/class-storefront.php',
	'customizer' => require 'inc/customizer/class-storefront-customizer.php',
);

require 'inc/storefront-functions.php';
require 'inc/storefront-template-hooks.php';
require 'inc/storefront-template-functions.php';

if ( class_exists( 'Jetpack' ) ) {
	$storefront->jetpack = require 'inc/jetpack/class-storefront-jetpack.php';
}

if ( storefront_is_woocommerce_activated() ) {
	$storefront->woocommerce = require 'inc/woocommerce/class-storefront-woocommerce.php';

	require 'inc/woocommerce/storefront-woocommerce-template-hooks.php';
	require 'inc/woocommerce/storefront-woocommerce-template-functions.php';
}

if ( is_admin() ) {
	$storefront->admin = require 'inc/admin/class-storefront-admin.php';
}


/**
 * Implementacion para obtener las url se assets por dispositivo BY SRP
 * get_stylesheet_directory_uri()
 * TEMPLATEPATH No Funciona
 */

function getAssetsByDevice($tipo) {
	$carpeta = NULL;

	$dirWorpress = get_template_directory_uri();
	$tempDir = get_template_directory();

	$folderAngular = 'angularapp-v6';
	
	switch ( wp_is_mobile() ) {
		case 1: 
			$carpeta = "frontend"; 
		break;
		case 0: 
			$carpeta = "frontend_desktop"; 
			$folderAngular = 'angularapp_desk_v6';
		break;			
		default: $carpeta = "frontend"; break;
	}

	switch ($tipo) {

		/**
		  * Carga de assets para la funcion site_stylesheets BY SRP
		  */
		case 'uri_bootstrap_css':
			$url = $dirWorpress . '/assets/'.$carpeta.'/css/vendor/min/bootstrap.min.css';
		break;

		case 'uri_mdb_css':
			$url = $dirWorpress . '/assets/'.$carpeta.'/css/vendor/min/mdb.min.css';
		break;

		case 'uri_fontAwesome_css':
			$url = $dirWorpress . '/assets/'.$carpeta.'/font/generales/fa/css/font-awesome.min.css';
		break;
		/**
		  * End Carga de assets para la funcion site_stylesheets BY SRP
		  */


		/**
		 * Carga de assets para la funcion mdb_scripts BY SRP
		 */
		case 'uri_jquery311_js':
			$url = $dirWorpress.'/assets/'.$carpeta.'/js/vendor/min/jquery-3.1.1.min.js';
		break;

		case 'uri_jqueryui_js':
			$url = $dirWorpress.'/assets/'.$carpeta.'/js/vendor/min/jquery-ui.min.js';
		break;

		case 'uri_slideout_js':
			$url = $dirWorpress.'/assets/'.$carpeta.'/js/vendor/min/slideout.min.js';
		break;

		case 'uri_tether_js':
			$url = $dirWorpress.'/assets/'.$carpeta.'/js/vendor/min/tether.min.js';
		break;

		case 'uri_bootstrap_js':
			$url = $dirWorpress.'/assets/'.$carpeta.'/js/vendor/min/bootstrap.min.js';
		break;

		case 'uri_mdb_js':
			$url = $dirWorpress.'/assets/'.$carpeta.'/js/vendor/min/mdb.min.js';
		break;

		case 'uri_typeahead_js':
			$url = $dirWorpress.'/assets/'.$carpeta.'/js/vendor/min/typeahead.min.js';
		break;
		/**
		 * End Carga de assets para la funcion mdb_scripts BY SRP
		 */

		/**
		 * Carga de assets para la funcion angular BY SRP
		 */

		case 'uri_inline_js':
			// $url = $dirWorpress . '/assets/'.$carpeta.'/js/'.$folderAngular.'/inline.bundle.js';
			$url = $dirWorpress . '/assets/'.$carpeta.'/js/'.$folderAngular.'/runtime.js';
		break;

		case 'uri_polyfills_js':
			// $url = $dirWorpress . '/assets/'.$carpeta.'/js/'.$folderAngular.'/polyfills.bundle.js';
			$url = $dirWorpress . '/assets/'.$carpeta.'/js/'.$folderAngular.'/polyfills.js';
		break;

		case 'uri_vendor_js':
			// $url = $dirWorpress . '/assets/'.$carpeta.'/js/'.$folderAngular.'/vendor.bundle.js';
			$url = $dirWorpress . '/assets/'.$carpeta.'/js/'.$folderAngular.'/vendor.js';
		break;

		case 'uri_main_js':
			// $url = $dirWorpress . '/assets/'.$carpeta.'/js/'.$folderAngular.'/main.bundle.js';
			$url = $dirWorpress . '/assets/'.$carpeta.'/js/'.$folderAngular.'/main.js';
		break;

		case 'uri_formApp_js':
			$url = $dirWorpress . '/assets/'.$carpeta.'/js/'.$folderAngular.'/app/ngformApp.min.js';
		break;
		/**
		 * End Carga de assets para la funcion angular BY SRP
		 */

		/**
		 * Carga de assets Generados por el servidor BY SRP
		 */
		case 'uri_seccion_css':
			$url = $dirWorpress . '/assets/'.$carpeta.'/css/secciones/min/';
		break;

		case 'uri_concatenado_css':
			$url = $dirWorpress . '/assets/'.$carpeta.'/css/concatenados_servidor/';
		break;

		case 'uri_seccion_js':
			$url = $dirWorpress . '/assets/'.$carpeta.'/js/secciones/min/';
		break;

		case 'uri_concatenado_js':
			$url = $dirWorpress . '/assets/'.$carpeta.'/js/concatenados_servidor/';
		break;

		case 'path_seccion_css':
			$url = $tempDir ."/assets/".$carpeta."/css/secciones/min/";
		break;

		case 'path_concatenado_css':
			$url = $tempDir ."/assets/".$carpeta."/css/concatenados_servidor/";
		break;

		case 'path_generales_css':
			$url = $dirWorpress .'/assets/'.$carpeta.'/css/generales/min/generales_mobile_temaunitec.min.css';
		break;

		case 'path_seccion_js':
			$url = $tempDir ."/assets/".$carpeta."/js/secciones/min/";
		break;

		case 'path_concatenado_js':
			$url = $tempDir ."/assets/".$carpeta."/js/concatenados_servidor/";
		break;

		case 'path_generales_js':
			$url = $dirWorpress .'/assets/'.$carpeta.'/js/secciones/min/generales.min.js';
		break;

		case 'path_lazyLoad_uri':
			$url = $dirWorpress .'/assets/frontend/js/vendor/min/jquery.lazy.min.js';
		break;
		/**
		 * End Carga de assets Generados por el servidor BY SRP
		 */
		
		default:			
		break;
	}

	return $url;
}

/**
 * End Implementacion para obtener las url se assets por dispositivo BY SRP
 */


/*Implementacion Carga de estilos Generales y librerias*/

/*Load Estilos Generales del sitio*/
function site_stylesheets(){
	//wp_register_style('bootstrareboot',get_template_directory_uri() . '/assets/frontend/css/vendor/min/bootstrap-reboot.min.css', '', null, 'screen' );
	//wp_enqueue_style('bootstrareboot');

	$bootstrap = getAssetsByDevice('uri_bootstrap_css');
	$mdb = getAssetsByDevice('uri_mdb_css');
	//$fontAwesome = getAssetsByDevice('uri_fontAwesome_css');

	wp_register_style('bootstrapcss', $bootstrap, '', null, 'screen' );
	wp_enqueue_style('bootstrapcss');

	wp_register_style('mdb', $mdb, '', null, 'screen' );
	wp_enqueue_style('mdb');

	///wp_enqueue_style('font-awesome', $fontAwesome, '', null, 'screen');
    ///wp_enqueue_style('font-awesome');

	///wp_enqueue_style('material-icons', 'https://fonts.googleapis.com/icon?family=Material+Icons', '', null, 'screen');
    ///wp_enqueue_style('material-icons');


	//No esta minimizado...
	//wp_enqueue_style('icon-u', get_template_directory_uri() . '/assets/frontend/css/vendor/min/icon-u.min.css', '', null, true );
    //wp_enqueue_style('icon-u');
	//wp_register_style('generales_mobile_temaunitec',get_template_directory_uri() . '/assets/frontend/css/generales/min/generales_mobile_temaunitec.min.css', '', null, 'screen' );
    //wp_enqueue_style('generales_mobile_temaunitec');
}
add_action('get_header','site_stylesheets');
/*End Load Estilos Generales del sitio*/

/*Load Scripts Generales del sitio*/
function mdb_scripts()
{	
	//$jquery = getAssetsByDevice('uri_jquery311_js');
  	$jqueryui = getAssetsByDevice('uri_jqueryui_js');

  	$lazyLoad = getAssetsByDevice('path_lazyLoad_uri');

  	$slideout = getAssetsByDevice('uri_slideout_js');
  	$tether = getAssetsByDevice('uri_tether_js');
  	$bootstrapjs = getAssetsByDevice('uri_bootstrap_js');
  	$mdbjs = getAssetsByDevice('uri_mdb_js');
  	$typeahead = getAssetsByDevice('uri_typeahead_js');

	//wp_register_script('jquery-3.1.1', $jquery, '', null, true );
	//wp_enqueue_script('jquery-3.1.1');

	wp_register_script('jqueryUI', $jqueryui, '', null, true );
	wp_enqueue_script('jqueryUI');

	wp_register_script('lazyLoad', $lazyLoad, '', null, true );
	wp_enqueue_script('lazyLoad');

	wp_register_script('slideout', $slideout, '', null, true );
	wp_enqueue_script('slideout');

	wp_register_script('tether', $tether, '', null, true );
	wp_enqueue_script('tether');

	wp_register_script('bootstrapjs', $bootstrapjs, '', null, true );
	wp_enqueue_script('bootstrapjs');

	wp_register_script('mdb', $mdbjs, '', null, true );
	wp_enqueue_script('mdb');

	wp_register_script('typeahead', $typeahead, '', null, true );
	wp_enqueue_script('typeahead');

	/*Carga Assets Generales*/
	/*wp_register_script('script_generales',get_template_directory_uri() . '/assets/frontend/js/secciones/min/generales.min.js', '', null, true );
	wp_enqueue_script('script_generales');*/
}
add_action( 'wp_enqueue_scripts', 'mdb_scripts' );
/*End Load Scripts Plugins Generales del sitio*/



/*Desregistrar Servicios que no se necesitan*/

/*Desregistrar estilos*/
function deregister_styles() {
	wp_deregister_style('storefront-style');
	wp_deregister_style('storefront_styles');
	wp_deregister_style('storefront_woocommerce_styles');
	wp_deregister_style('storefront-woocommerce-style');
}
add_action( 'wp_print_styles', 'deregister_styles', 100 );
/*End Desregistrar estilos*/

/*Desregistrar Scripts de Woocomerce*/
function deregister_scripts() {

	wp_deregister_script ('jquery');
	wp_deregister_script( 'wc-header-cart' );
	wp_deregister_script( 'wc-add-to-cart' );
	wp_deregister_script( 'wc-cart-fragments' );
	wp_deregister_script( 'wc-photoswipe' );
	wp_deregister_script( 'wc-photoswipe-ui-default' );
	wp_deregister_script( 'wc-single-product' );
	wp_deregister_script( 'wp-embed' );
}
add_action( 'wp_print_scripts', 'deregister_scripts', 100 );
/*End Desregistrar Scripts de Woocomerce*/

/*Remove js Emoji de Wordpress*/
remove_action('wp_head', 'print_emoji_detection_script', 7);
remove_action('wp_print_styles', 'print_emoji_styles');
/*Remove js Emoji de Wordpress*/

/*Remove jquery zoom Woocomerce*/
function remove_image_zoom_support() {
    remove_theme_support( 'wc-product-gallery-zoom' );
    remove_theme_support( 'wc-product-gallery-lightbox' );
	remove_theme_support( 'wc-product-gallery-slider' );
}
add_action( 'after_setup_theme', 'remove_image_zoom_support', 100 );
/*End Remove jquery zoom Woocomerce*/

/*Remove Links Inutilizables Header BY SRP 20-08-2018*/
// Removes some links from the header
function remove_headlinks_and_emojis() {
    remove_action( 'wp_head', 'wp_generator' );
    remove_action( 'wp_head', 'rsd_link' );
    remove_action( 'wp_head', 'wlwmanifest_link' );
    remove_action( 'wp_head', 'start_post_rel_link' );
    remove_action( 'wp_head', 'index_rel_link' );
    remove_action( 'wp_head', 'wp_shortlink_wp_head' );
    remove_action( 'wp_head', 'adjacent_posts_rel_link' );
    remove_action( 'wp_head', 'adjacent_posts_rel_link_wp_head', 10, 0 );
    remove_action( 'wp_head', 'parent_post_rel_link' );
    remove_action( 'wp_head', 'feed_links', 2 );
    remove_action( 'wp_head', 'feed_links_extra', 3 );
    remove_action( 'wp_head', 'print_emoji_detection_script', 7 );
    remove_action( 'wp_print_styles', 'print_emoji_styles' );
    remove_filter( 'wp_mail', 'wp_staticize_emoji_for_email' );
    remove_filter( 'the_content_feed', 'wp_staticize_emoji' );
    remove_filter( 'comment_text_rss', 'wp_staticize_emoji' );
}
add_action( 'init', 'remove_headlinks_and_emojis' );
/*Remove Links Inutilizables Header BY SRP 20-08-2018*/

/*Carga defer*/
function add_defer_attribute($tag, $handle) {
	$page_idStyles = (string)get_the_ID();

   // agregar los handles de los scripts en el array
   $scripts_to_async = array(	//'jquery-3.1.1',
   								//'jqueryUI',
   								//'lazyLoad',
   								'slideout',
   								'tether',
   								'bootstrapjs',
   								'mdb',
   								//'typeahead',
   								//'inline',
   								//'polyfills',
   								//'main',
   								'mob_2_banner_home',
   								// 'formApp',
   								'places_library',
   								'mapaOperations',
   								'sec_specialList',
   								'flipcard',
   								$page_idStyles
   							);
    
   foreach($scripts_to_async as $async_script) {
      if ($async_script === $handle) {
         return str_replace(' src', 'defer src', $tag);
      }
   }
   return $tag;
}
add_filter('script_loader_tag', 'add_defer_attribute', 10, 2);
/*End Carga defer*/

/*Descomentar Cuando se realice la integración con VUE by SRP 17-09-2019*/
/*Carga async*/
function add_async_attribute($tag, $handle) {
	$page_idStyles = (string)get_the_ID();

   // agregar los handles de los scripts en el array
   $scripts_to_async = array(	
   								'formApp'
   							);
    
   foreach($scripts_to_async as $async_script) {
      if ($async_script === $handle) {
         return str_replace(' src', 'async src', $tag);
      }
   }
   return $tag;
}
//add_filter('script_loader_tag', 'add_async_attribute', 10, 2);
/*End Carga defer*/

/*Carga onload estilos */
function wpse_231597_style_loader_tag($tag, $handle){
$page_idStyles = (string)get_the_ID();
	$styles_to_async = array(	
		'bootstrapcss',
		'mdb',
		$page_idStyles
	);

	foreach($styles_to_async as $async_style) {
		if ($async_style === $handle) {
		   return preg_replace("/media='screen'/", "media='none' onload=\"if(media!='screen')media='screen'\"", $tag);
		}
	 }
    // $tag = preg_replace("/media='screen'/", "media='none' onload=\"if(media!='screen')media='screen'\"", $tag);

    return $tag;
}
add_filter('style_loader_tag', 'wpse_231597_style_loader_tag', 10, 2);
/*Carga onload estilos */

/*Registro de API Google Maps Necesario Para Todo el Sitio*/
// function my_acf_init() {	
// 	acf_update_setting('google_api_key', 'AIzaSyCbAkvXEGAqKRfdNqIuUtsPF4Fyl1at2Is');
// }
// add_action('acf/init', 'my_acf_init');


function load_places() {
	global $post;
	$miAPIKey = NULL;

	/*	
		Default APIKey
		Querétaro	=>	354
		León		=>	350
		Cuitláhuac	=>	342

		Primera APIKey
		Atizapan	=>	340
		Sur			=>	356
		Ecatepec	=>	219

		Segunda APIKey
		Guadalajara	=>	348	
		Los Reyes	=>	1057
		Marina		=>	352
		Toluca		=>	358

		En Línea	=>	346 (N/A)
	*/

	$defaultAPIKey = "AIzaSyCbAkvXEGAqKRfdNqIuUtsPF4Fyl1at2Is";
	$primeraAPIKey = "AIzaSyBirmleXzNnzOrj7yNYSsIen_k-SEcW-yk";
	$segundaAPIKey = "AIzaSyB8rc5Fp2F0TyXna7Y5ylBhusDesXq_UrI";

	switch ( $post->ID ) {
		case 340:
		case 356:
		case 219:
			//echo "Primera API Key";
			$miAPIKey = $primeraAPIKey;
		break;

		case 348:
		case 1057:
		case 352:
		case 358:
			$miAPIKey = $segundaAPIKey;
			//echo "Segunda API Key";
		break;
		
		default:
			//echo "API Key Default";
			$miAPIKey = $defaultAPIKey;
		break;
	}
	acf_update_setting('google_api_key', $miAPIKey);
	wp_register_script('places_library', '//maps.googleapis.com/maps/api/js?key='.$miAPIKey.'&libraries=places&language=es', '', null, true );
}
add_action( 'wp_enqueue_scripts', 'load_places' );
//add_action('acf/init', 'my_acf_init');

/*Implementación carga de scrtips y estilos separados por página*/

/*Registro de archivos y asociacion con la función*/
function registra_sheetStyles($nameStyle, $dir = "") {	

	if ($dir == NULL || $dir == "") {
		$dir = '0';
	}

	switch ($dir) {
		case '1':
			$tipo = getAssetsByDevice("uri_seccion_css");
			$mi_directorio = $tipo.$nameStyle.'.min.css';
		break;
		
		default:
			$tipo = getAssetsByDevice("uri_concatenado_css");
			$mi_directorio = $tipo.$nameStyle.'.min.css';
		break;
	}

	/*echo "<script>console.log('Estilos: ".$mi_directorio."');</script>";*/

	wp_register_style($nameStyle, $mi_directorio, '', null, 'screen' );

	/*wp_register_style($nameStyle, get_template_directory_uri() . '/assets/frontend/css/concatenados_servidor/'.$nameStyle.'.min.css', '', null, 'screen' );*/
}
/*add_action( 'wp_head', 'registra_sheetStyles' );*/
/*End Registro de archivos y asociacion con la función*/


/*Registro de archivos y asociacion con la función
 **** $dir = 1 Cuando es un script necesario para una seccion se obtiene del array pluginsNecesarios
 **** $dir = 0 Es el archivo concatenado generado por el servidor dependiendo en número de secciones que la conforman.
 **** Nota : No es necesario colocar el 0
****/
function registra_script($nameScript, $dir = ""){	

	if ($dir == NULL || $dir == "") {
		$dir = '0';
	}

	switch ($dir) {
		case '1':
			$tipo = getAssetsByDevice("uri_seccion_js");
			$mi_directorio = $tipo.$nameScript.'.min.js';
		break;
		
		default:
			$tipo = getAssetsByDevice("uri_concatenado_js");
			$mi_directorio = $tipo.$nameScript.'.min.js';
		break;
	}
	
	/*echo "<script>console.log('Estilos por Dispositivo: ".$mi_directorio."');</script>";*/

	wp_register_script($nameScript, $mi_directorio, '', null, true );
}
//add_action( 'wp_enqueue_scripts', 'registra_script' );
/*End Registro de archivos y asociacion con la función*/

function load_styles($pluginsNecesarios){	

	$page_id = get_the_ID(); 
	$urlPage = get_permalink($page_id);
	$seccionUnoCss = NULL;
  	$seccionDosCss = NULL;
  	$assetsCssInline = NULL;
  	$dirWorpress = get_template_directory_uri();
  	$tempDir = get_template_directory();

	/*Validar si la página es mobile*/
	if( wp_is_mobile() ) {
		$carpeta = "frontend";
	    $page_section_conf = "_page_section_order_fieldmobile"; 
	}	else {
		$carpeta = "frontend_desktop";
	    $page_section_conf = "_page_section_order_field";
	} 

	/*Nombre de los Archivos a Concatenar*/	
	$styleSheetFileName = $page_id .".min.css";

	/*Url de las hojas de los scripts y hojas estilos de las secciones*/	
	$styleSheetUrl = getAssetsByDevice("path_seccion_css");
	$dirWorpressFile = '/assets/'.$carpeta.'/css/secciones/min/';
	//echo "MI RUTA: ".$styleSheetUrl;

	/*Url del directorio donde se guardaran los scripts y las hojas de estilos por página*/	
	$pageStyleSheetUrl = getAssetsByDevice("path_concatenado_css");

	/*Obtener las secciones Por Página*/
	$page_sec_id = get_post_meta($page_id, $page_section_conf, true);
	$page_sec_id = explode(",", $page_sec_id);
  	$numero_secciones = count($page_sec_id);

  	/*Remove de la Palabra PHP BY SRP 09-08-2018*/
  	$seccionUnoCss = explode( ".php", $page_sec_id[0] );
  	$seccionDosCss = explode( ".php", $page_sec_id[1] );
  	/*Remove de la Palabra PHP*/

  	/*Obtenemos las secciones a descartar para colocarlas inline BY SRP 09-08-2018*/
  	$assetsCssInline = array(  str_replace( ".", "_", $seccionUnoCss[0] ), str_replace( ".", "_", $seccionDosCss[0] ) );
  	/*End Obtenemos las secciones a descartar para colocarlas inline BY SRP 09-08-2018*/

  	$styleGenerales = getAssetsByDevice("path_generales_css"); 	
  	$hoja = file_get_contents( $styleGenerales );

  	//print_r( $assetsCssInline);
  	//$hoja = "";
  	
  	/*Inicia la concatenación de assets por página*/
  	if($numero_secciones > 0) {
  		/*Concatenacón de hojas de estilo*/
  		foreach ($page_sec_id as $id_page_sec) {
  			//echo "ID_SECCIONES: ".$id_page_sec
        	$section_name_array = explode(".php", $id_page_sec);        	
        	try {
        		
        		/*Se verifica las secicones a concatenar el el archivo CSS BY SRP 08-09-2018*/
        		if( ! in_array( str_replace(".", "_", $section_name_array[0]) , $assetsCssInline) ) {
					//echo "TEMPORAL:".TEMPLATEPATH;
					//echo "No se Encuentra (La Incluimos) ! <br/>";
        			//echo "EXISTE: ".$section_name_array[0]." - ".file_exists ($styleSheetUrl.str_replace(".", "_", $section_name_array[0]).".min.css");

	          		if(file_exists ( $styleSheetUrl.str_replace(".", "_", $section_name_array[0]).".min.css" ) ) {
	          			//echo "S: ".$dirWorpress.$dirWorpressFile.str_replace(".", "_", $section_name_array[0]).".min.css";
	            		$hoja .= file_get_contents( $dirWorpress.$dirWorpressFile.str_replace(".", "_", $section_name_array[0]).".min.css" );
						//echo "existe";
					}
				} 
				/*Se concatena estilos Menu,Banner y Generales para inyectar Inline BY SRP 08-09-2018*/
				else {
					 //echo "SECCION: ".str_replace(".", "_", $section_name_array[0]);
					 //$archivoInline .= file_get_contents( $styleSheetUrl.str_replace(".", "_", $section_name_array[0]).".min.css" );
				}
        	} 	

        	catch (Exception  $ex) {
      			$hoja = "";
    		}
			$string_css =  $hoja;
			//echo "HOLA_INLINE: ".$string_css; 
			//die();
      	}
	 
      	header("Content-type: text/css");
   		file_put_contents($pageStyleSheetUrl.$styleSheetFileName, $string_css);
      	call_user_func('callbackConcatenados', $pageStyleSheetUrl, $styleSheetFileName);
      	/*Termina carga dinamica de hojas de estilos dinamica*/
  	}

  	registra_sheetStyles($page_id, '0');

  	/*Se obtiene el tipo de post y dependiendo el tipo se encolar los estilos necesarios, se mantiene este switch para futuras implementaciones en ciertas páginas*/
	switch ( get_post_type( $page_id ) ) {
		case 'page':
			/*Validamos si hay Script Necesarios precargados*/
			if ( count($pluginsNecesarios) > 0) {
				foreach ($pluginsNecesarios as $key => $plugin) {
					registra_sheetStyles($plugin, '1');
					wp_enqueue_style($plugin);
				}
			}
			/*End Validamos si hay Script Necesarios precargados*/
			wp_enqueue_style($page_id);
		break;

		case 'product':

			/*Validamos si hay Script Necesarios precargados*/
			if ( is_array($pluginsNecesarios) && count($pluginsNecesarios) > 0) {
				foreach ($pluginsNecesarios as $key => $plugin) {
					registra_sheetStyles($plugin, '1');
					wp_enqueue_style($plugin);
				}
			}
			/*End Validamos si hay Script Necesarios precargados*/
			wp_enqueue_style($page_id);
		break;
		
		default:
		break;
	}
}
add_action( 'get_header', 'load_styles' );


/*Descomentar Cuando se realice la integración con VUE by SRP 17-09-2019*/
/*Load Scripts de la aplicacion de angular para formularios*/
function angular_forms_app_scripts() 
{
	global $post;

	$formApp = getAssetsByDevice('uri_formApp_js');

	if( strpos($post->post_name,'biblioteca-virtual') !== false ){
	
		wp_register_script('formApp', $formApp, '', null, true );
		wp_enqueue_script('formApp');
	}
}
add_action( 'wp_footer', 'angular_forms_app_scripts' );
/*End Load Scripts de la aplicacion de angular para formularios*/






function load_script($pluginsNecesarios){

	$page_id = get_the_ID();
	$urlPage = get_permalink($page_id);
	$seccionUnoJs = NULL;
  	$seccionDosJs = NULL;
  	$assetsJsInline = NULL;
  	$dirWorpress = get_template_directory_uri();
  	$tempDir = get_template_directory();

	/*Validar si la página es mobile*/
	if( wp_is_mobile() ) {
	    $page_section_conf = "_page_section_order_fieldmobile";
	    $carpeta = "frontend";
	}	else {
	    $page_section_conf = "_page_section_order_field";
	    $carpeta = "frontend_desktop";
	} 

	/*Nombre de los Archivos a Concatenar*/
	$scriptFileName = $page_id .".min.js";	

	/*Url de las hojas de los scripts y hojas estilos de las secciones*/
	$scriptsSheetUrl = getAssetsByDevice("path_seccion_js");;	

	/*Url del directorio donde se guardaran los scripts y las hojas de estilos por página*/
	$pageScriptsUrl = getAssetsByDevice("path_concatenado_js");	

	/*Obtener las secciones*/
	$page_sec_id = get_post_meta($page_id, $page_section_conf, true);
	$page_sec_id = explode(",", $page_sec_id);
  	$numero_secciones = count($page_sec_id);

  	$dirWorpressFile = '/assets/'.$carpeta.'/js/secciones/min/';

  	/*Remove de la Palabra PHP BY SRP 09-08-2018*/
  	$seccionUnoJs = explode( ".php", $page_sec_id[0] );
  	$seccionDosJs = explode( ".php", $page_sec_id[1] );
  	/*Remove de la Palabra PHP*/

  	/*Obtenemos las secciones a descartar para colocarlas inline BY SRP 09-08-2018*/
  	$assetsJsInline = array(  str_replace( ".", "_", $seccionUnoJs[0] ), str_replace( ".", "_", $seccionDosJs[0] ) );

  	//$scriptGenerales = getAssetsByDevice("path_generales_js");
  	//$script = file_get_contents( $scriptGenerales );
  	$script = "";
  	/*Inicia la concatenación de assets por página*/
  	if($numero_secciones > 0) {
  		$scriptInline = NULL; 		
  		/*Concatenacón de scripts*/  		
      	foreach ($page_sec_id as $id_page_sec) {
        	$section_name_array = explode(".php", $id_page_sec);
        	/*echo str_replace(".", "_", $section_name_array[0])."<br>";*/
        	try {       

        		if( ! in_array( str_replace(".", "_", $section_name_array[0]) , $assetsJsInline) ) {
        			//echo "Ruta: ".$scriptsSheetUrl.str_replace(".", "_", $section_name_array[0]).".min.js";
        			if(file_exists ( $scriptsSheetUrl.str_replace(".", "_", $section_name_array[0]).".min.js" )) {
	            		$script .= file_get_contents( $dirWorpress.$dirWorpressFile.str_replace(".", "_", $section_name_array[0]).".min.js" );
	            		//echo "Archivo: ".$dirWorpress.$dirWorpressFile.str_replace(".", "_", $section_name_array[0]).".min.js";
	          		}
        		} 
				/*Impresion de Scripts Inline BY SRP 13-08-2018*/
        		else {

        			// //echo "Se Encuentra (No La Incluimos) ! <br/>";
					 //$scriptInline =  $scriptsSheetUrl.str_replace(".", "_", $section_name_array[0]).".min.js" ;

					// if( file_exists ( $scriptInline ) ) {
					//  	$nombreEnqueue = str_replace(".", "_", $section_name_array[0]);
					//  	wp_register_script($nombreEnqueue, $scriptInline, '', null, true);
					//  	wp_enqueue_script( $nombreEnqueue );

				 // 		wp_add_inline_script(
				 // 	        $nombreEnqueue, // nombre del estilo dependiente
				 // 	        file_get_contents( $scriptInline ) // String con las reglas CSS que se imprimirán inline
				 // 	    );
					//  }

        		}
        		/*End Impresion de Scripts Inline BY SRP 13-08-2018*/
          		
        	} 	catch (Exception  $ex) {
          			$script = "";
        		}
        	$string_script =  $script;
      	}

      	//echo "SCRIPTS:".$script;
      	
      	header("Content-type: text/javascript");  	
      	file_put_contents($pageScriptsUrl.$scriptFileName, $string_script);
      	call_user_func('callbackConcatenados', $pageScriptsUrl, $scriptFileName);

      	//sleep(2);      	
      	/*chmod($pageScriptsUrl.$scriptFileName, 0644);
      	header('Content-Type: text/html; charset=utf-8');*/
      	/*Termina carga dinamica de hojas de estilos dinamica*/
  	}

  	/*Se Registra el script por secciones*/
	registra_script($page_id, '0');
  	
  	/*Se obtiene el tipo de post y dependiendo el tipo se encolar los estilos necesarios, se mantiene este switch para futuras implementaciones en ciertas páginas*/
	switch ( get_post_type( $page_id ) ) {
		case 'page':

			/*Validamos si hay Script Necesarios precargados*/
			if ( is_array($pluginsNecesarios) && count($pluginsNecesarios) > 0) {
				foreach ($pluginsNecesarios as $key => $plugin) {

					switch ($plugin) {
						case 'places':
							wp_enqueue_script('places_library');
						break;

						case 'videoAmp':
							//wp_enqueue_script('videoAmp_library');
						break;
						
						default:
							registra_script($plugin, '1');
							wp_enqueue_script($plugin);
						break;
					}

				}
			}
			/*End Validamos si hay Script Necesarios precargados*/			

			/*Se encola el script por secciones*/			
			wp_enqueue_script($page_id);		
		break;

		case 'product':			
			/*Validamos si hay Script Necesarios precargados*/
			if ( count($pluginsNecesarios) > 0) {
				foreach ($pluginsNecesarios as $key => $plugin) {
					
					switch ($plugin) {
						case 'places':
							wp_enqueue_script('places_library');
						break;

						case 'videoAmp':
							//wp_enqueue_script('videoAmp_library');
						break;
						
						default:
							registra_script($plugin, '1');
							wp_enqueue_script($plugin);
						break;
					}

				}
			}
			/*End Validamos si hay Script Necesarios precargados*/

			/*Se encola el script por sessiones*/			
			wp_enqueue_script($page_id);			
		break;
		
		default:
		break;
	}
	
}
add_action( 'wp_enqueue_scripts', 'load_script' );

function callbackConcatenados($url, $archivo) {
	chmod($url.$archivo, 0644);
    header('Content-Type: text/html; charset=utf-8');
}

/*function banners()
{
    wp_register_script('gcs',get_template_directory_uri() . '/assets/frontend/js/secciones/mob_2_banners.js', '', null, true );
    wp_enqueue_script('gcs');
}
add_action( 'wp_enqueue_scripts', 'banners' );*/
//Termina scripts y estilos separados por secciones.

/*Implementación Generación de iconos*/
function icono_shortcode($atts) {
	$icono = NULL;

	$icono_atts = shortcode_atts( array(
		'nombre' => 'egresados',
		'clase' => '',
		'estilos' => '',
	), $atts );	

	switch ( esc_attr($icono_atts['nombre']) ) {
		case 'ver-opciones':
			$icono = '<i class="icon-u icon-u-'.esc_attr($icono_atts['nombre']).'" class="dropdown-toggle" id="puntos" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"></i>';
		break;

		case 'cerrar':
			$icono = '<i class="icon-u icon-u-'.esc_attr($icono_atts['nombre']).'" data-dismiss="modal"></i>';
		break;

		case 'maps':
			$icono = '<i class="icon-u icon-u-'.esc_attr($icono_atts['nombre']).'">';
			$icono .= '<span class="path1"></span><span class="path2"></span><span class="path3"></span><span class="path4"></span><span class="path5"></span><span class="path6"></span><span class="path7"></span><span class="path8"></span></i>';
		break;

		case 'uber':
		case 'alemania':
		case 'espana':
		case 'francia':
		case 'irlanda':
		case 'italia':
		case 'rusia':
			$icono = '<i class="icon-u icon-u-'.esc_attr($icono_atts['nombre']).'">';
			$icono .= '<span class="path1"></span><span class="path2"></span><span class="path3"></span></i>';
		break;

		case 'waze':
			$icono = '<i class="icon-u icon-u-'.esc_attr($icono_atts['nombre']).'">';
			$icono .= '<span class="path1"></span><span class="path2"></span><span class="path3"></span><span class="path4"></span><span class="path5"></span><span class="path6"></span><span class="path7"></span><span class="path8"></span><span class="path9"></span><span class="path10"></span><span class="path11"></span></i>';
		break;

		case 'correo-card':
		case 'facebook-card':
		case 'google-card':
		case 'mensaje-card':
		case 'microfono':
		case 'twitter-card':
		case 'whatsapp-card':
		case 'whatsapp-footer':
		case 'ubicacion-card':
		case 'llamar-card':
		case 'whatsapp-menu':
		case 'inglaterra':
		case 'japon':
		case 'suiza':
			$icono = '<i class="icon-u icon-u-'.esc_attr($icono_atts['nombre']).' '.esc_attr($icono_atts['clase']).'"'.esc_attr($icono_atts['estilo']).'>';
			$icono .= '<span class="path1"></span><span class="path2"></span></i>';
		break;

		case 'sms':
			$icono = '<i class="icon-u icon-u-'.esc_attr($icono_atts['nombre']).'">';
			$icono .= '<span class="path1"></span><span class="path2"></span><span class="path3"></span><span class="path4"></span><span class="path5"></span></i>';
		break;

		case 'horario-card':
		case 'argentina':
		case 'canada':
		case 'chile':
			$icono = '<i class="icon-u icon-u-'.esc_attr($icono_atts['nombre']).'">';
			$icono .= '<span class="path1"></span><span class="path2"></span><span class="path3"></span><span class="path4"></span></i>';
		break;

		case 'chat-card':
		case 'china':
		case 'estadosunidos':
		case 'corea-del-norte':
			$icono = '<i class="icon-u icon-u-'.esc_attr($icono_atts['nombre']).'">';
			$icono .= '<span class="path1"></span><span class="path2"></span><span class="path3"></span><span class="path4"></span><span class="path5"></span><span class="path6"></span></i>';
		break;

		case 'australia':
			$icono = '<i class="icon-u icon-u-'.esc_attr($icono_atts['nombre']).'">';
			$icono .= '<span class="path1"></span><span class="path2"></span><span class="path3"></span><span class="path4"></span><span class="path5"></span><span class="path6"></span><span class="path7"></span><span class="path8"></span><span class="path9"></span><span class="path10"></span><span class="path11"></span><span class="path12"></span></i>';
		break;

		case 'nuevazelandia':
			$icono = '<i class="icon-u icon-u-'.esc_attr($icono_atts['nombre']).'">';
			$icono .= '<span class="path1"></span><span class="path2"></span><span class="path3"></span><span class="path4"></span><span class="path5"></span><span class="path6"></span><span class="path7"></span><span class="path8"></span><span class="path9"></span></i>';
		break;
		
		case 'corea-del-sur':
			$icono = '<i class="icon-u icon-u-'.esc_attr($icono_atts['nombre']).'">';
			$icono .= '<span class="path1"></span><span class="path2"></span><span class="path3"></span><span class="path4"></span><span class="path5"></span><span class="path6"></span><span class="path7"></span><span class="path8"></span><span class="path9"></span><span class="path10"></span><span class="path11"></span><span class="path12"></span><span class="path13"></span><span class="path14"></span><span class="path15"></span><span class="path16"></span><span class="path17"></span><span class="path18"></span><span class="path19"></span><span class="path20"></span><span class="path21"></span></i>';
		break;
		
		default:
			$icono = '<i class="icon-u icon-u-'.esc_attr($icono_atts['nombre']).' '.esc_attr($icono_atts['clase']).'"'.esc_attr($icono_atts['estilo']).'></i>';
		break;
	}
	return $icono;
}
add_shortcode( 'icono', 'icono_shortcode' );
/*End Implementación Generación de iconos*/

/*Shortcode para agregar bullets a la seccion Prestigio, procesos y Admisión*/
function shortcode_bullet() {
	return '<i class="icon-u icon-u-bullet" style="color:#006fba; font-size:10px; margin-left:1rem;"></i>';
}
add_shortcode('bullet', 'shortcode_bullet');
/*End Shortcode para agregar bullets a la seccion Prestigio, procesos y Admisión*/

/*Implementación para mostrar Fechas*/
function fecha_shortcode($atts) {
 $icono = NULL;

 $icono_atts = shortcode_atts( array(
  'mes' => 'Agosto',
  'texto' => 'Próximo inicio de clases',
 ), $atts ); 

 return '|<hr style="margin-top:.5rem; margin-bottom:.5rem;"><div class="item-description inicio-clases">
                <h6>'.esc_attr($icono_atts['texto']).'</h6>
                <h4><b>'.esc_attr($icono_atts['mes']).'</b></h4>
            </div>';
}
add_shortcode( 'fecha', 'fecha_shortcode' );
/*Implementación para mostrar Fechas*/

/*Implementación share de iconos*/
function share_shortcode($atts) {
$pagina=urlencode($atts['pagina']);
$cerrar=$atts['cerrar'];
$seccion = $atts['seccion'];

	//var_dump($atts);
	$html= '		<div class="text-center h-100 d-flex flex-column align-items-center justify-content-around">
						<h4 class="card-title card-title-comparte w-100 fixed-top">Comparte<a class="rotate-btn" data-card="'.$cerrar.'">'.do_shortcode( '[icono nombre="'.sanitize_title("cerrar").'"][/icono]' ).'</a></h4>
						<ul class="inline-ul">
							<li>
								<a target="_blank" href="https://www.facebook.com/sharer/sharer.php?u='.$pagina.'" class="btn-floating btn-small btn-social-media" data-gtm-tr="SocNetShr" data-gtm-value="Facebook" data-gtm-article="'.$seccion.'">'
								.do_shortcode( '[icono nombre="'.sanitize_title("facebook-card").'" clase="btn-share"][/icono]' ).'
								</a>
							</li>
							<li>
								<a href="fb-messenger://share/?link='.$pagina.'" class="btn-floating btn-mapa-share btn-small btn-social-media" data-gtm-tr="SocNetShr" data-gtm-value="Facebook-msn" data-gtm-article="'.$seccion.'">'
								.do_shortcode( '[icono nombre="'.sanitize_title("fb-msn-card").'" clase="btn-share"][/icono]' ).'
							</a>
							</li>
							<li>
								<a target="_blank" href="https://wa.me/?text='.$pagina.'" class="btn-floating btn-small btn-social-media" data-gtm-tr="SocNetShr" data-gtm-value="Whatsapp" data-gtm-article="'.$seccion.'">'
								.do_shortcode( '[icono nombre="'.sanitize_title("whatsapp-card").'" clase="btn-share"][/icono]' ).'
								</a>
							</li>
							<li>
								<a href="mailto:?subject=Liga de interés&body=¡Hola!, Creo que la siguiente liga podría serte útil: '.$pagina.'" class="btn-floating btn-small btn-social-media" data-gtm-tr="SocNetShr" data-gtm-value="Email" data-gtm-article="'.$seccion.'">'
								.do_shortcode( '[icono nombre="'.sanitize_title("correo-card").'" clase="btn-share"][/icono]' ).'
								</a>
							</li>
						</ul>
					</div>';
	return $html;
}
add_shortcode( 'share', 'share_shortcode' );
/*End Implementación Generación de iconos*/

/*Implementación share de iconos Desktop*/
function desktop_share_shortcode($atts) {
$pagina = urlencode($atts['pagina']);
$cerrar = $atts['cerrar'];
$clase = $atts['clase'];
$tipo = $atts['tipo'];
$seccion = $atts['seccion'];

	// <a href=”” data-gtm-tr=”SocNetShr” data-gtm-value= “<<Social Network>>” data-gtm-article=” <<Article name>>”></a>
	//var_dump($atts);
	$html= '		<div class="text-left w-100">
	
                        <ul class="'.$tipo.'">
                            <li>
                            	<a target="_blank" href="https://www.facebook.com/sharer/sharer.php?u='.$pagina.'" data-gtm-tr="SocNetShr" data-gtm-value="Facebook" data-gtm-article="'.$seccion.'">'
								.do_shortcode( '[icono nombre="'.sanitize_title("facebook-card").'" clase="'.$clase.'"][/icono]' ).'
								</a>
                            </li>

							<li>
								<a target="_blank" href="https://twitter.com/share?url='.$pagina.'" data-gtm-tr="SocNetShr" data-gtm-value="Twitter" data-gtm-article="'.$seccion.'">'
								.do_shortcode( '[icono nombre="'.sanitize_title("twitter-card").'" clase="'.$clase.'"][/icono]' ).'
                            	</a>
                            </li>
							
                            <li>
                            	<a target="_blank" href="https://wa.me/?text='.$pagina.'" data-gtm-tr="SocNetShr" data-gtm-value="Whatsapp" data-gtm-article="'.$seccion.'">'.do_shortcode( '[icono nombre="'.sanitize_title("whatsapp-card").'" clase="'.$clase.'"][/icono]' ).'
			                    </a>
			                </li>

                            <li>
                            	<a href="mailto:?subject=Liga de interés&body=¡Hola!, Creo que la siguiente liga podría serte útil: '.$pagina.'" data-gtm-tr="SocNetShr" data-gtm-value="Email" data-gtm-article="'.$seccion.'">'
                             	.do_shortcode( '[icono nombre="'.sanitize_title("correo-card").'" clase="'.$clase.'"][/icono]' ).'
								</a>
                            </li>
                        </ul>

                    </div>';
	return $html;
}
add_shortcode( 'desktop_share', 'desktop_share_shortcode' );
/*End Implementación Generación de iconos Desktop*/

/*Obtener el post Title Para Armar textos genericos*/
function shorcutPageTitle(){
   return get_the_title();
}
add_shortcode( 'page_title', 'shorcutPageTitle' );
/*Obtener el post Title Para Armar textos genericos*/

/*Obtener BL de cada Post*/
function shorcutBLTitle(){
	$titleBL = get_the_title();
	$blTitle = explode(" ", $titleBL);
   	return $blTitle[0];
}
add_shortcode( 'BL_title', 'shorcutBLTitle' );
/*Obtener BL de cada Post*/

/* Funcion para ser llamada via ajax trae la imagen destacada por id(getAttachmentUrlByID)*/
function getAttachmentUrlByID()
{

	global $post;
	
	if($_POST['page_id'] == '') {
		echo json_encode(['img_url' => '']);
	}else {
		$page_id_p = wc_get_product_id_by_sku( $_POST['page_id'] );
		//$thumbID = get_post_thumbnail_id($page_id_p);
		//$imgDestacada = wp_get_attachment_url( $thumbID );
		$imgDestacada = get_field('imagen_destacada_horizontal', $page_id_p );
		echo json_encode(array('img_url' => $imgDestacada,'p_id' => $page_id_p ));
	}
	die(); 
}
add_action('wp_ajax_getAttachmentUrlByID', 'getAttachmentUrlByID');
add_action('wp_ajax_nopriv_getAttachmentUrlByID', 'getAttachmentUrlByID');
/* Termina getAttachmentUrlByID*/

function getCampusBySlug()
{

	global $post;
	
	if($_POST['campus_name'] == '') {
		echo json_encode(['img_url' => '']);
	}else {
		$args = array(
			'title'  => $_POST['campus_name'],
			'post_type'   => 'any',
			'post_status' => 'publish',
			'numberposts' => 1
		);
		$my_posts = get_posts($args);
		$campus = $my_posts[0];
		$thumbID = get_field('imagen_destacada_horizontal', $campus->ID);
		$urlImg = $thumbID["url"];
		$altImg = $thumbID["alt"];
		$urlPage = get_permalink($campus->ID);
		// $my_posts = get_term_by('slug','campus','product_cat');
		// if( $my_posts )
		//     echo 'ID on the first post found ' . $my_posts[0]->ID;
		// print_r($thumbID);
		// print_r($urlPage);

		$data_array = ["name_campus" => $_POST['campus_name'], "imagen-grande" => $urlImg, "imagen-descripcion" => $altImg, "url-campus" => $urlPage];
		echo json_encode($data_array);
    
	}
	die(); 
}
add_action('wp_ajax_getCampusBySlug', 'getCampusBySlug');
add_action('wp_ajax_nopriv_getCampusBySlug', 'getCampusBySlug');

if (function_exists('acf_add_options_page')) {

    acf_add_options_page(array(
        'page_title' => 'Opciones de Schemas',
        'menu_title' => 'Opciones de Schemas',
        'menu_slug'  => 'schema-options',
        'capability' => 'edit_posts',
        'redirect'   => false
    ));

}
add_action('wp_head', function() {
	$pageId = get_the_ID();
  $type = get_post_type( $pageId );    
  switch ($type) {  	
  	case 'product':
  		global $product;
			$schemaCourse = array(
        '@context'  => "http://schema.org",
        '@type'     => "Course",
        'audience' => array(
        	'@type'=> 'EducationalAudience',
    	  	'audienceType' => 'estudiantes' )
			);  					

			if ($prod_code = $product->get_sku()) {
				$schemaCourse['courseCode'] = $code;				
			}
			if ($prod_desc = get_field('descripcion_del_producto')) {
				$schemaCourse['description'] = $prod_desc;				
			}
			if ($prod_desc = get_field('descripcion_del_producto')) {
				$schemaCourse['description'] = $prod_desc;				
			}			
			$schemaCourse['hasCourseInstance']=array("@type"=>'CourseInstance');
			if ($prod_mod = $product->get_attribute( 'modalidades' )) {
				$schemaCourse['hasCourseInstance']['courseMode'] = $prod_mod;
			}
			if ($prod_desc = get_field('descripcion_del_producto')) {
				$schemaCourse['hasCourseInstance']['description'] = $prod_desc;		
			}

			$schemaCourse['hasCourseInstance']['eventStatus']= "EventScheduled";

			if ($prod_img = get_field('imagen_destacada_horizontal')) {
				$schemaCourse['hasCourseInstance']['image'] = array('@type'=>'ImageObject',
																														'height' => 480,
																														'width' => 800,
																														'url' => $prod_img['url']
																													);
			}
			if ($prod_loc = $product->get_attribute( 'campus' )) {
				$campusdisp = explode(',', $prod_loc);
				foreach ($campusdisp as $camp) {
					$slug_camp = strtolower(remove_accents($camp));
					$page = get_page_by_path( 'campus-'.$slug_camp );
					if ( $page ) {
		      	$pId =  $page->ID;
		      	$loc_add = get_field('direccion_del_campus',$pId);
		      	if($loc_add && strlen($loc_add)>1){			      		
			      	$schemaCourse['hasCourseInstance']['location'][] = array('@type'=>'Place',
																														'address' => $loc_add,
																														'name' => "Campus ".$camp);	
		      	}			      	
		    	}
				}
			}	
			if ($prod_name = $product->get_attribute( 'nombre-de-la-carrera' )) {
				$schemaCourse['hasCourseInstance']['name'] = $prod_name;
			}		
			$schemaCourse['hasCourseInstance']['startDate'] = date('Y');			
			$schemaCourse['hasCourseInstance']['url'] = get_permalink( $product->ID );
			$schemaCourse['name'] = $prod_name;			
			if ($companyName) {
				$schemaCollege['name'] = $companyName;
			}
			$schemaCourse['provider'] = array('@type'=>'Organization',
																															'logo' => get_field('company_logo', 'option'),
																															'name' => get_field('name', 'option'));	

			echo '<script type="application/ld+json">' . json_encode($schemaCourse) . '</script>';

			$schemaBread = array(
	      '@context'  => "http://schema.org",
	      '@type'     => "BreadcrumbList",
	      'itemListElement' => array(array('@type' => 'ListItem', 
															'item'=> array('@id' =>site_url(),
																							'name'=> 'UNITEC'
																							 ),
															 'position' => 1															
															))
															);
			$parent = get_term_by('name', 'Oferta Educativa', 'product_cat');
			$cat = get_terms( array(
    		'taxonomy' => 'product_cat',
    		'parent' => $parent->term_id
			));
			foreach ($cat as $c) {				
				if ($c->name == 'Preparatoria' && has_term($c->term_id, 'product_cat' ) ) {
					$cluster = array('@id' =>site_url().'/prepa',
																							'name'=> 'UNITEC Preparatoria'
																							 );
				}else if($c->name == 'Licenciaturas' && has_term( $c->term_id, 'product_cat' )){
					$cluster = array('@id' =>site_url().'/licenciaturas',
																								'name'=> 'UNITEC Licenciaturas'
																								 );
				}else if($c->name == 'Ingenierías' && has_term(  $c->term_id, 'product_cat' )){
					$cluster = array('@id' =>site_url().'/ingenierias',
																								'name'=> 'UNITEC Ingenierías'
																								 );
				}else if($c->name == 'Diplomados' && has_term($c->term_id, 'product_cat' )){
					$cluster = array('@id' =>site_url().'/diplomados',
																								'name'=> 'UNITEC Diplomados'
																								 );
				}else if($c->name == 'Maestrías' && has_term($c->term_id, 'product_cat' )){
					$cluster = array('@id' =>site_url().'/maestrias',
																								'name'=> 'UNITEC Maestrías'
																								 );
				}
			}			
			if ($cluster) {
				$producto = array('@type' => 'ListItem', 
															'item'=> array('@id' =>get_permalink( $product->ID ),
																							'name'=> $prod_name
																							 ),
															 'position' => 3);
				$schemaBread['itemListElement'][] = array('@type' => 'ListItem',
																									'item'=>$cluster,
																									'position' => 2);
				$schemaBread['itemListElement'][] = $producto;				
			}	else{
				$producto = array('@type' => 'ListItem', 
															'item'=> array('@id' =>get_permalink( $product->ID ),
																							'name'=> $prod_name
																							 ),
															 'position' => 2);
				$schemaBread['itemListElement'][] = $producto;
			}	
			echo '<script type="application/ld+json">' . json_encode($schemaBread) . '</script>';

		break;
  	
  	default:    		
    break;
  }
  if ($companyName = get_field('name', 'option') ){  	
    $schemaCollege = array(
      '@context'  => "http://schema.org",
      '@type'     => "CollegeOrUniversity"
		);

		if ($award = get_field('award', 'option')) {
			$schemaCollege['award'] = $award;
		}
		$contact_type = get_field('contact_point_contact_type', 'option');
		$cp_email = get_field('contact_point_email', 'option'); 
		$cp_telephone = get_field('contact_point_telephone', 'option');
		$cp_area = get_field('contact_point_area_served', 'option');

		if ( $contact_type && ($cp_email || $cp_telephone || $cp_area)){
	        
      $schemaCollege['contactPoint'][] = array("@type"=>'ContactPoint');	       
      if ($cp_area) {
      	$schemaCollege['contactPoint'][0]['areaServed'] = $cp_area;	
      }
      		      
      if (have_rows('available_language', 'option')) {	 
      	$altname =array();
      	$name =array();
      	while (have_rows('available_language', 'option')) : the_row();			           		            
          while (have_rows('available_language_language', 'option')) : the_row();			           		            
            $altname[] = get_sub_field('language_alternate_name');
            $name[] = get_sub_field('language_name');
        	endwhile;	
          $availableLanguage = array(
              '@type'       => 'Language',
              'alternateName' => $altname,
              'name' => $name
          );
      		$schemaCollege['contactPoint'][0]['availableLanguage'] = $availableLanguage;	
      	endwhile;      	        	
      }
      
      if ($contact_type) {
      	$schemaCollege['contactPoint'][0]['contactType'] = $contact_type;
      }
      if ($cp_email) {
      	$schemaCollege['contactPoint'][0]['email'] = $cp_email;
      }      	
    	if ($cp_telephone) {
    		$schemaCollege['contactPoint'][0]['telephone'] = '+52 '.$cp_telephone;
    	}			      				            
		}
  		
		if ($desc = get_field('college_description', 'option')) {
			$schemaCollege['description'] = $desc;
		}
		if ($coll_email = get_field('email', 'option')) {
			$schemaCollege['email'] = $coll_email;
		}	
		if ($founder = get_field('founder', 'option')) {
			$schemaCollege['founder'] = $founder;
		}
		if ($found_date = get_field('founding_date', 'option')) {
			$schemaCollege['foundingDate'] = $found_date;
		}
		if ($found_loc = get_field('founding_location', 'option')) {
			$schemaCollege['foundingLocation'] = array('@type' => 'Place', 'name' => $found_loc );
		}
		if ($logo = get_field('company_logo', 'option')) {
			$schemaCollege['logo'] = array('@type' => 'ImageObject', 
															'height'=> 100,
															'width' => 350,
															'url'=> $logo);
			$worksFor['logo'] = array('@type' => 'ImageObject', 
															'height'=> 100,
															'width' => 350,
															'url'=> $logo);
		}
		if ($companyName) {
			$schemaCollege['name'] = $companyName;
			$worksFor['name'] = $companyName;
		}
		if ($employees = get_field('number_of_employees', 'option')) {
			$schemaCollege['numberOfEmployees'] = $employees;
		}
		if (have_rows('social_media', 'option')) {
      $schemaCollege['sameAs'] = array();
      while (have_rows('social_media', 'option')) : the_row();
        array_push($schemaCollege['sameAs'], get_sub_field('url'));
      endwhile;
    }
    if ($telephone = get_field('company_phone', 'option')) {
			$schemaCollege['telephone'] = $telephone;
		}
		if ($c_url = get_field('company_url', 'option')) {
			$schemaCollege['url'] = $c_url;
			$worksFor['url'] = $c_url;
		}  		
		echo '<script type="application/ld+json">' . json_encode($schemaCollege) . '</script>';
	}

	$schemaPerson = array(
	  '@context'  => "http://schema.org",
	  '@type'     => "Person",
	  "email"=> $cp_email,
	  "jobTitle" => 'Atención al estudiante',
	  "name" => 'Conócenos'
	);
	if ($cp_telephone) {
		$schemaPerson['telephone'] = $cp_telephone;
	}
	$worksFor['@type'] = 'CollegeOrUniversity';
	$schemaPerson['worksFor'] = $worksFor;
	echo '<script type="application/ld+json">' . json_encode($schemaPerson) . '</script>';
});

function abs_CuentaValid()
{

	global $post;
	
	$cuenta = $_POST['nocuenta'];
	if($cuenta !== '') {
		$abs_db =  new wpdb( 'cr3LeadS', 'ou&5tyi/ki+|oi#6.- ', 'crm_registros', 'localhost' );
		//$abs_db =  new wpdb( 'cr3LeadS', 'ou&5tyi/ki+|oi#6.- ', 'crm_registros', '104.130.101.192' );
		$exists= $abs_db->get_row(
	"
	SELECT no_cuenta,nombre_alumno,correo_personal,correo_myunitec,ultimaActualizacion
	FROM absorcion_actualizacion_datos
	WHERE no_cuenta = $cuenta
	"
);		
		if ($exists) {
			if ($exists->ultimaActualizacion == null) {
				$data_array = ["success"=>true,
										"data"=>array("cuenta"=>$exists->no_cuenta,
																	"nombre"=>$exists->nombre_alumno,	
																	"correo"=>$exists->correo_personal,
																	"myunitec"=>$exists->correo_myunitec)];
			}else{
				$data_array = ["success"=>false,
										"data"=>array("msg"=>"Tus datos ya fueron actualizados anteriormente.")];
			}			
		}else{
			$data_array = ["success"=>false,
										"data"=>array("msg"=>"Número de cuenta no encontrado.")];	
		}		
	}else {
		
		$data_array = ["success"=>false,
										"data"=>array("msg"=>"Número de cuenta no válido.")];	
  }
  echo json_encode($data_array);
	die(); 
}
add_action('wp_ajax_abs_CuentaValid', 'abs_CuentaValid');
add_action('wp_ajax_nopriv_abs_CuentaValid', 'abs_CuentaValid');

function abs_updt()
{
	
	global $post;
	date_default_timezone_set('America/Mexico_City');
	$cuenta = $_POST['nocuenta'];
	$mail = $_POST['email'];
	$cel = $_POST['cel'];
	if($cuenta !== '' && $mail !== '' && $cel !== '') {
		$abs_db =  new wpdb( 'cr3LeadS', 'ou&5tyi/ki+|oi#6.- ', 'crm_registros', 'localhost' );
		//$abs_db =  new wpdb( 'cr3LeadS', 'ou&5tyi/ki+|oi#6.- ', 'crm_registros', '104.130.101.192' );
		$upreturn = $abs_db->query( $abs_db->prepare( 
			"
				UPDATE absorcion_actualizacion_datos
				SET correo_actualizado = %s , celular_actualizado = %s, ultimaActualizacion = %s
				where no_cuenta = $cuenta
			", 
		  $mail, 
			$cel, 
			date('Y-m-d H:i:s')
		) );
		if ($upreturn) {
			$data_array = ["success"=>true,
										"data"=>array()];
		}else{
			$data_array = ["success"=>false];	
		}		
	}else {
		
		$data_array = ["success"=>false];	
  }
  echo json_encode($data_array);
	die(); 
}
add_action('wp_ajax_abs_updt', 'abs_updt');
add_action('wp_ajax_nopriv_abs_updt', 'abs_updt');

function getHubspotContact($email,$properties=array(),$propMode="value_only",$submMode="none",$showList="false"){	
	$prop = "";
	$HAPIKey = 'a02f7942-c782-4a98-9bf2-1e8ede4fca3b';
	if($properties){
		$prefixed_array = preg_filter('/^/', '&property=', $properties);		
		$prop = implode("", $prefixed_array);
	}
	$endpoint = 'https://api.hubapi.com/contacts/v1/contact/email/'.$email.'/profile?hapikey='.$HAPIKey.$prop."&propertyMode=".$propMode."&formSubmissionMode=".$submMode."&showListMemberships=".$showList;		
  $ch = curl_init();
  curl_setopt($ch, CURLOPT_URL, $endpoint);
  curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
  curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
  //curl_setopt($ch, CURLOPT_USERAGENT, $this->userAgent);    // new
  $output = curl_exec($ch);
  $errno = curl_errno($ch);
  $error = curl_error($ch);
  $http_status = curl_getinfo($ch, CURLINFO_HTTP_CODE);
  //$this->setLastStatusFromCurl($ch);
  curl_close($ch);
//var_dump($output);
  if ( $errno > 0 ){
      //throw new HubSpot_Exception('cURL error: ' . $error);  	
  	return false;
  }else{
  	if ($http_status==200) {
  		return $output;
  	}else{
  		return false;
  	}
	}	
  
}
function CitaSend(){	
//@ini_set( 'display_errors', 1 );
	date_default_timezone_set('America/Mexico_City');
	$root = realpath($_SERVER["DOCUMENT_ROOT"]);
	require "$root/procWebLeads/assets/nusoap/nusoap.php";
	//$email = "lmontesl@mail.unitec.mx";
	$email = $_REQUEST['email'];
	//$publicContactURL = "https://app.hubspot.com/contacts/49484/contact/239919537";
	$publicContactURL = $_REQUEST['URL'];	
	$fecha = $_REQUEST['fecha'];	
	$hora = $_REQUEST['hora'].":00";
	$cita = $fecha." ".$hora;
	//$email=$_REQUEST['email'];
	$nombre=$_REQUEST['nombre'];
	$lastname=$_REQUEST['lastname'];
	$noPersona=$_REQUEST['vid'];
	$campus=$_REQUEST['campus'];
//echo true; 
	$date=explode('/',$fecha);
	$dd=$date[0];
	$mm=$date[1];
	$yy=$date[2];
	$cita_bd=$yy."-".$mm."-".$dd. " ".$hora;
	//envio HUBSPOT
	$fecha_hs = strtotime($yy."/".$mm."/".$dd.' 00:00:00 UTC'); // Convierte fecha en UNIX	
	$fecha_hs .= "000"; // Fix HubSpot
	$idFormHubspot ='5292afc7-4594-488f-985a-70dc9af8f671';
	$portalId = '49484';	
	$urlSent = 'https://forms.hubspot.com/uploads/form/v2/'.$portalId.'/'.$idFormHubspot;
	$cita_txt = $fecha." a las ".$hora." hrs. en campus ".$campus;
	$str_post = "firstname=".urlencode($nombre).
				"&lastname=".urlencode($lastname).
				"&email=".urlencode($email).
				"&citaweb=".urlencode($cita_txt).
				"&cita_fecha=".urlencode($fecha_hs).
				"&citaonline=agendada";
	if (isset($_REQUEST['fuente'])) {
		$fuente = trim($_REQUEST['fuente']);
		$str_post.= "&co_fuente=".$fuente;
	}				
	$ch = curl_init();
	curl_setopt($ch, CURLOPT_POST, true);
	curl_setopt($ch, CURLOPT_POSTFIELDS, $str_post);
	curl_setopt($ch, CURLOPT_URL, $urlSent);
	curl_setopt($ch, CURLOPT_HTTPHEADER, array('application/x-www-form-urlencoded'));
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);	
	curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);	
	$response = curl_exec($ch);  //Log the response from HubSpot as needed.
	$status_code = @curl_getinfo($ch, CURLINFO_HTTP_CODE); //Log the response status code
	curl_close ($ch);	
//envio CRM	
	//$log_db =  new wpdb( 'cr3LeadS', 'ou&5tyi/ki+|oi#6.- ', 'crm_registros', 'localhost' );//15/072019
	$cliente = new nusoap_client("http://199.244.82.97/Hubspot/CRMEntHubspot.svc?wsdl",true);
	$cliente->soap_defencoding = 'UTF-8';
  	$error = $cliente->getError();
  	$parametros['emailContacto'] = $email;
	$parametros['PublicContactURL'] = $publicContactURL;  
	$parametros['FechaProgramacion'] = $cita;
  if ($error) {
  	$EnvioCRM = 0;
  }else{  	
		$result = $cliente->call("InsertCRM", array('parameters' => $parametros));
		if ($cliente->fault) {	      
	  	$EnvioCRM = 0;
	  }
	  else {
	      $error = $cliente->getError();
	      if ($error) {
	          //error en request	      	
	      	$EnvioCRM = 0;
	      }
	      else {
	        $EnvioCRM = 1;	        
	      }
	  }		
  }
  /*
  $logreturn = $log_db->query( $log_db->prepare( 
			"
				INSERT INTO log_citas_online
				(FirstName, LastName, EmailCita, PublicURL, FechaCitaCRM, FechaCita, HoraCita, WSResponse, CRMResponse, FechaRequest,EnvioCRM,CampusCita,HSReturnCode)
				VALUES (%s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %d, %s, %s)
			", 
		  $nombre, 
			$lastname, 
			$email, 
			$publicContactURL, 
			$cita, 
			$fecha, 
			$hora, 
			$cliente->response, 
			$result['InsertCRMResult'], 
			date('Y-m-d H:i:s'),$EnvioCRM,
			$campus,
			$status_code
		) );
		if ($logreturn) {
			$data_array = ["success"=>true,
										"data"=>array()];
		}else{
			$data_array = ["success"=>false];	
		}*/
		$data_array = ["success"=>true,"data"=>array("test"=>"test")];
		echo json_encode($data_array,true);
	die(); 
}
add_action('wp_ajax_CitaSend', 'CitaSend');
add_action('wp_ajax_nopriv_CitaSend', 'CitaSend');

function chat_rules()
{		
	$horarios = obtenerQueueParams();					
	$horariosq1 = obtenerQueueParams(1);
	$horariosq2 = obtenerQueueParams(2);
	$horariosq3 = obtenerQueueParams(3);
	$horariosq4 = obtenerQueueParams(4);

	$data_array = ["success"=>true,"data"=>["general"=>$horarios["Horarios"],"1"=>$horariosq1,"2"=>$horariosq2,"3"=>$horariosq3,"4"=>$horariosq4]];
  	echo json_encode($data_array);
  	//echo json_encode($horarios,true);  	
	die(); 
}
add_action('wp_ajax_chat_rules', 'chat_rules');
add_action('wp_ajax_nopriv_chat_rules', 'chat_rules');
function chat_init()
{	
	ini_set('display_errors',1);
	error_reporting(E_ALL);	
	date_default_timezone_set('America/Mexico_City');
	$nombre = $_POST['name-chat'];
	$celular = $_POST['phone-chat'];
	$correo = $_POST['email'];
	$mensaje = isset($_POST['msj-chat'])?$_POST['msj-chat']:"";
	$cid = $_POST['cid'];
	$depto = $_POST['depto'];
	$deptoqueue = $_POST['deptoqueue'];
	$tipoForm = $_POST['tipoForm'];

	$validOnline = false;
	$queueP = obtenerQueueParams($deptoqueue);					
	$horarios = $queueP["Horarios"];
	$queueName = $queueP["Nombre"];
	$d = date('w');
	$h = date('G');
	$horCorr = $horarios[$d];
	//var_dump($horCorr);
    for ($i = 0, $len = count($horCorr); $i < $len; $i++) {          
      $horario = $horCorr[$i];            
      if ($h>=$horario[0] && $h<$horario[1]) {
        $validOnline =  true;
        break;
      }
    }
    if($validOnline){
    	$interaccion = "online";
		$queueFinal = $queueName;
    }else{
    	$queuerespaldo = $queueP["QueueRespaldo"];
    	$queueRespaldoP = obtenerQueueParams($queuerespaldo);					
		$horariosRespaldo = $queueRespaldoP["Horarios"];
		$queueNameRespaldo = $queueRespaldoP["Nombre"];
		$horCorrRespaldo = $horariosRespaldo[$d];
		//var_dump($horCorr);
	    for ($i = 0, $len = count($horCorrRespaldo); $i < $len; $i++) {          
	      $horarioResp = $horCorrRespaldo[$i];            
	      if ($h>=$horarioResp[0] && $h<$horarioResp[1]) {
	        $validOnline =  true;
	        break;
	      }
	    }
    	if($validOnline){ 
    		$interaccion = "online";   	
    		$queueFinal = $queueNameRespaldo;
    	}else{
    		$interaccion = "offline";
    		
    	}
    	
    }
    $interaccion = "offline";
    if ($interaccion == "online") {
    	$chatConfig = ["webchatAppUrl"=>"https://apps.mypurecloud.com/webchat",    								
            				"webchatServiceUrl"=> "https://realtime.mypurecloud.com:443",
            				"orgId"=> 9038,            
            				"orgName"=> "promotorakranonsadecv",            
            				"priority"=> 8,            
            				"queueName"=>"Kranon",            
            				"logLevel"=> "DEBUG",
            				"locale"=> "es",
            				"data"=>[
            					"firstName"=> $nombre,			                
			                "phoneNumber"=> $celular,
			                "phoneType"=> "Cell",
			                "email"=> $correo,
			                "customerId"=> $cid,
											"customField1Label"=> "MensajeCliente",
											"customField1"=> $mensaje,
											"customField2Label"=> "Departamento",
											"customField2"=> $depto												
            				],
            				"companyLogo"=> [
                			"width"=> 149,
                			"height"=> 149,
                			"url"=> "https://unitecmx-universidadtecno.netdna-ssl.com/imagenes/unitec.png"
            				],            
            				"companyLogoSmall"=> [
                			"width"=> 149,
                			"height"=> 149,
                 			"url"=> "https://unitecmx-universidadtecno.netdna-ssl.com/imagenes/unitec.png"
										],            
            				"agentAvatar"=>[
                			"width"=> 462,
                			"height"=> 462,
              				"url"=> "https://d3a63qt71m2kua.cloudfront.net/developer-tools/1225/assets/images/PC-blue-nomark.png" 
			 							],            
            				"welcomeMessage"=> "Bienvenido al Centro de Atención de UNITEC ¿Requieres que te apoye con mayor información?",            
            				"cssClass"=> "webchat-frame",            
            				"css"=> [
                			"width"=> "100%",
                			"height"=> "550px"
            				]

    					];  
    	$link = (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on' ? 
            "https" : "http") . "://" . $_SERVER['HTTP_HOST'] ;
    	$data['nombre'] = trim( $nombre );
	    $data['mobilephone'] = trim($celular );
	    $data['email'] = trim($correo);
	    $data['depto'] = $depto;
	    $data['mensaje_chat'] = trim($mensaje);
	    $data['msgoff'] = trim("chat_online");
	    $responsews = file_get_contents($link."/desk/procWeb/microregistroChat.php"."?".http_build_query($data) );


		$response = array('success' =>true ,'data'=> array('chatConfig' => $chatConfig,"tipoInteraccion"=>"online") );
		echo json_encode($response);
    }elseif ($interaccion == "offline") {
    	$link = (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on' ? 
            "https" : "http") . "://" . $_SERVER['HTTP_HOST'] ;
    	$data['nombre'] = trim( $nombre );
	    $data['mobilephone'] = trim($celular );
	    $data['email'] = trim($correo);
	    $data['depto'] = $depto;
	    $data['mensaje_chat'] = trim($mensaje);
	    $data['msgoff'] = trim("chat_offline");
	    $responsews = file_get_contents($link."/desk/procWeb/microregistroChat.php"."?".http_build_query($data) );
	    $response = array('success' =>true ,'data'=> array('response' => $responsews,"tipoInteraccion"=>"offline") );
    	echo json_encode($response);
    }
	die(); 
}
add_action('wp_ajax_chat_init', 'chat_init');
add_action('wp_ajax_nopriv_chat_init', 'chat_init');

function obtenerQueueParams($queue=""){
	switch ($queue) {
		case 1:
			$horarios["Nombre"] = "CHAT";
			$horarios["QueueRespaldo"] = null;
			$horarios["Horarios"] = array(0=>[[10,14]],
				1=>[[8,24]],
				2=>[[0,2],[8,12]],
				3=>[[0,2],[8,24]],
				4=>[[0,2],[8,24]],
				5=>[[0,2],[8,24]],
				6=>[[0,2],[9,21]]				
				);
			$horarios["Departamentos"] = array("0" =>[1,'Licenciatura'], "1" => [1,'Preparatoria'], "2" =>[1,'Ciencias de la Salud'],"7" => [1,'Sin departamento']);
		break;
		case 2:
			$horarios["Nombre"] = "CHAT ONLINE";
			$horarios["QueueRespaldo"] = 1;
			$horarios["Horarios"] = array(0=>[[10,14]],
				1=>[[8,24]],
				2=>[[0,2],[8,12]],
				3=>[[0,2],[8,24]],
				4=>[[0,2],[8,24]],
				5=>[[0,2],[8,24]],
				6=>[[0,2],[9,21]]				
				);				
			$horarios["Departamentos"] = array("4"=>[2,'Estudios en línea']);
		break;
		case 3:
			$horarios["Nombre"] = "CHAT INTERNO";
			$horarios["QueueRespaldo"] = 1;
			$horarios["Horarios"] = array(0=>[[10,14]],
				1=>[[8,21]],
				2=>[[8,21]],
				3=>[[8,21]],
				4=>[[8,21]],
				5=>[[8,21]],
				6=>[[9,15]]				
				);				
			$horarios["Departamentos"] = array("5"=>[3,'Posgrados'],"6"=>[3,'Quiero titularme por seminario']);
		break;
		case 4:
			$horarios["Nombre"] = "CHAT REGRESOS";
			$horarios["QueueRespaldo"] = 1;
			$horarios["Horarios"] = array(0=>[[10,14]],
				1=>[[8,21]],
				2=>[[8,21]],
				3=>[[8,21]],
				4=>[[8,21]],
				5=>[[8,21]],
				6=>[[9,15]]				
				);				
			$horarios["Departamentos"] = array("3"=>[4,'Servicios Escolares']);
		break;
		default:
			$horarios["Nombre"] = "";
			$horarios["QueueRespaldo"] = "";
			$horarios["Horarios"] = array(0=>[[10,14]],
				1=>[[8,24]],
				2=>[[0,2],[8,24]],
				3=>[[0,2],[8,24]],
				4=>[[0,2],[8,24]],
				5=>[[0,2],[8,24]],
				6=>[[0,2],[9,21]]				
				);				
		break;
	}	
	return $horarios;
}
/**
 * Note: Do not add any custom code here. Please use a custom plugin so that your customizations aren't lost during updates.
 * https://github.com/woocommerce/theme-customisations
 */