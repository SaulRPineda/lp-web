<?php
    $data = NULL;
    $response = NULL;
    $endpoint = 'https://vmessage-api.thevisualverify.com/clickToCall/calls';
    $datos = $_REQUEST['theData'];

    function quitar_tildes($cadena) {
		$no_permitidas= array ("á","é","í","ó","ú","Á","É","Í","Ó","Ú","ñ","À","Ã","Ì","Ò","Ù","Ã™","Ã ","Ã¨","Ã¬","Ã²","Ã¹","ç","Ç","Ã¢","ê","Ã®","Ã´","Ã»","Ã‚","ÃŠ","ÃŽ","Ã”","Ã›","ü","Ã¶","Ã–","Ã¯","Ã¤","«","Ò","Ã","Ã„","Ã‹");
		$permitidas= array ("a","e","i","o","u","A","E","I","O","U","n","N","A","E","I","O","U","a","e","i","o","u","c","C","a","e","i","o","u","A","E","I","O","U","u","o","O","i","a","e","U","I","A","E");
		$texto = str_replace($no_permitidas, $permitidas ,$cadena);
		return $texto;
    }
    
    if ($datos != NULL || $datos !="") {
        $data = array("phoneNumber" => $datos["phonenumber"], "medium" => "device");                                                                   
        $data_string = json_encode($data);
        $ch = curl_init();
    
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string);
        curl_setopt($ch, CURLOPT_URL, $endpoint);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
            'Content-Type: application/json',
            'appKey: ba54b594bbe9-42c4-9230-4c413e80eb11'
        ));
    
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    
        $response    = curl_exec($ch);
        $status_code = curl_getinfo($ch, CURLINFO_HTTP_CODE);
    
         //var_dump($response);//test
         //var_dump($status_code);//test
    
        if(curl_error($ch)){
            $error_msg = curl_error($ch);  
                // s
                $arr = array('success'=>false,'codigo'=>$status_code,'msj'=>$error_msg);
                
        }else{
            if ($status_code==200) {
                # code...
                $arr = array('success'=>true);
            }else{
                $arr = array('success'=>false,'codigo'=>$status_code,'msj'=>$response);    
            }
        }
        curl_close($ch);
        //$arr = array('success'=>true);        
        @file_put_contents("ctc_log.log", $datos["phonenumber"]." ".date('Y-m-d H:i:s')." ".$status_code.PHP_EOL,FILE_APPEND);
        echo json_encode($arr);
    }

?>