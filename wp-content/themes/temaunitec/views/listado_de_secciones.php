<input type="hidden" name="page_noncename" id="page_sections_noncename" value="<?php echo wp_create_nonce('page_section_' . $post->ID); ?>"/>
<p>Elija las secciones de la página para mostrar arrastrando las secciones deseadas de <strong>"Secciones Disponibles"</strong> a la caja de<strong> "Secciones Seleccionadas".</strong></p>
<input type="hidden" id="single_page_id" name="single_page_id" value="<?php echo $post->ID; ?>"/>

<div id="order-post-type">
    <div id="available-page-sections">
        <h3><?php echo __('Secciones Disponibles DESKTOP', 'unitec_theme') ?></h3>
        <p class="box-description">Para elegir una sección de página, arrastrarlo y soltarlo en la caja ' Secciones Seleccionadas ' de la derecha.</p>
            <ul id="sortable1" class="blocks connected-sortable">
            <?php
            $themes = wp_get_themes();
            $theme = wp_get_theme();
            $templates = $theme['Template Files'];
            $post_templates = array();
            if( is_array($templates) ) {
                $base = array( trailingslashit(get_template_directory()."/secciones_desk/"), trailingslashit(get_stylesheet_directory()) );
                
                foreach ( $templates as $template ) {
                    $basename = str_replace($base, '', $template);
                    if ($basename != 'functions.php') {
                        // don't allow template files in subdirectories
              
                        if ( false !== strpos($basename, '/') )
                        continue;
                        
                        $template_data = implode( '', file( $template ));
                        $name = '';
                        
                        
                       // echo $template_data;
                        if ( preg_match( '|WP Post Template: desk_(.*)$|mi', $template_data, $name ) )
                        $name = _cleanup_header_comment($name[1]);
                        
                        if ( !empty( $name ) ) {
                            $post_templates[trim( $name )] = $basename;
                        }
                    }
                }
            }

            foreach ($post_templates as $key => $value)
             { ?>
                <li class="block" rel="section_<?php echo str_replace('_', '.', $value); ?>">
                    <dl class="block-bar">
                        <dt class="block-handle">
                            <div class="block-title"><?php echo $value; ?></div>
                        </dt>
                    </dl>
                    <div class="block-settings clearfix">
                        <div class="description">
                            <span style="font-size: 8px">(<?php echo $value; ?> )</span>
                            <?php echo wpautop($description); ?>
                        </div>
                        <div class="block-control-actions clearfix"></div>
                    </div>
                </li>
            <?php } ?>
            </ul>
    </div>


    <div id="chosen-page-sections">
        <h3><?php echo __('Secciones Seleccionadas', 'unitec_theme') ?> <span class="spinner"></span></h3>
        <p class="box-description">Usted puede cambiar el orden de las secciones en las que aparecerán en la página, arrastre y suelte la sección en la posición deseada.</p>
        <ul id="sortable2" class="blocks connected-sortable">
            <?php 
            $secciones=get_post_meta($post->ID,"_page_section_order_field");
            //var_dump($secciones);
            if($secciones){
                $secciones=explode(',', $secciones[0]);
                foreach ($secciones as $key => $value)
                {?>
                    <li class="block" rel="section_<?php echo str_replace('_', '.', $value); ?>">
                        <dl class="block-bar">
                            <dt class="block-handle">
                                <div class="block-title"><?php echo str_replace('.', '_', $value); ?></div>
                                <span class="block-controls">
                <a class="block-edit" id="edit-1" title="Edit Block" href="#block-settings-1"><?php echo __('Edit Block', 'unitec_theme') ?></a>
            </span>
                            </dt>
                        </dl>
                        <div class="block-settings clearfix">
                            <div class="description">
                                <span style="font-size: 8px">(<?php echo $value; ?> )</span>
                                <?php echo wpautop($description); ?>
                            </div>

                            <div class="block-control-actions clearfix">
            <?php
            if (current_user_can('edit_post', $post_id) && $link = esc_url(get_edit_post_link($post_id)))
                echo '<a class="edit" href="' . $link . '" target="_blank">' . __('Edit', 'unitec_theme') . '</a> | ';
            if (current_user_can('read', $post_id) && $link = esc_url(get_post_permalink($post_id)))
                echo '<a class="view" href="' . $link . '" target="_blank">' . __('View', 'unitec_theme') . '</a> | ';
            echo '<span class="hideable"><a href="#" class="remove">' . __('Remove', 'unitec_theme') . '</a> | </span>';
            echo '<a href="#" class="close">' . __('Close', 'unitec_theme') . '</a>';
            ?>
        </div>
                           
                        </div>
                    </li>
        <?php }
            }else{    
                    echo "No has agregado ninguna seccion";
                }
        ?>
        </ul>
    </div>

    <div class="clear"></div>
</div>


<!-- COMIENZA LA VERSION MOBILE -->
<hr>

<div id="order-post-typem">
    <div id="available-page-sections">
        <h3><?php echo __('Secciones Disponibles MOBILE', 'unitec_theme') ?></h3>
        <p class="box-description">Para elegir una sección de página, arrastrarlo y soltarlo en la caja ' Secciones Seleccionadas ' de la derecha.</p>
            <ul id="sortable1m" class="blocks connected-sortable">
            <?php
            $themes = wp_get_themes();
            $theme = wp_get_theme();
            $templates = $theme['Template Files'];
            $post_templates = array();
            
            if( is_array($templates) ) {
                $base = array( trailingslashit(get_template_directory()."/secciones_mobile/"), trailingslashit(get_stylesheet_directory()) );
                foreach ( $templates as $template ) {
                    $basename = str_replace($base, '', $template);
                    if ($basename != 'functions.php') {
                        // don't allow template files in subdirectories
                        if ( false !== strpos($basename, '/') )
                        continue;
                        
                        $template_data = implode( '', file( $template ));
                        $name = '';
                        
                        if ( preg_match( '|WP Post Template: mob_(.*)$|mi', $template_data, $name ) )
                        $name = _cleanup_header_comment($name[1]);
                        
                        if ( !empty( $name ) ) {
                            $post_templates[trim( $name )] = $basename;
                        }
                    }
                }
            }

            foreach ($post_templates as $key => $value)
             { ?>
                <li class="block" rel="sectionm_<?php echo str_replace('_', '.', $value); ?>">
                    <dl class="block-bar">
                        <dt class="block-handle">
                            <div class="block-title"><?php echo $value; ?></div>
                        </dt>
                    </dl>
                    <div class="block-settings clearfix">
                        <div class="description">
                            <span style="font-size: 8px">(<?php echo $value; ?> )</span>
                            <?php echo wpautop($description); ?>
                        </div>
                        <div class="block-control-actions clearfix"></div>
                    </div>
                </li>
            <?php } ?>
            </ul>
    </div>


    <div id="chosen-page-sections">
        <h3><?php echo __('Secciones Seleccionadas', 'unitec_theme') ?> <span class="spinner"></span></h3>
        <p class="box-description">Usted puede cambiar el orden de las secciones en las que aparecerán en la página, arrastre y suelte la sección en la posición deseada.</p>
        <ul id="sortable2m" class="blocks connected-sortable">
            <?php 
            $secciones=get_post_meta($post->ID,"_page_section_order_fieldmobile");
            if($secciones){
                $secciones=explode(',', $secciones[0]);
                foreach ($secciones as $key => $value)
                {?>
                    <li class="block" rel="sectionm_<?php echo str_replace('_', '.', $value); ?>">
                        <dl class="block-bar">
                            <dt class="block-handle">
                                <div class="block-title"><?php echo str_replace('.', '_', $value); ?></div>
                                <span class="block-controls">
                <a class="block-edit" id="edit-1" title="Edit Block" href="#block-settings-1"><?php echo __('Edit Block', 'unitec_theme') ?></a>
            </span>
                            </dt>
                        </dl>
                        <div class="block-settings clearfix">
                            <div class="description">
                                <span style="font-size: 8px">(<?php echo $value; ?> )</span>
                                <?php echo wpautop($description); ?>
                            </div>
                            
                            <div class="block-control-actions clearfix">
            <?php
            if (current_user_can('edit_post', $post_id) && $link = esc_url(get_edit_post_link($post_id)))
                echo '<a class="edit" href="' . $link . '" target="_blank">' . __('Edit', 'unitec_theme') . '</a> | ';
            if (current_user_can('read', $post_id) && $link = esc_url(get_post_permalink($post_id)))
                echo '<a class="view" href="' . $link . '" target="_blank">' . __('View', 'unitec_theme') . '</a> | ';
            echo '<span class="hideable"><a href="#" class="remove">' . __('Remove', 'unitec_theme') . '</a> | </span>';
            echo '<a href="#" class="close">' . __('Close', 'unitec_theme') . '</a>';
            ?>
        </div>
                        </div>
                    </li>
        <?php }
            }else{    
                    echo "No has agregado ninguna seccion";
                }
        ?>
        </ul>
    </div>

    <div class="clear"></div>
</div>

