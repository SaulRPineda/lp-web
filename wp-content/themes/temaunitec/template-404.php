<?php
/**
 * The template for displaying the homepage.
 *
 * This page template will display any functions hooked into the `homepage` action.
 * By default this includes a variety of product displays and the page content itself. To change the order or toggle these components
 * use the Homepage Control plugin.
 * https://wordpress.org/plugins/homepage-control/
 *
 * Template name: 404
 *
 * @package storefront
 */

?>

<main class="col-12 mt-0"> 
    <!-- SECTION-->
    <section id="elegant-card">
        <!-- Live preview-->
        <div class="row">
            <div class="col-md-12">
<!-- ***************************-> Inicia tu código ... -->
                <div class="card mb-2">
                    <h4 class="card-title">Lo sentimos... Error 404: Página no encontrada</h4>                    

                    <div class="view overlay hm-white-slight">
                        <!-- <img alt="" class="img-fluid" src="http://localhost/unitec2017/wp-content/uploads/2017/08/prepa-unitec-perfil-ingreso.jpg"> -->
                        <amp-img 
                            layout="responsive"
                            width="768" 
                            height="420" 
                            src="http://localhost/unitec2017/wp-content/uploads/2017/08/prepa-unitec-perfil-ingreso.jpg" 
                            alt="" />
                            <noscript><img src="http://localhost/unitec2017/wp-content/uploads/2017/08/prepa-unitec-perfil-ingreso.jpg" alt=""></noscript>
                        </amp-img>
                        <a><!-- <div class="mask waves-effect waves-light"></div> --></a>
                    </div>
                    <div class="card-block">
                        <div class="row">
                            <div class="col-12">
                                <p class="card-text roboto-light">Esperamos que una de las siguientes opciones sea de utilidad para ti.</p>
                            </div>
                        </div>        
                    </div>

                    <section id="tabNav">
                        <i class="icon-u icon-u-tab-izquierda leftTabNav direccionales"></i>                        <div class="tabs-wrapper"> 
                            <ul class="nav classic-tabs " role="tablist">
                                                            <li class="nav-item"><!-- waves-effect waves-light -->
                                    <a class=" waves-light active text-black " data-toggle="tab" href="#tab-panel-unitec-0" role="tab">Oferta Educativa</a>
                                </li>            
                                                        </ul>
                        </div>
                        <i class="icon-u icon-u-tab-derecha rightTabNav direccionales"></i>                    </section>

                                        <!-- Tab panels -->
                    <div class="tab-content lighten-4 ">
                        
                                            <!--Inicia Tab -->
                        <div class="tab-pane fade in show active" id="tab-panel-unitec-0" role="tabpanel">
                            <div class="col-12 p-0 specialList animated fadeIn">
                                                        
                                <div class="simple-list">
                                    <div type="button" class="list-item-container">

                                        
                                        <div class="item-icon">
                                            <a href="<?php echo site_url( '/prepa/'); ?>" ><i class="icon-u icon-u-preparatoria link-simple-list"></i></a>                                        </div>
                                        
                                        <div class="item-description">
                                            <a href="<?php echo site_url( '/prepa/'); ?>">Preparatoria</a>                                        </div> <!-- icon-u icon-u-alumnos -->
                                    </div>
                                </div>

                                                        
                                <div class="simple-list">
                                    <div type="button" class="list-item-container">

                                        
                                        <div class="item-icon">
                                            <a href="<?php echo site_url( '/licenciaturas/'); ?>"><i class="icon-u icon-u-licenciaturas link-simple-list"></i></a>                                        </div>
                                        
                                        <div class="item-description">
                                            <a href="<?php echo site_url( '/licenciaturas/'); ?>">Licenciaturas</a>                                        </div> <!-- icon-u icon-u-alumnos -->
                                    </div>
                                </div>

                                                        
                                <div class="simple-list">
                                    <div type="button" class="list-item-container">

                                        
                                        <div class="item-icon">
                                            <a href="<?php echo site_url( '/ingenierias/'); ?>"><i class="icon-u icon-u-ingenierias link-simple-list"></i></a>                                        </div>
                                        
                                        <div class="item-description">
                                            <a href="<?php echo site_url( '/ingenierias/'); ?>">Ingenierías</a>             </div> <!-- icon-u icon-u-alumnos -->
                                    </div>
                                </div>

                                                        
                                <div class="simple-list">
                                    <div type="button" class="list-item-container">

                                        
                                        <div class="item-icon">
                                            <a href="<?php echo site_url( '/diplomados/'); ?>"><i class="icon-u icon-u-diplomados link-simple-list"></i></a>                                        </div>
                                        
                                        <div class="item-description">
                                            <a href="<?php echo site_url( '/diplomados/'); ?>">Diplomados</a>                                        </div> <!-- icon-u icon-u-alumnos -->
                                    </div>
                                </div>

                                                        
                                <div class="simple-list">
                                    <div type="button" class="list-item-container">

                                        
                                        <div class="item-icon">
                                            <a href="<?php echo site_url( '/especialidades-odontologicas/'); ?>"><i class="icon-u icon-u-odontologia link-simple-list"></i></a>                                        </div>
                                        
                                        <div class="item-description">
                                            <a href="<?php echo site_url( '/especialidades-odontologicas/'); ?>">Especialidades</a>                                        </div> <!-- icon-u icon-u-alumnos -->
                                    </div>
                                </div>

                                                        
                                <div class="simple-list">
                                    <div type="button" class="list-item-container">

                                        
                                        <div class="item-icon">
                                            <a href="<?php echo site_url( '/maestrias/'); ?>"><i class="icon-u icon-u-maestrias link-simple-list"></i></a>                                        </div>
                                        
                                        <div class="item-description">
                                            <a href="<?php echo site_url( '/maestrias/'); ?>">Maestrías</a>                                        </div> <!-- icon-u icon-u-alumnos -->
                                    </div>
                                </div>

                                                        </div>
                        </div>
                        <!-- Termina Tab -->
                                        </div>
                    
                </div>
<!-- ***************************-> Termina tu código ... -->
            </div>
        </div><!-- /.Live preview-->
    </section><!-- SECTION-->
</main><!-- /.Main layout-->