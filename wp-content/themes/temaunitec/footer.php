<?php
/**
 * The template for displaying the footer.
 *
 * Contains the closing of the #content div and all content after
 *
 * @package storefront
 */

?>
<!-- Formularios
<app-formulario-tradicional style="display: none;"></app-formulario-tradicional>
<app-formulario-tradicional-OS style="display: none;"></app-formulario-tradicional-OS>-->
<!-- Terminan formularios -->
<!-- Hidden para los json -->
    <!-- <modal-app-formulario-taller></modal-app-formulario-taller> -->
    <?php
     $infoReqFlow = 'true';
     if(strpos($post->post_name,'calcula-tu-beca') !== false ) {
        $infoReqFlow = 'false';
     }


    if(strpos($post->post_name,'calcula-tu-beca') !== true) {
    ?>
       <app-modal></app-modal>
    <?php     
    }

    ?>
  <?php if( strpos($post->post_name,'conexion-unitec') === false && strpos($post->post_name,'agendar-cita') === false && strpos($post->post_name,'biblioteca-virtual') === false ){ ?>
    <!-- <app-modal></app-modal>
    <modal-formulario-tradicional></modal-formulario-tradicional>
    <modal-app-formulario-tradicional></modal-app-formulario-tradicional>
    <modal-calculadora-formulario-tradicional></modal-calculadora-formulario-tradicional> -->
    <?php if(!wp_is_mobile()) { ?>
        <div id="app" class="container-fluid d-flex justify-content-center align-items-center px-2 app">
            <calculate-scholarship-btn 
                :bus="bus" 
                :lp-base-url="lpCssBasePath" 
                :relative-css-path="relativeCssPath" 
                :parent-css-ids="parentCssIds"
                btn-title="Solicitud de Admisión"
                btn-id="modal_frm_app"
                v-show="false">
            </calculate-scholarship-btn>
            <scholarship-app 
                :bus="bus"
                :microregistro-completo="microregistroCompleto"
                :show-thankyou-page="showThankyouPage"
                :show-cycle-page="showCyclePage"
                :state-was-selected="stateWasSelected"
                :form="form"
                :level-was-selected="levelWasSelected"
                :campus-was-selected="campusWasSelected"
                :muestra-calculadora="muestraCalculadora"
                :show-overlay="showOverlay"
                :show-program-warning="showProgramWarning"
                :show-campus-warning="showCampusWarning"
                :show-study-mode-warning="showStudyModeWarning"
                :show-average="showAverage"
                :show-scholarship-calculator="showScholarshipCalculator"
                :is-req-info-flow="<?php echo $infoReqFlow; ?>"
                :show-footer-mobile="<?php echo $infoReqFlow; ?>">
            </scholarship-app>
        </div>
        <script type="text/javascript"  src="<?php echo get_template_directory_uri()?>/assets/frontend_desktop/js/vue/manifest.js"></script>
        <script type="text/javascript"  src="<?php echo get_template_directory_uri()?>/assets/frontend_desktop/js/vue/vendor.js"></script>
        <script type="text/javascript"  src="<?php echo get_template_directory_uri()?>/assets/frontend_desktop/js/vue/appCalculadoraUnitecMx.js"></script>
    <?php } else { ?>
        <div id="app" class="container-fluid d-flex justify-content-center align-items-center px-2">
            <calculate-scholarship-btn 
                :bus="bus" 
                :lp-base-url="lpCssBasePath" 
                :relative-css-path="relativeCssPath" 
                :parent-css-ids="parentCssIds"
                btn-title="Solicitud de Admisión"
                btn-id="modal_frm_app"
                v-show="true">
            </calculate-scholarship-btn>
            <scholarship-app 
                :bus="bus"
                :microregistro-completo="microregistroCompleto"
                :show-thankyou-page="showThankyouPage"
                :show-cycle-page="showCyclePage"
                :state-was-selected="stateWasSelected"
                :form="form"
                :level-was-selected="levelWasSelected"
                :campus-was-selected="campusWasSelected"
                :muestra-calculadora="muestraCalculadora"
                :show-overlay="showOverlay"
                :show-program-warning="showProgramWarning"
                :show-campus-warning="showCampusWarning"
                :show-study-mode-warning="showStudyModeWarning"
                :show-average="showAverage"
                :show-calc-section="showCalcSection"
                :show-scholarship-calculator="showScholarshipCalculator"
                :is-req-info-flow="<?php echo $infoReqFlow; ?>"
                :show-footer-mobile="<?php echo $infoReqFlow; ?>">
            </scholarship-app>
        </div>
        <script type="text/javascript" src="<?php echo get_template_directory_uri()?>/assets/frontend/js/vue/manifest.js"></script>
        <script type="text/javascript" src="<?php echo get_template_directory_uri()?>/assets/frontend/js/vue/vendor.js"></script>
        <script type="text/javascript" src="<?php echo get_template_directory_uri()?>/assets/frontend/js/vue/appCalculadoraUnitecMxMobile.js"></script>
    <?php } ?>

  <?php } ?>

  <?php if(strpos($post->post_name,'calcula-tu-beca')===false && 
          strpos($post->post_name,'conexion-unitec')===false && 
          strpos($post->post_name,'agendar-cita')===false && 
          strpos($post->post_name,'testvocacional')===false && 
          strpos($post->post_name,'prueba-performance')===false && !wp_is_mobile()){?>
        <!-- <link rel="stylesheet" id="calculadora-css" type="text/css" href="<?php echo get_template_directory_uri()?>\assets\frontend_desktop\css\secciones\min\desk_31_formulario.min.css" disabled="disabled">
        <script type="text/javascript" async src="<?php echo get_template_directory_uri()?>/assets/frontend_desktop/js/secciones/min/desk_31_1_formulario.min.js"></script>
        <script type="text/javascript" async src="<?php echo get_template_directory_uri()?>/assets/frontend_desktop/js/secciones/min/desk_31_formulario.min.js"></script> -->
    <?php }
        else if(strpos($post->post_name,'calcula-tu-beca')===false && 
                strpos($post->post_name,'conexion-unitec')===false && 
                strpos($post->post_name,'agendar-cita')===false && 
                strpos($post->post_name,'testvocacional')===false && 
                strpos($post->post_name,'prueba-performance')===false && wp_is_mobile()){
        // $critical_min= get_template_directory_uri()."/assets/frontend/css/vendor/min/critical.min.css";
        // echo '<style id="form-css" disabled="disabled">'.file_get_contents($critical_min).'</style>';
        // echo '<script async src="'.get_template_directory_uri().'/assets/frontend_desktop/js/secciones/min/desk_31_1_formulario.min.js"></script>';
        // echo '<script async src="'.get_template_directory_uri().'/assets/frontend/js/formulario_integracion/min/funciones-criticas.min.js"></script>';
    }

    else if( strpos($post->post_name,'testvocacional')!==false && wp_is_mobile()){ 
        //  $critical_min= get_template_directory_uri()."/assets/frontend/css/vendor/min/critical.min.css";
        // echo '<style id="form-css" disabled="disabled">'.file_get_contents($critical_min).'</style>';
        // echo '<script async src="'.get_template_directory_uri().'/assets/frontend_desktop/js/secciones/min/desk_37_1_formulario_auto.min.js"></script>';
        // echo '<script async src="'.get_template_directory_uri().'/assets/frontend/js/formulario_autocomplete/min/funciones-criticas.min.js"></script>';
     }

    else if( strpos($post->post_name,'testvocacional')!==false && !wp_is_mobile()){ ?>
      <!-- <link rel="stylesheet" id="calculadora-css" type="text/css" href="<?php echo get_template_directory_uri()?>\assets\frontend_desktop\css\secciones\min\desk_37_formulario_auto.min.css" disabled="disabled">
        <script type="text/javascript" async src="<?php echo get_template_directory_uri()?>/assets/frontend_desktop/js/secciones/min/desk_37_1_formulario_auto.min.js"></script>
        <script type="text/javascript" async src="<?php echo get_template_directory_uri()?>/assets/frontend_desktop/js/secciones/min/desk_37_formulario_auto.min.js"></script> -->
    <?php } ?>


  
  <input id="data_json_basura" type="hidden" value="0">
  <input id="data_json_basura_email" type="hidden" value="0">
  <input id="data_json_basura" type="hidden" value="0">
  <input id="data_json_basura_email" type="hidden" value="0">
  <input id="data_json_telefonos_basura" type="hidden" value="0">
  <!-- Input que almacena el titulo Dinamico del formulario -->
  <input id="h_titulo_modal_formulario" type="hidden" value="">
  <input id="formApp" type="hidden">
  <input id="url_img_destacada" type="hidden" value="">
  <input id="url_video_destacado" type="hidden" value="">
<!-- Termina hidden para los json -->
</div>


<?php if(strpos($post->post_name,'calcula-tu-beca')===false && 
        strpos($post->post_name,'conexion-unitec')===false && 
        strpos($post->post_name,'agendar-cita')===false && 
        strpos($post->post_name,'testvocacional')===false && 
        strpos($post->post_name,'prueba-performance')===false && wp_is_mobile()) { ?>
  <!-- <link rel="stylesheet" type="text/css" href="<?php echo get_template_directory_uri();?>/assets/frontend/css/vendor/min/basscss.min.css" media="none" onload="if(media!='all')media='all'" />
  <script defer src="<?php echo get_template_directory_uri();?>/assets/frontend/js/formulario_integracion/min/funciones-formulario.min.js"></script>
  <script async src="<?php echo get_template_directory_uri();?>/assets/frontend/js/formulario_integracion/min/funciones-async.min.js"></script>
  <script async src="<?php echo get_template_directory_uri();?>/assets/frontend/js/formulario_integracion/min/funciones-on.min.js"></script> -->
<?php } ?>

<?php if( strpos($post->post_name,'testvocacional')!==false && wp_is_mobile()) { ?>
  <!-- <link rel="stylesheet" type="text/css" href="<?php echo get_template_directory_uri();?>/assets/frontend/css/vendor/min/basscss.min.css" media="none" onload="if(media!='all')media='all'" />
  <script defer src="<?php echo get_template_directory_uri();?>/assets/frontend/js/formulario_autocomplete/min/funciones-formulario.min.js"></script>
  <script async src="<?php echo get_template_directory_uri();?>/assets/frontend/js/formulario_autocomplete/min/funciones-async.min.js"></script>
  <script async src="<?php echo get_template_directory_uri();?>/assets/frontend/js/formulario_autocomplete/min/funciones-on.min.js"></script> -->
<?php } ?>

<?php   
  /*Inicia Carga Por Secciones BY SRP 09-08-2018*/
  $dirWorpress = get_template_directory_uri();

  //Buscar en la raiz del server
  $tempDir = get_template_directory();

  $field = get_field_object('tipo_de_banner');
  $bannerDFP = $field['value'];
  $label = $field['choices'][ $bannerDFP ];

  switch ( wp_is_mobile() ) {
      case 1:
          $page_section_conf = "_page_section_order_fieldmobile";
          $carpeta = "frontend";
          $jsmenu = 'mob_1_menu.min.js';
      break;
    
      case 0:
          $page_section_conf = "_page_section_order_field";
          $carpeta = "frontend_desktop";
          $jsmenu = 'desk_1_menu.min.js';
      break;

      default:
          $page_section_conf = "_page_section_order_fieldmobile";
          $carpeta = "frontend";
          $jsmenu = 'mob_1_menu.min.js';
      break;
  }

  $page_id = get_the_ID();
  $page_sec_id = get_post_meta($page_id, $page_section_conf, true);
  $page_sec_id = explode(",", $page_sec_id);
  $numero_secciones = count($page_sec_id);

  /*Remove de la Palabra PHP BY SRP 09-08-2018*/
  $seccionUno = explode( ".php", $page_sec_id[0] );
  $seccionDos = explode( ".php", $page_sec_id[1] );

  /*Obtenemos las secciones a descartar para colocarlas inline BY SRP 09-08-2018*/
  $assetsInline = array( str_replace( ".", "_", $seccionUno[0] ), str_replace( ".", "_", $seccionDos[0] ) );
  //print_r( $assetsInline );

  $scripts_menuTemp = $tempDir . "/assets/".$carpeta."/js/secciones/min/";
  $scripts_menu = $dirWorpress . "/assets/".$carpeta."/js/secciones/min/";

  $scriptsGenerales = $dirWorpress . '/assets/'.$carpeta.'/js/secciones/min/generales.min.js';
  
  echo "<script type='text/javascript'>";
      foreach ($assetsInline as $key => $value) {
          //echo $scripts_menuTemp.$value.".min.js";
          if( file_exists ( $scripts_menuTemp.$value.".min.js" ) ) {
              echo file_get_contents( $scripts_menu.$value.".min.js" );
          }
      }
      //echo $scriptsGenerales;
      echo file_get_contents( $scriptsGenerales );
  echo "</script>";
?>

<script type="text/javascript">
    $(window).on('load', function(){
        // ga('create', 'UA-9351414-14', 'auto');
        // ga(function(tracker) {
        //   alert(tracker.get('clientId'));
        //     setCookieForm("cid_unitec", tracker.get('clientId'), 1);
        //     jQuery("#CID").val(tracker.get('clientId'));
        // });
        try {
          var trackers = ga.getAll();
          var i, len;
          for (i = 0, len = trackers.length; i < len; i += 1) {
            if (trackers[i].get('trackingId') === "UA-9351414-14") {
              // alert(trackers[i].get('clientId'));
              setCookieForm("cid_unitec", trackers[i].get('clientId'), 1);
              jQuery("#CID").val(trackers[i].get('clientId'));
            }
          }
        } catch(e) {}
    })

</script>

<?php wp_footer(); ?>

<?php  
    $ocultarWhatsapp = get_field( 'ocultar_whatsapp' ,$theID );
    $ocultarBotonera = get_field( 'ocultar_botonera' ,$theID );
    $ocultarCTC = get_field( 'ocultar_frm_ctc' ,$theID );
    $ocultarChat = get_field( 'ocultar_frm_chat' ,$theID );
    //$ocultarChat = false;
?>

<?php
  $dirWorpress = get_template_directory_uri();
?>
<?php
  if ((wp_is_mobile() === true && $ocultarBotonera != true) || strpos($post->post_name,'test-kranon') !== false)   {
?>
<!------------------------------------------------ B O T O N E R A -------------------------------------------------->

<!-- Carga de javascript y css inline para Widget Botonera -->
<?php
  $dirWorpress = get_template_directory_uri();
  if( wp_is_mobile() ) {
        /*Aqui se encuentra la funcionalidad del doble tap en la botonera y ocultar tooltips*/     
        $rutaWidget = $dirWorpress ."/assets/frontend/js/secciones/min/widget-botones.min.js";
    }else {
        /*Aqui se encuentra la funcionalidad de mostrar tooltips*/            
        $rutaWidget = $dirWorpress ."/assets/frontend_desktop/js/secciones/min/widget-botones.min.js";                
    }
  echo "<script defer type='text/javascript' src='$rutaWidget' id='script-inline-widget-botonera'>";
    // echo file_get_contents( $rutaWidget );
  echo "</script>";
?>
<!-- End Carga de javascript y css inline para Widget Botonera -->
<div id="botonera-gral" class="botonera-gral fixed-action-btn">
    <div id="chatMsjPC" class="chatMsj d-flex justify-content-end"></div>
    <a class="btn-floating btn-lg btn-principal" title="Más información"><?php echo do_shortcode( '[icono nombre="'.sanitize_title("solicita-informacion").'"][/icono]' ); ?></a>
    <ul class="list-unstyled">
       
      <?php if( wp_is_mobile() || strpos($post->post_name,'test-kranon') !== false ) { ?>
        <li class="tooltip-info-ctc"><span class="bot-contacto btn-floating m-1 btn-whatsapp" data-placement="left" id="openWhatsapp" onclick="openModal('#modal-whatsapp')" data-gtm-tr="evWhats" data-gtm-accion="intención" data-gtm-seccion="widget contacto" data-gtm-depto="" data-gtm-pos="intención whatsapp"><!-- <div id="whatsMsj-int" class="d-flex justify-content-end"></div> --><?php echo do_shortcode( '[icono nombre="'.sanitize_title("whatsapp").'"][/icono]' ); ?></span><span class="tooltiptext-ctc">Escríbenos tus dudas por whatsapp</span></li>
      <?php } ?>
     <?php if ($ocultarChat != true){ ?>
        <li class="tooltip-info-ctc"><span class="bot-contacto btn-floating m-1 btn-chat-botonera" data-placement="left" id="openChat"><div id="chatMsj-int-botonera" class="d-flex justify-content-end"></div><?php echo do_shortcode( '[icono nombre="'.sanitize_title("chat-card").'"][/icono]' ); ?></span><span class="tooltiptext-ctc">Chatea en línea</span></li>
      <?php }?>
      <li class="tooltip-info-ctc"><span class="bot-contacto btn-floating m-1 btn-calcu" data-placement="left" onclick="return clickCalcu(this)" data-gtm-pos="intencion calculadora">&dollar;</span><span class="tooltiptext-ctc">Calcula tu beca y colegiatura</span></li>
      <li class="tooltip-info-ctc"><span class="bot-contacto btn-floating m-1 btn-formulario" data-placement="left" id="ctc-btn-formulario" onclick="openModal('#modal_frm_app')" data-solicitud-location="widget contacto" data-gtm-pos="intencion formulario"><?php echo do_shortcode( '[icono nombre="'.sanitize_title("formularios").'"][/icono]' ); ?></span><span class="tooltiptext-ctc">Solicita información</span></li>
      <?php if( wp_is_mobile() ) { ?>
        <li class="tooltip-info-ctc"><span class="bot-contacto btn-floating m-1 btn-ctc" data-placement="left" onclick="return clickTel(this)" data-gtm-tr="pushCall" data-gtm-accion="intención llamada" data-gtm-pos="intencion llamada"><?php echo do_shortcode( '[icono nombre="'.sanitize_title("llamar-footer").'"][/icono]' ); ?></span><span class="tooltiptext-ctc">Llama a un asesor</span></li>
      <?php } ?>
    <?php /*if ($ocultarCTC != true ) {*/
      if (true != true ) {
    ?>
        <li class="tooltip-info-ctc"><span class="bot-contacto btn-floating m-1 btn-ctc" data-placement="left" id="openCTC" onclick="openModal('#modal-ctc')" data-gtm-tr="pushCall" data-gtm-accion="intención ctc" data-gtm-pos="intencion ctc"><?php echo do_shortcode( '[icono nombre="'.sanitize_title("llamar-footer").'"][/icono]' ); ?></span><span class="tooltiptext-ctc">Recibir la llamada de un asesor ahora</span></li>
    <?php }?>
    </ul>
</div>
<?php }else{

} ?>


<?php
  if ($ocultarCTC != true ) { 
?>
<!-------------------------------------- F O R M U L A R I O   D E   C T C ---------------------------------------->
<!-- Carga de javascript inline para Formulario Whatsapp -->
<?php
  $dirWorpress = get_template_directory_uri();
  if( wp_is_mobile() ) {
        $rutactc = $dirWorpress ."/assets/frontend/js/secciones/min/click-to-call.min.js";
    }else {
        $rutactc = $dirWorpress ."/assets/frontend_desktop/js/secciones/min/click-to-call.min.js";        
    }
    echo "<script defer type='text/javascript' src='$rutactc' id='script-inline-form-ctc'>";
        // echo file_get_contents( $rutactc );
    echo "</script>";
?>
<!-- End Carga de javascript inline para Formulario Whatsapp -->
<?php echo file_get_contents( $dirWorpress . "/form-ctc.php" ); ?>
<?php } ?>


<?php
  if ($ocultarWhatsapp != true )   { 
?>
<!-------------------------------------- F O R M U L A R I O   D E   W H A T S A P P ---------------------------------------->
<!-- Carga de javascript inline para Formulario Whatsapp -->
<?php
  $dirWorpress = get_template_directory_uri();
  if( wp_is_mobile() ) {
    if(strpos($post->post_name,'test-kranon') !== false){
       /*Aqui se encuentra la funcionalidad la del doble tap en la botonera y ocultar tooltips*/     
        $rutaFrmWhats = $dirWorpress ."/assets/frontend/js/secciones/min/form-whatsapp-kranon.min.js";
    }else{
       /*Aqui se encuentra la funcionalidad la del doble tap en la botonera y ocultar tooltips*/     
        $rutaFrmWhats = $dirWorpress ."/assets/frontend/js/secciones/min/form-whatsapp.min.js";
    }       
  }else {
    if(strpos($post->post_name,'test-kranon') !== false){
       /*Aqui se encuentra la funcionalidad la del doble tap en la botonera y ocultar tooltips*/     
        $rutaFrmWhats = $dirWorpress ."/assets/frontend_desktop/js/secciones/min/form-whatsapp-kranon.min.js";
    }else{
       /*Aqui se encuentra la funcionalidad la del doble tap en la botonera y ocultar tooltips*/     
        $rutaFrmWhats = $dirWorpress ."/assets/frontend_desktop/js/secciones/min/form-whatsapp.min.js";        
    }                 
  }
    echo "<script defer type='text/javascript' src='$rutaFrmWhats' id='script-inline-form-whatsapp'>";
        //echo file_get_contents( $rutaFrmWhats );
    echo "</script>";
    
?>

<!-- <link rel="stylesheet" type="text/css" href="<?php echo $dirWorpress; ?>/assets/frontend_desktop/css/secciones/min/form-whatsapp.min.css" media="none" onload="if(media!='all')media='all'" />
<script type="text/javascript"defer src="<?php echo $rutaFrmWhats; ?>"></script> -->
<?php echo file_get_contents( $dirWorpress . "/form-whatsapp.php" ); ?>
<?php } ?>

<?php
if ($ocultarChat != true)   { 
?>
<!-------------------------------------- F O R M U L A R I O   D E   C H A T -------------------------------------------->
<!-- Carga de javascript inline para Formulario Chat -->
<?php

  $dirWorpress = get_template_directory_uri();
  if ($ocultarChat != true ){ 
    if( wp_is_mobile() ) {
      //Aqui se encuentra la funcionalidad la del doble tap en la botonera y ocultar tooltips
      $rutaFrmWhats = $dirWorpress ."/assets/frontend/js/secciones/purecloud-chat.js";
    }else {
      //Aqui se encuentra la funcionalidad de mostrar tooltips
      $rutaFrmWhats = $dirWorpress ."/assets/frontend_desktop/js/secciones/purecloud-chat.js";        
    }
    ?>
    <script src="https://apps.mypurecloud.com/widgets/9.0/cxbus.min.js" onload="javascript:CXBus.configure({debug:false,pluginsPath:'https://apps.mypurecloud.com/widgets/9.0/plugins/'}); CXBus.loadPlugin('widgets-core');"></script>
    <?php   
    echo "<script defer type='text/javascript' src='$rutaFrmWhats' id='script-inline-form-chat'>";
          //echo file_get_contents( $rutaFrmWhats );
    echo "</script>";
    echo file_get_contents( $dirWorpress . "/secciones_generales/form-chat.php" );
  }  
}
?>

</body>
</html>