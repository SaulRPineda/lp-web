//Gruntfile unitec 2017 
module.exports = function(grunt) {
  //Variables de configuración de rutas
  //Ruta total donde se guardan los Js del tema
  var path_js = '../themes/temaunitec/assets/frontend/js/';
  //Ruta para los json
  var path_json = '../themes/temaunitec/assets/frontend/json/';
  //Rutas donde se guardan los js por página
  var path_js_vendor = 'vendor/';
  var path_js_generales = 'generales/';
  var path_js_home = 'home/';
  var path_js_campus = 'campus/';
  var path_js_producto = 'producto/';
  var path_js_secciones = 'secciones/';
  var path_js_integracion = 'calculadora_integracion/';
  var path_js_integracion_hb = 'calculadora_integracion_hb/';
  var path_js_integracion_form = 'formulario_integracion/';
  var path_js_autocomplete_form = 'formulario_autocomplete/';
  //Ruta donde se guardan los archivos sass del tema
  var path_sass = '../themes/temaunitec/assets/frontend/sass/';
  //Rutas donde se guardan los archivos sass por página
  var path_sass_vendor= 'vendor/';
  var path_sass_generales = 'generales/';
  var path_sass_home = 'home/';
  var path_sass_campus = 'campus/';
  var path_sass_producto = 'producto/';
  var path_sass_secciones = 'secciones/';
  //Ruta donde se guardan los css producto del proceso sass
  var path_css = '../themes/temaunitec/assets/frontend/css/';
  //Rutas donde se guardan los css por página
  var path_css_vendor = 'vendor/';
  var path_css_generales = 'generales/';
  var path_css_home = 'home/';
  var path_css_campus = 'campus/';
  var path_css_producto = 'producto/';
  var path_css_secciones = 'secciones/';
  //Archivos concatenados css
  var archivo_cssmin_concatenado_vendor = '../themes/temaunitec/assets/frontend/css/concatenados/vendor.min.css';
  var archivo_cssmin_concatenado_generales = '../themes/temaunitec/assets/frontend/css/concatenados/generales.min.css';
  var archivo_cssmin_concatenado_home = '../themes/temaunitec/assets/frontend/css/concatenados/home.min.css';
  var archivo_cssmin_concatenado_campus = '../themes/temaunitec/assets/frontend/css/concatenados/campus.min.css';
  var archivo_cssmin_concatenado_producto = '../themes/temaunitec/assets/frontend/css/concatenados/producto.min.css';
  var archivo_cssmin_concatenado_secciones = '../themes/temaunitec/assets/frontend/css/concatenados/secciones.min.css';
  //Archivos concatenados js
  var archivo_js_concatenado_json = '../themes/temaunitec/assets/frontend/json/concatenados/json.min.js';
  var archivo_js_concatenado_vendor = '../themes/temaunitec/assets/frontend/js/concatenados/vendor.min.js';
  var archivo_js_concatenado_generales = '../themes/temaunitec/assets/frontend/js/concatenados/generales.min.js';
  var archivo_js_concatenado_home = '../themes/temaunitec/assets/frontend/js/concatenados/home.min.js';
  var archivo_js_concatenado_campus = '../themes/temaunitec/assets/frontend/js/concatenados/campus.min.js';
  var archivo_js_concatenado_producto = '../themes/temaunitec/assets/frontend/js/concatenados/producto.min.js';
  var archivo_js_concatenado_secciones = '../themes/temaunitec/assets/frontend/js/concatenados/secciones.min.js';

  var path_js_angular = 'angularapp/';
  var path_js_angular_v6 = 'angularapp-v6/';
  var app_js_angular = '../themes/temaunitec/assets/frontend/js/angularapp/app/formApp.min.js';
  var app_js_angular_v6 = '../themes/temaunitec/assets/frontend/js/angularapp-v6/app/formApp.min.js';

  //Ruta Jquery Libraries
  var archivo_jquery_libraries = '../themes/temaunitec/assets/frontend/js/concatenados/jqueryLibraries.min.js';

  //Ruta Jquery Libraries DFP
  var archivo_jquery_libraries_DFP = '../themes/temaunitec/assets/frontend/js/vendor/min/jqueryLibrariesDFP.min.js';

  //Configuracion de compilacion
  var source_map_sass = false;
  var source_map_js = false;
  var drop_console_js = true;

  grunt.initConfig({
    minjson: {
      js_json: {
        files: [{
          expand: true,
          cwd: path_json,
          src: ['*.json', '!*.min.json'],
          dest: path_json + 'min/',
          ext: '.min.json',
          extDot: 'last'
        } ]
      },
      concatena_js_json: {
        src : path_json + 'min/**/*.min.json', 
        dest : archivo_js_concatenado_json
      },
    },
    uglify: {
      options: {
        sourceMap: false,
        compress: {
          drop_console: drop_console_js
        },
        mangle: {
          reserved: ['jQuery', 'Backbone']
        }
      },
      js_vendor: {
        files: [{
          expand: true,
          cwd: path_js + path_js_vendor,
          src: ['*.js', '!*.min.js'],
          dest: path_js + path_js_vendor + 'min/',
          ext: '.min.js',
          extDot: 'last'
        } ]
      },
      concatena_js_vendor: {
        src : path_js + path_js_vendor+ 'min/**/*.min.js', 
        dest : archivo_js_concatenado_vendor
      },
      js_generales: {
        files: [{
          expand: true,
          cwd: path_js + path_js_generales,
          src: ['*.js', '!*.min.js'],
          dest: path_js + path_js_generales + 'min/',
          ext: '.min.js',
          extDot: 'last'
        } ]
      },
      concatena_js_generales: {
        src : path_js + path_js_generales + 'min/**/*.min.js', 
        dest : archivo_js_concatenado_generales
      },
      js_home_unitec: {
        files: [{
          expand: true,
          cwd: path_js + path_js_home,
          src: ['*.js', '!*.min.js'],
          dest: path_js + path_js_home + 'min/',
          ext: '.min.js',
          extDot: 'last'
        } ]
      },
      concatena_js_home: {
        src : path_js + path_js_home + 'min/**/*.min.js', 
        dest : archivo_js_concatenado_home
      },
      js_campus_unitec: {
        files: [{
          expand: true,
          cwd: path_js + path_js_campus,
          src: ['*.js', '!*.min.js'],
          dest: path_js + path_js_campus + 'min/',
          ext: '.min.js',
          extDot: 'last'
        } ]
      },
      concatena_js_campus: {
        src : path_js + path_js_campus + 'min/**/*.min.js', 
        dest : archivo_js_concatenado_campus
      },
      js_producto_unitec: {
        files: [{
          expand: true,
          cwd: path_js + path_js_producto,
          src: ['*.js', '!*.min.js'],
          dest: path_js + path_js_producto + 'min/',
          ext: '.min.js',
          extDot: 'last'
        } ]
      },
      concatena_js_producto: {
        src : path_js + path_js_producto + 'min/**/*.min.js', 
        dest : archivo_js_concatenado_producto
      },
      js_secciones_unitec: {
        files: [{
          expand: true,
          cwd: path_js + path_js_secciones,
          src: ['*.js', '!*.min.js'],
          dest: path_js + path_js_secciones + 'min/',
          ext: '.min.js',
          extDot: 'last'
        } ]
      },
      js_integraciones_unitec: {
        files: [{
          expand: true,
          cwd: path_js + path_js_integracion,
          src: ['*.js', '!*.min.js'],
          dest: path_js + path_js_integracion + 'min/',
          ext: '.min.js',
          extDot: 'last'
        } ]
      },
      js_integraciones_unitec_hb: {
        files: [{
          expand: true,
          cwd: path_js + path_js_integracion_hb,
          src: ['*.js', '!*.min.js'],
          dest: path_js + path_js_integracion_hb + 'min/',
          ext: '.min.js',
          extDot: 'last'
        } ]
      },
      js_integraciones_form_unitec: {
        files: [{
          expand: true,
          cwd: path_js + path_js_integracion_form,
          src: ['*.js', '!*.min.js'],
          dest: path_js + path_js_integracion_form + 'min/',
          ext: '.min.js',
          extDot: 'last'
        } ]
      },
      js_integraciones_form_autocomplete: {
        files: [{
          expand: true,
          cwd: path_js + path_js_autocomplete_form,
          src: ['*.js', '!*.min.js'],
          dest: path_js + path_js_autocomplete_form + 'min/',
          ext: '.min.js',
          extDot: 'last'
        } ]
      },
      concatena_js_secciones: {
        src : path_js + path_js_secciones + 'min/**/*.min.js', 
        dest : archivo_js_concatenado_secciones
      },
      //Minificado app Angular By SRP
      // comprime_js_angular: {
      //   src : [
      //           path_js + path_js_angular + 'inline.bundle.js',
      //           path_js + path_js_angular + 'polyfills.bundle.js',
      //           path_js + path_js_angular + 'main.bundle.js'
      //   ], 
      //   dest : app_js_angular
      // },

      comprime_js_angular_v6: {
        src : [
                path_js + path_js_angular_v6 + 'runtime.js',
                path_js + path_js_angular_v6 + 'polyfills.js',
                path_js + path_js_angular_v6 + 'vendor.js',
                path_js + path_js_angular_v6 + 'main.js'
        ], 
        dest : app_js_angular_v6
      },

      //Minificado Jquery By SRP
      // comprime_jquery_library: {
      //   src : [
      //           path_js + path_js_vendor + 'min/jquery-3.1.1.min.js',
      //           path_js + path_js_vendor + 'min/jquery-ui.min.js',
      //           path_js + path_js_vendor + 'jquery.lazy.min.js'
      //   ], 
      //   dest : archivo_jquery_libraries
      // },

      //Minificado Jquery_DFP By SRP
      // comprime_jquery_library_DFP: {
      //   src : [
      //           path_js + path_js_vendor + 'min/sheetrock.min.js',
      //           path_js + path_js_vendor + 'min/handlebars.min.js'
      //   ], 
      //   dest : archivo_jquery_libraries_DFP
      // },
    },//Fin de la tarea uglify
    sass: {
      options: {
        sourcemap: 'none'
      },
      dist: {
        files: [
          {
            expand: true,
            cwd: path_sass + path_sass_vendor,
            src: ['*.scss'],
            dest: path_css + path_css_vendor,
            ext: '.css',
            extDot: 'last'
          },
          {
          expand: true,
          cwd: path_sass + path_sass_generales,
          src: ['*.scss'],
          dest: path_css + path_css_generales,
          ext: '.css',
          extDot: 'last'
          },
          {
            expand: true,
            cwd: path_sass + path_sass_home,
            src: ['*.scss'],
            dest: path_css + path_css_home,
            ext: '.css',
            extDot: 'last'
            },
          {
            expand: true,
            cwd: path_sass + path_sass_campus,
            src: ['*.scss'],
            dest: path_css + path_css_campus,
            ext: '.css',
            extDot: 'last'
          },
          {
            expand: true,
            cwd: path_sass + path_sass_producto,
            src: ['*.scss'],
            dest: path_css + path_css_producto,
            ext: '.css',
            extDot: 'last'
          },
          {
            expand: true,
            cwd: path_sass + path_sass_secciones,
            src: ['*.scss'],
            dest: path_css + path_css_secciones,
            ext: '.css',
            extDot: 'last'
            },
        ],
      }
    }, //Fin de la tarea Sass
    archivo_cssmin_concatenado_vendor: archivo_cssmin_concatenado_vendor,
    archivo_cssmin_concatenado_generales: archivo_cssmin_concatenado_generales,
    archivo_cssmin_concatenado_home: archivo_cssmin_concatenado_home,
    archivo_cssmin_concatenado_campus: archivo_cssmin_concatenado_campus,
    archivo_cssmin_concatenado_producto: archivo_cssmin_concatenado_producto,
    archivo_cssmin_concatenado_secciones: archivo_cssmin_concatenado_secciones,
    cssmin: {
      options: {
        keepSpecialComments: 0
      },
      cssmin_vendor: {
        files: [{
          expand: true,
          cwd: path_css + path_css_vendor,
          src: ['*.css', '*.min.css'],
          dest: path_css + path_css_vendor + 'min/',
          ext: '.min.css',
          extDot: 'last'
        }]
      },
     concatena_cssmin_vendor : {
        files: {
           '<%= archivo_cssmin_concatenado_vendor %>' : [path_css + path_css_vendor + 'min/*.min.css']
        } 
      },
      cssmin_generales: {
        files: [{
          expand: true,
          cwd: path_css + path_css_generales,
          src: ['*.css', '*.min.css'],
          dest: path_css + path_css_generales + 'min/',
          ext: '.min.css',
          extDot: 'last'
        }]
      },
      concatena_cssmin_generales : {
        files: {
           '<%= archivo_cssmin_concatenado_generales %>' : [path_css + path_css_generales + 'min/*.min.css']
        } 
      },
      cssmin_home: {
        files: [{
          expand: true,
          cwd: path_css + path_css_home,
          src: ['*.css', '*.min.css'],
          dest: path_css + path_css_home + 'min/',
          ext: '.min.css',
          extDot: 'last'
        }]
      },
      concatena_cssmin_home : {
        files: {
          '<%= archivo_cssmin_concatenado_home %>' : [path_css + path_css_home + 'min/*.min.css']
        } 
      },
      cssmin_campus: {
        files: [{
          expand: true,
          cwd: path_css + path_css_campus,
          src: ['*.css', '*.min.css'],
          dest: path_css + path_css_campus + 'min/',
          ext: '.min.css',
          extDot: 'last'
        }]
      },
      concatena_cssmin_campus : {
        files: {
          '<%= archivo_cssmin_concatenado_campus %>' : [path_css + path_css_campus + 'min/*.min.css']
        } 
      },
      cssmin_producto: {
        files: [{
          expand: true,
          cwd: path_css + path_css_producto,
          src: ['*.css', '*.min.css'],
          dest: path_css + path_css_producto + 'min/',
          ext: '.min.css',
          extDot: 'last'
        }]
      },
      concatena_cssmin_producto : {
        files: {
          '<%= archivo_cssmin_concatenado_producto %>' : [path_css + path_css_producto + 'min/*.min.css']
        } 
      },
      cssmin_secciones: {
        files: [{
          expand: true,
          cwd: path_css + path_css_secciones,
          src: ['*.css', '*.min.css'],
          dest: path_css + path_css_secciones + 'min/',
          ext: '.min.css',
          extDot: 'last'
        }]
      },
      concatena_cssmin_secciones : {
        files: {
          '<%= archivo_cssmin_concatenado_secciones %>' : [path_css + path_css_secciones + 'min/*.min.css']
        } 
      },
    },//Fin de la tarea cssmin
    watch: {
      configFiles: {
        files: [ 'Gruntfile.js'],
        options: {
            reload: true
        }
      },
      'json-minify': {
        files: [path_json + '**/*.json'],
        tasks: ['newer:json-minify:js_json'],
      },
      concatena_js_json: {
        files: [path_json + '**/*.min.json'],
        tasks: ['newer:json-minify:concatena_js_json'],
      },
      js_vendor: {
        files: [path_js + path_js_vendor + '**/*.js'],
        tasks: ['newer:uglify:js_vendor'],
      },
      concatena_js_vendor: {
        files: [path_js + path_js_vendor + '**/*.min.js'],
        tasks: ['newer:uglify:concatena_js_vendor'],
      },
      js_generales: {
        files: [path_js + path_js_generales + '**/*.js'],
        tasks: ['newer:uglify:js_generales'],
      },
      concatena_js_generales: {
        files: [path_js + path_js_generales + '**/*.min.js'],
        tasks: ['newer:uglify:concatena_js_generales'],
      },
      js_home_unitec: {
        files: [path_js + path_js_home+ '**/*.js'],
        tasks: ['newer:uglify:js_home_unitec'],
      },
      concatena_js_home: {
        files: [path_js + path_js_home + '**/*.min.js'],
        tasks: ['newer:uglify:concatena_js_home'],
      },
      js_campus_unitec: {
        files: [path_js + path_js_campus + '**/*.js'],
        tasks: ['newer:uglify:js_campus_unitec'],
      },
      concatena_js_campus: {
        files: [path_js + path_js_campus + '**/*.min.js'],
        tasks: ['newer:uglify:concatena_js_campus'],
      },
      js_producto_unitec: {
        files: [path_js + path_js_producto + '**/*.js'],
        tasks: ['newer:uglify:js_producto_unitec'],
      },
      concatena_js_producto: {
        files: [path_js + path_js_producto + '**/*.min.js'],
        tasks: ['newer:uglify:concatena_js_producto'],
      },
      js_secciones_unitec: {
        files: [path_js + path_js_secciones + '**/*.js'],
        tasks: ['newer:uglify:js_secciones_unitec'],
      },
      concatena_js_secciones: {
        files: [path_js + path_js_secciones + '**/*.min.js'],
        tasks: ['newer:uglify:concatena_js_secciones'],
      },
      sass: {
        files: [path_sass + '**/*.scss'],
        tasks: ['sass:dist'],
      },
      cssmin_vendor: {
        files: [path_css + path_css_vendor + '**/*.css'],
        tasks: ['newer:cssmin:cssmin_vendor'],
      },
      concatena_cssmin_vendor : {
        files: [path_css + path_css_vendor + '**/*.css'],
        tasks: ['newer:cssmin:concatena_cssmin_vendor'],
      },
      cssmin_generales : {
        files: [path_css + path_css_generales + '**/*.css'],
        tasks: ['newer:cssmin:cssmin_generales'],
      },
      concatena_cssmin_generales : {
        files: [path_css + path_css_generales + '**/*.css'],
        tasks: ['newer:cssmin:concatena_cssmin_generales'],
      },
      cssmin_home : {
        files: [path_css + path_css_home + '**/*.css'],
        tasks: ['newer:cssmin:cssmin_home'],
      },
      concatena_cssmin_home : {
        files: [path_css + path_css_home + '**/*.css'],
        tasks: ['newer:cssmin:concatena_cssmin_home'],
      },
      cssmin_campus : {
        files: [path_css + path_css_campus + '**/*.css'],
        tasks: ['newer:cssmin:cssmin_campus'],
      },
      concatena_cssmin_campus : {
        files: [path_css + path_css_campus + '**/*.css'],
        tasks: ['newer:cssmin:concatena_cssmin_campus'],
      },
      cssmin_producto : {
        files: [path_css + path_css_producto + '**/*.css'],
        tasks: ['newer:cssmin:cssmin_producto'],
      },
      concatena_cssmin_producto : {
        files: [path_css + path_css_campus + '**/*.css'],
        tasks: ['newer:cssmin:concatena_cssmin_producto'],
      },
       cssmin_secciones : {
        files: [path_css + path_css_secciones + '**/*.css'],
        tasks: ['newer:cssmin:cssmin_secciones'],
      },
      concatena_cssmin_secciones : {
        files: [path_css + path_css_campus + '**/*.css'],
        tasks: ['newer:cssmin:concatena_cssmin_secciones'],
      },
       php: {
          files: ['../themes/temaunitec/**/*.php']
      }
    },//Termina watch
    browserSync: {
      dev: {
        bsFiles: {
          src : [
            '../themes/temaunitec/**/*.php',
            '../themes/temaunitec/**/*.min.css',
            '../themes/temaunitec/**/*.min.js',
            '../themes/temaunitec/**/*.min.json'
          ]
        },
        options: {
          proxy: 'http://localhost:8888/unitec2017',
          open: true,
          watchTask: true
        }
      }
    },
    critical_css: {
    options: {
      width: 1200,
      height: 900,
    },
    your_target: {
      file: 'http://localhost:3000/',
      url: 'http://localhost:3000/', // Parse this page. 
      dest: 'dist/critical.css',     // Path to save critical css to. 
    },
  },

  });//Fin de init config
  //Carga de los modulos de grunt
  //Watch observa los archivos que modificas y realiza la cadena de procesos
  grunt.loadNpmTasks('grunt-contrib-watch');
  grunt.loadNpmTasks('grunt-contrib-uglify-es');
  grunt.loadNpmTasks('grunt-contrib-sass');
  grunt.loadNpmTasks('grunt-contrib-cssmin');
  grunt.loadNpmTasks('grunt-contrib-uglify-es');
  grunt.loadNpmTasks('grunt-minjson');
  grunt.loadNpmTasks('grunt-newer');
  grunt.loadNpmTasks('grunt-browser-sync');
  grunt.loadNpmTasks('grunt-critical-css');

  grunt.registerTask('default', ['browserSync', 'watch']);
};//Fin de module exports