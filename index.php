<?php
/**
 * Front to the WordPress application. This file doesn't do anything, but loads
 * wp-blog-header.php which does and tells WordPress to load the theme.
 *
 * @package WordPress
 */

/**
 * Tells WordPress to load the WordPress theme and output it.
 *
 * @var bool
 */

/* Función para detectar dispositivo By OAIC */
function trafico_es_mobil(){

    if ( empty($_SERVER['HTTP_USER_AGENT']) ) {
            $is_mobile = false;
        } elseif (
               strpos(strtolower($_SERVER['HTTP_USER_AGENT']), '240x320') !== false
            || strpos(strtolower($_SERVER['HTTP_USER_AGENT']), '2.0 ') !== false
            || strpos(strtolower($_SERVER['HTTP_USER_AGENT']), 'mmp') !== false
            || strpos(strtolower($_SERVER['HTTP_USER_AGENT']), 'bppcb') !== false
            || strpos(strtolower($_SERVER['HTTP_USER_AGENT']), 'alcatel') !== false
            || strpos(strtolower($_SERVER['HTTP_USER_AGENT']), 'amoi') !== false
            || strpos(strtolower($_SERVER['HTTP_USER_AGENT']), 'asus') !== false
            || strpos(strtolower($_SERVER['HTTP_USER_AGENT']), 'au-mic') !== false
            || strpos(strtolower($_SERVER['HTTP_USER_AGENT']), 'audiovox') !== false
            || strpos(strtolower($_SERVER['HTTP_USER_AGENT']), 'avantgo') !== false
            || strpos(strtolower($_SERVER['HTTP_USER_AGENT']), 'benq') !== false
            || strpos(strtolower($_SERVER['HTTP_USER_AGENT']), 'bird') !== false
            || strpos(strtolower($_SERVER['HTTP_USER_AGENT']), 'blackberry') !== false
            || strpos(strtolower($_SERVER['HTTP_USER_AGENT']), 'blazer') !== false
            || strpos(strtolower($_SERVER['HTTP_USER_AGENT']), 'cdm') !== false
            || strpos(strtolower($_SERVER['HTTP_USER_AGENT']), 'cellphone') !== false
            || strpos(strtolower($_SERVER['HTTP_USER_AGENT']), 'danger') !== false
            || strpos(strtolower($_SERVER['HTTP_USER_AGENT']), 'ddipocket') !== false
            || strpos(strtolower($_SERVER['HTTP_USER_AGENT']), 'docomo') !== false
            || strpos(strtolower($_SERVER['HTTP_USER_AGENT']), 'dopod') !== false
            || strpos(strtolower($_SERVER['HTTP_USER_AGENT']), 'elaine3.0') !== false
            || strpos(strtolower($_SERVER['HTTP_USER_AGENT']), 'ericsson') !== false
            || strpos(strtolower($_SERVER['HTTP_USER_AGENT']), 'eudoraweb') !== false
            || strpos(strtolower($_SERVER['HTTP_USER_AGENT']), 'fly') !== false
            || strpos(strtolower($_SERVER['HTTP_USER_AGENT']), 'haier') !== false
            || strpos(strtolower($_SERVER['HTTP_USER_AGENT']), 'hiptop') !== false
            || strpos(strtolower($_SERVER['HTTP_USER_AGENT']), 'hp.ipaq') !== false
            || strpos(strtolower($_SERVER['HTTP_USER_AGENT']), 'htc') !== false
            || strpos(strtolower($_SERVER['HTTP_USER_AGENT']), 'huawei') !== false
            || strpos(strtolower($_SERVER['HTTP_USER_AGENT']), 'i-mobile') !== false
            || strpos(strtolower($_SERVER['HTTP_USER_AGENT']), 'iemobile') !== false
            || strpos(strtolower($_SERVER['HTTP_USER_AGENT']), 'iemobile7') !== false
            || strpos(strtolower($_SERVER['HTTP_USER_AGENT']), 'iemobile/9') !== false
            || strpos(strtolower($_SERVER['HTTP_USER_AGENT']), 'j-phone') !== false
            || strpos(strtolower($_SERVER['HTTP_USER_AGENT']), 'kddi') !== false
            || strpos(strtolower($_SERVER['HTTP_USER_AGENT']), 'konka') !== false
            || strpos(strtolower($_SERVER['HTTP_USER_AGENT']), 'kwc') !== false
            || strpos(strtolower($_SERVER['HTTP_USER_AGENT']), 'kyocerawx310k') !== false
            || strpos(strtolower($_SERVER['HTTP_USER_AGENT']), 'lenovo') !== false
            || strpos(strtolower($_SERVER['HTTP_USER_AGENT']), 'lg') !== false
            || strpos(strtolower($_SERVER['HTTP_USER_AGENT']), 'lge') !== false
            || strpos(strtolower($_SERVER['HTTP_USER_AGENT']), 'midp') !== false
            || strpos(strtolower($_SERVER['HTTP_USER_AGENT']), 'midp-2.0') !== false
            || strpos(strtolower($_SERVER['HTTP_USER_AGENT']), 'mmef20') !== false
            || strpos(strtolower($_SERVER['HTTP_USER_AGENT']), 'mmp') !== false
            || strpos(strtolower($_SERVER['HTTP_USER_AGENT']), 'mobilephone') !== false
            || strpos(strtolower($_SERVER['HTTP_USER_AGENT']), 'mot-v') !== false
            || strpos(strtolower($_SERVER['HTTP_USER_AGENT']), 'motorola') !== false
            || strpos(strtolower($_SERVER['HTTP_USER_AGENT']), 'msie 10.0') !== false
            || strpos(strtolower($_SERVER['HTTP_USER_AGENT']), 'netfront') !== false
            || strpos(strtolower($_SERVER['HTTP_USER_AGENT']), 'newgen') !== false
            || strpos(strtolower($_SERVER['HTTP_USER_AGENT']), 'newt') !== false
            || strpos(strtolower($_SERVER['HTTP_USER_AGENT']), 'nintendo ds') !== false
            || strpos(strtolower($_SERVER['HTTP_USER_AGENT']), 'nintendo wii') !== false
            || strpos(strtolower($_SERVER['HTTP_USER_AGENT']), 'nitro') !== false
            || strpos(strtolower($_SERVER['HTTP_USER_AGENT']), 'nokia') !== false
            || strpos(strtolower($_SERVER['HTTP_USER_AGENT']), 'novarra') !== false
            || strpos(strtolower($_SERVER['HTTP_USER_AGENT']), 'o2') !== false
            || strpos(strtolower($_SERVER['HTTP_USER_AGENT']), 'openweb') !== false
            || strpos(strtolower($_SERVER['HTTP_USER_AGENT']), 'opera mobi') !== false
            || strpos(strtolower($_SERVER['HTTP_USER_AGENT']), 'opera.mobi') !== false
            || strpos(strtolower($_SERVER['HTTP_USER_AGENT']), 'p160u') !== false
            || strpos(strtolower($_SERVER['HTTP_USER_AGENT']), 'palm') !== false
            || strpos(strtolower($_SERVER['HTTP_USER_AGENT']), 'panasonic') !== false
            || strpos(strtolower($_SERVER['HTTP_USER_AGENT']), 'pantech') !== false
            || strpos(strtolower($_SERVER['HTTP_USER_AGENT']), 'pdxgw') !== false
            || strpos(strtolower($_SERVER['HTTP_USER_AGENT']), 'pg') !== false
            || strpos(strtolower($_SERVER['HTTP_USER_AGENT']), 'philips') !== false
            || strpos(strtolower($_SERVER['HTTP_USER_AGENT']), 'phone') !== false
            || strpos(strtolower($_SERVER['HTTP_USER_AGENT']), 'playbook') !== false
            || strpos(strtolower($_SERVER['HTTP_USER_AGENT']), 'playstation portable') !== false
            || strpos(strtolower($_SERVER['HTTP_USER_AGENT']), 'portalmmm') !== false
            || strpos(strtolower($_SERVER['HTTP_USER_AGENT']), 'proxinet') !== false
            || strpos(strtolower($_SERVER['HTTP_USER_AGENT']), 'psp') !== false
            || strpos(strtolower($_SERVER['HTTP_USER_AGENT']), 'qtek') !== false
            || strpos(strtolower($_SERVER['HTTP_USER_AGENT']), 'sagem') !== false
            || strpos(strtolower($_SERVER['HTTP_USER_AGENT']), 'samsung') !== false
            || strpos(strtolower($_SERVER['HTTP_USER_AGENT']), 'sanyo') !== false
            || strpos(strtolower($_SERVER['HTTP_USER_AGENT']), 'sch') !== false
            || strpos(strtolower($_SERVER['HTTP_USER_AGENT']), 'sch-i800') !== false
            || strpos(strtolower($_SERVER['HTTP_USER_AGENT']), 'sec') !== false
            || strpos(strtolower($_SERVER['HTTP_USER_AGENT']), 'sendo') !== false
            || strpos(strtolower($_SERVER['HTTP_USER_AGENT']), 'sgh') !== false
            || strpos(strtolower($_SERVER['HTTP_USER_AGENT']), 'sharp') !== false
            || strpos(strtolower($_SERVER['HTTP_USER_AGENT']), 'sharp-tq-gx10') !== false
            || strpos(strtolower($_SERVER['HTTP_USER_AGENT']), 'small') !== false
            || strpos(strtolower($_SERVER['HTTP_USER_AGENT']), 'smartphone') !== false
            || strpos(strtolower($_SERVER['HTTP_USER_AGENT']), 'softbank') !== false
            || strpos(strtolower($_SERVER['HTTP_USER_AGENT']), 'sonyericsson') !== false
            || strpos(strtolower($_SERVER['HTTP_USER_AGENT']), 'sph') !== false
            || strpos(strtolower($_SERVER['HTTP_USER_AGENT']), 'symbian') !== false
            || strpos(strtolower($_SERVER['HTTP_USER_AGENT']), 'symbian os') !== false
            || strpos(strtolower($_SERVER['HTTP_USER_AGENT']), 'symbianos') !== false
            || strpos(strtolower($_SERVER['HTTP_USER_AGENT']), 'toshiba') !== false
            || strpos(strtolower($_SERVER['HTTP_USER_AGENT']), 'treo') !== false
            || strpos(strtolower($_SERVER['HTTP_USER_AGENT']), 'ts21i-10') !== false
            || strpos(strtolower($_SERVER['HTTP_USER_AGENT']), 'up.browse') !== false
            || strpos(strtolower($_SERVER['HTTP_USER_AGENT']), 'up.link') !== false
            || strpos(strtolower($_SERVER['HTTP_USER_AGENT']), 'uts') !== false
            || strpos(strtolower($_SERVER['HTTP_USER_AGENT']), 'vertu') !== false
            || strpos(strtolower($_SERVER['HTTP_USER_AGENT']), 'vodafone') !== false
            || strpos(strtolower($_SERVER['HTTP_USER_AGENT']), 'wap') !== false
            || strpos(strtolower($_SERVER['HTTP_USER_AGENT']), 'willcome') !== false
            || strpos(strtolower($_SERVER['HTTP_USER_AGENT']), 'windows ce') !== false
            || strpos(strtolower($_SERVER['HTTP_USER_AGENT']), 'windows.ce') !== false
            || strpos(strtolower($_SERVER['HTTP_USER_AGENT']), 'winwap') !== false
            || strpos(strtolower($_SERVER['HTTP_USER_AGENT']), 'xda') !== false
            || strpos(strtolower($_SERVER['HTTP_USER_AGENT']), 'xoom') !== false
            || strpos(strtolower($_SERVER['HTTP_USER_AGENT']), 'zte') !== false) {
                $is_mobile = true;
            } else {$is_mobile = false; }
            return $is_mobile;
        }
/* End Función para detectar dispositivo By OAIC */

define('WP_USE_THEMES', true);

/** Loads the WordPress Environment and Template */
require( dirname( __FILE__ ) . '/wp-blog-header.php' );
