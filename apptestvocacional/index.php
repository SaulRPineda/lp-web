<?php 
	session_start();

?>

<html lang="es">
	<head>
		<?php include 'cabeceras.php';?>
		 <script type="text/javascript" src="js/validar_reg.js"></script>
		 <script type="text/javascript" src="js/cookies.js"></script>
		<title>Test Vocacional</title>
		<style type="text/css">
  .overlay {
  position: fixed; /* Sit on top of the page content */
  display: block; /* Hidden by default */
  width: 100%; /* Full width (cover the whole page) */
  height: 100%; /* Full height (cover the whole page) */
  top: 0; 
  left: 0;
  right: 0;
  bottom: 0;
  background-color: white; /* Black background with opacity */
  z-index: 200; /* Specify a stack order in case you're using a different order for other elements */
  cursor: pointer; /* Add a pointer on hover */
}
</style>

	</head>

<body class="background">
	<div id="overlay-oculta" class="overlay"><h3 id="ocultRedir">Redirigiendo...</h3></div>
	<div class="container-fluid">
		<div class="fixed-top row header-vocacional">
			<div class="col-md-2 logo">
				<a href="//www.unitec.mx" class="p-2">
					<img src="/wp-content/themes/temaunitec/assets/frontend_desktop/img/header/unitec.svg">
				</a>
			</div>
			
		</div>
	</div>
	<div class="container" id="test_voca">
		<div class="row">
			<div class="col-lg-12 text-center"><h1>TEST VOCACIONAL</h1></div>
			<!-- <div class="col-lg-2"><img src="images/testvocacional/la-reta-logo.png" style="width: 200px; margin-top: 14px;"></div> -->
		</div>
		<div class="row" id="preguntas_test">
			<div class="col-lg-12">
				<div class="row">
					<div class="col-lg-10 col-md-10 col-sm-10 col-xs-8">
					</div>
					<div class="col-lg-1 col-md-1 col-sm-1 col-xs-2">
						<center class="like">
							<img src="images/testvocacional/like.svg" style="width:30px; margin-top: -15px;">
							<small class="hidden-xs hidden-sm">Me gusta</small>
						</center>
					</div>
					<div class="col-lg-1 col-md-1 col-sm-1 col-xs-2">
						<center class="like">
							<img src="images/testvocacional/dislike.svg" style="width:30px; margin-top: -15px;">
							<small class="hidden-xs hidden-sm">No me gusta</small>
						</center>
					</div>
				</div>

				<!-- INICIO DE GRUPO 1 DE PREGUNTAS (1-8) -->
				<div id="grupo1_vocacional">
					<!-- Pregunta 1 -->
					<div class="row margin-top">
						<div class="col-lg-10 col-md-10 col-sm-10 col-xs-8">
						1. Diseñar programas de computación y nuevas aplicaciones
						</div>
						<div class="col-lg-1 col-md-1 col-sm-1 col-xs-2">
							<center><input type="radio" name="pregunta1" id="pregunta1a" class="area1 css-checkbox" value="1"><label for="pregunta1a" class="css-label"></label></center>
						</div>
						<div class="col-lg-1 col-md-1 col-sm-1 col-xs-2">
							<center><input type="radio" name="pregunta1" id="pregunta1b" class=" css-checkboxRED" value="0"><label for="pregunta1b" class="css-labelRED"></label></center>
						</div>
						<div class="col-lg-1 col-md-1 col-sm-1 col-xs-1">
						</div>
					</div>
					<!-- Pregunta 2 -->
					<div class="row margin-top">
						<div class="col-lg-10 col-md-10 col-sm-10 col-xs-8">
						2. Criar y cuidar animales
						</div>
						<div class="col-lg-1 col-md-1 col-sm-1 col-xs-2">
							<center><input type="radio" name="pregunta2" id="pregunta2a" class="area2 css-checkbox" value="1"><label for="pregunta2a" class="css-label"></label></center>
						</div>
						<div class="col-lg-1 col-md-1 col-sm-1 col-xs-2">
							<center><input type="radio" name="pregunta2" id="pregunta2b" class=" css-checkboxRED" value="0"><label for="pregunta2b" class="css-labelRED"></label></center>
						</div>
						<div class="col-lg-1 col-md-1 col-sm-1 col-xs-1">
						</div>
					</div>
					<!-- Pregunta 3 -->
					<div class="row margin-top">
						<div class="col-lg-10 col-md-10 col-sm-10 col-xs-8">
						3. Investigar sobre el cambio climático
						</div>
						<div class="col-lg-1 col-md-1 col-sm-1 col-xs-2">
							<center><input type="radio" name="pregunta3" id="pregunta3a" class="area2 css-checkbox" value="1"><label for="pregunta3a" class="css-label"></label></center>
						</div>
						<div class="col-lg-1 col-md-1 col-sm-1 col-xs-2">
							<center><input type="radio" name="pregunta3" id="pregunta3b" class=" css-checkboxRED" value="0"><label for="pregunta3b" class="css-labelRED"></label></center>
						</div>
						<div class="col-lg-1 col-md-1 col-sm-1 col-xs-1">
						</div>
					</div>
					<!-- Pregunta 4 -->
					<div class="row margin-top">
						<div class="col-lg-10 col-md-10 col-sm-10 col-xs-8">
						4. Ilustrar, animar y dibujar con computadora
						</div>
						<div class="col-lg-1 col-md-1 col-sm-1 col-xs-2">
							<center><input type="radio" name="pregunta4" id="pregunta4a" class="area4 css-checkbox" value="1"><label for="pregunta4a" class="css-label"></label></center>
						</div>
						<div class="col-lg-1 col-md-1 col-sm-1 col-xs-2">
							<center><input type="radio" name="pregunta4" id="pregunta4b" class=" css-checkboxRED" value="0"><label for="pregunta4b" class="css-labelRED"></label></center>
						</div>
						<div class="col-lg-1 col-md-1 col-sm-1 col-xs-1">
						</div>
					</div>
					<!-- Pregunta 5 -->
					<div class="row margin-top">
						<div class="col-lg-10 col-md-10 col-sm-10 col-xs-8">
						5. Hacer excavaciones para descubrir fósiles y restos antiguos
						</div>
						<div class="col-lg-1 col-md-1 col-sm-1 col-xs-2">
							<center><input type="radio" name="pregunta5" id="pregunta5a" class="area4 css-checkbox" value="1"><label for="pregunta5a" class="css-label"></label></center>
						</div>
						<div class="col-lg-1 col-md-1 col-sm-1 col-xs-2">
							<center><input type="radio" name="pregunta5" id="pregunta5b" class=" css-checkboxRED" value="0"><label for="pregunta5b" class="css-labelRED"></label></center>
						</div>
						<div class="col-lg-1 col-md-1 col-sm-1 col-xs-1">
						</div>
					</div>
					<!-- Pregunta 6 -->
					<div class="row margin-top">
						<div class="col-lg-10 col-md-10 col-sm-10 col-xs-8">
						6. Realizar cálculos para construir un puente
						</div>
						<div class="col-lg-1 col-md-1 col-sm-1 col-xs-2">
							<center><input type="radio" name="pregunta6" id="pregunta6a" class="area1 css-checkbox" value="1"><label for="pregunta6a" class="css-label"></label></center>
						</div>
						<div class="col-lg-1 col-md-1 col-sm-1 col-xs-2">
							<center><input type="radio" name="pregunta6" id="pregunta6b" class=" css-checkboxRED" value="0"><label for="pregunta6b" class="css-labelRED"></label></center>
						</div>
						<div class="col-lg-1 col-md-1 col-sm-1 col-xs-1">
						</div>
					</div>
					<!-- Pregunta 7 -->
					<div class="row margin-top">
						<div class="col-lg-10 col-md-10 col-sm-10 col-xs-8">
						7. Diseñar cursos para enseñar higiene y salud
						</div>
						<div class="col-lg-1 col-md-1 col-sm-1 col-xs-2">
							<center><input type="radio" name="pregunta7" id="pregunta7a" class="area2 css-checkbox" value="1"><label for="pregunta7a" class="css-label"></label></center>
						</div>
						<div class="col-lg-1 col-md-1 col-sm-1 col-xs-2">
							<center><input type="radio" name="pregunta7" id="pregunta7b" class=" css-checkboxRED" value="0"><label for="pregunta7b" class="css-labelRED"></label></center>
						</div>
						<div class="col-lg-1 col-md-1 col-sm-1 col-xs-1">
						</div>
					</div>
					<!-- Pregunta 8 -->
					<div class="row margin-top">
						<div class="col-lg-10 col-md-10 col-sm-10 col-xs-8">
						8. Tocar un instrumento y componer música
						</div>
						<div class="col-lg-1 col-md-1 col-sm-1 col-xs-2">
							<center><input type="radio" name="pregunta8" id="pregunta8a" class="area4 css-checkbox" value="1"><label for="pregunta8a" class="css-label"></label></center>
						</div>
						<div class="col-lg-1 col-md-1 col-sm-1 col-xs-2">
							<center><input type="radio" name="pregunta8" id="pregunta8b" class=" css-checkboxRED" value="0"><label for="pregunta8b" class="css-labelRED"></label></center>
						</div>
						<div class="col-lg-1 col-md-1 col-sm-1 col-xs-1">
						</div>
					</div>
					<div class="row">
						<div class="col-lg-2 col-md-2 col-sm-4 col-xs-4">
						</div>
						<div class="col-lg-8 col-md-6 col-sm-3 col-xs-3"></div>
						<div class="col-lg-2 col-md-4 col-sm-5 col-xs-5">
							<button id="btn-next-1-voc" type="button"  class="btn btn-default btn-siguiente">Siguiente </button>
						</div>
					</div>
				</div>
				<!-- FIN DE GRUPO 1 DE PREGUNTAS (1-8) -->
				<!-- GRUPO 2 DE PREGUNTAS (9-16) -->
				<div id="grupo2_vocacional">
					<!-- Pregunta 9 -->
					<div class="row margin-top">
						<div class="col-lg-10 col-md-10 col-sm-10 col-xs-8">
						9. Planificar las metas de una organización a mediano y largo plazo
						</div>
						<div class="col-lg-1 col-md-1 col-sm-1 col-xs-2">
							<center><input type="radio" name="pregunta9" id="pregunta9a" class="area3 css-checkbox" value="1"><label for="pregunta9a" class="css-label"></label></center>
						</div>
						<div class="col-lg-1 col-md-1 col-sm-1 col-xs-2">
							<center><input type="radio" name="pregunta9" id="pregunta9b" class=" css-checkboxRED" value="0"><label for="pregunta9b" class="css-labelRED"></label></center>
						</div>
						<div class="col-lg-1 col-md-1 col-sm-1 col-xs-1">
						</div>
					</div>
					<!-- Pregunta 10 -->
					<div class="row margin-top">
						<div class="col-lg-10 col-md-10 col-sm-10 col-xs-8">
						10. Planificar la producción de productos como: muebles, autos envases, etc.
						</div>
						<div class="col-lg-1 col-md-1 col-sm-1 col-xs-2">
							<center><input type="radio" name="pregunta10" id="pregunta10a" class="area3 css-checkbox" value="1"><label for="pregunta10a" class="css-label"></label></center>
						</div>
						<div class="col-lg-1 col-md-1 col-sm-1 col-xs-2">
							<center><input type="radio" name="pregunta10" id="pregunta10b" class=" css-checkboxRED" value="0"><label for="pregunta10b" class="css-labelRED"></label></center>
						</div>
						<div class="col-lg-1 col-md-1 col-sm-1 col-xs-1">
						</div>
					</div>
					<!-- Pregunta 11 -->
					<div class="row margin-top">
						<div class="col-lg-10 col-md-10 col-sm-10 col-xs-8">
						11. Diseñar logotipos y portadas de revistas
						</div>
						<div class="col-lg-1 col-md-1 col-sm-1 col-xs-2">
							<center><input type="radio" name="pregunta11" id="pregunta11a" class="area4 css-checkbox" value="1"><label for="pregunta11a" class="css-label"></label></center>
						</div>
						<div class="col-lg-1 col-md-1 col-sm-1 col-xs-2">
							<center><input type="radio" name="pregunta11" id="pregunta11b" class=" css-checkboxRED" value="0"><label for="pregunta11b" class="css-labelRED"></label></center>
						</div>
						<div class="col-lg-1 col-md-1 col-sm-1 col-xs-1">
						</div>
					</div>
					<!-- Pregunta 12 -->
					<div class="row margin-top">
						<div class="col-lg-10 col-md-10 col-sm-10 col-xs-8">
						12. Atender la salud de personas enfermas
						</div>
						<div class="col-lg-1 col-md-1 col-sm-1 col-xs-2">
							<center><input type="radio" name="pregunta12" id="pregunta12a" class="area2 css-checkbox" value="1"><label for="pregunta12a" class="css-label"></label></center>
						</div>
						<div class="col-lg-1 col-md-1 col-sm-1 col-xs-2">
							<center><input type="radio" name="pregunta12" id="pregunta12b" class=" css-checkboxRED" value="0"><label for="pregunta12b" class="css-labelRED"></label></center>
						</div>
						<div class="col-lg-1 col-md-1 col-sm-1 col-xs-1">
						</div>
					</div>
					<!-- Pregunta 13 -->
					<div class="row margin-top">
						<div class="col-lg-10 col-md-10 col-sm-10 col-xs-8">
						13. Llevar las finanzas de una organización
						</div>
						<div class="col-lg-1 col-md-1 col-sm-1 col-xs-2">
							<center><input type="radio" name="pregunta13" id="pregunta13a" class="area3 css-checkbox" value="1"><label for="pregunta13a" class="css-label"></label></center>
						</div>
						<div class="col-lg-1 col-md-1 col-sm-1 col-xs-2">
							<center><input type="radio" name="pregunta13" id="pregunta13b" class=" css-checkboxRED" value="0"><label for="pregunta13b" class="css-labelRED"></label></center>
						</div>
						<div class="col-lg-1 col-md-1 col-sm-1 col-xs-1">
						</div>
					</div>
					<!-- Pregunta 14 -->
					<div class="row margin-top">
						<div class="col-lg-10 col-md-10 col-sm-10 col-xs-8">
						14. Hacer experimentos con plantas
						</div>
						<div class="col-lg-1 col-md-1 col-sm-1 col-xs-2">
							<center><input type="radio" name="pregunta14" id="pregunta14a" class="area2 css-checkbox" value="1"><label for="pregunta14a" class="css-label"></label></center>
						</div>
						<div class="col-lg-1 col-md-1 col-sm-1 col-xs-2">
							<center><input type="radio" name="pregunta14" id="pregunta14b" class=" css-checkboxRED" value="0"><label for="pregunta14b" class="css-labelRED"></label></center>
						</div>
						<div class="col-lg-1 col-md-1 col-sm-1 col-xs-1">
						</div>
					</div>
					<!-- Pregunta 15 -->
					<div class="row margin-top">
						<div class="col-lg-10 col-md-10 col-sm-10 col-xs-8">
						15. Hacer planos para casas y edificios
						</div>
						<div class="col-lg-1 col-md-1 col-sm-1 col-xs-2">
							<center><input type="radio" name="pregunta15" id="pregunta15a" class="area1 css-checkbox" value="1"><label for="pregunta15a" class="css-label"></label></center>
						</div>
						<div class="col-lg-1 col-md-1 col-sm-1 col-xs-2">
							<center><input type="radio" name="pregunta15" id="pregunta15b" class=" css-checkboxRED" value="0"><label for="pregunta15b" class="css-labelRED"></label></center>
						</div>
						<div class="col-lg-1 col-md-1 col-sm-1 col-xs-1">
						</div>
					</div>
					<!-- Pregunta 16 -->
					<div class="row margin-top">
						<div class="col-lg-10 col-md-10 col-sm-10 col-xs-8">
						16. Desarrollar productos farmacéuticos
						</div>
						<div class="col-lg-1 col-md-1 col-sm-1 col-xs-2">
							<center><input type="radio" name="pregunta16" id="pregunta16a" class="area2 css-checkbox" value="1"><label for="pregunta16a" class="css-label"></label></center>
						</div>
						<div class="col-lg-1 col-md-1 col-sm-1 col-xs-2">
							<center><input type="radio" name="pregunta16" id="pregunta16b" class=" css-checkboxRED" value="0"><label for="pregunta16b" class="css-labelRED"></label></center>
						</div>
						<div class="col-lg-1 col-md-1 col-sm-1 col-xs-1">
						</div>
					</div>
					<div class="row">
						<div class="col-lg-2 col-md-2 col-sm-4 col-xs-4">
							<button id="btn-back-2-voc" type="button" class="btn btn-default  margin-top btn-siguiente">Atras </button>
						</div>
						<div class="col-lg-8 col-md-6 col-sm-3 col-xs-3"></div>
						<div class="col-lg-2 col-md-4 col-sm-5 col-xs-5">
							<button id="btn-next-2-voc" type="button" class="btn btn-default  margin-top btn-siguiente">Siguiente </button>
						</div>
					</div>
				</div>
				<div id="grupo3_vocacional">
				<!-- Pregunta 17 -->
				<div class="row margin-top">
					<div class="col-lg-10 col-md-10 col-sm-10 col-xs-8">
					17. Hacer estrategias para aprovechar relaciones económicas entre países
					</div>
					<div class="col-lg-1 col-md-1 col-sm-1 col-xs-2">
						<center><input type="radio" name="pregunta17" id="pregunta17a" class="area3 css-checkbox" value="1"><label for="pregunta17a" class="css-label"></label></center>
					</div>
					<div class="col-lg-1 col-md-1 col-sm-1 col-xs-2">
						<center><input type="radio" name="pregunta17" id="pregunta17b" class=" css-checkboxRED" value="0"><label for="pregunta17b" class="css-labelRED"></label></center>
					</div>
				</div>
				<!-- Pregunta 18 -->
				<div class="row margin-top">
					<div class="col-lg-10 col-md-10 col-sm-10 col-xs-8">
					18. Pintar, hacer esculturas, ilustrar libros
					</div>
					<div class="col-lg-1 col-md-1 col-sm-1 col-xs-2">
						<center><input type="radio" name="pregunta18" id="pregunta18a" class="area4 css-checkbox" value="1"><label for="pregunta18a" class="css-label"></label></center>
					</div>
					<div class="col-lg-1 col-md-1 col-sm-1 col-xs-2">
						<center><input type="radio" name="pregunta18" id="pregunta18b" class=" css-checkboxRED" value="0"><label for="pregunta18b" class="css-labelRED"></label></center>
					</div>
				</div>
				<!-- Pregunta 19 -->
				<div class="row margin-top">
					<div class="col-lg-10 col-md-10 col-sm-10 col-xs-8">
					19. Realizar campañas para un nuevo producto en el mercado
					</div>
					<div class="col-lg-1 col-md-1 col-sm-1 col-xs-2">
						<center><input type="radio" name="pregunta19" id="pregunta19a" class="area3 css-checkbox" value="1"><label for="pregunta19a" class="css-label"></label></center>
					</div>
					<div class="col-lg-1 col-md-1 col-sm-1 col-xs-2">
						<center><input type="radio" name="pregunta19" id="pregunta19b" class=" css-checkboxRED" value="0"><label for="pregunta19b" class="css-labelRED"></label></center>
					</div>
				</div>
				<!-- Pregunta 20 -->
				<div class="row margin-top">
					<div class="col-lg-10 col-md-10 col-sm-10 col-xs-8">
					20. Examinar y curar problemas de la vista
					</div>
					<div class="col-lg-1 col-md-1 col-sm-1 col-xs-2">
						<center><input type="radio" name="pregunta20" id="pregunta20a" class="area2 css-checkbox" value="1"><label for="pregunta20a" class="css-label"></label></center>
					</div>
					<div class="col-lg-1 col-md-1 col-sm-1 col-xs-2">
						<center><input type="radio" name="pregunta20" id="pregunta20b" class=" css-checkboxRED" value="0"><label for="pregunta20b" class="css-labelRED"></label></center>
					</div>
				</div>
				<!-- Pregunta 21 -->
				<div class="row margin-top">
					<div class="col-lg-10 col-md-10 col-sm-10 col-xs-8">
					21. Defender en juicios a personas y empresas
					</div>
					<div class="col-lg-1 col-md-1 col-sm-1 col-xs-2">
						<center><input type="radio" name="pregunta21" id="pregunta21a" class="area3 css-checkbox" value="1"><label for="pregunta21a" class="css-label"></label></center>
					</div>
					<div class="col-lg-1 col-md-1 col-sm-1 col-xs-2">
						<center><input type="radio" name="pregunta21" id="pregunta21b" class=" css-checkboxRED" value="0"><label for="pregunta21b" class="css-labelRED"></label></center>
					</div>
				</div>
				<!-- Pregunta 22 -->
				<div class="row margin-top">
					<div class="col-lg-10 col-md-10 col-sm-10 col-xs-8">
					22. Diseñar máquinas que simulen actividades humanas
					</div>
					<div class="col-lg-1 col-md-1 col-sm-1 col-xs-2">
						<center><input type="radio" name="pregunta22" id="pregunta22a" class="area1 css-checkbox" value="1"><label for="pregunta22a" class="css-label"></label></center>
					</div>
					<div class="col-lg-1 col-md-1 col-sm-1 col-xs-2">
						<center><input type="radio" name="pregunta22" id="pregunta22b" class=" css-checkboxRED" value="0"><label for="pregunta22b" class="css-labelRED"></label></center>
					</div>
				</div>
				<!-- Pregunta 23 -->
				<div class="row margin-top">
					<div class="col-lg-10 col-md-10 col-sm-10 col-xs-8">
					23. Investigar las causas y efectos de los problemas emocionales
					</div>
					<div class="col-lg-1 col-md-1 col-sm-1 col-xs-2">
						<center><input type="radio" name="pregunta23" id="pregunta23a" class="area2 css-checkbox" value="1"><label for="pregunta23a" class="css-label"></label></center>
					</div>
					<div class="col-lg-1 col-md-1 col-sm-1 col-xs-2">
						<center><input type="radio" name="pregunta23" id="pregunta23b" class=" css-checkboxRED" value="0"><label for="pregunta23b" class="css-labelRED"></label></center>
					</div>
				</div>
				<!-- Pregunta 24 -->
				<div class="row margin-top">
					<div class="col-lg-10 col-md-10 col-sm-10 col-xs-8">
					24. Supervisar ventas de un centro comercial
					</div>
					<div class="col-lg-1 col-md-1 col-sm-1 col-xs-2">
						<center><input type="radio" name="pregunta24" id="pregunta24a" class="area3 css-checkbox" value="1"><label for="pregunta24a" class="css-label"></label></center>
					</div>
					<div class="col-lg-1 col-md-1 col-sm-1 col-xs-2">
						<center><input type="radio" name="pregunta24" id="pregunta24b" class=" css-checkboxRED" value="0"><label for="pregunta24b" class="css-labelRED"></label></center>
					</div>
				</div>
				<div class="row">
					<div class="col-lg-2 col-md-2 col-sm-4 col-xs-4">
						<button id="btn-back-3-voc" type="button" class="btn btn-default  margin-top btn-siguiente">Atras </button>
					</div>
					<div class="col-lg-8 col-md-6 col-sm-3 col-xs-3"></div>
					<div class="col-lg-2 col-md-4 col-sm-5 col-xs-5">
						<button id="btn-next-3-voc" type="button" class="btn btn-default  margin-top btn-siguiente">Siguiente </button>
					</div>
				</div>
			</div>
			<!-- FIN DE GRUPO 3 -->
			<div id="grupo4_vocacional">
				<!-- Pregunta 25 -->
				<div class="row margin-top">
					<div class="col-lg-10 col-md-10 col-sm-10 col-xs-8">
					25. Realizar ejercicios para personas con problemas físicos
					</div>
					<div class="col-lg-1 col-md-1 col-sm-1 col-xs-2">
						<center><input type="radio" name="pregunta25" id="pregunta25a" class="area2 css-checkbox" value="1"><label for="pregunta25a" class="css-label"></label></center>
					</div>
					<div class="col-lg-1 col-md-1 col-sm-1 col-xs-2">
						<center><input type="radio" name="pregunta25" id="pregunta25b" class=" css-checkboxRED" value="0"><label for="pregunta25b" class="css-labelRED"></label></center>
					</div>
				</div>
				<!-- Pregunta 26 -->
				<div class="row margin-top">
					<div class="col-lg-10 col-md-10 col-sm-10 col-xs-8">
					26. Prepararse para ser actor de teatro
					</div>
					<div class="col-lg-1 col-md-1 col-sm-1 col-xs-2">
						<center><input type="radio" name="pregunta26" id="pregunta26a" class="area4 css-checkbox" value="1"><label for="pregunta26a" class="css-label"></label></center>
					</div>
					<div class="col-lg-1 col-md-1 col-sm-1 col-xs-2">
						<center><input type="radio" name="pregunta26" id="pregunta26b" class=" css-checkboxRED" value="0"><label for="pregunta26b" class="css-labelRED"></label></center>
					</div>
				</div>
				<!-- Pregunta 27 -->
				<div class="row margin-top">
					<div class="col-lg-10 col-md-10 col-sm-10 col-xs-8">
					27. Aconsejar sobre planes de ahorro e inversiones
					</div>
					<div class="col-lg-1 col-md-1 col-sm-1 col-xs-2">
						<center><input type="radio" name="pregunta27" id="pregunta27a" class="area3 css-checkbox" value="1"><label for="pregunta27a" class="css-label"></label></center>
					</div>
					<div class="col-lg-1 col-md-1 col-sm-1 col-xs-2">
						<center><input type="radio" name="pregunta27" id="pregunta27b" class=" css-checkboxRED" value="0"><label for="pregunta27b" class="css-labelRED"></label></center>
					</div>
				</div>
				<!-- Pregunta 28 -->
				<div class="row margin-top">
					<div class="col-lg-10 col-md-10 col-sm-10 col-xs-8">
					28. Realizar los planos de una casa
					</div>
					<div class="col-lg-1 col-md-1 col-sm-1 col-xs-2">
						<center><input type="radio" name="pregunta28" id="pregunta28a" class="area1 css-checkbox" value="1"><label for="pregunta28a" class="css-label"></label></center>
					</div>
					<div class="col-lg-1 col-md-1 col-sm-1 col-xs-2">
						<center><input type="radio" name="pregunta28" id="pregunta28b" class=" css-checkboxRED" value="0"><label for="pregunta28b" class="css-labelRED"></label></center>
					</div>
				</div>
				<!-- Pregunta 29 -->
				<div class="row margin-top">
					<div class="col-lg-10 col-md-10 col-sm-10 col-xs-8">
					29. Diseñar juegos de video
					</div>
					<div class="col-lg-1 col-md-1 col-sm-1 col-xs-2">
						<center><input type="radio" name="pregunta29" id="pregunta29a" class="area1 css-checkbox" value="1"><label for="pregunta29a" class="css-label"></label></center>
					</div>
					<div class="col-lg-1 col-md-1 col-sm-1 col-xs-2">
						<center><input type="radio" name="pregunta29" id="pregunta29b" class=" css-checkboxRED" value="0"><label for="pregunta29b" class="css-labelRED"></label></center>
					</div>
				</div>
				<!-- Pregunta 30 -->
				<div class="row margin-top">
					<div class="col-lg-10 col-md-10 col-sm-10 col-xs-8">
					30. Control de calidad en alimentos
					</div>
					<div class="col-lg-1 col-md-1 col-sm-1 col-xs-2">
						<center><input type="radio" name="pregunta30" id="pregunta30a" class="area3 css-checkbox" value="1"><label for="pregunta30a" class="css-label"></label></center>
					</div>
					<div class="col-lg-1 col-md-1 col-sm-1 col-xs-2">
						<center><input type="radio" name="pregunta30" id="pregunta30b" class=" css-checkboxRED" value="0"><label for="pregunta30b" class="css-labelRED"></label></center>
					</div>
				</div>
				<!-- Pregunta 31 -->
				<div class="row margin-top">
					<div class="col-lg-10 col-md-10 col-sm-10 col-xs-8">
					31. Administrar un negocio
					</div>
					<div class="col-lg-1 col-md-1 col-sm-1 col-xs-2">
						<center><input type="radio" name="pregunta31" id="pregunta31a" class="area3 css-checkbox" value="1"><label for="pregunta31a" class="css-label"></label></center>
					</div>
					<div class="col-lg-1 col-md-1 col-sm-1 col-xs-2">
						<center><input type="radio" name="pregunta31" id="pregunta31b" class=" css-checkboxRED" value="0"><label for="pregunta31b" class="css-labelRED"></label></center>
					</div>
				</div>
				<!-- Pregunta 32 -->
				<div class="row margin-top">
					<div class="col-lg-10 col-md-10 col-sm-10 col-xs-8">
					32. Escribir, artículos, cuentos y novelas
					</div>
					<div class="col-lg-1 col-md-1 col-sm-1 col-xs-2">
						<center><input type="radio" name="pregunta32" id="pregunta32a" class="area4 css-checkbox" value="1"><label for="pregunta32a" class="css-label"></label></center>
					</div>
					<div class="col-lg-1 col-md-1 col-sm-1 col-xs-2">
						<center><input type="radio" name="pregunta32" id="pregunta32b" class=" css-checkboxRED" value="0"><label for="pregunta32b" class="css-labelRED"></label></center>
					</div>
				</div>
				<div class="row">
					<div class="col-lg-2 col-md-2 col-sm-4 col-xs-4">
						<button id="btn-back-4-voc" type="button" class="btn btn-default  margin-top btn-siguiente">Atras </button>
					</div>
					<div class="col-lg-8 col-md-6 col-sm-3 col-xs-3"></div>
					<div class="col-lg-2 col-md-4 col-sm-5 col-xs-5">
						<button id="btn-next-4-voc" type="button" class="btn btn-default  margin-top btn-siguiente">Siguiente </button>
					</div>
				</div>
			</div>
			<!-- FIN DE GRUPO 4 -->
			<div id="grupo5_vocacional">
				<!-- Pregunta 33 -->
				<div class="row margin-top">
					<div class="col-lg-10 col-md-10 col-sm-10 col-xs-8">
					33. Redactar guiones y libretos
					</div>
					<div class="col-lg-1 col-md-1 col-sm-1 col-xs-2">
						<center><input type="radio" name="pregunta33" id="pregunta33a" class="area4 css-checkbox" value="1"><label for="pregunta33a" class="css-label"></label></center>
					</div>
					<div class="col-lg-1 col-md-1 col-sm-1 col-xs-2">
						<center><input type="radio" name="pregunta33" id="pregunta33b" class=" css-checkboxRED" value="0"><label for="pregunta33b" class="css-labelRED"></label></center>
					</div>
				</div>
				<!-- Pregunta 34 -->
				<div class="row margin-top">
					<div class="col-lg-10 col-md-10 col-sm-10 col-xs-8">
					34. Organizar la distribución de un gran almacén
					</div>
					<div class="col-lg-1 col-md-1 col-sm-1 col-xs-2">
						<center><input type="radio" name="pregunta34" id="pregunta34a" class="area3 css-checkbox" value="1"><label for="pregunta34a" class="css-label"></label></center>
					</div>
					<div class="col-lg-1 col-md-1 col-sm-1 col-xs-2">
						<center><input type="radio" name="pregunta34" id="pregunta34b" class=" css-checkboxRED" value="0"><label for="pregunta34b" class="css-labelRED"></label></center>
					</div>
				</div>
				<!-- Pregunta 35 -->
				<div class="row margin-top">
					<div class="col-lg-10 col-md-10 col-sm-10 col-xs-8">
					35. Estudiar la cultura en el campo y la ciudad
					</div>
					<div class="col-lg-1 col-md-1 col-sm-1 col-xs-2">
						<center><input type="radio" name="pregunta35" id="pregunta35a" class="area4 css-checkbox" value="1"><label for="pregunta35a" class="css-label"></label></center>
					</div>
					<div class="col-lg-1 col-md-1 col-sm-1 col-xs-2">
						<center><input type="radio" name="pregunta35" id="pregunta35b" class=" css-checkboxRED" value="0"><label for="pregunta35b" class="css-labelRED"></label></center>
					</div>
				</div>
				<!-- Pregunta 36 -->
				<div class="row margin-top">
					<div class="col-lg-10 col-md-10 col-sm-10 col-xs-8">
					36. Evaluar convenios comerciales internacionales
					</div>
					<div class="col-lg-1 col-md-1 col-sm-1 col-xs-2">
						<center><input type="radio" name="pregunta36" id="pregunta36a" class="area3 css-checkbox" value="1"><label for="pregunta36a" class="css-label"></label></center>
					</div>
					<div class="col-lg-1 col-md-1 col-sm-1 col-xs-2">
						<center><input type="radio" name="pregunta36" id="pregunta36b" class=" css-checkboxRED" value="0"><label for="pregunta36b" class="css-labelRED"></label></center>
					</div>
				</div>
				<!-- Pregunta 37 -->
				<div class="row margin-top">
					<div class="col-lg-10 col-md-10 col-sm-10 col-xs-8">
					37. Crear campañas de publicidad
					</div>
					<div class="col-lg-1 col-md-1 col-sm-1 col-xs-2">
						<center><input type="radio" name="pregunta37" id="pregunta37a" class="area3 css-checkbox" value="1"><label for="pregunta37a" class="css-label"></label></center>
					</div>
					<div class="col-lg-1 col-md-1 col-sm-1 col-xs-2">
						<center><input type="radio" name="pregunta37" id="pregunta37b" class=" css-checkboxRED" value="0"><label for="pregunta37b" class="css-labelRED"></label></center>
					</div>
				</div>
				<!-- Pregunta 38 -->
				<div class="row margin-top">
					<div class="col-lg-10 col-md-10 col-sm-10 col-xs-8">
					38. Investigar especies marinas
					</div>
					<div class="col-lg-1 col-md-1 col-sm-1 col-xs-2">
						<center><input type="radio" name="pregunta38" id="pregunta38a" class="area2 css-checkbox" value="1"><label for="pregunta38a" class="css-label"></label></center>
					</div>
					<div class="col-lg-1 col-md-1 col-sm-1 col-xs-2">
						<center><input type="radio" name="pregunta38" id="pregunta38b" class=" css-checkboxRED" value="0"><label for="pregunta38b" class="css-labelRED"></label></center>
					</div>
				</div>
				<!-- Pregunta 39 -->
				<div class="row margin-top">
					<div class="col-lg-10 col-md-10 col-sm-10 col-xs-8">
					39. Producir alimentos para consumo masivo
					</div>
					<div class="col-lg-1 col-md-1 col-sm-1 col-xs-2">
						<center><input type="radio" name="pregunta39" id="pregunta39a" class="area3 css-checkbox" value="1"><label for="pregunta39a" class="css-label"></label></center>
					</div>
					<div class="col-lg-1 col-md-1 col-sm-1 col-xs-2">
						<center><input type="radio" name="pregunta39" id="pregunta39b" class=" css-checkboxRED" value="0"><label for="pregunta39b" class="css-labelRED"></label></center>
					</div>
				</div>
				<!-- Pregunta 40 -->
				<div class="row margin-top">
					<div class="col-lg-10 col-md-10 col-sm-10 col-xs-8">
					40. Realizar proyectos educativos
					</div>
					<div class="col-lg-1 col-md-1 col-sm-1 col-xs-2">
						<center><input type="radio" name="pregunta40" id="pregunta40a" class="area4 css-checkbox" value="1"><label for="pregunta40a" class="css-label"></label></center>
					</div>
					<div class="col-lg-1 col-md-1 col-sm-1 col-xs-2">
						<center><input type="radio" name="pregunta40" id="pregunta40b" class=" css-checkboxRED" value="0"><label for="pregunta40b" class="css-labelRED"></label></center>
					</div>
				</div>
				<div class="row">
					<div class="col-lg-2 col-md-2 col-sm-4 col-xs-4">
						<button id="btn-back-5-voc" type="button" class="btn btn-default  margin-top btn-siguiente">Atras </button>
					</div>
					<div class="col-lg-8 col-md-6 col-sm-3 col-xs-3"></div>
					<div class="col-lg-2 col-md-4 col-sm-5 col-xs-5">
						<button id="btn-next-5-voc" type="button" class="btn btn-default  margin-top btn-siguiente">Siguiente </button>
					</div>
				</div>
			</div>
			<!-- FIN DE GRUPO 5 -->
			<div id="grupo6_vocacional">
				<!-- Pregunta 41 -->
				<div class="row margin-top">
					<div class="col-lg-10 col-md-10 col-sm-10 col-xs-8">
					41. Decorar viviendas y espacios comunes
					</div>
					<div class="col-lg-1 col-md-1 col-sm-1 col-xs-2">
						<center><input type="radio" name="pregunta41" id="pregunta41a" class="area4 css-checkbox" value="1"><label for="pregunta41a" class="css-label"></label></center>
					</div>
					<div class="col-lg-1 col-md-1 col-sm-1 col-xs-2">
						<center><input type="radio" name="pregunta41" id="pregunta41b" class=" css-checkboxRED" value="0"><label for="pregunta41b" class="css-labelRED"></label></center>
					</div>
				</div>
				<!-- Pregunta 42 -->
				<div class="row margin-top">
					<div class="col-lg-10 col-md-10 col-sm-10 col-xs-8">
					42. Administrar una empresa de turismo
					</div>
					<div class="col-lg-1 col-md-1 col-sm-1 col-xs-2">
						<center><input type="radio" name="pregunta42" id="pregunta42a" class="area3 css-checkbox" value="1"><label for="pregunta42a" class="css-label"></label></center>
					</div>
					<div class="col-lg-1 col-md-1 col-sm-1 col-xs-2">
						<center><input type="radio" name="pregunta42" id="pregunta42b" class=" css-checkboxRED" value="0"><label for="pregunta42b" class="css-labelRED"></label></center>
					</div>
				</div>
				<!-- Pregunta 43 -->
				<div class="row margin-top">
					<div class="col-lg-10 col-md-10 col-sm-10 col-xs-8">
					43. Aplicar métodos de medicina alternativa
					</div>
					<div class="col-lg-1 col-md-1 col-sm-1 col-xs-2">
						<center><input type="radio" name="pregunta43" id="pregunta43a" class="area2 css-checkbox" value="1"><label for="pregunta43a" class="css-label"></label></center>
					</div>
					<div class="col-lg-1 col-md-1 col-sm-1 col-xs-2">
						<center><input type="radio" name="pregunta43" id="pregunta43b" class=" css-checkboxRED" value="0"><label for="pregunta43b" class="css-labelRED"></label></center>
					</div>
				</div>
				<!-- Pregunta 44 -->
				<div class="row margin-top">
					<div class="col-lg-10 col-md-10 col-sm-10 col-xs-8">
					44. Diseñar ropa para niños, jóvenes y adultos
					</div>
					<div class="col-lg-1 col-md-1 col-sm-1 col-xs-2">
						<center><input type="radio" name="pregunta44" id="pregunta44a" class="area4 css-checkbox" value="1"><label for="pregunta44a" class="css-label"></label></center>
					</div>
					<div class="col-lg-1 col-md-1 col-sm-1 col-xs-2">
						<center><input type="radio" name="pregunta44" id="pregunta44b" class=" css-checkboxRED" value="0"><label for="pregunta44b" class="css-labelRED"></label></center>
					</div>
				</div>
				<!-- Pregunta 45 -->
				<div class="row margin-top">
					<div class="col-lg-10 col-md-10 col-sm-10 col-xs-8">
					45. Investigar organismos para hacer vacunas
					</div>
					<div class="col-lg-1 col-md-1 col-sm-1 col-xs-2">
						<center><input type="radio" name="pregunta45" id="pregunta45a" class="area2 css-checkbox" value="1"><label for="pregunta45a" class="css-label"></label></center>
					</div>
					<div class="col-lg-1 col-md-1 col-sm-1 col-xs-2">
						<center><input type="radio" name="pregunta45" id="pregunta45b" class=" css-checkboxRED" value="0"><label for="pregunta45b" class="css-labelRED"></label></center>
					</div>
				</div>
				<!-- Pregunta 46 -->
				<div class="row margin-top">
					<div class="col-lg-10 col-md-10 col-sm-10 col-xs-8">
					46. Manejar y dar mantenimiento a equipo de aviones y barcos
					</div>
					<div class="col-lg-1 col-md-1 col-sm-1 col-xs-2">
						<center><input type="radio" name="pregunta46" id="pregunta46a" class="area1 css-checkbox" value="1"><label for="pregunta46a" class="css-label"></label></center>
					</div>
					<div class="col-lg-1 col-md-1 col-sm-1 col-xs-2">
						<center><input type="radio" name="pregunta46" id="pregunta46b" class=" css-checkboxRED" value="0"><label for="pregunta46b" class="css-labelRED"></label></center>
					</div>
				</div>
				<!-- Pregunta 47 -->
				<div class="row margin-top">
					<div class="col-lg-10 col-md-10 col-sm-10 col-xs-8">
					47. Estudiar idiomas extranjeros
					</div>
					<div class="col-lg-1 col-md-1 col-sm-1 col-xs-2">
						<center><input type="radio" name="pregunta47" id="pregunta47a" class="area4 css-checkbox" value="1"><label for="pregunta47a" class="css-label"></label></center>
					</div>
					<div class="col-lg-1 col-md-1 col-sm-1 col-xs-2">
						<center><input type="radio" name="pregunta47" id="pregunta47b" class=" css-checkboxRED" value="0"><label for="pregunta47b" class="css-labelRED"></label></center>
					</div>
				</div>
				<!-- Pregunta 48 -->
				<div class="row margin-top">
					<div class="col-lg-10 col-md-10 col-sm-10 col-xs-8">
					48. Restaurar obras de arte
					</div>
					<div class="col-lg-1 col-md-1 col-sm-1 col-xs-2">
						<center><input type="radio" name="pregunta48" id="pregunta48a" class="area4 css-checkbox" value="1"><label for="pregunta48a" class="css-label"></label></center>
					</div>
					<div class="col-lg-1 col-md-1 col-sm-1 col-xs-2">
						<center><input type="radio" name="pregunta48" id="pregunta48b" class=" css-checkboxRED" value="0"><label for="pregunta48b" class="css-labelRED"></label></center>
					</div>
				</div>
				<div class="row">
					<div class="col-lg-2 col-md-2 col-sm-4 col-xs-4">
						<button id="btn-back-6-voc" type="button" class="btn btn-default  margin-top btn-siguiente">Atras </button>
					</div>
					<div class="col-lg-8 col-md-6 col-sm-3 col-xs-3"></div>
					<div class="col-lg-2 col-md-4 col-sm-5 col-xs-5">
						<button id="btn-next-6-voc" type="button" class="btn btn-default  margin-top btn-siguiente">Siguiente </button>
					</div>
				</div>
			</div>
			<!-- FIN DE GRUPO 6 -->
			<div id="grupo7_vocacional">
				<!-- Pregunta 49 -->
				<div class="row margin-top">
					<div class="col-lg-10 col-md-10 col-sm-10 col-xs-8">
					49. Componer aparatos eléctricos y electrónicos
					</div>
					<div class="col-lg-1 col-md-1 col-sm-1 col-xs-2">
						<center><input type="radio" name="pregunta49" id="pregunta49a" class="area1 css-checkbox" value="1"><label for="pregunta49a" class="css-label"></label></center>
					</div>
					<div class="col-lg-1 col-md-1 col-sm-1 col-xs-2">
						<center><input type="radio" name="pregunta49" id="pregunta49b" class=" css-checkboxRED" value="0"><label for="pregunta49b" class="css-labelRED"></label></center>
					</div>
				</div>
				<!-- Pregunta 50 -->
				<div class="row margin-top">
					<div class="col-lg-10 col-md-10 col-sm-10 col-xs-8">
					50. Enseñar a niños menores de 5 años
					</div>
					<div class="col-lg-1 col-md-1 col-sm-1 col-xs-2">
						<center><input type="radio" name="pregunta50" id="pregunta50a" class="area4 css-checkbox" value="1"><label for="pregunta50a" class="css-label"></label></center>
					</div>
					<div class="col-lg-1 col-md-1 col-sm-1 col-xs-2">
						<center><input type="radio" name="pregunta50" id="pregunta50b" class=" css-checkboxRED" value="0"><label for="pregunta50b" class="css-labelRED"></label></center>
					</div>
				</div>
				<!-- Pregunta 51 -->
				<div class="row margin-top">
					<div class="col-lg-10 col-md-10 col-sm-10 col-xs-8">
					51. Investigar nuevos mercados
					</div>
					<div class="col-lg-1 col-md-1 col-sm-1 col-xs-2">
						<center><input type="radio" name="pregunta51" id="pregunta51a" class="area3 css-checkbox" value="1"><label for="pregunta51a" class="css-label"></label></center>
					</div>
					<div class="col-lg-1 col-md-1 col-sm-1 col-xs-2">
						<center><input type="radio" name="pregunta51" id="pregunta51b" class=" css-checkboxRED" value="0"><label for="pregunta51b" class="css-labelRED"></label></center>
					</div>
				</div>
				<!-- Pregunta 52 -->
				<div class="row margin-top">
					<div class="col-lg-10 col-md-10 col-sm-10 col-xs-8">
					52. Atender la salud dental de las personas
					</div>
					<div class="col-lg-1 col-md-1 col-sm-1 col-xs-2">
						<center><input type="radio" name="pregunta52" id="pregunta52a" class="area2 css-checkbox" value="1"><label for="pregunta52a" class="css-label"></label></center>
					</div>
					<div class="col-lg-1 col-md-1 col-sm-1 col-xs-2">
						<center><input type="radio" name="pregunta52" id="pregunta52b" class=" css-checkboxRED" value="0"><label for="pregunta52b" class="css-labelRED"></label></center>
					</div>
				</div>
				<!-- Pregunta 53 -->
				<div class="row margin-top">
					<div class="col-lg-10 col-md-10 col-sm-10 col-xs-8">
					53. Hacer estrategias para aprovechar relaciones económicas entre países
					</div>
					<div class="col-lg-1 col-md-1 col-sm-1 col-xs-2">
						<center><input type="radio" name="pregunta53" id="pregunta53a" class="area2 css-checkbox" value="1"><label for="pregunta53a" class="css-label"></label></center>
					</div>
					<div class="col-lg-1 col-md-1 col-sm-1 col-xs-2">
						<center><input type="radio" name="pregunta53" id="pregunta53b" class=" css-checkboxRED" value="0"><label for="pregunta53b" class="css-labelRED"></label></center>
					</div>
				</div>
				<!-- Pregunta 54 -->
				<div class="row margin-top">
					<div class="col-lg-10 col-md-10 col-sm-10 col-xs-8">
					54. Hacer estrategias de promoción para nuevos productos
					</div>
					<div class="col-lg-1 col-md-1 col-sm-1 col-xs-2">
						<center><input type="radio" name="pregunta54" id="pregunta54a" class="area3 css-checkbox" value="1"><label for="pregunta54a" class="css-label"></label></center>
					</div>
					<div class="col-lg-1 col-md-1 col-sm-1 col-xs-2">
						<center><input type="radio" name="pregunta54" id="pregunta54b" class=" css-checkboxRED" value="0"><label for="pregunta54b" class="css-labelRED"></label></center>
					</div>
				</div>
				<!-- Pregunta 55 -->
				<div class="row margin-top">
					<div class="col-lg-10 col-md-10 col-sm-10 col-xs-8">
					55. Implementar dietas sanas para las personas
					</div>
					<div class="col-lg-1 col-md-1 col-sm-1 col-xs-2">
						<center><input type="radio" name="pregunta55" id="pregunta55a" class="area2 css-checkbox" value="1"><label for="pregunta55a" class="css-label"></label></center>
					</div>
					<div class="col-lg-1 col-md-1 col-sm-1 col-xs-2">
						<center><input type="radio" name="pregunta55" id="pregunta55b" class=" css-checkboxRED" value="0"><label for="pregunta55b" class="css-labelRED"></label></center>
					</div>
				</div>
				<!-- Pregunta 56 -->
				<div class="row margin-top">
					<div class="col-lg-10 col-md-10 col-sm-10 col-xs-8">
					56. Trabajar en producción de petróleo
					</div>
					<div class="col-lg-1 col-md-1 col-sm-1 col-xs-2">
						<center><input type="radio" name="pregunta56" id="pregunta56a" class="area1 css-checkbox" value="1"><label for="pregunta56a" class="css-label"></label></center>
					</div>
					<div class="col-lg-1 col-md-1 col-sm-1 col-xs-2">
						<center><input type="radio" name="pregunta56" id="pregunta56b" class=" css-checkboxRED" value="0"><label for="pregunta56b" class="css-labelRED"></label></center>
					</div>
				</div>
				<div class="row">
					<div class="col-lg-2 col-md-2 col-sm-4 col-xs-4">
						<button id="btn-back-7-voc" type="button" class="btn btn-default  margin-top btn-siguiente">Atras </button>
					</div>
					<div class="col-lg-8 col-md-6 col-sm-3 col-xs-3"></div>
					<div class="col-lg-2 col-md-4 col-sm-5 col-xs-5">
						<button id="btn-next-7-voc" type="button" class="btn btn-default  margin-top btn-siguiente">Siguiente </button>
					</div>
				</div>
			</div>
			<!-- FIN DE GRUPO 7 -->
			<div id="grupo8_vocacional">
				<!-- Pregunta 57 -->
				<div class="row margin-top">
					<div class="col-lg-10 col-md-10 col-sm-10 col-xs-8">
					57. Administrar una empresa
					</div>
					<div class="col-lg-1 col-md-1 col-sm-1 col-xs-2">
						<center><input type="radio" name="pregunta57" id="pregunta57a" class="area3 css-checkbox" value="1"><label for="pregunta57a" class="css-label"></label></center>
					</div>
					<div class="col-lg-1 col-md-1 col-sm-1 col-xs-2">
						<center><input type="radio" name="pregunta57" id="pregunta57b" class=" css-checkboxRED" value="0"><label for="pregunta57b" class="css-labelRED"></label></center>
					</div>
				</div>
				<!-- Pregunta 58 -->
				<div class="row margin-top">
					<div class="col-lg-10 col-md-10 col-sm-10 col-xs-8">
					58. Trabajar en un taller mecánico
					</div>
					<div class="col-lg-1 col-md-1 col-sm-1 col-xs-2">
						<center><input type="radio" name="pregunta58" id="pregunta58a" class="area1 css-checkbox" value="1"><label for="pregunta58a" class="css-label"></label></center>
					</div>
					<div class="col-lg-1 col-md-1 col-sm-1 col-xs-2">
						<center><input type="radio" name="pregunta58" id="pregunta58b" class=" css-checkboxRED" value="0"><label for="pregunta58b" class="css-labelRED"></label></center>
					</div>
				</div>
				<!-- Pregunta 59 -->
				<div class="row margin-top">
					<div class="col-lg-10 col-md-10 col-sm-10 col-xs-8">
					59. Ejecutar proyectos en una mina
					</div>
					<div class="col-lg-1 col-md-1 col-sm-1 col-xs-2">
						<center><input type="radio" name="pregunta59" id="pregunta59a" class="area1 css-checkbox" value="1"><label for="pregunta59a" class="css-label"></label></center>
					</div>
					<div class="col-lg-1 col-md-1 col-sm-1 col-xs-2">
						<center><input type="radio" name="pregunta59" id="pregunta59b" class=" css-checkboxRED" value="0"><label for="pregunta59b" class="css-labelRED"></label></center>
					</div>
				</div>
				<!-- Pregunta 60 -->
				<div class="row margin-top">
					<div class="col-lg-10 col-md-10 col-sm-10 col-xs-8">
					60. Investigar fauna salvaje
					</div>
					<div class="col-lg-1 col-md-1 col-sm-1 col-xs-2">
						<center><input type="radio" name="pregunta60" id="pregunta60a" class="area2 css-checkbox" value="1"><label for="pregunta60a" class="css-label"></label></center>
					</div>
					<div class="col-lg-1 col-md-1 col-sm-1 col-xs-2">
						<center><input type="radio" name="pregunta60" id="pregunta60b" class=" css-checkboxRED" value="0"><label for="pregunta60b" class="css-labelRED"></label></center>
					</div>
				</div>
				<!-- Pregunta 61 -->
				<div class="row margin-top">
					<div class="col-lg-10 col-md-10 col-sm-10 col-xs-8">
					61. Diseñar programas educativos
					</div>
					<div class="col-lg-1 col-md-1 col-sm-1 col-xs-2">
						<center><input type="radio" name="pregunta61" id="pregunta61a" class="area4 css-checkbox" value="1"><label for="pregunta61a" class="css-label"></label></center>
					</div>
					<div class="col-lg-1 col-md-1 col-sm-1 col-xs-2">
						<center><input type="radio" name="pregunta61" id="pregunta61b" class=" css-checkboxRED" value="0"><label for="pregunta61b" class="css-labelRED"></label></center>
					</div>
				</div>
				<!-- Pregunta 62 -->
				<div class="row margin-top">
					<div class="col-lg-10 col-md-10 col-sm-10 col-xs-8">
					62. Aplicar e interpretar encuestas
					</div>
					<div class="col-lg-1 col-md-1 col-sm-1 col-xs-2">
						<center><input type="radio" name="pregunta62" id="pregunta62a" class="area3 css-checkbox" value="1"><label for="pregunta62a" class="css-label"></label></center>
					</div>
					<div class="col-lg-1 col-md-1 col-sm-1 col-xs-2">
						<center><input type="radio" name="pregunta62" id="pregunta62b" class=" css-checkboxRED" value="0"><label for="pregunta62b" class="css-labelRED"></label></center>
					</div>
				</div>
				<!-- Pregunta 63 -->
				<div class="row margin-top">
					<div class="col-lg-10 col-md-10 col-sm-10 col-xs-8">
					63. Fotografiar hechos históricos
					</div>
					<div class="col-lg-1 col-md-1 col-sm-1 col-xs-2">
						<center><input type="radio" name="pregunta63" id="pregunta63a" class="area4 css-checkbox" value="1"><label for="pregunta63a" class="css-label"></label></center>
					</div>
					<div class="col-lg-1 col-md-1 col-sm-1 col-xs-2">
						<center><input type="radio" name="pregunta63" id="pregunta63b" class=" css-checkboxRED" value="0"><label for="pregunta63b" class="css-labelRED"></label></center>
					</div>
				</div>
				<!-- Pregunta 64 -->
				<div class="row margin-top">
					<div class="col-lg-10 col-md-10 col-sm-10 col-xs-8">
					64. Hacer estrategias para aprovechar relaciones económicas entre países
					</div>
					<div class="col-lg-1 col-md-1 col-sm-1 col-xs-2">
						<center><input type="radio" name="pregunta64" id="pregunta64a" class="area4 css-checkbox" value="1"><label for="pregunta64a" class="css-label"></label></center>
					</div>
					<div class="col-lg-1 col-md-1 col-sm-1 col-xs-2">
						<center><input type="radio" name="pregunta64" id="pregunta64b" class=" css-checkboxRED" value="0"><label for="pregunta64b" class="css-labelRED"></label></center>
					</div>
				</div>
				<div class="row">
					<div class="col-lg-2 col-md-2 col-sm-4 col-xs-4">
						<button id="btn-back-8-voc" type="button" class="btn btn-default  margin-top btn-siguiente">Atras </button>
					</div>
					<div class="col-lg-8 col-md-6 col-sm-3 col-xs-3"></div>
					<div class="col-lg-2 col-md-4 col-sm-5 col-xs-5">
						<button id="btn-next-8-voc" type="button" class="btn btn-default  margin-top btn-siguiente">Siguiente </button>
					</div>
				</div>
			</div>
			<!-- FIN DE GRUPO 8 -->
			<div id="grupo9_vocacional">
				<!-- Pregunta 65 -->
				<div class="row margin-top">
					<div class="col-lg-10 col-md-10 col-sm-10 col-xs-8">
					65. Formar parte de una obra de teatro
					</div>
					<div class="col-lg-1 col-md-1 col-sm-1 col-xs-2">
						<center><input type="radio" name="pregunta65" id="pregunta65a" class="area4 css-checkbox" value="1"><label for="pregunta65a" class="css-label"></label></center>
					</div>
					<div class="col-lg-1 col-md-1 col-sm-1 col-xs-2">
						<center><input type="radio" name="pregunta65" id="pregunta65b" class=" css-checkboxRED" value="0"><label for="pregunta65b" class="css-labelRED"></label></center>
					</div>
				</div>
				<!-- Pregunta 66 -->
				<div class="row margin-top">
					<div class="col-lg-10 col-md-10 col-sm-10 col-xs-8">
					66. Producir cortometrajes y programas
					</div>
					<div class="col-lg-1 col-md-1 col-sm-1 col-xs-2">
						<center><input type="radio" name="pregunta66" id="pregunta66a" class="area4 css-checkbox" value="1"><label for="pregunta66a" class="css-label"></label></center>
					</div>
					<div class="col-lg-1 col-md-1 col-sm-1 col-xs-2">
						<center><input type="radio" name="pregunta66" id="pregunta66b" class=" css-checkboxRED" value="0"><label for="pregunta66b" class="css-labelRED"></label></center>
					</div>
				</div>
				<!-- Pregunta 67 -->
				<div class="row margin-top">
					<div class="col-lg-10 col-md-10 col-sm-10 col-xs-8">
					67. Diseñar robots
					</div>
					<div class="col-lg-1 col-md-1 col-sm-1 col-xs-2">
						<center><input type="radio" name="pregunta67" id="pregunta67a" class="area1 css-checkbox" value="1"><label for="pregunta67a" class="css-label"></label></center>
					</div>
					<div class="col-lg-1 col-md-1 col-sm-1 col-xs-2">
						<center><input type="radio" name="pregunta67" id="pregunta67b" class=" css-checkboxRED" value="0"><label for="pregunta67b" class="css-labelRED"></label></center>
					</div>
				</div>
				<!-- Pregunta 68 -->
				<div class="row margin-top">
					<div class="col-lg-10 col-md-10 col-sm-10 col-xs-8">
					68. Desarrollar tecnología para obtener energías limpias
					</div>
					<div class="col-lg-1 col-md-1 col-sm-1 col-xs-2">
						<center><input type="radio" name="pregunta68" id="pregunta68a" class="area1 css-checkbox" value="1"><label for="pregunta68a" class="css-label"></label></center>
					</div>
					<div class="col-lg-1 col-md-1 col-sm-1 col-xs-2">
						<center><input type="radio" name="pregunta68" id="pregunta68b" class=" css-checkboxRED" value="0"><label for="pregunta68b" class="css-labelRED"></label></center>
					</div>
				</div>
				<!-- Pregunta 69 -->
				<div class="row margin-top">
					<div class="col-lg-10 col-md-10 col-sm-10 col-xs-8">
					69. Asesorar a inversionistas
					</div>
					<div class="col-lg-1 col-md-1 col-sm-1 col-xs-2">
						<center><input type="radio" name="pregunta69" id="pregunta69a" class="area2 css-checkbox" value="1"><label for="pregunta69a" class="css-label"></label></center>
					</div>
					<div class="col-lg-1 col-md-1 col-sm-1 col-xs-2">
						<center><input type="radio" name="pregunta69" id="pregunta69b" class=" css-checkboxRED" value="0"><label for="pregunta69b" class="css-labelRED"></label></center>
					</div>
				</div>
				<!-- Pregunta 70 -->
				<div class="row margin-top">
					<div class="col-lg-10 col-md-10 col-sm-10 col-xs-8">
					70. Diseñar maquinaria para la industria
					</div>
					<div class="col-lg-1 col-md-1 col-sm-1 col-xs-2">
						<center><input type="radio" name="pregunta70" id="pregunta70a" class="area1 css-checkbox" value="1"><label for="pregunta70a" class="css-label"></label></center>
					</div>
					<div class="col-lg-1 col-md-1 col-sm-1 col-xs-2">
						<center><input type="radio" name="pregunta70" id="pregunta70b" class=" css-checkboxRED" value="0"><label for="pregunta70b" class="css-labelRED"></label></center>
					</div>
				</div>
				<!-- Pregunta 71 -->
				<div class="row margin-top">
					<div class="col-lg-10 col-md-10 col-sm-10 col-xs-8">
					71. Explorar el espacio
					</div>
					<div class="col-lg-1 col-md-1 col-sm-1 col-xs-2">
						<center><input type="radio" name="pregunta71" id="pregunta71a" class="area1 css-checkbox" value="1"><label for="pregunta71a" class="css-label"></label></center>
					</div>
					<div class="col-lg-1 col-md-1 col-sm-1 col-xs-2">
						<center><input type="radio" name="pregunta71" id="pregunta71b" class=" css-checkboxRED" value="0"><label for="pregunta71b" class="css-labelRED"></label></center>
					</div>
				</div>
				<!-- Pregunta 72 -->
				<div class="row margin-top">
					<div class="col-lg-10 col-md-10 col-sm-10 col-xs-8">
					72. Investigar curas para enfermedades
					</div>
					<div class="col-lg-1 col-md-1 col-sm-1 col-xs-2">
						<center><input type="radio" name="pregunta72" id="pregunta72a" class="area2 css-checkbox" value="1"><label for="pregunta72a" class="css-label"></label></center>
					</div>
					<div class="col-lg-1 col-md-1 col-sm-1 col-xs-2">
						<center><input type="radio" name="pregunta72" id="pregunta72b" class=" css-checkboxRED" value="0"><label for="pregunta72b" class="css-labelRED"></label></center>
					</div>
				</div>
				<div class="row">
					<div class="col-lg-2 col-md-2 col-sm-4 col-xs-4">
						<button id="btn-back-9-voc" type="button" class="btn btn-default  margin-top btn-siguiente">Atras </button>
					</div>
					<div class="col-lg-8 col-md-6 col-sm-3 col-xs-3"></div>
					<div class="col-lg-2 col-md-4 col-sm-5 col-xs-5">
						<button id="btn-next-9-voc" type="button" class="btn btn-default  margin-top btn-siguiente">Siguiente </button>
					</div>
				</div>
			</div>
<!-- FIN DE GRUPO 9 -->
<!-- INICIO DE GRUPO 10 -->
			<div id="grupo10_vocacional">
				<!-- Pregunta 73 -->
				<div class="row margin-top">
					<div class="col-lg-10 col-md-10 col-sm-10 col-xs-8">
					73. Diseñar el motor de un auto
					</div>
					<div class="col-lg-1 col-md-1 col-sm-1 col-xs-2">
						<center><input type="radio" name="pregunta73" id="pregunta73a" class="area1 css-checkbox" value="1"><label for="pregunta73a" class="css-label"></label></center>
					</div>
					<div class="col-lg-1 col-md-1 col-sm-1 col-xs-2">
						<center><input type="radio" name="pregunta73" id="pregunta73b" class=" css-checkboxRED" value="0"><label for="pregunta73b" class="css-labelRED"></label></center>
					</div>
				</div>
				<!-- Pregunta 74 -->
				<div class="row margin-top">
					<div class="col-lg-10 col-md-10 col-sm-10 col-xs-8">
					74. Renovar menús de restaurantes
					</div>
					<div class="col-lg-1 col-md-1 col-sm-1 col-xs-2">
						<center><input type="radio" name="pregunta74" id="pregunta74a" class="area2 css-checkbox" value="1"><label for="pregunta74a" class="css-label"></label></center>
					</div>
					<div class="col-lg-1 col-md-1 col-sm-1 col-xs-2">
						<center><input type="radio" name="pregunta74" id="pregunta74b" class=" css-checkboxRED" value="0"><label for="pregunta74b" class="css-labelRED"></label></center>
					</div>
				</div>
				<!-- Pregunta 75 -->
				<div class="row margin-top">
					<div class="col-lg-10 col-md-10 col-sm-10 col-xs-8">
					75. Crear nuevos sistemas operativos
					</div>
					<div class="col-lg-1 col-md-1 col-sm-1 col-xs-2">
						<center><input type="radio" name="pregunta75" id="pregunta75a" class="area1 css-checkbox" value="1"><label for="pregunta75a" class="css-label"></label></center>
					</div>
					<div class="col-lg-1 col-md-1 col-sm-1 col-xs-2">
						<center><input type="radio" name="pregunta75" id="pregunta75b" class=" css-checkboxRED" value="0"><label for="pregunta75b" class="css-labelRED"></label></center>
					</div>
				</div>
				<!-- Pregunta 76 -->
				<div class="row margin-top">
					<div class="col-lg-10 col-md-10 col-sm-10 col-xs-8">
					76. Diseñar programas turísticos
					</div>
					<div class="col-lg-1 col-md-1 col-sm-1 col-xs-2">
						<center><input type="radio" name="pregunta76" id="pregunta76a" class="area3 css-checkbox" value="1"><label for="pregunta76a" class="css-label"></label></center>
					</div>
					<div class="col-lg-1 col-md-1 col-sm-1 col-xs-2">
						<center><input type="radio" name="pregunta76" id="pregunta76b" class=" css-checkboxRED" value="0"><label for="pregunta76b" class="css-labelRED"></label></center>
					</div>
				</div>
				<!-- Pregunta 77 -->
				<div class="row margin-top">
					<div class="col-lg-10 col-md-10 col-sm-10 col-xs-8">
					77. Planear el trazo de ciudades
					</div>
					<div class="col-lg-1 col-md-1 col-sm-1 col-xs-2">
						<center><input type="radio" name="pregunta77" id="pregunta77a" class="area1 css-checkbox" value="1"><label for="pregunta77a" class="css-label"></label></center>
					</div>
					<div class="col-lg-1 col-md-1 col-sm-1 col-xs-2">
						<center><input type="radio" name="pregunta77" id="pregunta77b" class=" css-checkboxRED" value="0"><label for="pregunta77b" class="css-labelRED"></label></center>
					</div>
				</div>
				<!-- Pregunta 78 -->
				<div class="row margin-top">
					<div class="col-lg-10 col-md-10 col-sm-10 col-xs-8">
					78. Desarrollar programas computacionales
					</div>
					<div class="col-lg-1 col-md-1 col-sm-1 col-xs-2">
						<center><input type="radio" name="pregunta78" id="pregunta78a" class="area1 css-checkbox" value="1"><label for="pregunta78a" class="css-label"></label></center>
					</div>
					<div class="col-lg-1 col-md-1 col-sm-1 col-xs-2">
						<center><input type="radio" name="pregunta78" id="pregunta78b" class=" css-checkboxRED" value="0"><label for="pregunta78b" class="css-labelRED"></label></center>
					</div>
				</div>
				<!-- Pregunta 79 -->
				<div class="row margin-top">
					<div class="col-lg-10 col-md-10 col-sm-10 col-xs-8">
					79. Mejorar procesos industriales
					</div>
					<div class="col-lg-1 col-md-1 col-sm-1 col-xs-2">
						<center><input type="radio" name="pregunta79" id="pregunta79a" class="area1 css-checkbox" value="1"><label for="pregunta79a" class="css-label"></label></center>
					</div>
					<div class="col-lg-1 col-md-1 col-sm-1 col-xs-2">
						<center><input type="radio" name="pregunta79" id="pregunta79b" class=" css-checkboxRED" value="0"><label for="pregunta79b" class="css-labelRED"></label></center>
					</div>
				</div>
				<!-- Pregunta 80 -->
				<div class="row margin-top">
					<div class="col-lg-10 col-md-10 col-sm-10 col-xs-8">
					80. Estudiar especies animiales en sus entornos
					</div>
					<div class="col-lg-1 col-md-1 col-sm-1 col-xs-2">
						<center><input type="radio" name="pregunta80" id="pregunta80a" class="area2 css-checkbox" value="1"><label for="pregunta80a" class="css-label"></label></center>
					</div>
					<div class="col-lg-1 col-md-1 col-sm-1 col-xs-2">
						<center><input type="radio" name="pregunta80" id="pregunta80b" class=" css-checkboxRED" value="0"><label for="pregunta80b" class="css-labelRED"></label></center>
					</div>
				</div>
				<div class="row">
					<div class="col-lg-2 col-md-2 col-sm-4 col-xs-4">
						<button id="btn-back-10-voc" type="button" class="btn btn-default  margin-top btn-siguiente">Atras </button>
					</div>
					<div class="col-lg-8 col-md-6 col-sm-3 col-xs-3"></div>
					<div class="col-lg-2 col-md-4 col-sm-5 col-xs-5">
						<button id="btn-end-voc" type="button" class="btn btn-default  margin-top btn-siguiente">Finalizar </button>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>


<script type="text/javascript">

  $(document).ready(function(){

  	var cookieForm = null;
  	var accesoCC = null

  	if( checkCookie('c_form_data') ){
  		cookieForm = JSON.parse(decodeURIComponent(getCookie('c_form_data')));
  		accesoCC = cookieForm.accesoCC;
  	} 

	if(!accesoCC) {
		removeCookie('accesoCC');
		window.location.href = "https://www.unitec.mx/testvocacional";
	}else{
		var red = document.getElementById("ocultRedir")
        red.parentNode.removeChild(red);
        var over = document.getElementById("overlay-oculta")
        over.classList.remove("overlay")
	//}
	$('#intrucciones_vocacional').modal(open)

  		$("#btn-next-1-voc").click(function(){
  			var cuantos= $("#grupo1_vocacional input:radio:checked").length;
			if (cuantos<8) {
				var faltan=8-cuantos;
				// alert('Te faltan '+faltan +' preguntas para avanzar.');
				var texto_faltantes ='Te faltan '+faltan +' preguntas para avanzar.';
				$('#texto_faltantes').text(texto_faltantes);
				$('#preguntas_faltantes').modal();
			}else{
				$("#grupo1_vocacional").hide("slide",{direction: 'left'},"slow", function(){
  					$("#grupo2_vocacional").show("slide",{direction: 'right'},"fast");
  				})
			}
  		});

  		$("#btn-next-2-voc").click(function(){
  			var cuantos= $("#grupo2_vocacional input:radio:checked").length;
			if (cuantos<8) {
				var faltan=8-cuantos;
				// alert('Te faltan '+faltan +' preguntas para avanzar.');
				var texto_faltantes ='Te faltan '+faltan +' preguntas para avanzar.';
				$('#texto_faltantes').text(texto_faltantes);
				$('#preguntas_faltantes').modal();
			}else{
  					$("#grupo2_vocacional").hide("slide",{direction: 'left'},"slow", function(){
  						$("#grupo3_vocacional").show("slide",{direction: 'right'},"fast");
  					})
  			}
  		})

  		$("#btn-next-3-voc").click(function(){
  			var cuantos= $("#grupo3_vocacional input:radio:checked").length;
			if (cuantos<8) {
				var faltan=8-cuantos;
				// alert('Te faltan '+faltan +' preguntas para avanzar.');
				var texto_faltantes ='Te faltan '+faltan +' preguntas para avanzar.';
				$('#texto_faltantes').text(texto_faltantes);
				$('#preguntas_faltantes').modal();
			}else{
  			$("#grupo3_vocacional").hide("slide",{direction: 'left'},"slow", function(){
  				$("#grupo4_vocacional").show("slide",{direction: 'right'},"fast");
  			})
  			}
  		})

  		$("#btn-next-4-voc").click(function(){
  			var cuantos= $("#grupo4_vocacional input:radio:checked").length;
			if (cuantos<8) {
				var faltan=8-cuantos;
				// alert('Te faltan '+faltan +' preguntas para avanzar.');
				var texto_faltantes ='Te faltan '+faltan +' preguntas para avanzar.';
				$('#texto_faltantes').text(texto_faltantes);
				$('#preguntas_faltantes').modal();
			}else{
  			$("#grupo4_vocacional").hide("slide",{direction: 'left'},"slow", function(){
  				$("#grupo5_vocacional").show("slide",{direction: 'right'},"fast");
  			})
  			}
  		})

  		$("#btn-next-5-voc").click(function(){
  			var cuantos= $("#grupo5_vocacional input:radio:checked").length;
			if (cuantos<8) {
				var faltan=8-cuantos;
				// alert('Te faltan '+faltan +' preguntas para avanzar.');
				var texto_faltantes ='Te faltan '+faltan +' preguntas para avanzar.';
				$('#texto_faltantes').text(texto_faltantes);
				$('#preguntas_faltantes').modal();
			}else{
  			$("#grupo5_vocacional").hide("slide",{direction: 'left'},"slow", function(){
  				$("#grupo6_vocacional").show("slide",{direction: 'right'},"fast");
  			})
  			}
  		})

  		$("#btn-next-6-voc").click(function(){
  			var cuantos= $("#grupo6_vocacional input:radio:checked").length;
			if (cuantos<8) {
				var faltan=8-cuantos;
				// alert('Te faltan '+faltan +' preguntas para avanzar.');
				var texto_faltantes ='Te faltan '+faltan +' preguntas para avanzar.';
				$('#texto_faltantes').text(texto_faltantes);
				$('#preguntas_faltantes').modal();
			}else{
  			$("#grupo6_vocacional").hide("slide",{direction: 'left'},"slow", function(){
  				$("#grupo7_vocacional").show("slide",{direction: 'right'},"fast");
  			})
  			}
  		})

  		$("#btn-next-7-voc").click(function(){
  			var cuantos= $("#grupo7_vocacional input:radio:checked").length;
			if (cuantos<8) {
				var faltan=8-cuantos;
				// alert('Te faltan '+faltan +' preguntas para avanzar.');
				var texto_faltantes ='Te faltan '+faltan +' preguntas para avanzar.';
				$('#texto_faltantes').text(texto_faltantes);
				$('#preguntas_faltantes').modal();
			}else{
  			$("#grupo7_vocacional").hide("slide",{direction: 'left'},"slow", function(){
  				$("#grupo8_vocacional").show("slide",{direction: 'right'},"fast");
  			})
  			}
  		})

  		$("#btn-next-8-voc").click(function(){
  			var cuantos= $("#grupo8_vocacional input:radio:checked").length;
			if (cuantos<8) {
				var faltan=8-cuantos;
				// alert('Te faltan '+faltan +' preguntas para avanzar.');
				var texto_faltantes ='Te faltan '+faltan +' preguntas para avanzar.';
				$('#texto_faltantes').text(texto_faltantes);
				$('#preguntas_faltantes').modal();
			}else{
  			$("#grupo8_vocacional").hide("slide",{direction: 'left'},"slow", function(){
  				$("#grupo9_vocacional").show("slide",{direction: 'right'},"fast");
  			})
  			}
  		})

  		$("#btn-next-9-voc").click(function(){
  			var cuantos= $("#grupo9_vocacional input:radio:checked").length;
			if (cuantos<8) {
				var faltan=8-cuantos;
				// alert('Te faltan '+faltan +' preguntas para avanzar.');
				var texto_faltantes ='Te faltan '+faltan +' preguntas para avanzar.';
				$('#texto_faltantes').text(texto_faltantes);
				$('#preguntas_faltantes').modal();
			}else{
  			$("#grupo9_vocacional").hide("slide",{direction: 'left'},"slow", function(){
  				$("#grupo10_vocacional").show("slide",{direction: 'right'},"fast");
  			})
  			}
  		})

  		$("#btn-back-2-voc").click(function(){
  			$("#grupo2_vocacional").hide("slide",{direction: 'right'},"slow", function(){
  				$("#grupo1_vocacional").show("slide",{direction: 'left'},"fast")
  			})
  		});

  		$("#btn-back-3-voc").click(function(){
  			$("#grupo3_vocacional").hide("slide",{direction: 'right'},"slow", function(){
  				$("#grupo2_vocacional").show("slide",{direction: 'left'},"fast")
  			})
  		});

  		$("#btn-back-4-voc").click(function(){
  			$("#grupo4_vocacional").hide("slide",{direction: 'right'},"slow", function(){
  				$("#grupo3_vocacional").show("slide",{direction: 'left'},"fast")
  			})
  		});

  		$("#btn-back-5-voc").click(function(){
  			$("#grupo5_vocacional").hide("slide",{direction: 'right'},"slow", function(){
  				$("#grupo4_vocacional").show("slide",{direction: 'left'},"fast")
  			})
  		});

  		$("#btn-back-6-voc").click(function(){
  			$("#grupo6_vocacional").hide("slide",{direction: 'right'},"slow", function(){
  				$("#grupo5_vocacional").show("slide",{direction: 'left'},"fast")
  			})
  		});

  		$("#btn-back-7-voc").click(function(){
  			$("#grupo7_vocacional").hide("slide",{direction: 'right'},"slow", function(){
  				$("#grupo6_vocacional").show("slide",{direction: 'left'},"fast")
  			})
  		});

  		$("#btn-back-8-voc").click(function(){
  			$("#grupo8_vocacional").hide("slide",{direction: 'right'},"slow", function(){
  				$("#grupo7_vocacional").show("slide",{direction: 'left'},"fast")
  			})
  		});

  		$("#btn-back-9-voc").click(function(){
  			$("#grupo9_vocacional").hide("slide",{direction: 'right'},"slow", function(){
  				$("#grupo8_vocacional").show("slide",{direction: 'left'},"fast")
  			})
  		});

  		$("#btn-back-10-voc").click(function(){
  			$("#grupo10_vocacional").hide("slide",{direction: 'right'},"slow", function(){
  				$("#grupo9_vocacional").show("slide",{direction: 'left'},"fast")
  			})
  		});
  	}


/* Funcion para obtener el area y meter posibles diferenciadores */
function getTheArea(arrayAnswers, differentiator){
	// var arrayAnswers = [432,234,432,234];
	var max = 0;
	var key = 0;
	var dataKey = [];
	var answerOf = [];
	arrayAnswers.forEach(function (v, k) { 
		if (max < v && v != max) { 
			max = v; 
			key = k; 
			dataKey = [];
			dataKey.push(k);
		} else if(max <= v) {
			max = v; 
			key = k; 
			dataKey.push(k);
		}
	});

	if(dataKey.length == 1){
		answerOf.push(dataKey[0] + 1); //Se aumenta uno para el area
		return answerOf;
	} else if(differentiator != false) {
		//Usar diferenciadores
		var differentiator = [0,2,3,1];
		var exclude = 0;
		var keyExclude = 0;
		dataKey.forEach(function (v, k) { 
			if(exclude < differentiator[dataKey[k]]) {
				exclude = v; 
				keyExclude = k; 
			}
		});
		answerOf.push(dataKey[keyExclude] + 1); //Se aumenta uno para el area
		return answerOf;
	} else {
		dataKey.forEach(function (v, k) { 
			dataKey[k] = v + 1; //Se aumenta uno para el area
		});
		return dataKey;
	}
}

  //validar

$('#btn-end-voc').click(function(){
	var cuantos= $("#grupo10_vocacional input:radio:checked").length;
			if (cuantos<8) {
				var faltan=8-cuantos;
				alert('Te faltan '+faltan +' preguntas para avanzar.');
			}else{
				var narea=new Array();
				narea1= $(".area1:input:radio:checked").length;
				narea2= $(".area2:input:radio:checked").length;
				narea3= $(".area3:input:radio:checked").length;
				narea4= $(".area4:input:radio:checked").length;
				console.log(narea1+' '+narea2+' '+narea3+' '+narea4);

				var ganador;

				var valuesAreas = [];
				valuesAreas.push(narea1);
				valuesAreas.push(narea2);
				valuesAreas.push(narea3);
				valuesAreas.push(narea4);

				ganador = getTheArea(valuesAreas,false);
				console.log(ganador);

				if(ganador.length == 1){
					//window.location ='area'+ganador[0]+'.php?app=1';
					areag = ganador[0];
				} else {
					var escogeUno = Math.floor(Math.random() * ganador.length);
					//window.location ='area'+ganador[escogeUno]+'.php?app=1';
					areag = escogeUno;
				}
				switch(areag) {
				  case 1:
				    areanombre = 'fisicomatematicas';
				    break;
				  case 2:
				    areanombre = 'salud';
				    break;
				    case 3:
				    areanombre = 'economico-administrativas';
				    break;
				    case 4:
				    areanombre = 'ciencias-sociales';
				    break;
				  default:
				    // code block
				}
				window.location =areanombre;
			}
})

  //fin validar
 	})
</script>


<div class="modal fade alerta-1" id="intrucciones_vocacional">
  <div class="modal-dialog">
    <div class="modal-content border_orange">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true" style="color:#0072BD !important; font-weight: bold; font-size:20px">X</span><span class="sr-only">Close</span></button>
      </div>
      <div class="modal-body">
        	<div class="instrucciones"><center><h2>Test Vocacional</h2><br/>
        	<strong><p>INSTRUCCIONES</p></strong>
        	<ul>
				<li>1. Lee atentamente cada una de las actividades.</li><br/><li>2. Indica si te gustan con un <img src="images/testvocacional/like.svg" style="width:30px; margin-top: -10px;"> y las que no con un
				<img src="images/testvocacional/dislike.svg" style="width:30px"></li><br/>
				<li>3. No existen respuestas correctas o incorrectas. Contesta con sinceridad y confianza para que puedas conocer mejor tus intereses vocacionales.</li>
				</ul><br/><h4>¡Ahora estás listo para responder el Test Vocacional!</h4><br/></center>
			</div>
      </div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->


<div class="modal fade alerta-1" id="preguntas_faltantes">
  <div class="modal-dialog">
    <div class="modal-content border_orange">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true" style="color:#0072BD !important; font-weight: bold; font-size:20px">X</span><span class="sr-only">Close</span></button>
      </div>
      <div class="modal-body">
        	<div class="instrucciones"><center><h2>Test Vocacional</h2><br/>
				<br/><h4><strong><p id="texto_faltantes"></p></strong></h4><br/></center>
			</div>
      </div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->


<!-- FIN modal registro-->
<?php //include 'includes/footer.php';?>
</body>
</html>