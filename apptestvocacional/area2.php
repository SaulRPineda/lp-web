<html lang="es">
	<head>
<?php 
	include 'cabeceras.php';
 ?>
		<title>Test Vocacional | UNITEC Universidad Tecnológica de México</title>
	</head>
	<body>
		<div class="container" id="mitos">
<?php include 'head_app.php'; ?>
			<div class="row">
				<div class="col-lg-12">
					<center><h2>Esta es la oferta académica que tenemos para ti:</h2></center><br/>
				</div>
			</div>
			<div class="row">
				<div class="col-lg-2"></div>
				<div class="col-lg-8">
					<div class="row" id="area2">
						<div class="col-lg-7">
							<img src="images/mitos/area2.png" class="img-responsive">
						</div>
						<div class="col-lg-5 padding-minus">
							<p class="white-txt"><strong>El saludable.</strong> Sabemos que tu gusto por ayudar a los demás es grande, es por eso que nuestras carreras de Ciencias de la Salud son ideales para ti. Si no le temes tanto a la sangre y te gustan las batas, ¡anímate!</p>
						</div>
					</div>
					<div class="row" id="carreras2">
						<div class="col-lg-12 padding-zero">
							<br/>
							<ul>
<?php
	include ("conexion.php");
	//Consulta
	$consulta= "Select * from Catalogo_Carrera where AreaTest='2'";
	$resultado_camp= $mysqli -> query($consulta);
	while($fila = $resultado_camp -> fetch_array()){
		echo '<a href="salud-mitos-y-realidades?car='.$fila['id_horaLibre'].'"><li>'.utf8_encode($fila['Carrera_front']).'</li></a>';
	}
?>					
							</ul>
						</div>
					</div>
				</div>
				<div class="col-lg-2"></div>
			</div>
		</div>
	</body>
</html>