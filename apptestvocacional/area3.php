<html lang="es">
	<head>
<?php
	include 'cabeceras.php';
 ?>
		<title>Test Vocacional | UNITEC Universidad Tecnológica de México</title>
	</head>
	<body>
		<div class="container" id="mitos">
<?php include 'head_app.php'; ?>
			<div class="row">
				<div class="col-lg-12">
					<center><h2>Esta es la oferta académica que tenemos para ti:</h2></center><br/>
				</div>
			</div>
			<div class="row">
				<div class="col-lg-2"></div>
				<div class="col-lg-8">
					<div class="row" id="area3">
						<div class="col-lg-7">
							<img src="images/mitos/area3.png" class="img-responsive">
						</div>
						<div class="col-lg-5 padding-minus">
							<p class="white-txt"><strong>El Administrado.</strong> Calcular cuánto gastas a la semana y hacer que el dinero rinda es lo tuyo, ¿cierto? 
							Seguro que defender causas justas también te interesa. Te invitamos a darle un vistazo a nuestras carreras Económico administrativas y Sociales.</p>
						</div>
					</div>
					<div class="row" id="carreras3">
						<div class="col-lg-12 padding-zero">
						<br/>
							<ul>
<?php
	include ("conexion.php");
//Consulta
	$consulta= "Select * from Catalogo_Carrera where AreaTest='3'";
	$resultado_camp= $mysqli -> query($consulta);
	while($fila = $resultado_camp -> fetch_array()){
		echo '<a href="economico-administrativas-mitos-y-realidades?car='.$fila['id_horaLibre'].'"><li>'.utf8_encode($fila['Carrera_front']).'</li></a>';
	}
?>					
							</ul>
						</div>
					</div>
				</div>
				<div class="col-lg-2"></div>
			</div>
		</div>
	</body>
</html>