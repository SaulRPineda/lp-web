function showDiv() {
	document.getElementById('comentario_oculto').style.display = "block";
}
$(document).ready(function () {
	$("#login").click(function () {
		var ValorUsuario = $("#usuario_validar").val()
		var password_validar = $("#password_validar").val()
		if (ValorUsuario == "" || ValorUsuario == null) {
			$.colorbox({ html: '<div class="alerta" style="padding:10px;color:#666;"><p class="lead">Ingresa un usuario</p></div>', close: 'Cerrar' });
			$("#usuario_validar").effect('pulsate', 'shake');
			$("#usuario_validar").focus();
			return false;
		} else {
		} if (password_validar == "" || password_validar == null) {
			$.colorbox({ html: '<div class="alerta" style="padding:10px;color:#666;"><p class="lead">Ingresa un password</p></div>', close: 'Cerrar' });
			$("#password_validar").effect('pulsate', 'shake');
			$("#password_validar").focus();
			return false;
		} else {
			$.post("acciones/valida_usuario.php", $("#logueate").serialize(), function (res) {
				console.log(res);
				if (res == "Usuario Valido") {
					window.location = "perfil.php";
				} else {
					$.colorbox({
						html: '<div class="alerta" style="padding:10px;color:#666;"><p class="lead">' + res + '</p></div>', close: 'Cerrar', onClosed: function () {
							$("#registrar").click()
						}
					});
				}
			});
		}
	});

	// Validar y enviar registro
	$("#crear").click(function () {
		alert("click en crear")
		var nick = $("#nickname").val()
		if (nick == "" || nick == null || nick.length <= 3 || nick.length >= 5) {
			$.colorbox({ html: '<div class="alerta" style="padding:10px;color:#666;"><p class="lead">Ingresa un nickname</p></div>', close: 'Cerrar' });
			$("#nickname").effect('pulsate', 'shake');
			$("#nickname").focus();
		}
		var Nombrebasura = new Array('asdasd', 'asdasdasd', 'aaa', 'aad', 'abc', 'ads', 'aea', 'ahh', 'apellido', 'asd', 'asf', 'asj', 'bbb', 'bfj', 'bjk', 'bvh', 'ccc', 'cds', 'chc', 'cju', 'csd', 'cvb', 'cxz', 'dadsa', 'das', 'ddd', 'dee', 'demo', 'dfg', 'djf', 'djj', 'dnd', 'drh', 'dse', 'dsf', 'dss', 'dsx', 'dvc', 'dxx', 'dyy', 'eee', 'eeh', 'ewf', 'fff', 'fgg', 'fgh', 'fgt', 'fhg', 'fjg', 'fjh', 'fkj', 'fnh', 'frg', 'gfl', 'ggg', 'ghb', 'ghh', 'ghk', 'gjs', 'gvk', 'hah', 'hch', 'hdj', 'hgd', 'hgf', 'hhf', 'hhh', 'hjk', 'hjn', 'hola', 'ibf', 'igh', 'ihi', 'iii', 'iji', 'ioj', 'iph', 'jaj', 'jaja', 'jhg', 'jhj', 'jhn', 'jhr', 'jhu', 'jjj', 'jjn', 'jkl', 'jlk', 'jok', 'kbe', 'khv', 'kjk', 'kjn', 'kjs', 'kkj', 'kkk', 'klk', 'knk', 'lkh', 'lkj', 'llj', 'lll', 'lol', 'luu', 'mkk', 'mmm', 'mpm', 'nalga', 'nel', 'njp', 'nkj', 'nnn', 'nomb', 'nul', 'oif', 'oio', 'ooo', 'ouo', 'paa', 'personal', 'pito', 'ppp', 'qqq', 'qwe', 'rgt', 'rrr', 'rtr', 'sda', 'sdf', 'sds', 'sfj', 'sss', 'sws', 'trial', 'trr', 'trt', 'ttt', 'uuu', 'vee', 'vvv', 'wfe', 'www', 'xcv', 'xde', 'xxx', 'xzz', 'yrt', 'yyy', 'zxc', 'zye', 'zzz', 'none', 'asas', 'fds', 'jnk', 'jll', 'rrb', 'perra', 'puta', 'culo', 'culiada', 'marica', 'mamar', 'puto', 'pendejo', 'ldk', 'xoxo', 'ramera', 'hff', 'verga', 'Verga', 'maricon', 'superm', 'batman', 'Batman', 'zorra', 'Zorra', 'Fuckyou', 'fuckyou', 'qwerty', 'qwertyu', 'uytrewq', 'poiuytr', 'qwert', 'qazwsx', 'xswzaq', 'asdffgh', 'ñlkjh', 'mnbvcxz', 'zxcvbnm', 'zxcv', 'bnm', 'ñplokm', 'ijnmko', 'sdfghjklñ', 'edcrfv', 'uhbygv', 'tgbuhb', 'vfrbhu', 'njicdebuh', 'edcijn', 'edcijn', 'ytruie', 'aqsw', 'plok', 'rweiop', 'poqwiehbf', 'poiuyt');
		for (i = 0; i < Nombrebasura.length; i++) {
			var ValorNombre = $("#nombre").val();
			if (ValorNombre == null || ValorNombre.length <= 2 || /^ \s+$/.test(ValorNombre) || !isNaN(ValorNombre) || ValorNombre == Nombrebasura[i]) {

				$.colorbox({ html: '<div class="alerta" style="padding:10px;color:#666;"><p class="lead">Ingresa un Nombre correcto</p></div>', close: 'Cerrar' });
				$("#nombre").effect('pulsate', 'shake');
				$("#nombre").focus();
				return false;
			}

		}
		for (i = 0; i < Nombrebasura.length; i++) {
			var ValorApellidoPat = $("#appaterno").val()
			if (ValorApellidoPat == null || ValorApellidoPat.length <= 2 || /^ \s+$/.test(ValorApellidoPat) || !isNaN(ValorApellidoPat) || ValorApellidoPat == Nombrebasura[i]) {
				$.colorbox({ html: '<div class="alerta" style="padding:10px;color:#666;"><p class="lead">Ingresa un Apellido Paterno correcto</p></div>', close: 'Cerrar' });
				$("#appaterno").effect('pulsate', 'shake');
				$("#appaterno").focus();
				return false;
			}
		}
		for (i = 0; i < Nombrebasura.length; i++) {
			var ValorApellidoMat = $("#apmaterno").val()
			if (ValorApellidoMat == null || ValorApellidoMat.length <= 2 || /^ \s+$/.test(ValorApellidoMat) || !isNaN(ValorApellidoMat) || ValorApellidoMat == Nombrebasura[i]) {
				$.colorbox({ html: '<div class="alerta" style="padding:10px;color:#666;"><p class="lead">Ingresa un Apellido Materno correcto</p></div>', close: 'Cerrar' });
				$("#apmaterno").effect('pulsate', 'shake');
				$("#apmaterno").focus();
				return false;
			}
		}
		var ValorCorreo = $("#email").val()
		if (!(/^[a-zA-Z0-9._-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,4}$/.test(ValorCorreo))) {
			$.colorbox({ html: '<div class="alerta" style="padding:10px;color:#666;"><p class="lead">Ingresa un email válido</p></div>', close: 'Cerrar' });
			$("#email").effect('pulsate', 'shake');
			$("#email").focus();
			return false;
		}
		// var Telefonobasura = new Array('0000000000','3333333333','444444444','5555555555','6666666666','7777777777','8888888888','9999999999','1111111111','2222222222','1234', '2345','0987','000000', '123456', '987654', '101010100', '11111', '55555', '101010', '202020','12345678', "23456789", "3456789", "456789", "56789","222222", '6581111','3259000', '123456', '555555','1234567890','2222222222','1231231231','9876543210','5556581111');
		// for (i=0; i<Telefonobasura.length; i++){
		//     ValorTelefono=document.forms["form_calc"]["Telefonos"].value;
		//     if( ValorTelefono==null || ValorTelefono.length<=9 || ValorTelefono.length>=11 || isNaN(ValorTelefono) || ValorTelefono==Telefonobasura[i]){
		//         $("#Atelefono").addClass('has-error');
		//         $("#telefono").focus()
		//         alert('Ingresa un teléfono correcto LADA + 10 digitos');
		//         return false;
		//     }else{
		//         $("#Atelefono").removeClass('has-error');

		//     }
		//}
		elemento = document.getElementById("politicas");
		if (!elemento.checked) {
			alert('Debes aceptar el Aviso de Privacidad')
			$("#politicas").focus()
			return false;
		} else {
			$.post("includes/recolecta_form.php", $("#registro").serialize(), function (res) {
				console.log(res);

			})
		}
	});

	$("#postear").validate({
		rules: {
			comentario_post: {
				required: true
			}
		},
		messages: {
			comentario_post: {
				required: "Por favor escribe algo"
			}
		}
	})

	$("#comentarios").validate({
		rules: {
			comentario: {
				required: true
			}
		},
		messages: {
			comentario: {
				required: "Por favor ingresa tu comentario"
			}
		}
	})
});
