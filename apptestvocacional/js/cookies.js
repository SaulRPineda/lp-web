/*Funciones para el manejo de Cookies*/
/*By Ing SRP
 *10-12-2019
 */

function setCookieForm(cname, cvalue, exdays) {
    console.log("Nombre: " + cname + " Valor: " + decodeURI(cvalue) + " days: " + exdays);
    var d = new Date();
    d.setTime(d.getTime() + (exdays * 24 * 60 * 60 * 1000));
    var expires = "expires=" + d.toUTCString();
    document.cookie = cname + "=" + cvalue + "; " + expires + ";path=/";
}

function getCookie(cname) {
    var name = cname + "=";
    var ca = document.cookie.split(';');
    for (var i = 0; i < ca.length; i++) {
        var c = ca[i];
        while (c.charAt(0) == ' ') c = c.substring(1);
        if (c.indexOf(name) == 0) return c.substring(name.length, c.length);
    }
    return "";
}

function checkCookie(valor) {
    var user = getCookie(valor);
    if (user != "") {
        return true;
    }
    else {
        return false;
    }
}

/** Va agregando valores a una Cookie ya existente 
 * @param Cookie name, Value to add, Value of cookie, Expiración
 * @return Crea la cookie con el nuevo valor
 */
function appendCookie(nombre_cookie, clave, valor, exdays) {
    var objCookie;
    var cadena_cookie = "";
    var cookieActual = decodeURIComponent(getCookie(nombre_cookie));
    var obj = JSON.parse(cookieActual);
    var cadena_cookie = "{";

    // console.log("Cookie antes de Procesar: " + cookieActual);
    for(let field in obj){
        //console.log( "Recorrido Key: " + field + " Recorrido Valor: " + obj[field] );
        cadena_cookie += '"' + field + '":' + '"' + obj[field] + '",';
    }
    
    cadena_cookie += '"' + clave + '":' + '"' + valor + '",';
    cadena_cookie = cadena_cookie.substr( 0, cadena_cookie.length - 1 );

    cadena_cookie += "}";
    //console.log( "Final cadena_cookie: " + cadena_cookie );

    if ( checkCookie(nombre_cookie) ) {
        removeCookie(nombre_cookie);
    }

    setCookieForm(nombre_cookie, encodeURI(cadena_cookie), 1);
}


function updateCookie(nombre_cookie, clave, valor, exdays) {
    var objCookie;
    var cadena_cookie = "";
    var cookieActual = decodeURIComponent(getCookie(nombre_cookie));
    var obj = JSON.parse(cookieActual);
    var cadena_cookie = "{";

    console.log("Cookie antes de Procesar: " + cookieActual);

    for(let field in obj){

        if (field === clave) {
            console.log("Actualizo Valor de " + field + " debido a que cambio");
            obj[field] = valor
        }

        //console.log( "Recorrido Key: " + field + " Recorrido Valor: " + obj[field] );
        cadena_cookie += '"' + field + '":' + '"' + obj[field] + '",';
    }
    
    //cadena_cookie += '"' + clave + '":' + '"' + valor + '",';
    cadena_cookie = cadena_cookie.substr( 0, cadena_cookie.length - 1 );

    cadena_cookie += "}";
    //console.log( "Final cadena_cookie: " + cadena_cookie );

    if ( checkCookie(nombre_cookie) ) {
        removeCookie(nombre_cookie);
    }

    setCookieForm(nombre_cookie, encodeURI(cadena_cookie), 1);
}

/** Eliminar una Cookie existente
 * @param String name
 * @return 
 */
function removeCookie(nombre_cookie) {
    return document.cookie = nombre_cookie + "=;expires=Thu, 01 Jan 1970 00:00:00 GMT;path=/";
}


/** Comprobar si la url trae UTM_CAMPAING
 * @param String name
 * @return String
 */
function getParameterByName(name) {
    var results;
    name = name.replace(/[\[]/, "\\[").replace(/[\]]/, "\\]");
    var regex = new RegExp("[\\?&]" + name + "=([^&#]*)");
    if(location.search === ""){
        results = regex.exec(location.hash);
    } else {
        results = regex.exec(location.search);
    }
    return results === null ? false : decodeURIComponent(results[1].replace(/\+/g, " "));
}

function validaCookie(boolean, valueDefault){
    var cookieBanner;
    var bannerActivo = valueDefault;

    console.log("BANNER ACTIVO: " + bannerActivo);

    switch(boolean){
        case false:
            console.log("No Existe La cookie entonces la creo");
            cookieBanner = getParameterByName('utm_campaign');

            if (cookieBanner) {
                console.log("Existe utm_campaign");
                bannerActivo = cookieBanner;
            }
            console.log("BANNER ACTIVO DENTRO DE SWITCH NO EXISTE COOKIE: " + bannerActivo);
        break;

        default:
            console.log("Existe la cookie entonces No La creo");
            cookieBanner = getParameterByName('utm_campaign');

            if (cookieBanner) {
                console.log("Existe utm_campaign entonces Actualizo");
                bannerActivo = cookieBanner;
            }
            console.log("BANNER ACTIVO DENTRO DE SWITCH EXISTE COOKIE: " + bannerActivo);
        break;

    }
    return bannerActivo;
}

function activarCookies(){

    //let url = location.href.indexOf( "buscador" );
    var url = location.pathname;
    console.log("URL: " + url);
    var activar = false;

    switch( url ){
        case '/landing-web':
            activar = false;
            console.log("Modo " + activar + " Cookies Amiguito !! " + url);
        break;

        case '/landing-web-calculadora':
            activar = false;
            console.log("Modo " + activar + " Cookies Amiguito !! " + url);
        break;

        case '/testvocacional-web':
            activar = false;
            console.log("Modo " + activar + " Cookies Amiguito !! " + url);
        break;

        case '/testvocacional-promotor':
            activar = false;
            console.log("Modo " + activar + " Cookies Amiguito !! " + url);
        break;

        case '/referido-tlmk':
            activar = true;
            console.log("Modo " + activar + " Cookies Amiguito !! " + url);
        break;

        case '/referido-web':
            activar = true;
            console.log("Modo " + activar + " Cookies Amiguito !! " + url);
        break;

        case '/referido-onb':
            activar = true;
            console.log("Modo " + activar + " Cookies Amiguito !! " + url);
        break;

        case '/referido-promotor':
            activar = true;
            console.log("Modo " + activar + " Cookies Amiguito !! " + url);
        break;

        default:
        break;
    }

    console.log( "Modo: " + activar );
    return activar;
}

function getUrlParameterByName(name) {
    name = name.replace(/[\[]/, "\\[").replace(/[\]]/, "\\]");
    var regex = new RegExp("[\\?&]" + name + "=([^&#]*)"),
    results = regex.exec(location.search);
    return results === null ? "" : decodeURIComponent(results[1].replace(/\+/g, " "));
}