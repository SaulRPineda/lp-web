import { getCookie } from './Cookies';

/*Funcion para almacenar datos en Local Storage BY SRP 28-02-2018*/
export function setlocalStorage(name, value) {
    return localStorage.setItem(name, value);
}
/*End Funcion para almacenar datos en Local Storage BY SRP 28-02-2018*/

/*Funcion para almacenar datos en Local Storage BY SRP 28-02-2018*/
export function getlocalStorage(name) {
    return localStorage.getItem(name);
}
/*End Funcion para almacenar datos en Local Storage BY SRP 28-02-2018*/

/*Funcion para remover datos en Local Storage BY SRP 28-02-2018*/
export function removelocalStorage(name) {
    localStorage.removeItem(name);
   	return true;
}
/*End Funcion para remover datos en Local Storage BY SRP 28-02-2018*/

/*Funcion para actualizar conjunto de datos en Local Storage BY SRP 28-02-2018*/
export function updatelocalStorage(name, cookie) {
    let compruebaRemove = removelocalStorage(name);

    if (compruebaRemove) {
        console.log("Valor a Local: " + decodeURIComponent(getCookie(cookie)));
        setlocalStorage(name, decodeURIComponent(getCookie(cookie)));
    }
}
/*End Funcion para actualizar conjunto de datos en Local Storage BY SRP 28-02-2018*/

/*Funcion para actualizar un dato en Local Storage BY SRP 28-02-2018*/
export function updateValorStorage(name, value){
	let compruebaRemove = removelocalStorage(name);

    if (compruebaRemove) {
        console.log("Valor a Local: " + decodeURIComponent(getCookie(value)));
        setlocalStorage(name, value);
    }
}
/*End Funcion para actualizar un dato en Local Storage BY SRP 28-02-2018*/