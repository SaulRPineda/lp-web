<html lang="es">
	<head>
		<?php
			include 'cabeceras.php';
	 	?>
		<title>Test Vocacional | UNITEC Universidad Tecnológica de México</title>
	</head>
	<body>
		<div class="container" id="mitos">
		<?php include 'head_app.php'; ?>
			<div class="row">
				<div class="col-lg-12">			
					<center><h2>Esta es la oferta académica que tenemos para ti:</h2></center><br/>
				</div>
			</div>
			<div class="row">
				<div class="col-lg-2"></div>
				<div class="col-lg-8">
					<div class="row" id="area1">
						<div class="col-lg-7">
							<img src="images/mitos/area1.jpg" class="img-responsive">
						</div>
						<div class="col-lg-5 padding-minus">
							<p class="white-txt"><strong>El estructurado.</strong> Te gusta construir, desarmar, ajustar, adaptar y ver cómo funcionan las cosas. Tu pensamiento es metódico y aunque en ocasiones no tengas que seguir las reglas paso a paso, lo tuyo son las ingenierías. </p>
						</div>
					</div>
					<div class="row" id="carreras1">
						<div class="col-lg-12 padding-zero">
						<br/>
							<ul>
<?php
	include ("conexion.php");
	//Consulta
	$consulta= "Select * from Catalogo_Carrera where AreaTest='1'";
	$resultado_camp= $mysqli -> query($consulta);
	while($fila = $resultado_camp -> fetch_array()){
		echo '<a href="fisicomatematicas-mitos-y-realidades?car='.$fila['id_horaLibre'].'"><li>'.utf8_encode($fila['Carrera_front']).'</li></a>';
	}
?>					

							</ul>
						</div>
					</div>			
				</div>
				<div class="col-lg-2"></div>		
			</div>
		</div>
	</body>
</html>